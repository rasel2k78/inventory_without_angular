ReactDOM.render(
	<span>নতুন ইউনিট  যোগ করুন!</span>,
	document.getElementById('form_header_text')
	);

var UnitRow = React.createClass({
	render: function(row) {
		return (
			<tr key={row.id}>
			<td>{row.name}</td>
			<td>{row.link}</td>
			</tr>
			);
	}
});

var UnitList = React.createClass({
	getInitialState: function() {
		return {
			unitList: []
		};
	},

	componentDidMount: function() {
		$.get(this.props.source, function(result) {
			if (this.isMounted()) {
				this.setState({
					unitList: JSON.parse(result)
				});
			}
		}.bind(this));
	},  

	deleteUnit: function(e) {
		console.log(e);
		console.log(e.target.attributes.value);
		this.setState(state => {
			state.items.splice(taskIndex, 1);
			return {items: state.items};
		});
		onChange: function(e) {
			this.setState({ task: e.target.value });
		},
	},

	render: function() {
		var that = this;
		return (<table className="table table-striped table-bordered table-hover">
			<thead>
			<tr>
			<th>ইউনিটের  নাম </th>
			<th>পরিবর্তন / ডিলিট</th>
			</tr>
			</thead>
			<tbody>
			{this.state.unitList.map(function(row) {
				return <tr key={row.units_id}>
				<td>{row.units_name}</td>
				<td> 
				<a className="btn btn-primary edit_unit" id={'edit_'+row.units_id}>পরিবর্তন </a>
				<a className="btn btn-danger delete_unit" onClick={that.deleteUnit} value={row.units_id}>ডিলিট </a>
				</td>
				</tr>;
			})}
			</tbody>
			</table>);
	}
});

ReactDOM.render(
	<UnitList source="unit/test" />,
	document.getElementById('unit_table')
	);