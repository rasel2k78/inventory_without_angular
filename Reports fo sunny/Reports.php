<?php 
/**
 * Report Controller for generating all types of reports.
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    Shoaib <shofik.shoaib@gmail.com>
 * @author    Musabbir Mamun <musabbir.mamun@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for generating different types of reports. 
 *
 * @package Controller
 * @author  Shoaib <shofik.shoaib@gmail.com>
  * @author  Musabbir Mamun <musabbir.mamun@gmail.com>
 **/
class Reports extends CI_Controller
{
    /**
 * Construtor function load the language files and inventroy model
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 * @author Musabbir Mamun <musabbir.mamun@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('access_message_lang', $cookie);
     $this->load->model('Inventory_model');
     $this->load->model('Reports_model');
     $this->load->model('User_access_check_model');
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('report_lang', $cookie);
     $this->lang->load('left_side_nev_lang', $cookie);
     $user_id = $this->session->userdata('user_id');
     if ($user_id == null) {

      redirect('Login', 'refresh');
    }
  }
    /**
 * Load the report view page.
 *
 * @return void
 * @author Musabbir Mamun <musabbir.mamun@gmail.com>
 **/
    public function index()
    {
      load_header();
      $user_id_logged = $this->session->userdata('user_id');
      $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
      {
       $this->load->view('report/report_page');
     }
     else{
      $this->load->view('access_deny/not_permitted');
    }
    load_footer();
  }
    /**
 * Shows last 30 days sell history for datatatble
 *
 * @return void
 * @author Musabbir Mamun <musabbir.mamun@gmail.com>
 **/
    public function monthly_sell_info_for_datatable()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->monthly_sell_info_for_datatable($filters);
     $all_data_without_limit = $this->Inventory_model->monthly_sell_info_for_datatable($filters, true);
     $all_data_final = $this->Inventory_model->monthly_sell_info_for_datatable($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days buy history for datatatble
 *
 * @return void
 * @author Musabbir Mamun <musabbir.mamun@gmail.com>
 **/
    public function monthly_buy_info_for_datatable()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->monthly_buy_info_for_datatable($filters);
     $all_data_without_limit = $this->Inventory_model->monthly_buy_info_for_datatable($filters, true);
     $all_data_final = $this->Inventory_model->monthly_buy_info_for_datatable($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }

   public function getBuyItemsByVoucher($voucher_no)
   {
    echo json_encode($this->Inventory_model->getBuyItemsByVoucher($voucher_no));
  }
  public function getSoldItemsByVoucher($voucher_no)
  {
    echo json_encode($this->Inventory_model->getSoldItemsByVoucher($voucher_no));
  }

    /**
 * Shows last 30 days top sell items history for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_top_sell_items_report()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_top_sell_items_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_top_sell_items_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_top_sell_items_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days top sales representative history for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_top_sales_rep_report()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_top_sales_rep_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_top_sales_rep_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_top_sales_rep_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days top customers history for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_top_customers_report()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_customers_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_customers_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_customers_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days all expenses list for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_expenses_list()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_expenses_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_expenses_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_expenses_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days al demage and lost items history for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_demage_lost_list()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_demage_lost_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_demage_lost_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_demage_lost_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows last 30 days top vendors history for datatatble
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function monthly_vendor_info()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->monthly_vendor_info_for_datatable($filters);
     $all_data_without_limit = $this->Inventory_model->monthly_vendor_info_for_datatable($filters, true);
     $all_data_final = $this->Inventory_model->monthly_vendor_info_for_datatable($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows all loan list for datatable
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_loan_list()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_loan_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_loan_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_loan_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows all buy due list for datatable
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_buy_due_list()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_buy_due_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_buy_due_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_buy_due_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Shows all sell due list for datatable
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_sell_due_list()
    {
     $filters = $this->input->get();
     $all_data = $this->Inventory_model->all_sell_due_report_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_sell_due_report_info($filters, true);
     $all_data_final = $this->Inventory_model->all_sell_due_report_info($filters, true);
     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }
    /**
 * Calculate profit loss for report
 *
 * @return  void
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
    public function print_report_preview()
    {
      $report_type= $this->input->post('cus_report_type', TRUE);
      $from_create_date= $this->input->post('from_create_date', TRUE);
      $to_create_date= $this->input->post('to_create_date', TRUE);
      $search_limit = $this->input->post('cus_seach_limit', TRUE);
      switch ($report_type)
      {

        case "due_order_by_customer":
        $output_data['due_details'] = $this->Reports_model->due_details_by_customer();
        $output_data['total_due'] = $this->Reports_model->total_due_of_customer();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/due_list_by_customer', $output_data, FALSE);
        load_footer();
        break;
        case "due_order_by_vendor":
        $output_data['due_details'] = $this->Reports_model->due_details_by_vendor();
        $output_data['total_due'] = $this->Reports_model->total_due_of_vendor();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/due_list_by_vendor', $output_data, FALSE);
        load_footer();
        break;
        case "item_purchase_and_inventory":
        $output_data['item_history'] = $this->Reports_model->item_details_history();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/items_details_report', $output_data, FALSE);
        load_footer();
        break;
        case "sell_items_report":
        $output_data['sell_items_data'] = $this->Reports_model->sell_items_report_info($from_create_date,$to_create_date);
        $output_data['sell_details'] = $this->Reports_model->total_sell_details($from_create_date,$to_create_date);
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/sell_items_report', $output_data, FALSE);
        load_footer();
        break;

        case "sell_details_report":
        $output_data['sell_details_data'] = $this->Reports_model->sell_details_report_info($from_create_date,$to_create_date);
        $output_data['sell_details'] = $this->Reports_model->total_sell_details($from_create_date,$to_create_date);
        $selected_items= $this->Reports_model->all_sell_items_list($from_create_date,$to_create_date);
        $output_data['total_buying_cost']= 0;
        foreach ($selected_items as $value_selected_item) 
        {
          $selected_item_sped_id = $value_selected_item['item_spec_set_id'];
          $get_avg_buying_price= $this->Reports_model->get_avg_buying_price_info($selected_item_sped_id, $to_create_date);
          $final_avg_price = round($get_avg_buying_price['sum_price']/$get_avg_buying_price['total_quantity'],2);
          $output_data['total_buying_cost']+= $value_selected_item['total_quantity']* $final_avg_price;
        }
        $output_data['total_profit']= $output_data['sell_details']['net_payable'] - ($output_data['total_buying_cost'] + $output_data['sell_details']['card_charge']);
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/sell_details_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "buy_details_report":
        $output_data['item_inventory_data'] = $this->Reports_model->new_calculate_total_items_inventory($from_create_date,$to_create_date);
        $output_data['buy_details'] = $this->Reports_model->total_buy_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/new_item_inventroy_template', $output_data, FALSE);
        load_footer();
        break;
        case "buy_report":
        $output_data['buy_data'] = $this->Reports_model->calculate_total_buy($from_create_date,$to_create_date);
        $output_data['buy_details'] = $this->Reports_model->total_buy_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/buy_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "sell_report":
        $output_data['sell_data'] = $this->Reports_model->calculate_total_sell($from_create_date,$to_create_date);
        $output_data['sell_details'] = $this->Reports_model->total_sell_details($from_create_date,$to_create_date);
        $selected_items= $this->Reports_model->all_sell_items_list($from_create_date,$to_create_date);
        $output_data['total_buying_cost']= 0;
        foreach ($selected_items as $value_selected_item) 
        {
          $selected_item_sped_id = $value_selected_item['item_spec_set_id'];
          $get_avg_buying_price= $this->Reports_model->get_avg_buying_price_info($selected_item_sped_id, $to_create_date);
          $final_avg_price = round($get_avg_buying_price['sum_price']/$get_avg_buying_price['total_quantity'],2);
          $output_data['total_buying_cost']+= $value_selected_item['total_quantity']* $final_avg_price;
        }
        $output_data['total_profit']= $output_data['sell_details']['net_payable'] - ($output_data['total_buying_cost'] + $output_data['sell_details']['card_charge']);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/sell_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "vat_report":
        $output_data['vat_data'] = $this->Reports_model->calculate_total_vat($from_create_date,$to_create_date);
        $output_data['vat_details'] = $this->Reports_model->total_vat_amount($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/vat_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "item_inventory":
        $output_data['item_inventory_data'] = $this->Reports_model->calculate_total_items_inventory($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/item_inventory_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "expense_report":
        $output_data['expense_data'] = $this->Reports_model->calculate_total_expense($from_create_date,$to_create_date);
        $output_data['expense_details'] = $this->Reports_model->total_expense_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/expense_report_template', $output_data, FALSE);
        load_footer();
        break;

        case "exchange_report":
        $output_data['exchange_data'] = $this->Reports_model->get_all_exchange_history($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/exchange_report_template', $output_data, FALSE);
        load_footer();
        break;

        case "return_report":
        $output_data['return_data'] = $this->Reports_model->get_all_return_history($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/return_report_template', $output_data, FALSE);
        load_footer();
        break;

        case "vendor_payment_history":
        $vendor_id = $this->input->post('vendors_id',TRUE);
        $output_data['vendor_data']=$this->Reports_model->get_vendor_info_by_id($vendor_id);
        $buy_id = $this->Reports_model->all_buy_id_by_vendors($vendor_id);
        $output_data['payment_history']= $this->Reports_model->all_payment_history_info($buy_id);
        $output_data['store_info']= $this->Reports_model->store_details();
        // $output_data['date_from']=$from_create_date;
        // $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/vendor_payment_history_template', $output_data, FALSE);
        load_footer();
        break;

        case "category_history":
        $categories_id = $this->input->post('categories_id',TRUE);
        $output_data['category_history']= $this->Reports_model->category_item_details($categories_id);
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/category_history_template', $output_data, FALSE);
        load_footer();
        break;

        case "customer_payment_history":
        $customer_id = $this->input->post('customers_id',TRUE);
        $output_data['customer_data']=$this->Reports_model->get_customer_info_by_id($customer_id);
        $sell_id = $this->Reports_model->all_sell_id_by_vendors($customer_id);
        $output_data['payment_history']= $this->Reports_model->all_sell_payment_history_info($sell_id);
        $output_data['store_info']= $this->Reports_model->store_details();
        // $output_data['date_from']=$from_create_date;
        // $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/customer_payment_history_template', $output_data, FALSE);
        load_footer();
        break;
        case "individual_sales_rep_report":
        $output_data['sales_rep_data']=$this->Reports_model->get_sales_rep_by_id($from_create_date,$to_create_date);
        // echo "<pre>";
        // print_r($output_data['sales_rep_data']);
        // exit();
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/individual_sales_rep_template', $output_data, FALSE);
        load_footer();
        break;
        case "damage_lost_report":
        $output_data['damage_lost_data'] = $this->Reports_model->calculate_total_damage_lost($from_create_date,$to_create_date);
        $output_data['damage_lost_details'] = $this->Reports_model->total_damage_lost_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/damage_lost_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "bank_account_report":
        $output_data['account_balance_data'] = $this->Reports_model->calculate_bank_account_balance();
        $output_data['total_amount'] = $this->Reports_model->total_amounts_details();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/bank_account_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "loan_report":
        $output_data['loan_data'] = $this->Reports_model->calculate_total_loan($from_create_date,$to_create_date);
        $output_data['loan_details'] = $this->Reports_model->total_loan_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/loan_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "dpst_wtdrl_report":
        $output_data['deposit_wtdrl_data'] = $this->Reports_model->calculate_total_dpst_wtdrl($from_create_date,$to_create_date);
        $output_data['deposit_details'] = $this->Reports_model->total_dpst_details($from_create_date,$to_create_date);
        $output_data['withdrawal_details']= $this->Reports_model->total_wtdrl_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/dept_wtdrl_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "buy_due_report":
        $output_data['buy_due_data'] = $this->Reports_model->calculate_total_buy_due($from_create_date,$to_create_date);
        $output_data['buy_due_details'] = $this->Reports_model->total_buy_due_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/buy_due_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "sell_due_report":
        $output_data['sell_due_data'] = $this->Reports_model->calculate_total_sell_due($from_create_date,$to_create_date);
        $output_data['sell_due_details'] = $this->Reports_model->total_sell_due_details($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/sell_due_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "all_customers":
        $output_data['all_customer_data'] = $this->Reports_model->all_customers_list();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/all_customers_list_template', $output_data, FALSE);
        load_footer();
        break;
        case "all_vendors":
        $output_data['all_vendor_data'] = $this->Reports_model->all_vendors_list();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/all_vendors_list_template', $output_data, FALSE);
        load_footer();
        break;
        case "available_imei_report":
        $output_data['available_imei']= $this->Reports_model->get_available_imei_info();
        $output_data['store_info']= $this->Reports_model->store_details();
        load_header();
        $this->load->view('report/imei_quantity_template', $output_data, FALSE);
        load_footer();
        break;
        case "card_payment_report":
        $output_data['card_payment_info']= $this->Reports_model->get_card_payment_info($from_create_date,$to_create_date);
        $output_data['card_cash_payment']= $this->Reports_model->get_card_amout_for_cash_card_payment_info($from_create_date,$to_create_date);
        $output_data['total_card_collection']= $this->Reports_model->get_total_card_collection($from_create_date,$to_create_date);
        $output_data['total_collection_from_cash_card']= $this->Reports_model->get_cash_card_collection($from_create_date,$to_create_date);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/card_payment_template', $output_data, FALSE);
        load_footer();
        break;
        case "top_rev_items":
        $output_data['top_rev_data'] = $this->Reports_model->calculate_top_rev_items($from_create_date,$to_create_date,$search_limit);
        $output_data['top_rev_details'] = $this->Reports_model->top_rev_items_details($from_create_date,$to_create_date,$search_limit);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/top_revenue_items_report_template', $output_data, FALSE);
        load_footer();
        break;
        case "top_categories":
        $output_data['top_rev_data'] = $this->Reports_model->calculate_top_rev_category($from_create_date,$to_create_date,$search_limit);
        $output_data['store_info']= $this->Reports_model->store_details();
        $output_data['date_from']=$from_create_date;
        $output_data['date_to']=$to_create_date;
        load_header();
        $this->load->view('report/top_category_template', $output_data, FALSE);
        load_footer();
        break;
        case "tot_amt_inv_items":
        $output_data['available_items']=$this->Reports_model->available_items_info();
        $final_data['item_info'] =array();
        $final_data['total_amt']=0;
        $final_data['total_quantity']=0;
        foreach ($output_data['available_items'] as $v_available_item) {
          $item_id = $v_available_item['item_spec_set_id'];
          $total_sell_qty= 0;
          $total_demage_lost_qty=0;
          $total_tranfer_qty=0;
          $output_data2['total_buy_amt']=0;
          $output_data2['total_buy_qty']=0;
          $output_data['buy_info'] = $this->Reports_model->buy_info_by_id($item_id);
          $output_data['sell_info'] = $this->Reports_model->sell_info_by_id($item_id);
          $output_data['damage_lost_info']= $this->Reports_model->damage_Lost_info_by_id($item_id);
          $output_data['transfer_info'] = $this->Reports_model->transfer_info_by_id($item_id);
          foreach ($output_data['sell_info'] as $v_sell_info) {
            $total_sell_qty+= $v_sell_info['quantity'];
          }
          foreach ($output_data['damage_lost_info'] as $v_demage_lost) {
            $total_demage_lost_qty+= $v_demage_lost['demage_lost_quantity'];
          }
          foreach ($output_data['transfer_info'] as $v_transfer_info) {
           $total_tranfer_qty += $v_transfer_info['quantity'];
         }
         $unsold_qty = $total_sell_qty+$total_demage_lost_qty+$total_tranfer_qty;
         foreach ($output_data['buy_info'] as $key=>$v_buy_info) {
          if($unsold_qty <= $output_data['buy_info'][$key]['quantity'])
          {
            $output_data['buy_info'][$key]['quantity']-=$unsold_qty;
            $unsold_qty = 0;
          }
          else
          {
            $output_data['buy_info'][$key]['quantity']=0;
            $unsold_qty -= $output_data['buy_info'][$key]['quantity'];
          }
          if($unsold_qty == 0)
          {
           break;
         }
       }
       foreach ($output_data['buy_info'] as $value_data) {
        $output_data2['item_name']= $value_data['spec_set'];
        $output_data2['parent_category']= $value_data['parent_name'];
        $output_data2['child_category']= $value_data['categories_name'];
        $output_data2['total_buy_qty']+= $value_data['quantity'];
        $output_data2['total_buy_amt']+= ($value_data['quantity'] * $value_data['buying_price']);
      }
      $final_data['total_quantity']+= $output_data2['total_buy_qty'];
      $final_data['total_amt']+=$output_data2['total_buy_amt'];
      array_push($final_data['item_info'], $output_data2);
    }
    $final_data['store_info']= $this->Reports_model->store_details();
    load_header();
    $this->load->view('report/total_amt_of_inv_item_tempplate', $final_data, FALSE);
    load_footer();
    break;
    case "profit_loss":
    $vat_include = $this->input->post('vat_amt_selection', TRUE);
    if ($vat_include == 'yes') {
      $output_data['total_vat_amount']= $this->Reports_model->total_vat_amount($from_create_date,$to_create_date);
    }
    else{
      $output_data['total_vat_amount']=array('vat_amount' =>0);   
    }

    $selected_items= $this->Reports_model->all_sell_items_list($from_create_date,$to_create_date);
    $output_data['total_buying_cost']= 0;
    foreach ($selected_items as $value_selected_item) 
    {
      $selected_item_sped_id = $value_selected_item['item_spec_set_id'];
      $get_avg_buying_price= $this->Reports_model->get_avg_buying_price_info($selected_item_sped_id, $to_create_date);
      $final_avg_price = round($get_avg_buying_price['sum_price']/$get_avg_buying_price['total_quantity'],2);
      $output_data['total_buying_cost']+= $value_selected_item['total_quantity']* $final_avg_price;
    }
    $output_data['total_commission'] = $this->Reports_model->total_commission_amt($from_create_date,$to_create_date);
    $output_data['total_sell_amount'] = $this->Reports_model->total_sell_amt($from_create_date,$to_create_date);
    $output_data['total_card_charge_amt'] = $this->Reports_model->total_card_charge_amt($from_create_date,$to_create_date);
    $output_data['total_damage_amount'] = $this->Reports_model->total_damage_amt($from_create_date,$to_create_date);
    $output_data['total_expense_amount'] = $this->Reports_model->total_expense_amt($from_create_date,$to_create_date);
    $output_data['total_landed_cost'] = $this->Reports_model->total_landed_cost_amt($from_create_date,$to_create_date);
    $output_data['total_debit_amount']= $output_data['total_buying_cost'] + $output_data['total_damage_amount']['damage_lost_amount']+ $output_data['total_landed_cost']['landed_cost']+$output_data['total_expense_amount']['expenditures_amount']+ $output_data['total_vat_amount']['vat_amount']+ $output_data['total_card_charge_amt']['card_charge_amount'] ;
    $output_data['total_credit_amount']= $output_data['total_sell_amount']['paid'] + $output_data['total_commission'];
    $output_data['store_info']= $this->Reports_model->store_details();
    $output_data['date_from']=$from_create_date;
    $output_data['date_to']=$to_create_date;
    load_header();
    $this->load->view('report/profit_loss_report', $output_data, FALSE);
    load_footer();
    break;
    default:
    echo "Please Try Again.";
  }
}
}
/* End of file Reports.php */
/* Location: ./application/controllers/Reports.php */