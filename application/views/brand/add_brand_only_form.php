<?php
/**
 * Form Name:"Brand Form" 
 *
 * This is brand form for insert , edit and update a brand.
 * 
 * @link   Brand/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>
<div style="display:none" role="alert">দুঃখিত !! ব্রান্ডের তথ্য সেভ হয়নি</div>
<div class="alert alert-success" id="insert_success" style="display:none" role="alert">সফলভাবে ব্রান্ডের তথ্য সেভ হয়েছে</div>
<div class="alert alert-success" id="update_success" style="display:none" role="alert">সফলভাবে ব্রান্ডের তথ্য আপডেট হয়েছে</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i><div id="form_header_text">নতুন ব্রান্ড যোগ করুন</div>
        </div>
        <div class="tools">
            <!-- BEGIN PAGE LEVEL STYLES -->
            <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
            <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
            <!-- END PAGE LEVEL STYLES -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success" id="delete_success" style="display:none" role="alert">সফলভাবে ব্রান্ডের তথ্য ডিলিট হয়েছে</div>
                    <div class="alert alert-danger" id="insert_failure" style="d
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form id="brand_form" class="form-horizontal form-bordered" action="<?php echo site_url('/Brand/save_brand_info');?>" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                        <div class="form-group <?php if(isset($error['brands_name'])) { echo 'has-error'; 
                       } ?> ">
                            <label class="control-label col-md-3">ব্রান্ডের নাম *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="brands_name" placeholder="ব্রান্ডের নাম" value="<?php if(isset($previous_data['brands_name'])) { echo $previous_data['brands_name']; 
                               } ?>" >
                                <span class="help-block"><?php if(isset($error['brands_name'])) { echo $error['brands_name']; 
                               } ?></span>
                            </div>
                        </div>
                        <div class="form-group <?php if(isset($error['brands_description'])) { echo 'has-error'; 
                       } ?>">
                            <label class="control-label col-md-3">ব্রান্ডের বিবরণ</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="brands_description"  placeholder="ব্রান্ডের বিবরণ" value="<?php if(isset($previous_data['brands_description'])) { echo $previous_data['brands_description']; 
                               } ?>" >
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-md-3">ব্রান্ডের লোগো</label>
                            <div class="col-md-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new">
                                                লোগো নির্বাচন করুন
                                            </span>
                                            <span class="fileinput-exists">
                                                পরিবর্তন 
                                            </span>
                                            <input type="file" name="brands_logo">
                                        </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                            ডিলিট 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn green">সেভ করুন</button>
                        <button type="reset" class="btn default">বাতিল করুন</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 hide">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>সকল ব্রান্ডের তালিকা
                </div>
                <div class="tools">
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                ব্রান্ডের নাম
                            </th>
                            <th>
                                পরিবর্তন / ডিলিট
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        <!-- foreach loops use here for php based data retreive -->
                    </tbody>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	