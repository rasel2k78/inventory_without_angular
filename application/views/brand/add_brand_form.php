<?php
/**
 * Form Name:"Brand Form" 
 *
 * This is brand form for insert , edit and update a brand.
 * 
 * @link   Brand/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failure'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('insert_success'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?></div>

        <div class="alert alert-danger" id="no_brand" style="display:none" role="alert"><?php echo $this->lang->line('brand_name_required'); ?></span></div>


        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><span id="header_text"><?php echo $this->lang->line('left_portlet_title'); ?></span>
                </div>
                
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <!-- <form id="brand_form" class="form-horizontal form-bordered" action="<?php echo site_url('/Brand/save_brand_info');?>" method="post" enctype="multipart/form-data">
            -->

    <?php 
    $attributes = array('class' => 'form-horizontal form-bordered', 'id' => 'brand_form');
    echo form_open('Brand/save_brand_info', $attributes);
    ?>
            <div class="form-body">
                <div class="form-group <?php if(isset($error['brands_name'])) { echo 'has-error'; 
               } ?> ">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('brand_name'); ?>*</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="brands_name" placeholder="<?php echo $this->lang->line('brand_name'); ?>" value="<?php if(isset($previous_data['brands_name'])) { echo $previous_data['brands_name']; 
                       } ?>" >
                        <span class="help-block"><?php if(isset($error['brands_name'])) { echo $error['brands_name']; 
                       } ?></span>
                    </div>
                </div>

                <!-- <div class="form-group <?php if(isset($error['brands_description'])) { echo 'has-error'; 
               } ?>">
                    <label class="control-label col-md-3">Select Vendor</label>
                    <div class="col-md-4">
                        <select class="form-control" id="vendor_select" name="vendors_id" data-live-search="true">
                            <!-- <option>select one</option> -->
                    <!--    </select>
                    </div>
                    <div class="fa-item col-md-3 col-sm-4">
                        <a><i class="fa fa-plus-circle"></i></a>
                    </div>
                </div> -->

                <div class="form-group <?php if(isset($error['brands_description'])) { echo 'has-error'; 
               } ?>">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('brand_description'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="brands_description"  placeholder="<?php echo $this->lang->line('brand_description'); ?>" value="<?php if(isset($previous_data['brands_description'])) { echo $previous_data['brands_description']; 
                       } ?>" >
                    </div>
                </div>
                <div id="image_of_user" class="form-group ">
                    <label id="image_of_user_label" class="control-label col-md-3"> <?php echo $this->lang->line('brand_logo'); ?></label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div id="brands_logo" class="fileinput-preview thumbnail" name="brands_logo" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            </div>



                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new">
            <?php echo $this->lang->line('brand_select_picture'); ?>
                                    </span>
                                    <span class="fileinput-exists">
            <?php echo $this->lang->line('edit'); ?> 
                                    </span>
                                    <input type="file" name="brands_logo">
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
            <?php echo $this->lang->line('delete'); ?>  
                                </a>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <div class="form-actions">
                <input type="submit" value="<?php echo $this->lang->line('save'); ?>" name="image_upload" id="image_upload" class="btn green"/>
                <button type="reset" id="brand_cancle" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
</div>
<div class="col-md-6">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i><?php echo $this->lang->line('right_portlet_title'); ?>
            </div>
            
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
        <?php echo $this->lang->line('brand_name'); ?>
                        </th>
                        <th>
        <?php echo $this->lang->line('edit'); ?> / <?php echo $this->lang->line('delete'); ?>
                        </th>

                    </tr>
                </thead>
                <tbody>

                    <!-- foreach loops use here for php based data retreive -->
                </tbody>

            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

</div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
</div>
</div>
<!-- 
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">পরিবর্তন নিশ্চিতকরণঃ <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                আপনি কি জমা / উত্তোলনের এই তথ্য পরিবর্তন করতে চান ? 
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
        <button type="button" id="edit_confirmation" class="btn blue">Yes</button>
    </div>
</div> -->



<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--  <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
// 	// table_brand = $('#sample_2').DataTable();		

var table_brand = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/brand/all_brands_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});

$('#brand_cancle').click(function(event) {
    $('input[name="brands_name"]').val('');
    $('input[name="brands_description"]').val('');
    $('.fileinput-preview').html("<img src=''>");
});
// </script>

<script type="text/javascript">

    $('table').on('click', '.edit_brand',function(event) {
        event.preventDefault();

        var brand_id = $(this).attr('id').split('_')[1];
        $('#current_brand_id').remove();

        $('.has-error').removeClass('has-error');
        $('.help-block').text('');

        $.get('<?php echo site_url();?>/Brand/get_brand_info/'+brand_id).done(function(data) {

            var brand_data = $.parseJSON(data);
            if(brand_data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
            var html = "<input id='current_brand_id' type='hidden' class='form-control' name='brands_id' value='"+brand_id+"'>";
            $('#brand_form').append(html);
            $('#brand_form').attr('action', "<?php echo site_url('/Brand/update_brand_info');?>");
            //$('#form_header_text').text("ব্রান্ডের তথ্য পরিবর্তন করুন");
            $('#header_text').text("<?php echo $this->lang->line('edit_brand'); ?>");
            $('input[name = brands_name]').val(brand_data.brand_info_by_id.brands_name);
            $('input[name = brands_description]').val(brand_data.brand_info_by_id.brands_description);

            if(brand_data.brand_info_by_id.brands_logo!="" && brand_data.brand_info_by_id.brands_logo!=null)
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+brand_data.brand_info_by_id.brands_logo+"'>");
            }
            else
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/brand_images/no_image.png'>");
            }

        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

</script>
<script type="text/javascript">
    var csrf = $("input[name='csrf_test_name']").val();


    var brands_id = '' ;
    $('table').on('click', '.delete_brand',function(event) {
        event.preventDefault();
        brands_id = $(this).attr('id').split('_')[1];
    });

    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        //var brands_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'brands_id' : brands_id,
            csrf_test_name: csrf

        }; 

        $.post('<?php echo site_url();?>/Brand/delete_brand/'+brands_id,post_data).done(function(data) {
            if(data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                $('#responsive_modal_delete').modal('hide');
            }
            else if(data = "success")
            {
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                $('#responsive_modal_delete').modal('hide');
                table_brand.row($('#delete_'+brands_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );        
            }


        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });

/*
    $('#brand_form').submit(function(event) {
        event.preventDefault();
        var data = {};
        data.brands_name = $('input[name=brands_name]').val();
        data.brands_description = $('input[name=brands_description]').val();

        $.post('<?php echo site_url() ?>/brand/save_brand_info',data).done(function(data1, textStatus, xhr) {
            var data2 = $.parseJSON(data1);
            if(data2.success==0)
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }
            else
            {
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();

                $('input[name= brands_name]').val('');
                $('#insert_success').slideDown();

                table_brand.ajax.reload();

                setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
                table_brand.row.add({
                    "0":       data.brands_name,
                    "1":   "<a class='btn btn-primary edit_brand' id='edit_"+data2.data+"' >    পরিবর্তন </a><a class='btn btn-danger delete_brand' id='delete_"+data2.data+"'>ডিলিট </a>"
                }).draw();
            }

        }).error(function() {

            $('#insert_failure').slideDown();
            setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        });    
});*/



//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#brand_form').ajaxForm({
    beforeSubmit: function() {    
    },
    success: function(msg) {
        if(msg == "Give a brand name")
        {
            $('#no_brand').slideDown();
            setTimeout( function(){$('#no_brand').slideUp()}, 1500 );exit;
        }
    },
    complete: function(xhr) {
        $('#insert_success').slideDown();
        setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
        $('input[name="brands_name"]').val('');
        $('input[name="brands_description"]').val('');
        $('.fileinput-preview').html("<img src=''>");

        table_brand.ajax.reload();
        return false;
    }
}); 
//Ajax form submit end



</script>
<script type="text/javascript">
    var csrf = $("input[name='csrf_test_name']").val();

    $(document).ready(function(){
        // Start of AJAX Booostrap for Brand Selection
        var options = {
            ajax          : {
                url     : '<?php echo site_url(); ?>/vendor/search_vendor_by_name',
                type    : 'POST',
                dataType: 'json',
// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
// automatically replace it with the value of the search query.
data    : {
    q: '{{{q}}}',
    csrf_test_name: csrf
}
},
locale        : {
    emptyTitle: 'ব্রান্ড নির্বাচন করুন'
},
log           : 3,
preprocessData: function (data) {
    var i, l = data.length, array = [];
    if (l) {
        for (i = 0; i < l; i++) {
            array.push($.extend(true, data[i], {
                text : data[i].vendors_name,
                value: data[i].vendors_id,
                data : {
                    subtext: data[i].vendors_id
                }
            }));
        }
    }
// You must always return a valid array when processing data. The
// data argument passed is a clone and cannot be modified directly.
return array;
}
};

$('#vendor_select').selectpicker().ajaxSelectPicker(options);
$('#vendor_select').trigger('change');
// End of AJAX Boostrap for Brand selection
});
</script>

