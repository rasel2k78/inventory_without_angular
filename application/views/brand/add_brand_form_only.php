<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title_brand'); ?>
        </div>

    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <!-- <form accept-charset="utf-8" method="post" class="form-horizontal form-bordered" id="unit_form" action="http://localhost/inventory/index.php/Unit"> -->

    <?php 
    $attributes = array('class' => 'form-horizontal form-bordered', 'id' => 'brand_form');
    echo form_open('Brand/save_brand_info', $attributes);
    ?>
        <div class="form-body">
            <div class="form-group <?php if(isset($error['brands_name'])) { echo 'has-error'; 
           } ?> ">
                <label class="control-label col-md-3"><?php echo $this->lang->line('brand_name'); ?> *</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="brands_name" placeholder="<?php echo $this->lang->line('brand_name'); ?>" value="<?php if(isset($previous_data['brands_name'])) { echo $previous_data['brands_name']; 
                   } ?>" >
                    <span class="help-block"><?php if(isset($error['brands_name'])) { echo $error['brands_name']; 
                   } ?></span>
                </div>
            </div>

    <!--         <div class="form-group <?php if(isset($error['brands_description'])) { echo 'has-error'; 
   } ?>">
                <label class="control-label col-md-3">Select Vendor</label>
                <div class="col-md-4">
                    <select class="form-control" id="vendor_select" name="vendors_id" data-live-search="true">
                        <!-- <option>select one</option> -->
                <!--     </select>
                </div>
                <div class="fa-item col-md-3 col-sm-4">
                    <a><i class="fa fa-plus-circle"></i></a>
                </div>
            </div> -->

            <div class="form-group <?php if(isset($error['brands_description'])) { echo 'has-error'; 
           } ?>">
                <label class="control-label col-md-3"><?php echo $this->lang->line('brand_description'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="brands_description"  placeholder="<?php echo $this->lang->line('brand_description'); ?>" value="<?php if(isset($previous_data['brands_description'])) { echo $previous_data['brands_description']; 
                   } ?>" >
                </div>
            </div>
            <div id="image_of_user" class="form-group ">
                <label id="image_of_user_label" class="control-label col-md-3"> <?php echo $this->lang->line('brand_logo'); ?></label>
                <div class="col-md-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div id="brands_logo" class="fileinput-preview thumbnail" name="brands_logo" data-trigger="fileinput" style="width: 200px; height: 150px;">
                        </div>



                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
            <?php echo $this->lang->line('brand_select_picture'); ?>
                                </span>
                                <span class="fileinput-exists">
            <?php echo $this->lang->line('edit'); ?> 
                                </span>
                                <input type="file" name="brands_logo">
                            </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                <?php echo $this->lang->line('delete'); ?> 
                            </a>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <div class="form-actions">
            <input type="submit" value="<?php echo $this->lang->line('save'); ?>" name="image_upload" id="image_upload" class="btn green"/>
            <button type="reset" data-dismiss="modal" class="btn dark btn-outline"><?php echo $this->lang->line('cancle'); ?></button>
        </div>
    <?php echo form_close();?>
    </div>
</div>