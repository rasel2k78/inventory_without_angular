<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/jstree/dist/themes/default/style.min.css">
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<div class="row">
    <div id = "edit_delete_div" class="col-md-6">
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?></div>

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failed'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('insert_succeded'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_succeded'); ?></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption left">

                    <i class="fa fa-gift"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title'); ?></span>
                </div>
            </div>
            <div class="portlet-body form" id="category_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'category_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_portlet_title'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" id="search_key" autocomplete="off" class="form-control" name="categories_name" placeholder="<?php echo $this->lang->line('category_name'); ?>">
                            <div id="suggesstion-box"></div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('category_description'); ?> </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="categories_description" placeholder="<?php echo $this->lang->line('category_description'); ?> ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('main_category_name'); ?>
                        </label>
                        <div class="col-md-6">
                            <input type="text" autocomplete="off" name="category" id="category_select" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">

                            <input type="hidden" autocomplete="off" name="parent_categories_id" class="form-control">


                            <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>


                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="reset" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancle'); ?></button>
                    <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="category_edit_div" style="display:none">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'category_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');
                ?>

                <input type="text" class="form-control" name="categories_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_portlet_title'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="categories_name_edit" placeholder="<?php echo $this->lang->line('categories_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('category_description'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="categories_description_edit" placeholder="<?php echo $this->lang->line('category_description'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('main_category_name'); ?>
                        </label>
                        <div class="col-md-4">
                       <!--      <select class="form-control" id="category_select_edit" name="parent_categories_id_edit" data-live-search="true">
                                <!-- <option>select one</option> 
                            </select> -->


                            <input type="text" autocomplete="off" name="category_select_edit" id="category_select_edit" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">

                            <input type="hidden" autocomplete="off" name="parent_categories_id_edit" class="form-control">


                            <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result_edit" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>



                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" style="margin-left: 8px" id="edit_category_cancle"><?php echo $this->lang->line('cancle'); ?></button>
                    <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?></button>
                    <button type="submit" data-toggle="modal" href="#responsive_modal_delete" id="deleteCategory" class="btn red pull-right"><?php echo $this->lang->line('delete'); ?></button>
                </div>

                <?php echo form_close();?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption right">
                    <i class="fa fa-globe"></i><?php echo $this->lang->line('right_portlet_title'); ?>
                </div>

            </div>
            <div class="portlet-body" id="parent_category_tree">

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?> 
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
        <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<!--  <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jstree/dist/jstree.min.js"></script>

<script src="<?php echo CURRENT_ASSET;?>assets/build/react.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/react-dom.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/browser.min.js"></script>


<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- <script type="text/babel" src="<?php echo CURRENT_ASSET;?>assets/components/category.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    //table_category = $('#sample_2').DataTable();
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
    $(document).ready(function(){
        $("#search_key").keyup(function(){
            if ($(this).val() == '') {
                $("#suggesstion-box").html('');
            }
            else{
                $.ajax({
                    type: "POST",
                    url: "Category/search_category",
                    data    : {
                        'keyword' : $(this).val(),
                        csrf_test_name: csrf
                    },

                    beforeSend: function(){
                        $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        var parsedData = JSON.parse(data);
                        //$("#suggesstion-box").html("");
                        var html_data = '';
                        html_data+= '<ul  role="menu" class="list-group">';                        
                        for (var i in parsedData) 
                        {
                            html_data+= '<li class="list-group-item" data-original-index="'+i+'"><a style="color:black;text-decoration:none;" href="#" onclick="set_value(\''+parsedData[i].categories_name+'\');">'+parsedData[i].categories_name+'</a></li><div style="height:3px;"></div>';
                        }
                        html_data+='</ul>';
                        $("#suggesstion-box").html(html_data);
                    }
                });
            }
        });
    });

    function set_value(search_value)
    {
        $('#search_key').val(search_value);
        $("#suggesstion-box ul").html('');
    }

    $('#brand_cancle').click(function(event) {
        $('input[name="categories_name"]').val('');
        $('input[name="categories_description"]').val('');
        $('input[name="category_select"]').val('');
    });


    var table_category = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/Category/all_category_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ]
    });


    $("#category_select").keyup(function(event) 
    {
        $("#category_select_result").show();
        $("#category_select_result").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result").html(html);
            });
        }, 500);
    });
    
    $("#category_select_result").on('click', 'td', function(event) {
        $('input[name="category"]').val($(this).html());
        $('input[name="parent_categories_id"]').val($(this).attr('data'));
        $("#category_select_result").hide();
    });




    $("#category_select_edit").keyup(function(event) 
    {
        $("#category_select_result_edit").show();
        $("#category_select_result_edit").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select_edit").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result_edit").html(html);
            });
        }, 500);
    });
    $("#category_select_result_edit").on('click', 'td', function(event) {
        $('input[name="category_select_edit"]').val($(this).html());
        $('input[name="parent_categories_id_edit"]').val($(this).attr('data'));
        $("#category_select_result_edit").hide();
    });

</script>

<script type="text/javascript">
    $('#edit_category_cancle').click(function(event) {
        event.preventDefault();
        $('#category_insert_div').slideDown('slow');
        $('#category_edit_div').slideUp('fast');
        $("#edit_delete_div").show();
        $("#category_edit_div").hide();
        $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title'); ?>");
    });

</script>


<!-- JSTREE CODE -->
<script type="text/javascript">

    var category_only = <?php echo json_encode($all_categories) ?>;
    console.log(category_only);
    //FOR GENERATING THE CATEGORY SELECTION TREE FOR ITEM EDIT
    tree_data2=[];
    function generate_tree_data_category(new_data) {
        var t_data=[];
        $.each(category_only, function(index, val) {
            if((val.parent_categories_id=='') || (val.parent_categories_id=='Null'))
            {
                var a={};
                a.text=val.categories_name;
                a.id="parent_"+val.categories_id;
                a.children=get_children_category(val.categories_id);
                a.icon="fa fa-cubes icon-state-danger icon-lg";
                t_data.push(a);
            }
        });
        tree_data2 = t_data;
        if (new_data.length != 0) {
            var a={};
            a.text=new_data.categories_name;
            a.id="parent_"+new_data.categories_id;
            a.children=[];
            a.icon="fa fa-cubes icon-state-danger icon-lg";
            //tree_data2.push(a);
        };
        contextualMenuSample_category();
    }
    function get_children_category (id_cat){
        var cat_array=[];
        $.each(category_only, function(index, val) {
            if(val.parent_categories_id==id_cat)
            {
                var a={};
                a.text=val.categories_name;
                a.id="parent_"+val.categories_id;
                a.children=get_children_category(val.categories_id);
                a.icon="fa fa-cubes icon-state-success icon-lg";
                cat_array.push(a);
            }
        });
        return cat_array;
    }

    var contextualMenuSample_category = function() {
        //console.log(tree_data2);
        $("#parent_category_tree").jstree({
            "core" : {
                "themes" : {
                    "responsive":  true
                }, 
                  // so that create works
                  "check_callback" : true,
                  'data': tree_data2,
              },
              // "state" : { "key" : "demo2" },
              "plugins" : [ "search" ]
          }).on('search.jstree before_open.jstree', function (e, data) {
              if(data.instance.settings.search.show_only_matches) {
                  data.instance._data.search.dom.find('.jstree-node')
                  .show().filter('.jstree-last').filter(function() { return this.nextSibling; }).removeClass('jstree-last')
                  .end().end().end().find(".jstree-children").each(function () { $(this).children(".jstree-node:visible").eq(-1).addClass("jstree-last"); });
              }
          });
      }    
      generate_tree_data_category([]);
    //parent_category_tree.DataTable();

    $(document).on('click', '.jstree-anchor', function(event) {
        event.preventDefault();
        $('#edit_delete_div').css('visibility', 'visible');
        $('.has-error').removeClass('has-error');
        $('.help-block').text('');

        var category_id = $(this).closest('li').attr('id').split('_')[1];
        $('#current_category_id').remove();

        $.get('<?php echo site_url();?>/category/get_category_info/'+category_id).done(function(data) {
            var category_data = $.parseJSON(data);

            if(category_data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
            //$('#form_header_text').text("ক্যাটাগরির তথ্য পরিবর্তন করুন");
            $('#form_header_text').text("<?php echo $this->lang->line('edit_category'); ?>");
            $('input[name = categories_name_edit]').val(category_data.category_info_by_id.categories_name);
            $('input[name = categories_description_edit]').val(category_data.category_info_by_id.categories_description);
            $('input[name = parent_categories_id_edit]').val(category_data.category_info_by_id.parent_categories_id);
            $('input[name = category_select_edit]').val(category_data.category_info_by_id.parent_name);
            $('#category_select_edit').html('<option value="'+category_data.category_info_by_id.parent_categories_id+'" data-subtext="'+category_data.category_info_by_id.parent_categories_id+'" selected="selected">'+category_data.category_info_by_id.parent_name+'</option>');
            $('#category_select_edit').trigger('change');
            $('#category_insert_div').slideUp('fast');
            $('#category_edit_div').slideDown('slow');
            $('input[name=categories_id]').val(category_id);

        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });


// FOR ADDING NEW CATEGORY
$('#category_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    var data_new = {};
    data.categories_name = $('input[name=categories_name]').val();
    data.categories_description = $('input[name=categories_description]').val();
    data.parent_categories_id = $('input[name=parent_categories_id]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    console.log(data);
    $.post('<?php echo site_url() ?>/category/save_category_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        console.log(data2);
        if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }
        else
        {
            data_new.categories_id = data2.data;
            data_new.categories_name = $('input[name=categories_name]').val();
            data_new.categories_description = $('input[name=categories_description]').val();
            data_new.parent_categories_id = ($('input[name=parent_categories_id]').val())== null ? '' : $('input[name=parent_categories_id]').val();
            category_only.push(data_new);
            $("#parent_category_tree").jstree('destroy');
            generate_tree_data_category(data_new);

            //$('#parent_category_tree').jstree('select_node', 'parent_'+data_new.categories_id);
            $('.form-group').removeClass('has-error');
            $('.help-block').hide();
            $('input[name= categories_name]').val('');
            $('input[name= categories_description]').val('');
            $('input[name= parent_categories_id]').val('');
            $('#category_select').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
        }
    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
});	


//FOR EDITING CATEGORY
$('#category_edit_form').submit(function(event) {
    event.preventDefault();
    var post_data={};
    post_data.categories_name_edit = $('input[name = categories_name_edit]').val();
    post_data.categories_description = $('input[name = categories_description_edit]').val();
    post_data.parent_categories_id = $('input[name = parent_categories_id_edit]').val();
    post_data.categories_id = $('input[name=categories_id]').val();
    post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title'); ?>");

    $.post('<?php echo site_url() ?>/category/update_category_info',post_data).done(function(data, textStatus, xhr) {

        var data2 = $.parseJSON(data);

        // if(data=="success")
        
        if(data2['success'])
        {
            $('#update_success').slideDown();
            setTimeout( function(){$('#update_success').slideUp()}, 3000 );
            
            $.each(category_only, function(index, val) {
                if(val.categories_id == post_data.categories_id)
                {
                    category_only[index].categories_name = post_data.categories_name_edit;
                    category_only[index].parent_categories_id = post_data.parent_categories_id;
                    category_only[index].categories_description = post_data.categories_description;
                    category_only[index].categories_id = post_data.categories_id;
                }
            });
            $("#parent_category_tree").jstree('destroy');
            generate_tree_data_category([]);
            $('#parent_category_tree').jstree('select_node', 'parent_'+post_data.categories_id);
            // $('#parent_category_tree #parent_'+post_data.categories_id).atrr('aria-expanded', 'true');
            $("#category_edit_div").hide();
            $("#category_insert_div").show();
            $('input[name= parent_categories_id]').val('');
            $('#category_insert_div').slideDown('slow');
            $('#category_edit_div').slideUp('fast');   
        }
        else if(!Object.keys(data2.error).length)
        {
            alert("Category updating failed");                
        }
        else
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });        
        }
    }).error(function() {

        alert("Connection error.");
    });    
});	
//FOR EDITING CATEGORY END

$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
    var categoriesId = $('input[name=categories_id]').val();
    var post_data ={
        'categories_id' : categoriesId,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/category/delete_category/'+categoriesId,post_data).done(function(data) {
        if(data == "No permission")
        {
            $('#not_permitted').slideDown();
            setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            $('#responsive_modal_delete').modal('hide');
        }
        else if(data = "success")
        {
            $('#delete_success').slideDown();
            setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
            $.each(category_only, function(index, val)
            {
                if(val.categories_id == $('input[name=categories_id]').val())
                {
                    category_only.splice(index,1);
                    return false;
                }
            });
            $("#parent_category_tree").jstree('destroy');
            generate_tree_data_category([]);
            $("#category_edit_div").hide();
            $("#category_insert_div").show();
            $('input[name= parent_categories_id]').val('');
            $('#responsive_modal_delete').modal('hide');
        }
        else
        {
            $('#delete_failure').slideDown();
            setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
        }
    }).error(function() {
        alert("An error has occurred. pLease try again");                
    });
});

</script>