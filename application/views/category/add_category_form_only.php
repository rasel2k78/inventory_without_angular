<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption left">
			<i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title_category'); ?>
        </div>
    </div>
    <div class="portlet-body form" id="category_insert_div">
        <!-- BEGIN FORM-->
        <?php $form_attrib = array('id' => 'category_form','class' => 'form-horizontal form-bordered');
        echo form_open('',  $form_attrib, '');?>

        <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('category_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                <div class="col-md-9">
                    <input type="text" id="search_key" class="form-control" name="categories_name" placeholder="<?php echo $this->lang->line('category_name'); ?>">
                    <div id="suggesstion-box"><ul  role="menu" class="list-group"></ul></div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('category_description'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="categories_description" placeholder="<?php echo $this->lang->line('category_description'); ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('main_category_name'); ?> 
                </label>
                <div class="col-md-6">
                    <!-- <select class="form-control" id="select_category" name="parent_categories_id" data-live-search="true">
                        <!-- <option>select one</option> 
                    </select> -->



                    <input type="text" autocomplete="off" name="category_child" id="category_select_child" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">

                    <input type="hidden" autocomplete="off" name="parent_categories_id" class="form-control">


                    <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result_child" style="position: absolute;z-index: 10;background-color: #fff;">
                    </table>


                </div>
            </div>
        </div>
        <div class="form-actions">
            <button data-dismiss="modal" id="cateory_cancel" class="btn dark btn-outline pull-right" style="margin-left: 8px" type="button"><?php echo $this->lang->line('cancle'); ?></button>
            <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?></button>
        </div>
        <?php echo form_close();?>
        <!-- END FORM-->
    </div>
</div>

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script type="text/javascript">

    $('#category_form').on('click', '#cateory_cancel', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('input[name= categories_name]').val('');
        $('input[name= categories_description]').val('');

    });



    $("#category_select_child").keyup(function(event) 
    {
        $("#category_select_result_child").show();
        $("#category_select_result_child").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select_child").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result_child").html(html);
            });
        }, 500);
    });

    $("#category_select_result_child").on('click', 'td', function(event) 
    {
        $('input[name="category_child"]').val($(this).html());
        $('input[name="parent_categories_id"]').val($(this).attr('data'));
        $("#category_select_result_child").hide();
    });



</script>