<?php
/**
 * Form Name:"VAT Form" 
 *
 * This is form for insert , edit and update vat.
 * 
 * @link   vat/index
 * @author shoaib <shofik.shoaib@gmail.com>
 */
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
	<div class="col-md-6">
		<div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
		<div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
		<div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
		<div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
		<div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>

		<div class="alert alert-success" id="active_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_active_success');?></div>
		<div class="alert alert-danger" id="active_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_active_failed');?></div>
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption left">
					<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
				</div>
			</div>
			<div class="portlet-body form" id="vat_insert_div">
				<!-- BEGIN FORM-->
				<?php $form_attrib = array('id' => 'vat_form','class' => 'form-horizontal form-bordered');
				echo form_open('',  $form_attrib, '');?>
				<!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_type_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="vat_name" placeholder="<?php echo $this->lang->line('left_expense_type_name_placeholder');?>">
							<span class="help-block"></span></div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo $this->lang->line('percentage');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="vat_percentage" placeholder="<?php echo $this->lang->line('vat_percentage');?>">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="vat_details" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
							</div>
						</div>
					</div>
					<div class="form-actions">
						<button type="reset" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
						<button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
					</div>
					<?php echo form_close();?>
					<!-- END FORM-->
				</div>
				<div class="portlet-body form" id="vat_edit_div" style="display:none">
					<!-- BEGIN FORM-->
					<?php
					$form_attrib = array('id' => 'vat_edit_form','class' => 'form-horizontal form-bordered');
					echo form_open('',  $form_attrib, '');
					?>
					<input type="text" class="form-control" name="vat_id" style="display:none">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_type_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="vat_name_edit" placeholder="<?php echo $this->lang->line('left_expense_type_name_placeholder');?>">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo $this->lang->line('percentage');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="vat_percentage_edit" placeholder="<?php echo $this->lang->line('vat_percentage');?>">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?></label>
							<div class="col-md-9">
								<input type="text" class="form-control" name="vat_details_edit" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
							</div>
						</div>
					</div>
					<div class="form-actions">
						<button type="button" class="btn default pull-right" id="vat_edit_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
						<button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
					</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<div class="caption right">
						<i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
								<th><?php echo $this->lang->line('right_expense_type');?></th>
								<th><?php echo $this->lang->line('right_percentage');?></th>
								<th><?php echo $this->lang->line('right_date');?></th>
								<th>
									<?php echo $this->lang->line('right_edit_delete');?>
								</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th><?php echo $this->lang->line('right_expense_type');?></th>
								<th><?php echo $this->lang->line('right_description');?></th>
								<th>
									<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control dont-search" name="from_create_date">
										<span class="input-group-addon">
											to
										</span>
										<input type="text" class="form-control dont-search" name="to_create_date">
									</div>
								</th>
								<th><?php echo $this->lang->line('right_edit_delete');?></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<?php echo $this->lang->line('alert_delete_details');?>
				</div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
	<button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<div id="responsive_modal_active" class="modal fade in animated shake" tabindex="-1" data-width="460">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"><?php echo $this->lang->line('alert_activate');?> <span id="selected_name_activated" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->lang->line('alert_activate_details');?>
			</div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
	<button type="button" id="activation_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->lang->line('alert_edit_details');?>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
		<button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
	</div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>-->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- <script src="<?php echo CURRENT_ASSET;?>assets/build/react.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/react-dom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/vat.js"></script>
<script type="text/babel" src="<?php echo CURRENT_ASSET;?>assets/components/vat.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
	/*loading tooltip*/
	jQuery(document).ready(function($) {
		$("[data-toggle='popover']").popover();

	});
	/*lading date picker*/
	jQuery(document).ready(function() {    
		$('.date-picker').datepicker();
		UIExtendedModals.init();
	});
	$('#sample_2 tfoot th').each(function (idx,elm){
		if (idx == 0 || idx == 1 || idx == 3) { 
			var title = $('#example tfoot th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
		}
	});
	var table_vat = $('#sample_2').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo site_url(); ?>/vat/all_vat_info_for_datatable/",
		"lengthMenu": [
		[10, 15, 20,30],
		[10, 15, 20,30] 
		],
		"pageLength": 10,
		"language": {
			"lengthMenu": " _MENU_ records",
			"paging": {
				"previous": "Prev",
				"next": "Next"
			}
		},
		"columnDefs": [{  
			'orderable': true,
			'targets': [0]
		}, {
			"searchable": true,
			"targets": [0]
		}],
		"order": [
		[0, "desc"]
		],
		/*for display tooltip message*/
		"drawCallback": function( settings ) {
			$("[data-toggle='popover']").popover();
		}
		/* tooltip message ends*/
	});
	/*data table customize search starts*/
	table_vat.columns().every( function () {
		var that = this;
		$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
			if (!($(this).hasClass('dont-search'))) {
				that
				.search( this.value )
				.draw();
			}
		});
		$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
			var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
			var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
			var final_string = '';
			if(date_from !='' || date_to != ''){
				if (date_from!='' && date_to=='') {
					final_string = date_from+'_0';
				}
				if (date_to!='' && date_from=='') {
					final_string = '0_'+date_to;
				}
				if (date_from!='' && date_to!='') {
					final_string = date_from+'_'+date_to;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
	});
	/*datatable customize search ends*/
	$('table').on('click', '.edit_vat',function(event) {
		event.preventDefault();
		var vat_id = $(this).attr('id').split('_')[1];
		$('#current_vat_id').remove();
		$.get('<?php echo site_url();?>/vat/get_vat_info/'+vat_id).done(function(data) {
			var vat_data = $.parseJSON(data);
			if(vat_data =="No Permisssion"){
				$('#access_failure').slideDown();
				setTimeout( function(){$('#access_failure').slideUp()}, 3000 );                
			}
			else {

				$('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
				$('input[name = vat_name_edit]').val(vat_data.vat_info_by_id.vat_name);
				$('input[name = vat_percentage_edit]').val(vat_data.vat_info_by_id.vat_percentage);
				$('input[name = vat_details_edit]').val(vat_data.vat_info_by_id.vat_details);
				$('#vat_insert_div').slideUp('fast');
				$('#vat_edit_div').slideDown('slow');
				$('input[name=vat_id]').val(vat_id);
			}
		}).error(function() {
			alert("An error has occured. pLease try again");
		});
	});
	$('#vat_edit_form').submit(function(event) {
		event.preventDefault();
		$('#responsive_modal_edit').modal('show');

	});
	$('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
		event.preventDefault();
		var post_data={};
		post_data.vat_name_edit = $('input[name = vat_name_edit]').val();
		post_data.vat_details_edit = $('input[name = vat_details_edit]').val();
		post_data.vat_percentage_edit = $('input[name = vat_percentage_edit]').val();
		post_data.vat_id = $('input[name=vat_id]').val();
		post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
		$.post('<?php echo site_url() ?>/vat/update_vat_info',post_data).done(function(data, textStatus, xhr) {
			var data2= $.parseJSON(data);
			if(data2['success'])
			{
				$('#responsive_modal_edit').modal('hide');
				$('#update_success').slideDown();
				setTimeout( function(){$('#update_success').slideUp()}, 3000 );
				var row_data = table_vat.row( $('#edit_'+post_data.vat_id).closest('tr')[0] ).data();
				row_data[0] = post_data.vat_name_edit;
				table_vat.row( $('#edit_'+post_data.vat_id).closest('tr')[0] ).data(row_data).draw();
				$('#vat_insert_div').slideDown('fast');
				$('#vat_edit_div').slideUp('slow');
				$('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
				$('.help-block').hide();
			}
			else if(!Object.keys(data2.error).length)
			{
				alert("<?php echo $this->lang->line('alert_edit_failed');?>");                
			}
			else
			{
				$('#responsive_modal_edit').modal('hide');
				$.each(data2.error, function(index, val) {
					$('input[name='+index+']').parent().parent().addClass('has-error');
					$('input[name='+index+']').parent().parent().find('.help-block').text(val);
					$('.help-block').show();
				});            
			}
		}).error(function() {

			alert("<?php echo $this->lang->line('alert_edit_failed');?>");
		});
	});
	/* Delete Expenditure Info  Starts*/
	var vat_id = '' ;
	var selected_name = '';
	$('table').on('click', '.delete_vat',function(event) {
		event.preventDefault();
		vat_id = $(this).attr('id').split('_')[1];
		selected_name = $(this).parent().parent().find('td:first').text();
		$('#selected_name').html(selected_name);
	});
	$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
		event.preventDefault();
    // var vat_id = $(this).attr('id').split('_')[1];
    var post_data ={
    	'vat_id' : vat_id,
    	'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/vat/delete_vat/'+vat_id,post_data).done(function(data) {
    	if(data == "No Permisssion"){
    		$('#responsive_modal_delete').modal('hide');
    		$('#access_failure').slideDown();
    		setTimeout( function(){$('#access_failure').slideUp()}, 3000 );

    	}
    	else if(data == "success")
    	{
    		$('#responsive_modal_delete').modal('hide');
    		$('#delete_success').slideDown();
    		setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
    		table_vat.row($('#delete_'+vat_id).parents('tr')).remove().draw();
    	}
    	else
    	{
    		$('#delete_failure').slideDown();
    		setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
    	}

    }).error(function() {
    	alert("An error has occured. pLease try again");                
    });
});
// ajax form submit starts....
$('#vat_form').submit(function(event) {
	event.preventDefault();
	var data = {};
	data.vat_name = $('input[name=vat_name]').val();
	data.vat_details = $('input[name=vat_details]').val();
	data.vat_percentage = $('input[name=vat_percentage]').val();
	data.csrf_test_name = $('input[name=csrf_test_name]').val();
	$.post('<?php echo site_url() ?>/vat/save_vat_info',data).done(function(data1, textStatus, xhr) {
		var data2 = $.parseJSON(data1);
		if(data2 =="No Permisssion"){
			$('#access_failure').slideDown();
			setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
		}
		else if(data2.success==0)
		{
			$.each(data2.error, function(index, val) {
				$('input[name='+index+']').parent().parent().addClass('has-error');
				$('input[name='+index+']').parent().parent().find('.help-block').text(val);
				$('.help-block').show();
			});
		}
		else
		{
			$('.form-group.has-error').removeClass('has-error');
			$('.help-block').hide();

			$('input[name= vat_name]').val('');
			$('input[name= vat_details]').val('');
			$('input[name= vat_percentage]').val('');

			$('#insert_success').slideDown();
			setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
			table_vat.row.add({
				"0":       '',
				"1":'',
				"2":  '',
				"3": '', 
			}).draw();
		}

	}).error(function() {

		$('#insert_failure').slideDown();
		setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
	});    
});
// ajax form submit end.......
$('#vat_edit_form').on('click', '#vat_edit_cancel', function(event) {
	event.preventDefault();
	$('#vat_insert_div').slideDown('slow');
	$('#vat_edit_div').slideUp('fast');
	$('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
	/* Act on the event */
});
$('table').on('click', '.active_vat', function(event) {
	event.preventDefault();
	vat_id = $(this).attr('id').split('_')[1];
	selected_name = $(this).parent().parent().find('td:first').text();
	$('#selected_name_activated').html(selected_name);
});
$('#responsive_modal_active').on('click', '#activation_confirmation',function(event) {
	event.preventDefault();
    // var vat_id = $(this).attr('id').split('_')[1];
    var post_data ={
    	'vat_id' : vat_id,
    	'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/vat/activate_vat/'+vat_id,post_data).done(function(data)
    {
    	if(data == "No Permission"){
    		$('#responsive_modal_active').modal('hide');
    		$('#access_failure').slideDown();
    		setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
    	}
    	else if(data == "success")
    	{
    		$('#responsive_modal_active').modal('hide');
    		$('#active_success').slideDown();
    		setTimeout( function(){$('#active_success').slideUp()}, 3000 );
    table_vat.ajax.reload(null, false); // user paging is not reset on reload
            // location.reload();
            // table.row( $('#payout_'+data.vat_id).closest('tr')[0]).data(row_data).draw()
        }
        else if(data == "failed")
        {
        	$('#responsive_modal_active').modal('hide');

        	$('#active_failure').slideDown();
        	setTimeout( function(){$('#active_failure').slideUp()}, 3000 );        
        }
    }).error(function() {
    	alert("<?php echo $this->lang->line('default_error');?>");                
    });
});
</script>