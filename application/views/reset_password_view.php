<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>MAX Inventory Solution</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url()?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url()?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url()?>assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo base_url()?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="index.html">
            <img src="<?php echo base_url()?>assets/admin/layout2/img/logo-big.png" alt=""/>
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">

    <?php 
    $hidden = array('token' => $token);

    $attributes = array(
    'id' => 'reset_form',
    'class' =>'login-form'
    );

    echo form_open('Login/resetPassword', $attributes, $hidden);
    ?>


        <div class="form-group">
            <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
            <div class="col-lg-8">
                <input type="password" class="form-control" id="password" name="password"><br>
            </div>

        </div>

        <div class="form-group">

            <label for="email" class="col-lg-4 control-label">Confirm Password <span class="require">*</span></label>
            <div  class="col-lg-8">
                <input type="password" class="form-control" id="con_pass" name="con_pass"><br>
            </div>
        </div>


        <div align="center" class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
                <button type="submit" id="change_submit" class="btn btn-primary">RESET</button>
            </div>
        </div>
    <?php echo form_close(); ?>


    </div>
    <div class="copyright">
        2016 © MAX Group
    </div>

    <script src="<?php echo base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url()?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url()?>assets/global/scripts/metronic.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <script type="text/javascript">


    // $('#reset_form').submit(function (event) {

    //     event.preventDefault();
    //     dataString = $("#reset_form").serialize();
    //     $.ajax({
    //         type:"POST",
    //         url:"<?php echo site_url();?>/Login/resetPassword",
    //         data:dataString,

    //         success:function (data) {

    //             if(data == "Password didn't match"){
    //                 alert("Password didn't match");    
    //             }
    //             else if(data == "Password didn't changed"){
    //                 $('#pass_update_failure').slideDown();
    //                 setTimeout( function(){$('#pass_update_failure').slideUp()}, 1500 );
    //             }
    //             else if(data == "New password didn't match"){
    //                 $('#new_pass_not_matched').slideDown();
    //                 setTimeout( function(){$('#new_pass_not_matched').slideUp()}, 1500 );
    //             }
    //             else if(data == "Old paasword didn't match"){
    //                 $('#old_pass_not_matched').slideDown();
    //                 setTimeout( function(){$('#old_pass_not_matched').slideUp()}, 1500 );
    //             }
    //             else if(data == "Password field can't be empty"){
    //                 $('#empty_pass').slideDown();
    //                 setTimeout( function(){$('#empty_pass').slideUp()}, 1500 );
    //             }
    //         }
    //     });
    // });



</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>







