				<!-- BEGIN DASHBOARD STATS -->
				<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
                <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
                <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
                <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
                <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

                <div class="row">
                    <?php if($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager") {
                        ?>


                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-light red-soft" href="javascript:;">
                                <div class="visual">
                                    <i class=""></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $this->lang->line('money_sign');?> 
                                    </div>
                                    <div class="desc">
                                        <?php echo $this->lang->line('current_cashbox_amount'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <?php  } ?>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
                                <div class="visual">
                                    <i class=""></i>
                                </div>
                                <div class="details">
                                    <div class="number" >
                                        <?php echo $this->lang->line('money_sign');?> 
                                    </div>
                                    <div class="desc">
                                        <?php echo $this->lang->line('today_total_sell'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager") : ?>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-light blue-soft" href="javascript:;">
                                    <div class="visual">
                                        <i class=""></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <?php echo $this->lang->line('money_sign');?> 
                                        </div>
                                        <div class="desc">
                                            <?php echo $this->lang->line('today_total_buy'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>


                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
                                    <div class="visual">
                                        <i class=""></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <?php echo $this->lang->line('money_sign');?>
                                        </div>
                                        <div class="desc">
                                            <?php echo $this->lang->line('today_total_expense'); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
<!--                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a style="background: #3c3b56" class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
                                    <div class="visual">
                                        <i class=""></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <?php echo $this->lang->line('money_sign');?>
                                        </div>
                                        <div class="desc">
                                            Total Transfer Receives
                                        </div>
                                    </div>
                                </a>
                            </div> -->

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a  style="background: #3c3b56" class="transfer_details dashboard-stat dashboard-stat-light sky-soft" href="javascript:;">
                                    <div class="visual">
                                        <i class=""></i>
                                    </div>
                                    <div class="details">
                                        <div class="transfer number" >
                                            <?php echo $this->lang->line('money_sign');?> 
                                        </div>
                                        <div class="desc">
                                          Total Transfer Send 
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a id="transfer_send" style="background:#474648" class="transfer_send dashboard-stat dashboard-stat-light redd-soft" href="javascript:;">
                                <div class="visual">
                                    <i class=""></i>
                                </div>
                                <div class="details">
                                    <div class="send number">
                                        <?php echo $this->lang->line('money_sign');?>
                                    </div>
                                    <div class="desc">
                                       Total Transfer Receives
                                   </div>
                               </div>
                           </a>
                       </div>
                   <?php endif ?>
               </div>
               <!-- END DASHBOARD STATS -->
               <div class="row">

                <div class="col-md-6">
                    <div class="portlet box green-haze">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list"></i><?php echo $this->lang->line('today_buy_list'); ?>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_4">
                                <thead>
                                    <tr>
                                        <th>
                                            <?php echo $this->lang->line('buy_voucher_no'); ?>
                                        </th>
                                        <th><?php echo $this->lang->line('grand_total'); ?></th>
                                        <th><?php echo $this->lang->line('discount'); ?></th>
                                        <th><?php echo $this->lang->line('paid'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager") : ?>
                    <div class="col-md-6">
                        <div class="portlet box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-list"></i><?php echo $this->lang->line('today_sell_list');?>
                                </div>

                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_3">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?php echo $this->lang->line('sell_voucher_no');?>
                                            </th>
                                            <th><?php echo $this->lang->line('grand_total'); ?></th>
                                            <th><?php echo $this->lang->line('discount'); ?></th>
                                            <th><?php echo $this->lang->line('paid'); ?></th>
                                            <th><?php echo $this->lang->line('due'); ?></th>
                                            <th><?php echo $this->lang->line('change'); ?></th>
                                            <!-- <th>Date</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                <?php endif ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-list"></i><?php echo $this->lang->line('all_shortage_list'); ?>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>
                                            <?php echo $this->lang->line('item_name'); ?>
                                        </th>
                                        <th>
                                            <?php echo $this->lang->line('quantity'); ?>
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    


                <!--     <div class="row"> -->
                <?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager") : ?>
                    <div class="col-md-6">
                        <div class="portlet box green-haze">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-list"></i><?php echo $this->lang->line('staff_wise_daily_sell'); ?>
                                </div>

                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_5">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?php echo $this->lang->line('staff_name'); ?>
                                            </th>
                                            <th><?php echo $this->lang->line('todays_sell'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                <?php endif ?>
                <!-- </div> -->
            </div>
        <!--             <?php echo date("d-m-Y")."<br>"; ?>
        <?php echo date("jS\, F Y ");?> -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
        <!-- date picker js starts -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- date picker js ends -->

        <!-- Boostrap modal starts-->
        <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
        <!-- Boostrap modal end -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

        <script type="text/javascript">

            var user_role= <?php echo json_encode($this->session->userdata('user_role')); ?>;
            $.get('<?php echo site_url(); ?>/Dashboard/curr_cash_amt_todays_buy_and_expense/', function(data) {
                var collected_data = $.parseJSON(data);
                /*Today's staffwise sell amount or total sell amount for owner*/
                if(collected_data.todays_sell == null){
                    var html = $('.dashboard-stat.dashboard-stat-light.green-soft .number').html().trim() + '' + 0 ;
                    $('.dashboard-stat.dashboard-stat-light.green-soft .number').html(html);
                }
                else{
                    var html = $('.dashboard-stat.dashboard-stat-light.green-soft .number').html().trim() + '' + collected_data.todays_sell ;
                    $('.dashboard-stat.dashboard-stat-light.green-soft .number').html(html);
                }
                if(collected_data.total_transfer == null){
                    var html = $('.transfer_details.dashboard-stat.dashboard-stat-light.sky-soft .transfer.number').html().trim() + '' + 0 ;
                    $('.transfer_details.dashboard-stat.dashboard-stat-light.sky-soft .transfer.number').html(html);
                }
                else{
                    var html = $('.transfer_details.dashboard-stat.dashboard-stat-light.sky-soft .transfer.number').html().trim() + '' + collected_data.total_transfer;
                    $('.transfer_details.dashboard-stat.dashboard-stat-light.sky-soft .transfer.number').html(html);
                }

                if(collected_data.total_receive == null){
                    var html = $('.transfer_send.dashboard-stat.dashboard-stat-light.redd-soft .send.number').html().trim() + '' + 0 ;
                    $('.transfer_send.dashboard-stat.dashboard-stat-light.redd-soft .send.number').html(html);
                }
                else{
                    var html = $('.transfer_send.dashboard-stat.dashboard-stat-light.redd-soft .send.number').html().trim() + '' + collected_data.total_receive ;
                    $('.transfer_send.dashboard-stat.dashboard-stat-light.redd-soft .send.number').html(html);
                }


                // if(collected_data.total_transfer == null){
                //     var html = $('#transfer_send .number').html().trim() + '' + 0 ;
                //     $('#transfer_send .number').html(html);
                // }
                // else{
                //     var html = $('#transfer_send .number').html().trim() + '' + collected_data.total_transfer;
                //     $('#transfer_send .number').html(html);
                // }


                /*Current Cashbox Amount*/
                if(collected_data.current_cashbox_amount == null){
                    var html = $('.dashboard-stat.dashboard-stat-light.red-soft .number').html().trim() + '' + 0 ;
                    $('.dashboard-stat.dashboard-stat-light.red-soft .number').html(html);
                }
                else{
                    var html = $('.dashboard-stat.dashboard-stat-light.red-soft .number').html().trim() + '' + collected_data.current_cashbox_amount ;
                    $('.dashboard-stat.dashboard-stat-light.red-soft .number').html(html);
                }
                /*Todays Total Buy Amount*/
                if(collected_data.todays_buy == null){
                    var html = $('.dashboard-stat.dashboard-stat-light.blue-soft .number').html().trim() + '' + 0 ;
                    $('.dashboard-stat.dashboard-stat-light.blue-soft .number').html(html);
                }
                else{
                    var html = $('.dashboard-stat.dashboard-stat-light.blue-soft .number').html().trim() + '' + collected_data.todays_buy ;
                    $('.dashboard-stat.dashboard-stat-light.blue-soft .number').html(html);
                }
                /*Todays Total Expense Amount*/
                if(collected_data.todays_expense == null){
                    var html = $('.dashboard-stat.dashboard-stat-light.purple-soft .number').html().trim() + '' + 0 ;
                    $('.dashboard-stat.dashboard-stat-light.purple-soft .number').html(html);
                }
                else{
                    var html = $('.dashboard-stat.dashboard-stat-light.purple-soft .number').html().trim() + '' + collected_data.todays_expense ;
                    $('.dashboard-stat.dashboard-stat-light.purple-soft .number').html(html);
                }

            });

                var table_item = $('#sample_2').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?php echo site_url(); ?>/Dashboard/shortage_item_info_for_dashboard/",
                    "lengthMenu": [
                    [10, 15, 20,30],
                    [10, 15, 20,30] 
                    ],
                    "pageLength": 10,
                    "language": {
                        "lengthMenu": " _MENU_ records",
                        "paging": {
                            "previous": "Prev",
                            "next": "Next"
                        }
                    },
                    "columnDefs": [{  
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                    "order": [
                    [0, "desc"]
                    ]
                });

                var table_buy_list_all= $('#sample_4').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": "<?php echo site_url(); ?>/Dashboard/all_todays_buy_list_info_for_datatable/",
                    "lengthMenu": [
                    [10, 15, 20,30],
                    [10, 15, 20,30] 
                    ],
                    "pageLength": 10,
                    "language": {
                        "lengthMenu": " _MENU_ records",
                        "paging": {
                            "previous": "Prev",
                            "next": "Next"
                        }
                    },
                    "columnDefs": [{  
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                    "order": [
                    [0, "desc"]
                    ]
                });    


                jQuery(document).ready(function($) {
                    if (user_role != "staff") {


                        var table_sell_list_all = $('#sample_3').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": "<?php echo site_url(); ?>/Dashboard/all_todays_sell_list_info_for_datatable/",
                            "lengthMenu": [
                            [10, 15, 20,30],
                            [10, 15, 20,30] 
                            ],
                            "pageLength": 10,
                            "language": {
                                "lengthMenu": " _MENU_ records",
                                "paging": {
                                    "previous": "Prev",
                                    "next": "Next"
                                }
                            },
                            "columnDefs": [{  
                                'orderable': true,
                                'targets': [0]
                            }, {
                                "searchable": true,
                                "targets": [0]
                            }],
                            "order": [
                            [0, "desc"]
                            ]
                        });    


                        var staff_daily_sell_list_all= $('#sample_5').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ajax": "<?php echo site_url(); ?>/Dashboard/todays_total_sell_by_staff/",
                            "lengthMenu": [
                            [10, 15, 20,30],
                            [10, 15, 20,30] 
                            ],
                            "pageLength": 10,
                            "language": {
                                "lengthMenu": " _MENU_ records",
                                "paging": {
                                    "previous": "Prev",
                                    "next": "Next"
                                }
                            },
                            "columnDefs": [{  
                                'orderable': true,
                                'targets': [0]
                            }, {
                                "searchable": true,
                                "targets": [0]
                            }],
                            "order": [
                            [0, "desc"]
                            ]
                        });    
                    }
                });


            </script>