<!DOCTYPE html>
<html>
<head>
	<title>Generated Barcode</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<style type="text/css">
		html
		{
			background-color: #FFFFFF ; 
			margin: 0px;  /* this affects the margin on the html before sending to printer */
		}
		body
		{
			margin-top: 12px;
			margin-left: 0px;
			margin-right: 0px;
			padding: 0px;
		}
		table{
			margin: auto;
		}
		.barcode-box{
			overflow: hidden !important;
			width: 46mm !important;
			height: 21mm !important;
			padding: 1mm 2mm !important;
			display: inline-block !important;
		}
		.barcode-top{
			text-align: center;
			font-size: 9px;
			word-wrap: break-word;
			overflow: hidden;
			height: 19px;
		}
		.barcode-middle{
			padding: 0px 30px;
		}
		.barcode-bottom{
			font-size: 10px;
			text-align: center;
		}
		td:nth-child(odd) {
			
		}
		td:nth-child(even) {
			
		}
		tr:nth-child(0) > td{
			margin-top: 0px;
		}
		tr:nth-child(1) > td{
			margin-top: 1px;
		}
		tr:nth-child(2) > td{
			margin-top: 2px;
		}
		tr:nth-child(3) > td{
			margin-top: 3px;
		}
		tr:nth-child(4) > td{
			margin-top: 4px;
		}
		tr:nth-child(5) > td{
			margin-top: 4px;
		}
		tr:nth-child(6) > td{
			margin-top: 4px;
		}
		tr:nth-child(7) > td{
			margin-top: 4px;
		}
		tr:nth-child(8) > td{
			margin-top: 4px;
		}
		/*tr:last-child > td{
			margin-top: 4px;
		}
		*/
		@media print {
			body
			{
				/*margin-top: 12px;*/
				margin-left: 0px;
				margin-right: 0px;
				padding: 0px;
			}
			table{
				margin: auto;
			}
			/*.barcode-box:last-child{
				width: 50mm !important;
			}*/
			.control-group {
				display: none !important;
			}
		}
	</style>

</head>
<body >

	<table id="bar_code_id" align="center" border='0'>
		<tr>
			<?php
			for ($i=0; $i <$print_quantity ; $i++)
			{ 
				if ($i%4==0 && $i!=0) {    
					echo "</tr><tr>";
				}
				echo "<td id='bcTarget".$i."' class='barcode-box'>";
				echo '<div class="barcode-top">WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW</div>'; 
				echo '<div class="barcode-middle"><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 10px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 2px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 4px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 2px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 4px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 2px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 4px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 2px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 4px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 2px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 4px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 3px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 3px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 3px"></div><div style="float: left; font-size: 0px; width:0; border-left: 3px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 1px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 1px"></div><div style="float: left; font-size: 0px; width:0; border-left: 2px solid #000000; height: 30px;"></div><div style="float: left; font-size: 0px; background-color: #FFFFFF; height: 30px; width: 10px"></div><div style="clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;">aaaa</div>
			</div>'; 
			echo '<div class="barcode-bottom">Price: 100000.00 Taka</div>'; 
			echo '</td>'; 

		}
		?>
	</tr>
	<!-- <button>Print Barcode</button> -->
</table>

<br>
<br>
<div align="center">
	<button  class="btn btn-success control-group" onclick="history.go(-1);">Back</button>
</div>



<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var barcode1 = "<?php echo $barcode; ?>";
	var print_quantity = <?php echo $print_quantity ?>;
	var itemName = '<?php echo str_replace('\'', '', $items_name)?>';
	var itemPrice = "<?php echo $price?>";
	for (var i = 0; i < print_quantity; i++) 
	{
		$("#bcTarget"+i).find('.barcode-middle').barcode(barcode1, "code128",{barWidth:1, barHeight:30}).css('padding', '0px 30px');    
    	// $("#bcTarget"+i).css('padding', '20px');
    	$("#bcTarget"+i).find('.barcode-top').html(itemName);
    	$("#bcTarget"+i).find('.barcode-bottom').html(itemPrice);

    };    

    /*
td {
    overflow: hidden;
    width: 54mm !important;
    height: 24mm !important;
    padding: 0px !important;
    display: inline-block !important;
}
*/

// window.print();

//    $( document ).ready(function() {
//     window.print();
// });
</script>


</body>
</html>