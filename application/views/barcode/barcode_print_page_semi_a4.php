<!DOCTYPE html>
<html>
<head>
	<!-- <title>Generated Barcode</title> -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" href="<?php echo CURRENT_ASSET;?>assets/bootstrap/css/bootstrap.min.css">
	<style type="text/css" media="print">
		@page 
		{
			size:  auto;   /* auto is the initial value */
			margin: 0mm;  /* this affects the margin in the printer settings */
		}
		html
		{
			background-color: #FFFFFF ; 
			margin: 0px;  /* this affects the margin on the html before sending to printer */
		}
		body
		{
			border: solid 1px white ;
			/*margin: 10mm 15mm 10mm 15mm; */
		}

		@media print {
			.control-group {
				display: none !important;
			}
		}
	</style>
</head>
<body >
	<table id="bar_code_id"  align="center" border='0' width="100%">
		<tr >
			<?php
			for ($i=0; $i <$print_quantity ; $i++)
			{ 
				if ($i%4==0 && $i!=0) {    
					echo "</tr><tr>";
				}
				echo "<td align='center' style='padding-top:2px'  >";
				echo "<div id='bcTarget".$i."' ></div></td>";
			}
			?>
		</tr>
		<!-- <button>Print Barcode</button> -->
	</table>
	<br>
	<br>
	<div align="center">
		<button  class="btn btn-success control-group" onclick="history.go(-1);">Back</button>
	</div>

</body>
</html>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>
<script type="text/javascript">
	var barcode1 = "<?php echo $barcode; ?>";
    // var barcode1 = "102030405060";
    var print_quantity = <?php echo $print_quantity ?>;
    for (var i = 0; i < print_quantity; i++) 
    {
    	$("#bcTarget"+i).barcode(barcode1, "code128",{barWidth:1, barHeight:20});    
    	$("#bcTarget"+i).css({'margin:auto;padding-top':'0px','margin-top':'2px', 'padding-bottom':'0px','word-wrap': 'break-word'});
    	$("#bcTarget"+i).prepend('<div style="clear:both; text-align:center; width: 80%; background-color: #FFFFFF; color: #000000;  font-size: 10px; font-family:consolas; margin-top: 5px;"><?php echo str_replace('\'', '',str_pad($items_name,50, '_', STR_PAD_BOTH))?><?php echo $price?></div>');
    };    
    window.print();
</script>