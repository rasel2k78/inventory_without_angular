	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption left">
				<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('barcode_header')?></span>
            </div>
        </div>
        <div class="portlet-body form" id="barcode_quantity_div">
            <!-- BEGIN FORM-->
            <?php $form_attrib = array('id' => 'barcode_quantity_form','class' => 'form-horizontal form-bordered');
            echo form_open('',  $form_attrib, '');?>

            <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

            <div class="form-body">            
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('barcode')?></label>
                    <div class="col-md-9">

                        <input type="text"  class="form-control" name="unique_id" readonly="true">


                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('barcode_quantity')?></label>
                    <div class="col-md-9">
                        <input type="number" min="1" step="1" class="form-control" name="print_quantity" placeholder="<?php echo $this->lang->line('barcode_quantity')?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Page Type</label>
                    <div class="radio-list">
                        <label class="radio-inline">
                            <input type="radio" name="page_type" id="page_4x10" value="4x10">4x10
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="page_type" id="page_5x13" value="5x13">5x13
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="page_type" id="page_plain" value="plain">Plain
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Company Name</label>
                    <div class="radio-list">
                        <label class="radio-inline">
                            <input type="radio" name="add_company" value="Yes" checked="checked">Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="add_company" value="No" >No
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button data-dismiss="modal" class="btn dark btn-outline pull-right" style="margin-left: 8px" type="button"><?php echo $this->lang->line('cancle_print')?></button>
                <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('print_barcode')?></button>
            </div>
            <?php echo form_close();?>
            <!-- END FORM-->
        </div>
    </div>

    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->