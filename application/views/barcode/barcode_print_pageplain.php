<!DOCTYPE html>
<html>
<head>
	<title>Generated Barcode</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="<?php echo CURRENT_ASSET;?>assets/bootstrap/css/bootstrap.min.css">
    <style type="text/css" media="print">
      @page 
      {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    html
    {
        background-color: #FFFFFF ; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }
    body
    {
        border: solid 1px white ;
        margin: 10mm 15mm 10mm 15mm; 
    }
    
    @media print {
       .control-group {
        display: none !important;
    }
}
</style>

</head>
<body >

    <table id="bar_code_id" align="center" border='0'>
        <tr>
            <?php
            for ($i=0; $i <$print_quantity ; $i++)
            { 
                if ($i%4==0 && $i!=0) {    
                    echo "</tr><tr>";
                }
                echo "<td align='center' id='bcTarget".$i."'>";
                echo '<div style="clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;">sdf</div></td>'; 

            }
            ?>
        </tr>
        <!-- <button>Print Barcode</button> -->
    </table>

    <br>
    <br>
    <div align="center">
        <button  class="btn btn-success control-group" onclick="history.go(-1);">Back</button>
    </div>

    
</body>
</html>

<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var barcode1 = "<?php echo $barcode; ?>";
    // var barcode1 = "102030405060";
    var print_quantity = <?php echo $print_quantity ?>;

    for (var i = 0; i < print_quantity; i++) 
    {
        $("#bcTarget"+i).barcode(barcode1, "code128",{barWidth:1, barHeight:44});    
        $("#bcTarget"+i).css('padding', '20px');
        $("#bcTarget"+i).prepend('<div style="clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;"><?php echo str_replace('\'', '', $items_name)?><br><b><?php echo $price?></b></div>');
    };    

    window.print();

</script>

