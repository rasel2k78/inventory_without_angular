<!DOCTYPE html>
<html>
<head>
	<title>Generated Barcode</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="<?php echo CURRENT_ASSET;?>assets/bootstrap/css/bootstrap.min.css">
    <style type="text/css" media="print">
      @page 
      {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    html
    {
        background-color: #FFFFFF ; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }
    body
    {
        border: solid 1px white ;
        margin: 10mm 15mm 10mm 15mm; 
    }
    
    @media print {
     .control-group {
        display: none !important;
    }
}
</style>

</head>
<body >

    <table id="bar_code_id" align="center" border='0'>
        <tr>
            <?php
            for ($i=0; $i < count($sdata) ; $i++)
            { 
                if ($i%4==0 && $i!=0) {    
                    echo "</tr><tr>";
                }
                echo "<td align='center' id='bcTarget".$i."'>";
                echo '<div style="clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;">sdf</div></td>'; 

            }
            ?>
        </tr>
        <!-- <button>Print Barcode</button> -->
    </table>

    <br>
    <br>
    <div align="center">
        <button  class="btn btn-success control-group" onclick="history.go(-1);">Back</button>
    </div>

    
</body>
</html>

<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var finalData = <?php echo json_encode($sdata)?>;

    $.each(finalData, function(index, val) {
        $("#bcTarget"+index).barcode(val.barcode, "code128",{barWidth:1, barHeight:44});    
        $("#bcTarget"+index).css('padding', '20px');
        $("#bcTarget"+index).prepend("<div style='clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;'>"+val.items_name+ "<br><b>"+val.price+"</b></div>");
    }); 
    
    window.print();

</script>

