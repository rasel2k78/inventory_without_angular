<?php
/**
 * Form Name:"Sell Form" 
 *
 * This is form for selling items.
 * 
 * @link   Sell/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">

	#voucher_preview table td{font-weight: bold}
	.container { max-width: 630px; min-height: 900px; background: #fff; margin: 0 auto; }
	/* .main_body { overflow: hidden; width: 595px; height: 842px; margin: 0 auto; background: #eee;}  */
	.balnk_top { min-height: 150px; }
	.address_part { overflow: hidden; margin: 15px;} 
	.left_part { 
		float: left;
		width: 270px;
		background-image: linear-gradient(#000000, #000000), linear-gradient(#000000, #000000), linear-gradient(#000000, #000000), linear-gradient(#000000, #000000);
		background-repeat: no-repeat;
		background-size: 8px 3px;
		background-position: top left, top right, bottom left, bottom right;
		border: solid #000000;
		text-align: justify;
		border-width: 0 3px;
		display: inline-block;
		vertical-align: top;
		padding: 0 0 0 0;
	} 
	.left_part ul { margin: 0 auto; padding: 10px 0px 10px 20px;}
	.left_part ul li { list-style: none; color: #222; line-height: 30px; font-size: 15px; font-family: arial;}
	.right_part { float: right; width: 270px;}
	.right_part table tr td { border:1px solid #c1c1c1; padding: 8px 0 8px 8px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; } .back_col { width: 90px; background: #ddd;}
	.right_col { width: 160px;} .description { margin: 15px; overflow: hidden;} .description p { font-family: Arial, Helvetica, sans-serif}
	.fist_border { border:1px solid #000; float: left; width: 30px; padding: 5px; height: 330px;} 
	.second_border { height: 330px; border:1px solid #000; float: left;  width: 255px; padding: 5px;}
	.third_border { height: 330px; border:1px solid #000; float: left; width: 60px; padding: 5px;}
	.fourth_border { height: 330px; border:1px solid #000; float: left; width: 97px; padding: 5px;} 
	.fifth_border { height: 330px; border:1px solid #000; float: left; width: 98px; padding: 5px;}
	.description table tr td { font-family: Arial, Helvetica, sans-serif; font-size: 13px;}
	.description table { margin-top: 15px; margin-bottom: 5px;}
	.total_amount { float: right;} .total_amount ul { margin: 0; padding: 0;} 
	.total_amount ul li { list-style: none; display: inline; border-right: 1px solid #000; border-bottom: 1px solid; margin: 0; float: left; text-align: left; width: 109px;
		font-family: arial; font-size: 11px; padding: 10px 0 10px 0;}
		.total_amount ul li.last-child { border-right: none;} .bottom_text { margin: 15px;} .bottom_text { font-family: Arial, Helvetica, sans-serif; font-size: 14px;}

		.nmpd-grid {border: none; padding: 20px;}
		.nmpd-grid>tbody>tr>td {border: none;}
		#voucher_preview{font-size: 9px;}
		/* Some custom styling for Bootstrap */
		.qtyInput {display: block;
			width: 100%;
			padding: 6px 12px;
			color: #555;
			background-color: white;
			border: 1px solid #ccc;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		}
		/* Tutorial Class Starts*/
		.focus_tutorial_element{
			position: relative;
			z-index: 100000;
			background-color: #f1f3fa;
		}


		.tutorial_tooltip{
			height: 180px;
			width: 220px;
			position: fixed;
			z-index: 1000000;
			top: 100px;
			left: 100px;
			background: #fff;
		}

		.tutorial_tooltip_edge{
			height: 20px;
			width: 20px;
			position: absolute;
			top: 10px;
			left: -10px;
			background-color: #fff;
			-ms-transform: rotate(45deg);
			-webkit-transform: rotate(45deg);
			transform: rotate(45deg);
			z-index: -1;
		}

		.tutorial_tooltip_content{
			padding: 10px;
		}

		.lasttablebg table tbody tr td {
			/* border: 1px solid #ccc; */
			text-align: center;
			width: 231px;
		}
	/* table {    
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	} */
	.lasttablebg { float: right; overflow: hidden; }
	.signaturebg { width: 960px; overflow: hidden; }
	.signaturebg p { 
		border-top: 1px solid #ddd;
		width: 240px;
		padding-bottom: 0;
		line-height: 40px;
		font-size: 16px;
		font-family: arial;
		margin-top: 70px;
		text-align: center;
	}
	.thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
	.thankyousms p { margin: 15px 0 0 0; padding: 0; }
	.thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
	#invoice{border:1px solid cadetblue;padding: 20px;}



	@media print {
		body {
			/* zoom:80%;  */
			height: auto;
			font-size: 8px;
			width: auto;
			margin: 1.6cm;
			/* transform: scale(.5); */
		}


		.header, .hide { visibility: hidden }
		display: none;
		/* zoom:80%; */
		table {page-break-inside: avoid;}
	}



</style>
<style type="text/css" media="print">
	/* body {
		zoom:85%; or whatever percentage you need, play around with this number
	} */


	.header, .hide { visibility: hidden }
	display: none;
	table {page-break-inside: avoid;}
	a{visibility: hidden;}

</style>


<!--<script type="text/javascript">
	document.getElementById('footer').style.display = 'none';
</script> -->
<?php date_default_timezone_set("Asia/Dhaka");?>
<!-- <script src="<?php echo base_url()?>assets/typed.js" type="text/javascript"></script> -->
<style>

	.typed-cursor{
		opacity: 1;
		font-weight: 100;
		-webkit-animation: blink 1.7s infinite;
		-moz-animation: blink 0.7s infinite;
		-ms-animation: blink 0.7s infinite;
		-o-animation: blink 0.7s infinite;
		animation: blink 0.7s infinite;
	}
</style>
<div id="typed-strings" style="display:none">
	<span>This is test html.</span>
</div>
<div id="typed-value" style="display:none">
	<span>This is test html.</span>
</div>
<span id="typed" style="white-space:pre;"></span> 

<div class="row hidden-print">
	<div class="col-md-6">
		<select id='holdingInvoiceSelection' class='form-control' style="display: none;">
		</select>
		<script type="text/javascript">
			var allHoldingInvoices;
			var setHoldingInvoices = function() {
				allHoldingInvoices = localStorage.getItem('hodldingInvoices');
				if(allHoldingInvoices == null)
				{
					allHoldingInvoices = [];
				}
				else
				{
					allHoldingInvoices = JSON.parse(allHoldingInvoices);
				}

				var html = "<option value=''><?php echo $this->lang->line('select_hold_invoice'); ?></option>";

				$.each(allHoldingInvoices, function(index, val) {
					html += "<option value='"+index+"'>"+val.holding_sn+"</option>";
				});

				$('#holdingInvoiceSelection').html(html);
			}

			setHoldingInvoices();
		</script>
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-pencil-square-o"></i>Multiple Items Barcode Print
				</div>
			</div>
			<div class="portlet-body form">
				<?php
				$form_attrib = array('class' => 'form-horizontal form-bordered', 'autocomplete'=>'off');
				echo form_open('', $form_attrib); ?>
				<div class="form-body">
					<div class="form-group" style="display: none">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_sell_type'); ?>
						</label>

						<div class="col-md-9 input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_type_tooltip');?>"></i>
							<select class="form-control" id="sell_type_id" name="sell_type">

								<option value="retail"><?php echo $this->lang->line('left_sell_type_retail'); ?></option>
								<option value="wholesale"><?php echo $this->lang->line('left_sell_type_wholesale'); ?></option>
								<option value="Online">Online</option>

							</select>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_item_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
						</label>
						<div class="col-md-9">
							<input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('left_item_name_placeholder'); ?>" id="item_select" class="form-control">
							<input type="hidden" autocomplete="off" name="item_spec_set_id" id="spec_id_for_get" class="form-control">
							<table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
							</table>
						</div>
					</div>
					<br/>
					<div class="form-group" id="current_qty" style="display: none">
						<label class="control-label col-md-3"><?php echo $this->lang->line('current_quantity'); ?> </label>
						<div class="col-md-9 input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('current_quantity_tooltip');?>"></i>
							<input type="text" id="current_quantity" name="current_quantity_inventory" class="form-control" readonly="true">

						</div>

					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Barcode</label>
						<div class="col-md-9">
							<input type="text" readonly="true" id="barcode_number" name="barcode" class="form-control" >
							<span class="help-block" style="display:none">Barcode Not Found.</span>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_quantity'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9">
							<div class="input-group">
								<input type="number" min="1" step="1" pattern="^[0-9]" id="numpadButton1"  name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('left_quantity'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn1" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_quantity_err_msg'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_selling_price'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9">
							<div class="input-group">
								<input type="number"  class="form-control selling_price_and_numberpad" step="any" min="0"   name="selling_price" id="price" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_price'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn2" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_selling_price_err_msg'); ?></span>
						</div>
					</div>

					<div class="form-group" style="display: none">
						<div class="panel-group accordion" id="accordion3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">

											<?php echo $this->lang->line('left_extra_fields'); ?>
										</a>
									</h4>
								</div>
								<div id="collapse_3_1" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="extra_fields" >

											<div class="form-group">
												<label class="control-label col-md-3"><?php echo $this->lang->line('left_discount_type'); ?>
												</label>
												<div class="col-md-3 input-icon">
													<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_dicount_tt');?>"></i>
													<select class="form-control" id="dis_type" name="discount_type">
														<option value=""><?php echo $this->lang->line('select_one')?></option>
														<option value="amount"><?php echo $this->lang->line('left_discount_type_taka'); ?></option>
														<option value="percentage"><?php echo $this->lang->line('left_discount_type_percentage'); ?></option>
														<option value="free_quantity"><?php echo $this->lang->line('left_discount_type_quantity'); ?></option>
													</select>

												</div>
												<div class="col-md-6">
													<input type="number" min="0" step="any" name="discount_amount" id="dis_amt_id" class="form-control" placeholder="<?php echo $this->lang->line('left_discount_type_placeholder'); ?>">

												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3"><?php echo $this->lang->line('left_comments'); ?>
												</label>
												<div class="col-md-9">
													<input type="text" class="form-control" name="sell_comments"  placeholder="<?php echo $this->lang->line('left_comments_placeholder'); ?>">
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="form-actions">
					<button  id="cancle_item" type="button" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle_button'); ?></button>
					<button id="add_item" type="button" class="btn green  pull-right">Add To List</button>
				</div>
				<?php echo form_close(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div id="success_messages">
			<div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('customer_insert_success'); ?></div>
			<div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
			<div class="alert alert-success" id="sell_success" style="display:none" role="alert"><?php echo $this->lang->line('sell_success');?></div>
		</div>
		<div id="error_messages">

			<div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?></div>
			<div class="alert alert-danger" id="no_username_customer" style="display:none" role="alert"><?php echo $this->lang->line('no_username_customer'); ?></div>
			<div class="alert alert-danger" id="no_phone" style="display:none" role="alert"><?php echo $this->lang->line('no_phone'); ?></div>
			<div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
			<div class="alert alert-danger" id="no_permission_left" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>

			<div class="alert alert-danger" id="paid_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_failure');?></div>
			<div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
			<div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('price_amount_failure');?></div>
			<div class="alert alert-danger" id="vat_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('vat_amount_failure');?></div>
			<div class="alert alert-danger" id="vat_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('vat_not_numeric');?></div>
			<div class="alert alert-danger" id="dis_not_valid" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_valid');?></div>
			<div class="alert alert-danger" id="dis_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_numeric');?></div>
			<div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
			<div class="alert alert-danger" id="paid_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('paid_not_numeric');?></div>
			<div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_info_empty');?></div>
			<div class="alert alert-danger" id="no_card_voucher" style="display:none" role="alert"><?php echo $this->lang->line('no_card_voucher');?></div>
			<div class="alert alert-danger" id="card_voucher_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('card_voucher_not_numeric');?></div>
			<div class="alert alert-danger" id="select_card_only" style="display:none" role="alert"><?php echo $this->lang->line('select_card_only');?></div>
			<div class="alert alert-danger" id="card_amount_more_than_total" style="display:none" role="alert"><?php echo $this->lang->line('card_amount_more_than_total');?></div>

			<div class="alert alert-danger" id="select_customer_for_due_transaction" style="display:none" role="alert"><?php echo $this->lang->line('select_customer_for_due_transaction');?></div>
			<div class="alert alert-danger" id="total_calculation_mismatch" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch');?></div>

			<div class="alert alert-danger" id="total_calculation_mismatch_only_in_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_only_in_discount');?></div>
			<div class="alert alert-danger" id="total_calculation_mismatch_after_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_after_discount');?></div>
			<div class="alert alert-danger" id="item_quantity_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_quantity_empty');?></div>
			<div class="alert alert-danger" id="no_name" style="display:none" role="alert"><?php echo $this->lang->line('sales_rep_no_name');?></div>
			<div class="alert alert-danger" id="calculation_mismatch" style="display:none" role="alert"><?php echo $this->lang->line('default_calculation_erro');?></div>

			<div class="alert alert-danger" id="discount_is_greater_than_grand_total" style="display:none" role="alert"><?php echo $this->lang->line('discount_is_greater_than_grand_total');?></div>

		</div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green-haze" id="right_box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-list"></i>Items List For Barcode Print  
				</div>

			</div>
			<div class="portlet-body form">
				<form id="sell_form" class="form-horizontal form-bordered" action="" method="get" accept-charset="utf-8">
					<div class="form-body">
						<div class="form-group" style="display: none">
							<label class="control-label col-md-3"><?php echo $this->lang->line('right_sales_rep'); ?>
							</label>
							<div class="col-md-6 input-icon">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sales_rep_tt');?>"></i>
								<input type="text" autocomplete="off" name="sales_rep_name" placeholder="<?php echo $this->lang->line('right_sales_rep_placeholder'); ?>" id="sales_rep_select" class="form-control">
								<input type="hidden" autocomplete="off" name="sales_rep_id"  class="form-control">
								<table class="table table-condensed table-hover table-bordered clickable" id="sales_rep_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
								</table>
							</div>
							<div class=" col-md-3">
								<a href="#sales_rep_form_modal" title="<?php echo $this->lang->line('right_sales_rep_title'); ?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="form-group" style="display: none">
							<label class="control-label col-md-3"><?php echo $this->lang->line('right_customer');?>
							</label>
							<div class="col-md-6 input-icon">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('customer_tt');?>"></i>
								<input type="text" autocomplete="off" name="customers_name" placeholder="<?php echo $this->lang->line('right_customer_placeholder');?>" id="customers_select" class="form-control">
								<span class="help-block" id="customer_help_block" ></span>
								<input type="hidden" autocomplete="off" name="customers_id"  class="form-control">
								<table class="table table-condensed table-hover table-bordered clickable" id="customers_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
								</table>
							</div>
							<div class=" col-md-3 col-sm-4">
								<a data-target="#customer_form_modal"  title="<?php echo $this->lang->line('right_customer_title');?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="form-group" style="display: none">
							<label class="control-label col-md-3"><?php echo $this->lang->line('left_date');?> </label>
							<div class="col-md-3">
								<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="sell_date" placeholder="<?php echo $this->lang->line('left_date');?>" />
								<span class="help-block"></span>
							</div>
						</div> 
						<div class="form-group" style="display: none">
							<label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
							<div class="radio-list">
								<label class="radio-inline">
									<input type="radio" name="payment_type" id="options_cash" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="payment_type" id="options_card" value="card"><?php echo $this->lang->line('right_type_card'); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="payment_type" id="options_cash_card_both" value="cash_card_both"><?php echo $this->lang->line('right_type_both'); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" name="payment_type" id="options_cheque" value="cheque"><?php echo $this->lang->line('cheque'); ?>
								</label>
							</div>
						</div>
						<div class="form-group" style="display: none" id="ac_select_div">
							<div class="col-md-6 form-group">
								<label class="control-label col-md-5"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
								</label>
								<div class="col-md-7">
									<input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
									<input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
									<span class="help-block" id="bank_acc_help_block" ></span>
									<table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
									</table>
								</div>
							</div>
							<div class="col-md-6 form-group">
								<label class="control-label col-md-6">Charge</label>
								<div class="col-md-6">
									<input type="text"  class="form-control" name="bank_charge_percent" id="charge_percent_id">
								</div>
							</div>
						</div>
						<div class="form-group" style=" display:none" id="card_voucher_no">
							<label class="control-label col-md-3"><?php echo $this->lang->line('right_card_voucher_no'); ?></label>
							<div class="col-md-6">

								<input type="text" class="form-control" name="card_payment_voucher_no" id="card_pay_voucher_id" placeholder="<?php echo $this->lang->line('right_card_voucher_no'); ?>">
								<span class="help-block"></span>
							</div>
						</div>

						<div class="form-group" style=" display:none" id="card_amount_for_both">
							<label class="control-label col-md-3"><?php echo $this->lang->line('right_card_amount'); ?></label>
							<div class="col-md-6">

								<input type="number" min="0" step="any" class="form-control" name="card_amount" id="card_amount_id" placeholder="<?php echo $this->lang->line('right_card_amount'); ?>">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style=" display:none" id="cheque_num">
							<label class="control-label col-md-3"><?php echo $this->lang->line('chk_number');?></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="cheque_number" id="chk_num" placeholder="<?php echo $this->lang->line('chk_number');?>">
								<span class="help-block"></span>
							</div>
						</div>
						<table id="sell_table" class="table table-condensed">
							<thead>
								<tr>
									<th><?php echo $this->lang->line('right_item_name');?></th>
									<th>Barcode</th>
									<th><?php echo $this->lang->line('right_quantity');?></th>
									<th><?php echo $this->lang->line('right_sell_price');?></th>
									<th style="display: none"><?php echo $this->lang->line('right_sub_total');?></th>
									<th style="display: none"><?php echo $this->lang->line('right_discount_type');?></th>
									<th><?php echo $this->lang->line('right_delete_edit');?></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th></th>
									<th>Total</th>
									<th id="total_barcode_quantity"></th>
									<th></th>
									<th>
										<button type="button" data-target="#barcode_print" data-toggle="modal"  class="btn green">Confirm</button>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>    

					<div class ="form-actions">

					</div>

				</form>
			</div>    
		</div>
	</div>
</div>

<div class="portlet light" style="display: none" id="voucher_preview">
	<div class="portlet-body" style="padding:30px;">
		<div class="invoice" id="invoice">
		</div>
	</div>
</div>

<!-- Print Modal -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="barcode_print" class="modal fade">	
	<div class="modal-content">
		<div class="modal-body">
			<?php $this->load->view('barcode/multi_barcode_print_options');?>
		</div>
	</div>
</div>
<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="customer_form_modal" class="modal fade">	
	<div class="modal-content">
		<div class="modal-body">
			<?php $this->load->view('customer/add_customer_form_only');?>
		</div>
	</div>
</div>
<!-- Customer Modal Form End -->

<!-- Sales Rep Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="sales_rep_form_modal" class="modal fade">	
	<div class="modal-content">
		<div class="modal-body">
			<?php $this->load->view('sales_rep/add_sales_rep_form_only');?>
		</div>
	</div>
</div>
<!-- Sales Rep Modal Form End -->
<!-- <span id="newText">sfdsdf</span> -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.numpad.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
	/*lading date picker*/
	jQuery(document).ready(function() {    
        		// $('.date-picker').datepicker();
        		$('.date-picker').datepicker({
        			format: 'yyyy-mm-dd',
        			rtl: Metronic.isRTL(),
        			orientation: "left",
        			autoclose: true
        		});
        		UIExtendedModals.init();
        	});
	/*loading date picker ends*/
	var csrf = $("input[name='csrf_test_name']").val();
	jQuery(document).ready(function($) {
		$.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
		$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
		$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
		$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
		$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
		$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
		$('#numpadButton-btn1').numpad({
			target: $('#numpadButton1')
		});        
		$('#numpadButton-btn2').numpad({
			target: $('.selling_price_and_numberpad')
		});    
	});
	table = $('#sample_2').DataTable();        
				// Customize Boostrap Select for Item Selection.
				var is_imei_used = "";
				var temp_item_id = "";
				var timer;
				$("#item_select").keyup(function(event) 
				{
					$("#item_select_result").show();
					$("#item_select_result").html('');
					clearTimeout(timer);
					timer = setTimeout(function() 
					{
						var search_item = $("#item_select").val();
						var html = '';
						$.post('<?php echo site_url(); ?>/sell/search_item_by_name_for_barcode',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
							data = JSON.parse(data);
							if(data.barcode_yes != null && data.barcode_yes != undefined){
								$.each(data, function(index, val) {
									if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
										val.item_spec_set_image = "images/item_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
									html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
								});
								$("#item_select_result").html(html);
								is_imei_used= "no";
								$('#item_select_result').find('td').eq(0).click();
								is_imei_used="no";

								if(is_unique_barcode != "yes" && is_imei_used== "no"){
									$('input[name=quantity]').val(1);
									$('#add_item').prop('disabled', false);
								}
								else{	
									$('input[name=quantity]').val('');
									$('#add_item').prop('disabled', true);
								}
								is_unique_barcode="";
							}
							else{
								$.each(data, function(index, val) {
									if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
										val.item_spec_set_image = "images/item_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
									html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
								});
								$("#item_select_result").html(html);
								is_imei_used= "no";
								is_unique_barcode="";

							}
						});
					},
					500);
				});

				$('#inclusive_vat_checkbox').prop('checked', true);
//  Script for adding product through AJAX.
var render_data= {
	'item_info' : [],
	'all_basic_data':{},
	'imei_serial_data':[],
};

var add_to_cart = function(event,relevent_data) {
	
	var error_found = false;
	var dis_type;
	var indivi_dis_amt;

	if(relevent_data == null)
	{
		event.preventDefault();
		if($('input[name="item_spec_set_id"]').val()==null || $('input[name="item_spec_set_id"]').val()== "")
		{
			$('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
			$('input[name="item_spec_set_id"]').parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name="item_spec_set_id"]').closest('.form-group').removeClass('has-error');
			$('input[name="item_spec_set_id"]').parent().find('.help-block').hide();

		}
		if($('input[name=quantity]').val().trim()=="")
		{
			$('input[name=quantity]').closest('.form-group').addClass('has-error');
			$('input[name=quantity]').parent().parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name=quantity]').closest('.form-group').removeClass('has-error');
			$('input[name=quantity]').parent().parent().find('.help-block').hide();
		}
		if($('input[name=selling_price]').val().trim()=="")
		{
			$('input[name=selling_price]').closest('.form-group').addClass('has-error');
			$('input[name=selling_price]').parent().parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name=selling_price]').closest('.form-group').removeClass('has-error');
			$('input[name=selling_price]').parent().parent().find('.help-block').hide();
		}
		if($('input[name=barcode]').val().trim()=="")
		{
			$('input[name=barcode]').closest('.form-group').addClass('has-error');
			$('input[name=barcode]').parent().parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name=barcode]').closest('.form-group').removeClass('has-error');
			$('input[name=barcode]').parent().parent().find('.help-block').hide();
		}
		dis_type = $('#dis_type').val();
		indivi_dis_amt= $('#dis_amt_id').val();
	}
	else
	{
		dis_type = relevent_data.discount_type;
		indivi_dis_amt = relevent_data.discount_amount;
		is_imei_used = relevent_data.imei_used;
	}

	if(dis_type != "" && indivi_dis_amt == ""){
		alert("<?php echo $this->lang->line('enter_dis_amt')?>");
		error_found =true;
	}
	if(dis_type == "" && indivi_dis_amt != ""){
		alert("<?php echo $this->lang->line('enter_dis_type')?>");
		error_found =true;
	}
	if(error_found)
	{
		return false;
	}
	var found = false;

	$.each(render_data['item_info'], function(index, val) {
		if(val.item_spec_set_id == $('input[name="item_spec_set_id"]').val())
		{
			render_data['item_info'][index]['actual_quantity'] =parseFloat(render_data['item_info'][index]['actual_quantity']) + parseFloat($('input[name="quantity"]').val());
			render_data['item_info'][index]['quantity'] =parseFloat(render_data['item_info'][index]['quantity']) + parseFloat($('input[name="quantity"]').val());

			found = true;
		}
		$("#itemDataValue").val(JSON.stringify(render_data.item_info));
	});
	if(!found){
		render_data.item_info.push({
			'barcode_number' :$('#barcode_number').val(),
			'item_spec_set_id' : $ ('input[name="item_spec_set_id"]').val(),
			'item_name' : $('input[name="items_name"]').val(),
			'actual_quantity' : $('input[name="quantity"]').val(),
			'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),
			'selling_price' : $('input[name="selling_price"]').val(),
		});

		$("#itemDataValue").val(JSON.stringify(render_data.item_info));
	}


	sell_table_render();
	$('input[name="items_name"]').val('');
	$('input[name="item_spec_set_id"]').val('');
	$('input[name="quantity"]').val('');
	$('input[name="barcode"]').val('');
	$('input[name="selling_price"]').val('');
	$('input[name="sell_comments"]').val('');
	$('input[name="current_quantity_inventory"]').val('');
	$('input[name="discount_amount"]').val('');
	$('#dis_type').val('');
	$('input[name=items_name]').focus();
}
var is_unique_barcode = "";

$(document).on('change', '#holdingInvoiceSelection', function(event) {
	clear_items_from_cart();
	var id = $(this).val();
	render_data.imei_serial_data = allHoldingInvoices[id].render_data.imei_serial_data;
	$.each(allHoldingInvoices[id].render_data.item_info, function(index, val) {
		add_to_cart(null,val);
	});
});

render_data['all_basic_data']['due_after_paid']=0;

$('.form-actions').on('click', '#add_item', add_to_cart);

$('.form-actions').on('click', '#cancle_item', function(event) {
	event.preventDefault();
	$('#add_item').show('fast');
	$('#update_item').hide();
	$('#current_qty').show('fast');
	// $('#add_item').prop("disabled",true);
	$('#item_select_field').show('fast');
	$('#item_select').prop("disabled",false);
	$('#sell_type_id').prop("disabled",false);
	$('input[name="items_name"]').val('');
	$('input[name="item_spec_set_id"]').val('');
	$('input[name="quantity"]').val('');
	$('input[name="current_quantity_inventory"]').val('');
	$('input[name="selling_price"]').val('');
	$('input[name="sell_comments"]').val('');
	$('input[name="discount_amount"]').val('');
	$('#dis_type').val('');
});
var current_active_vat_percentage =0;
var total_amount = 0;
var total_net_payable= 0;
var net_pay_after_discount = 0;
$(document).on('change', '.table_quantity', function(event) {
	event.preventDefault();
	var new_quantity = $(this).val();
	var row_number = $(this).attr('name').split('_')[1];
	var current_item_spec_id = render_data['item_info'][row_number]['item_spec_set_id'];
	$.get('<?php echo site_url() ?>/sell/quantity_by_item_from_inventory/'+current_item_spec_id, function(data) {
		var curr_item_quantity = $.parseJSON(data);
		if( parseFloat(new_quantity) > parseFloat(curr_item_quantity)){
			alert ("<?php echo $this->lang->line('alert_not_enough_quantity');?>");
			$('input[name=tablequantity_'+row_number+']').val(render_data['item_info'][row_number]['quantity']);
		}
		else {
			render_data['item_info'][row_number]['actual_quantity'] = new_quantity;
			render_data['item_info'][row_number]['quantity'] = new_quantity;
			render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['selling_price']*new_quantity;
			sell_table_render();
			// $('input[name=items_name]').focus();
		}
	});
});
$(document).on('change', '.table_price', function(event) {
	event.preventDefault();
	var new_price = $(this).val();
	var row_number = $(this).attr('name').split('_')[1];
	render_data['item_info'][row_number]['selling_price'] = new_price;
	render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['quantity']*new_price;
	sell_table_render();
	// $('input[name=items_name]').focus();
});
function sell_table_render () {
	var sell_table_html = '';
	total_amount = 0;
	var subtotal_amount = 0;
	$('#total_paid').val("");
	$('#total_discount').val('');
	var total_barcode = 0;
	$.each(render_data['item_info'], function(index, val) {
		total_barcode += parseInt(val.quantity);
		sell_table_html += '<tr class="sell_item_row_table" id="'+index+'">';
		sell_table_html += '<td>'+val.item_name+'</td>';
		sell_table_html += '<td>'+val.barcode_number+'</td>';
		sell_table_html += '<td>'+val.quantity+'</td>';
		sell_table_html += '<td>'+Number(val.selling_price).toFixed(2)+'</td>';
		sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red; margin-left:10px"></a>';
		sell_table_html += '</td>';
		sell_table_html += '</tr>';
	});
	$('#total_barcode_quantity').html(total_barcode);

	if($('#inclusive_vat_checkbox').is(":checked")){
		render_data['all_basic_data']['net_payable']=Number(this.total_amount.toFixed(2));;
		$('#sell_table tbody').html(sell_table_html);
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		var current_vat_percent = Number($('#total_vat').val());
		var vat_on_sell = parseFloat(total_amount) * parseFloat(current_vat_percent)||0 ;
		vat_on_sell /=100;
		final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
		$('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));    
		$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
		$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= current_vat_percent;
	}
	else{
		render_data['all_basic_data']['net_payable']=Number(this.total_amount.toFixed(2));
		$('#sell_table tbody').html(sell_table_html);
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		$('#total_amount_with_vat').text(Number(total_amount.toFixed(2)));    
		$('#total_net_payable').text(Number(total_amount.toFixed(2)));
		$('#total_due').text(Number(total_amount.toFixed(2)));
		render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= 0;
	}
}
render_data['all_basic_data']['discount_by_percentage']='yes';
$('#discount_percentage').prop('checked', true);
/* Discount with percentage before final submission starts*/
$(document).on('change', '#discount_percentage', function(event) {
	/* Act on the event */
	if($('#discount_percentage').is(":checked")){
		var discount_percent = $('#total_discount').val()||0;
		var discount_calculation = (parseFloat($('#total_amount_with_vat').text()) * discount_percent)/100 ;
		var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_calculation)||0;
		$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
		$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
		if($('#total_paid').val() !=""){
			render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		}
		$('#total_change').text(0);
		render_data['all_basic_data']['discount']=parseFloat(discount_percent)||0;
		render_data['all_basic_data']['discount_by_percentage']='yes';
		render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
		render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
		$('#total_paid').val('');
	}
	else{
		var discount_amt = $('#total_discount').val()||0; 
		if (discount_amt > Number($('#total_amount_with_vat').text()) ) {
			alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
			$('#total_discount').val(''); 
			$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
			$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		}
		var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_amt)||0;
		$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
		$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
		if($('#total_paid').val() !=""){
			render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		}
		$('#total_change').text(0);
		render_data['all_basic_data']['discount']=parseFloat(discount_amt)||0;
		render_data['all_basic_data']['discount_by_percentage']='no';
		render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
		render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
		$('#total_paid').val('');
	}
});


var final_amount_with_vat;

$.get('<?php echo site_url() ?>/sell/get_current_vat_percentage/', function(data) {
	var vat_data = $.parseJSON(data);
	$('#total_vat').val(vat_data.vat_percentage);
	current_active_vat_percentage = vat_data.vat_percentage;
});
$('input[name="discount"]').val('');
$('#total_discount').keyup(function(event) {
	if($('#discount_percentage').is(":checked")){
		var discount_percent = $('#total_discount').val()||0;
		if( discount_percent >100){
			alert("Discount not available more than 100");
			$('#total_discount').val('');
			$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
			$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		}
		else{
			var discount_calculation = (parseFloat($('#total_amount_with_vat').text()) * discount_percent)/100 ;
			var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_calculation)||0;
			$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
			$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
			if($('#total_paid').val() !=""){
				render_data['all_basic_data']['paid']= Number($('#total_paid').val());
			}
			$('#total_change').text(0);
			render_data['all_basic_data']['discount']=parseFloat(discount_percent)||0;
			render_data['all_basic_data']['discount_by_percentage']='yes';
			render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
			render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
			$('#total_paid').val('');
		}
	}
	else{
		var dis_amt = $('#total_discount').val();
		if (dis_amt > Number($('#total_amount_with_vat').text()) ) {
			alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
			$('#total_discount').val('');
			$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
			$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		}
		var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(dis_amt)||0;
		$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
		$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
		if($('#total_paid').val() !=""){
			render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		}
		$('#total_change').text(0);
		render_data['all_basic_data']['discount']=parseFloat($(this).val())||0;
		render_data['all_basic_data']['discount_by_percentage']='no';
		render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
		render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
		$('#total_paid').val('');
	}
});


$('#sell_table').on('click', '#item_remove', function () {
	var clicked_id = $(this).closest('tr').attr('id');
	var temp_id_remove = render_data['item_info'][clicked_id].temp_id_info;
	$.each(render_data['imei_serial_data'], function(index, val) {
		if(val.temp_id == temp_id_remove){
			render_data['imei_serial_data'].splice(index,1);
			return false;
		}
	});
	render_data['item_info'].splice(clicked_id,1);
	sell_table_render();
	$("#itemDataValue").val(JSON.stringify(render_data.item_info));
});


function remove_all_errors() {
	// $('#right_box').siblings('#error_messages').slideUp(1);
	$('#right_box').find('.has-error').removeClass('has-error');
	$('table tfoot tr').eq(5).css("color","black").removeClass('has-error');
	$('table tfoot tr').eq(3).css("color","black").removeClass('has-error');
	$('#total_paid').siblings().show().text("");
	$('#customer_help_block').text("");
	$('#total_discount').siblings().eq(0).text("");
	$('#card_amount_id').closest('.form-group').removeClass('has-error');
	$('#card_amount_id').siblings().text("");
	$('#card_pay_voucher_id').closest('.form-group').removeClass('has-error');
	$('#card_pay_voucher_id').siblings().text("");
	$('#chk_num').closest('.form-group').removeClass('has-error');
	$('#chk_num').siblings().text("");
	$('#bank_acc_select').siblings().text('');
}
var product_warranty = "";
/* a123*/
$('#item_select_result').on('click', 'td', function(event) {
	event.preventDefault();
	$('input[name="items_name"]').val($(this).text());
	$('input[name="item_spec_set_id"]').val($(this).attr('data'));
	$("#item_select_result").hide();
	var item_select_val = $('input[name="item_spec_set_id"]').val();
	$.get('<?php echo site_url() ?>/sell/get_item_barcode/'+item_select_val, function(data) {
		var data_get = $.parseJSON(data);
		console.log(data_get);
		$('#barcode_number').val(data_get.barcode_number);
		$('#price').val(data_get.price);
	});
});


// $('#add_item').prop("disabled",true);

$('input:radio[name="payment_type"]').change(
	function(){
		if ( $(this).val() == 'card' ) {
			$('#card_voucher_no').show('fast');
			$('#ac_select_div').show('fast');
			$('#card_amount_for_both').hide('fast');   
			$('#cheque_num').hide('fast'); 
			$('#chk_num').val('');   
			$('#card_amount_id').val('');
		}    
		if($(this).val() == 'cash_card_both'){
			$('#card_voucher_no').show('fast');
			$('#ac_select_div').show('fast');
			$('#card_amount_for_both').show('fast'); 
			$('#cheque_num').hide('fast');    
			$('#chk_num').val(''); 
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
		if($(this).val() == 'cash'){
			$('#card_voucher_no').hide('fast');
			$('#ac_select_div').hide('fast');
			$('#card_amount_for_both').hide('fast');
			$('#cheque_num').hide('fast');   
			$('#chk_num').val(''); 
			$('#card_pay_voucher_id').val('');
			$('#card_amount_id').val('');
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
		if($(this).val() == 'cheque'){
			$('#card_voucher_no').hide('fast');
			$('#ac_select_div').hide('fast');
			$('#card_amount_for_both').hide('fast');    
			$('#cheque_num').show('fast');    
			$('#card_pay_voucher_id').val('');
			$('#card_amount_id').val('');
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
	});
$('input[name=items_name]').focus();

$('#dis_amt_id').keyup(function(event) {
	var dis_type = $('#dis_type').val();
	if (dis_type == ""){
		alert("<?php echo $this->lang->line('enter_dis_type');?>");
		$('#dis_amt_id').val('');
	}
});
$('#dis_type').change(function(event) {
	var dis_type = $('#dis_type').val();
	var indivi_dis_amt= $('#dis_amt_id').val();
	if(dis_type == "" && indivi_dis_amt != ""){
		alert("<?php echo $this->lang->line('enter_dis_type_and_amt');?>");
		$('#dis_amt_id').val('');
	}
});

var hold_invoice_data= {
	'individual_invoice_data' : [],
};
var date_data = new Date();
var month = date_data.getMonth()+1;
var day = date_data.getDate();
var date_output = date_data.getFullYear() + '/' +(month<10 ? '0' : '') + month + '/' +(day<10 ? '0' : '') + day;

$('#sell_table').on('click', '#hold_invoice_id', function(event) {
	event.preventDefault();
	$("#holdingInvoiceSelection").show();

  	// hold_invoice_data['individual_invoice_data'].push(render_data);
  	var allHoldingInvoices = localStorage.getItem('hodldingInvoices');
  	
  	if(allHoldingInvoices == null)
  	{
  		allHoldingInvoices = [];
  	}else{
  		allHoldingInvoices = JSON.parse(allHoldingInvoices);
  	}

  	if(render_data['item_info'].length == 0){
  		alert ("No Item Available In This Invoice");
  	}
  	else{
  		hold_invoice_temp_id= new Date().getUTCMilliseconds();
  		invoice_data = {
  			'holding_sn' : hold_invoice_temp_id+" --> "+date_output,
  			'render_data' : render_data
  		}
  		allHoldingInvoices.push(invoice_data);
  		localStorage.setItem('hodldingInvoices',JSON.stringify(allHoldingInvoices));
  		setHoldingInvoices();
  		clear_items_from_cart();
  		alert("Success.Invoice Hold For Further Use");
  	}
  	
  });

function clear_items_from_cart() {
	$('#discount_percentage').prop('checked', true);
	$('#discount_percentage').parent().addClass('checked');
	$('input[name="sales_rep_name"]').val('');
	$('input[name="customers_name"]').val('');
	$('input[name=sell_date]').val('');
	$('input[name="sales_rep_id"]').val('');
	$('input[name="customers_id"]').val('');
	$('.sell_item_row_table').remove();
	render_data['item_info'] = [];
	render_data['imei_serial_data'] = [];
	render_data['all_basic_data'] = {};
	$('input[name="tax_percentage"]').val('');
	$('input[name="discount"]').val('');
	$('input[name="received_money"]').val('');
	$('input[name="paid"]').val('');
	$('input[name="card_payment_voucher_no"]').val('');
	$('input[name="card_amount"]').val('');
	$('input[name="voucher_no"]').val('');
	$('#chk_num').val('');
	$('#total_amount').html('');
	$('#total_amount_with_vat').html('');
	$('#total_net_payable').html('');
	$('#total_change').html('');
	$('#total_due').html('');
	$('input[name="customers_id"]').val('');
	$('input[name="sales_rep_id"]').val('');
	$('#save_sell').prop("disabled",false);
	$('input[name=items_name]').focus();
	$('input[name="bank_acc_num"]').val('');
	$('input[name="bank_acc_id"]').val('');
	$('#total_vat').val(current_active_vat_percentage);
	if(!$('#inclusive_vat_checkbox').is(":checked")){
		$('#inclusive_vat_checkbox').prop('checked', true);
		$('#inclusive_vat_checkbox').parent().addClass('checked');
	}
	$("#options_cash").attr('checked', 'checked').click();
	$('#card_voucher_no').hide('fast');
	$('#ac_select_div').hide('fast');
	$('#card_amount_for_both').hide('fast');
	$('#cheque_num').hide('fast');  
};

</script>