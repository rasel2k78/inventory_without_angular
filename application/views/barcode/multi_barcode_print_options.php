
<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption left">
			<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('barcode_header')?></span>
		</div>
	</div>
	<div class="portlet-body form" id="barcode_quantity_div">
		<!-- BEGIN FORM-->
		<?php $form_attrib = array('id' => 'barcode_quantity_form','class' => 'form-horizontal form-bordered');
		echo form_open('item/multi_barcode_print_barcode',  $form_attrib, '');?>
		<div class="form-body">    	
			<input type="hidden" name="itemData" id="itemDataValue" value="" placeholder="">
			<div class="form-group">
				<label class="control-label col-md-3">Page Type</label>
				<div class="radio-list">
					<label class="radio-inline">
						<input type="radio" name="page_type" id="page_4x10" value="4x10">4x10
					</label>
					<label class="radio-inline">
						<input type="radio" name="page_type" id="page_5x13" value="5x13">5x13
					</label>
					<label class="radio-inline">
						<input type="radio" name="page_type" id="page_plain" value="plain">Plain
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Company Name</label>
				<div class="radio-list">
					<label class="radio-inline">
						<input type="radio" name="add_company" value="Yes" checked="checked">Yes
					</label>
					<label class="radio-inline">
						<input type="radio" name="add_company" value="No" >No
					</label>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<button data-dismiss="modal" class="btn dark btn-outline pull-right" style="margin-left: 8px" type="button"><?php echo $this->lang->line('cancle_print')?></button>
			<button type="submit" class="btn green pull-right"><?php echo $this->lang->line('print_barcode')?></button>
		</div>
		<?php echo form_close();?>
	</div>
</div>
<script type="text/javascript">
	/* Barcode Print Starts*/
// 	$('#barcode_quantity_form').submit(function(event) {
// 		event.preventDefault();
// 		var post_data={};
// 		post_data.barcode_list = render_data['item_info'];
// 		post_data.page_size  = $('input[name="page_type"]:checked').val();
// 		post_data.company_name_add =  $('input[name="add_company"]:checked').val();
// 		post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
// 		var params = jQuery.param(render_data['item_info']);
// 		if (post_data.barcode_quantity == "") {
// 			$('input[name= print_quantity]').parent().parent().addClass('has-error');
// 			$('input[name= print_quantity]').parent().parent().find('.help-block').text("<?php echo $this->lang->line('barcode_quantity_alert'); ?>");
// 		}
// 		else if(!$('input[name="page_type"]:checked').val()){
// 			alert("<?php echo $this->lang->line('page_size_alert'); ?>");
// 		}
// 		else {
// 		// location.href="<?php echo site_url() ?>/item/multi_barcode_print_barcode/"+params+"/"+post_data.page_size+"/"+post_data.company_name_add;
// 		$.post('<?php echo site_url() ?>/item/multi_barcode_print_barcode/', {post_data}, function(data, textStatus, xhr) {
// 		});
// 	}
// });
</script>