<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-6">

        <!-- CODE FOR SLIDER ALERT -->
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failure'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?></div>
        <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?></div>
        <div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?></div>
        <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
        <div class="alert alert-danger" id="customer_exist" style="display:none" role="alert"><?php echo $this->lang->line('customer_exist'); ?></div>
        <div class="alert alert-success" id="delete_success_from_list" style="display:none" role="alert"><?php echo $this->lang->line('deletion_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('no_deletion'); ?></div>
        <div class="alert alert-danger" id="no_permission_right" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
        <div class="alert alert-danger" id="no_phone" style="display:none" role="alert"><?php echo $this->lang->line('no_phone'); ?></div>
        <div class="alert alert-danger" id="id_exist_alert" style="display:none" role="alert"><?php echo $this->lang->line('id_exist_alert'); ?></div>


        <!-- CODE FOR SLIDER ALERT END-->

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('add_new_customer'); ?></span>
                </div>
                
            </div>
            <div class="portlet-body form">

                <!-- BEGIN OF CUSTOMER FORM-->

                <?php
                $form_attrib = array('id' => 'customer_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/Customers/createAccount'),  $form_attrib, '');
                ?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_id'); ?> </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="customer_custom_id" placeholder="<?php echo $this->lang->line('customer_id'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="customers_name" placeholder="<?php echo $this->lang->line('customer_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_phone'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="1" class="form-control" name="customers_phone_1"  placeholder="<?php echo $this->lang->line('customer_phone'); ?> ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_phone_2'); ?></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="1" class="form-control" name="customers_phone_2"  placeholder="<?php echo $this->lang->line('customer_phone_2'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_email'); ?></label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="customers_email" placeholder="<?php echo $this->lang->line('customer_email'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_present_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="customers_present_address"  placeholder="<?php echo $this->lang->line('customer_present_address'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_permanent_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="customers_permanent_address"  placeholder="<?php echo $this->lang->line('customer_permanent_address'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('customer_nid'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="customers_national_id"  placeholder="<?php echo $this->lang->line('customer_nid'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                    <div id="image_of_user" class="form-group ">
                        <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('customer_image'); ?></label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div id="user_image" class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            <?php echo $this->lang->line('select_image'); ?>
                                        </span>
                                        <span class="fileinput-exists">
                                            <?php echo $this->lang->line('change'); ?> 
                                        </span>
                                        <input type="file" name="user_image">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        <?php echo $this->lang->line('delete'); ?> 
                                    </a>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                    <input type="submit" value="<?php echo $this->lang->line('save'); ?>"  class="btn green pull-right"/>
                    <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
                    <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
                </div>
            </form>
            <!-- END OF CUSTOMER FORM-->
        </div>
    </div>
</div>
<div class="col-md-6">

    <!-- BEGIN CUSTOMER TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><?php echo $this->lang->line('all_customer'); ?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            <?php echo $this->lang->line('name'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('mobile'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('change_delete'); ?>
                        </th>

                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>
    <!-- END CUSTOMER TABLE PORTLET-->

</div>
</div>


<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="user_name_selected" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('do_you_want_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('no'); ?></button>
    <button type="button" id="user_delete_confirm_y" class="btn blue"><?php echo $this->lang->line('yes'); ?></button>
</div>
</div>

<!-- Time: <?php echo $this->benchmark->elapsed_time(); ?> -->
<!-- Mem: <?php echo $this->benchmark->memory_usage(); ?> -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {    
        UIExtendedModals.init();
    });
</script>  

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>

//Code for data table start
var table_user = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/Customers/all_customer_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for data table end


//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#customer_form').ajaxForm({
    beforeSubmit: function() {    
        $('input[name = customers_name]').closest('.form-group').removeClass('has-error');
        $('input[name = customers_name]').siblings().text("");
        $('input[name = customers_phone_1]').closest('.form-group').removeClass('has-error');
        $('input[name = customers_phone_1]').siblings().text("");
    },
    success: function(msg) {

        if(msg == "Customer not set")
        {
            $('input[name = customers_name]').closest('.form-group').addClass('has-error');
            $('input[name = customers_name]').siblings().text("<?php echo $this->lang->line('no_username'); ?>");
            // $('#no_username').slideDown();
            // setTimeout( function(){$('#no_username').slideUp()}, 3000 );
        }
        else if(msg == "Phone not set")
        {
            $('input[name = customers_phone_1]').closest('.form-group').addClass('has-error');
            $('input[name = customers_phone_1]').siblings().text("<?php echo $this->lang->line('no_phone'); ?>");
            // $('#no_phone').slideDown();
            // setTimeout( function(){$('#no_phone').slideUp()}, 3000 );
        }
        else if(msg == "Customer id exist")
        {
            $('#id_exist_alert').slideDown();
            setTimeout( function(){$('#id_exist_alert').slideUp()}, 3000 );
        }
        else if( msg == "Customer Already Created."){
            alert("<?php echo $this->lang->line('customer_already_exists'); ?>");
        }
        else if(msg.substring(0, 7) == "success"){
            // setTimeout( function(){$('#no_username').slideUp()}, 1 );
            // setTimeout( function(){$('#no_phone').slideUp()}, 1 );
            $('#user_insert_success').slideDown();
            setTimeout( function(){$('#user_insert_success').slideUp()}, 3000 );
            $('input[name = customers_name]').val("");
            $('input[name = customer_custom_id]').val("");
            $('input[name = customers_email]').val("");
            $('input[name= customers_present_address]').val("");
            $('input[name = customers_permanent_address]').val("");
            $('input[name = customers_phone_1]').val("");
            $('input[name = customers_phone_2]').val("");
            $('input[name = customers_national_id]').val("");
            $('.fileinput-preview').html("<img src=''>");

        }else if(msg == "Customer already exist"){
            $('#customer_exist').slideDown();
            setTimeout( function(){$('#customer_exist').slideUp()}, 5000 );
        }
        else if(msg == "No permission"){
            $('#no_permission').slideDown();
            setTimeout( function(){$('#no_permission').slideUp()}, 5000 );
        }
    },
    complete: function(xhr) {
        table_user.ajax.reload(null, false);
        return false;
    }

}); 
//Ajax form submit end

</script>

<script type="text/javascript">

    var user_id ='';


//Code for showing user data on form for edit customer button start
$('table').on('click', '.edit_user',function(event) {
    event.preventDefault();
    user_id = $(this).attr('id').split('_')[1];
    $('#current_user_id').remove();

    $.get('<?php echo site_url();?>/Customers/customer_info_by_id/'+user_id).done(function(data) 
    {

        var user_data = $.parseJSON(data);

        var html = "<input id='current_user_id' type='hidden' class='form-control' name='user_id' value='"+user_id+"'>";
        $('#customer_form').append(html);
        $('#customer_form').attr('id', 'user_form_update');
        $('#user_form_update').attr('action', "<?php echo site_url('/Customers/update_customer_info');?>"+"/"+user_id);



        $('#form_header_text').text("<?php echo $this->lang->line('change_customer_info'); ?>");
        $('#add_or_update').text("<?php echo $this->lang->line('update_info'); ?>");
        $('#email').prop('disabled', true);
        $('.pass_hide').hide('slow');
        $('#change_pic').hide('slow');
        $("#user_image").prop('disabled', false);
        $("#image_of_user_label").text("<?php echo $this->lang->line('image_of_customer'); ?>");
        
        /*To set the value on input field after clicking on edit button for a particular user*/
        
        $('input[name = customer_custom_id]').val(user_data.customer_info_by_id.customer_custom_id);
        $('input[name = customers_name]').val(user_data.customer_info_by_id.customers_name);
        $('input[name = customers_email]').val(user_data.customer_info_by_id.customers_email);
        $('input[name = customers_present_address]').val(user_data.customer_info_by_id.customers_present_address);
        $('input[name = customers_permanent_address]').val(user_data.customer_info_by_id.customers_permanent_address);
        $('input[name = customers_phone_1]').val(user_data.customer_info_by_id.customers_phone_1);
        $('input[name = customers_phone_2]').val(user_data.customer_info_by_id.customers_phone_2);
        $('input[name = customers_national_id]').val(user_data.customer_info_by_id.customers_national_id);

        if(user_data.customer_info_by_id.customers_image!="" && user_data.customer_info_by_id.customers_image!=null)
        {
            $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+user_data.customer_info_by_id.customers_image+"'>");
        }
        else
        {
            $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/user_images/no_image.png'>");
        }

        $('#user_form_update').ajaxForm({
            beforeSubmit: function() {    
                $('input[name = customers_name]').closest('.form-group').removeClass('has-error');
                $('input[name = customers_name]').siblings().text("");
                $('input[name = customers_phone_1]').closest('.form-group').removeClass('has-error');
                $('input[name = customers_phone_1]').siblings().text("");
            },
            success: function(msg) {
                if(msg == "Customer not set")
                {
                    $('input[name = customers_name]').closest('.form-group').addClass('has-error');
                    $('input[name = customers_name]').siblings().text("<?php echo $this->lang->line('no_username'); ?>");
                    // $('#no_username').slideDown();
                    // setTimeout( function(){$('#no_username').slideUp()}, 3000 );
                }

                else if(msg == "Phone not set")
                {
                    $('input[name = customers_phone_1]').closest('.form-group').addClass('has-error');
                    $('input[name = customers_phone_1]').siblings().text("<?php echo $this->lang->line('no_phone'); ?>");
                    // $('#no_phone').slideDown();
                    // setTimeout( function(){$('#no_phone').slideUp()}, 3000 );
                }

                else if(msg == "success"){
                    $('#user_insert_success').slideDown();
                    setTimeout( function(){$('#user_insert_success').slideUp()}, 3000 );
                    $('#form_header_text').text("<?php echo $this->lang->line('add_new_customer'); ?>");
                    $('input[name = customer_custom_id]').val("");
                    $('input[name = customers_name]').val("");
                    $('input[name = customers_email]').val("");
                    $('input[name= customers_present_address]').val("");
                    $('input[name = customers_permanent_address]').val("");
                    $('input[name = customers_phone_1]').val("");
                    $('input[name = customers_phone_2]').val("");
                    $('input[name = customers_national_id]').val("");
                    $('.fileinput-preview').html("<img src=''>");

                    $('#user_form_update').attr('action', "<?php echo site_url('/Customers/createAccount');?>");
                    $('#user_form_update').attr('id', 'customer_form');
                    $('#current_user_id').remove();

                }else if(msg == "update_fail"){
                    $('#insert_failure').slideDown();
                    setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
                }else if(msg == "No permission"){
                    $('#no_permission').slideDown();
                    setTimeout( function(){$('#no_permission').slideUp()}, 3000 );
                }
            },
            complete: function(xhr) {
                table_user.ajax.reload(null, false);
                return false;
            }

        }); 

    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
//Code for showing user data on form for edit customer button end


//Code for clear the form on clicking cancel button start
$('#cancel_update').click(function(event) {
    $('#form_header_text').text("<?php echo $this->lang->line('add_new_customer'); ?>");
    $('input[name = customer_custom_id]').val("");
    $('input[name = customers_name]').val("");
    $('input[name = customers_email]').val("");
    $('input[name= customers_present_address]').val("");
    $('input[name = customers_permanent_address]').val("");
    $('input[name = customers_phone_1]').val("");
    $('input[name = customers_phone_2]').val("");
    $('input[name = customers_national_id]').val("");
    $('.fileinput-preview').html("<img src=''>");

    $('#user_form_update').attr('action', "<?php echo site_url('/Customers/createAccount');?>");
    $('#user_form_update').attr('id', 'customer_form');
    $('#current_user_id').remove();
});
//Code for clear the form on clicking cancel button end



//Code for delete user with confirmation modal satrt


var user_id = '' ;
var user_name_selected = '';
$('table').on('click', '.delete_user',function(event) {
    event.preventDefault();
    user_id = $(this).attr('id').split('_')[1];
    user_name_selected = $(this).parent().parent().find('td:first').text();
    $('#user_name_selected').html(user_name_selected);
});


$('#responsive_modal_delete').on('click', '#user_delete_confirm_y', function(event) {
    event.preventDefault();
    var post_data ={
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        'user_id' : user_id
    }; 
    $.post('<?php echo site_url();?>/Customers/delete_user/'+user_id,post_data).done(function(data) {
        if(data == "Deletion success")
        {
            $('#responsive_modal_delete').modal('hide');
            $('#delete_success_from_list').slideDown();
            setTimeout( function(){$('#delete_success_from_list').slideUp()}, 1500 );
            table_user.row($('#delete_'+user_id).parents('tr')).remove().draw();

            $('input[name = customers_name]').val("");
            $('input[name = customers_email]').val("");
            $('input[name= customers_present_address]').val("");
            $('input[name = customers_permanent_address]').val("");
            $('input[name = customers_phone_1]').val("");
            $('input[name = customers_phone_2]').val("");
            $('input[name = customers_national_id]').val("");
            $('.fileinput-preview').html("<img src=''>");
        } 
        else if(data == "No permission"){
            $('#responsive_modal_delete').modal('hide');
            $('#no_permission_right').slideDown();
            setTimeout( function(){$('#no_permission_right').slideUp()}, 1500 );
        }
        else
        {
            alert("An error has occured. pLease try again");                
        }

    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});



//Code for update customer information start
// $('.portlet-body.form').on('click', '#user_form_update input[type="submit"]', function(event){
// 	event.preventDefault();

// 	var data = {};
// 	data.csrf_test_name = $('input[name=csrf_test_name]').val();

// 	data.customers_id = user_id;
// 	data.customers_name = $('input[name = customers_name]').val();
// 	data.customers_present_address = $('input[name= customers_present_address]').val();
// 	data.customers_permanent_address = $('input[name = customers_permanent_address]').val();
// 	data.customers_phone_1 = $('input[name = customers_phone_1]').val();
// 	data.customers_phone_2 = $('input[name = customers_phone_2]').val();
// 	data.customers_national_id = $('input[name = customers_national_id]').val();

// 	$.post("<?php echo site_url('/Customers/update_customer_info');?>",data).done(function(data1, textStatus, xhr) {
// 		if(data1 == "info_updated"){
// 			table_user.ajax.reload(null, false);
// 			$('#update_success').slideDown();
// 			setTimeout( function(){$('#update_success').slideUp()}, 1500 );
// 		}

// 		else if(data1 == "Username not set")
// 		{
// 			$('#no_username').slideDown();
// 			setTimeout( function(){$('#no_username').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Username already exists"){
// 			$('#username_exist').slideDown();
// 			setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Email already exists"){
// 			$('#email_exist').slideDown();
// 			setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Mobile no. 1 can not be Empty"){
// 			$('#no_mobile').slideDown();
// 			setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
// 		}

// 	}).error(function() {
// 		$('#insert_failure').slideDown();
// 		setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
// 	});	
// });
//Code for update customer information end

</script>
