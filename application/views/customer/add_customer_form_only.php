<!-- CODE FOR SLIDER ALERT END-->

<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('add_new_customer'); ?></span>
        </div>
        
    </div>
    <div class="portlet-body form">

        <!-- BEGIN OF CUSTOMER FORM-->

        <?php
        $form_attrib = array('id' => 'customer_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
        echo form_open(site_url('/Customers/createAccount'),  $form_attrib, '');
        ?>

        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="customers_name" id="customer_name_sell_page" placeholder="<?php echo $this->lang->line('customer_name'); ?>">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_phone'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                <div class="col-md-9">
                    <input type="number" min="0" step="1" class="form-control" name="customers_phone_1"  placeholder="<?php echo $this->lang->line('customer_phone'); ?> ">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_present_address'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="customers_present_address"  placeholder="<?php echo $this->lang->line('customer_present_address'); ?>">
                    <span class="help-block"></span>
                </div>
            </div>

            <div id="image_of_user" class="form-group ">
                <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('customer_image'); ?></label>
                <div class="col-md-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div id="user_image" class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                        </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    <?php echo $this->lang->line('select_image'); ?>
                                </span>
                                <span class="fileinput-exists">
                                    <?php echo $this->lang->line('change'); ?> 
                                </span>
                                <input type="file" name="user_image">
                            </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                <?php echo $this->lang->line('delete'); ?> 
                            </a>
                        </div>


                    </div>

                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
            <input type="submit" value="<?php echo $this->lang->line('save'); ?>" name="image_upload" id="image_upload" class="btn green pull-right"/>
            <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
            <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
        </div>
    </form>
</div>
<!-- END OF CUSTOMER FORM-->

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

<script type="text/javascript">

    //Ajax form submit start and slider shown after validation check based on msg which is comes from controller
    $('#customer_form').ajaxForm({
        beforeSubmit: function() {    
            $('#customer_name_sell_page').closest('.form-group').removeClass('has-error');
            $('#customer_name_sell_page').siblings().text("");
            $('input[name = customers_phone_1]').closest('.form-group').removeClass('has-error');
            $('input[name = customers_phone_1]').siblings().text("");
        },
        success: function(msg) {
            if(msg == "Customer not set")
            {
                $('#customer_name_sell_page').closest('.form-group').addClass('has-error');
                $('#customer_name_sell_page').siblings().text("<?php echo $this->lang->line('no_username_customer'); ?>");
                // $('#customer_form_modal').modal('hide');
                // $('#no_username_customer').slideDown();
                // setTimeout( function(){$('#no_username_customer').slideUp()}, 3000 );
            }

            else if(msg == "Phone not set")
            {
                $('input[name = customers_phone_1]').closest('.form-group').addClass('has-error');
                $('input[name = customers_phone_1]').siblings().text("<?php echo $this->lang->line('no_phone'); ?>");
                //$('#customer_form_modal').modal('hide');
                //$('#no_phone').slideDown();
                //setTimeout( function(){$('#no_phone').slideUp()}, 3000 );
            }

            else if(msg.substring(0, 7) == "success"){
                $('#customer_form_modal').modal('hide');
                $('#user_insert_success').slideDown();
                setTimeout( function(){$('#user_insert_success').slideUp()}, 3000 );
                var customer_name = $('#customer_name_sell_page').val();
                var customer_id = msg.substring(7, 44);
                $('#customers_select').val(customer_name);
                $('input[name="customers_id"]').val(customer_id);
                $('#customer_form').trigger("reset");
            }

            else if(msg == "No permission"){
                $('#customer_form_modal').modal('hide');
                $('#customer_form').trigger("reset");
                $('#no_permission_left').slideDown();
                setTimeout( function(){$('#no_permission_left').slideUp()}, 5000 );
                
            }
        },
        complete: function(xhr) {
            // table_user.ajax.reload();
            return false;
        }

    }); 

    $('#cancel_update').click(function(event) {
        event.preventDefault();
        $('#customer_form').trigger("reset");
        $('#customer_form_modal').modal('hide');
    });
//Ajax form submit end
</script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->