<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<div class="row">

    <div class="col-md-7">
        <div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_item_price'); ?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_appropriate_amount'); ?></div>
        <div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('give_item_quntity'); ?></div>
        <div class="alert alert-danger" id="item_price_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('item_invalid_price'); ?></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('transfer_receive_list'); ?>
                </div>
            </div>
            <div class="portlet-body">

                <?php 
                if(!$transfer_in) 
                {
                    echo 'No transfer is receivable for you right now';
                }

                else
                {
                    ?>
                    <table id="sell_table" class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('transfer_from'); ?></th>
                                <th width="8%"><?php echo $this->lang->line('total_quantity'); ?></th>
                                <th width="8%"><?php echo $this->lang->line('price'); ?></th>
                                <th>Transfer Date</th>
                                <th><?php echo $this->lang->line('status'); ?></th>
                                <th>Update Date</th>
                                <!-- <th><?php echo $this->lang->line('details'); ?></th> -->
                                <!-- <th><?php echo $this->lang->line('description'); ?></th> -->
                            </tr>
                            <?php 

                            foreach ($transfer_in as $key => $value) {
                                ?>

                                <tr>
                                   <th style="display:none"><?php echo $value['transfer_from']?></th>
                                   <th><?php echo $value['name']?></th>
                                   <th id="totalQuantity"><?php echo $value['total_item_quantity']?></th>
                                   <th id="totalAmount"><?php echo $value['total_amount']?></th>
                                   <th><?php echo date('Y-m-d', strtotime($value['date_created']))?></th>

                                   <?php 
                                   if($value['status'] == 'pending') 
                                   {
                                    ?>
                                    <th id="totalAmount" ><?php echo $value['status']?>
                                        <button type="button" id="details_id_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green transfer_details"><?php echo $this->lang->line('details'); ?></button></th>
                                        <?php
                                    }

                                    else
                                    {
                                        ?><th id="totalAmount" style="color:green"><?php echo $value['status']?></th><?php
                                    }
                                    ?>
                                    <!-- <th id="transfer_details_id" style="hidden"><?php echo $transfer_details['transfer_details_id']?></th> -->

                                    <th><?php echo date('Y-m-d', strtotime($value['date_updated']))?></th>
                                    <?php 
                                    if($value['status']=="completed")
                                    {
                                        ?><th><a style="color:gold" href="#" id="details_id_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green adjust_cashbox">Adjust Cashbox?</a></th><?php
                                    }
                                    ?>
                                </tr>
                                <?php
                            }

                            ?>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <?php
                }
                ?>
                <input type="hidden" name="transfer_from_id" id="transferFromId">
            </div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
    </div>



    <div class="col-md-5">
        <div class="portlet box green-haze" id="transfer_details_form2" style="display:none">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i> <?php echo $this->lang->line('transfer_items'); ?>
                </div>

            </div>
            <div class="portlet-body">
                <input type="hidden" id="transfer_details_id" name="trasfer_id">
                <table id="sell_table2" class="table table-condensed">
                    <thead>
                        <tr>
                            <th colspan="5" align="center">
                                <input style="border:0.5px solid #1F897F" type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('search_item'); ?>" id="item_select" class="form-control">
                                <i id="itemTooltip" class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('match_item_tt'); ?>"></i>

                                <input type="hidden" autocomplete="off" name="item_spec_set_id"  class="form-control">
                                <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>    
                            </th>
                        </tr>
                        <tr>
                            <th><?php echo $this->lang->line('item'); ?></th>
                            <th><?php echo $this->lang->line('total_quantity'); ?></th>
                            <th>Buying Price</th>
                            <th><?php echo $this->lang->line('selling_price'); ?></th>
                            <th><?php echo $this->lang->line('item_on_inventory'); ?>&nbsp;<i id="itemTooltip" class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_on_tt'); ?>"></i>
                            </th>
                            <th><?php echo $this->lang->line('match_item'); ?>&nbsp;<i id="itemTooltip" class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('adjust_click_tt'); ?>"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="transfer_items_add"></tbody>
                    <tfoot>
                        <tr>
                            <th></th><th></th>
                            <th><button type="submit" id="save_transfer" class="btn green confirm_receive"><?php echo $this->lang->line('receive'); ?></button></th>
                            <th></th><th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460" style="top:20% !important">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_update'); ?> 
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('no'); ?></button>
        <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('yes'); ?></button>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script>
    var csrf = $("input[name='csrf_test_name']").val();
    function change_form()
    {
        $('#form1').hide();
        $('#form2').show();
    }
    var render_data= {
        'item_info' : [],
        'all_basic_data':{},
    };
    var render_transfer_data= {
        'item_info' : [],
        'all_basic_data':{},
    };


        // $('#transfer_details_form2').on('keyup', '.item_select', function(event) {
        //     $("#item_select_result").show();
        //     $("#item_select_result").html('');
        //     var timer;
        //     clearTimeout(timer);
        //     timer = setTimeout(function() 
        //     {
        //         var search_item = $("this").val();
        //         var html = '';
        //         $.post('<?php echo site_url(); ?>/buy/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
        //         // console.log(data);
        //         data = JSON.parse(data);
        //         // console.log(data);
        //         $.each(data, function(index, val) {
        //             if(data['item_image'] == null){
        //                 val.item_spec_set_image    = "images/item_images/no_image.png";
        //             }
        //             var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
        //             html+= '<tr><td onclick="test();" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
        //         //    html+= '<tr><td data="'+val.item_spec_set_id+'">'+val.spec_set+'</td></tr>';
        //     });
        //         $("#item_select_result").html(html);
        //     });
        //     }, 500);
        // });
        var timer;

        $("#item_select").keyup(function(event) 
        {
            $("#item_select_result").show();
            $("#item_select_result").html('');

            clearTimeout(timer);
            timer = setTimeout(function() 
            {
                var search_item = $("#item_select").val();
                var html = '';
                $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                    data = JSON.parse(data);
                    $.each(data, function(index, val) {
                        if(data['item_image'] == null){
                            val.item_spec_set_image    = "images/item_images/no_image.png";
                        }
                        var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                        html+= '<tr><td data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                    });
                    $("#item_select_result").html(html);
                });
            }, 500);
        });
        $("#item_select_result").on('click', 'td', function(event) {
            $('input[name="items_name"]').val($(this).text());
            $('input[name="item_spec_set_id"]').val($(this).attr('data'));
            $("#item_select_result").hide();
        });
        $('#sell_table2').on('click', '.add_item_match', function(event) 
        {
            var result = $("#item_select").val();
            if(result=='')
            {
                alert('<?php echo $this->lang->line('pic_please'); ?>');
                return false;
            }
            $(this).parent().parent().find("th:nth-child(5)").html(result);
            render_transfer_data['item_info'].push({
                'transfered_item_spec' : $(this).parent().parent().find("th:nth-child(1)").html(),
                'quantity' : $(this).parent().parent().find("th:nth-child(2)").html(),
                'buying_price' : $(this).parent().parent().find("th:nth-child(3)").html(),
                'selling_price' : $(this).parent().parent().find("th:nth-child(4)").html(),
                'matched_item_spec' : $(this).parent().parent().find("th:nth-child(5)").html(),
                'matched_item_spec_id' : $('input[name="item_spec_set_id"]').val(),
            });
            $("#item_select").val('');
        });
        $('#sell_table').on('click', '.adjust_cashbox', function(event) 
        {
            transfer_details_id = $(this).attr('id').split('_')[2];
            transfer_from_id = $(this).parent().parent().find("th:nth-child(1)").text();
            cash_amount = $(this).parent().parent().find("th:nth-child(4)").text();
            if(confirm("Do you want to adjust Cashbox?"))
            {
                $.post('<?php echo site_url(); ?>/transfer_items/update_cashbox/', {cash_amount: cash_amount,transfer_details_id:transfer_details_id}, function(data, textStatus, xhr) 
                {
                    if(data=="1")
                    {
                        alert("Transfer Cashbox Amount adjusted succesfully and notified");
                        location.reload();
                    }
                });
            }
            else
            {

            }
            
        })

        var details_item_count = 0;
        $('#sell_table').on('click', '.transfer_details', function(event) {
            if (window.navigator.onLine === true) {
                event.preventDefault();
                $('#transferFromId').val('');
                $('#transfer_details_form2').show(400);
                transfer_details_id = $(this).attr('id').split('_')[2];
                total_item_quantity = $(this).parent().parent().find("th:nth-child(2)").text();
                transfer_from_id = $(this).parent().parent().find("th:nth-child(1)").text();
                total_item_retail_price = $(this).parent().parent().find("th:nth-child(3)").text();
                $('#transferFromId').val(transfer_from_id);
                $('#transfer_items_add').html('');
                $('#transfer_details_id').val(transfer_details_id);
                var post_data ={
                    'keyword' : transfer_details_id,
                    'csrf_test_name': "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
                }; 
                $.post('<?php echo site_url() ?>/Transfer_items/get_transfer_items_by_transfer_details',post_data ).done(function(datar) {
                    var parsedData = JSON.parse(datar);
                    console.log(parsedData);
                    var html_data ='';
                    var header_html = '';
                    details_item_count = parsedData.length;
                //alert(parsedData.length);exit;
                for (var i in parsedData) 
                {
                    html_data += '<tr><th>'+parsedData[i].spec_set+'</th><th>'+parsedData[i].quantity+'</th><th>'+parsedData[i].buying_price+'</th><th>'+parsedData[i].selling_price+'</th>';
                    html_data += '<th></th><th><button type="button" id="'+i+'" class="btn btn-xs green add_item_match"> <?php echo $this->lang->line("adjust_item"); ?></button></th></tr>';
                    
                }
                header_html += '<tr>';
                header_html += '<th><?php echo $this->lang->line('item'); ?></th>';
                header_html += '<th><?php echo $this->lang->line('total_quantity'); ?></th>';
                header_html += '<th><?php echo $this->lang->line('selling_price'); ?></th>';
                header_html += '<th>Item on inventory</th>';
                header_html += '<th>Match Item</th>';
                header_html += '</tr>';
                $('#transfer_items_add').html(html_data);
            }).error(function() {
                alert("<?php echo $this->lang->line('sorrry'); ?>!!");
            });
            render_data['item_info'].push({
                'items_id' : $('#item_select').val(),
                'item_name' : $('#item_select option:selected').text(),
                'stores_id' : $('#store_select').val(),
                'stores_name' : $('#item_select option:selected').text(),
                'item_quantity' : $('input[name="item_quantity"]').val(),
                'selling_price' : $('input[name="retail_price"]').val(),
                'discount_type' : $('#dis_type').val(),
            })
            transfer_table_render();
            $('input[name="item_quantity"]').val('');
            $('input[name="selling_price"]').val('');
            $('input[name="retail_price"]').val('');    
        }
        else
        {
            alert("<?php echo $this->lang->line('check_internet'); ?>!!");
        }
    });


        var total_amount = 0;
        var total_net_payable= 0;

        function transfer_table_render () {
            var sell_table_html = '';
            total_amount = 0;
            total_quantity =0;
    //subtotal_quantity =0;
    var subtotal_amount = 0;
    var subtotal_quantity=0;

    $.each(render_data['item_info'], function(index, val) {
        sell_table_html += '<tr id="'+index+'">';

        sell_table_html += '<td>'+val.item_name+'</td>';
        var quantity = parseInt(val.item_quantity);
        subtotal_quantity+=quantity;
        sell_table_html += '<td>'+quantity+'</td>';
        
        sell_table_html += '<td>'+val.selling_price+'</td>';
        var total= val.item_quantity*val.selling_price;
        sell_table_html += '<td>'+total+'</td>';
        subtotal_amount+= total;
        sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a>';
        sell_table_html += '</td>';
        sell_table_html += '</tr>';
        total_amount = subtotal_amount;
        total_quantity = subtotal_quantity;
    });
    render_data['all_basic_data']['net_payable']=total_amount;
    render_data['all_basic_data']['due']=total_amount;
    $('#total_amount').html(total_amount);
    $('#total_quantity').html(total_quantity);
    $('#total_net_payable').html(total_amount);
    $('#total_due').html(total_amount);
    $('#total_new_amount').html(total_amount);
}

$('#sell_table2').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    transfer_table_render();
});
$('#save_transfer').click(function(event) {
    event.preventDefault();
    $('#responsive_modal_delete').modal('show');
    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        $('#responsive_modal_delete').modal('hide');
        var transferId = $('#transfer_details_id').val();
        var transferFromId = $('#transferFromId').val();
        render_transfer_data.all_basic_data.transfer_details_id = transferId;
        render_transfer_data.all_basic_data.total_quantity = $('#totalQuantity').html();
        render_transfer_data.all_basic_data.total_amount = $('#totalAmount').html();
        render_transfer_data.all_basic_data.transfer_from_id = transferFromId;
        if(render_transfer_data['item_info'].length==0)
        {
            alert('<?php echo $this->lang->line('pic_item'); ?>');
            return false;s
        }
        else if(render_transfer_data['item_info'].length != details_item_count)
        {
            alert('Please adjust each item individually and then proceed to save button');
            return false;
        }
        var all_data= {
            'csrf_test_name' :$('input[name=csrf_test_name]').val(),
            'all_data' : render_transfer_data,
        }
        $.post('<?php echo site_url() ?>/Transfer_items/confirm_receive_transfer',all_data ).done(function(data, textStatus, xhr) {
            if(data == 'success')
            {
                alert ("<?php echo $this->lang->line('finish'); ?>");
                $("#sell_table2 > tbody").html("");
                $("#sell_table > thead").html("");
                $("#transfer_items_add").html("");
                $('#transfer_details_id').val('');
            //window.location.href = '<?php echo site_url()?>/Transfer_items/receivable_transfer_details';
            window.location = "<?php echo site_url();?>/Transfer_items/receivable_transfer_details";
            return false;
        }
    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>");
    });
});
});

$('#item_select').change(function(event) 
{
    var items_id = $(this).val();
    var post_data ={
        'items_id' : $(this).val(),
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+items_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        $('input[name="retail_price"]').val(data2.retail_price);
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});  

$('#item_quantity').keyup(function(event) {
    var items_id = $('#item_select').val();
    var item_new_quantity = parseInt($(this).val());
    var post_data ={
        'items_id' : items_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/Transfer_items/check_items_on_inventory/'+items_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        if(item_new_quantity>data2.quantity)
        {
            alert('Not enough items on inventory');
            exit;
        }
        else
        {
            var per_item_price = $('#selling_price').val();
            $('#total_price').val(item_new_quantity*per_item_price);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});


</script>
