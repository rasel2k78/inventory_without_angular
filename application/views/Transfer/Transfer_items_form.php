<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">

<div class="row">
    <div class="col-md-7">
        <div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_item_price'); ?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_appropriate_amount'); ?></div>
        <div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('give_item_quntity'); ?></div>
        <div class="alert alert-danger" id="item_price_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('item_invalid_price'); ?></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>
            </div>
            <div class="portlet-body form" id="size_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'size_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('shop_in'); ?>*</label>
                            <div class="fa-item col-md-3 col-sm-4 input-form input-icon">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('your_shop_tt'); ?>"></i>
                                <input type="text" class="form-control" name="stores_from" value="<?php echo $shop_info['name'];?>" readonly>
                                <input type="hidden" name="store_form_id" value="<?php echo $shop_info['shop_id'];?>">
                            </div>
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('shop_to'); ?></label>
                            <div class="fa-item col-md-3 col-sm-4 input-form input-icon">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('shop_to_tt'); ?>"></i>
                                <select class="form-control" name="stores_id" id="store_select" data-live-search="true"><?php echo $this->lang->line('select_shop'); ?>
                                    <option style="color:#CB5A5E" value="">
                                        <?php echo $this->lang->line('select_shop'); ?>
                                    </option>
                                    <?php 
                                    foreach ($other_shops as $key => $shops) {
                                    //echo $valdd->shop_id;exit;
                                        ?><option  value="<?php echo $shops->shop_id?>"><?php echo $shops->name?></option><?php	                            
                                    }
                                    ?>
                                    <!-- <span class="help-block" style="display:none"><?php echo $this->lang->line('select_shop'); ?></span> -->

                                </select>

                                <span id="store_select_required" class="help-block" style="display:none;color:#CB5A5E"><?php echo $this->lang->line('select_shop'); ?></span>



                                <!-- <input type="text" autocomplete="off" name="store_name" id="store_select" class="form-control" placeholder="<?php echo $this->lang->line('select_shop'); ?>"> -->
                                <!-- <input type="hidden" autocomplete="off" name="stores_id" class="form-control"> -->
                                <table class="table table-condensed table-hover table-bordered clickable" id="store_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>



                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">
                                <?php echo $this->lang->line('select_item'); ?></label>
                                <div class="col-md-9 input-form input-icon">
                            <!--     <select class="form-control" name="item_spec_set_id" id="item_select" data-live-search="true">
                                </select>
                                <span class="help-block" style="display:none"><?php echo $this->lang->line('select_item'); ?></span> -->

                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('transfer_item_tt'); ?>"></i>
                                <input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('select_item'); ?>" id="item_select" class="form-control">
                                <input type="hidden" autocomplete="off" name="item_spec_set_id"  class="form-control">
                                <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>


                            </div>
                        </div>




                        <div class="form-group">
                            <label class="control-label col-md-3">Available Quantity</label>
                            <div class="col-md-9 ">
                               <input type="text" name="available_quantity" class="form-control" placeholder="<?php echo $this->lang->line('total_quantity'); ?>" aria-describedby="numpadButton-btn">
                                <div class="input-group input-form input-icon">
                                   
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('total_quantity'); ?></label>
                            <div class="col-md-9 ">
                                <!-- <input type="text" class="form-control" name="quantity"  placeholder="মোট পরিমাণ" > -->

                                <div class="input-group input-form input-icon">
                                    <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->

                                    <input type="text" id="item_quantity" name="item_quantity" class="form-control" placeholder="<?php echo $this->lang->line('total_quantity'); ?>" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                    </span>
                                </div>
                                <span class="help-block" style="display:none"><?php echo $this->lang->line('item_quantity'); ?></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Buying Price</label>
                            <div class="col-md-9 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text" class="form-control" id="buying_price" name="buying_price"  placeholder="Buying Price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('selling_price_unit'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text" class="form-control" id="selling_price" name="retail_price"  placeholder="<?php echo $this->lang->line('sell_price_single'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('total_price'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text" class="form-control" id="total_price" name="total_price"  placeholder="<?php echo $this->lang->line('sell_price_single'); ?> ">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn green" id="add_item"><?php echo $this->lang->line('add_item'); ?></button>
                        <!-- <button type="reset" class="btn default">বাতিল করুন</button> -->
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>

            <!-- BEGIN EXAMPLE TABLE PORTLET-->

        </div>

        <div class="col-md-5">
            <div class="portlet box green-haze" id="sell_voucher_form2" style="display:none">

                <div class="portlet-title">
                    <div class="caption">
                        <!-- <i style="color:black" class="fa fa-question-circle tooltips" data-container="body" data-placement="top" data-original-title="List of items to be transfered. Press the button for transfer"></i> -->
                        <i class="fa fa-globe"></i> <?php echo $this->lang->line('item_transfer_info'); ?>
                    </div>

                </div>
                <div class="portlet-body">

                    <table id="sell_table2" class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('item_f'); ?></th>
                                <th><?php echo $this->lang->line('total_quantity_f'); ?></th>
                                <th>Buying Price</th>
                                <th><?php echo $this->lang->line('selling_price_f'); ?></th>
                                <th><?php echo $this->lang->line('sub_total_f'); ?></th>
                                <th><?php echo $this->lang->line('delete'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo $this->lang->line('total_quantity_f'); ?> = </th>
                                <th id="total_quantity"></th>
                                <th><?php echo $this->lang->line('total_price'); ?>:</th>
                                <th id="total_amount"></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <!-- <th>মোট বাকি:</th> -->
                                <!-- <th></th> -->
                                <th id="total_due"></th>
                                <th></th>
                                <th><button type="submit" id="save_transfer" class="btn green"><?php echo $this->lang->line('save'); ?></button></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <style>
        /* .table thead tr th {
            font-size: 0px;
            font-weight: 0;
        }
    */    </style>
    <div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">ডিলিট নিশ্চিতকরণঃ <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    আপনি কি জমা / উত্তোলনের এই তথ্য ডিলিট করতে চান ? 
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
            <button type="button" id="delete_confirmation" class="btn blue">Yes</button>
        </div>
    </div>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <!--<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>-->

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->    
    <script>

        var csrf = $("input[name='csrf_test_name']").val();
        function change_form()
        {
            $('#form1').hide();
            $('#form2').show();
        }
        table_brand = $('#sample_2').DataTable();

        var render_data= {
            'item_info' : [],
            'all_basic_data':{},
        };




        $('.form-actions').on('click', '#add_item', function(event) {
            event.preventDefault();
            var error_found=false;
            if($('input[name=item_quantity]').val().trim()=="")
            {
                $('input[name=item_quantity]').closest('.form-group').addClass('has-error');
                $('input[name=item_quantity]').parent().find('.help-block').show();
                error_found=true;
            }
            else
            {
                $('input[name=item_quantity]').closest('.form-group').removeClass('has-error');
                $('input[name=item_quantity]').parent().find('.help-block').hide();
            }
            if($('input[name="item_spec_set_id"]').val()==null)
            {
                $('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
                $('input[name="item_spec_set_id"]').parent().find('.help-block').show();
                error_found=true;
            }
            else
            {
                $('input[name=item_spec_set_id]').closest('.form-group').removeClass('has-error');
                $('input[name=item_spec_set_id]').parent().find('.help-block').hide();
            }
            if($('select[name="stores_id"]').val()=="")
            {
                //alert('sdf');
                $(this).closest('.span').addClass('has-error');
                $('input[name="stores_id"]').parent().find('.help-block').show();
                $('#store_select_required').show();
                error_found=true;
            }
            else
            {
                //$('input[name=stores_id]').closest('.form-group').removeClass('has-error');
                $('#store_select_required').hide();
            }
            if(error_found)
            {
                return false;
            }

            $("#sell_voucher_form2").show();

            var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
            var item_new_quantity = $('input[name="item_quantity"]').val();
            var post_data ={
                'item_spec_set_id' : item_spec_set_id,
                'csrf_test_name' : $('input[name=csrf_test_name]').val(),
            }; 

            $.post('<?php echo site_url();?>/Transfer_items/check_items_on_inventory/'+item_spec_set_id,post_data).done(function(data) {
                var data2 = $.parseJSON(data);
                if(parseInt(item_new_quantity)<parseInt(data2.quantity))
                { 
                    render_data['item_info'].push({
                        'item_spec_set_id' : $('input[name="item_spec_set_id"]').val(),
                        'item_name' : $('input[name="items_name"]').val(),
                        'item_quantity' : $('input[name="item_quantity"]').val(),
                        'buying_price' : $('input[name="buying_price"]').val(),
                        'selling_price' : $('input[name="retail_price"]').val(),
                    })

                    transfer_table_render();

                    $('input[name="item_quantity"]').val('');
                    $('input[name="selling_price"]').val('');
                    $('input[name="buying_price"]').val('');
                    $('input[name="retail_price"]').val('');    
                    $('input[name="total_price"]').val('');    
                    $('input[name="items_name"]').val('');    
                    $('input[name="item_spec_set_id"]').val('');

                }
                else
                {
                    alert('<?php echo $this->lang->line('not_enough_item'); ?>');
                    $('#item_quantity').val('');
                    $('#total_price').val('');
                    render_data.length = 0;
                }
            }).error(function() {
                alert("<?php echo $this->lang->line('not_enough_item'); ?>");                
            });

        });

        var total_amount = 0;
        var total_net_payable= 0;

        function transfer_table_render () {
            var sell_table_html = '';
            total_amount = 0;
            total_quantity =0;
    //subtotal_quantity =0;
    var subtotal_amount = 0;
    var subtotal_quantity=0;

    $.each(render_data['item_info'], function(index, val) {
        sell_table_html += '<tr id="'+index+'">';

        sell_table_html += '<td>'+val.item_name+'</td>';
        var quantity = parseInt(val.item_quantity);
        subtotal_quantity+=quantity;
        sell_table_html += '<td>'+quantity+'</td>';
        sell_table_html += '<td>'+val.buying_price+'</td>';
        sell_table_html += '<td>'+val.selling_price+'</td>';
        var total= val.item_quantity*val.buying_price;
        sell_table_html += '<td>'+total+'</td>';
        subtotal_amount+= total;
        sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a></td>';
        sell_table_html += '</tr>';
        total_amount = subtotal_amount;
        total_quantity = subtotal_quantity;
    });

    render_data['all_basic_data']['net_payable']=total_amount;
    render_data['all_basic_data']['due']=total_amount;


    $('#sell_table2 tbody').html(sell_table_html);
    $('#total_amount').html(total_amount);
    $('#total_quantity').html(total_quantity);
    $('#total_net_payable').html(total_amount);
    $('#total_due').html(total_amount);
    $('#total_new_amount').html(total_amount);

}

$('#sell_table2').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    transfer_table_render();
});




$('#save_transfer').click(function(event) 
{
    if (window.navigator.onLine === true) 
    {
        event.preventDefault();
        render_data.all_basic_data.store_form_name = $('input[name=stores_from]').val();
        render_data.all_basic_data.store_form_id = $('input[name=store_form_id]').val();
        render_data.all_basic_data.total_quantity = $('#total_quantity').html();
        render_data.all_basic_data.total_amount = $('#total_amount').html();
        render_data.all_basic_data.store_to_id = $('select[name=stores_id]').val();
        render_data.all_basic_data.store_to_name = $('input[name=store_name]').val();
        var all_data= {
            'csrf_test_name' :$('input[name=csrf_test_name]').val(),
            'all_data' : render_data,
        }
        $.post('<?php echo site_url() ?>/Transfer_items/save_transfer_info',all_data ).done(function(data, textStatus, xhr) {
            var result =JSON.parse(data);;
            //console.log(result);exit;
            if (result== "success") 
            {
                // console.log(<?php echo $this->session->userdata('transfered');?>);
                // alert(<?php echo $this->session->userdata('transfered');?>);

                //exit;
                alert ("<?php echo $this->lang->line('finish_transfer'); ?>");
                render_data['item_info'].length = 0;
                all_data.length = 0;
                $("#sell_table2 tbody").html("");
                $("#total_quantity").html('');
                $("#total_amount").html('');
                $("#total_due").html('');
                $('input[name=items_name]').val('');
                $('input[name=item_spec_set_id]').val('');
                $('input[name=store_name]').val('');
                $('select[name=stores_id]').val('');
                //window.location.href = '<?php echo site_url()?>/Transfer_items/transfer_list';

                //window.location = "<?php echo site_url();?>/Transfer_items/transfer_list";
                window.location = "<?php echo site_url();?>/Sync";
                return false;
            }
            else
            {
                var error_array = [];
                $.each(result, function(index, val) {
                    if(val == "PLease Enter Item Quantity"){
                        error_array.push('item_info_empty');
                    }

                    if(val == "Item does not have enough quantity"){
                        error_array.push('item_amount_failure');
                    }
                    if(val == "price not given"){
                        error_array.push('price_amount_failure');
                    }
                    if(val == "price not perfect"){
                        error_array.push('item_price_not_matched');
                    }
                });
                $.each(error_array, function(index, val) {
                    $('#'+val).slideDown();
                    setTimeout( function(){$('#'+val).slideUp()}, 1500 );
                });
            }

        }).error(function() {
            alert("<?php echo $this->lang->line('sorry'); ?>");
        });
    }
    else
    {
        alert("<?php echo $this->lang->line('check_internet'); ?>");
    }
});

$("#store_select").keyup(function(event) 
{
 $("#store_select_result").hide();
 if (window.navigator.onLine === true) {
     var post_data ={
         'shop_id' : <?php echo $shop_info['shop_id']?>,
         'csrf_test_name': "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
     }; 
     var html_data = '';
     $.post('<?php echo site_url() ?>/Transfer_items/get_all_shops',post_data ).done(function(datar) 
     {
         var parsedData = JSON.parse(datar);
         for (var i in parsedData) 
         {
             html_data+='<tr><td data="'+parsedData[i].shop_id+'">'+parsedData[i].shop_name+'</td></tr>';
         }
         $("#store_select_result").show();
         $("#store_select_result").html(html_data);
     });
 }
 else
 {
     alert("<?php echo $this->lang->line('check_internet'); ?>");
 }
});

$("#store_select_result").on('click', 'td', function(event) {
 $('input[name="store_name"]').val($(this).html());
 $('select[name="stores_id"]').val($(this).attr('data'));
 $("#store_select_result").hide();
});

var timer;
$("#item_select").keyup(function(event) 
{
 $("#item_select_result").show();
 $("#item_select_result").html('');

 clearTimeout(timer);
 timer = setTimeout(function() 
 {
     var search_item = $("#item_select").val();
     var html = '';
     $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
        data = JSON.parse(data);
        console.log(data);
        $.each(data, function(index, val) {
            if(data['item_image'] == null){
                val.item_spec_set_image    = "images/item_images/no_image.png";
            }
            var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
            html+= '<tr><td quantity="'+val.quantity+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                //    html+= '<tr><td data="'+val.item_spec_set_id+'">'+val.spec_set+'</td></tr>';
            });
        $("#item_select_result").html(html);
    });
 }, 500);
});

$("#item_select_result").on('click', 'td', function(event) {
 $('input[name="items_name"]').val($(this).text());
 $('input[name="item_spec_set_id"]').val($(this).attr('data'));
 $('input[name="available_quantity"]').val($(this).attr('quantity'));

 $("#item_select_result").hide();
 var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
 var post_data ={
     'item_spec_set_id' : item_spec_set_id,
     'csrf_test_name' : $('input[name=csrf_test_name]').val(),
 }; 
 $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+item_spec_set_id,post_data).done(function(data) {
     var data2 = $.parseJSON(data);
     console.log(data2);
     $('input[name="retail_price"]').val(data2.retail_price);
     $('input[name="buying_price"]').val(data2.buying_price);
 }).error(function() {
     alert("<?php echo $this->lang->line('sorry'); ?>");                
 });
});

$('#item_quantity').keyup(function(event) {

    var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
    var item_new_quantity = parseInt($(this).val());
    var post_data ={
        'item_spec_set_id' : item_spec_set_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/Transfer_items/check_items_on_inventory/'+item_spec_set_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);

        if(parseInt(item_new_quantity) > parseInt(data2.quantity) )
        {
            alert("<?php echo $this->lang->line('not_enough_item'); ?>");
            $('#item_quantity').val('');
            $('#total_price').val('');
        }
        else
        {
            var per_item_price = $('#buying_price').val();
            $('#total_price').val(item_new_quantity*per_item_price);
        }
    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>");                
    });
});
</script>
