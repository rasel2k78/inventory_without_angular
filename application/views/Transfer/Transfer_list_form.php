<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<div class="row">

    <div class="col-md-7">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert">সফলভাবে  সাইজের তথ্য ডিলিট হয়েছে</div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert">দুঃখিত !! সাইজের তথ্য সেভ হয়নি</div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert">সফলভাবে  সাইজের তথ্য সেভ হয়েছে</div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert">সফলভাবে  সাইজের তথ্য আপডেট হয়েছে</div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('transfer_list'); ?>
                </div>
            </div>
            <div class="portlet-body">


                <?php 

                // echo "<pre>";print_r($transfer_list);

                if(!$transfer_list) {
                    echo 'You did not do any transfer to any shop';
                }
                else
                {
                    ?>
                    <table id="sell_table" class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Transfer To</th>
                                <th width="8%"><?php echo $this->lang->line('total_quantity'); ?></th>
                                <th width="8%"><?php echo $this->lang->line('total_price'); ?></th>
                                <th>Transfer Date</th>
                                <th width="8%"><?php echo $this->lang->line('status'); ?></th>
                                <th><?php echo $this->lang->line('details'); ?></th>
                                <th>Updated on</th>

                            </tr>
                            <?php 
                            if($transfer_list) {
                                foreach ($transfer_list as $key => $value) {
                                    ?>
                                    <tr>
                                       <th style="display:none;"><?php echo $value['transfer_to']?></th>
                                       <th><?php echo $value['name']?></th>
                                       <th><?php echo $value['total_item_quantity']?></th>
                                       <th id="totalAmount"><?php echo $value['total_amount']?></th>
                                       <th id="totalAmount"><?php echo date('Y-m-d', strtotime($value['date_created']))?></th>
                                       <th style="color:green"><?php echo $value['status']?></th>



                                       <?php 
                                       if($value['status'] == 'received') 
                                       {
                                        ?>
                                        <th>
                                            <button type="button" id="item__details_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green transfer_items"><?php echo $this->lang->line('item_details'); ?></button>
                                            <button type="button" id="details_id_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green save_transfer"><?php echo $this->lang->line('update'); ?> ?</button>
                                        </th>
                                        <?php
                                    }
                                    else if($value['status'] == 'adjusted')
                                    {
                                        ?>
                                        <th>
                                            <button type="button" id="item__details_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green transfer_items"><?php echo $this->lang->line('item_details'); ?></button>
                                            <button style="color:gold" type="button" id="details_id_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green adjust_cashbox">Finish Transfer</button>
                                        </th>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <th><button type="button" id="item__details_<?php echo $value['transfer_details_id']?>" class="btn btn-xs green transfer_items"><?php echo $this->lang->line('item_details'); ?></button></th>


                                        <?php
                                    }
                                    ?>
                                    <!-- <th id="transfer_details_id" style="hidden"><?php echo $transfer_details['transfer_details_id']?></th> -->
                                    <th><?php echo date('Y-m-d', strtotime($value['date_updated']))?></th>

                                </tr>
                                <?php
                            }
                        }

                        ?>        
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <?php
            }

            ?>



        </div>
    </div>
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
</div>


<div class="col-md-5">
    <div class="portlet box green-haze" id="transfer_items_form" style="display:none">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i> <?php echo $this->lang->line('transfer_items'); ?>
            </div>
        </div>
        <div class="portlet-body">
            <table id="sell_table2" class="table table-condensed">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('item'); ?></th>
                        <th><?php echo $this->lang->line('total_quantity'); ?></th>
                        <th><?php echo $this->lang->line('price'); ?></th>
                    </tr>
                </thead>
                <tbody id="transfer_items">

                </tbody>
            </table>
        </div>

    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460" style="top:20% !important">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_update'); ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('no'); ?></button>
        <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('yes'); ?></button>
    </div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<!--<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>-->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->    
<script>

    var csrf = $("input[name='csrf_test_name']").val();
    function change_form()
    {
        $('#form1').hide();
        $('#form2').show();
    }
    table_brand = $('#sample_2').DataTable();

    var render_data= {
        'item_info' : [],
        'all_basic_data':{},
    };

    $('#sell_table').on('click', '.transfer_details', function(event) {
        event.preventDefault();
        $('#transfer_details_form2').show(400);
        transfer_details_id = $(this).attr('id').split('_')[2];
        total_item_quantity = $(this).parent().parent().find("th:nth-child(2)").text();
        total_item_retail_price = $(this).parent().parent().find("th:nth-child(3)").text();

        $('#transfer_details_id').val(transfer_details_id);
        var post_data ={
            'keyword' : transfer_details_id,
            csrf_test_name: "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
        }; 


        $.post('<?php echo site_url() ?>/Transfer_items/get_transfer_items_by_transfer_details',post_data ).done(function(datar) {
            var parsedData = JSON.parse(datar);
                //console.log(parsedData);exit;
                var html_data ='';
                for (var i in parsedData) 
                {

                    html_data += '<tr><td>'+parsedData[i].spec_set+'</td><td>'+parsedData[i].quantity+'</td><td>'+parsedData[i].price+'</td></tr>';
                }
                $('#transfer_items_add').html(html_data);
            }).error(function() {
                alert("<?php echo $this->lang->line('sorry'); ?>!!");
            });


    //alert(total_item_retail_price);exit;

    render_data['item_info'].push({
        'items_id' : $('#item_select').val(),
        'item_name' : $('#item_select option:selected').text(),
        'stores_id' : $('#store_select').val(),
        'stores_name' : $('#item_select option:selected').text(),
        'item_quantity' : $('input[name="item_quantity"]').val(),
        'selling_price' : $('input[name="retail_price"]').val(),
        'discount_type' : $('#dis_type').val(),
    })

    transfer_table_render();
                //  clear the whole form rather than individual        **
                $('#item_select').selectpicker('val','');
                //$('#store_select').selectpicker('val','');
                $('input[name="item_quantity"]').val('');
                $('input[name="selling_price"]').val('');
                $('input[name="retail_price"]').val('');    
            });


    var total_amount = 0;
    var total_net_payable= 0;

    function transfer_table_render () {
        var sell_table_html = '';
        total_amount = 0;
        total_quantity =0;
    //subtotal_quantity =0;
    var subtotal_amount = 0;
    var subtotal_quantity=0;

    $.each(render_data['item_info'], function(index, val) {
        sell_table_html += '<tr id="'+index+'">';

        sell_table_html += '<td>'+val.item_name+'</td>';
        var quantity = parseInt(val.item_quantity);
        subtotal_quantity+=quantity;
        sell_table_html += '<td>'+quantity+'</td>';
        
        sell_table_html += '<td>'+val.selling_price+'</td>';
        var total= val.item_quantity*val.selling_price;
        sell_table_html += '<td>'+total+'</td>';
        subtotal_amount+= total;
        sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a>';
        sell_table_html += '</td>';
        sell_table_html += '</tr>';
        total_amount = subtotal_amount;
        total_quantity = subtotal_quantity;
    });

    render_data['all_basic_data']['net_payable']=total_amount;
    render_data['all_basic_data']['due']=total_amount;


    $('#sell_table2 tbody').html(sell_table_html);
    $('#total_amount').html(total_amount);
    $('#total_quantity').html(total_quantity);
    $('#total_net_payable').html(total_amount);
    $('#total_due').html(total_amount);
    $('#total_new_amount').html(total_amount);

}

$('#sell_table2').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    transfer_table_render();
});


$('#sell_table').on('click', '.adjust_cashbox', function(event) 
{
    transfer_details_id = $(this).attr('id').split('_')[2];
    transfer_from_id = $(this).parent().parent().find("th:nth-child(1)").text();
    cash_amount = $(this).parent().parent().find("th:nth-child(4)").text();
    if(confirm("Do you want to adjust Cashbox?"))
    {
        $.post('<?php echo site_url(); ?>/transfer_items/update_own_cashbox/', {cash_amount: cash_amount,transfer_details_id:transfer_details_id}, function(data, textStatus, xhr) 
        {
            if(data=="1")
            {
                alert("Transfer Finished both side. Want to close now?");
                location.reload();
            }
        });
    }
    else
    {

    }

})

$('.transfer_items').click(function(event) {
    event.preventDefault();
    $("#transfer_items_form").show(400);
    transfer_details_id = $(this).attr('id').split('_')[3];
    var post_data ={
        'transfer_details_id' : transfer_details_id,
        csrf_test_name: "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
    }; 
    $.post('<?php echo site_url() ?>/Transfer_items/transfered_items',post_data ).done(function(data, textStatus, xhr) {
        var parsedData = JSON.parse(data);
        var html_data ='';
        for (var i in parsedData) 
        {
            html_data += '<tr><th>'+parsedData[i].spec_set+'</th><th>'+parsedData[i].quantity+'</th><th>'+parsedData[i].buying_price+'</th>';
        }
        $('#transfer_items').html(html_data);
    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>!!");
    });
});


$('.save_transfer').click(function(event) {
    event.preventDefault();
    var transferId = $(this).attr('id').split('_')[2];
    $('#responsive_modal_delete').modal('show');
    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        $('#responsive_modal_delete').modal('hide');

    //transfer_details_id = $(this).attr('id').split('_')[2];
    
    var post_data ={
        'transfer_details_id' : transferId,
        'total_amount' :  $('#totalAmount').html(),
        csrf_test_name: "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
    }; 
    $.post('<?php echo site_url() ?>/Transfer_items/update_transfer_sent',post_data ).done(function(data, textStatus, xhr) {
        //console.log(data);exit;
        if(data == 'success')
        {
            alert ("<?php echo $this->lang->line('finish_transfer'); ?>");
            $("#sell_table2 > tbody").html("");
            $("#sell_table > thead").html("");
            $("#transfer_items_add").html("");
            $('#transfer_details_id').val('');
            window.location.href = '<?php echo site_url()?>/Transfer_items/transfer_list';
        }
    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>!!");
    });

});
    
});




         // Start of AJAX Booostrap for Vendor selection
         var options = {
           ajax          : {
               url     : '<?php echo site_url(); ?>/Exchange_with_vendors/search_item_by_name',
               type    : 'POST',
               dataType: 'json',
            // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
            // automatically replace it with the value of the search query.
            data    : {
                q: '{{{q}}}',
                csrf_test_name: csrf,
            }
        },
        locale        : {
            emptyTitle: 'পণ্য  নির্বাচন করুন'
        },
        log           : 3,
        preprocessData: function (data) {
            var i, l = data.length, array = [];
            if (l) {
                for (i = 0; i < l; i++) {
                    array.push($.extend(true, data[i], {
                        text : data[i].items_name,
                        value: data[i].items_id,
                        data : {
                            subtext: '',
                            content: "<img height='50px' width='50px' src='<?php echo base_url() ?>"+data[i].item_image+"'>&nbsp;"+data[i].items_name
                        }
                    }));
                }
            }
            return array;
        }
    };
    $('#item_select').selectpicker().ajaxSelectPicker(options);


         // Start of AJAX Booostrap for Vendor selection
         var options = {
           ajax          : {
               url     : '<?php echo site_url(); ?>/Transfer_items/search_store_by_name',
               type    : 'POST',
               dataType: 'json',
            // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
            // automatically replace it with the value of the search query.
            data    : {
                q: '{{{q}}}',
                csrf_test_name: csrf,
            }
        },
        locale        : {
            emptyTitle: 'দোকান  নির্বাচন করুন'
        },
        log           : 3,
        preprocessData: function (data) {
            var i, l = data.length, array = [];
            if (l) {
                for (i = 0; i < l; i++) {
                    array.push($.extend(true, data[i], {
                        text : data[i].name,
                        value: data[i].stores_id,
                        data : {
                            subtext: 'disabled'
                        }
                    }));
                }
                //$('#store_select').attr("disabled", true); 
            }
            return array;
        }

    };
    $('#store_select').selectpicker().ajaxSelectPicker(options);
    $('#store_select').trigger('change');

    $('#item_select').change(function(event) 
    {
    //alert($(this).val());
    var items_id = $(this).val();

    var post_data ={
        'items_id' : $(this).val(),
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+items_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        $('input[name="retail_price"]').val(data2.retail_price);
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});

   // $('#item_select').selectpicker().ajaxSelectPicker(options);
    // $('#item_select').trigger('change');
    // End of AJAX Boostrap for Unit selection    

    $('#item_quantity').keyup(function(event) {
        var items_id = $('#item_select').val();
        var item_new_quantity = parseInt($(this).val());
        var post_data ={
            'items_id' : items_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Transfer_items/check_items_on_inventory/'+items_id,post_data).done(function(data) {
            var data2 = $.parseJSON(data);
            if(item_new_quantity>data2.quantity)
            {
                alert('Not enough items on inventory');
                exit;
            }
            else
            {
                var per_item_price = $('#selling_price').val();
                $('#total_price').val(item_new_quantity*per_item_price);
            }
        }).error(function() {
            alert("An error has occured. pLease try again");                
        });

        
        
        //alert(item_new_quantity*per_item_price);
    });


</script>
