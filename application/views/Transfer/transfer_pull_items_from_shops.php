<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<style type="text/css">
  .loader,
  .loader:before,
  .loader:after {
      border-radius: 50%;
      width: 2.5em;
      height: 2.5em;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      -webkit-animation: load7 1.8s infinite ease-in-out;
      animation: load7 1.8s infinite ease-in-out;
  }
  .loader {
      color: #3B9C96;
      font-size: 10px;
      margin: 80px auto;
      position: absolute;
      text-indent: -9999em;
      -webkit-transform: translateZ(0);
      -ms-transform: translateZ(0);
      transform: translateZ(0);
      -webkit-animation-delay: -0.16s;
      animation-delay: -0.16s;
      z-index: 100;
      margin-top: 6%;
      margin-left: 32%;
  }
  .loader:before,
  .loader:after {
      content: '';
      position: absolute;
      top: 0;
  }
  .loader:before {
      left: -3.5em;
      -webkit-animation-delay: -0.32s;
      animation-delay: -0.32s;
  }
  .loader:after {
      left: 3.5em;
  }
  @-webkit-keyframes load7 {
      0%,
      80%,
      100% {
        box-shadow: 0 2.5em 0 -1.3em;
    }
    40% {
        box-shadow: 0 2.5em 0 0;
    }
}
@keyframes load7 {
  0%,
  80%,
  100% {
    box-shadow: 0 2.5em 0 -1.3em;
}
40% {
    box-shadow: 0 2.5em 0 0;
}
}

#overlay{
    position: absolute;
    left: 0;
    top: 5%;
    bottom: 0;
    right: 0;
    background: #DDDDDD;
    opacity: 0.6;
    filter: alpha(opacity=150);
    height:145%;
} 
</style>
<div class="loader" id="loader" style="display: none;">Loading...</div>
<div class="row">
    <div class="col-md-10">
        <div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_item_price'); ?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_appropriate_amount'); ?></div>
        <div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('give_item_quntity'); ?></div>
        <div class="alert alert-danger" id="item_price_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('item_invalid_price'); ?></div>






        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('pull_form'); ?>
                </div>
            </div>
            <div class="portlet-body form" id="size_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'size_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('shop_in'); ?>*</label>
                        <div class="fa-item col-md-6 col-sm-6 input-form input-icon">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('your_shop_tt'); ?>"></i>
                            <input type="text" class="form-control" name="stores_from" value="<?php echo $shop_info['name'];?>" readonly>
                            <input type="hidden" name="store_form_id" value="<?php echo $shop_info['shop_id'];?>">
                        </div>
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('shop_to'); ?></label>
                        <div class="fa-item col-md-6 col-sm-6 input-form input-icon" style="margin-left: 25%">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('shop_to_tt'); ?>"></i>
                            <select class="form-control" name="stores_id" id="store_select" data-live-search="true">Pull Shop Item's
                                <option style="color:#CB5A5E" value="">
                                    <?php echo $this->lang->line('select_shop'); ?>
                                </option>
                                <?php 
                                foreach ($other_shops as $key => $shops) 
                                {
                                    ?><option  value="<?php echo $shops->shop_id?>"><?php echo $shops->name?></option><?php                            
                                }
                                ?>
                            </select>
                            <span id="store_select_required" class="help-block" style="display:none;color:#CB5A5E"><?php echo $this->lang->line('select_shop'); ?></span>
                            <table class="table table-condensed table-hover table-bordered clickable" id="store_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green" id="add_item"><?php echo $this->lang->line('get_items'); ?></button>
                    <!-- <button type="reset" class="btn default">বাতিল করুন</button> -->
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
    </div>

    <div class="col-md-5">
        <div class="portlet box green-haze" id="sell_voucher_form2" style="display:none">

            <div class="portlet-title">
                <div class="caption">
                    <!-- <i style="color:black" class="fa fa-question-circle tooltips" data-container="body" data-placement="top" data-original-title="List of items to be transfered. Press the button for transfer"></i> -->
                    <i class="fa fa-globe"></i> <?php echo $this->lang->line('item_transfer_info'); ?>
                </div>

            </div>
            <div class="portlet-body">
                <table id="sell_table2" class="table table-condensed">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('item_f'); ?></th>
                            <th><?php echo $this->lang->line('total_quantity_f'); ?></th>
                            <th><?php echo $this->lang->line('selling_price_f'); ?></th>
                            <th><?php echo $this->lang->line('sub_total_f'); ?></th>
                            <th><?php echo $this->lang->line('delete'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('total_quantity_f'); ?> = </th>
                            <th id="total_quantity"></th>
                            <th><?php echo $this->lang->line('total_price'); ?>:</th>
                            <th id="total_amount"></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <!-- <th>মোট বাকি:</th> -->
                            <!-- <th></th> -->
                            <th id="total_due"></th>
                            <th></th>
                            <th><button type="submit" id="save_transfer" class="btn green"><?php echo $this->lang->line('save'); ?></button></th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
</div>

<style>
        /* .table thead tr th {
            font-size: 0px;
            font-weight: 0;
        }
        */    </style>
        <div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">ডিলিট নিশ্চিতকরণঃ <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        আপনি কি জমা / উত্তোলনের এই তথ্য ডিলিট করতে চান ? 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                <button type="button" id="delete_confirmation" class="btn blue">Yes</button>
            </div>
        </div>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <!--<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>-->

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->    
        <script>

            var csrf = $("input[name='csrf_test_name']").val();
            function change_form()
            {
                $('#form1').hide();
                $('#form2').show();
            }
            table_brand = $('#sample_2').DataTable();

            var render_data= {
                'item_info' : [],
                'all_basic_data':{},
            };


            $('.form-actions').on('click', '#add_item', function(event) 
            {
                event.preventDefault();
                var error_found=false;
                var over = '<div id="overlay"></div>';
                $(over).appendTo('body');
                $("#loader").show();
                if($('select[name="stores_id"]').val()=="")
                {
                //alert('sdf');
                $(this).closest('.span').addClass('has-error');
                $('input[name="stores_id"]').parent().find('.help-block').show();
                $('#store_select_required').show();
                error_found=true;
            }
            else
            {
                //$('input[name=stores_id]').closest('.form-group').removeClass('has-error');
                $('#store_select_required').hide();
            }
            if(error_found)
            {
                return false;
            }


            var stores_id = $('select[name=stores_id]').val();
            var post_data ={
                'stores_id' : stores_id,
                'csrf_test_name' : $('input[name=csrf_test_name]').val(),
            };
            $.post('<?php echo site_url();?>/Transfer_items/pull_shop_items_from_server/',post_data).done(function(data) 
            {
                var data2 = $.parseJSON(data);
                if(data2=="success")
                {
                    alert("<?php echo $this->lang->line('received_successfully'); ?>");
                    $('#overlay').remove();
                    $("#loader").hide();
                    return false;
                }

            });
        });


            $("#store_select").keyup(function(event) 
            {
               $("#store_select_result").hide();
               if (window.navigator.onLine === true) {
                   var post_data ={
                       'shop_id' : <?php echo $shop_info['shop_id']?>,
                       'csrf_test_name': "<?php echo $this->input->cookie('csrf_cookie_name'); ?>",
                   }; 
                   var html_data = '';
                   $.post('<?php echo site_url() ?>/Transfer_items/get_all_shops',post_data ).done(function(datar) 
                   {
                       var parsedData = JSON.parse(datar);
                       for (var i in parsedData) 
                       {
                           html_data+='<tr><td data="'+parsedData[i].shop_id+'">'+parsedData[i].shop_name+'</td></tr>';
                       }
                       $("#store_select_result").show();
                       $("#store_select_result").html(html_data);
                   });
               }
               else
               {
                   alert("<?php echo $this->lang->line('check_internet'); ?>");
               }
           });

            $("#store_select_result").on('click', 'td', function(event) {
               $('input[name="store_name"]').val($(this).html());
               $('select[name="stores_id"]').val($(this).attr('data'));
               $("#store_select_result").hide();
           });





         // // Start of AJAX Booostrap for Vendor selection
         // var options = {
         //     ajax          : {
         //         url     : '<?php echo site_url(); ?>/Transfer_items/search_store_by_name',
         //         type    : 'POST',
         //         dataType: 'json',
   //          // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
   //          // automatically replace it with the value of the search query.
   //          data    : {
   //              q: '{{{q}}}',
   //              csrf_test_name: csrf,
   //          }
   //      },
   //      locale        : {
   //          emptyTitle: '<?php echo $this->lang->line('select_shop'); ?>'
   //      },
   //      log           : 3,
   //      preprocessData: function (data) {
   //          var i, l = data.length, array = [];
   //          if (l) {
   //              for (i = 0; i < l; i++) {
   //                  array.push($.extend(true, data[i], {
   //                      text : data[i].name,
   //                      value: data[i].stores_id,
   //                      data : {
   //                          subtext: 'disabled'
   //                      }
   //                  }));
   //              }
   //              //$('#store_select').attr("disabled", true); 
   //          }
   //          return array;
   //      }

   //  };
   //  $('#store_select').selectpicker().ajaxSelectPicker(options);
   //  $('#store_select').trigger('change');

   var timer;
   $("#item_select").keyup(function(event) 
   {
       $("#item_select_result").show();
       $("#item_select_result").html('');

       clearTimeout(timer);
       timer = setTimeout(function() 
       {
           var search_item = $("#item_select").val();
           var html = '';
           $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                // console.log(data);
                data = JSON.parse(data);
                // console.log(data);
                $.each(data, function(index, val) {
                    if(data['item_image'] == null){
                        val.item_spec_set_image    = "images/item_images/no_image.png";
                    }
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                    html+= '<tr><td data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                //    html+= '<tr><td data="'+val.item_spec_set_id+'">'+val.spec_set+'</td></tr>';
            });
                $("#item_select_result").html(html);
            });
       }, 500);
   });

   $("#item_select_result").on('click', 'td', function(event) {
       $('input[name="items_name"]').val($(this).text());
       $('input[name="item_spec_set_id"]').val($(this).attr('data'));
       $("#item_select_result").hide();
       var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
       var post_data ={
           'item_spec_set_id' : item_spec_set_id,
           'csrf_test_name' : $('input[name=csrf_test_name]').val(),
       }; 
       $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+item_spec_set_id,post_data).done(function(data) {
           var data2 = $.parseJSON(data);
           $('input[name="retail_price"]').val(data2.retail_price);
       }).error(function() {
           alert("<?php echo $this->lang->line('sorry'); ?>");                
       });
   });



//    $('#item_select').change(function(event) 
//    {
// 	//alert($(this).val());
// 	var item_spec_set_id = $(this).val();

// 	var post_data ={
// 		'item_spec_set_id' : $(this).val(),
// 		'csrf_test_name' : $('input[name=csrf_test_name]').val(),
// 	}; 

// 	$.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+item_spec_set_id,post_data).done(function(data) {
// 		var data2 = $.parseJSON(data);
// 		$('input[name="retail_price"]').val(data2.retail_price);
// 	}).error(function() {
// 		alert("An error has occured. pLease try again");				
// 	});
// });

   // $('#item_select').selectpicker().ajaxSelectPicker(options);
    // $('#item_select').trigger('change');
    // End of AJAX Boostrap for Unit selection    

    $('#item_quantity').keyup(function(event) {

        var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
        var item_new_quantity = parseInt($(this).val());
        var post_data ={
            'item_spec_set_id' : item_spec_set_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        $.post('<?php echo site_url();?>/Transfer_items/check_items_on_inventory/'+item_spec_set_id,post_data).done(function(data) {
            var data2 = $.parseJSON(data);
            
            if(parseInt(item_new_quantity) > parseInt(data2.quantity) )
            {
                alert("<?php echo $this->lang->line('not_enough_item'); ?>");
                $('#item_quantity').val('');
                $('#total_price').val('');
            }
            else
            {
                var per_item_price = $('#selling_price').val();
                $('#total_price').val(item_new_quantity*per_item_price);
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('sorry'); ?>");                
        });
    });
</script>
