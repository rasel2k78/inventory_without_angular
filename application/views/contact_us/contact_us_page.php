<?php
/**
 * Form Name:"Contact Us Page" 
 * 
 * For instant support.
 * 
 * @link   Contact_us/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<html>
<body>
	<h1><?php echo $this->lang->line('help_support');?></h1>
	<h4><?php echo $this->lang->line('help_details');?></h4>
	<ul style="list-style-type:square">
		<li><b><h3><?php echo $this->lang->line('person_one');?></h3></b></li>
		<li><b><h3><?php echo $this->lang->line('person_two');?></h3></b></li>
	</ul>
</body>
</html>