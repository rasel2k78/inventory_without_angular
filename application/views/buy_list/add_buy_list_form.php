<?php
/**
 * Form Name:"All due buy list Page" 
 *
 * Display only buy list which has due payment.
 * 
 * @link   Buy_list/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="col-md-12">
            <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_paymentmade_failed')?></div>
            <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_paymentmade_success')?></div>
            <div class="alert alert-danger" id="no_permission_alert" style="display:none" role="alert"><?php echo $this->lang->line('alert_message_ed')?></div>
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption left">
                        <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('search_vendor')?></span>
                    </div>
                </div>
                <div class="portlet-body form" id="search_vendor">
                    <!-- BEGIN FORM-->
                    <?php $form_attrib = array('id' => 'search_vendor_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('select_vendor')?><sup><i class="fa fa-star custom-required"></i></sup>
                            </label>
                            <div class="col-md-9">
                                <input type="text" autocomplete="off" name="vendors_name" placeholder="<?php echo $this->lang->line('right_select_vendor_placeholder'); ?> " id="vendor_select" class="form-control">
                                <span class="help-block" id="vendor_help_block" ></span>
                                <input type="hidden" name="vendors_id"  class="form-control">
                                <table class="table table-condensed table-hover table-bordered clickable" id="vendor_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn default pull-right" id="search_vendor_cancel" style="margin-left: 8px"><?php echo $this->lang->line('right_cancle_button')?></button>
                        <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('search')?></button>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i><?php echo $this->lang->line('left_portlet_title')?>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                        <thead>
                            <tr>
                                <th>
                                    <?php echo $this->lang->line('left_vendor')?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('left_voucher_no')?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('left_purchase_date')?>
                                </th>
                                <th>
                                    <?php echo $this->lang->line('left_paymentmade')?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo $this->lang->line('left_vendor')?></th>
                                <th><?php echo $this->lang->line('left_voucher_no')?></th>
                                <th>
                                    <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control dont-search" name="from_create_date">
                                        <span class="input-group-addon">
                                            to
                                        </span>
                                        <input type="text" class="form-control dont-search" name="to_create_date">
                                    </div>
                                </th>
                                <th><?php echo $this->lang->line('left_paymentmade')?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <div class="portlet box red" id="bunch_due_payment_form" style="display:none">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-pencil-square-o"></i><span id="form_header_text" ><?php echo $this->lang->line('bunch_payment_title');?></span>
                    </div>
                </div>
                <div class="portlet-body form" id="bunch_due_payment_div" >
                    <!-- BEGIN FORM-->
                    <?php $form_attrib = array('id' => 'bunch_paymentmade_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('vendor_name');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="vendors_name"  readonly="true">
                            <input type="hidden" class="form-control" name="vendors_id"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_net_payable')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bunch_net_payable"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paid')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bunch_paid"  readonly="true"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_due')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bunch_due"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="bunch_payment_type" id="bunch_payment_cash" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="bunch_payment_type" id="bunch_payment_cheque" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="display: none" id="bunch_ac_select_div">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                            <span class="help-block" id="bank_acc_help_block" ></span>
                            <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <div class="form-group" style=" display:none" id="bunch_chk_page_div">
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="cheque_page_num" id="chk_page" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                            <span class="help-block" id="cheque_help_block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paymentmade_amount')?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 input-icon">
                            <input type="number" step="any" min="0" class="form-control" id="bunch_payment_amt" name="bunch_due_paymentmade_amount" placeholder="<?php echo $this->lang->line('right_paymentmade_amount_placeholder')?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_description')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bunch_due_paymentmade_comments" placeholder="<?php echo $this->lang->line('right_description_placeholder')?>">
                        </div>
                        <div>
                            <input type="checkbox" id="adjust_bunch_due"><b><?php echo $this->lang->line('adjust_due_lang')?></b>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button id="bunch_due_paymentmade_cancle" type="button" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('right_cancle_button')?></button>
                        <button type="submit" id="due_paymentmade_submit" class="btn green pull-right"><?php echo $this->lang->line('right_save_button')?></button>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box red" id="pay_due_amount_form" style="display:none">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-pencil-square-o"></i><span id="form_header_text" ><?php echo $this->lang->line('right_portlet_title')?></span>
                    </div>
                </div>
                <div class="portlet-body form" id="buy_due_paymentmade_div" >
                    <!-- BEGIN FORM-->
                    <?php $form_attrib = array('id' => 'paymentmade_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="voucher_no"  readonly="true">
                            <input type="hidden" class="form-control" name="buy_details_id"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_net_payable')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paid')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid"  readonly="true"> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_due')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="due"  readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="optionsRadios4" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="optionsRadios5" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="display: none" id="ac_select_div">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="bank_acc_num_indivi" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select_indivi" class="form-control">
                            <input type="hidden" autocomplete="off" name="bank_acc_id_indivi"  class="form-control">
                            <span class="help-block" id="bank_acc_help_block" ></span>
                            <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result_indivi" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>

                    <div class="form-group" style=" display:none" id="chk_page_div">
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="cheque_page_num_indivi" id="chk_page" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                            <span class="help-block" id="cheque_help_block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paymentmade_amount')?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 input-icon">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('buy_due_paymentmade_tt');?>"></i>
                            <input type="number" step="any" min="0" class="form-control" id="payment_amount_check" name="due_paymentmade_amount" placeholder="<?php echo $this->lang->line('right_paymentmade_amount_placeholder')?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_description')?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="due_paymentmade_comments" placeholder="<?php echo $this->lang->line('right_description_placeholder')?>">
                        </div>
                        <div>
                            <input type="checkbox" id="adjust_single_due"> <b><?php echo $this->lang->line('adjust_due_lang')?></b>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button id="buy_due_paymentmade_cancle" type="button" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('right_cancle_button')?></button>
                        <button type="submit" id="due_paymentmade_submit" class="btn green pull-right"><?php echo $this->lang->line('right_save_button')?></button>
                    </div>
                    <div class="form-group">
                        <table id="table_items" class="table table-hover control-label col-md-3">
                            <thead>
                                <tr >
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_item_name')?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_quantity')?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_purchase_price')?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_sub_total')?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_type');?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_amount');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 3 ) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table= $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/buy_list/all_due_buy_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [2, "desc"]
        ]
    });
    /*data table customize search starts*/
    table.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
// ajax form submit starts....
var buy_details_id;
$('table').on('click', '.pay_due_amount',function(event) {
    event.preventDefault();
    $('#bunch_due_payment_form').hide();
    buy_details_id = $(this).attr('id').split('_')[1];
    $('#current_buy_details_id').remove();
    $.get('<?php echo site_url();?>/buy_list/get_buy_info/'+buy_details_id).done(function(data) {
        var buy_data = $.parseJSON(data);
        if (buy_data== "No Permission"){
            // $('#no_permission_alert').slideDown();
            // setTimeout( function(){$('#no_permission_alert').slideUp()}, 3000 );
            toastr.error('<?php echo $this->lang->line('alert_message_ed')?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else{
            $('#pay_due_amount_form').show(400);
            $('input[name = voucher_no]').val(buy_data.buy_info_by_id[0].voucher_no);
            $('input[name = net_payable]').val(buy_data.buy_info_by_id[0].net_payable);
            $('input[name = paid]').val(buy_data.buy_info_by_id[0].paid);
            $('input[name = due]').val(buy_data.buy_info_by_id[0].due);
            $('input[name = buy_details_id]').val(buy_details_id);
            var table_html = '';
            $.each(buy_data.buy_info_by_id, function(index, val) {
                table_html += '<tr>'
                table_html += '<td>'
                table_html += val.spec_set;
                table_html += '</td><td>';
                table_html += val.quantity;
                table_html += '</td><td>';
                table_html += val.buying_price;
                table_html += '</td><td>';
                table_html += val.sub_total;
                table_html += '</td><td>';
                table_html += val.discount_type;
                table_html += '</td><td>';
                table_html += val.discount_amount;
                table_html += '</td>';
                table_html += '</tr>'
            });
            $('#table_items tbody').html(table_html);
        }
    }).error(function() {
        alert("An error has occurred. Please try again");
    });
});
/* cancle button hide the due paymentmade form*/
$('#buy_due_paymentmade_cancle').click(function(event) {
    event.preventDefault();
    $('#pay_due_amount_form').hide(400);
});
/* cancle button end*/
$('#paymentmade_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    if ($('#adjust_single_due').is(":checked")) {
        data.adjust_due = "adjust";
    }
    else{
        data.adjust_due = "not_adjust";
    }
    data.due_paymentmade_amount = $('input[name=due_paymentmade_amount]').val();
    data.voucher_no = $('input[name=voucher_no]').val();
    data.buy_details_id = $('input[name=buy_details_id]').val();
    data.due_paymentmade_comments = $('input[name=due_paymentmade_comments]').val();
    data.payment_type = $('input[name=payment_type]:checked').val(); 
    data.bank_acc_id = $('input[name=bank_acc_id_indivi]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    data.cheque_number = $('input[name=cheque_page_num_indivi]').val();
    $.post('<?php echo site_url() ?>/buy_list/save_due_paymentmade',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();
            });
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('#pay_due_amount_form').hide(400);
            table.ajax.reload(null, false);
            $('input[name="due_paymentmade_amount"]').val('');
            $('input[name="due_paymentmade_comments"]').val('');
            $('input[name="bank_acc_id_indivi"]').val('');
            $('input[name="cheque_page_num_indivi"]').val('');
            $('input[name="bank_acc_num_indivi"]').val('');
            // $('#insert_success').slideDown();
            // setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            toastr.success('<?php echo $this->lang->line('alert_paymentmade_success')?>', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            $('#adjust_single_due').prop('checked', false);
            $('#adjust_single_due').parent().removeClass('checked');
        }
    }).error(function() {
        // $('#insert_failure').slideDown();
        // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        toastr.error('<?php echo $this->lang->line('alert_paymentmade_failed')?>', 'Failed', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    });    
});
/* Fornt End Check for Larger Payment Made Amount than Due. Later We changed it to Backend Check*/
// $(document).on('keyup', '#payment_amount_check', function(event) {
// 	event.preventDefault();
// 	var input_amount = $(this).val();
// 	$.get('<?php echo site_url() ?>/buy_list/get_buy_info/'+buy_details_id, function(data) {
// 		var buy_data = $.parseJSON(data);
// 		// var due_loan = parseFloat(data_quantity.all_loan_by_id.loan_amount) - parseFloat(data_quantity.all_loan_by_id.total_payout_amount);
// 		if( parseFloat(input_amount) > parseFloat(buy_data.buy_info_by_id[0].due)){
// 			$('#due_paymentmade_submit').prop("disabled",true);
// 			$("input[name='due_paymentmade_amount']").parent().parent().addClass('has-error');
// 			alert ("<?php echo $this->lang->line('alert_more_paid');?>");
// 		}
// 		else {
// 			$('#due_paymentmade_submit').prop("disabled",false);
// 			$("input[name='due_paymentmade_amount']").parent().parent().removeClass('has-error');
// 		}
// 	});
// });	
var timer;
var csrf = $("input[name='csrf_test_name']").val();
$("#vendor_select").keyup(function(event) 
{
    $("#vendor_select_result").show();
    $("#vendor_select_result").html('');
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        var search_vendor = $("#vendor_select").val();
        var html = '';
        $.post('<?php echo site_url(); ?>/buy/search_vendor_by_name',{q: search_vendor,csrf_test_name: csrf}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                if(val.vendor_image == null || val.vendor_image == ''){
                    val.vendor_image = "images/user_images/no_image.png";
                }
                var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.vendor_image+'">';
                html+= '<tr><td data="'+val.vendors_id+'">'+image_html+' '+ val.vendors_name+' -- '+val.vendors_phone_1+'</td></tr>';
            });
            $("#vendor_select_result").html(html);
        });
    }, 500);
});

$("#vendor_select_result").on('click', 'td', function(event) {
    $('input[name="vendors_name"]').val($(this).text());
    $('input[name="vendors_id"]').val($(this).attr('data'));
    $("#vendor_select_result").hide();
});

  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
      $("#bank_acc_select_result").show();
      $("#bank_acc_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
      $('input[name="bank_acc_num"]').val($(this).html());
      $('input[name="bank_acc_id"]').val($(this).attr('data'));
      $("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select_indivi").keyup(function(event) 
  {
      $("#bank_acc_select_result_indivi").show();
      $("#bank_acc_select_result_indivi").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select_indivi").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result_indivi").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result_indivi").on('click', 'td', function(event) {
      $('input[name="bank_acc_num_indivi"]').val($(this).html());
      $('input[name="bank_acc_id_indivi"]').val($(this).attr('data'));
      $("#bank_acc_select_result_indivi").hide();
  });
  //End of AJAX  for Bank Account Selection


  $('#search_vendor_form').submit(function(event) {
     event.preventDefault();
     $('#pay_due_amount_form').hide();
     var vendors_id = $('input[name=vendors_id]').val();
     if(vendors_id == ""){
        alert("<?php echo $this->lang->line('select_vendor_first');?>");
    }
    else{
        $('#bunch_due_payment_form').show();
        var vendors_id = $('input[name=vendors_id]').val();
        $.get('<?php echo site_url();?>/buy_list/get_due_info_by_vendor/'+vendors_id).done(function(data) {
           var due_data = $.parseJSON(data);
           $('input[name = bunch_net_payable]').val(due_data.vendor_due_by_id.net_payable);
           $('input[name = bunch_paid]').val(due_data.vendor_due_by_id.paid);
           $('input[name = bunch_due]').val(due_data.vendor_due_by_id.due);
       }).error(function() {
         // $('#insert_failure').slideDown();
         // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
         toastr.error('<?php echo $this->lang->line('alert_paymentmade_failed')?>', 'Failed', {timeOut: 5000});
         toastr.options = {"positionClass": "toast-top-right"}; 
     });
   }

});

  $('#bunch_paymentmade_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.due_paymentmade_amount = $('input[name=bunch_due_paymentmade_amount]').val();
    data.due_paymentmade_comments = $('input[name=bunch_due_paymentmade_comments]').val();
    data.payment_type = $('input[name=bunch_payment_type]:checked').val(); 
    data.bank_acc_id = $('input[name=bank_acc_id]').val();
    data.cheque_number = $('input[name=cheque_page_num]').val();
    if ($('#adjust_bunch_due').is(":checked")) {
        data.adjust_bunch_due = "adjust";
    }
    else{
        data.adjust_bunch_due = "not_adjust";
    }
    data.vendors_id = $('input[name=vendors_id]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url();?>/buy_list/bunch_payment_by_vendor',data).done (function(data1, textStatus, xhr) {
        var bunch_data = $.parseJSON(data1);
        if(bunch_data.success == 0){
           $.each(bunch_data.error, function(index, val) {
            $('input[name='+index+']').parent().parent().addClass('has-error');
            $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            $('input[name='+index+']').parent().parent().find('.help-block').show();
        });
       }
       else if(bunch_data.success == "yes"){
        table.ajax.reload(null, false);
        // $('#insert_success').slideDown();
        // setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
        toastr.success('<?php echo $this->lang->line('alert_paymentmade_success')?>', 'Success', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"}; 
        $('.form-group.has-error').removeClass('has-error');
        $('.help-block').hide();
        $('#bunch_due_payment_form').hide();
        $('input[name = bunch_net_payable]').val('');
        $('input[name = bunch_paid]').val('');
        $('input[name = bunch_due]').val('');
        $('input[name = vendors_name]').val('');
        $('input[name = vendors_id]').val('');
        $('input[name = bunch_due_paymentmade_amount]').val('');
        $('input[name = bunch_due_paymentmade_comments]').val('');
        $('input[name="bank_acc_id"]').val('');
        $('input[name="cheque_page_num"]').val('');
        $('input[name="bank_acc_num"]').val('');
        $("#bunch_payment_cash").attr('checked', 'checked').click();
        $('#bunch_ac_select_div').hide('fast');
        $('#bunch_chk_page_div').hide('fast');
        $('#adjust_bunch_due').prop('checked', false);
        $('#adjust_bunch_due').parent().removeClass('checked');
    }
    else{
        // $('#insert_failure').slideDown();
        // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        toastr.error('<?php echo $this->lang->line('alert_paymentmade_failed')?>', 'Failed', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    }
}).error(function() {
    // $('#insert_failure').slideDown();
    // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    toastr.error('<?php echo $this->lang->line('alert_paymentmade_failed')?>', 'Failed', {timeOut: 5000});
    toastr.options = {"positionClass": "toast-top-right"}; 
});
});
  $('#bunch_due_payment_form').on('click', '#bunch_due_paymentmade_cancle', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('.form-group.has-error').removeClass('has-error');
    $('.help-block').hide();
    $('#bunch_due_payment_form').hide();
    $('input[name = bunch_net_payable]').val('');
    $('input[name = bunch_paid]').val('');
    $('input[name = bunch_due]').val('');
    $('input[name = vendors_name]').val('');
    $('input[name = vendors_id]').val('');
    $('input[name = bunch_due_paymentmade_amount]').val('');
    $('input[name = bunch_due_paymentmade_comments]').val('');
});
  $('input:radio[name="payment_type"]').change(
    function(){
        if ( $(this).val() == 'cheque' ) {
            $('#chk_page_div').show('fast');
            $('#ac_select_div').show('fast');    
        }    
        if($(this).val() == 'cash'){
            $('#chk_page_div').hide('fast');
            $('#ac_select_div').hide('fast');  
            $('input[name = bank_acc_num]').val('');
            $('input[name = bank_acc_id]').val('');
            $('input[name=cheque_page_num]').val('');
            $('input[name = bank_acc_num_indivi]').val('');
            $('input[name = bank_acc_id_indivi]').val('');
            $('input[name=cheque_page_num_indivi]').val('');
        }
    });
  $('input:radio[name="bunch_payment_type"]').change(
    function(){
        if ( $(this).val() == 'cheque' ) {
            $('#bunch_chk_page_div').show('fast');
            $('#bunch_ac_select_div').show('fast');    
        }    
        if($(this).val() == 'cash'){
            $('#bunch_chk_page_div').hide('fast');
            $('#bunch_ac_select_div').hide('fast');  
            $('input[name = bank_acc_num]').val('');
            $('input[name = bank_acc_id]').val('');
            $('input[name=cheque_page_num]').val('');
            $('input[name = bank_acc_num_indivi]').val('');
            $('input[name = bank_acc_id_indivi]').val('');
            $('input[name=cheque_page_num_indivi]').val('');
        }
    });

// var timer;
// var csrf = $("input[name='csrf_test_name']").val();

</script>