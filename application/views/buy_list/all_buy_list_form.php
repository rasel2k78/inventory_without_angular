<?php
/**
 * Form Name:"All Buy list Page" 
 *
 * Display all buy list including due buy lists.
 * 
 * @link   Buy_list_all/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- Date Range Picker css ends -->
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .lasttablebg { float: right; overflow: hidden; }
    .signaturebg { width: 960px; overflow: hidden; }
    .signaturebg p { 
        border-top: 1px solid #ddd;
        width: 240px;
        padding-bottom: 0;
        line-height: 40px;
        font-size: 16px;
        font-family: arial;
        margin-top: 70px;
        text-align: center;
    }
    .thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
    .thankyousms p { margin: 15px 0 0 0; padding: 0; }
    .thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
    #invoice{border:1px solid cadetblue;padding: 20px;}



    @media print {
        body {
            /* zoom:80%;  */
            height: auto;
            font-size: 12px;
            width: auto;
            margin: 1.6cm;
            /* transform: scale(.5); */
        }


        .header, .hide { visibility: hidden }
        display: none;
        /* zoom:80%; */
        table {page-break-inside: avoid;}
    }
</style>


<div class="row hidden-print">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert">Update Failure. Please Try Again.</div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert">Date Changed Successfully.</div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert">সফলভাবে বাকি টাকা পরিশোধ হল</div>
        <div class="alert alert-danger" id="delete_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_failure');?></div>
        <div class="alert alert-danger" id="delete_failure_for_insuff_qty" style="display:none" role="alert"><?php echo $this->lang->line('delete_failure_for_insuff_qty');?></div>
        <div class="alert alert-danger" id="imei_already_sold" style="display:none" role="alert">Sorry! IMEI/Serial Already Used In Sell. Delete Not Possible</div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('left_details_vendor'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('left_voucher_no'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('left_purchase_date'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('left_details_delete'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('left_details_vendor'); ?></th>
                            <th><?php echo $this->lang->line('left_voucher_no'); ?></th>
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th></th>
                        </tr>
                    </tfoot>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
    <div class="col-md-6">
        <div class="portlet box red" id="buy_voucher_date_change_form" style="display:none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><span id="form_header_text" >Change Date</span>
                </div>
            </div>
            <div class="portlet-body form"  >
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'buy_date_change_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Change Date</label>
                        <div class="col-md-3">
                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="change_purchase_date" placeholder="Change Date" />
                            <input type="hidden" name="buy_details_id" value=""/>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" id="date_cancel" style="margin-left: 8px">Cancel</button>
                    <button type="submit" class="btn green pull-right">Save</button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <div class="col-md-6">


        <div class="portlet box red" id="buy_voucher_details_form" style="display:none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><span id="form_header_text" ><?php echo $this->lang->line('right_portlet_title'); ?></span>
                </div>

            </div>
            <div class="portlet-body form"  >
                <!-- BEGIN FORM-->

                <?php $form_attrib = array('id' => 'all_buy_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="voucher_no"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_landed_cost'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="landed_cost"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_grand_total'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="grand_total"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_discount'); ?> </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="discount"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_net_payable'); ?> </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paid'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_due'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="due"  readonly="true">

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <table id="table_items" class="table table-hover control-label col-md-3">
                        <thead>
                            <tr >
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_item_name'); ?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_quantity'); ?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_purchase_price'); ?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_sub_total'); ?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_type');?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_amount');?></th>
                                <th class="control-label col-md-2">IMEI/ Serial</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <?php 
                if($this->input->cookie('printer_settings')=="thermal_printer")
                {
                    ?>
                    <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#voucher_info" id="buy_voucher_print" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                    <?php
                }
                else if($this->input->cookie('printer_settings')=="normal_printer")
                {
                    ?>
                    <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#" id="print_new" class="btn btn-primary"><i class="fa fa-print"></i> Print </a>
                    <?php
                }
                else
                {
                   ?>
                   <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#" id="print_new" class="btn btn-primary"><i class="fa fa-print"></i> Print </a>
                   <?php
               }
               ?>
<!-- 
    <button style="display:none" type="button" id="buy_voucher_print" class="btn green"><?php echo $this->lang->line('voucher_print');?></button> -->

    <?php echo form_close();?>
    <!-- END FORM-->
</div>
</div>
</div>
</div>
<div class="portlet light" style="display: none" id="voucher_preview">
    <div class="portlet-body" style="padding:30px;">
        <div class="invoice" id="invoice">
        </div>
    </div>
</div>
<!-- Modal view starts -->
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no'); ?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes'); ?></button>
</div>
</div>
<!-- Modal view ends -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Date Range Picker starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Date Range Picker Ends -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    /*lading date picker*/
    jQuery(document).ready(function() {    
        // $('.date-picker').datepicker();
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
        UIExtendedModals.init();
    });
    /*loading date picker ends*/
        // table_buy_list_all = $('#sample_2').DataTable();    
        /*lading date picker*/
        jQuery(document).ready(function() {    
            $('.date-picker').datepicker();
            UIExtendedModals.init();
        });

        
        $('#sample_2 tfoot th').each(function (idx,elm){
            if (idx == 0 || idx == 1 || idx == 3 ) { 
                var title = $('#example tfoot th').eq( $(this).index() ).text();
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            }
        });

        var table_buy_list_all= $('#sample_2').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo site_url(); ?>/buy_list_all/all_buy_info_for_datatable/",
            "lengthMenu": [
            [10, 15, 20,30],
            [10, 15, 20,30] 
            ],
            "pageLength": 10,
            "language": {
                "lengthMenu": " _MENU_ records",
                "paging": {
                    "previous": "Prev",
                    "next": "Next"
                }
            },
            "columnDefs": [{  
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
            [2, "desc"]
            ]
        });    

        /*data table customize search starts*/
        table_buy_list_all.columns().every( function () {
            var that = this;
            $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
                if (!($(this).hasClass('dont-search'))) {
                    that
                    .search( this.value )
                    .draw();
                }
            });

            $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
                var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
                var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

                var final_string = '';
                if(date_from !='' || date_to != ''){
                    if (date_from!='' && date_to=='') {
                        final_string = date_from+'_0';
                    }
                    if (date_to!='' && date_from=='') {
                        final_string = '0_'+date_to;
                    }
                    if (date_from!='' && date_to!='') {
                        final_string = date_from+'_'+date_to;
                    }
                }
                else{
                    final_string = '0_0';
                }
                that.search(final_string).draw();
            });
        });
        /*datatable customize search ends*/

    </script>
    <script type="text/javascript">

// date edit starts for buy
$('table').on('click', '.edit_buy_info', function(event) {
    event.preventDefault();
    var buy_details_id = $(this).attr('id').split('_')[1];
    $('#current_buy_details_id').remove();
    $('#buy_voucher_date_change_form').show();
    $('input[name=buy_details_id]').val(buy_details_id);
});
$('#buy_date_change_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.buy_details_id = $('input[name=buy_details_id]').val();
    data.change_purchase_date = $('input[name=change_purchase_date]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/buy_list_all/change_buy_date_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.success=="no")
        {
            alert("Please Select a Valid Date");
        }
        else
        {
            $('input[name= buy_details_id]').val('');
            $('input[name= change_purchase_date]').val('');
            $('#buy_voucher_date_change_form').hide();
            // $('#insert_success').slideDown();
            // setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            toastr.success('Date Changed Successfully.', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            table_buy_list_all.ajax.reload(null, false);
        }
    }).error(function() {
        // $('#insert_failure').slideDown();
        // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        toastr.error('Update Failure. Please Try Again.', 'Failed', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    }); 
});

$("#print_new").click(function(event) 
{
   $("#voucher_preview").show();
   setTimeout(function(){ window.print(); $("#voucher_preview").hide();}, 500);    
});


$('form').on('click', '#date_cancel', function(event) {
    event.preventDefault();
    $('input[name= buy_details_id]').val('');
    $('input[name= change_purchase_date]').val('');
    $('#buy_voucher_date_change_form').hide();
});
// ajax form view starts....

$('table').on('click', '.buy_voucher_details',function(event) {
    event.preventDefault();
    var buy_details_id = $(this).attr('id').split('_')[1];
    $('#current_buy_details_id').remove();
    $.get('<?php echo site_url();?>/buy_list_all/get_all_buy_info/'+buy_details_id).done(function(data) {
        var buy_data = $.parseJSON(data);
        if(buy_data =="No Permission"){     
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};
        }
        else{
            $('#buy_voucher_details_form').show(400);
            $('input[name = voucher_no]').val(buy_data.all_buy_info_by_id[0].voucher_no);
            $('input[name = net_payable]').val(buy_data.all_buy_info_by_id[0].net_payable);
            $('input[name = grand_total]').val(buy_data.all_buy_info_by_id[0].grand_total);
            $('input[name = discount]').val(buy_data.all_buy_info_by_id[0].discount);
            $('input[name = paid]').val(buy_data.all_buy_info_by_id[0].paid);
            $('input[name = landed_cost]').val(buy_data.all_buy_info_by_id[0].landed_cost);
            $('input[name = due]').val(buy_data.all_buy_info_by_id[0].due);

            $('#buy_voucher_print').val(buy_details_id);
            $('#buy_voucher_print').attr('href', '#get_buy_info@'+buy_details_id);
            var table_html = '';
            $.each(buy_data.all_buy_info_by_id, function(index, val) {
                table_html += '<tr>'
                table_html += '<td>'
                table_html += val.spec_set;
                table_html += '</td><td>';
                table_html += val.quantity;
                table_html += '</td><td>';
                table_html += val.buying_price;
                table_html += '</td><td>';
                table_html += val.sub_total;
                table_html += '</td><td>';
                table_html += val.discount_type;
                table_html += '</td><td>';
                table_html += val.discount_amount;
                table_html += '</td><td>';
                table_html += val.imei_barcode.replace(/\,/g, "<br>");
                table_html += '</td>';
                table_html += '</tr>'
            });
            $('#table_items tbody').html(table_html);

            var invoice_header = '';
            invoice_header+= '<div class="row invoice"><div class="col-xs-12" style="text-align: center;"><div>';
            <?php 
            if(!empty($store_details['shop_image']))
            {
                ?>
                invoice_header+= '<img style="height: 15%; width: 15%" src="<?php echo base_url();?><?php echo $store_details['shop_image']?>" /></div><div>';
                <?php
            }
            ?>
            invoice_header+= '<h3 style="text-align: center;"><?php echo $store_details['name']?></h3>';
            invoice_header+= '<ul class="list-styled" style="list-style:none">';
            invoice_header+= '<li>';
            invoice_header+= '<?php echo $store_details['address']?>';
            invoice_header+= '</li>';
            invoice_header+= '<li>';
            invoice_header+= '<?php echo $store_details['phone']?>';
            invoice_header+= '</li>';
            invoice_header+= '<li>';
            invoice_header+= '<?php echo $store_details['website']?>';
            invoice_header+= '</li>';
            invoice_header+= '<li>';
            invoice_header+= '</li>';
            invoice_header+= '</ul></div></div></div>';
            if (!buy_data.voucher_number.vendors_id) 
            {
                invoice_header+= '<span>Voucher No : '+buy_data.voucher_number.voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a") ?></p><p>Vendor Name : </p><p>Vendor Address : </p></div>';
            }
            else
            {
                invoice_header+= '<span>Voucher No:  '+buy_data.voucher_number.voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a")?></p><p>Vendor Name : '+buy_data.vendor_info.vendors_name+', Phone Number: '+buy_data.vendor_info.vendors_phone_1+'</p><p>Vendor Address : '+buy_data.vendor_info.vendors_present_address+'</p></div>';
            }
            invoice_header+= '</div></div><div class="row"><div class="col-md-12">';
            invoice_header+= '<table class="table table-striped table-bordered table-hover">';
            invoice_header+= '<thead><tr><th>SL.</th><th>Item</th><th class="hidden-480">Discount</th> <th class="hidden-480">Qty</th><th class="hidden-480"> Unit Cost</th><th>Total Price</th></tr></thead><tbody>';
            var i=1;
            // console.log(sell_data.all_sell_info_by_id);
            $.each(buy_data.all_buy_info_by_id,function(index, val)
            {
                invoice_header+= ' <tr><td>'+i+' </td>';
                invoice_header+= '<td>'+val.spec_set+'</td>';
                if(val.discount_type == "percentage"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' %'+ '</td>';
                }
                if(val.discount_type == "amount"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' Tk'+ '</td>';
                }
                if(val.discount_type == "free_quantity"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' pic'+ '</td>';
                }
                if(val.discount_type == ""){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+ '</td>';
                }
                invoice_header+= '<td class="hidden-480">'+val.quantity+'</td>';
                invoice_header+= '<td class="hidden-480">'+val.buying_price+'</td>';
                invoice_header+= '<td class="hidden-480">'+(val.quantity*val.buying_price)+'</td>';
                i++;
            });
            invoice_header+= '</tbody></table></div></div>';
            invoice_header+='<div class="lasttablebg">';
            invoice_header+='<table class="table table-striped table-bordered table-hover" style="float:right"><tbody>';

            invoice_header+='<tr>';
            invoice_header+='<td>Grand Total</td>';
            invoice_header+='<td>'+buy_data.voucher_number.net_payable+'</td>';
            invoice_header+='</tr>';
            invoice_header+='<tr>';
            invoice_header+='<td>Paid</td>';
            invoice_header+='<td>'+buy_data.voucher_number.paid+'</td>';
            invoice_header+='</tr>';
            invoice_header+='<tr>';
            invoice_header+='<td>Due</td>';
            invoice_header+='<td>'+buy_data.voucher_number.due+'</td>';
            invoice_header+='</tr>';
            invoice_header+='</tbody></table></div>';
            invoice_header+= '<div class="row signaturebg"><p> Signature </p> </div><p style="text-align: center">Thank you for shopping with us</p>';
            $("#invoice").html(invoice_header);
        }
    }).error(function() {
        alert("An error has occurred. Please try again");
    });
});


// Ajax Delete 

var buy_details_id = '' ;
var selected_name = '';
$('table').on('click', '.delete_buy_info',function(event) {
    event.preventDefault();
    buy_details_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});


$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
    // var buy_details_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'buy_details_id' : buy_details_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/buy_list_all/delete_buy/'+buy_details_id,post_data).done(function(data) {

        if(data == "No Permission"){
            $('#responsive_modal_delete').modal('hide');
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};
        }
        else if(data == "imei_of_this_voucher_already_sold"){
            $('#responsive_modal_delete').modal('hide');
            toastr.error('Sorry! IMEI/Serial Already Used In Sell. Delete Not Possible', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};
        }
        else if(data == "success")
        {
            $('#responsive_modal_delete').modal('hide');
            toastr.success('<?php echo $this->lang->line('alert_delete_success');?>', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};

            table_buy_list_all.row($('#delete_'+buy_details_id).parents('tr')).remove().draw();
        }

        else if(data == "delete_not_possible_for_insuff_qty"){
            $('#responsive_modal_delete').modal('hide');      
            toastr.error('<?php echo $this->lang->line('delete_failure_for_insuff_qty');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};
        }
        else if(data == "all_items_not_deleted"){
          $('#responsive_modal_delete').modal('hide');      
          toastr.error('Operation Failed Due to Not Deletion Of All Items', 'Failed', {timeOut: 5000});
          toastr.options = {"positionClass": "toast-top-right"};
      }

      else
      {
        $('#responsive_modal_delete').modal('hide');
        toastr.error('<?php echo $this->lang->line('alert_delete_failure');?>', 'Failed', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"};               
    }

}).error(function() {
    alert("An error has occurred. Please try again");                
});
});
</script>