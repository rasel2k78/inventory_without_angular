 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 
 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Buy History Report [Vendor wise] </p>
                <p><strong>Vendor Name:</strong> <?php echo $vendor_data['vendors_name']?> -- <?php echo $vendor_data['vendors_phone_1']?></p>
            </div>
            <div class="col-xs-6">
              <!--   <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p> -->
          </div>
      </div>
      <div class="col-xs-12">
        <table class="table table-striped table-hover table2excel">
            <thead>
                <tr>
                    <th>
                       Item Name
                   </th>
                   <th class="hidden-480">
                    Quantity
                </th>
                <th>
                  Buying Price
              </th>
              <th class="hidden-480">
                Retail Price
            </th>
            <th class="hidden-480">
                Wholesale Price
            </th>
            <th class="hidden-480">
                Date
            </th>
        </tr>
    </thead>
    <tbody id="buy_report_template">
        <?php foreach ($purchase_history as $purchase_value): ?>
            <tr>
                <td>
                    <?php echo $purchase_value['spec_set']?>
                </td>
                <td class="hidden-480">
                    <?php echo $purchase_value['quantity']?>
                </td>
                <td>
                    <?php echo $purchase_value['buying_price']?>
                </td>
                <td class="hidden-480">
                    <?php echo $purchase_value['retail_price']?>
                </td>
                <td class="hidden-480">
                    <?php echo $purchase_value['whole_sale_price']?>
                </td>
                <td class="hidden-480">
                    <?php echo $purchase_value['date_created']?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Total Purchase Amount:</strong> <?php echo nbs(1), $purchase_data['grand_total']?>
            </li>
            <li>
                <strong>Total Discount:</strong>   <?php echo nbs(16), $purchase_data['discount']?>
            </li>
            <li>
                <strong>Total Paid:</strong>   <?php echo nbs(24), $purchase_data['paid']?>
            </li>
            <li>
                <strong>Total Due:</strong>  <?php echo nbs(25), $purchase_data['due']?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "BuyDetailsByVendor_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
