 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
   <div class="portlet-body">
    <div class="invoice">
     <div class="row">
      <div class="col-xs-12">
       <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
       <p style="text-align: center;"><?php echo $store_info['address']?></p>
       <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>

   </div>
   <div class="row">
    <div class="col-xs-12">
      <div class="col-xs-6">
        <p><strong>Report:</strong> Item Average Price Report </p>
      </div>
    </div>
    <div class="col-xs-12">
      <table class="table table-striped table-hover table2excel">
        <thead>
          <tr>
            <th>Item Name</th>
            <th>Available Quantity</th>
            <th>Avg. Buying Price (Unit)</th>
            <th>Total Buying Price</th>
            <th>Avg. Retail Price (Unit)</th>
            <th>Total Retail Price</th>
            <th>Avg. Wholesale Price (Unit)</th>
            <th>Total Wholesale Price</th>
          </tr>
        </thead>
        <tbody id="item_invtory_list_template">
          <?php foreach ($avg_price_data as $v_inv_data): ?>
            <tr>
              <td>
                <?php echo $v_inv_data['item_name']?>
              </td>
              <td>
                <?php echo $v_inv_data['current_quantity']?>
              </td>
              <td>
                <?php echo ($v_inv_data['avg_buy_price'])?>
              </td>
              <td>
                <?php echo ($v_inv_data['total_buy_price'])?>
              </td>
              <td>
                <?php echo ($v_inv_data['avg_sell_price'])?>
              </td>
              <td>
                <?php echo ($v_inv_data['total_sell_price'])?>
              </td>
              <td>
                <?php echo ($v_inv_data['avg_whole_sale_price'])?>
              </td>
              <td>
                <?php echo ($v_inv_data['total_whole_sell_price'])?>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
      <ul class="list-unstyled amounts">
      </ul>
      <br/>
      <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
      </a>
      <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                      </a> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <style type="text/css" media="print">
              @page {
                size: auto;   /* auto is the initial value */
                margin: 0;   /*this affects the margin in the printer settings */
                margin-top: 1cm;
                margin-bottom: .5cm;
              }
            </style>
            <script>
              $(document).on('click', '#btnForExcel', function(event) {
                event.preventDefault();
                $(".table2excel").table2excel({
                  exclude: ".noExl",
                  name: "Excel Document Name",
                  filename: "ItemAvgPrice_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  fileext: ".xls",
                  exclude_img: true,
                  exclude_links: true,
                  exclude_inputs: true
                });
              });
            </script>
