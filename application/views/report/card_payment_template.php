 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 
 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>
       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Card Payment Report </p>
            </div>
            <div class="col-xs-6">
                <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                            Voucher
                        </th>
                        <th>
                            Amount (Taka)
                        </th>
                        <th class="hidden-480">
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody id="dpst_wtdrl_report_template">
                    <?php foreach ($card_payment_info as $val_card_payment_info): ?>
                        <tr>
                           <td>
                            <?php echo $val_card_payment_info['sell_local_voucher_no']?>
                        </td> 
                        <td>
                            <?php echo $val_card_payment_info['amount']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $val_card_payment_info['date']?>
                        </td>
                    </tr>
                <?php endforeach ?>
                <?php foreach ($card_cash_payment as $val_card_cash_payment): ?>
                    <tr>
                       <td>
                        <?php echo $val_card_cash_payment['sell_local_voucher_no']?>
                    </td> 
                    <td>
                        <?php echo $val_card_cash_payment['deposit_withdrawal_amount']?>
                    </td>
                    <td class="hidden-480">
                        <?php echo $val_card_cash_payment['date']?>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Total Amount (Tk):</strong> <?php echo nbs(2),number_format(($total_card_collection['card_amount']+$total_collection_from_cash_card['cash_card_both_amt']),2)?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "cardPayment_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
