 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>
       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Both Customer and Vendor Due Report </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                Receivable [Customer]
                            </th>
                            <th>
                                Amount
                            </th>
                        </tr>
                    </thead>
                    <tbody id="buy_report_template">
                     <tbody id="buy_due_report_template">
                        <?php foreach ($due_details_customer as $v_due_details): ?>
                            <tr>
                                <td>
                                    <?php echo $v_due_details['customer']?>
                                </td>
                                <td>
                                    <?php echo number_format($v_due_details['due'])?> 
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </tbody>
            </table>
        </div>
        <div class="col-xs-6">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            Payable [Vendor]
                        </th>
                        <th>
                            Amount
                        </th>
                    </tr>
                </thead>
                <tbody id="buy_report_template">
                   <?php foreach ($due_details_vendor as $v_due_details): ?>
                    <tr>
                        <td>
                            <?php echo $v_due_details['vendor']?>
                        </td>
                        <td>
                            <?php echo number_format($v_due_details['due'])?> 
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
           <li>
               <strong>Customer Due Amount:</strong> <?php echo nbs(2), number_format($total_due_customer['due'])?>
           </li>
           <li>
            <strong>Vendor Due Amount:</strong> <?php echo nbs(6), number_format($total_due_vendor['due'])?>
        </li>
    </ul>
    <br/>
    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
    <!-- <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button> -->

</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "bothDue_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
