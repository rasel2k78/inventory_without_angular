 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>

 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Sale Items Report [Only For Individual Discount and No Due Transaction]</p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover table2excel">
            <thead>
                <tr>
                    <th>
                     Sales Date
                 </th>
                 <th>
                    Invoice No
                </th>
                <th>
                    Sales Point
                </th>
                <th>
                    Sales Staff
                </th>
                <th>
                    Category
                </th>
                <th>
                    Sub Category
                </th>

                <th>
                    Product
                </th>
                <th>
                    Quantity
                </th>
                <th>
                    Selling Price
                </th>

                <th>
                 Sub Total
             </th>
             <th>
                Discount Amount (Tk)
            </th>
            <th>
                Discount
            </th>
            <th>
                Discount Type
            </th>
            <th>
                Net Payable
            </th>
            <th>
                Paid
            </th>
            <th>
                Due
            </th>
            <th>
                Customer
            </th>
            <th>
                Phone
            </th>
            <th>
                Address
            </th>
<!--             <th>
                IMEI/ Serial
            </th> -->

        </tr>
    </thead>
    <tbody id="item_invtory_list_template">
        <?php foreach ($sell_items_data as $val_sell_details_data): ?>
            <tr>
                <td>
                    <?php echo $val_sell_details_data['date_created']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['sell_local_voucher_no']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['sell_type']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['sales_rep_name']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['parent_name']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['categories_name']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['spec_set']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['quantity']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['selling_price']?>
                </td>
                <td>
                    <?php echo $val_sell_details_data['quantity'] * $val_sell_details_data['selling_price'] ?>
                </td>
                <?php if ($val_sell_details_data['discount_type'] == "amount"): ?> 
                    <td>
                        <?php echo $val_sell_details_data['discount_amount']?>
                    </td>
                <?php endif ?>
                <?php if ($val_sell_details_data['discount_type'] == "percentage"): ?>
                    <?php 
                    $total_amt = ($val_sell_details_data['quantity'] * $val_sell_details_data['selling_price'] * $val_sell_details_data['discount_amount'])/100 ;
                    ?>
                    <td>
                        <?php echo $total_amt?>
                    </td>
                <?php endif ?>
                <?php if ($val_sell_details_data['discount_type'] == "free_quantity"): ?>
                    <td>
                        <?php echo $val_sell_details_data['selling_price'] * $val_sell_details_data['discount_amount']?>
                    </td>
                <?php endif ?>
                <?php if ($val_sell_details_data['discount_type'] == ""): ?>
                    <td>
                      0
                  </td>
              <?php endif ?>
              <th>
                <?php echo $val_sell_details_data['discount_amount']?>
            </th>
            <th>
               <?php echo $val_sell_details_data['discount_type']?>
           </th>
           <td>
               <?php echo $val_sell_details_data['sub_total']?>
           </td>
           <td>
               <?php echo $val_sell_details_data['sub_total']?>
           </td>
           <!-- <td>A</td> -->
           <td>
             0
         </td>
         <td>
            <?php echo $val_sell_details_data['customers_name']?>
        </td>
        <td>
            <?php echo $val_sell_details_data['customers_phone_1']?>
        </td>
        <td>
            <?php echo $val_sell_details_data['customers_present_address']?>
        </td>
<!--         <td>
            <?php echo $val_sell_details_data['imei_barcode']?>
        </td> -->
    </tr>
<?php endforeach ?>
</tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Grand Total:</strong><?php echo nbs(12),($sell_details['grand_total'])?>
            </li>
            <li>
                <strong>Net Payable:</strong><?php echo nbs(12),number_format($sell_details['net_payable'],2)?>
            </li>
            <li>
                <strong>Total Received:</strong><?php echo nbs(8),$sell_details['paid']?>
            </li>
            <li>
                <strong>Total Due:</strong><?php echo nbs(17),$sell_details['due']?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>
    </div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Sell_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
