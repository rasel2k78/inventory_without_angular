 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>
 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Deposit & Withdrawal Report </p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover table2excel">
            <thead>
                <tr>
                    <th>
                        Type
                    </th>
                    <th>
                        Amount
                    </th>
                    <th class="hidden-480">
                        Date
                    </th>
                    <th class="hidden-480">
                        By
                    </th>
                    <th class="hidden-480">
                        Comments
                    </th>
                </tr>
            </thead>
            <tbody id="dpst_wtdrl_report_template">
                <?php foreach ($deposit_wtdrl_data as $v_dpst_wtdrl_data): ?>
                    <tr>
                        <?php if ($v_dpst_wtdrl_data['cash_type']=="deposit"): ?>
                            <td>
                                Deposit
                            </td>
                        <?php endif ?>
                        <?php if ($v_dpst_wtdrl_data['cash_type']=="withdrawal"): ?>
                         <td>
                            Withdrawal
                        </td>
                    <?php endif ?>
                    <td>
                        <?php echo $v_dpst_wtdrl_data['deposit_withdrawal_amount']?>
                    </td>
                    <td class="hidden-480">
                        <?php echo $v_dpst_wtdrl_data['date']?>
                    </td>
                    <td class="hidden-480">
                        <?php echo $v_dpst_wtdrl_data['name']?>
                    </td>
                    <td class="hidden-480">
                        <?php echo $v_dpst_wtdrl_data['cash_description']?>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Total Deposit Amount:</strong> <?php echo nbs(7),$deposit_details['amount']?>
            </li>
            <li>
                <strong>Total Withdrawal Amount:</strong>   <?php echo nbs(2),$withdrawal_details['amount']?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>


    </div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "depositWithdrawal_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
