 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> IMEI/SERIAL Report </p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                            Item Name
                        </th>
                        <th>
                            IMEI / SERIAL Number
                        </th>
                        <th>
                            Vendor
                        </th>
                    </tr>
                </thead>
                <tbody id="item_invtory_list_template">
                    <?php foreach ($available_imei as $val_imei_data): ?>
                        <tr>
                            <td>
                                <?php echo $val_imei_data['spec_set']?>
                            </td>
                            <td>
                                <?php echo $val_imei_data['imei_barcode']?>
                            </td>
                            <td>
                                <?php echo $val_imei_data['vendors_name']?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
        </div>
        <div class="col-xs-8 invoice-block">
            <br/>
            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                Print <i class="fa fa-print"></i>
            </a>
            <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

        </div>
    </div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Imei_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
