 <!-- BEGIN PAGE CONTENT--> 
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>
 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Expense Report </p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover table2excel">
            <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Amount
                    </th>
                    <th class="hidden-480">
                        Date
                    </th>
                    <th class="hidden-480">
                        Details
                    </th>
                </tr>
            </thead>
            <tbody id="buy_report_template">
                <?php foreach ($expense_data as $v_expense_data): ?>
                    <tr>
                        <td>
                            <?php echo $v_expense_data['name']?>
                        </td>
                        <td>
                            <?php echo $v_expense_data['amount']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $v_expense_data['date']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $v_expense_data['details']?>
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr style="font-weight: bold; font-size: 14px">
                    <td>
                        Total
                    </td>
                    <td>
                        <?php echo $expense_details['total_expense']?>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Total Expense:</strong> <?php echo $expense_details['total_expense']?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

    </div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "expense_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
