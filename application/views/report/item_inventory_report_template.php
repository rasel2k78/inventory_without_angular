 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
   <div class="portlet-body">
    <div class="invoice">
     <div class="row">
      <div class="col-xs-12">
       <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
       <p style="text-align: center;"><?php echo $store_info['address']?></p>
       <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>

   </div>
   <div class="row">
    <div class="col-xs-12">
      <div class="col-xs-6">
        <p><strong>Report:</strong> Inventory Report </p>
      </div>
  <!--       <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
          </div> -->
        </div>
        <div class="col-xs-12">
          <table class="table table-striped table-hover table2excel">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Buying Price (Unit)</th>
                <th>Buying Price (Total)</th>
                <th>Retail Price (Unit)</th>
                <th>Retail Price (Total)</th>
                <th>Wholesale Price (Unit)</th>
                <th>Wholesale Price (Total)</th>
              </tr>
            </thead>
            <tbody id="item_invtory_list_template">
              <?php foreach ($item_inventory_data as $v_inv_data): ?>
                <tr>
                  <td>
                    <?php echo $v_inv_data['spec_set']?>
                  </td>
                  <td>
                    <?php echo $v_inv_data['quantity']?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['buying_price'],2)?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['buying_price'] * $v_inv_data['quantity'],2)?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['retail_price'],2)?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['retail_price'] * $v_inv_data['quantity'],2)?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['whole_sale_price'],2)?>
                  </td>
                  <td>
                    <?php echo number_format($v_inv_data['whole_sale_price'] * $v_inv_data['quantity'],2)?>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-4">
        </div>
        <div class="col-xs-8 invoice-block">
          <ul class="list-unstyled amounts">
           <li>
             <strong>Total Quantity:</strong> <?php echo nbs(18), $total_quantity?>
           </li>
           <li>
             <strong>Total Buying Amount:</strong> <?php echo nbs(6), number_format($buying_price,2)?>
           </li>
           <li>
             <strong>Total Retail Amount:</strong> <?php echo nbs(9), number_format($retail_price,2)?>
           </li>
           <li>
             <strong>Total Wholesale Amount:</strong> <?php echo nbs(1), number_format($wholesale_price,2)?>
           </li>
         </ul>
         <br/>
         <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
          Print <i class="fa fa-print"></i>
        </a>
        <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                      </a> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <style type="text/css" media="print">
              @page {
                size: auto;   /* auto is the initial value */
                margin: 0;   /*this affects the margin in the printer settings */
                margin-top: 1cm;
                margin-bottom: .5cm;
              }
            </style>
            <script>
              $(document).on('click', '#btnForExcel', function(event) {
                event.preventDefault();
                $(".table2excel").table2excel({
                  exclude: ".noExl",
                  name: "Excel Document Name",
                  filename: "Item_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  fileext: ".xls",
                  exclude_img: true,
                  exclude_links: true,
                  exclude_inputs: true
                });
              });
            </script>
