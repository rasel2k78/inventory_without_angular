  <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
  <!-- BEGIN PAGE CONTENT-->
  <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Available Item With Total Purchase Amount</p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                            Category
                        </th>
                        <th>
                           Sub Category
                       </th>
                       <th>
                        Item Name
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Amount
                    </th>
                </tr>
            </thead>
            <tbody id="item_invtory_list_template">
                <?php foreach ($item_info as $v_item_info): ?>
                    <tr>
                        <td>
                            <?php echo $v_item_info['parent_category']?>
                        </td>
                        <td>
                            <?php echo $v_item_info['child_category']?>
                        </td>
                        <td>
                            <?php echo $v_item_info['item_name']?>
                        </td>
                        <td>
                            <?php echo $v_item_info['total_buy_qty']?>
                        </td>
                        <td>
                            <?php echo $v_item_info['total_buy_amt']?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
     <ul class="list-unstyled amounts">
        <li>
            <strong>Total Quantity:</strong> <?php echo nbs(2), $total_quantity?><br>
            <strong>Total Amount:</strong> <?php echo nbs(3), $total_amt?><br>
        </li>

    </ul>
    <br/>
    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
    <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>
</div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Item_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>