 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Vendor Payment Report [Date Range]</p>
            </div>
            <div class="col-xs-6">
                <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                            Voucher No
                        </th>
                        <th>
                            Vendor Name
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody >
                    <?php foreach ($payment_data as $v_data): ?>
                        <tr>
                            <td>
                               <?php echo $v_data['voucher_no']?>
                           </td>
                           <?php if ($v_data['vendors_name'] == ""): ?>
                            <td>Empty</td>
                        <?php endif ?>
                        <?php if ($v_data['vendors_name'] != ""): ?>
                            <td>
                                <?php echo $v_data['vendors_name']?>
                            </td>
                        <?php endif ?>
                        <td>
                            <?php echo round($v_data['deposit_withdrawal_amount'], 2)?>
                        </td>
                        <td>
                            <?php echo $v_data['date_created']?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
     <ul class="list-unstyled amounts">
         <li>
             <strong>Total Amount:</strong> <?php echo nbs(2), number_format($total_amount_paid['total_amount'])?>
         </li>
     </ul>
     <br/>
     <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
    <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

</div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "dateRangeVendorPayment_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
