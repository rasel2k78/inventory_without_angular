 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>
 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Transaction Report </p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            Credit Type (+)
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody id="buy_report_template">
                   <tbody id="buy_due_report_template">
                    <?php foreach ($credit_data as $v_credit_arr): ?>
                        <tr>
                            <td>
                                <?php 
                                if($v_credit_arr['type']=="sell")
                                {
                                 echo "Sell";
                             }
                             elseif($v_credit_arr['type']=="sell_due_paymentmade")
                             {
                                echo "Sell Due Paymentmade";
                            }
                            elseif($v_credit_arr['type']=="loan")
                            {
                                echo "Loan";
                            }
                            elseif($v_credit_arr['type']=="deposit")
                            {
                                echo "Deposit";
                            }
                            elseif($v_credit_arr['type']=="commission")
                            {
                                echo "Commission";
                            }
                            ?>
                        </td>
                        <td>
                            <?php echo number_format($v_credit_arr['amount'],2)?> 
                        </td>
                        <td>
                            <?php echo ($v_credit_arr['date_created'])?> 
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-6">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>
                    Debit Type (-)
                </th>
                <th>
                    Amount
                </th>
                <th>
                    Date
                </th>
            </tr>
        </thead>
        <tbody id="buy_report_template">
            <?php foreach ($debit_data as $v_debit_arr): ?>
                <tr>
                    <td>
                       <?php 
                       if($v_debit_arr['type']=="buy")
                       {
                         echo "Buy";
                     }
                     elseif($v_debit_arr['type']=="buy_due_paymentmade")
                     {
                        echo "Buy Due Paymentmade";
                    }
                    elseif($v_debit_arr['type']=="loan_payout")
                    {
                        echo "Loan Payout";
                    }
                    elseif($v_debit_arr['type']=="expense")
                    {
                        echo "Expense";
                    }
                    elseif($v_debit_arr['type']=="withdrawal")
                    {
                        echo "Withdrawal";
                    }
                    elseif($v_debit_arr['type']=="transferred")
                    {
                        echo "Transfer to Bank";
                    }
                    ?>
                </td>
                <td>
                    <?php echo number_format($v_debit_arr['amount'],2)?> 
                </td>
                <td>
                    <?php echo $v_debit_arr['date_created']?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
</div>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
         <li>
             <strong>Total Credit Amount (+):</strong> <?php echo nbs(4), number_format($total_credit_amount,2)?>
         </li>
         <li>
            <strong>Total Debit Amount (-):</strong> <?php echo nbs(6), number_format($total_debit_amount,2)?>
        </li>
    </ul>
    <br/>
    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
    <!-- <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button> -->

</div>
</div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Transaction_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
