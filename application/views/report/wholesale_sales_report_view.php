 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>
 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Wholesale Sales Report</p>
            </div>
            <div class="col-xs-6">
                <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                           Sales Date
                       </th>
                       <th>
                        Invoice No
                    </th>
                    <th>
                        Sales Point
                    </th>
                    <th>
                        Product
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Selling Price
                    </th>


                    <th>
                        Discount Amount (Tk)
                    </th>
                    <th>
                        Discount
                    </th>
                    <th>
                        Discount Type
                    </th>
                    <th>
                       Sub Total
                   </th>
                   <th>
                    Sales Staff
                </th>
                <th>
                    Customer
                </th>
                <th>
                    Phone
                </th>
<!--             <th>
                IMEI/ Serial
            </th> -->
        </tr>
    </thead>
    <tbody id="item_invtory_list_template">
        <?php foreach ($wholesale_sales_data as $val_of_array): ?>
            <tr>
                <td>
                    <?php echo $val_of_array['date_created']?>
                </td>
                <td>
                    <?php echo $val_of_array['sell_local_voucher_no']?>
                </td>
                <td>
                 Wholesale
             </td>
<!--              <td>
                <?php echo $val_of_array['sales_rep_name']?>
            </td> -->
<!--             <td>
                <?php echo $val_of_array['parent_name']?>
            </td>
            <td>
                <?php echo $val_of_array['categories_name']?>
            </td> -->
            <td>
                <?php echo $val_of_array['spec_set']?>
            </td>
            <td>
                <?php echo $val_of_array['quantity']?>
            </td>
            <td>
                <?php echo $val_of_array['selling_price']?>
            </td>
            <?php if ($val_of_array['discount_type'] == "amount"): ?> 
                <td>
                    <?php echo $val_of_array['discount_amount']?>
                </td>
            <?php endif ?>
            <?php if ($val_of_array['discount_type'] == "percentage"): ?>
                <?php 
                $total_amt = ($val_of_array['quantity'] * $val_of_array['selling_price'] * $val_of_array['discount_amount'])/100 ;
                ?>
                <td>
                    <?php echo $total_amt?>
                </td>
            <?php endif ?>
            <?php if ($val_of_array['discount_type'] == "free_quantity"): ?>
                <td>
                    <?php echo $val_of_array['selling_price'] * $val_of_array['discount_amount']?>
                </td>
            <?php endif ?>
            <?php if ($val_of_array['discount_type'] == ""): ?>
                <td>
                  0
              </td>
          <?php endif ?>
          <th>
            <?php echo $val_of_array['discount_amount']?>
        </th>
<!--         <th>
         <?php echo $val_of_array['discount_type']?>
     </th> -->
     <?php if ($val_of_array['discount_type'] == "free_quantity"): ?>
        <th>Free Quantity</th>
    <?php endif ?>
    <?php if ($val_of_array['discount_type'] == "percentage"): ?>
        <th>Percent</th>
    <?php endif ?>
    <?php if ($val_of_array['discount_type'] == "amount"): ?>
        <th>Amount</th>
    <?php endif ?>
    <?php if ($val_of_array['discount_type'] == ""): ?>
        <th></th>
    <?php endif ?>
    <td>
        <?php echo $val_of_array['sub_total']?>
    </td>
    <td>
        <?php echo $val_of_array['sales_rep_name']?>
    </td>
    <td>
        <?php echo $val_of_array['customers_name']?>
    </td>
    <td>
        <?php echo $val_of_array['customers_phone_1']?>
    </td>
<!--         <td>
            <?php echo $val_of_array['imei_barcode']?>
        </td>  -->
    </tr>
<?php endforeach ?>
</tbody>
</table>
</div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
           <li>
               <strong>Total Sub-total:<?php echo nbs(2),number_format($total_subtotal,2)  ?> </strong> <br>
               <strong>Total Discount:<?php echo nbs(2),number_format($total_discount,2)  ?> </strong> 
           </li>
       </ul>
       <br/>
       <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
    <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>
</div>
</div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Wholesale_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
