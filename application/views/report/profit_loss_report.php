 <!-- BEGIN PAGE CONTENT-->
 <script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.table2excel.js"></script>

 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>
       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Profit/Revenue Report </p>
            </div>
            <div class="col-xs-6">
                <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover table2excel">
                <thead>
                    <tr>
                        <th>
                            Credit
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Debit
                        </th>
                        <th>
                            Amount
                        </th>
                    </tr>
                </thead>
                <tbody id="buy_report_template">
                    <tr>
                        <td>
                            Sell 
                        </td>
                        <td>
                            <?php echo number_format($total_sell_amount['paid'], 2)?>
                        </td>
                        <td>
                            Buy
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_buying_cost, 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Commission
                        </td>
                        <td>
                            <?php echo number_format($total_commission, 2)?>

                        </td>
                        <td>
                            Expense
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_expense_amount['expenditures_amount'], 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Bank Charge (Card Payment)
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_card_charge_amt['card_charge_amount'], 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Damage-Lost
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_damage_amount['damage_lost_amount'], 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Landed Cost
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_landed_cost['landed_cost'], 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            VAT
                        </td>
                        <td class="hidden-480">
                            <?php echo number_format($total_vat_amount['vat_amount'], 2)?>
                        </td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                    <tr style="font-weight: bold; font-size: 14px">
                        <td>
                            Total
                        </td>
                        <td><?php echo number_format($total_sell_amount['paid']+$total_commission, 2)?> </td>
                        <td>
                        </td>
                        <td> <?php echo number_format($total_buying_cost+$total_expense_amount['expenditures_amount']+ $total_damage_amount['damage_lost_amount'] + $total_landed_cost['landed_cost']+$total_vat_amount['vat_amount']+$total_card_charge_amt['card_charge_amount'], 2)?></td>
                        <td class="hidden-480">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
        </div>
        <div class="col-xs-8 invoice-block">
            <ul class="list-unstyled amounts">
                <li>
                    <strong>Total Credit:<?php echo nbs(2),number_format($total_credit_amount,2)  ?> </strong> 
                </li>
                <li>
                    <strong>Total Debit:<?php echo nbs(3),number_format($total_debit_amount, 2)?></strong>   
                </li>
                <?php if ($total_debit_amount > $total_credit_amount) : ?>
                    <li>
                        <strong style="color: red">Loss:<?php echo nbs(13),number_format($total_debit_amount-$total_credit_amount, 2)?></strong>   
                    </li>
                <?php else: ?>
                    <li>
                        <strong style="color: green">Revenue:<?php echo nbs(6),number_format($total_credit_amount-$total_debit_amount, 2)?></strong>
                    </li>
                <?php endif ?>
            </ul>
            <br/>
            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                Print <i class="fa fa-print"></i>
            </a>
            <button type="button" id="btnForExcel" class="btn btn-lg yellow">Export To Excel</button>

        </div>
    </div>
</div>
</div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
        margin-top: 1cm;
        margin-bottom: .5cm;
    }
</style>
<script>
    $(document).on('click', '#btnForExcel', function(event) {
        event.preventDefault();
        $(".table2excel").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "profitLoss_" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    });
</script>
