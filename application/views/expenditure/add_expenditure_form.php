<?php
/**
 * Form Name:"Expenditure Form" 
 *
 * This is form for insert , edit and update expenditures informations.
 * 
 * @link   Expenditure/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assest/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/> -->
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="insert_failure_model" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed_modal');?></div>
        <div class="alert alert-success" id="insert_success_model" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success_modal');?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption left">
                    <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
                </div>
            </div>
            <div class="portlet-body form" id="expenditure_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'expenditure_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_exp_type');?> <sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-6 input-icon">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('type_select_tt'); ?>"></i> 
                            <input type="text" autocomplete="off" name="expenditure_types_name" placeholder="<?php echo $this->lang->line('left_exp_type_placeholder');?> " id="expenditure_type_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="expenditure_types_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="expenditure_type_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                        <div class=" col-md-3 col-sm-4">
                            <a href="#expenditure_type_form_modal" title="<?php echo $this->lang->line('left_exp_type_title');?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('pre_date'); ?></label>
                        <div class="col-md-3">
                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="exp_date" placeholder="<?php echo $this->lang->line('left_date');?>" />
                            <span class="help-block"></span>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="cash_btn" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="chk_btn" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="display: none" id="ac_select_div">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                            <span class="help-block" id="bank_acc_help_block" ></span>
                            <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <div class="form-group" style=" display:none" id="chk_page_div">
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="cheque_page_num" id="chk_page" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                            <span class="help-block" id="cheque_help_block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="any" class="form-control" name="expenditures_amount" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="expenditures_comments" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" id="expense_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" id="save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="expenditure_edit_div" style="display:none">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'expenditure_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');
                ?>
                <input type="text" class="form-control" name="expenditures_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_portlet_title');?> 
                        </label>
                        <div class="col-md-9">
                            <select class="form-control" name="expenditure_types_id_edit" id="expenditure_type_select_edit" data-live-search="true">
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="payment_type_edit" id="cash_btn_edit" value="cash"><?php echo $this->lang->line('right_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type_edit" id="chk_btn_edit" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="display: none" id="ac_select_div_edit">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?>
                        </label>
                        <div class="col-md-9">
                            <select class="form-control" name="bank_acc_id_edit" id="bank_acc_num_edit" data-live-search="true">
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group" style=" display:none" id="chk_page_div_edit">
                        <label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="cheque_page_num_edit" id="chk_page_edit" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                            <span class="help-block" id="cheque_help_block_edit"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('pre_date'); ?></label>
                        <div class="col-md-3">
                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="exp_date_edit" placeholder="<?php echo $this->lang->line('left_date');?>" />
                            <span class="help-block"></span>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="expenditures_amount_edit" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="expenditures_comments_edit" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" id="exp_edit_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" id="edit_save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption right">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('right_exp_type');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_amount');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_description');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_date');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_edit_delete');?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('right_exp_type');?></th>
                            <th> <?php echo $this->lang->line('right_amount');?></th>
                            <th><?php echo $this->lang->line('right_description');?></th>
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th><?php echo $this->lang->line('right_edit_delete');?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>
<!-- Expenditure Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="expenditure_type_form_modal" class="modal fade">	
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('expenditure_type/add_expenditure_type_form_only');?>
        </div>
    </div>
</div>
<!-- Expenditure MOdal Form End -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
    /*lading date picker*/
    jQuery(document).ready(function() {    
                // $('.date-picker').datepicker();
                $('.date-picker').datepicker({
                    format: 'yyyy-mm-dd',
                    rtl: Metronic.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
                UIExtendedModals.init();
            });
    /*loading date picker ends*/
    $(document).on('submit', '#expenditure_type_form', function(event) {
        event.preventDefault();
        var data = {};
        data.expenditure_types_name = $('input[name=expenditure_types_name_2]').val();
        data.expenditure_types_description = $('input[name=expenditure_types_description]').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();
        $.post('<?php echo site_url() ?>/expenditure_type/save_expenditure_type_info',data).done(function(data1, textStatus, xhr) {
            var data2 = $.parseJSON(data1);
            if(data2 =="No Permisssion")
            {
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            }
            else if(data2.success==0)
            {
                $.each(data2.error, function(index, val) 
                {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    $('.help-block').show();
                });
            }
            else
            {
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();
                $('#expenditure_type_form_modal').modal('hide');

                $('input[name= expenditure_types_name_2]').val('');
                $('input[name= expenditure_types_description]').val('');
                $('#insert_success_model').slideDown();
                setTimeout( function(){$('#insert_success_model').slideUp();}, 3000 );
            }
        }).error(function() {
            $('#insert_failure_model').slideDown();
            setTimeout( function(){$('#insert_failure_model').slideUp()}, 3000 );
        });    
    });    
</script>
<script>
    /*loading tooltip*/
    jQuery(document).ready(function($) {
        $("[data-toggle='popover']").popover();

    });
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
    });
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 2 || idx == 4) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table_expenditure = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/expenditure/all_expenditure_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [3, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/
    });
    /*data table customize search starts*/
    table_expenditure.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
    $('table').on('click', '.edit_expenditure',function(event) {
        event.preventDefault();
        var expenditure_id = $(this).attr('id').split('_')[1];
        $('#current_expenditure_id').remove();
        $.get('<?php echo site_url();?>/expenditure/get_expenditure_info/'+expenditure_id).done(function(data) {
            var expenditure_data = $.parseJSON(data);
            if(expenditure_data =="No Permission"){
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );            
            }
            else {
                $('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
            // $('select[name = brands_id]').val(item_data.item_info_by_id.brands_id);
            $('select[name = expenditure_types_id_edit]').val(expenditure_data.expenditure_info_by_id.expenditure_types_id);
            $('#expenditure_type_select_edit').html('<option value="'+expenditure_data.expenditure_info_by_id.expenditure_types_id+'" selected="selected">'+expenditure_data.expenditure_info_by_id.expenditure_types_name+'</option>');
            $('#expenditure_type_select_edit').trigger('change');
            $('input[name=payment_type_edit]').attr("disabled",true);
            var payment_type = expenditure_data.expenditure_info_by_id.payment_type;
            if(payment_type == "cash" || payment_type == "" ){
                $("#cash_btn_edit").attr('checked', 'checked').click();
                $('#ac_select_div_edit').hide();
                $('#chk_page_div_edit').hide();
                $('input[name = bank_acc_id_edit]').val('');
                $('#bank_acc_num_edit').val('');
                $('#chk_page_edit').val('');
            }
            else{
                $('#ac_select_div_edit').show();
                $('#chk_page_div_edit').show();
                $("#chk_btn_edit").attr('checked', 'checked').click();
                $('select[name = bank_acc_id_edit]').val(expenditure_data.expenditure_info_by_id.bank_acc_id);
                $('#bank_acc_num_edit').html('<option value="'+expenditure_data.expenditure_info_by_id.bank_acc_id+'" selected="selected">'+expenditure_data.expenditure_info_by_id.bank_acc_num+'</option>');
                $('#bank_acc_num_edit').trigger('change');
                $('input[name = cheque_page_num_edit]').val(expenditure_data.expenditure_info_by_id.cheque_number);
            }
            $('input[name = expenditures_amount_edit]').val(expenditure_data.expenditure_info_by_id.expenditures_amount);
            $('input[name = exp_date_edit]').val(expenditure_data.expenditure_info_by_id.date);
            $('input[name = expenditures_comments_edit]').val(expenditure_data.expenditure_info_by_id.expenditures_comments);
            $('#expenditure_insert_div').slideUp('fast');
            $('#expenditure_edit_div').slideDown('slow');
            $('input[name=expenditures_id]').val(expenditure_id);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
    $('#expenditure_edit_form').submit(function(event) {
        event.preventDefault();
        $('#responsive_modal_edit').modal('show');
    });
    $('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
        event.preventDefault();
        $('#edit_save_id').prop('disabled', true);
        var post_data={};
        post_data.expenditure_types_id_edit = $('select[name = expenditure_types_id_edit]').val();
        post_data.payment_type_edit = $('input[name=payment_type_edit]:checked').val();;
        post_data.bank_acc_id_edit= $('select[name = bank_acc_id_edit]').val();;
        post_data.cheque_page_num_edit = $('input[name = cheque_page_num_edit]').val();;
        post_data.expenditures_comments_edit = $('input[name = expenditures_comments_edit]').val();
        post_data.expenditures_amount_edit = $('input[name = expenditures_amount_edit]').val();
        post_data.expenditures_date_edit = $('input[name = expenditures_date_edit]').val();
        post_data.exp_date_edit = $('input[name = exp_date_edit]').val();
        post_data.expenditures_id = $('input[name=expenditures_id]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
        $.post('<?php echo site_url() ?>/expenditure/update_expenditure_info',post_data).done(function(data, textStatus, xhr) {
            var data2= $.parseJSON(data);
            if(data2['success'])
            {
                $('#update_success').slideDown();
                $('#responsive_modal_edit').modal('hide');
                setTimeout( function(){$('#update_success').slideUp()}, 3000 );
                var row_data = table_expenditure.row( $('#edit_'+post_data.expenditures_id).closest('tr')[0] ).data();
                row_data[0] = $('select[name=expenditure_types_id_edit] option:selected').text();
                table_expenditure.row( $('#edit_'+post_data.expenditures_id).closest('tr')[0] ).data(row_data).draw();
                $('#expenditure_insert_div').slideDown('slow');
                $('#expenditure_edit_div').slideUp('fast');
                $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
                $('.help-block').hide();
                $('input[name = exp_date_edit]').val("");
                $('#edit_save_id').prop('disabled', false);
            }
            else if(!Object.keys(data2.error).length)
            {
                alert("<?php echo $this->lang->line('alert_edit_failed');?>");  
                $('#edit_save_id').prop('disabled', false);              
            }
            else
            {
             $('#edit_save_id').prop('disabled', false);
             $('#responsive_modal_edit').modal('hide');
             $('.help-block').hide();
             $('.form-group.has-error').removeClass('has-error');
             $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();
            });            
         }
     }).error(function() {
       $('#edit_save_id').prop('disabled', false);
       alert("<?php echo $this->lang->line('alert_edit_failed');?>");
   });
 });
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX Booostrap for Expenditure Type Selection
  $("#expenditure_type_select").keyup(function(event) 
  {
      $("#expenditure_type_select_result").show();
      $("#expenditure_type_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachExpendtureType = $("#expenditure_type_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/expenditure/search_expenditure_type_by_name',{q: seachExpendtureType,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              // console.log(data);
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td data="'+val.expenditure_types_id+'">'+val.expenditure_types_name+'</td></tr>';
              });
              $("#expenditure_type_select_result").html(html);
          });
      }, 500);
  });
  $("#expenditure_type_select_result").on('click', 'td', function(event) {
      $('input[name="expenditure_types_name"]').val($(this).html());
      $('input[name="expenditure_types_id"]').val($(this).attr('data'));
      $("#expenditure_type_select_result").hide();
  });
  // var options = {
  //     ajax          : {
  //         url     : '<?php echo site_url(); ?>/expenditure/search_expenditure_type_by_name',
  //         type    : 'POST',
  //         dataType: 'json',
  //           // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
  //           // automatically replace it with the value of the search query.
  //           data    : {
  //               q: '{{{q}}}',
  //               csrf_test_name: csrf,
  //           }
  //       },
  //       locale        : {
  //           emptyTitle: "<?php echo $this->lang->line('left_exp_type_placeholder');?>"
  //       },
  //       log           : 3,
  //       preprocessData: function (data) {
  //           var i, l = data.length, array = [];
  //           if (l) {
  //               for (i = 0; i < l; i++) {
  //                   array.push($.extend(true, data[i], {
  //                       text : data[i].expenditure_types_name,
  //                       value: data[i].expenditure_types_id,
  //                       data : {
  //                           subtext: ''
  //                       }
  //                   }));
  //               }
  //           }
  //           // You must always return a valid array when processing data. The
  //           // data argument passed is a clone and cannot be modified directly.
  //           return array;
  //       }
  //   };
  //   $('#expenditure_type_select').selectpicker().ajaxSelectPicker(options);
  //   $('#expenditure_type_select').trigger('change');
  //   $('#expenditure_type_select_edit').selectpicker().ajaxSelectPicker(options);
  //   $('#expenditure_type_select_edit').trigger('change');
    // End of AJAX Boostrap for Expenditure Type selection
    /* Delete Expenditure Info  Starts*/
    var expenditures_id = '' ;
    var selected_name = '';
    $('table').on('click', '.delete_expenditure',function(event) {
        event.preventDefault();
        expenditures_id = $(this).attr('id').split('_')[1];
        selected_name = $(this).parent().parent().find('td:first').text();
        $('#selected_name').html(selected_name);
    });
    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        // var expenditures_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'expenditures_id' : expenditures_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        $.post('<?php echo site_url();?>/expenditure/delete_expenditure/'+expenditures_id,post_data).done(function(data) {
            if(data == "No Permission"){
                $('#responsive_modal_delete').modal('hide');
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                table_expenditure.row($('#delete_'+expenditures_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('alert_delete_failure');?>");                
        });
    });
// ajax form submit starts....
$('#expenditure_form').submit(function(event) {
    event.preventDefault();
    $('#save_id').prop('disabled', true);
    var data = {};
    data.expenditure_types_id = $('input[name=expenditure_types_id]').val();
    data.expenditures_amount = $('input[name=expenditures_amount]').val();
    data.expenditures_date = $('input[name=expenditures_date]').val();
    data.expenditures_comments = $('input[name=expenditures_comments]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    data.payment_type = $('input[name=payment_type]:checked').val();
    data.bank_acc_id = $('input[name=bank_acc_id]').val();
    data.cheque_page_num = $('input[name="cheque_page_num"]').val();
    data.exp_date =$('input[name="exp_date"]').val();
    $.post('<?php echo site_url() ?>/expenditure/save_expenditure_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 == "No Permission"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            $('#save_id').prop('disabled', false);
        }
        else if(data2.success==0)
        {
            $('.help-block').hide();
            $('#save_id').prop('disabled', false);
            $('.form-group.has-error').removeClass('has-error');
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();
            });
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('#save_id').prop('disabled', false);
            // $('select[name= expenditure_types_id]').val('');
            $("#cash_btn").attr('checked', 'checked').click();
            $('#chk_page_div').hide('fast');
            $('#ac_select_div').hide('fast');    
            $('input[name=bank_acc_num]').val('');
            $('input[name=bank_acc_id]').val('');
            $('input[name=cheque_page_num]').val('');  
            $('input[name= expenditures_amount]').val('');
            $('input[name= expenditure_types_id]').val('');
            $('input[name= expenditures_date]').val('');
            $('input[name= expenditures_comments]').val('');
            $('input[name= exp_date]').val('');
            $('#expenditure_type_select').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table_expenditure.ajax.reload(null, false);
        }
    }).error(function() {
        $('#save_id').prop('disabled', false);
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
    $('#expenditure_type_select').selectpicker('val','');
});
// ajax form submit end.......
$('#expenditure_edit_form').on('click', '#exp_edit_cancel', function(event) {
    event.preventDefault();
    $('#expenditure_insert_div').slideDown('slow');
    $('#expenditure_edit_div').slideUp('fast');
    $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
    /* Act on the event */
});
$('.date-picker').datepicker({
    format: 'yyyy-mm-dd',
    rtl: Metronic.isRTL(),
    orientation: "left",
    autoclose: true
});
$("#expenditure_form").on('click', '#expense_cancel', function(event) {
    event.preventDefault();
    $('#expenditure_type_select').val('');
    $('input[name= expenditure_types_id]').val('');
    $('input[name= expenditures_amount]').val('');
    $('input[name= expenditures_date]').val('');
    $('input[name= expenditures_comments]').val('');
});
$('input:radio[name="payment_type"]').change(
    function(){
        if ( $(this).val() == 'cheque' ) {
            $('#chk_page_div').show('fast');
            $('#ac_select_div').show('fast');    
        }    
        if($(this).val() == 'cash'){
         $('#chk_page_div').hide('fast');
         $('#ac_select_div').hide('fast');    
     }
 });
var timer;
var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
      $("#bank_acc_select_result").show();
      $("#bank_acc_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
      $('input[name="bank_acc_num"]').val($(this).html());
      $('input[name="bank_acc_id"]').val($(this).attr('data'));
      $("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
</script>