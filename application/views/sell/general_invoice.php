	<div class="portlet light">
					<div class="portlet-body">
						<div class="invoice">
							<div class="row">
								<div class="col-xs-12" style="text-align: center;">
								<div >
									<img style="height: 15%; width: 15%" src="<?php echo base_url();?>images/shop_images/shop_logo.png" />
								</div>
								<div>
									<h3>BIGPOINT</h3>
									<ul class="list-unstyled">
										<li>
											Level -5, Block-B, Shop-12-16, Bashundhara City, Panthapat, Dhaka-1215
										</li>
										<li>
											Cell: 01731-97445, 01611-974445, Ext: 205012,205016
										</li>
										<li>
											Email: bigpoint1216@gmail.com
										</li>
										<li>
										</li>
									</ul>
									</div>
								</div>
							</div>
							<div class="row" class="col-xs-12">
									<div class="col-xs-12">
										<div class="col-xs-8">
										<p>No.</p>
										</div>
										<div class="col-xs-4">
										<p>Date: <?php echo date('d-M-Y');?></p>
										</div>
									</div>
									<div class="col-xs-12">
										<div class="col-xs-6">
										<!-- <p>Name : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </p> -->
										<p>Name : <u>Shofik</u> </p>
										</div>
										<div class="col-xs-6">
										<p>Address : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _  _ _ _ _ _ _ _ _</p>
										</div>
									</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<table class="table table-striped table-hover">
									<thead>
									<tr>
										<th>
											 SL.
										</th>
										<th>
											 Item
										</th>
										<th class="hidden-480">
											 Description
										</th>
										<th class="hidden-480">
											 Qty
										</th>
										<th class="hidden-480">
											 Unit Cost
										</th>
										<th>
											 Total Price
										</th>
									</tr>
									</thead>
									<tbody>
								<?php foreach ($item_info as $v_item): ?>
	
									<tr>
										<td>
											 1
										</td>
										<td>
											<?php echo $v_item['item_name']?>
										</td>
										<td class="hidden-480">
											<?php echo $v_item['sell_comments']?>
										</td>
										<td class="hidden-480">
											<?php echo $v_item['quantity']?>
										</td>
										<td class="hidden-480">
											<?php echo $v_item['selling_price']?>
										</td>
										<td>
											<?php echo $v_item['sub_total']?>
										</td>
									</tr>
								<?php endforeach ?>
									</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-8">
								</div>
								<div class="col-xs-4 invoice-block">
									<ul class="list-unstyled amounts">
										<li>
											<strong>Sub-Total:</strong> <?php echo $all_basic_data['net_payable']?>
										</li>
										<li>
											<strong>VAT (<?php echo $all_basic_data['tax_percentage']?>%):</strong> <?php echo $all_basic_data['net_pay_with_vat']?>
										</li>
										<li>
											<strong>Discount:</strong> <?php echo $all_basic_data['discount']?>
										</li>
			
										<li>
											<strong>Grand Total:</strong> <?php echo $all_basic_data['net_pay_after_discount']?>
										</li>
									</ul>
									<br/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
		<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>

<script type="text/javascript">
	window.print();
</script>