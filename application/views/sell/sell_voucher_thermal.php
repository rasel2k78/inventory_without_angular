<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css"/>
  <style type="text/css" media="print">

/*  @media print {
    body {transform: scale(.4);}
    table {page-break-inside: avoid;}
  }*/

  @page 
  {
    size:  auto;   /* auto is the initial value */
    margin: 0mm;  /* this affects the margin in the printer settings */
  }

  html
  {
    background-color: #FFFFFF; 
    margin: 0px;  /* this affects the margin on the html before sending to printer */
  }

  body
  {
     /* border: solid 1px white ;
     margin: 10mm 15mm 10mm 15mm; */
   }

   .invoice-title h2, .invoice-title h3 {
    display: inline-block;
  }

  .table > tbody > tr > .no-line {
    border-top: none;
  }

  .table > thead > tr > .no-line {
    border-bottom: none;
  }

  .table > tbody > tr > .thick-line {
    border-top: 1px solid;
  }
  .verification_section {
    position: absolute;
    left: 0;
    bottom: 20px;
    overflow: hidden;
    width: 100%;
  }
  .verification_section > div {
    float: left;
    width: 40%;
  }
  .verification_section > div p {
    max-width:200px; 
    border-top:1px solid #000;
  }

</style>
</head>
<body style= "font-size: 9px;" >
  <div class="container">
    <div class="row">
      <div class="col-xs-12">

        <div class="row">
          <div style="margin-top: 2mm;" class="col-md-4 col-md-offset-4  text-center">
            <address>
              <img style=" width: 100px; height: 50px;" src="<?php echo base_url().$shop_info['shop_image'];?>"><br><br>
              <strong style= "font-size: 14px;" ><?php echo $shop_info['name']; ?></strong><br>
              <?php echo $shop_info['address']; ?><br>
              <?php echo $shop_info['phone']; ?><br><br>

              <span class="pull-left" style="text-align:left;"> Voucher no. <?php echo $voucher_number['sell_local_voucher_no'];?> </span>
              <span class="pull-right" style="text-align:right;"> Date: <?php echo date("d/m/Y", strtotime($voucher_number['date_created']));?> </span>
              <br>
              <strong><span class="pull-left" style="text-align:left;"> Name: <?php echo $customer_info['customers_name']." (".$customer_info['customers_phone_1'].")";?> </span></strong>
              <strong><span class="pull-right" style="text-align:right;"> Address: <?php echo $customer_info['customers_present_address'];?> </span></strong>


            </address>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-body">
            <div class="table-responsive"  style="position:relative;">
              <table style= "font-size: 9px;" id="ordered_items" class="table table-condensed">
                <thead>
                  <tr >
                    <th  style="text-align:left;">
                      Item
                    </th>
                    <th  style="text-align:right;" > 
                      Rate
                    </th>
                    <th  style="text-align:right;" >
                      Quantity
                    </th>
                    <th  style="text-align:right;" >
                      Disc(%)
                    </th>
                    <th  style="text-align:right;" >
                      Price
                    </th>
                  </tr>
                </thead>
                <tbody>

                </tbody>

              </table>
            </div>

            <div style="text-align: center;" >
              <?php echo $shop_info['policy_plan']; ?><br>
              <?php echo "Thanks for shopping with ". $shop_info['name']; ?>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>

  var total_info = '';
  total_info+='<tr>';
  total_info+='<td class="thick-line text-left"><strong>Subtotal:</strong></td>';
  total_info+='<td class="thick-line"></td>';
  total_info+='<td class="thick-line"></td>';
  total_info+='<td class="thick-line"></td>';
  total_info+='<td id="sub_total_amount" class="thick-line sub_total_amount text-right"><?php echo $voucher_number['sub_total'];?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>VAT:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['vat_amount'];?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>Grand total:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['grand_total']; ?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>Discount:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['discount']; ?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>Net payable:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['net_payable'];?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>Paid:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['paid'];?></td>';
  total_info+='</tr>';

  total_info+='<tr>';
  total_info+='<td class="no-line text-left"><strong>Due:</strong></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line"></td>';
  total_info+='<td class="no-line text-right"><?php echo $voucher_number['due'];?></td>';
  total_info+='</tr>';

  $.get('<?php echo site_url('/');?>SellPosOnline/getSellItemData/<?php echo $id_voucher;?>', function(data) {
    var voucher_item_data = $.parseJSON(data);
    var voucher_data_td = '';
    $.each(voucher_item_data, function(index, val) {
      voucher_data_td+='<tr>';
      voucher_data_td+='<td style="text-align:left;">'+ val.spec_set+'</td>';
      voucher_data_td+='<td style="text-align:right;">'+val.selling_price+'</td>';
      voucher_data_td+='<td style="text-align:right;">'+val.quantity+'</td>';
      voucher_data_td+='<td style="text-align:right;">'+val.discount+'</td>';
      voucher_data_td+='<td style="text-align:right;">'+val.sub_total+'</td>';
      voucher_data_td+='</tr>';
    });

    $(document).ready(function() {
      $('#ordered_items tbody').html(voucher_data_td+total_info);
      window.print();
    // window.history.back();
  });
  });
</script>