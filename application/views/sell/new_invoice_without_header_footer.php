<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
</head>
<style>
	@media print {
		.table_margin_top{
			margin-top: 60px;
		}

		.top_header_box {
			min-height: 100px;
		}
		.subtotalbg table {
			float: right;
			width: 42.8% !important;
		}
		.totalamount table {
			float: right;
			width: 42.8% !important;
		}
		.page_break_css{
			page-break-after: always;
		}
		td.ammountbg {
			width: 240px !important;
			float: left !important;
		}

	}
	.tablebg tr th{
		border-right: 1px solid #000;
		/* font-weight: bold; */
	}
	.fullborder {
		border: 1px solid #000 !important;
	}
	.table td, .table th {
		border-top: 0px;
	}
	table.total_amount {
		text-align: right;
		width: 100%;
		margin-bottom: 15px;
	}
	table.total_amount tr td {
		font-weight: bold;
		font-size: 18px;
	}
	.burimages tr td img {
		width: 200px;
		margin-top: 15px;
	}
	table.burimages tr td {
		width: 300px;
		float: right;
		overflow: hidden;
		margin: 0 auto;
	}
	ul.header_bg {
		margin: 0 0 10px 0;
		padding: 0px;
		float: left;
	}
	ul.header_bg li {
		list-style: none;
		font-weight: 500;
		font-size: 13px;
	}

	/**** New css ***/
	.subtotalbg table {
		float: right;
		width: 37.5%;
		border-bottom: 1px solid #000;
	}
	.table td, .table th {
		border-top: 0px !important;
	}
	.subtotalbg {
		overflow: hidden;
	}
	.totalamount table {
		float: right;
		width: 37.5%;
	}
	span.equal_bg {
		float: right;
	}

</style>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 border">
				<?php if($print_type == "re_print"){ ?>
				<span style="float: right">Copy</span>
				<?php	}?>
				<div class="logo_place text-center" style="height: 100px;">
					
				</div>

				<?php 
				$serial_no = 0;

				foreach ($viewFinalData as $key => $value) { 
					if($value["pageType"] == 'page2' || $value["pageType"] == 'page1') {
						?>

						<!-- Header Start -->
						<p style="font-weight : bold; text-align: center;">Invoice/Bill</p>

						<div class="row">
							<div class="col-md-6">
								<ul class="header_bg">
									<li><strong>Invoice No : <?php echo $voucher_number['sell_local_voucher_no']?></strong></li>
									<li><strong>Customer Name : <?php echo $voucher_number['customers_name']?></strong></li>
									<li><strong>Customer Contact : <?php echo $voucher_number['customers_phone_1']?></strong></li>
									<li><strong>Customer Address : <?php echo $voucher_number['customers_present_address']?></strong></li>
								</ul>
							</div>
							<div class="col-md-4 col-md-offset-2">
								<ul class="header_bg" style="float: right;">
									<li><strong>Date-
										<?php $dateTime = $voucher_number['date_created']; $splitDateTime = explode(" ",$dateTime); echo $splitDateTime[0]?></strong></li>
										<li><strong>Time- 
											<?php $dateTime =  $voucher_number['date_created']; 
											$splitDateTime = explode(" ",$dateTime);
											if (($splitDateTime[1]) == "00:00:00") {
												echo $splitDateTime[1];
											}
											else{
												echo date("g:i a", strtotime($splitDateTime[1]));
											}
											?></strong>
										</li>
										<li><strong>Sales Representative- <?php echo $voucher_number['sales_rep_name']?></strong></li>
										<li><strong>Tracking ID- <?php echo $voucher_number['voucher_unique_barcode']?></strong></li>
									</ul>
								</div>
							</div>
							<!-- Header End -->
							<?php } ?>


							<!-- Product Body Start -->
							<table class="table fullborder tablebg" style="margin: 0px;">
								<thead>
									<tr class="text-center">
										<th style="border-bottom: 1px solid #000; width: 75px; text-align: center;" scope="col">Sl No</th>
										<th style="border-bottom: 1px solid #000; width: 600px;" scope="col">Product Description</th>
										<th style="border-bottom: 1px solid #000; width: 75px; text-align: center;" scope="col">Qty</th>
										<th style="border-bottom: 1px solid #000; width: 120px; text-align: center;" scope="col">Unit Price</th>
										<th style="border-bottom: 1px solid #000; width: 60px; text-align: center;" scope="col">Discount</th>
										<th style="border-bottom: 1px solid #000; width: 70px; text-align: center;" scope="col">Total</th>
									</tr>
								</thead>


								<tbody>
									<?php 
									$count = 0;
									foreach($value["pageData"] as $k => $item) { 
										$count+= 1;
										?>									
										<tr>
											<th class="text-center" style="width: 60px; font-weight: normal; font-size: 14px;">
												<?php echo ++$serial_no; ?>
											</th>
											<td style="border-right: 1px solid #000; padding: 5px 0 5px 5px !important;">
												<p style="margin:0px; padding:0 0 2px 0; font-size: 14px;">
													<?php echo $item['spec_set']; ?>
												</p>
												<p style="font-size: 12px; line-height: 15px; margin: 0;">
													<?php if (!empty($item['imei_barcode'])){ ?>
													<?php echo "IMEI: ".$item['imei_barcode']; 
												} else print "&nbsp;"?></p>

											</td>
											<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right: 1px solid #000; font-size: 14px; font-weight: 500;"><?php echo $item['quantity']; ?> pcs</td>
											<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; font-size: 14px; font-weight: 500;"><?php echo $item['selling_price']; ?></td>
											<?php if ($item['discount_type'] == "amount"): ?> 
												<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; font-size: 14px; font-weight: 500;">
													<?php echo $item['discount_amount']?> Tk
												</td>
											<?php endif ?>
											<?php if ($item['discount_type'] == "percentage"): ?>
												<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; font-size: 14px; font-weight: 500;">
													<?php echo $item['discount_amount']?> %
												</td>
											<?php endif ?>
											<?php if ($item['discount_type'] == "free_quantity"): ?>
												<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; font-size: 14px; font-weight: 500;">
													<?php echo $item['discount_amount']?> pcs
												</td>
											<?php endif ?>
											<?php if ($item['discount_type'] == ""): ?>
												<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; font-size: 14px; font-weight: 500;">
													0
												</td>
											<?php endif ?>
											<td style="border-right: 1px solid #000; text-align: right; font-size: 15px; font-weight: 500;"><?php echo $item['sub_total']; ?></td>																			
										</tr>
										<?php } ?>

										<?php 
										$blank_counter = 0;
										if($value["pageType"] == 'page1'){
											$blank_counter = 7 - $count;
										}
										if($value["pageType"] == 'page2'){
											$blank_counter = 13 - $count;
										}
										if($value["pageType"] == 'page3'){
											$blank_counter = 16 - $count;
										}
										if($value["pageType"] == 'page4'){
											$blank_counter = 10 - $count;
										}
										for($i=0;$i<$blank_counter;$i++){ ?>
										<tr>
											<th class="text-center" style="width: 80px;">
												&nbsp;
											</th>
											<td style="border-right: 1px solid #000; padding: 5px 0 5px 5px !important;">
												<p style="margin:0px; padding:0 0 2px 0; font-size: 15px; font-weight: 500;">&nbsp;
												</p>
												<p style="font-size: 12px; line-height: 15px; margin: 0;">&nbsp; </p>

											</td>
											<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right: 1px solid #000; font-size: 14px; font-weight: 500;">&nbsp;</td>
											<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; width: 130px; font-size: 14px; font-weight: 500;">&nbsp;</td>
											<td class="text-center" style="padding: 5px 0 5px 5px !important; border-right:1px solid #000; width: 130px; font-size: 14px; font-weight: 500;">&nbsp;
											</td>
											<td style="border-right: 1px solid #000; text-align: right; font-size: 14px; font-weight: 500;">&nbsp;</td>																		
										</tr>
										<?php } ?>
									</tbody>
								</table>
								<?php 
								if(count($value["pageData"]) == $count && $value["pageType"] != 'page4' && $value["pageType"] != 'page1'){ 
									print '<div class="page_break_css"></div>';
									print '<div class="logo_place text-center text-uppercase" style="height: 100px;"></div>';
								} ?>
								<!-- Product Body End -->
								<!-- Footer Start -->
								<?php 
								if($value["pageType"] == 'page1' || $value["pageType"] == 'page4') {
									?>
									<div class="subtotalbg">
										<table>
											<tbody>
												<tr>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left;padding: 0 0 0 10px;">Sub-Total</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['sub_total']?></td>
												</tr>
												<tr>
													<?php if (empty($voucher_number['vat_amount'])){ ?>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Vat (0%)</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;">0</td>
													<?php } else{ ?>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Vat (<?php echo $voucher_number['tax_percentage']?>%)</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['vat_amount']?></td>
													<?php } ?>
												</tr>

												<tr>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Grand Total</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['grand_total']?></td>
												</tr>
												<tr>
													<?php if ($voucher_number['discount_percentage'] == "yes"): ?> 									
														<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Dicsount(<?php echo $voucher_number['discount']?>%)</td>
														<?php 
														$total_amount = (($voucher_number['sub_total'] + $voucher_number['vat_amount']) * $voucher_number['discount'])/100 ;
														?>
														<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $total_amount?></td>
													<?php endif ?>
													<?php if ($voucher_number['discount_percentage'] == "no"): ?> 
														<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Dicsount</td>
														<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['discount']?></td>
													<?php endif ?>
												</tr>
												<tr>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Net Payable</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><span id="get_netpayable"><?php echo $voucher_number['net_payable']?></td>
												</tr>
												<tr>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Paid</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['paid']?></td>
												</tr>
												<tr>
													<td colspan="5" style="border-left: 1px solid #000;font-size: 14px; font-weight: bold;text-align: left; padding: 0 0 0 10px;">Due</td>
													<td style="border-right: 1px solid #000;font-size: 14px; font-weight: bold; text-align: right; width: 120px; padding: 0 10px 0 0px;"><?php echo $voucher_number['due']?></td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="totalamount">
										<table class="total_amount">
											<tbody>
												<tr>
													<td class="ammountbg" style="font-weight: 700; padding: 0 10px 0 0; font-size: 14px; width: 360px; text-align: left;">
														Total Amount <span class="equal_bg">:</span>
													</td>
													<td style="font-size: 14px; padding: 0 12px 0px 0;"><?php echo $voucher_number['net_payable']?></td>
												</tr>
											</tbody>
										</table>
									</div>

									<table class="fullborder" style="width: 100%; margin-bottom: 20px;">
										<tbody>
											<tr>
												<td style="font-size: 14px; font-weight: 700; padding: 5px 0 5px 15px;">In Word: <span id="netpayable_in_words"></span></td>
											</tr>
										</tbody>
									</table>

									<table style="width: 100%; margin-bottom: 10px; float: right; padding: 15px; margin-top: 30px;">
										<tbody>
											<tr>
												<td style="width: 20%; float: left; text-align: center; border-top: 1px solid #000; font-weight: 700;">
													Customer Signature
												</td>
												<td style="width: 23%; float: right; text-align: center; border-top: 1px solid #000; font-weight: 700;">
													Authorized Signature
												</td>
											</tr>
										</tbody>
									</table>

									<table class="fullborder" style="width: 100%; margin-bottom: 10px;">
										<tbody>
											<tr>
												<td style="font-size: 14px; font-weight: 700; padding: 5px 0 5px 15px; text-align: left;">Our Policy: <?php echo $store_details['policy_plan']?></td>
											</tr>
										</tbody>
									</table>
								</div>

								<!--Footer End -->
								<?php } ?>
								<?php } ?>
							</div>
						</div>
					</body>
					</html>
					<script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

					<script type="text/javascript">
						function convertNumberToWords(amount) {
							var words = new Array();
							words[0] = '';
							words[1] = 'One';
							words[2] = 'Two';
							words[3] = 'Three';
							words[4] = 'Four';
							words[5] = 'Five';
							words[6] = 'Six';
							words[7] = 'Seven';
							words[8] = 'Eight';
							words[9] = 'Nine';
							words[10] = 'Ten';
							words[11] = 'Eleven';
							words[12] = 'Twelve';
							words[13] = 'Thirteen';
							words[14] = 'Fourteen';
							words[15] = 'Fifteen';
							words[16] = 'Sixteen';
							words[17] = 'Seventeen';
							words[18] = 'Eighteen';
							words[19] = 'Nineteen';
							words[20] = 'Twenty';
							words[30] = 'Thirty';
							words[40] = 'Forty';
							words[50] = 'Fifty';
							words[60] = 'Sixty';
							words[70] = 'Seventy';
							words[80] = 'Eighty';
							words[90] = 'Ninety';
							amount = amount.toString();
							var atemp = amount.split(".");
							var number = atemp[0].split(",").join("");
							var n_length = number.length;
							var words_string = "";
							if (n_length <= 9) {
								var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
								var received_n_array = new Array();
								for (var i = 0; i < n_length; i++) {
									received_n_array[i] = number.substr(i, 1);
								}
								for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
									n_array[i] = received_n_array[j];
								}
								for (var i = 0, j = 1; i < 9; i++, j++) {
									if (i == 0 || i == 2 || i == 4 || i == 7) {
										if (n_array[i] == 1) {
											n_array[j] = 10 + parseInt(n_array[j]);
											n_array[i] = 0;
										}
									}
								}
								value = "";
								for (var i = 0; i < 9; i++) {
									if (i == 0 || i == 2 || i == 4 || i == 7) {
										value = n_array[i] * 10;
									} else {
										value = n_array[i];
									}
									if (value != 0) {
										words_string += words[value] + " ";
									}
									if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
										words_string += "Crores ";
									}
									if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
										words_string += "Lakhs ";
									}
									if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
										words_string += "Thousand ";
									}
									if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
										words_string += "Hundred and ";
									} else if (i == 6 && value != 0) {
										words_string += "Hundred ";
									}
								}
								words_string = words_string.split("  ").join(" ");
							}
							return words_string;
						}
						var actaul_number  = $('#get_netpayable').html();
						var netpayable_in_words = convertNumberToWords(actaul_number);
						$('#netpayable_in_words').html(netpayable_in_words);

						// window.history.back();
						window.print();
						setTimeout(function(){ 
							var print_type = "<?php echo $print_type ?>";
							if(print_type == "re_print"){
								window.location.href = "<?php echo site_url('/');?>Sell_list_all";
							}
							else{
								window.location.href = "<?php echo site_url('/');?>sell";
							}
						}, 500);
					</script>