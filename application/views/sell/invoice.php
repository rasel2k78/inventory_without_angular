
<style type="text/css">
  .signaturebg p { 
    border-top: 1px solid #ddd;
    width: 240px;
    padding-bottom: 0;
    line-height: 40px;
    font-size: 16px;
    font-family: arial;
    margin-top: 70px;
    text-align: center;
  }
  .thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
  .thankyousms p { margin: 15px 0 0 0; padding: 0; }
  .thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
  #invoice{border:1px solid cadetblue;padding: 20px;}
  table {page-break-inside: avoid;}
  a{visibility: hidden;}
</style>

<div class="portlet light" style="" id="voucher_preview">
	<div class="portlet-body" style="padding:30px;">
		<div class="invoice" id="invoice">
      <div class="row invoice">
        <div class="col-xs-12" style="text-align: center;">
          <div>

            <?php 
            if(!empty($store_details['shop_image']))
            {
              ?><img style="height: 15%; width: 15%" src="<?php echo base_url();?><?php echo $store_details['shop_image']?>"><?php
            }
            ?>
          </div>
          <div>
            <h3 style="text-align: center;"><?php echo $store_details['name']?></h3>
            <ul class="list-styled" style="list-style:none">
              <li><?php echo $store_details['address']?></li>
              <li><?php echo $store_details['phone']?></li><li><?php echo $store_details['website']?></li>
              <li></li>
            </ul>
          </div>
        </div>
      </div>
      <span>Voucher No : <?php echo $voucher_data['sell_local_voucher_no']?></span>
      <p style="float:right">Date : <?php echo date("F j, Y, g:i a")?></p><p>Customer Name : <?php echo $customer_info['customers_name']?>, Phone Number : <?php echo $customer_info['customers_phone_1']?></p>
      <p>Customer Address : <?php echo $customer_info['customers_present_address']?></p>
      <!-- <p style="float:right;">Customer phone number : <?php echo $customer_info['customers_phone_1']?></p> -->
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover"><thead>
            <tr>
              <th>SL.</th>
              <th>Item</th>
              <th class="hidden-480">Discount</th>
              <th class="hidden-480">Qty</th>
              <th class="hidden-480"> Unit Cost</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>

            <?php 
            $i=1;
            foreach ($voucher_details as $key => $value) 
            {
             ?>
             <tr>
               <td><?php echo $i?> </td>
               <td> <?php echo $value['spec_set']?></td>
               <td class="hidden-480"><?php echo $value['discount_amount']?></td>
               <td class="hidden-480"><?php echo $value['quantity']?></td>
               <td class="hidden-480"><?php echo $value['selling_price']?></td>
               <td><?php echo $value['sub_total']?></td>
             </tr>
             <?php
             $i++;
           }
           ?>

         </tbody>
       </table>
     </div></div>
     <div class="lasttablebg">
       <table class="table table-striped table-bordered table-hover" style="float: right;width: 50%;">
        <tbody>
          <tr>
            <td>Sub-Total</td>
            <td><?php echo $voucher_data['sub_total']?></td>
          </tr><tr>
          <td>VAT (<?php echo $voucher_data['vat_amount']?>%)</td>
          <td><?php echo $voucher_data['vat_amount']?></td>
        </tr>
        <tr>
          <td>Discount</td>
          <td><?php echo $voucher_data['discount']?></td>
        </tr>
        <tr>
          <td>Net Payable</td>
          <td><?php echo $voucher_data['net_payable']?></td>
        </tr>
        <tr>
          <td>Paid</td>
          <td><?php echo $voucher_data['paid']?></td>
        </tr>
        <tr>
          <td>Due</td>
          <td><?php echo $voucher_data['due']?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="row signaturebg"><p> Signature </p> </div><p style="text-align: center">Thank you for shopping with us</p></div>

  <script>
    jQuery(document).ready(function() {    
   Metronic.init(); // 
   Layout.init(); // init layout
   Demo.init(); // init demo features 
   QuickSidebar.init(); // init quick sidebar
 });

</script>


<script>
  jQuery(document).ready(function() {    
    UIExtendedModals.init();
  });
</script>  


<div id="responsive_modal" class="modal fade in animated shake" tabindex="-1" data-width="460">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Please login again <span id="user_name_selected" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
       Sure to login
     </div>
      <!-- <div class="col-md-6">
    </div> -->
  </div>
</div>
<div class="modal-footer">
  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
  <button type="button" id="user_delete_confirm_y" class="btn blue">Yes</button>
</div>
</div>


<!-- END JAVASCRIPTS -->

<!-- END BODY -->



</div></div>