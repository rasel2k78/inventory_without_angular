<?php
/**
 * "All Sell List Page" 
 *
 * This is page display all sell lists for checking individual sell and  delete.
 * 
 * @link   Sell_list_all/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
// echo $this->input->cookie('printer_settings');exit;
?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->

<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<link href="<?php echo CURRENT_ASSET;?>assets/ppp.css" rel="stylesheet" type="text/css" media="print"/>


<style type="text/css">
    .lasttablebg { float: right; overflow: hidden; }
    .signaturebg { width: 960px; overflow: hidden; }
    .signaturebg p { 
        border-top: 1px solid #ddd;
        width: 240px;
        padding-bottom: 0;
        line-height: 40px;
        font-size: 16px;
        font-family: arial;
        margin-top: 70px;
        text-align: center;
    }
    .thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
    .thankyousms p { margin: 15px 0 0 0; padding: 0; }
    .thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
    #invoice{border:1px solid cadetblue;padding: 20px;}



    @media print {
        body {
            /* zoom:80%;  */
            height: auto;
            font-size: 12px;
            width: auto;
            margin: 1.6cm;
            /* transform: scale(.5); */
        }


        .header, .hide { visibility: hidden }
        display: none;
        /* zoom:80%; */
        table {page-break-inside: avoid;}
    }
</style>

<style type="text/css" media="print">
    /* body {
        zoom:85%; or whatever percentage you need, play around with this number
    } */

    
    .header, .hide { visibility: hidden }
    display: none;
    table {page-break-inside: avoid;}
    a{visibility: hidden;}

</style>

<?php date_default_timezone_set("Asia/Dhaka");?>
<div class="row hidden-print">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?> </div>
        <div class="alert alert-danger" id="delete_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_failed');?> </div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?> </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('left_portlet_title');?>
                </div>
                
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('left_customer');?></th>
                            <th><?php echo $this->lang->line('customer_phone');?></th>
                            <th>
                                <?php echo $this->lang->line('left_voucher_no');?>
                            </th>
                            <th><?php echo $this->lang->line('left_date');?> </th>
                            <th>
                                <?php echo $this->lang->line('left_details_delete');?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('left_customer');?></th>
                            <th><?php echo $this->lang->line('customer_phone');?></th>
                            <th><?php echo $this->lang->line('left_voucher_no');?></th>
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th><?php echo $this->lang->line('left_details_delete');?></th>
                        </tr>
                    </tfoot>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
    <div class="col-md-6">
        <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('customer_insert_success'); ?></div>
        <div class="portlet box red" id="sell_voucher_details_form" style="display:none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><span id="form_header_text" ><?php echo $this->lang->line('right_portlet_title');?></span>
                </div>
            </div>
            <div class="portlet-body form" id="sell_due_paymentmade_div" >
                <!-- BEGIN FORM-->

                <?php $form_attrib = array('id' => 'sell_paymentmade_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="sell_local_voucher_no"  readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_grand_total');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="grand_total"  readonly="true">
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_discount');?> </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="discount"  readonly="true">
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_net_payable');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable"  readonly="true">
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_paid');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid"  readonly="true">
                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_due');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="due"  readonly="true">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <table id="table_items" class="table table-hover control-label col-md-3">
                        <thead>
                            <tr >
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_item_name');?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_quantity');?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_price');?> </th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_sub_total');?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_type');?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_amount');?></th>
                                <th class="control-label"><?php echo $this->lang->line('imei_or_serial');?></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <th>
                    <?php 
                    if($this->session->userdata('printer_type')=="pos")
                    {
                        ?>
                        <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#voucher_info" id="print_now" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
                        <?php
                    }
                    else if($this->session->userdata('printer_type')=="normal")
                    {
                        ?>
                        <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#" id="print_new" class="btn btn-primary"><i class="fa fa-print"></i> Print </a>
                        <?php
                    }
                    else
                    {
                        ?>
                        <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "#" id="print_new" class="btn btn-primary"><i class="fa fa-print"></i> Print </a>
                        <?php
                    }
                    ?>
                </th>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
        <div class="portlet box red" id="sell_voucher_edit_form" style="display:none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><span id="form_header_text" >Edit Sale</span>
                </div>
            </div>
            <div class="portlet-body form" id="sell_edit_div" >
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'sell_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Customer
                        </label>
                        <div class="col-md-6">
                            <input type="text" autocomplete="off" name="customers_name" placeholder="<?php echo $this->lang->line('right_customer_placeholder');?>" id="customers_select" class="form-control">
                            <span class="help-block" id="customer_help_block" ></span>
                            <input type="hidden" autocomplete="off" name="customers_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="customers_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                        <div class=" col-md-3 col-sm-4">
                            <a data-target="#customer_form_modal"  title="<?php echo $this->lang->line('right_customer_title');?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <input type="hidden" name="sell_details_id" value=""/>
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn default pull-right" id="id_cancel" style="margin-left: 8px">Cancel</button>
                        <button type="submit" class="btn green pull-right">Save</button>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
</div>
<div class="portlet light" style="display: none" id="voucher_preview">
    <div class="portlet-body" style="padding:30px;">
        <div class="invoice" id="invoice">
        </div>
    </div>
</div>
<!-- Modal view starts -->
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?>: <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="customer_form_modal" class="modal fade">    
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('customer/add_customer_form_only');?>
        </div>
    </div>
</div>
<!-- Customer Modal Form End -->

<div class="portlet light" style="display: none" id="voucher_preview">
    <div class="portlet-body" style="padding:30px;">
        <div class="invoice" id="invoice">


            <!---header will replaced---!>


            <div class="row" class="col-xs-12">
                <div class="col-xs-12">
                    <div class="col-xs-8">
                        <p>No.</p>
                    </div>
                    <div class="col-xs-4">
                        <p>Date: <?php echo date('d-M-Y');?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <!-- <p>Name : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </p> -->



                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

            <!-- Modal view ends -->

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
            <!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
            <!-- 	 <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
            <!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>

            <!-- date picker js starts -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- date picker js ends -->

            <!-- Boostrap modal starts-->
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
            <!-- Boostrap modal end -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
            <!-- Responsive Datatable Scripts Starts -->
            <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
            <!-- Responsive Datatable Scripts Ends -->
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->	


            <script>

                jQuery(document).ready(function() {    
                    $('.date-picker').datepicker();
                    UIExtendedModals.init();
                });

                $('#sample_2 tfoot th').each(function (idx,elm){
                    if (idx == 0 || idx ==1 || idx == 2 || idx == 4 ) { 
                        var title = $('#example tfoot th').eq( $(this).index() ).text();
                        $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
                    }
                });
        // table_sell_list_all = $('#sample_2').DataTable();    
        var table_sell_list_all = $('#sample_2').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo site_url(); ?>/sell_list_all/all_sell_list_info_for_datatable/",
            "lengthMenu": [
            [10, 15, 20,30],
            [10, 15, 20,30] 
            ],
            "pageLength": 10,
            "language": {
                "lengthMenu": " _MENU_ records",
                "paging": {
                    "previous": "Prev",
                    "next": "Next"
                }
            },
            "columnDefs": [{  
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
            [2, "desc"]
            ]
        });    

        /*data table customize search starts*/
        table_sell_list_all.columns().every( function () {
            var that = this;
            $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
                if (!($(this).hasClass('dont-search'))) {
                    that
                    .search( this.value )
                    .draw();
                }
            });

            $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
                var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
                var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

                var final_string = '';
                if(date_from !='' || date_to != ''){
                    if (date_from!='' && date_to=='') {
                        final_string = date_from+'_0';
                    }
                    if (date_to!='' && date_from=='') {
                        final_string = '0_'+date_to;
                    }
                    if (date_from!='' && date_to!='') {
                        final_string = date_from+'_'+date_to;
                    }
                }
                else{
                    final_string = '0_0';
                }
                that.search(final_string).draw();
            });
        });
        /*datatable customize search ends*/

    </script>
    <script type="text/javascript">
       var sell_details_id = "";
       $('table').on('click', '.sell_voucher_details',function(event) {
        event.preventDefault();
        sell_details_id = $(this).attr('id').split('_')[1];
        $('#current_sell_details_id').remove();
        $.get('<?php echo site_url();?>/sell_list_all/get_all_sell_info/'+sell_details_id).done(function(data) {
            var sell_data = $.parseJSON(data);
            console.log(sell_data);
            if(sell_data == "No Permission")
            {
                    // $('#access_failure').slideDown();
                    // setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
                    toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 5000});
                    toastr.options = {"positionClass": "toast-top-right"}; 
                }
                else
                {
                    $('#sell_voucher_details_form').show(400);
                    $('#sell_voucher_edit_form').hide();
                    $('input[name = sell_local_voucher_no]').val(sell_data.all_sell_info_by_id[0].sell_local_voucher_no);
                    $('input[name = net_payable]').val(sell_data.all_sell_info_by_id[0].net_payable);
                    $('input[name = paid]').val(sell_data.all_sell_info_by_id[0].paid);
                    $('input[name = due]').val(sell_data.all_sell_info_by_id[0].due);
                    $('input[name = discount]').val(sell_data.all_sell_info_by_id[0].discount);
                    $('input[name = grand_total]').val(sell_data.all_sell_info_by_id[0].grand_total);
                    $('#sell_voucher_print').val(sell_details_id);
                    $('#print_now').attr('href', '#voucher_info@'+sell_details_id);
                    var table_html = '';
                    $.each(sell_data.all_sell_info_by_id, function(index, val) {
                        table_html += '<tr>'
                        table_html += '<td>'
                        table_html += val.spec_set;
                        table_html += '</td><td>';
                        table_html += val.quantity;
                        table_html += '</td><td>';
                        table_html += val.selling_price;
                        table_html += '</td><td>';
                        table_html += val.sub_total;
                        table_html += '</td><td>';
                        table_html += val.discount_type;
                        table_html += '</td><td>';
                        table_html += val.discount_amount;
                        table_html += '</td><td>';
                        table_html += val.imei_barcode;
                        table_html += '</td>';
                        table_html += '</tr>'
                    });
                    $('#table_items tbody').html(table_html);



                    var invoice_header = '';
                    invoice_header+= '<div class="row invoice"><div class="col-xs-12" style="text-align: center;"><div>';
                    <?php 
                    if(!empty($store_details['shop_image']))
                    {
                        ?>
                        invoice_header+= '<img style="height: 15%; width: 15%" src="<?php echo base_url();?><?php echo $store_details['shop_image']?>" /></div><div>';
                        <?php
                    }
                    ?>
                    invoice_header+= '<h3 style="text-align: center;"><?php echo $store_details['name']?></h3>';
                    invoice_header+= '<ul class="list-styled" style="list-style:none">';
                    invoice_header+= '<li>';
                    invoice_header+= '<?php echo $store_details['address']?>';
                    invoice_header+= '</li>';
                    invoice_header+= '<li>';
                    invoice_header+= '<?php echo $store_details['phone']?>';
                    invoice_header+= '</li>';
                    invoice_header+= '<li>';
                    invoice_header+= '<?php echo $store_details['website']?>';
                    invoice_header+= '</li>';
                    invoice_header+= '<li>';
                    invoice_header+= '</li>';
                    invoice_header+= '</ul></div></div></div>';

                    if (!sell_data.voucher_number.customers_id) 
                    {
                        invoice_header+= '<span>Voucher No : '+sell_data.voucher_number.sell_local_voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a") ?></p><p>Customer Name : </p><p>Customer Address : </p></div>';
                    }
                    else
                    {
                        invoice_header+= '<span>Voucher No:  '+sell_data.voucher_number.sell_local_voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a")?></p><p>Customer Name : '+sell_data.customer_info.customers_name+', Phone Number: '+sell_data.customer_info.customers_phone_1+'</p><p>Customer Address : '+sell_data.customer_info.customers_present_address+'</p></div>';
                    }
                    invoice_header+= '</div></div><div class="row"><div class="col-md-12">';
                    invoice_header+= '<table class="table table-striped table-bordered table-hover">';
                    invoice_header+= '<thead><tr><th>SL.</th><th>Item</th><th class="hidden-480">Discount</th> <th class="hidden-480">Qty</th><th class="hidden-480"> Unit Cost</th><th>Total Price</th></tr></thead><tbody>';
                    var i=1;
            // console.log(sell_data.all_sell_info_by_id);
            $.each(sell_data.all_sell_info_by_id,function(index, val)
            {
                invoice_header+= ' <tr><td>'+i+' </td>';
                invoice_header+= '<td>'+val.spec_set+'</td>';
                if(val.discount_type == "percentage"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' %'+ '</td>';
                }
                if(val.discount_type == "amount"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' Tk'+ '</td>';
                }
                if(val.discount_type == "free_quantity"){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' pic'+ '</td>';
                }
                if(val.discount_type == ""){
                    invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+ '</td>';
                }
                invoice_header+= '<td class="hidden-480">'+val.quantity+'</td>';
                invoice_header+= '<td class="hidden-480">'+val.selling_price+'</td>';
                invoice_header+= '<td>'+val.sub_total+'</td></tr>';
                i++;
            });
            invoice_header+= '</tbody></table></div></div>';
            invoice_header+='<div class="lasttablebg">';
            invoice_header+='<table class="table table-striped table-bordered table-hover" style="float:right"><tbody>';
            invoice_header+='<tr>';
            invoice_header+='<td>Sub-Total</td>';
            invoice_header+='<td>'+sell_data.voucher_number.sub_total+'</td>';
            invoice_header+='</tr>';
            invoice_header+='<tr>';
            invoice_header+='<td>VAT ('+sell_data.voucher_number.tax_percentage+'%)</td>';
            invoice_header+='<td>'+sell_data.voucher_number.vat_amount+'</td>';
            invoice_header+='</tr>';
            if(sell_data.voucher_number.discount_percentage == "no"){
                invoice_header+='<tr>';
                invoice_header+='<td>Discount</td>';
                invoice_header+='<td>'+sell_data.voucher_number.discount+'</td>';
                invoice_header+='</tr>';
            }
            else{
                var discount_amt= ((sell_data.voucher_number.grand_total)-(sell_data.voucher_number.net_payable)).toFixed(2);
                invoice_header+='<tr>';
                invoice_header+='<td>Discount ('+sell_data.voucher_number.discount+'%)</td>';
                invoice_header+='<td>'+discount_amt +'</td>';
                invoice_header+='</tr>';
            }

            invoice_header+='<tr>';
            invoice_header+='<td>Grand Total</td>';
            invoice_header+='<td>'+parseFloat(sell_data.voucher_number.net_payable).toFixed(2)+'</td>';
            invoice_header+='</tr>';
            invoice_header+='<tr>';
            invoice_header+='<td>Paid</td>';
            invoice_header+='<td>'+parseFloat(sell_data.voucher_number.paid).toFixed(2)+'</td>';
            invoice_header+='</tr>';
            invoice_header+='<tr>';
            invoice_header+='<td>Due</td>';
            invoice_header+='<td>'+sell_data.voucher_number.due+'</td>';
            invoice_header+='</tr>';
            invoice_header+='</tbody></table></div>';
            invoice_header+= '<div class="row signaturebg"><p> Signature </p> </div><p style="text-align: center">Thank you for shopping with us</p><span>Copy</span>';
            $("#invoice").html(invoice_header);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});


$("#print_new").click(function(event) 
{
    var selected_printer_type = "<?php echo $this->session->userdata('printer_type')?>";
    var sell_id = sell_details_id;
    var print_type = "re_print";
    // console.log("hello");
    // return false;
    if(selected_printer_type == "normal")
    {    
        // console.log("normal");
        $.post('<?php echo site_url();?>/sell/normal_print/'+sell_id+"/"+print_type).done(function(data) {
            window.location.assign("<?php echo site_url('/');?>sell/normal_print/"+sell_id+"/"+print_type);
        }).error(function() {
            alert("Error! Refresh The Page and Try Again.");                
        });
    }
    if(selected_printer_type == "normal_without_header"){
        // console.log("normal_without_header");
        // console.log(sell_id);
        // console.log(print_type);
        $.post('<?php echo site_url();?>/sell/without_header_normal_print/'+sell_id+"/"+print_type).done(function(data) {
            window.location.assign("<?php echo site_url('/');?>sell/without_header_normal_print/"+sell_id+"/"+print_type);
        }).error(function() {
            alert("Error! Refresh The Page and Try Again.");                
        });
    }
});



// Ajax Delete 
var sell_details_id = '' ;
var selected_name = '';

$('table').on('click', '.delete_sell_info',function(event) {
    event.preventDefault();
    sell_details_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});


$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
    // var buy_details_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'sell_details_id' : sell_details_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/sell_list_all/delete_sell/'+sell_details_id,post_data).done(function(data)
    {
        var sell_data = $.parseJSON(data);

        if(sell_data.success==true){
            $('#responsive_modal_delete').modal('hide');
            // $('#delete_success').slideDown();
            // setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
            toastr.success('<?php echo $this->lang->line('alert_delete_success');?>', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            table_sell_list_all.row($('#delete_'+sell_details_id).parents('tr')).remove().draw();
        }    

        else if (sell_data.success==false)
        {
            $('#responsive_modal_delete').modal('hide');
            // $('#delete_failure').slideDown();
            // setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );    
            toastr.error('<?php echo $this->lang->line('alert_delete_failed');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else if (sell_data.no_permission == true)
        {

            $('#responsive_modal_delete').modal('hide');
            // $('#access_failure').slideDown();
            // setTimeout( function(){$('#access_failure').slideUp()}, 3000 );     
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"};    
        }
        else if (sell_data.update_error ==true)
        {
            $('#responsive_modal_delete').modal('hide');
            // $('#delete_failure').slideDown();
            // setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );            
            toastr.error('<?php echo $this->lang->line('alert_delete_failed');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }    


    }).error(function() {
        alert("An error has occurred. Please try again");                
    });
});
$('table').on('click', '.edit_sell_info', function(event) {
    event.preventDefault();
    var sell_details_id = $(this).attr('id').split('_')[1];
    $('#current_sell_details_id').remove();
    $.get('<?php echo site_url();?>/sell_list_all/get_all_sell_info/'+sell_details_id).done(function(data) {
        var sell_data = $.parseJSON(data);
        if(sell_data == "No Permission"){
            // $('#access_failure').slideDown();
            // setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else{
            $('#sell_voucher_details_form').show(400);
            $('#sell_voucher_edit_form').hide();
            $('input[name = sell_local_voucher_no]').val(sell_data.all_sell_info_by_id[0].sell_local_voucher_no);
            $('input[name = net_payable]').val(sell_data.all_sell_info_by_id[0].net_payable);
            $('input[name = paid]').val(sell_data.all_sell_info_by_id[0].paid);
            $('input[name = due]').val(sell_data.all_sell_info_by_id[0].due);
            $('input[name = discount]').val(sell_data.all_sell_info_by_id[0].discount);
            $('input[name = grand_total]').val(sell_data.all_sell_info_by_id[0].grand_total);
            $('#sell_voucher_print').val(sell_details_id);
            $('#print_now').attr('href', '#voucher_info@'+sell_details_id);
            var table_html = '';
            $.each(sell_data.all_sell_info_by_id, function(index, val) {
                table_html += '<tr>'
                table_html += '<td>'
                table_html += val.spec_set;
                table_html += '</td><td>';
                table_html += val.quantity;
                table_html += '</td><td>';
                table_html += val.selling_price;
                table_html += '</td><td>';
                table_html += val.sub_total;
                table_html += '</td><td>';
                table_html += val.discount_type;
                table_html += '</td><td>';
                table_html += val.discount_amount;
                table_html += '</td>';
                table_html += '</tr>'
            });
            $('#table_items tbody').html(table_html);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});

// $(document).on('click', '#print_now', function(event) {
//     event.preventDefault();

// });
$(document).on('click', '.sell_voucher_edit', function(event) {
    event.preventDefault();
    var sell_details_id = $(this).attr('id').split('_')[1];
    $('#sell_voucher_details_form').hide();
    $('#sell_voucher_edit_form').show();
    $('input[name=sell_details_id]').val(sell_details_id);
});

/*Select Customer Through Customize Boostrap Select*/
var timer;
var csrf = $("input[name='csrf_test_name']").val();
$("#customers_select").keyup(function(event) 
{
    $("#customers_select_result").show();
    $("#customers_select_result").html('');
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        var search_customers = $("#customers_select").val();
        var html = '';
        $.post('<?php echo site_url(); ?>/sell/search_customer_by_name',{q: search_customers,csrf_test_name: csrf}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                if(val.customers_image == null || val.customers_image == ''){
                    val.customers_image = "images/user_images/no_image.png";
                }
                var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.customers_image+'">';
                html+= '<tr><td data="'+val.customers_id+'">'+image_html+' '+ val.customers_name+' -- '+val.customers_phone_1+'</td></tr>';
            });
            $("#customers_select_result").html(html);
        });
    }, 500);
});
$("#customers_select_result").on('click', 'td', function(event) {
    $('input[name="customers_name"]').val($(this).text());
    $('input[name="customers_id"]').val($(this).attr('data'));
    $("#customers_select_result").hide();
});

$('#sell_edit_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.sell_details_id = $('input[name=sell_details_id]').val();
    data.customers_id = $('input[name=customers_id]').val();
    data.customers_name = $('input[name=customers_name]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/sell/update_sell_voucher_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 == "sell_details_id not found")
        {
            toastr.error('Voucher Id Not Found. System Error', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        if(data2 == "customers_id not found"){
            toastr.error('Customer Id Not Found. Try Again', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        if(data2 == "customers_name not found"){
            toastr.error('Customer Not Selected. Try Again', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        if(data2 == "success")
        {
            $('#sell_edit_form')[0].reset();
            $('#sell_voucher_edit_form').hide();
            toastr.success('Sell Voucher Updated Successfully.', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            table_sell_list_all.ajax.reload(null, false);
        }
        else{
            toastr.error('Update Failure. Please Try Again.', 'Failed', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
    }).error(function() {
        toastr.error('Update Failure. Please Try Again.', 'Failed', {timeOut: 5000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    }); 
});

$(document).on('click', '#id_cancel', function(event) {
    event.preventDefault();
    $('#sell_edit_form')[0].reset();
    $('#sell_voucher_edit_form').hide();
});
</script>