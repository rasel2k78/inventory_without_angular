<?php
/**
 * Form Name:"Sell Form" 
 *
 * This is form for selling items.
 * 
 * @link   Sell/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>


<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>


<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>

<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
    .nmpd-grid {border: none; padding: 20px;}
    .nmpd-grid>tbody>tr>td {border: none;}

    /* Some custom styling for Bootstrap */
    .qtyInput {display: block;
        width: 100%;
        padding: 6px 12px;
        color: #555;
        background-color: white;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>

<div class="row">
    

    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title'); ?> 
                </div>

            </div>
            <div class="portlet-body form">
                <form class="form-horizontal form-bordered" action="" method="get" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-md-1"><b><?php echo $this->lang->line('right_sales_rep'); ?></b>
                        </label>
                        <div class="col-md-2">

                            <input type="text" autocomplete="off" name="sales_rep_name" placeholder="<?php echo $this->lang->line('right_sales_rep'); ?>" id="sales_rep_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="sales_rep_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="sales_rep_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                        <div class=" col-md-1">
                            <a href="#sales_rep_form_modal" class="btn btn-icon-only btn-circle green" data-toggle="modal">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <label class="control-label col-md-1"><b><?php echo $this->lang->line('right_customer');?></b>
                        </label>
                        <div class="col-md-2">

                            <input type="text" autocomplete="off" name="customers_name" placeholder="<?php echo $this->lang->line('right_customer');?>" id="customers_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="customers_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="customers_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                        <div class=" col-md-1">
                            <a href="#customer_form_modal" class="btn btn-icon-only btn-circle green" data-toggle="modal">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <label class="control-label col-md-1"><b><?php echo $this->lang->line('right_cash_type'); ?></b>
                        </label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="optionsRadios4" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="optionsRadios5" value="card"><?php echo $this->lang->line('right_type_card'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="payment_type" id="optionsRadios6" value="cash_card_both"><?php echo $this->lang->line('right_type_both'); ?>
                            </label>
                        </div>
                    </div>

                    <div class="form-group" style=" display:none" id="card_voucher_no">
                        <label class="control-label col-md-9"><b><?php echo $this->lang->line('right_card_voucher_no'); ?></b></label>
                        <div class="col-md-3">

                            <input type="text" class="form-control" name="card_payment_voucher_no" placeholder="<?php echo $this->lang->line('right_card_voucher_no'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group" style=" display:none" id="card_amount_for_both">
                        <label class="control-label col-md-9"><b><?php echo $this->lang->line('right_card_amount'); ?></b></label>
                        <div class="col-md-3">

                            <input type="number" min="0" step="any" class="form-control" name="card_amount" placeholder="<?php echo $this->lang->line('right_card_amount'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- 2nd Row Starts -->

                    <div class="form-group">



                        <label class="control-label col-md-1"><b>Vat</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' readonly="true" id="total_vat" name="tax_percentage"  placeholder="<?php echo $this->lang->line('right_vat');?>"> 

                            <input type="checkbox" id="inclusive_vat_checkbox"> <b><?php echo $this->lang->line('inclusive_vat');?> </b>
                        </div>


                        <label class="control-label col-md-1"><b>Grand Total</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount"  readonly="true" placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>

                        <label class="control-label col-md-1"><b>Discount</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount"   placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>
                        <label class="control-label col-md-1"><b>Net-Payable</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount" readonly="true"  placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>

                    </div>

                    <!-- 2nd ROW Ends -->
                    <!-- 3rd  ROW Begins -->
                    <div class="form-group">
                        <label class="control-label col-md-1"><b>Receive</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount" readonly="true"  placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>


                        <label class="control-label col-md-1"><b>Return</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' readonly="true" id="total_vat" name="tax_percentage"  > 

                            
                        </div>


                        <label class="control-label col-md-1"><b>Paid</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount"  placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>

                        <label class="control-label col-md-1"><b>Due</b>
                        </label>
                        <div class="col-md-2">

                            <input class='form-control keyup_not_triggered' id="total_discount" name="discount" readonly="true"  placeholder="<?php echo $this->lang->line('right_discount');?>">

                        </div>

                    </div>

                    <!-- 3rd ROW Ends -->


                    <div class ="form-actions">

                    </div>

                </form>
            </div>    
        </div>
    </div>

    <div class="col-md-12">

        <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('customer_insert_success'); ?></div>
        <div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?></div>
        <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
        <div class="alert alert-danger" id="no_permission_left" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>

        <div class="alert alert-danger" id="paid_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_failure');?></div>
        <div class="alert alert-success" id="sell_success" style="display:none" role="alert"><?php echo $this->lang->line('sell_success');?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
        <div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('price_amount_failure');?></div>
        <div class="alert alert-danger" id="vat_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('vat_amount_failure');?></div>
        <div class="alert alert-danger" id="vat_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('vat_not_numeric');?></div>
        <div class="alert alert-danger" id="dis_not_valid" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_valid');?></div>
        <div class="alert alert-danger" id="dis_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_numeric');?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
        <div class="alert alert-danger" id="paid_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('paid_not_numeric');?></div>
        <div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_info_empty');?></div>
        <div class="alert alert-danger" id="no_card_voucher" style="display:none" role="alert"><?php echo $this->lang->line('no_card_voucher');?></div>
        <div class="alert alert-danger" id="card_voucher_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('card_voucher_not_numeric');?></div>
        <div class="alert alert-danger" id="select_card_only" style="display:none" role="alert"><?php echo $this->lang->line('select_card_only');?></div>
        <div class="alert alert-danger" id="card_amount_more_than_total" style="display:none" role="alert"><?php echo $this->lang->line('card_amount_more_than_total');?></div>

        <div class="alert alert-danger" id="select_customer_for_due_transaction" style="display:none" role="alert"><?php echo $this->lang->line('select_customer_for_due_transaction');?></div>
        <div class="alert alert-danger" id="total_calculation_mismatch" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch');?></div>

        <div class="alert alert-danger" id="total_calculation_mismatch_only_in_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_only_in_discount');?></div>
        <div class="alert alert-danger" id="total_calculation_mismatch_after_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_after_discount');?></div>
        <div class="alert alert-danger" id="item_quantity_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_quantity_empty');?></div>
        <div class="alert alert-danger" id="no_name" style="display:none" role="alert"><?php echo $this->lang->line('sales_rep_no_name');?></div>


        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>

            </div>
            <div class="portlet-body form">
                <form class="form-horizontal form-bordered" action="" method="get" accept-charset="utf-8">

                    <table class="table table-condensed">

                        <thead>
                            <tr>
                                <th>Sell Type</th>
                                <th>Select Item</th>
                                <th>Available Qty</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Dist. Type</th>
                                <th>Amount</th>
                                <th>Add to cart</th>


                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Sell Type</td>
                                <td>            
                                    <div class="col-md-7">

                                        <input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('left_item_name_placeholder'); ?>" id="item_select" class="form-control"><i class="fa fa-question-circle tooltips" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_item_select_tt');?>"></i>
                                        <input type="hidden" autocomplete="off" name="item_spec_set_id" id="spec_id_for_get" class="form-control">
                                        <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                        </table>
                                    </div>
                                </td>
                                <td><input type="text" name="" value="" placeholder="Current Qty"></td>
                                <td><input type="text" name="" value="" placeholder="Qty"></td>
                                <td><input type="text" name="" value="" placeholder=""></td>
                                <td><input type="text" name="" value="" placeholder=""></td>
                                <td><input type="text" name="" value="" placeholder=""></td>
                                <td><button id="add_item" type="button" class="btn green  pull-right"><?php echo $this->lang->line('left_save_button'); ?></button></td>

                            </tr>

                        </tbody>
                    </table>
                    <div class ="form-actions">

                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- END EXAMPLE TABLE PORTLET-->
</div>


<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="customer_form_modal" class="modal fade">	
    <div class="modal-content">
        <div class="modal-body">
    <?php $this->load->view('customer/add_customer_form_only');?>
        </div>
    </div>
</div>
<!-- Customer Modal Form End -->

<!-- Sales Rep Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="sales_rep_form_modal" class="modal fade">	
    <div class="modal-content">
        <div class="modal-body">
    <?php $this->load->view('sales_rep/add_sales_rep_form_only');?>
        </div>
    </div>
</div>
<!-- Sales Rep Modal Form End -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.numpad.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    var csrf = $("input[name='csrf_test_name']").val();

    jQuery(document).ready(function($) {
        $.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
        $.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
        $.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
        $.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
        $.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
        $.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
        $('#numpadButton-btn1').numpad({
            target: $('#numpadButton1')
        });        
        $('#numpadButton-btn2').numpad({
            target: $('.selling_price_and_numberpad')
        });    

    });

    table = $('#sample_2').DataTable();        

 // Start of AJAX Booostrap for item selection
 var timer;
         // Customize Boostrap Select for Item Selection.
         var timer;
         $("#item_select").keyup(function(event) 
         {
             $("#item_select_result").show();
             $("#item_select_result").html('');

             clearTimeout(timer);
             timer = setTimeout(function() 
             {
                 var search_item = $("#item_select").val();
                 var html = '';
                 $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                // console.log(data);
                data = JSON.parse(data);
                // console.log(data);
                $.each(data, function(index, val) {
                    if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
                        val.item_spec_set_image = "images/item_images/no_image.png";
                    }

                    // console.log(data['item_image']);
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                    html+= '<tr><td data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';

                });

                $("#item_select_result").html(html);

                if ($('#item_select').val().length >= 13) {
                    $('#item_select_result').find('td').eq(0).click();
                    $('input[name=quantity]').val(1);
                    $('#add_item').prop('disabled', false);
                }
            });
             },

             500);

             
         });

         /* Select Sales Representative through Customize Boostrap Select*/
         $("#sales_rep_select").keyup(function(event) 
         {
             $("#sales_rep_select_result").show();
             $("#sales_rep_select_result").html('');

             clearTimeout(timer);
             timer = setTimeout(function() 
             {
                 var search_sales_rep = $("#sales_rep_select").val();
                 var html = '';
                 $.post('<?php echo site_url(); ?>/sell/search_sales_rep_by_name',{q: search_sales_rep,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                // console.log(data);
                data = JSON.parse(data);
                // console.log(data);
                $.each(data, function(index, val) {
                    if(val.sales_rep_image == null || val.sales_rep_image == ''){
                        val.sales_rep_image = "images/sales_rep_images/no_image.png";
                    }

                    // console.log(data['item_image']);
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.sales_rep_image+'">';
                    html+= '<tr><td data="'+val.sales_rep_id+'">'+image_html+' '+ val.sales_rep_name+'</td></tr>';
                });
                $("#sales_rep_select_result").html(html);
            });
             }, 500);
         });

         $("#sales_rep_select_result").on('click', 'td', function(event) {
             $('input[name="sales_rep_name"]').val($(this).text());
             $('input[name="sales_rep_id"]').val($(this).attr('data'));
             $("#sales_rep_select_result").hide();
         });

         /*Select Customer Through Customize Boostrap Select*/
         $("#customers_select").keyup(function(event) 
         {
             $("#customers_select_result").show();
             $("#customers_select_result").html('');

             clearTimeout(timer);
             timer = setTimeout(function() 
             {
                 var search_customers = $("#customers_select").val();
                 var html = '';
                 $.post('<?php echo site_url(); ?>/sell/search_customer_by_name',{q: search_customers,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                // console.log(data);
                data = JSON.parse(data);
                // console.log(data);
                $.each(data, function(index, val) {
                    if(val.customers_image == null || val.customers_image == ''){
                        val.customers_image = "images/user_images/no_image.png";
                    }

                    // console.log(data['item_image']);
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.customers_image+'">';
                    html+= '<tr><td data="'+val.customers_id+'">'+image_html+' '+ val.customers_name+'</td></tr>';
                });
                $("#customers_select_result").html(html);
            });
             }, 500);
         });

         $("#customers_select_result").on('click', 'td', function(event) {
             $('input[name="customers_name"]').val($(this).text());
             $('input[name="customers_id"]').val($(this).attr('data'));
             $("#customers_select_result").hide();
         });



 // var options = {
 //     ajax          : {
 //         url     : '<?php echo site_url(); ?>/sell/search_item_by_name',
 //         type    : 'POST',
 //         dataType: 'json',
 //            // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
 //            // automatically replace it with the value of the search query.
 //            data    : {
 //                q: '{{{q}}}',
 //                csrf_test_name: csrf
 //            }
 //        },
 //        locale        : {
 //            emptyTitle: "<?php echo $this->lang->line('left_item_name_placeholder');?>"
 //        },
 //        log           : 3,
 //        preprocessData: function (data) {
 //            var i, l = data.length, array = [];
 //            if (l) {
 //                for (i = 0; i < l; i++) {
 //                    if (data[i].item_image == null) {
 //                        data[i].item_image = "images/item_images/no_image.png";
 //                    }
 //                    else{
 //                        data[i].item_image = data[i].item_image;
 //                    };
 //                    array.push($.extend(true, data[i], {
 //                        text : data[i].spec_set,
 //                        value: data[i].item_spec_set_id,
 //                        data : {
 //                            subtext: '',
 //                            content: "<img height='50px' width='50px' src='<?php echo base_url() ?>"+data[i].item_image+"'>&nbsp;"+data[i].spec_set
 //                        }
 //                    }));
 //                }
 //            }
 //            // You must always return a valid array when processing data. The
 //            // data argument passed is a clone and cannot be modified directly.
 //            return array;
 //        }
 //    };

 //    $('#item_select').selectpicker().ajaxSelectPicker(options);
 //    $('#item_select').trigger('change');
    // End of AJAX Boostrap for Unit selection

     // Start of AJAX Booostrap for customer selection
     // var options = {
     //     ajax          : {
     //         url     : '<?php echo site_url(); ?>/sell/search_customer_by_name',
     //         type    : 'POST',
     //         dataType: 'json',
  //                       // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
  //                       // automatically replace it with the value of the search query.
  //                       data    : {
  //                           q: '{{{q}}}', 
  //                           csrf_test_name: csrf,
  //                       }
  //                      },
  //                      locale        : {
  //                          emptyTitle: "<?php echo $this->lang->line('right_customer_placeholder');?>"
  //                      },
  //                      log           : 3,
  //                      preprocessData: function (data) {
  //                          var i, l = data.length, array = [];
  //                          if (l) {
  //                              for (i = 0; i < l; i++) {
  //                                  array.push($.extend(true, data[i], {
  //                                      text : data[i].customers_name,
  //                                      value: data[i].customers_id,
  //                                      data : {
  //                                          subtext: ''
  //                                      }
  //                                  }));
  //                              }
  //                          }
  //           // You must always return a valid array when processing data. The
  //           // data argument passed is a clone and cannot be modified directly.
  //           return array;
  //       }
  //   };

  //   $('#customer_select').selectpicker().ajaxSelectPicker(options);
  //   $('#customer_select').trigger('change');
    // End of AJAX Boostrap for Unit selection

         // Start of AJAX Booostrap for Sales Representative selection
    //      var options = {
    //          ajax          : {
    //              url     : '<?php echo site_url(); ?>/sell/search_sales_rep_by_name',
    //              type    : 'POST',
    //              dataType: 'json',
    //                     // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
    //                     // automatically replace it with the value of the search query.
    //                     data    : {
    //                         q: '{{{q}}}', 
    //                         csrf_test_name: csrf,
    //                     }
    //                    },
    //                    locale        : {
    //                        emptyTitle: "<?php echo $this->lang->line('right_sales_rep_placeholder');?>"
    //                    },
    //                    log           : 3,
    //                    preprocessData: function (data) {
    //                        var i, l = data.length, array = [];
    //                        if (l) {
    //                            for (i = 0; i < l; i++) {
    //                                array.push($.extend(true, data[i], {
    //                                    text : data[i].sales_rep_name,
    //                                    value: data[i].sales_rep_id,
    //                                    data : {
    //                                        subtext: ''
    //                                    }
    //                                }));
    //                            }
    //                        }
    //         // You must always return a valid array when processing data. The
    //         // data argument passed is a clone and cannot be modified directly.
    //         return array;
    //     }
    // };

    // $('#sales_rep_select').selectpicker().ajaxSelectPicker(options);
    // $('#sales_rep_select').trigger('change');
    // End of AJAX Boostrap for Unit selection




// For check box always checked.
$('#inclusive_vat_checkbox').prop('checked', true);

//  Script for adding product through AJAX.
var render_data= {
    // 'basic_data' : {},
    'item_info' : [],
    'all_basic_data':{},
};

$('.form-actions').on('click', '#add_item', function(event) {
    event.preventDefault();
    var error_found=false;

    if($('input[name="item_spec_set_id"]').val()==null || $('input[name="item_spec_set_id"]').val()== "")
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').removeClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').hide();
        
    }
    if($('input[name=quantity]').val().trim()=="")
    {
        $('input[name=quantity]').closest('.form-group').addClass('has-error');
        $('input[name=quantity]').parent().parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=quantity]').closest('.form-group').removeClass('has-error');
        $('input[name=quantity]').parent().parent().find('.help-block').hide();
    }
    if($('input[name=selling_price]').val().trim()=="")
    {
        $('input[name=selling_price]').closest('.form-group').addClass('has-error');
        $('input[name=selling_price]').parent().parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=selling_price]').closest('.form-group').removeClass('has-error');
        $('input[name=selling_price]').parent().parent().find('.help-block').hide();
    }
    
    
    if(error_found)
    {
        return false;
    }

    render_data['item_info'].push({
        'sell_type'  : $('#sell_type_id').val(),
        // 'item_spec_set_id' : $('#item_select').val(),
        // 'item_name' : $('#item_select option:selected').text(),
        'item_spec_set_id' : $ ('input[name="item_spec_set_id"]').val(),
        'item_name' : $('input[name="items_name"]').val(),
        'actual_quantity' : $('input[name="quantity"]').val(),
        'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),
        'sub_total' : $('#dis_type').val()=='free_quantity' ? Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)) : ($('#dis_type').val()=='amount' ? Number((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val()))-parseFloat($ ('input[name="discount_amount"]').val()).toFixed(2)) : Number(((parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()))-((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val())*parseFloat($ ('input[name="discount_amount"]').val()))/100)).toFixed(2))) || Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)),

        'selling_price' : $('input[name="selling_price"]').val(),

        'sell_comments' : $ ('input[name="sell_comments"]').val(),
        'discount_amount' : $ ('input[name="discount_amount"]').val(),
        'discount_type' : $('#dis_type').val(),


    })

    // $('#voucher_no_span').html(render_data.basic_data.voucher_no);
    // $('#vendor_name_span').html(render_data.basic_data.vendor_name);
    // $('#landed_cost_span').html(render_data.basic_data.total_cost);

    sell_table_render();
    // $('.portlet .collapse:first').click();

    //  clear the whole form rather than individual        **
    // $('#item_select').selectpicker('val','');
    $('input[name="items_name"]').val(''),
    $('input[name="quantity"]').val('');
    $('input[name="selling_price"]').val('');
    $('input[name="sell_comments"]').val('');
    $('input[name="current_quantity_inventory"]').val('');
    $('input[name="discount_amount"]').val('');
    $('#dis_type').val('');
    if($('#extra_fields').show()){
        // $('#extra_fields').hide();
        
    }
    $('#add_item').prop("disabled",true);
    $('input[name=items_name]').focus();

});
var current_active_vat_percentage =0;
var total_amount = 0;
var total_net_payable= 0;
var net_pay_after_discount = 0;

$(document).on('change', '.table_quantity', function(event) {
    event.preventDefault();
    var new_quantity = $(this).val();
    var row_number = $(this).attr('name').split('_')[1];
    render_data['item_info'][row_number]['actual_quantity'] = new_quantity;
    render_data['item_info'][row_number]['quantity'] = new_quantity;
    render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['selling_price']*new_quantity;
    sell_table_render();
    $('input[name=items_name]').focus();
});

$(document).on('change', '.table_price', function(event) {
    event.preventDefault();
    var new_price = $(this).val();
    var row_number = $(this).attr('name').split('_')[1];
    render_data['item_info'][row_number]['selling_price'] = new_price;
    render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['quantity']*new_price;
    sell_table_render();
    $('input[name=items_name]').focus();
});

function sell_table_render () {
    var sell_table_html = '';
    total_amount = 0;
    
    var subtotal_amount = 0;

    $.each(render_data['item_info'], function(index, val) {
        sell_table_html += '<tr class="sell_item_row_table" id="'+index+'">';

        sell_table_html += '<td>'+val.item_name+'</td>';
        if(val.discount_type =="" || val.discount_type =="amount" || val.discount_type =="percentage" ){
            sell_table_html += '<td><input type="number" min="1" step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+val.quantity+'"></td>';
            
        }
        else if(val.discount_type =="free_quantity"){
            // var total_quantity_with_free = parseFloat(val.quantity)+parseFloat(val.discount_amount);
            var total_quantity_with_free = parseInt(val.actual_quantity);
            sell_table_html += '<td><input type="number" min="1" step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+total_quantity_with_free+'"></td>';
            
            // sell_table_html += '<td>'+total_quantity_with_free+'</td>';
            // render_data['item_info']['quantity']= total_quantity_with_free;
        }
        
        sell_table_html += '<td><input type="number" min="0.1" class="form-control table_price" name="tableprice_'+index+'" value="'+Number(val.selling_price).toFixed(2)+'"></td>';

        if (val.discount_type =="") {

            sell_table_html += '<td>'+Number(val.actual_quantity*val.selling_price).toFixed(2)+'</td>';
            subtotal_amount+= val.actual_quantity*val.selling_price;
            Number(subtotal_amount.toFixed(2));


        }
        else if(val.discount_type =="amount"){
            
            var total= val.quantity*val.selling_price;
            total -= val.discount_amount;
            sell_table_html += '<td>'+Number(total).toFixed(2)+'</td>';
            subtotal_amount+= total;
            Number(subtotal_amount.toFixed(2));    
        }

        else if (val.discount_type  =="percentage"){

            var total_sub_total= val.quantity*val.selling_price;
            var total =total_sub_total*val.discount_amount;
            total /=100;
            total_sub_total-=total;

            sell_table_html += '<td>'+Number(total_sub_total).toFixed(2)+'</td>';
            subtotal_amount+= total_sub_total;
            Number(subtotal_amount.toFixed(2));
        }
        else if(val.discount_type =="free_quantity"){

            sell_table_html += '<td>'+Number(val.actual_quantity*val.selling_price).toFixed(2)+'</td>';
            subtotal_amount+= val.actual_quantity*val.selling_price;
            Number(subtotal_amount.toFixed(2));
        }
        
        sell_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
        sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a> <a id="" class="glyphicon glyphicon-pencil item_edit" style="color:blue;"></a>';
        sell_table_html += '</td>';
        sell_table_html += '</tr>';

        total_amount = subtotal_amount;
        Number(total_amount.toFixed(2));
        

    });

    if($('#inclusive_vat_checkbox').is(":checked")){
        render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));;
        $('#sell_table tbody').html(sell_table_html);
        $('#total_amount').html(Number(total_amount.toFixed(2)));

        var current_vat_percent = Number($('#total_vat').val());
        var vat_on_sell = parseFloat(total_amount) * parseFloat(current_vat_percent)||0 ;
        vat_on_sell /=100;

        final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
        $('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));    
        $('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
        $('#total_due').text(Number(final_amount_with_vat.toFixed(2)));

        render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
        render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
        render_data['all_basic_data']['tax_percentage']= current_vat_percent;

    }
    else{
        render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));
        
        $('#sell_table tbody').html(sell_table_html);
        $('#total_amount').html(Number(total_amount.toFixed(2)));
        $('#total_amount_with_vat').text(Number(total_amount.toFixed(2)));    
        $('#total_net_payable').text(Number(total_amount.toFixed(2)));
        $('#total_due').text(Number(total_amount.toFixed(2)));

        render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
        render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
        render_data['all_basic_data']['tax_percentage']= 0;
    }


}

$(document).on('change', '#inclusive_vat_checkbox', function(event) {
    
    if ($('#inclusive_vat_checkbox').is(":checked")) {       
        render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));;

        $('#total_amount').html(Number(total_amount.toFixed(2)));
        if($('#total_paid').val() !=""){
            render_data['all_basic_data']['paid']= Number($('#total_paid').val());
        }


        var current_vat_percent = Number($('#total_vat').val());

        var vat_on_sell = parseFloat(total_amount) * parseFloat(current_vat_percent)||0 ;
        vat_on_sell /=100;

        final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
        $('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));    
        $('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
        $('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
    // render_data['all_basic_data']['due']=Number(final_amount_with_vat.toFixed(2));
    render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
    render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
    render_data['all_basic_data']['tax_percentage']= current_vat_percent;
    render_data['all_basic_data']['due_after_paid']= Number($('#total_due').text());
}
else{
    render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));;

    if($('#total_paid').val() !=""){
        render_data['all_basic_data']['paid']= Number($('#total_paid').val());
    }
        // $('#sell_table tbody').html(sell_table_html);
        $('#total_amount').html(Number(total_amount.toFixed(2)));
        $('#total_amount_with_vat').text(Number(total_amount.toFixed(2)));    
        $('#total_net_payable').text(Number(total_amount.toFixed(2)));
        $('#total_due').text(Number(total_amount.toFixed(2)));

        render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
        render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
        render_data['all_basic_data']['tax_percentage']= 0;
        render_data['all_basic_data']['due_after_paid']= Number($('#total_due').text());
        
    }

});


$('#sell_table').on('click', '.item_edit', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    
    var editing_item_info;
    editing_item_info = render_data['item_info'][clicked_id];
    // console.log(editing_item_info);

    $('select[name=sell_type]').val(editing_item_info.sell_type);
    $('select[name=discount_type]').val(editing_item_info.discount_type);
    // $('#item_select').prop("disabled",true);

    $('#item_select_field').hide('fast');
    $('#sell_type_id').prop("disabled",true);
    $('#item_select').prop("disabled",true);
    $('#current_qty').hide('fast');

    // $('#item_select').attr('disabled',true);
    // $.get('<?php echo site_url(); ?>/sell/get_item_info_for_sell/'+editing_item_info.item_spec_set_id, function(data) {
    //     var items = $.parseJSON(data);
    //     var html = '<option value="'+items.item_spec_set_id+'" data-content="<img height=\'50px\' width=\'50px\' src=\'<?php echo base_url() ?>'+items.item_image+'\'>'+items.items_name+'">'+items.item_name+'</option>';
    //     $('#item_select').append(html);
    //     $('#item_select').selectpicker('val', editing_item_info.item_spec_set_id);
    //     $('input[name="current_quantity_inventory"]').val(items.quantity);
    // });

    // $('#item_select').selectpicker('val', editing_item_info.item_spec_set_id);

    $('input[name="item_spec_set_id"]').val(editing_item_info.item_spec_set_id),
    $('input[name="items_name"]').val(editing_item_info.item_name),
    $('input[name="quantity"]').val(editing_item_info.actual_quantity);
    $('input[name="sell_comments"]').val(editing_item_info.sell_comments);
    $('input[name="discount_amount"]').val(editing_item_info.discount_amount);
    $('input[name="discount_type"]').val(editing_item_info.discount_type);
    $('input[name="selling_price"]').val(editing_item_info.selling_price);

    $('#add_item').hide();
    $('#update_item').show('slow');
    $('#update_item').attr('selected-id', clicked_id);

});


$('.form-actions').on('click', '#update_item', function(event) {
    event.preventDefault();
    var change_item_index_array = $(this).attr('selected-id');
    // render_data.item_info[change_item_index_array].item_spec_set_id    = $('#sell_type_id').val();
    render_data.item_info[change_item_index_array].sell_type    = $('#sell_type_id').val();
    // render_data.item_info[change_item_index_array].item_spec_set_id = $('#item_select').val();
    // render_data.item_info[change_item_index_array].item_name = $('#item_select option:selected').text();
    render_data.item_info[change_item_index_array].item_spec_set_id = $('input[name="item_spec_set_id"]').val();
    render_data.item_info[change_item_index_array].item_name = $('input[name="items_name"]').val();

    render_data.item_info[change_item_index_array].quantity = ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val();
    render_data.item_info[change_item_index_array].actual_quantity = $('input[name="quantity"]').val();

    render_data.item_info[change_item_index_array].comments = $('input[name="comments"]').val();
    render_data.item_info[change_item_index_array].discount_type = $('#dis_type').val();
    render_data.item_info[change_item_index_array].selling_price = $('input[name="selling_price"]').val();
    render_data.item_info[change_item_index_array].discount_amount = $('input[name="discount_amount"]').val();

    sell_table_render();

    $('#add_item').show('fast');
    $('#update_item').hide();
    // $('#current_quantity').show('fast');
    $('#current_qty').show('fast');
    
    $('#add_item').prop("disabled",true);
    $('#item_select_field').show('fast');
    $('#item_select').prop("disabled",false);
    $('#sell_type_id').prop("disabled",false);

    // $('#sell_type_id').val('');
    // $('#item_select').selectpicker('val','');
    $('input[name="items_name"]').val('');
    $('input[name="item_spec_set_id"]').val('');
    $('input[name="quantity"]').val('');
    $('input[name="current_quantity_inventory"]').val('');
    
    $('input[name="selling_price"]').val('');
    $('input[name="sell_comments"]').val('');
    $('input[name="discount_amount"]').val('');
    $('#dis_type').val('');
    // $('#extra_fields').show();


});


var final_amount_with_vat;

// $( "#sell_table" ).on( "keyup", "#total_vat", function(e) {

// 	$(this).removeClass('keyup_not_triggered');
// 	var vat_on_sell = parseFloat(total_amount) * parseFloat($(this).val())||0 ;
// 	vat_on_sell /=100;

// 	final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
// 	$('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));	
// 	$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
// 	$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
// 	render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
// 	render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));

// });

$.get('<?php echo site_url() ?>/sell/get_current_vat_percentage/', function(data) {
    var vat_data = $.parseJSON(data);
    // console.log(vat_data);
    $('#total_vat').val(vat_data.vat_percentage);
    current_active_vat_percentage = vat_data.vat_percentage;

});

$('input[name="discount"]').val(0);


$('#total_discount').keyup(function(event) {

    var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat($(this).val())||0;
    $('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
    $('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
    if($('#total_paid').val() !=""){
        render_data['all_basic_data']['paid']= Number($('#total_paid').val());
    }

    render_data['all_basic_data']['discount']=parseFloat($(this).val())||0;
    render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
    render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));


});

$('#total_received').keyup(function(event) {

    if (event.keyCode == '9') {
        return false;
    };

    var total_change_after_receive = parseFloat($( this ).val()) - parseFloat($('#total_net_payable').text())||0;
    $('#total_change').text(Number(total_change_after_receive.toFixed(2)));


    render_data['all_basic_data']['received_money']=parseFloat($(this).val());
    render_data['all_basic_data']['return_change']=Number(total_change_after_receive.toFixed(2));

});

$('#total_paid').keyup(function(event) {

    if (event.keyCode == '9') {
        return false;
    };

    var due_after_paid = parseFloat($('#total_net_payable').text()) - parseFloat($(this).val())||0;

    $('#total_due').text(Number(due_after_paid.toFixed(2)));

    // if($('#total_vat').hasClass('keyup_not_triggered')){

    //     render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
    //     // render_data['all_basic_data']['net_pay_after_discount']=total_amount;        commented this after solving discount and net_pay_after_discount problem 
    //     if ($('#total_discount').val() == '') {
    //         render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
    //     }
    // }


    // if($('#total_discount').hasClass('keyup_not_triggered')){
    //     if($('#total_discount').val() == ''){
    //         render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
    //     }

    //     if ($('#total_discount').val() == '') {
    //         render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
    //     }

    // }

    render_data['all_basic_data']['paid']=parseFloat($(this).val());
    render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));

});

$('#sell_table').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    sell_table_render();
});




$(document).on('click', '#save_sell', function(event) {

    event.preventDefault();
    $('#save_sell').prop("disabled",true);
    // render_data.all_basic_data.sell_local_voucher_no = $('input[name="sell_local_voucher_no"]').val();
    render_data.all_basic_data.customers_id = $('input[name="customers_id"]').val();
    render_data.all_basic_data.sales_rep_id = $('input[name="sales_rep_id"]').val();
    
    // render_data.all_basic_data.customers_name = $('#customer_select option:selected').text();
    // render_data.all_basic_data.total_cost = $('input[name="landed_cost"]').val();
    render_data.all_basic_data.discount = $('input[name="discount"]').val();
    render_data.all_basic_data.received_money = $('input[name="received_money"]').val();
    render_data.all_basic_data.tax_percentage = $('input[name="tax_percentage"]').val();
    render_data.all_basic_data.payment_type = $('input[name="payment_type"]:checked').val();
    render_data.all_basic_data.card_amount = $('input[name="card_amount"]').val();
    render_data.all_basic_data.card_payment_voucher_no = $('input[name="card_payment_voucher_no"]').val();
    render_data.all_basic_data.paid = $('input[name="paid"]').val();

    
    var all_data= {
        'csrf_test_name' :$('input[name=csrf_test_name]').val(),
        'all_data' : render_data,
    }

    $.post('<?php echo site_url() ?>/sell/save_sell_info',all_data ).done(function(data, textStatus, xhr) {
        var result = $.parseJSON(data);
        
        // console.log(result);
        if (result[0].split(' ')[0].search("success")!=-1)
        {
            // $('#sales_rep_select').selectpicker('val','');
            // $('#customer_select').selectpicker('val','');
            $('input[name="sales_rep_name"]').val('');
            $('input[name="customers_name"]').val('');

            $('.sell_item_row_table').remove();
            render_data['item_info'] = [];
            render_data['all_basic_data'] = {};
            $('input[name="tax_percentage"]').val('');
            $('input[name="discount"]').val(0);
            $('input[name="received_money"]').val('');
            $('input[name="paid"]').val('');
            $('input[name="card_payment_voucher_no"]').val('');
            $('input[name="card_amount"]').val('');
            $('#total_amount').html('');
            $('#total_amount_with_vat').html('');
            $('#total_net_payable').html('');
            $('#total_change').html('');
            $('#total_due').html('');
            $('input[name="customers_id"]').val('');
            $('input[name="sales_rep_id"]').val('');
            // alert ("<?php echo $this->lang->line('alert_sus_msg_of_item_sell');?>");
            $('#sell_success').slideDown();
            setTimeout( function(){$('#sell_success').slideUp()}, 3000 );        
            $('#print_now').attr('href','#save_sell_info@'+result[0].split(' ')[1]);
            $('#print_now')[0].click();
            $('#save_sell').prop("disabled",false);
            $('#total_vat').val(current_active_vat_percentage);
            if(!$('#inclusive_vat_checkbox').is(":checked")){
                $('#inclusive_vat_checkbox').prop('checked', true);
                $('#inclusive_vat_checkbox').parent().addClass('checked');

            }
        } 

        else {
            var error_array = [];
            $.each(result, function(index, val) {
                if(val == "item info not given"){
                    error_array.push('item_quantity_empty');
                }
                if(val == "PLease Enter Item Quantity"){
                    error_array.push('item_info_empty');
                }

                if(val == "Total Paid Amount is Empty or Not Numeric"){
                    error_array.push('paid_amount_failure');
                }

                if(val == "Item does not have enough quantity"){
                    error_array.push('item_amount_failure');
                }
                if(val == "price not given"){
                    error_array.push('price_amount_failure');
                }
                if(val == "VAT Amount Is Not Valid"){
                    error_array.push('vat_amount_failure');
                }
                
                if(val == "VAT Amount Is Not Numeric"){
                    error_array.push('vat_not_numeric');
                }
                if(val == "Discount Amount Is Not Valid"){
                    error_array.push('dis_not_valid');
                }
                if(val == "Discount Amount Is Not Numeric"){
                    error_array.push('dis_not_numeric');
                }
                if(val == "Paid Amount Is Not Numeric"){
                    error_array.push('paid_not_numeric');
                }
                if(val == "Card Voucher Or Card Amount Is Empty"){
                    error_array.push('no_card_voucher');
                }
                if(val == "Card Vocuher Is Not Numeric"){
                    error_array.push('card_voucher_not_numeric');
                }
                if(val == "Please Select Card Option"){
                    error_array.push('select_card_only');
                }
                if(val == "Card Amount Is More Than Total Amount"){
                    error_array.push('card_amount_more_than_total');
                }
                if(val == "Select Customer for Due Transaction"){
                    error_array.push('select_customer_for_due_transaction');
                }
                if(val == "Total Calculation mismatch"){
                    error_array.push('total_calculation_mismatch');
                }
                if(val == "Total Calculation mismatch after discount"){
                    error_array.push('total_calculation_mismatch_after_discount');
                }
                if(val == "Total Calculation mismatch in discount field"){
                    error_array.push('total_calculation_mismatch_only_in_discount');
                }
                
            });
            $.each(error_array, function(index, val) {
                $('#'+val).slideDown();
                setTimeout( function(){$('#'+val).slideUp()}, 1500 );
                $('#save_sell').prop("disabled",false);
            });
        }
        // window.location.reload(true);
    }).error(function() {
        alert("<?php echo $this->lang->line('alert_failed_msg');?>");
        $('#save_sell').prop("disabled",false);
    });


});

$('#item_select_result').on('click', 'td', function(event) {
    event.preventDefault();
    
    $('input[name="items_name"]').val($(this).text());
    $('input[name="item_spec_set_id"]').val($(this).attr('data'));
    $("#item_select_result").hide();

    var sell_type_id = $('#sell_type_id').val();
    var item_select_val = $('input[name="item_spec_set_id"]').val();
    // console.log(item_select_val);
    // return false;
    $.get('<?php echo site_url() ?>/sell/get_item_price_info/'+item_select_val, function(data) {
        var data_get = $.parseJSON(data);
        // console.log(data_get);
        $('#current_quantity').val(data_get[0].quantity);


        
        // $('#numpadButton1').attr("placeholder", "Type your answer here");
        if (sell_type_id == 'wholesale') {
            $('.selling_price_and_numberpad').val(data_get[0].whole_sale_price);
        }
        else if(sell_type_id == 'retail'){
            $('.selling_price_and_numberpad').val(data_get[0].retail_price);
        };

        $('#add_item').click();
    });
});


$('#add_item').prop("disabled",true);

$(document).on('keyup', '#numpadButton1', function(event) {
    event.preventDefault();
    var input_quantity = $(this).val();
    var item_select_val = $('input[name="item_spec_set_id"]').val();
    $.get('<?php echo site_url() ?>/sell/quantity_by_item_from_inventory/'+item_select_val, function(data) {
        var data_quantity = $.parseJSON(data);

        if( parseFloat(input_quantity) > parseFloat(data_quantity)){
            
            $('#update_item').prop("disabled",true);
            $('#add_item').prop("disabled",true);
            $("input[name='quantity']").parent().parent().parent().addClass('has-error');
            alert ("<?php echo $this->lang->line('alert_not_enough_quantity');?>");
            
        }
        else {
            $('#update_item').prop("disabled",false);
            $('#add_item').prop("disabled",false);
            $("input[name='quantity']").parent().parent().parent().removeClass('has-error');
            $('#add_item').prop("disabled",false);
            
        }
    });

});		

$('input:radio[name="payment_type"]').change(
    function(){
        if ( $(this).val() == 'card' ) {
            $('#card_voucher_no').show('fast');
            $('#card_amount_for_both').hide('fast');    
        }    
        // if($(this).val() != 'card'){
        //     $('#card_voucher_no').hide('fast');
        // }
        if($(this).val() == 'cash_card_both'){
            $('#card_voucher_no').show('fast');
            $('#card_amount_for_both').show('fast');    

        }

        if($(this).val() == 'cash'){
            $('#card_voucher_no').hide('fast');
            $('#card_amount_for_both').hide('fast');    

        }

    });


$('input[name=items_name]').focus();

// jQuery(function(){
// 	jQuery('#item_select').click();
// 	// $('#item_select').trigger('click');
// });

// $(document).ready(function(){ 
// 	$("input").attr("autocomplete", "off"); 
// });






</script>