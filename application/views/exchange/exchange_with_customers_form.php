<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/> -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="exchange_success" style="display:none" role="alert"><?php echo $this->lang->line('exchange_success'); ?></div>

        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </div>
        <div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_item_price'); ?></div>
        <div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('give_appropriate_amount'); ?></div>
        <div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('give_item_quntity'); ?></div>
        <div class="alert alert-danger" id="item_price_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('give_appropriate_amount'); ?></div>
        <div class="alert alert-danger" id="customer_not" style="display:none" role="alert">Exchange in due is not possible without customer</div>

        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('exchange_with_customer_info'); ?>
                </div>
            </div>
            <div class="portlet-body form" id="form1">
                <!-- BEGIN FORM-->
                <!-- <form id="brand_form" class="form-horizontal form-bordered" action="<?php echo site_url('/Brand/save_brand_info');?>" method="post" enctype="multipart/form-data"> -->
                    <?php $form_attrib = array('id' => 'exchange_customers_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('find_voucher_tt'); ?>"></i>
                                <input type="text"  class="form-control" name="sell_voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('phone_number'); ?> </label>
                            <div class="col-md-9 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text"  class="form-control" name="mobile_number" placeholder="<?php echo $this->lang->line('phone_number'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('date'); ?></label>
                            <div class="col-md-3 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" id="voucherDate" name="voucher_date" placeholder="<?php echo $this->lang->line('date'); ?>" />
                            </div>
                            <div class="col-md-3 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text"  class="form-control" name="voucher_item_name" placeholder="<?php echo $this->lang->line('item'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="cancle_voucher" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>                    
                        <button type="submit" class="btn green"><?php echo $this->lang->line('find'); ?> </button>

                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>

            <div class="portlet box green-haze" id="voucher_list_products" style="display:none">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i><?php echo $this->lang->line('item_buy_details'); ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <table id="sell_table3" class="table table-condensed">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('voucher_no'); ?></th>
                                    <th><?php echo $this->lang->line('total_price'); ?></th>
                                    <th><?php echo $this->lang->line('paid'); ?></th>
                                    <th><?php echo $this->lang->line('Due'); ?></th>
                                    <!-- <th>ছাড়ের ধরন</th> -->
                                    <th><?php echo $this->lang->line('details'); ?> 
                                        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('details_button_tt'); ?>"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <table id="table_items" class="table table-hover control-label col-md-5">
                            <thead>
                                <tr >
                                    <th class="control-label col-md-3"><?php echo $this->lang->line('item_name'); ?></th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('quantity'); ?></th>          
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('buying_price_unit'); ?> </th>
                                    <th>Discount</th>
                                    <th class="control-label col-md-2"><?php echo $this->lang->line('sub_total'); ?></th>
                                    <th class="control-label col-md-2">Total qty</th>
                                    <th class="control-label col-md-2">Imei</th>
                                    <th class="control-label col-md-3"><?php echo $this->lang->line('new_quantity'); ?></th>
                                    <th class="control-label col-md-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Grand Total</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="grand_total" id="grand_total" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Net Payable</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Discount</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="total_discount" id="total_discount" placeholder="Total Discount" readonly="true">
                        </div>
                    </div>


                    <div class="form-group" style="display: none;">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('total_paid'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid_new" placeholder="<?php echo $this->lang->line('total_paid'); ?>" readonly="true">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Due</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="total_due" placeholder="Total Due" readonly="true" style="background: lightblue;">
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="control-label col-md-3">Total Hidden Due</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="total_hidden_due" placeholder="Total Due" readonly="true" style="background: lightblue;">
                        </div>
                    </div>
                    
                    <div class="form-group" id="return_money_div">
                        <label class="control-label col-md-3">Money Return</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="return_money" placeholder="<?php echo $this->lang->line('return_money'); ?>" readonly="true" style="background: lightblue;">
                        </div>
                    </div>

                    <div class="form-group" id="paid_now_div" style="display: none;">
                        <label class="control-label col-md-3">Paid</label>
                        <div class="col-md-9">
                            <input style="border:1px solid #3B9C96;" type="text" class="form-control" name="paid_now" id="paid_now" placeholder="paid amount">
                        </div>
                    </div>

                    <div class="form-group" style="display: none;">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>" readonly="true">
                            <input type="hidden" name="sell_details_id" id="sell_details_id">
                        </div>
                    </div>
                    <div class="form-group" id="usable_div">
                        <button type="button" style="margin-left: 50%;padding: 5px;" disabled="true" class="btn green" id="useable_voucher"><?php echo $this->lang->line('use_voucher'); ?></button>
                    </div>
                    <div style="clear: both;height: 10px;"></div>
                    <div class="form-group" id="exchange_save" style="display: none;">
                        <button type="button" style="margin-left: 50%;background: darkred" class="btn green" id="save_sell">Save Exchange</button>
                    </div>

                </div>
            </div>    

        </div>


        <div class="col-md-6">

            <div class="portlet box green-haze" id="sell_voucher_details_form" style="display:none">    
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i><?php echo $this->lang->line('item_sell'); ?>
                        <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php $form_attrib = array('id' => 'exchange_customeasdfasdfrs_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('item'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('new_item_tt'); ?>"></i>
                                <input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('item'); ?>" id="item_select" class="form-control">
                                <input type="hidden" autocomplete="off" name="item_spec_set_id"  class="form-control">
                                <input type="hidden" autocomplete="off" name="new_item_imei"  class="form-control">
                                <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>
                            </div>
                        </div>
                        <input type="hidden" name="item_type" class="form-control">


                        <div class="form-group">
                            <label class="control-label col-md-3">Available Quantity</label>
                            <div class="col-md-9 input-form input-icon">
                                <input disabled="true" type="text" id="inventory_quantity" name="inventory_quantity" class="form-control" placeholder="<?php echo $this->lang->line('total_quantity'); ?>" aria-describedby="numpadButton-btn">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('total_quantity'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Search with specific fields and find and select your voucher then proceed to exchange"></i> -->
                                <input type="text" id="item_quantity" name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('total_quantity'); ?>" aria-describedby="numpadButton-btn">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('sell_price_unit'); ?></label>
                            <div class="col-md-9 input-form input-icon">
                                <input type="text" class="form-control" name="retail_price_hidden"  placeholder="<?php echo $this->lang->line('single_price'); ?>">
                                <!-- <input type="hidden" name="retail_price_hidden"> -->
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="control-label col-md-3">Sub-total Price</label>
                            <div class="col-md-9 input-form input-icon">
                                <input type="text" class="form-control" name="retail_price"  placeholder="<?php echo $this->lang->line('single_price'); ?>">
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label class="control-label col-md-3">Item discount</label>
                            <div class="col-md-9 input-form input-icon">
                                <input type="text" class="form-control" id="new_item_discount" name="new_item_discount"  placeholder="individual discount optional">
                            </div>
                        </div>


                        <div id="extra_fields" style="display:none">
                            <div class="form-group">
                                <label class="control-label col-md-3">মূল্যছাড়ের পরিমাণ
                                </label>
                                <div class="col-md-3">
                                    <select class="form-control" id="dis_type" name="discount_type">
                                        <option ></option>
                                        <option value="amount">টাকা</option>
                                        <option value="percentage">%</option>
                                        <option value="free_quantity">টা(পণ্য)</option>
                                    </select>
                                </div>
                                <input type="text" name="discount_amount" placeholder="ছাড়ের পরিমাণ প্রদান করুন">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">মন্তব্য</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="comments"  placeholder="মন্তব্য">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn green" id="add_item"><?php echo $this->lang->line('add_new_item'); ?></button>
                        <button type="button" id="cancle_item" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                    </div>
                    <?php echo form_close();?>
                    <!-- END FORM-->
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze" id="sell_voucher_form1" style="display:none;">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i><?php echo $this->lang->line('left_portlet_title'); ?>

                    </div>
                </div>
                <div class="portlet-body">

                    <div class="form-group">
                        <table id="sell_table" class="table table-condensed">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('item'); ?> 
                                        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_list_tt'); ?>"></i></th>
                                        <th><?php echo $this->lang->line('quantity'); ?></th>
                                        <th>Selling Price</th>
                                        <th>Net Sell price</th>
                                        <th>Net Subtotal</th>
                                        <th>Imei Number</th>
                                        <!-- <th><?php echo $this->lang->line('Delete_items'); ?></th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="width:200px;">Returned item's total amount :</th>
                                        <th id="total_exchanged"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>



                            <div class="form-group" id="new_items" style="display: none;">

                                <div style="padding: 5px; height:30px;border:1px solid #3B9C96;width:100%;text-align: center;font-weight: bold">New Items for exchange</div>

                                <table id="sell_table_new_items" class="table table-condensed">
                                 <thead>
                                    <tr>
                                        <th>
                                            <?php echo $this->lang->line('item'); ?> 
                                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_new_tt'); ?>"></i>
                                        </th>
                                        <th><?php echo $this->lang->line('total_quantity'); ?></th>
                                        <th><?php echo $this->lang->line('sell_price'); ?></th>
                                        <th><?php echo $this->lang->line('sub_total'); ?></th>
                                        <th>Discount</th>
                                        <th>Net Sub-total</th>
                                        <th>Imei</th>
                                    </tr>

                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="width:200px;">New item's total amount :</th>
                                        <th id="total_new"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->    
        <script>
            var csrf = $("input[name='csrf_test_name']").val();
            var timer;
            function change_form()
            {
                $('#form1').hide();
                $('#form2').show();
            }

            jQuery(document).ready(function() {    
                $('.date-picker').datepicker();
            });

            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            $("#add_item").prop('disabled', 'false');
            $('#useable_voucher').click(function(event) {
                $("#useable_voucher").css("display", "none");
                $("#exchange_save").css("display", "block");
                $("#save_sell").attr("disabled", "true");
                $('.sell_new_items').show();
                $('.exchanged_quantity').show();
                $('.exchanged_quantity').css("border","1px solid #45b6af");
                $('#sell_table3').hide();
                $('#sell_table3_wrapper').hide();
            });


            $('#button_extra').click(function(event) {
                $('#extra_fields').toggle('slow');
            });

            table_brand = $('#sample_2').DataTable();

            var render_data= {
                'item_info' : [],
                'imei_serial_data' : [],
                'all_basic_data':{},
            };

            var render_new_data= {
                'item_info_new' : [],
                'all_basic_data':{},
            };

            $('#cancle_voucher').click(function(event) {
                $('input[name="sell_voucher_no"]').val('');
                $('input[name="voucher_date"]').val('');
                $('input[name="voucher_item_name"]').val('');
                $('input[name="mobile_number"]').val('');

                $("#sell_table > tbody").html("");
                $("#sell_table2 > tbody").html("");
                $("#table_items > tbody").html("");
                $("#total_exchanged").html('');
                $("#total_payable_with_returned").html('');
                $("#total_difference").html('');
                $("#total_new_amount").html('');
                $('input[name="sell_voucher_no"]').val('');
                $('input[name="sell_details_id"]').val('');
                $('input[name="voucher_no"]').val('');
                $('input[name="mobile_number"]').val('');
                $('input[name="voucher_date"]').val('');
                $('input[name="voucher_item_name"]').val('');
            //$('#useable_voucher').hide();
            $('#voucher_list_products').hide();
            $('#sell_voucher_details_form').hide();
            $('#sell_voucher_form1').hide();
            $('#sell_voucher_form2').hide();
            all_data.length =0;

        });

            $('#cancle_item').click(function(event) {
                $('#item_select').val('');
                $('input[name="quantity"]').val('');
                $('input[name="retail_price"]').val('');
            });


            var sell_item_id = '' ;
            var selected_name = '';

            clearTimeout(timer);
            timer = setTimeout(function() 
            {
                $('table').on('click', '.ok_button',function(event) {
                    event.preventDefault();
                    var item_imei_number;
                    updated_sell_item_id = $(this).attr('id').split('_')[2];
                    updated_item_name = $(this).parent().parent().find("td:nth-child(1)").text();
                    current_item_quantity = $(this).parent().parent().find("td:nth-child(2)").text();
                    updated_item_retail_price = $(this).parent().parent().find("td:nth-child(3)").text();
                    item_quantity_left = $(this).parent().parent().find("td:nth-child(6)").text();
                    updated_item_quantity = parseInt($(this).parent().parent().find("input:first").val());
                    item_subtotal = $(this).parent().parent().find("td:nth-child(5)").text();
                    item_imei_number = $(this).parent().parent().find("td:nth-child(7)").text();
                    if(parseInt(updated_item_quantity)<1)
                    {
                        alert("please give correct quantity to exchange");exit;
                    }
                    current_total = $('input[name=net_payable]').val();
                    total_paid = $('input[name=paid_new]').val();
                    total_receivable = $('input[name=due]').val();
                    total_due = $('input[name=total_due]').val() ||0;
                    total_return = $('input[name=return_money]').val() ||0;

                    var grand_total = $("#grand_total").val();
                    var total_discount = $("#total_discount").val() ||0;
                    var unit_price_with_total_discount = Number((parseFloat(total_discount/grand_total))) ||0;
                    var sub_total_after_total_discount = Number(parseFloat(item_subtotal -parseFloat(item_subtotal*unit_price_with_total_discount))).toFixed(2) ||0;

                    var item_unit_price_after_total_individual_discount = parseFloat(sub_total_after_total_discount/current_item_quantity) ||0;
                    returned_items_price = Number(parseFloat(item_unit_price_after_total_individual_discount*updated_item_quantity)).toFixed(2) ||0;


                    if($.isNumeric(updated_item_quantity))
                    {
                        if(updated_item_quantity.length===0)
                        {
                            alert('No amount changed yet');
                            $(this).parent().parent().find("input:first").val('');
                            exit;
                        }
                        else if (parseInt(updated_item_quantity)>parseInt(item_quantity_left)) 
                        {
                            alert('Exchane item number excedded');
                            $(this).parent().parent().find("input:first").val('');
                            exit;    
                        }
                        else
                        {
                            $(this).parent().parent().find("td:nth-child(6)").text(item_quantity_left-updated_item_quantity);
                            $("#return_money_div").show();

                            if(parseFloat(returned_items_price)>parseFloat(total_due))
                            {
                                $('input[name=total_due]').val(0);  
                                $('input[name=total_hidden_due]').val(0);  
                                $('input[name=return_money]').val(Number(parseFloat(total_return) + parseFloat(returned_items_price-total_due)).toFixed(2));
                            }
                            else if(parseFloat(total_due)>= parseFloat(returned_items_price))
                            {
                                $('input[name=total_due]').val(Number(total_due-returned_items_price).toFixed(2));
                                $('input[name=total_hidden_due]').val(Number(total_due-returned_items_price).toFixed(2));
                                $('#paid_now_div').show();
                                $('input[name=return_money]').val(0);
                            }
                            $('#sell_voucher_form1').show(400);
                            $('#sell_voucher_details_form').show(400);
                            $('#sell_voucher_form1').show(400);
                            $('#sell_voucher_form2').show(400);
                            var item_spec_set_id = updated_sell_item_id;
                            var exchanged_quantity = updated_item_quantity;
                            var voucher_no = $('input[name=voucher_no]').val();
                            var post_data ={
                                'item_spec_set_id' : item_spec_set_id,
                                'voucher_no' : voucher_no,
                                'csrf_test_name' : $('input[name=csrf_test_name]').val(),
                            }; 
                            var found = false;
                            $.post('<?php echo site_url();?>/Exchange_with_customers/check_buy_cart_items/'+item_spec_set_id,post_data).done(function(data) {
                                var data2 = $.parseJSON(data);
                                $.each(render_new_data['item_info_new'], function(index, val) 
                                {
                                    if(!val.item_imei_number)
                                    {
                                       if(val.item_spec_set_id == item_spec_set_id)
                                       {
                                        render_new_data['item_info_new'][index]['quantity'] =parseInt(render_new_data['item_info_new'][index]['quantity']) + parseInt(updated_item_quantity);
                                        found = true;
                                    }
                                }
                            });
                                if(found==true)
                                {

                                }
                                else
                                {
                                    render_new_data['item_info_new'].push({
                                        'item_spec_set_id' : item_spec_set_id,
                                        'item_name' : updated_item_name,
                                        'quantity' : updated_item_quantity,
                                        'selling_price' : updated_item_retail_price,
                                        'selling_price_final' : item_unit_price_after_total_individual_discount,
                                        'item_imei_number' : item_imei_number?item_imei_number:"",
                                    })
                                }
                                $(this).parent().parent().find("input:first").val('');
                                sell_table_new_render();
                            }).error(function() {
                                alert("<?php echo $this->lang->line('sorry'); ?>");                
                            });    
                            $(this).parent().parent().find("input:first").val('');
                        }
                    }
                    else
                    {
                        alert("<?php echo $this->lang->line('give_numeric'); ?>");
                        $(this).parent().parent().find("input:first").val('');
                        exit;
                    }
                });
}, 500);

var total_amount = 0;
var total_net_payable= 0;

function sell_table_new_render (){
    var sell_table_new_html = '';
    total_amount = 0;
    var subtotal_amount = 0;

    $.each(render_new_data['item_info_new'], function(index, val) {
        subtotal_amount+= val.quantity*val.selling_price_final;
        sell_table_new_html += '<tr id="'+index+'">';
        sell_table_new_html += '<td>'+val.item_name+'</td>';
        sell_table_new_html += '<td>'+val.quantity+'</td>';
        sell_table_new_html += '<td>'+val.selling_price+'</td>';
        sell_table_new_html += '<td>'+(val.selling_price_final).toFixed(2)+'</td>';
        sell_table_new_html += '<td>'+(val.quantity*val.selling_price_final).toFixed(2)+'</td>';
        sell_table_new_html += '<td>'+val.item_imei_number ||''+'</td>';
        sell_table_new_html += '</tr>';
        total_amount = subtotal_amount;
    });

    render_new_data['all_basic_data']['net_payable']=total_amount;
    render_new_data['all_basic_data']['due']=total_amount;
    $('#sell_table tbody').html(sell_table_new_html);
    $('#total_exchanged').html(Number(subtotal_amount).toFixed(3));
    $('#total_net_payable_new').html(total_amount);
    $('#total_due_new').html(total_amount);
    var exchanged_amount = $('#total_exchanged').html()||0;
    var new_item_amount = $('#total_new_amount').html() ||0;
    var    new_payable = parseInt($('#total_new_amount').html() || 0) - parseInt($('#total_exchanged').html() || 0);
}

$('#sell_table3').on('click', '.voucher_details', function(event) {
    $("#useable_voucher").show();
    $("#useable_voucher").prop("disabled", false);
    var data = {};
    updated_sell_voucher_id = $(this).attr('id').split('_')[1];
    data.sell_voucher_no = updated_sell_voucher_id;
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/Exchange_with_customers/get_voucher_information',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.success=="no items")
        {
            alert("<?php echo $this->lang->line('no_items'); ?>");
            return false;
        }
        else
        {
            $('input[name = voucher_no]').val(data2.all_sell_info_by_id[0].sell_local_voucher_no);
            $('input[name = net_payable]').val(data2.all_sell_info_by_id[0].net_payable);
            $('input[name = grand_total]').val(data2.all_sell_info_by_id[0].grand_total);
            $('input[name = sub_total]').val(data2.all_sell_info_by_id[0].sub_total);
            $('input[name = paid_new]').val(data2.all_sell_info_by_id[0].paid);
            $('input[name = total_due]').val(data2.all_sell_info_by_id[0].due);
            $('input[name = total_hidden_due]').val(data2.all_sell_info_by_id[0].due);
            $('input[name = sell_details_id]').val(data2.all_sell_info_by_id[0].sell_details_id);
            $('input[name = return_money]').val(0);
            if(data2.all_sell_info_by_id[0].discount_percentage=="yes")
            {
                discount_in_amount = Number(parseFloat(data2.all_sell_info_by_id[0].discount/100)*(data2.all_sell_info_by_id[0].grand_total)).toFixed(2);
                $('input[name = total_discount]').val(discount_in_amount);
            }
            else
            {
                $('input[name = total_discount]').val(data2.all_sell_info_by_id[0].discount);
            }

            var table_html = '';
            $.each(data2.all_sell_info_by_id, function(index, val) {
                table_html += '<tr>'
                table_html += '<td id="item_name_"'+val.item_spec_set_id+'>'
                table_html += val.spec_set;
                table_html += '</td><td id="item_quantity_"'+val.item_spec_set_id+'>';
                table_html += val.quantity;
                table_html += '</td><td>';
                table_html += val.selling_price;

                if(val.discount_type=="amount")
                {
                    table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                    table_html += val.discount_amount;
                }
                else if(val.discount_type=="percentage")
                {
                    discount_in_amount = Number(parseFloat(val.discount_amount/100)*(val.quantity*val.selling_price)).toFixed(2);
                    table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                    table_html += discount_in_amount;
                }
                else
                {
                    table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                    table_html += val.discount_amount;
                }

                table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                table_html += val.sub_total;
                table_html += '</td><td>';
                table_html += val.quantity;
                table_html += '</td><td>';
                table_html += val.imei_barcode?val.imei_barcode:"";
                table_html += '</td><td>';
                table_html += '<input style="display:none;1px solid #45b6af !important" type="text" name="new_amount" class="form-control exchanged_quantity">';
                table_html += '</td>';
                table_html += '<td><button style="border:1px solid #45b6af !important" type="button" class="form-control ok_button" id="new_amount_'+val.item_spec_set_id+'">Ok</button></td>';
                table_html += '</tr>';
            });
            $('#table_items tbody').html(table_html);
        }
    }).error(function() {
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
});

$('.form-actions').on('click', '#add_item', function(event) {
    event.preventDefault();
    var error_found=false;
    if($('input[name=voucher_no]').val().trim()=="")
    {
        $('input[name=voucher_no]').closest('.form-group').addClass('has-error');
        $('input[name=voucher_no]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
        $('input[name=voucher_no]').parent().find('.help-block').hide();
    }
    if($('input[name="item_spec_set_id"]').val()==null)
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').removeClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').hide();

    }
    if($('input[name=quantity]').val().trim()=="")
    {
        $('input[name=quantity]').closest('.form-group').addClass('has-error');
        $('input[name=quantity]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=quantity]').closest('.form-group').removeClass('has-error');
        $('input[name=quantity]').parent().find('.help-block').hide();
    }
    if($('input[name=selling_price]').val()=="")
    {
        $('input[name=retail_price]').closest('.form-group').addClass('has-error');
        $('input[name=retail_price]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=retail_price]').closest('.form-group').removeClass('has-error');
        $('input[name=retail_price]').parent().find('.help-block').hide();
    }

    if(error_found)
    {
        return false;
    }

    var item_spec_set_id = $('input[name="item_spec_set_id"]').val();
    var given_quantity = $('input[name="quantity"]').val();
    var post_data ={
        'item_spec_set_id' : item_spec_set_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/Exchange_with_customers/check_items_on_inventory/'+item_spec_set_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        if(parseInt(given_quantity) <= parseInt(data2.quantity))
        {
           current_total = $('input[name=net_payable]').val();
           total_paid = $('input[name=paid_new]').val();
           total_receivable = $('input[name=due]').val();
           var grand_total = $("#grand_total").val();
           var new_item_price = parseFloat(($('input[name="retail_price_hidden"]').val())*$('input[name="quantity"]').val())- parseFloat($('input[name="new_item_discount"]').val()||0);
           total_due = $('input[name=total_due]').val() ||0;
           total_return = $('input[name=return_money]').val();
           if(total_return>0)
           {
            if(parseFloat(new_item_price)>parseFloat(total_return))
            {
                $('input[name=total_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=total_hidden_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=return_money]').val(0);
                $('#paid_now_div').show();
            }
            else if(parseFloat(total_return)>= parseFloat(new_item_price))
            {
                $('input[name=total_due]').val(0);
                $('input[name=total_hidden_due]').val(0);
                $('input[name=return_money]').val(Number(parseFloat(total_return)-parseFloat(new_item_price)).toFixed(2));
            }
        }
        else if(total_due>=0)
        {
           $('input[name=total_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
           $('input[name=total_hidden_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2)); 
           $('input[name=return_money]').val(0);
           $('#paid_now_div').show();
       }

       var found = false;
       $.each(render_data['item_info'], function(index, val) 
       { 
        if(!val.item_imei_number)
        {
            if(val.item_spec_set_id == item_spec_set_id)
            {
                render_data['item_info'][index]['quantity'] = parseInt(render_data['item_info'][index]['quantity']) + parseInt($('input[name="quantity"]').val());
                found = true;
            }
        }
    });

       if(found==true)
       {

       }
       else
       {
        render_data['item_info'].push({
            'item_spec_set_id' : $('input[name="item_spec_set_id"]').val(),
            'item_name' : $('#item_select').val(),
            'quantity' : $('input[name="quantity"]').val(),
            'item_imei_number' : $('input[name="new_item_imei"]').val(),
            'new_item_discount' : $('input[name="new_item_discount"]').val() ||0,
            'selling_price' : $('input[name="retail_price_hidden"]').val(),
            'whole_sale_price' : $ ('input[name="whole_sale_price"]').val(),
            'discount_type' : $('#dis_type').val(),
        });
    }

    $("#save_sell").attr("disabled",false);

    sell_table_render();

    $('input[name="item_type"]').val('');
    $('input[name="item"]').val('');
    $('#item_select').val('');
    $('input[name="quantity"]').val('');
    $('input[name="inventory_quantity"]').val('');
    $('input[name="selling_price"]').val('');
    $('input[name="new_item_discount"]').val('');
    $('input[name="new_item_imei"]').val('');
    $('input[name="retail_price"]').val('');
    $('input[name="retail_price_hidden"]').val('');
    $('input[name="whole_sale_price"]').val('');
    $('input[name="discount_amount"]').val('');
    $('#dis_type').val('');
    $('#button_extra').show();
}
else
{
    alert("<?php echo $this->lang->line('give_appropriate_amount'); ?>");
}    
}).error(function() {
    alert("<?php echo $this->lang->line('sorry'); ?>");            
});

});

var total_amount = 0;
var total_net_payable= 0;

function sell_table_render () {
    var sell_table_html = '';
    total_amount = 0;
    
    var subtotal_amount = 0;
    $.each(render_data['item_info'], function(index, val) 
    {
       // console.log(val);
       sell_table_html += '<tr id="'+index+'">';         
       sell_table_html += '<td>'+val.item_name+'</td>';
       sell_table_html += '<td>'+val.quantity+'</td>';

       sell_table_html += '<td><input type="number" min="0.1" class="form-control table_price" name="tableprice_'+index+'" value="'+val.selling_price+'"></td>';
       subtotal_amount+= (val.quantity*val.selling_price)-(val.new_item_discount||0);

       var net_subtotal = (val.quantity*val.selling_price)-(val.new_item_discount||0);

       sell_table_html += '<td>'+val.quantity*val.selling_price+'</td>';
       sell_table_html += '<td><input type="number" min="0.1" class="form-control table_discount" name="tablediscount_'+index+'" value="'+val.new_item_discount+'"></td>';
       sell_table_html += '<td>'+net_subtotal+'</td>';
       sell_table_html += '<td>'+val.item_imei_number+'</td>';
        // sell_table_html += '<td><input type="hidden" name="tablepricebefore_'+index+'" value="'+net_subtotal+'"></td>';
        sell_table_html += '</tr>';
        total_amount = subtotal_amount;
    });

    $('#sell_table_new_items tbody').html(sell_table_html);
    $('#total_new').html(total_amount);
    $('#total_amount').html(total_amount);
    $('#total_net_payable').html(total_amount);
    $('#total_due').html(total_amount);
    $('#total_new_amount').html(total_amount);
    $('#new_items').show();
}


$('#total_discount').keyup(function(event) {
    var net_pay_after_discount = parseFloat(total_amount) - parseFloat($( this ).val());
    $('#total_net_payable').text(net_pay_after_discount);
    $('#total_due').text(net_pay_after_discount);

    render_data['all_basic_data']['discount'] = parseFloat($(this).val());
    render_data['all_basic_data']['net_pay_after_discount']=net_pay_after_discount;
});

var current_cashbox_amount =<?php echo $cashbox_amount['total_cashbox_amount']?>;

$("#exchanged_custom_price").keyup(function(event) {
    alert($(this).val());
});
$('#item_quantity').keyup(function(event) {

    var item_spec_set_id = $('input[name=item_spec_set_id]').val();
    var item_new_quantity = parseInt($(this).val());
    var post_data ={
        'item_spec_set_id' : item_spec_set_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/Exchange_with_customers/check_items_on_inventory/'+item_spec_set_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        if(item_new_quantity>data2.quantity)
        {
            alert('Not enough items on inventory');
            $('#item_quantity').val('');
            $('#total_price').val('');
            // $("#add_item").prop('disabled', 'disabled');
            // exit;
            return false;
        }
        else
        {
            var per_item_price = $('#selling_price').val();
            $('#total_price').val(item_new_quantity*per_item_price);
            $("#add_item").removeAttr("disabled");
        }
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});

//var timer;



$('#paid_now').keyup(function(event) 
{

    var current_due = $('input[name="total_due"]').val()||0;
    var total_hidden_due = $('input[name="total_hidden_due"]').val()||0;
    var paid = $(this).val();
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        if(paid=="")
        {
            $('input[name="total_due"]').val(total_hidden_due);
        }
        else
        {
            if(parseFloat(paid)>parseFloat(current_due))
            {
                $('input[name="total_due"]').val(0);
            }
            else
            {
                $('input[name="total_due"]').val(parseFloat(total_hidden_due)-parseFloat(paid));
            }
        }
    }, 500);
});

$('#sell_table').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_new_data['item_info_new'].splice(clicked_id,1);
    sell_table_new_render();
});

$('#sell_table2').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    sell_table_render();
});

$('#sell_table').on('click', '#item_edit', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    var editing_item_info;
    editing_item_info = render_data['item_info'][clicked_id];

    $('#item_select').selectpicker('val', editing_item_info.item_spec_set_id);
    $('input[name="selling_price"]').val(editing_item_info.selling_price);
    $('input[name="quantity"]').val(editing_item_info.quantity);
    //$('input[name="comments"]').val(editing_item_info.comments);
    $('input[name="discount_amount"]').val(editing_item_info.discount_amount);
    $('input[name="discount_type"]').val(editing_item_info.discount_type);
    $('input[name="retail_price"]').val(editing_item_info.retail_price);
    $('input[name="whole_sale_price"]').val(editing_item_info.whole_sale_price);
});



$('#save_sell').click(function(event) {
    event.preventDefault();
    $("#item_select").css('border', 'inherit');
    
    render_data.all_basic_data.voucher_no = $('input[name="voucher_no"]').val();
    render_data.all_basic_data.exchanged = render_new_data['item_info_new'];
    render_data.all_basic_data.add_paid = $('#paid_now').val();
    render_data.all_basic_data.add_due = $('input[name="total_due"]').val();
    render_data.all_basic_data.add_return = $('input[name="return_money"]').val();

    $('#item_select').selectpicker('val','');
    $('input[name="quantity"]').val('');
    $('input[name="retail_price"]').val('');

    //exit;
    var all_data= {
        'csrf_test_name' :$('input[name=csrf_test_name]').val(),
        'all_data' : render_data,
    }
    console.log(all_data);
    $.post('<?php echo site_url() ?>/Exchange_with_customers/save_sell_info',all_data ).done(function(data, textStatus, xhr) 
    {
        var result = $.parseJSON(data);
        $('#final_calculation').show();
        if(result == "no_exchange")
        {
            alert("<?php echo $this->lang->line('not_exchanged'); ?>");
        }

        else if(result == "Please add exchanged items")
        {
            alert('<?php echo $this->lang->line('add_exchanged_item'); ?>');
            $("#item_select").css('border', '1px solid red');
            return false;
        }

        else if(result == "No customer exist")
        {
            $('#customer_not').slideDown();
            setTimeout( function(){$('#customer_not').slideUp()}, 2500 );
            return false;
        }

        else if(result == "success")
        {
            $('#total_paid').val('');
            $('#total_amount').html('');
            $('#exchange_success').slideDown();
            setTimeout( function(){$('#exchange_success').slideUp()}, 2500 );
           // location.reload();
           $("#sell_table > tbody").html("");
           $("#sell_table2 > tbody").html("");
           $("#table_items > tbody").html("");
           $("#total_exchanged").html('');
           $("#total_payable_with_returned").html('');
           $("#total_difference").html('');
           $("#total_new_amount").html('');
           $('input[name="sell_voucher_no"]').val('');
           $('input[name="sell_details_id"]').val('');
           $('input[name="voucher_no"]').val('');
           $('input[name="mobile_number"]').val('');
           $('input[name="voucher_date"]').val('');
           $('input[name="voucher_item_name"]').val('');
            //$('#useable_voucher').hide();
            $('#voucher_list_products').hide();
            $('#sell_voucher_details_form').hide();
            $('#sell_voucher_form1').hide();
            $('#sell_voucher_form2').hide();
            all_data.length =0;

        }
        else
        {
            var error_array = [];
            $.each(result, function(index, val) {
                if(val == "PLease Enter Item Quantity"){
                    error_array.push('item_info_empty');
                }

                if(val == "Item does not have enough quantity"){
                    error_array.push('item_amount_failure');
                }
                if(val == "price not given"){
                    error_array.push('price_amount_failure');
                }
                if(val == "price not perfect"){
                    error_array.push('item_price_not_matched');
                }
            });
            $.each(error_array, function(index, val) {
                $('#'+val).slideDown();
                setTimeout( function(){$('#'+val).slideUp()}, 1500 );
            });
        }

    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>");
    });

});


$('#exchange_customers_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    $('input[name=voucher_no]').val('');
    $('#sell_table3').show();
    $('#sell_table3_wrapper').show();
    $('#sell_table3 tbody').html('');
    $('#table_items tbody').html('');
    $('#total_paid').val('');
    $('#total_amount').html('');
    data.sell_voucher_no = $('input[name=sell_voucher_no]').val();
    data.item_name = $('input[name=voucher_item_name]').val();
    data.voucher_date = $('#voucherDate').val();
    data.mobile_number = $('input[name=mobile_number]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/Exchange_with_customers/get_voucher_details_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.all_sell_info_by_id.length==0)
        {
            alert("<?php echo $this->lang->line('no_voucher'); ?>");
        }
        else
        {
            $('#voucher_list_products').show(400);
            var table_html = '';
            $.each(data2.all_sell_info_by_id, function(index, val) {
                table_html += '<tr>'
                table_html += '<td id="item_name_"'+val.item_spec_set_id+'>'
                table_html += val.sell_local_voucher_no;
                table_html += '</td><td id="item_quantity_"'+val.item_spec_set_id+'>';
                table_html += val.grand_total;
                table_html += '</td><td>';
                table_html += val.paid;
                table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                table_html +=  val.due;
                table_html += '</td><td>';
                table_html += '<button class="btn btn-xs green voucher_details" id="voucherDetails_'+val.sell_local_voucher_no+'">Details</button></td>';
                table_html += '</tr>'
            });
            $('#sell_table3 tbody').html(table_html);
            $('#sell_table3').DataTable();
            render_new_data['item_info_new'].length = 0;
            render_data['item_info'].length = 0;
            $("#sell_table > tbody").html("");
            $("#sell_table2 > tbody").html("");
            $("#table_items > tbody").html("");
            $("#total_exchanged").html('');
            $("#total_payable_with_returned").html('');
            $("#total_difference").html('');
            $("#total_new_amount").html('');
            $('input[name="sell_details_id"]').val('');
            $('input[name="mobile_number"]').val('');
            $('input[name="voucher_date"]').val('');
            $('input[name="voucher_item_name"]').val('');
            $('input[name="grand_total"]').val('');
            $('input[name="net_payable"]').val('');
            $('input[name="total_discount"]').val('');
            $('input[name="total_due"]').val('');
            $('input[name="return_money"]').val('');
            $('input[name="paid_now"]').val('');
            $("#paid_now_div").hide();
            $("#exchange_save").hide();
            sell_table_render();
        }
    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
});    

$('#save_sell_new').click(function(event) {
    var net_amount =0;
    var return_payment = render_data['all_basic_data']['net_payable'];
    var new_payment = render_new_data['all_basic_data']['net_payable'];
    net_amount = (return_payment) -(new_payment);
    $('#return_payment').html(return_payment);
    $('#new_payment').html(new_payment);
    $('#new_net_amount').html(net_amount);
});

// clearTimeout(timer);
// timer = setTimeout(function() 
// {

//var voucher_due = $('input[name=total_due]').val() ||0;

$("#sell_voucher_form1").on('change', '.table_discount', function(event) {
   var current_discount = $(this).val();
    var row_number = $(this).attr('name').split('_')[1];
    var previous_subtotal = (render_data['item_info'][row_number]['selling_price'] * render_data['item_info'][row_number]['quantity']) -(render_data['item_info'][row_number]['new_item_discount']||0);

    var new_subtotal = (render_data['item_info'][row_number]['selling_price'] * render_data['item_info'][row_number]['quantity']) -(current_discount ||0);;

    if(parseFloat(new_subtotal)>parseFloat(previous_subtotal))
    {
        var add_price = new_subtotal - previous_subtotal;
        var discount  = $(this).parent().parent().find("td:nth-child(5)").text();
        current_total = $('input[name=net_payable]').val();
        total_paid    = $('input[name=paid_new]').val();
        total_receivable = $('input[name=due]').val();
        var grand_total = $("#grand_total").val();
        var new_item_price = add_price;
        total_due = $('input[name=total_due]').val() ||0;
        total_return = $('input[name=return_money]').val();
        if(total_return>0)
        {
            if(parseFloat(new_item_price)>parseFloat(total_return))
            {
                $('input[name=total_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=total_hidden_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=return_money]').val(0);
                $('#paid_now_div').show();
            }
            else if(parseFloat(total_return)>= parseFloat(new_item_price))
            {
                $('input[name=total_due]').val(0);
                $('input[name=total_hidden_due]').val(0);
                $('input[name=return_money]').val(Number(parseFloat(total_return)-parseFloat(new_item_price)).toFixed(2));
            }
        }
        else if(total_due>=0)
        {
           $('input[name=total_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
           $('input[name=total_hidden_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
           $('input[name=return_money]').val(0);
           $('#paid_now_div').show();
       }
   }
   else
   {
    var return_price = previous_subtotal - new_subtotal;
    var discount = $(this).parent().parent().find("td:nth-child(5)").text();

    current_total = $('input[name=net_payable]').val();
    total_paid = $('input[name=paid_new]').val();
    total_receivable = $('input[name=due]').val();
    total_due = $('input[name=total_due]').val() ||0;
    total_return = $('input[name=return_money]').val() ||0;
    returned_items_price = return_price;

    if(parseFloat(returned_items_price)>parseFloat(total_due))
    {
        $('input[name=total_due]').val(0);  
        $('input[name=total_hidden_due]').val(0);  
        $('input[name=return_money]').val(Number(parseFloat(total_return) + parseFloat(returned_items_price-total_due)).toFixed(2));
    }
    else if(parseFloat(total_due)>= parseFloat(returned_items_price))
    {
        $('input[name=total_hidden_due]').val(Number(total_due-returned_items_price).toFixed(2));
        $('input[name=total_due]').val(Number(total_due-returned_items_price).toFixed(2));
        $('#paid_now_div').show();
        $('input[name=return_money]').val(0);
    }
}

render_data['item_info'][row_number]['new_item_discount'] = current_discount;
sell_table_render();
    
});


$("#sell_voucher_form1").on('change', '.table_price', function(event) {
    var new_price = $(this).val();
    var row_number = $(this).attr('name').split('_')[1];
    var previous_subtotal = render_data['item_info'][row_number]['selling_price'] * render_data['item_info'][row_number]['quantity'];
    var new_subtotal = render_data['item_info'][row_number]['quantity']*new_price;

    if(parseFloat(new_subtotal)>parseFloat(previous_subtotal))
    {
        var add_price = new_subtotal - previous_subtotal;
        var discount  = $(this).parent().parent().find("td:nth-child(5)").text();
        current_total = $('input[name=net_payable]').val();
        total_paid    = $('input[name=paid_new]').val();
        total_receivable = $('input[name=due]').val();
        var grand_total = $("#grand_total").val();
        var new_item_price = add_price;
        total_due = $('input[name=total_due]').val() ||0;
        total_return = $('input[name=return_money]').val();
        if(total_return>0)
        {
            if(parseFloat(new_item_price)>parseFloat(total_return))
            {
                $('input[name=total_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=total_hidden_due]').val(Number(new_item_price-total_return).toFixed(2));  
                $('input[name=return_money]').val(0);
                $('#paid_now_div').show();
            }
            else if(parseFloat(total_return)>= parseFloat(new_item_price))
            {
                $('input[name=total_due]').val(0);
                $('input[name=total_hidden_due]').val(0);
                $('input[name=return_money]').val(Number(parseFloat(total_return)-parseFloat(new_item_price)).toFixed(2));
            }
        }
        else if(total_due>=0)
        {
           $('input[name=total_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
           $('input[name=total_hidden_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
           $('input[name=return_money]').val(0);
           $('#paid_now_div').show();
       }
   }
   else
   {
    var return_price = previous_subtotal - new_subtotal;
    var discount = $(this).parent().parent().find("td:nth-child(5)").text();

    current_total = $('input[name=net_payable]').val();
    total_paid = $('input[name=paid_new]').val();
    total_receivable = $('input[name=due]').val();
    total_due = $('input[name=total_due]').val() ||0;
    total_return = $('input[name=return_money]').val() ||0;
    returned_items_price = return_price;

    if(parseFloat(returned_items_price)>parseFloat(total_due))
    {
        $('input[name=total_due]').val(0);  
        $('input[name=total_hidden_due]').val(0);  
        $('input[name=return_money]').val(Number(parseFloat(total_return) + parseFloat(returned_items_price-total_due)).toFixed(2));
    }
    else if(parseFloat(total_due)>= parseFloat(returned_items_price))
    {
        $('input[name=total_hidden_due]').val(Number(total_due-returned_items_price).toFixed(2));
        $('input[name=total_due]').val(Number(total_due-returned_items_price).toFixed(2));
        $('#paid_now_div').show();
        $('input[name=return_money]').val(0);
    }
}

render_data['item_info'][row_number]['selling_price'] = new_price;
sell_table_render();
});




$("#item_select").keyup(function(event) 
{
    $("#item_select_result").show();
    $("#item_select_result").html('');
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        var search_item = $("#item_select").val();
        var html = '';
        $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            console.log(data);
            if(data.imei_barcode_yes != null && data.imei_barcode_yes != undefined)
            {
                $.each(data, function(index, val) {
                    html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">' + val.spec_set+'</td></tr>';
                });
                $("#item_select_result").html(html);
                $('input[name=new_item_imei]').val(data.imei_barcode_yes.imei_barcode);
                $('input[name=quantity]').val(1);
                $('input[name=item_type]').val("barcode");
                $('#item_select_result').find('td').eq(0).click();
                $('#add_item').prop('disabled', false);
               // $('#add_item').click(); 
           }
           else if(data.barcode_yes != null && data.barcode_yes != undefined)
           {
            $.each(data, function(index, val) {
                html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+ val.spec_set+'</td></tr>';
            });
            $("#item_select_result").html(html);
            $('input[name=new_item_imei]').val("");
            $('input[name=quantity]').val(1);
            $('#item_select_result').find('td').eq(0).click();
            $('input[name=item_type]').val("barcode");
            $('#add_item').prop('disabled', false);
               // $('#add_item').click(); 
           }
           else
           {
            $.each(data, function(index, val) {
                if(data['item_image'] == null){
                    val.item_spec_set_image    = "images/item_images/no_image.png";
                }
                var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'" quantity="'+val.quantity+'">'+image_html+' '+ val.spec_set+'</td></tr>';
            });
            $("#item_select_result").html(html);
            $('input[name=new_item_imei]').val("");
            $('input[name=item_type]').val("");
        }
    });
    }, 500);
});

$("#item_select_result").on('click', 'td', function(event) {
    event.preventDefault();
    $('input[name="items_name"]').val($(this).text());
    var item_type = $('input[name=item_type]').val();
    $('input[name="item_spec_set_id"]').val($(this).attr('data'));
    $('input[name="inventory_quantity"]').val($(this).attr('quantity'));
    var is_unique_barcode = $(this).attr('is_unique');
    var imei = $('input[name=new_item_imei]').val();
    if(is_unique_barcode=="yes" && imei=="")
    {
        $("#item_select_result").hide();
        $('input[name=item_type]').val("");
        $('input[name="quantity"]').val('');
        $('input[name="items_name"]').val('');
        $('input[name="item_spec_set_id"]').val('');
        $('input[name="inventory_quantity"]').val('');
        $('input[name="retail_price"]').val('');
        $('input[name="retail_price_hidden"]').val('');
        alert("Imei item should sell by Imei number");
        $('input[name=items_name]').focus();
    }
    else
    {
        var item_spec_set_id = $(this).attr('data');
        var post_data ={
            'item_spec_set_id' : item_spec_set_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+item_spec_set_id,post_data).done(function(data) 
        {
            var data2 = $.parseJSON(data);
            $('input[name="retail_price_hidden"]').val(data2.retail_price);
            $('input[name="retail_price"]').val(data2.retail_price);

            if(item_type=="barcode")
            {
                render_data['item_info'].push({
                    'item_spec_set_id' : $('input[name="item_spec_set_id"]').val(),
                    'item_name' : $('#item_select').val(),
                    'quantity' : $('input[name="quantity"]').val(),
                    'item_imei_number' : $('input[name="new_item_imei"]').val(),
                    'new_item_discount' : $('input[name="new_item_discount"]').val() ||0,
                    'selling_price' : $('input[name="retail_price_hidden"]').val(),
                    'whole_sale_price' : $ ('input[name="whole_sale_price"]').val(),
                    'discount_type' : $('#dis_type').val(),
                });

                current_total = $('input[name=net_payable]').val();
                total_paid = $('input[name=paid_new]').val();
                total_receivable = $('input[name=due]').val();
                var grand_total = $("#grand_total").val();
                var new_item_price = parseFloat(($('input[name="retail_price_hidden"]').val())*$('input[name="quantity"]').val())- parseFloat($('input[name="new_item_discount"]').val()||0);
                total_due = $('input[name=total_due]').val() ||0;
                total_return = $('input[name=return_money]').val();
                if(total_return>0)
                {
                    if(parseFloat(new_item_price)>parseFloat(total_return))
                    {
                        $('input[name=total_due]').val(Number(new_item_price-total_return).toFixed(2));  
                        $('input[name=total_hidden_due]').val(Number(new_item_price-total_return).toFixed(2));  
                        $('input[name=return_money]').val(0);
                        $('#paid_now_div').show();
                    }
                    else if(parseFloat(total_return)>= parseFloat(new_item_price))
                    {
                        $('input[name=total_due]').val(0);
                        $('input[name=total_hidden_due]').val(0);
                        $('input[name=return_money]').val(Number(parseFloat(total_return)-parseFloat(new_item_price)).toFixed(2));
                    }
                }
                else if(total_due>=0)
                {
                   $('input[name=total_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
                   $('input[name=total_hidden_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2)); 
                   $('input[name=return_money]').val(0);
                   $('#paid_now_div').show();
               }

               $("#save_sell").attr("disabled",false);
               sell_table_render();

               $('input[name="item_spec_set_id"]').val('');
               $('#item_select').val('');
               $('input[name="item_type"]').val('');
               $('input[name="quantity"]').val('');
               $('input[name="inventory_quantity"]').val('');
               $('input[name="selling_price"]').val('');
               $('input[name="new_item_discount"]').val('');
               $('input[name="new_item_imei"]').val('');
               $('input[name="retail_price"]').val('');
               $('input[name="retail_price_hidden"]').val('');
               $('input[name="whole_sale_price"]').val('');
               $('input[name="discount_amount"]').val('');
               $('#dis_type').val('');
               $('#button_extra').show();
           }
           else
           {

           }
       });
        $("#item_select_result").hide();
    }
});

$('#item_quantity').keyup(function(event) {
    var item_new_quantity = $(this).val();
    var per_item_price = $('input[name=retail_price_hidden]').val();
    $('input[name=retail_price]').val(item_new_quantity*per_item_price);
});
$('body').tooltip({
    selector: '.fa.fa-question-circle.tooltips'
});

</script>
