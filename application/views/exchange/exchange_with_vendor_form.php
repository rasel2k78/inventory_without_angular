<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">

<style type="text/css">
.nmpd-grid {border: none; padding: 20px;}
.nmpd-grid>tbody>tr>td {border: none;}

/* Some custom styling for Bootstrap */
.qtyInput {display: block;
    width: 100%;
    padding: 6px 12px;
    color: #555;
    background-color: white;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->


<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="exchange_success" style="display:none" role="alert"><?php echo $this->lang->line('exchange_success'); ?></div>

        <div class="alert alert-danger" id="no_voucher_or_not_found" style="display:none" role="alert"><?php echo $this->lang->line("no_voucher"); ?></div>


        <div class="alert alert-danger" id="vendor_not" style="display:none" role="alert">Exchange in due is not possible without Vendor</div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('exchange_with_vendor_info'); ?>
                </div>
            </div>
            <div class="portlet-body form" id="form1">
                <!-- BEGIN FORM-->
                <!-- <form id="brand_form" class="form-horizontal form-bordered" action="<?php echo site_url('/Brand/save_brand_info');?>" method="post" enctype="multipart/form-data"> -->
                    <?php $form_attrib = array('id' => 'exchange_vendor_form','class' => 'form-horizontal form-bordered');
                    echo form_open('',  $form_attrib, '');?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                <?php echo $this->lang->line('voucher_no'); ?> </label>
                                <div class="col-md-9 input-form input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('get_voucher_tt'); ?>"></i>
                                    <input type="text"  class="form-control" name="buy_voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn green"><?php echo $this->lang->line('find'); ?> </button>
                            <button type="button" id="cancle_voucher" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- </div> -->


            <!-- <div class="col-md-6"> -->
                <div class="portlet box red" id="buy_voucher_details_form" style="display:none" >
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i><span id="form_header_text" ><?php echo $this->lang->line('item_buy_details'); ?></span>
                        </div>

                    </div>
                    <div class="portlet-body form"  >
                        <!-- BEGIN FORM-->
                        <div class="form-body">
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>" readonly="true">
                            <input type="hidden" name="buy_details_id" id="buy_details_id">
                        </div>
                    </div> -->


                    <div class="form-group">
                        <label class="control-label col-md-3">Grand Total</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="grand_total" id="grand_total" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Net Payable</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Discount</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="total_discount" id="total_discount" placeholder="Total Discount" readonly="true">
                        </div>
                    </div>


                    <div class="form-group" style="display: none;">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('total_paid'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid_new" placeholder="<?php echo $this->lang->line('total_paid'); ?>" readonly="true">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Total Due</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="total_due" placeholder="Total Due" readonly="true" style="background: lightblue;">
                        </div>
                    </div>
                    <div class="form-group" id="return_money_div">
                        <label class="control-label col-md-3">Money Return</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="return_money" placeholder="<?php echo $this->lang->line('return_money'); ?>" readonly="true" style="background: lightblue;">
                        </div>
                    </div>

                    <div class="form-group" id="paid_now_div" style="display: none;">
                        <label class="control-label col-md-3">Paid</label>
                        <div class="col-md-9">
                            <input style="border:1px solid #3B9C96;" type="text" class="form-control" name="paid_now" id="paid_now" placeholder="paid amount">
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <table id="table_items" class="table table-hover control-label col-md-3">
                        <thead>
                            <tr >
                                <th class="control-label col-md-3"><?php echo $this->lang->line('item_name'); ?></th>
                                <th class="control-label col-md-2"><?php echo $this->lang->line('quantity'); ?></th>    <th class="control-label col-md-2"><?php echo $this->lang->line('buying_price_unit'); ?></th>
                                <th>Discount</th>
                                <th class="control-label col-md-3"><?php echo $this->lang->line('sub_total'); ?></th>
                                <th class="control-label col-md-2">Total Qty</th>
                                <th class="control-label col-md-2">Imei</th>
                                <th class="control-label col-md-3"><?php echo $this->lang->line('new_quantity'); ?>
                                </th>
                                <th class="control-label col-md-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>


                <?php $form_attrib = array('id' => 'exchange_customeasdfasdfrs_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo $this->lang->line('item'); ?>
                        </label>
                        <div class="col-md-9 input-form input-icon">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('select_item_tt'); ?>"></i>
                            <input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('item'); ?>" id="item_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="item_spec_set_id"  class="form-control">
                            <input type="hidden" name="is_unique_barcode" id="is_unique_barcode">
                            <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo $this->lang->line('total_quantity'); ?>
                        </label>
                        <div class="col-md-9 ">
                            <!-- <input type="text" class="form-control" name="quantity"  placeholder="মোট পরিমাণ" > -->
                            <div class="input-group input-form input-icon">
                                <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Select new item to exchange"></i> -->

                                <input type="number" id="item_quantity" name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('total_quantity'); ?>" aria-describedby="numpadButton-btn">

                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="numpadButton-btn" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                </span>
                            </div>
                            <span class="help-block" style="display:none"><?php echo $this->lang->line('give_item_quantity'); ?></span>
                        </div>
                    </div>

                    <div class="form-group" id="unique_barcode_div">

                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo $this->lang->line('buying_price_unit'); ?>
                        </label>
                        <div class="col-md-9 input-form input-icon">
                            <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Select new item to exchange"></i> -->
                            <input type="number" class="form-control" name="buying_price"  placeholder="<?php echo $this->lang->line('buying_price'); ?>">
                            <span class="help-block" style="display:none"><?php echo $this->lang->line('give_buying_price'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            <?php echo $this->lang->line('sell_price_unit'); ?>
                            <!-- <i class="fa fa-question-circle tooltips" data-container="body" data-placement="top" data-original-title="Item selling price"></i> -->
                        </label>
                        <div class="col-md-9 input-form input-icon">
                            <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Select new item to exchange"></i> -->
                            <input type="number" class="form-control" name="retail_price"  placeholder="<?php echo $this->lang->line('single_price'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sell_price_whole'); ?></label>
                        <div class="col-md-9 input-form input-icon">
                            <!-- <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Select new item to exchange"></i> -->
                            <input type="number" class="form-control" name="whole_sale_price"  placeholder="<?php echo $this->lang->line('whole_price'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3" for="new_item_discount">Idividual Item Discount</label>
                        <div class="col-md-9 input-form input-icon">
                            <input type="text" class="form-control" id="new_item_discount" name="new_item_discount" placeholder="individual discount optional">
                        </div>
                    </div>



                    <!-- <div class="form-group">
                        <div class="col-md-offset-3">
                            <button id="button_extra" type="button" class="btn green"> অতিরিক্ত অংশ </button>
                        </div>
                    </div> -->
                    <div id="extra_fields" style="display:none">

                        <div class="form-group">
                            <label class="control-label col-md-3">মেয়াদ উত্তীর্ণের তারিখ</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" name="expire_date" placeholder="মেয়াদ উত্তীর্ণের তারিখ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">মূল্যছাড়ের পরিমাণ
                            </label>
                            <div class="col-md-3">
                                <select class="form-control" id="dis_type" name="discount_type">
                                    <option ></option>
                                    <option value="amount">টাকা</option>
                                    <option value="percentage">%</option>
                                    <option value="free_quantity">টা(পণ্য)</option>
                                </select>
                            </div>
                            <input type="text" name="discount_amount" placeholder="ছাড়ের পরিমাণ প্রদান করুন">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">মন্তব্য</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="comments"  placeholder="মন্তব্য">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn green" id="add_item"><?php echo $this->lang->line('add_new_item'); ?></button>
                    <button type="button" id="cancel_item" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
    </div>





    <div class="col-md-6">    
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert">
            <?php echo $this->lang->line('not_permitted'); ?> 
        </span>
    </div>
    <div class="portlet box green-haze" id="buy_voucher_form1" style="display:none;">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <?php echo $this->lang->line('left_portlet_title'); ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="voucher_no" placeholder="রসিদ নং">
                        <!-- <span class="help-block" style="display:none">রসিদ নাম্বার প্রদান করুন</span> -->
                    </div>
                </div>

            </div>    

            <table id="buy_table" class="table table-condensed">
                <thead>
                    <tr>
                        <th>
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_list_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('item'); ?>
                        </th>
                        <th><?php echo $this->lang->line('quantity'); ?></th>
                        <th><?php echo $this->lang->line('buying_price'); ?></th>
                        <th>Net buying price</th>
                        <th>Net sub-total</th>
                        <th>Imei Number</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('total_price_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('total'); ?></th>
                        <th id="total_exchanged"></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- END EXAMPLE TABLE PORTLET-->




    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box green-haze" id="buy_voucher_form2" style="display:none">

        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i>
                <?php echo $this->lang->line('new_item_buy_info'); ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="new_voucher" name="voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>">
                        <!-- <span class="help-block" style="display:none">রসিদ নাম্বার প্রদান করুন</span> -->
                    </div>
                </div>
            </div>    
            <table id="buy_table2" class="table table-condensed">
                <thead>
                    <tr>
                        <th>
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_new_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('item'); ?></th>
                            <th><?php echo $this->lang->line('total_quantity'); ?></th>
                            <th><?php echo $this->lang->line('buying_price'); ?></th>
                            <th>Sub-toal</th>
                            <th>Individual Discount</th>
                            <!-- <th>ছাড়ের ধরন</th> -->
                            <th>Net sub-total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Total new item price</th>
                            <th></th>
                            <th id="total_amount"></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th></th><th></th><th></th>    <th></th>    
                            <th><button type="submit" id="save_buy" class="btn green"><?php echo $this->lang->line('save'); ?></button></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>


    </div>

</div>



<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->    
<script>
    var csrf = $("input[name='csrf_test_name']").val();
    var timer;
    function change_form()
    {
        $('#form1').hide();
        $('#form2').show();
    }

    $('#button_extra').click(function(event) {
        $('#extra_fields').toggle('slow');
    });

    table_brand = $('#sample_2').DataTable();


    var render_data= {
        'imei_barcode' :[],
        'item_info' : [],
        'all_basic_data':{},
    };

    var render_new_data= {
        'item_info_new' : [],
        'all_basic_data':{},
    };

    $('#cancle_voucher').click(function(event) {
        $('input[name="buy_voucher_no"]').val('');
        $('input[name="buy_voucher_no"]').val('');
        $('input[name="voucher_no"]').val('');
        $("#buy_table > tbody").html("");
        $("#buy_table2 > tbody").html("");
        //$('#buy_voucher_form1').hide();
        $('#total_exchanged').html('');
        $('#total_payable_with_returned').html('');
        $('#total_difference').html('');
        $('#total_new_amount').html('');
        $('#total_amount').html('');
        $("#table_items > tbody").html("");
        $("#total_due").html('');
        all_data.length =0;
    });


    $('#cancel_item').click(function(event) {
        alert('sdf');
        $('#item_select').val('');
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
    });


    var buy_item_id = '' ;
    var selected_name = '';

    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        $('table').on('click', '.ok_button',function(event) {
            event.preventDefault();
            $('#buy_voucher_form1').show(400);
            updated_buy_item_id = $(this).attr('id').split('_')[2];
            updated_item_name = $(this).parent().parent().find("td:nth-child(1)").text();
            current_item_quantity = $(this).parent().parent().find("td:nth-child(2)").text();
            updated_item_retail_price = $(this).parent().parent().find("td:nth-child(3)").text();
            item_quantity_left = $(this).parent().parent().find("td:nth-child(6)").text();
            item_imei_number = $(this).parent().parent().find("td:nth-child(7)").text();

            updated_item_quantity = $(this).parent().parent().find("input:first").val();
            item_subtotal = $(this).parent().parent().find("td:nth-child(5)").text();
            current_total = $('input[name=net_payable]').val();
            total_paid = $('input[name=paid_new]').val();
            total_receivable = $('input[name=due]').val();
            total_due = $('input[name=total_due]').val() ||0;
            total_return = $('input[name=return_money]').val() ||0;

            var grand_total = $("#grand_total").val();
            var total_discount = $("#total_discount").val() ||0;
            var unit_price_with_total_discount = Number((parseFloat(total_discount/grand_total)).toFixed(5)) ||0;
            var sub_total_after_total_discount = Number(parseFloat(item_subtotal -parseFloat(item_subtotal*unit_price_with_total_discount))).toFixed(5) ||0;
            var item_unit_price_after_total_individual_discount = parseFloat(sub_total_after_total_discount/current_item_quantity) ||0;
            returned_items_price = Number(parseFloat(item_unit_price_after_total_individual_discount*updated_item_quantity)).toFixed(2) ||0;



            if($.isNumeric(updated_item_quantity))
            {
                if(updated_item_quantity.length===0)
                {
                    alert('No amount changed yet');
                    $(this).parent().parent().find("input:first").val('');
                    exit;
                }
                else if (parseInt(updated_item_quantity)>parseInt(item_quantity_left)) 
                {
                    alert('Exchane item number excedded');
                    $(this).parent().parent().find("input:first").val('');
                    exit;    
                }
                else
                {

                    $(this).parent().parent().find("td:nth-child(6)").text(parseInt(item_quantity_left)-parseInt(updated_item_quantity));

                    $("#return_money_div").show();

                    if(parseFloat(returned_items_price)>parseFloat(total_due))
                    {
                        $('input[name=total_due]').val(0);  
                        $('input[name=return_money]').val(Number(parseFloat(total_return) + parseFloat(returned_items_price-total_due)).toFixed(2));
                    }
                    else if(parseFloat(total_due)>= parseFloat(returned_items_price))
                    {
                        $('input[name=total_due]').val(Number(total_due-returned_items_price).toFixed(2));
                        $('#paid_now_div').show();
                        $('input[name=return_money]').val(0);
                    }

                    $('#sell_voucher_form1').show(400);
                    $('#sell_voucher_details_form').show(400);
                    $('#sell_voucher_form1').show(400);
                    $('#sell_voucher_form2').show(400);


                    var item_spec_set_id = updated_buy_item_id;
                    var exchanged_quantity = updated_item_quantity;
                    var voucher_no = $('input[name=voucher_no]').val();
                    var post_data ={
                        'item_spec_set_id' : item_spec_set_id,
                        'voucher_no' : voucher_no,
                        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
                    }; 
                    var found = false;
                    $.post('<?php echo site_url();?>/Exchange_with_vendors/check_buy_cart_items/'+item_spec_set_id,post_data).done(function(data) {
                        var data2 = $.parseJSON(data);
                        if(parseInt(exchanged_quantity)<=parseInt(data2))
                        {
                         $.each(render_new_data['item_info_new'], function(index, val) {
                            if(val.item_spec_set_id == item_spec_set_id)
                            {
                                if(!val.item_imei_number)
                                {
                                    render_new_data['item_info_new'][index]['quantity'] =parseInt(render_new_data['item_info_new'][index]['quantity']) + parseInt(updated_item_quantity);
                                    found = true;
                                } 
                            }
                        });
                         if(found==true)
                         {

                         }
                         else
                         {
                            render_new_data['item_info_new'].push({
                                'item_spec_set_id' : updated_buy_item_id,
                                'item_name' : updated_item_name,
                                'quantity' : updated_item_quantity,
                                'buying_price' : updated_item_retail_price,
                                'buying_price_final' : item_unit_price_after_total_individual_discount,
                                'item_imei_number' : item_imei_number,
                            })
                        }

                        $(this).parent().parent().find("input:first").val('');

                        buy_table_new_render();
                    }
                    else
                    {
                        alert("<?php echo $this->lang->line('exchanged_quantity_exceded'); ?>");
                    }
                }).error(function() {
                    alert("<?php echo $this->lang->line('sorry'); ?>");                
                });    
                $(this).parent().parent().find("input:first").val('');
            }
        }
        else
        {
            alert("<?php echo $this->lang->line('give_numeric'); ?>");
            $(this).parent().parent().find("input:first").val('');
            exit;
        }
    });

}, 500); 

var total_amount = 0;
var total_net_payable= 0;

function buy_table_new_render (){
    var buy_table_new_html = '';
    total_amount = 0;
    var subtotal_amount = 0;
    $.each(render_new_data['item_info_new'], function(index, val) {
        subtotal_amount+= val.quantity*val.buying_price;
        buy_table_new_html += '<tr id="'+index+'">';
        buy_table_new_html += '<td>'+val.item_name+'</td>';
        buy_table_new_html += '<td>'+val.quantity+'</td>';
        buy_table_new_html += '<td>'+val.buying_price+'</td>';
        buy_table_new_html += '<td>'+val.buying_price_final+'</td>';
        buy_table_new_html += '<td>'+val.quantity*val.buying_price_final+'</td>';
        buy_table_new_html += '<td>'+val.item_imei_number || ''+'</td>';
        buy_table_new_html += '</tr>';
        total_amount = subtotal_amount;
    });

    render_new_data['all_basic_data']['net_payable']=total_amount;
    render_new_data['all_basic_data']['due']=total_amount;

    $('#buy_table tbody').html(buy_table_new_html);
    $('#total_exchanged').html(subtotal_amount);
    $('#total_net_payable_new').html(total_amount);
    $('#total_due_new').html(total_amount);

    var    new_payable = parseInt($('#total_new_amount').html() || 0) - parseInt($('#total_exchanged').html() || 0);
    $('#total_payable_with_returned').html(new_payable);
    if(new_payable>=0){$('#payable_message').html('<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('amount_exchange_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('payable'); ?>');}
    else{$('#payable_message').html('<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('amount_exchange_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('receivable'); ?>');}

    //var paid = 0;
    var paid = $('#total_paid').html();
    //var moneyAfterPaid = $('#total_difference').html(parseInt(new_payable || 0)-parseInt(paid || 0));
    var moneyAfterPaid = parseInt(new_payable || 0)-parseInt(paid || 0);
    $('#total_difference').html(moneyAfterPaid);
    if(moneyAfterPaid>=0)
    {
        $('#money_after_paid').html('<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('amount_after_paid_tt'); ?>"></i>&nbsp;Due');
    }
    else
    {
        $('#money_after_paid').html('<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('amount_after_paid_tt'); ?>"></i>&nbsp;<?php echo $this->lang->line('receivable'); ?>');
    }
}

$('.form-actions').on('click', '#add_item', function(event) {
    event.preventDefault();
    $('#item_select').css('border', 'inherit');
    var error_found=false;
    if($('input[name=voucher_no]').val().trim()=="")
    {
        $('input[name=voucher_no]').closest('.form-group').addClass('has-error');
        $('input[name=voucher_no]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
        $('input[name=voucher_no]').parent().find('.help-block').hide();
    }
    if($('input[name="item_spec_set_id"]').val()=="")
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name="item_spec_set_id"]').closest('.form-group').removeClass('has-error');
        $('input[name="item_spec_set_id"]').parent().find('.help-block').hide();

    }
    if($('input[name=quantity]').val().trim()=="")
    {
        $('input[name=quantity]').closest('.form-group').addClass('has-error');
        $('input[name=quantity]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=quantity]').closest('.form-group').removeClass('has-error');
        $('input[name=quantity]').parent().find('.help-block').hide();
    }
    if($('input[name=buying_price]').val().trim()=="")
    {
        $('input[name=buying_price]').closest('.form-group').addClass('has-error');
        $('input[name=buying_price]').parent().find('.help-block').show();
        error_found=true;
    }
    else
    {
        $('input[name=buying_price]').closest('.form-group').removeClass('has-error');
        $('input[name=buying_price]').parent().find('.help-block').hide();
    }


    var imei_error = false;

    if($("#is_unique_barcode").val()=="yes")
    {

        var imei_barcodes = [];
        $("input[name='unique_barcode_value[]']").each(function() 
        {
            if($(this).val()=="")
            {

                $(this).css("border-color", "red"); 
                imei_error = true;
            }
            else
            {
                $(this).css("border-color", "");
                imei_barcodes.push({
                    'item_spec_set_id' : $ ('input[name="item_spec_set_id"]').val(),
                    'imei_barcode': $(this).val(),
                })
            }

        });
        render_data['imei_barcode'].push(imei_barcodes); 
    }


    if(error_found)
    {
        return false;
    }


    if(imei_error)
    {
        if(confirm("Want to serial number empty?"))
        {
            imei_error = true;
               // $("#unique_barcode_div").html("");
               //return true;
           }
           else
           {
            render_data['imei_barcode'].length=0;
            return false;
        }
    }


    if($("#is_unique_barcode").val()=="yes")
    {
        var unique_barcode_exist = $('input[name*="unique_barcode_value[]"]').length;
        var current_quantity = $('input[name="quantity"]').val();
        if(current_quantity!=unique_barcode_exist)
        {
            alert("Please make equal quantity of imei field");
            $(".imei_remove_class").show();
            return false;
        }
    }

    var item_spec_set_id  = $('input[name="item_spec_set_id"]').val();
    current_total = $('input[name=net_payable]').val();
    total_paid = $('input[name=paid_new]').val();
    total_receivable = $('input[name=due]').val();
    var grand_total = $("#grand_total").val();
    var new_item_price = parseFloat(($('input[name="buying_price"]').val())* $('input[name="quantity"]').val())- parseFloat($('input[name="new_item_discount"]').val()||0);
    total_due = $('input[name=total_due]').val() ||0;
    total_return = $('input[name=return_money]').val() ||0;
    if(total_return>0)
    {
        if(parseFloat(new_item_price)>parseFloat(total_return))
        {
            $('input[name=total_due]').val(Number(new_item_price-total_return).toFixed(2));  
            $('input[name=return_money]').val(0);
            $('#paid_now_div').show();
        }
        else if(parseFloat(total_return)>= parseFloat(new_item_price))
        {
            $('input[name=total_due]').val(0);
            $('input[name=return_money]').val(Number(parseFloat(total_return)-parseFloat(new_item_price)).toFixed(2));
        }
    }
    else if(total_due>0)
    {
       $('input[name=total_due]').val(Number(parseFloat(total_due)+parseFloat(new_item_price)).toFixed(2));  
       $('input[name=return_money]').val(0);
       $('#paid_now_div').show();
   }

   var found = false;
   $.each(render_data['item_info'], function(index, val) {
    if(val.item_spec_set_id == item_spec_set_id)
    {
        render_data['item_info'][index]['quantity'] =parseInt(render_data['item_info'][index]['quantity']) + parseInt($('input[name="quantity"]').val());
        found = true;
    }
});

   if(found==true)
   {

   }
   else
   {
     render_data['item_info'].push({
        'item_spec_set_id' : $('input[name="item_spec_set_id"]').val(),
        'item_name' : $('#item_select').val(),
        'quantity' : $('input[name="quantity"]').val(),
        'buying_price' : $('input[name="buying_price"]').val(),
        'new_item_discount' : $('input[name="new_item_discount"]').val() ||0,
                    // 'buying_price_with_landed_cost' : $('input[name="buying_price_with_landed_cost"]').val(),
                    'retail_price' : $ ('input[name="retail_price"]').val(),
                    'whole_sale_price' : $ ('input[name="whole_sale_price"]').val(),
                    'expire_date' : $ ('input[name="expire_date"]').val(),
                    'comments' : $ ('input[name="comments"]').val(),
                    'unique_barcode' : $("#is_unique_barcode").val(),
                    // 'discount_amount' : $ ('input[name="discount_amount"]').val(),
                    // 'discount_type' : $('#dis_type').val(),

                })
 }

 buy_table_render();

        //  clear the whole form rather than individual        **
        $('#item_select').val('');
        $('input[name="item_spec_set_id"]').val('');
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        $("#unique_barcode_div").html("");
        $("#is_unique_barcode").val('');
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
        $('input[name="expire_date"]').val('');
        $('input[name="comments"]').val('');
        $('input[name="new_item_discount"]').val('');
        $('#dis_type').val('');
        $('#button_extra').show();
    });

var total_amount = 0;
var total_net_payable= 0;

function buy_table_render () {
    var buy_table_html = '';
    total_amount = 0;
    var subtotal_amount = 0;

    $.each(render_data['item_info'], function(index, val) {
        buy_table_html += '<tr id="'+index+'">';
        buy_table_html += '<td>'+val.item_name+'</td>';
        buy_table_html += '<td>'+val.quantity+'</td>';
        buy_table_html += '<td>'+val.buying_price+'</td>';
        subtotal_amount+= (val.quantity*val.buying_price)-(val.new_item_discount||0);
        var net_subtotal = (val.quantity*val.buying_price)-(val.new_item_discount||0);
        buy_table_html += '<td>'+val.quantity*val.buying_price+'</td>';
        buy_table_html += '<td>'+val.new_item_discount+'</td>';
        buy_table_html += '<td>'+net_subtotal+'</td>';
        buy_table_html += '</tr>';
        total_amount = subtotal_amount;
    });

    render_data['all_basic_data']['net_payable']=total_amount;
    render_data['all_basic_data']['due']=total_amount;
    $('#buy_table2 tbody').html(buy_table_html);
    $('#total_amount').html(total_amount);
}


render_data['all_basic_data']['discount']=0;
render_data['all_basic_data']['net_pay_after_discount']=0;

$('#total_discount').keyup(function(event) {
    var net_pay_after_discount = parseFloat(total_amount) - parseFloat($( this ).val());
    $('#total_net_payable').text(net_pay_after_discount);
    $('#total_due').text(net_pay_after_discount);

    render_data['all_basic_data']['discount']=parseFloat($(this).val());
    render_data['all_basic_data']['net_pay_after_discount']=net_pay_after_discount;
    // console.log(render_data['all_basic_data']);


});
var current_cashbox_amount = <?php echo $cashbox_amount['total_cashbox_amount']?>;

// $('#total_paid').keyup(function(event) {
//     var current_due = parseInt($('#total_payable_with_returned').html());
//     var paid_now = $(this).val();
//     if(paid_now=='' || 0)
//     {
//         $('#total_difference').html(current_due);
//     }
//     else
//     {
//         $('#total_difference').html((parseInt($('#total_payable_with_returned').html()) - parseInt(paid_now)));
//     }

// });


$('#paid_now').keyup(function(event) 
{
    var current_due = $('input[name="total_due"]').val()||0;
    var paid;
    
    paid = $(this).val();

    clearTimeout(timer);
    timer = setTimeout(function() 
    {

        if(paid=='' || 0)
        {
            $('input[name="total_due"]').val(current_due);
        }
        else
        {
            if(parseFloat(paid)>parseFloat(current_due))
            {
                $('input[name="total_due"]').val(0);
            }
            else
            {
                $('input[name="total_due"]').val(parseFloat(current_due)-parseFloat(paid));
            }
        }
    }, 500);
});



$('#buy_table').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_new_data['item_info_new'].splice(clicked_id,1);
    buy_table_new_render();
});

$('#buy_table2').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_data['item_info'].splice(clicked_id,1);
    buy_table_render();
});

$('#buy_table').on('click', '#item_edit', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    //console.log(clicked_id);
    var editing_item_info;
    editing_item_info = render_data['item_info'][clicked_id];

    $('#item_select').selectpicker('val', editing_item_info.item_spec_set_id);
    $('input[name="buying_price"]').val(editing_item_info.buying_price);
    $('input[name="quantity"]').val(editing_item_info.quantity);
    $('input[name="comments"]').val(editing_item_info.comments);
    $('input[name="discount_amount"]').val(editing_item_info.discount_amount);
    $('input[name="discount_type"]').val(editing_item_info.discount_type);
    $('input[name="expire_date"]').val(editing_item_info.expire_date);
    $('input[name="retail_price"]').val(editing_item_info.retail_price);
    $('input[name="whole_sale_price"]').val(editing_item_info.whole_sale_price);


});


$('#save_buy').click(function(event) {
    event.preventDefault();
    $('#item_select').css('border', 'inherit');
    render_data.all_basic_data.voucher_no = $('input[name="voucher_no"]').val();
    render_data.all_basic_data.exchanged = render_new_data['item_info_new'];
    render_data.all_basic_data.add_paid = $('#paid_now').val();
    render_data.all_basic_data.add_due = $('input[name="total_due"]').val();
    render_data.all_basic_data.add_return = $('input[name="return_money"]').val();
    //console.log(render_data.all_basic_data.add_due);
    
    //exit;
    var all_data= {
        'csrf_test_name' :$('input[name=csrf_test_name]').val(),
        'all_data' : render_data,
    }

    if(render_data['item_info'].length==0)
    {
        alert("<?php echo $this->lang->line('not_exchanged'); ?>");
        $('#item_select').css('border', '1px solid red');
        return false;
    }
    if(render_data.length ==0)
    {
        alert("<?php echo $this->lang->line('not_exchanged'); ?>");
        $('#item_select').css('border', '1px solid red');
        return false;
    }

    $.post('<?php echo site_url() ?>/Exchange_with_vendors/save_buy_info',all_data ).done(function(data, textStatus, xhr) {
        $('#final_calculation').show();
        if(data == "no_exchange")
        {
            alert("<?php echo $this->lang->line('not_exchanged'); ?>");
            return false;
        }
        else if(data == "no_new_item")
        {
            alert("<?php echo $this->lang->line('no_new_item'); ?>");
            return false;
        }
        else if(data == "duplicate imei inserted")
        {
            alert("Duplicate imei added");
            return false;
        }
        else if(data == "already sold once and status deactivated")
        {
            alert("Imei already sold once");
            return false;
        }
        else if(data == "No vendor exist")
        {
            $('#vendor_not').slideDown();
            setTimeout( function(){$('#vendor_not').slideUp()}, 2500 );
            return false;
        }
        else if(data == "success")
        {
            $('#exchange_success').slideDown();
            setTimeout( function(){$('#exchange_success').slideUp()}, 1500 );
            $('input[name="buy_voucher_no"]').val('');
            $('input[name="voucher_no"]').val('');
            $("#buy_table > tbody").html("");
            $("#buy_table2 > tbody").html("");
        //$('#buy_voucher_form1').hide();
        $('#total_exchanged').html('');
        $('#total_payable_with_returned').html('');
        $('#total_difference').html('');
        $('#total_new_amount').html('');
        $('#total_amount').html('');
        $("#table_items > tbody").html("");
        $("#total_due").html('');
        all_data.length =0;
        render_data['imei_barcode'].length=0;
        return false;
            //alert("<?php echo $this->lang->line('exchange_success'); ?>");
        }
        // $('#buy_voucher_details_form').hide();
        // $('#buy_voucher_form1').hide();
        // $('#buy_voucher_form2').hide();
        
    }).error(function() {
        alert("<?php echo $this->lang->line('sorry'); ?>");
    });

});

$('#exchange_vendor_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    $('#table_items tbody').html('');
    $('input[name=buy_voucher_no]').css('border', 'inherit');
    render_new_data['item_info_new'].length = 0;
    render_data['item_info'].length = 0;
    $("#buy_table > tbody").html("");
    $("#buy_table2 > tbody").html("");
    $('#total_exchanged').html('');
    $('#total_payable_with_returned').html('');
    $('#total_difference').html('');
    $('#total_new_amount').html('');
    $('#total_amount').html('');
    $("#table_items > tbody").html("");
    data.buy_voucher_no = $('input[name=buy_voucher_no]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/Exchange_with_vendors/get_voucher_details_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        console.log(data2);
        if(data2 == "No permission"){
            $('#not_permitted').slideDown();
            setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
        }
        else if(data2.success=="no items")
        {
            alert('<?php echo $this->lang->line("no_items"); ?>');
            return false;
        }
        else if(data2.success==0)
        {
            $('input[name=buy_voucher_no]').css('border', '1px solid red');
            $('#no_voucher_or_not_found').slideDown();
            setTimeout( function(){$('#no_voucher_or_not_found').slideUp()}, 5000 );
                // alert('<?php echo $this->lang->line("no_voucher"); ?>');
                return false;
            }

            else
            {
                $('#buy_voucher_details_form').show(400);
                $('#buy_voucher_form2').show(400);
                $('input[name = voucher_no]').val(data2.all_buy_info_by_id[0].voucher_no);
                $('input[name = net_payable]').val(data2.all_buy_info_by_id[0].net_payable);
                $('input[name = grand_total]').val(data2.all_buy_info_by_id[0].grand_total);
                $('input[name = total_discount]').val(data2.all_buy_info_by_id[0].discount);
                $('input[name = paid_new]').val(data2.all_buy_info_by_id[0].paid);
                $('input[name = total_due]').val(data2.all_buy_info_by_id[0].due);
                $('input[name = return_money]').val(0);
                $('input[name = buy_details_id]').val(data2.all_buy_info_by_id[0].buy_details_id);

                var table_html = '';
                $.each(data2.all_buy_info_by_id, function(index, val) {
                    table_html += '<tr>'
                    table_html += '<td id="item_name_"'+val.item_spec_set_id+'>'
                    table_html += val.spec_set;
                    table_html += '</td><td id="item_quantity_"'+val.item_spec_set_id+'>';
                    if(val.imei_barcode)
                    {
                        table_html += 1;
                    }
                    else
                    {
                        table_html += val.quantity;
                    }
                    table_html += '</td><td>';
                    table_html += val.buying_price;

                    if(val.discount_type=="amount")
                    {
                        table_html += '</td><td id="buying_price_"'+val.item_spec_set_id+'>';
                        table_html += val.discount_amount;
                    }
                    else if(val.discount_type=="percentage")
                    {
                        discount_in_amount = Number(parseFloat(val.discount_amount/100)*(val.quantity*val.buying_price)).toFixed(2);
                        table_html += '</td><td id="buying_price_"'+val.item_spec_set_id+'>';
                        table_html += discount_in_amount;
                    }
                    else
                    {
                        table_html += '</td><td id="buying_price_"'+val.item_spec_set_id+'>';
                        table_html += val.discount_amount;
                    }

                    table_html += '</td><td id="buying_price_"'+val.item_spec_set_id+'>';
                    if(val.imei_barcode)
                    {
                        table_html += (val.sub_total)/val.quantity;
                    }
                    else
                    {
                     table_html += val.sub_total; 
                 }

                 table_html += '</td><td>';
                 if(val.imei_barcode)
                 {
                    table_html += 1;
                }
                else
                {
                    table_html += val.quantity;
                }

                table_html += '</td><td>';
                table_html += val.imei_barcode?val.imei_barcode:"";
                table_html += '</td><td>';
                table_html += '<input style="border:1px solid #45b6af !important" type="text" name="new_amount" class="form-control exchanged_quantity">';
                table_html += '</td>';
                table_html += '<td><button style="border:1px solid #45b6af !important" type="button" class="form-control ok_button" id="new_amount_'+val.item_spec_set_id+'">Ok</button></td>';
                table_html += '</tr>';
            });
                $('#table_items tbody').html(table_html);
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();
            }
        }).error(function() {
            $('#insert_failure').slideDown();
            setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        });    
    });    


var timer;

$("#item_select").keyup(function(event) 
{

    $("#item_select_result").show();
    $("#item_select_result").html('');
    $("#unique_barcode_div").html("");
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        var search_item = $("#item_select").val();
        var html = '';
        $.post('<?php echo site_url(); ?>/buy/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            if(data.barcode_yes != null){
                $.each(data, function(index, val) {
                    if(val.item_spec_set_image == null || val.item_spec_set_image == '')
                    {
                        val.item_spec_set_image    = "images/item_images/no_image.png";
                    }
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                    html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                });
                $("#item_select_result").html(html);
                $('#item_select_result').find('td').eq(0).click();
                $('input[name=quantity]').focus();
            }
            else
            {
                $.each(data, function(index, val) 
                {
                    if(val.item_spec_set_image == null || val.item_spec_set_image == '')
                    {
                        val.item_spec_set_image    = "images/item_images/no_image.png";
                    }
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                    html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                });
                $("#item_select_result").html(html);
            }
                    // if ($('#item_select').val().length >= 17) {
                    //     $('#item_select_result').find('td').eq(0).click();
                    //     $('input[name=quantity]').focus();
                    // }
                });
    }, 500);
});

$("#item_select_result").on('click', 'td', function(event) 
{
    $('input[name="items_name"]').val($(this).text());
    $('input[name="items_id"]').val($(this).attr('data'));
    $('input[name="item_spec_set_id"]').val($(this).attr('data'));
    $('input[name="is_unique_barcode"]').val($(this).attr('unique_barcode'));
    $("#item_select_result").hide();
});


$('#save_buy_new').click(function(event) {
    var net_amount =0;
    var return_payment = render_data['all_basic_data']['net_payable'];
    var new_payment = render_new_data['all_basic_data']['net_payable'];
    net_amount = (return_payment) -(new_payment);
    $('#return_payment').html(return_payment);
    $('#new_payment').html(new_payment);
    $('#new_net_amount').html(net_amount);
    
});


$('#item_select').change(function(event) 
{
        //alert($(this).val());
        var item_spec_set_id = $(this).val();

        var post_data ={
            'item_spec_set_id' : $(this).val(),
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Exchange_with_vendors/find_item_price/'+item_spec_set_id,post_data).done(function(data) {
            var data2 = $.parseJSON(data);

            // $('input[name="buying_price"]').val(data2.buying_price);

            // $('input[name="retail_price"]').val(data2.retail_price);
            // $('input[name="whole_sale_price"]').val(data2.whole_sale_price);


            //$('#item_price_hidden').val(data2.retail_price);
            


        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });


$('#item_quantity').keyup(function(event) {


    var item_quantity = $(this).val();
    clearTimeout(timer);
    timer = setTimeout(function() 
    {
        var unique_barcode_count = $('input[name*="unique_barcode_value[]"]').length;
        if($("#is_unique_barcode").val() == "yes")
        {

         if(item_quantity=="")
         {
            $("#unique_barcode_div").html("");
        }
        else if(unique_barcode_count==0)
        {
            var html = "";
            for(var i=1;i<=item_quantity;)
            {
                html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
                html+= '</label><div class="col-md-9 input-form input-icon"><input ';
                html+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
                html+= 'Number '+i+'"></div></div>';
                i++;
            }
            $("#unique_barcode_div").html(html);
        }
        else if(unique_barcode_count>item_quantity)
        {
         $(".imei_remove_class").show();
     }  
     else if(unique_barcode_count<item_quantity)
     {
        var html_new = "";
        for(var i=unique_barcode_count;i<item_quantity;)
        {
            html_new+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
            html_new+= '</label><div class="col-md-9 input-form input-icon"><input ';
            html_new+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
            html_new+= 'Number '+i+'"></div></div>';
            i++;
        }
        $("#unique_barcode_div").append(html_new);
    }              
}
}, 500);


    var item_new_quantity = $(this).val();
    var per_item_price = $('#item_price_hidden').val();
    $('#item_price').val(item_new_quantity*per_item_price);
        //alert(item_new_quantity*per_item_price);
    });






$('body').tooltip({
    selector: '.fa.fa-question-circle.tooltips'
});

</script>

