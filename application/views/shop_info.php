<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>
        <!-- CODE FOR SLIDER ALERT -->
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failure'); ?></div>
        <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?></div>
        <div class="alert alert-danger" id="no_vendorname" style="display:none" role="alert"><?php echo $this->lang->line('validation_name'); ?></div>
        <div class="alert alert-danger" id="no_phone" style="display:none" role="alert">Give vendor phone number</div>
        <div class="alert alert-danger" id="customer_exist" style="display:none" role="alert"><?php echo $this->lang->line('customer_exist'); ?></div>
        <div class="alert alert-success" id="delete_success_from_list" style="display:none" role="alert"><?php echo $this->lang->line('deletion_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('no_deletion'); ?></div>
        <!-- CODE FOR SLIDER ALERT END-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('left_portlet_title'); ?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN OF CUSTOMER FORM-->
                <?php
                $form_attrib = array('id' => 'shop_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('shop_info/update_shop'),  $form_attrib, '');
                ?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_name'); ?>*</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="<?php echo $shop_info['name']?>" name="name" placeholder="<?php echo $this->lang->line('shop_name'); ?>">
                            <input type="hidden" name="shop_id" value="<?php echo $shop_info['shop_id']?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" value="<?php echo $shop_info['address']?>" class="form-control" name="address"  placeholder="<?php echo $this->lang->line('shop_address'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_phone'); ?></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="1" value="<?php echo $shop_info['phone']?>" class="form-control" name="phone"  placeholder="<?php echo $this->lang->line('shop_phone'); ?> ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_website'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="<?php echo $shop_info['website']?>" name="website"  placeholder="<?php echo $this->lang->line('shop_website'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_vat_reg_no'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="<?php echo $shop_info['vat_reg_no']?>" name="vat_reg_no"  placeholder="<?php echo $this->lang->line('shop_vat_reg_no'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('shop_policy_plan'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="<?php echo $shop_info['policy_plan']?>" name="policy_plan"  placeholder="<?php echo $this->lang->line('shop_policy_plan'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div id="image_of_user" class="form-group ">
                        <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('shop_image'); ?></label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div id="shop_image" class="fileinput-preview thumbnail" name="shop_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                    <?php if(!empty($shop_info['shop_image'])) {

                                        $image = $shop_info['shop_image'];
                                        ?><img src="<?php echo base_url()?><?php echo $image?>" alt=""><?php
                                    }
                                    ?>
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            <?php echo $this->lang->line('select_image'); ?>
                                        </span>
                                        <span class="fileinput-exists">
                                            <?php echo $this->lang->line('edit'); ?> 
                                        </span>
                                        <input type="file" name="shop_image">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        <?php echo $this->lang->line('delete'); ?> 
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="vendor_cancle" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('shop_cancel_btn'); ?></button>
                    <button type="submit" value="<?php echo $this->lang->line('save'); ?>"  class="btn green pull-right"><?php echo $this->lang->line('shop_save_btn'); ?></button>
                    <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
                    <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
                </div>
            </form>
            <!-- END OF CUSTOMER FORM-->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
      <!-- CODE FOR SLIDER ALERT END-->
      <div class="alert alert-success" id="printer_added_success" style="display:none" role="alert"><?php echo $this->lang->line('type_selection_success'); ?> </div>
      <div class="alert alert-danger" id="printer_added_failure" style="display:none" role="alert"><?php echo $this->lang->line('type_selection_failure'); ?> </div>
      <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-print"></i> <span id="form_header_text"><?php echo $this->lang->line('select_printer_type'); ?> </span>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN OF CUSTOMER FORM-->
            <div class="form-body">
              <div class="form-group">
                <div class="radio-list">
                    <label class="radio">
                        <div class="radio" id="">
                            <input type="radio" name="printer_type" value="normal" id="normal_printer">
                        </div>  Normal With Shop Info                      
                    </label>
                    <label class="radio">
                        <div class="radio" id="">
                            <input type="radio" name="printer_type" value="normal_without_header" id="normal_without_header_printer">
                        </div>  Normal Without Shop Info
                    </label>
                    <label class="radio">
                        <div class="radio" id="">
                            <input type="radio" name="printer_type" value="pos" id="thermal_printer">
                        </div>  <?php echo $this->lang->line('pos'); ?> 
                    </label>
                    <label class="radio">
                        <div class="radio" id="">
                            <input type="radio" name="printer_type" value="none" id="no_printer">
                        </div>  <?php echo $this->lang->line('none'); ?> 
                    </label>
                </div>
            </div>
        </div>
        <!-- END OF CUSTOMER FORM-->
    </div>
</div>
</div>
<div id="responsive_modal" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="user_name_selected" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('do_you_want_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('no'); ?></button>
    <button type="button" id="user_delete_confirm_y" class="btn blue"><?php echo $this->lang->line('yes'); ?></button>
</div>
</div>
<!-- Time: <?php echo $this->benchmark->elapsed_time(); ?> -->
<!-- Mem: <?php echo $this->benchmark->memory_usage(); ?> -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function() {    
        UIExtendedModals.init();
    });
</script>  
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    var csrf = $("input[name='csrf_test_name']").val();
//Code for data table start
var table_user = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/Vendor/all_vendor_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for data table end
$('#vendor_cancle').click(function(event) {
    $('input[name="vendors_name"]').val('');
    $('input[name="vendors_phone_1"]').val('');
    $('input[name="vendors_phone_2"]').val('');
    $('input[name="vendors_present_address"]').val('');
    $('input[name="vendors_permanent_address"]').val('');
    $('input[name="vendors_national_id"]').val('');
    $('input[name="vendors_phone_1"]').val('');
    $('input[name="vendors_phone_1"]').val('');
    $('.fileinput-preview').html("<img src=''>");
});
//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#shop_form').ajaxForm({
    beforeSubmit: function() {    
    },
    success: function(msg) {
        if(msg == "success")
        {
            $('#update_success').slideDown();
            setTimeout( function(){$('#update_success').slideUp()}, 1500 );
        }
        console.log(msg);
    },
    complete: function(xhr) {
        //setTimeout( function(){$('#update_success').slideUp()}, 1500 );
    }
}); 
//Ajax form submit end
</script>
<script type="text/javascript">
    var user_id ='';
//Code for showing user data on form for edit customer button start
$('table').on('click', '.edit_user',function(event) {
    event.preventDefault();
    user_id = $(this).attr('id').split('_')[1];
    $('#current_user_id').remove();
    $.get('<?php echo site_url();?>/Vendor/vendor_info_by_id/'+user_id).done(function(data) 
    {
        var user_data = $.parseJSON(data);
        if(user_data == "No permission")
        {
            $('#not_permitted').slideDown();
            setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
        }
        //console.log(user_data);
        var html = "<input id='current_user_id' type='hidden' class='form-control' name='user_id' value='"+user_id+"'>";
        $('#shop_form').append(html);
        $('#shop_form').attr('id', 'user_form_update');
        $('#user_form_update').attr('action', "<?php echo site_url('/Vendor/update_vendor_info');?>"+"/"+user_id);
        $('#form_header_text').text("<?php echo $this->lang->line('change_customer_info'); ?>");
        $('#add_or_update').text("<?php echo $this->lang->line('update_info'); ?>");
        //$('#email').prop('disabled', true);
        //$('.pass_hide').hide('slow');
        $('#change_pic').hide('slow');
        $("#shop_image").prop('disabled', false);
        $("#image_of_user_label").text("<?php echo $this->lang->line('image_of_customer'); ?>");
        /*To set the value on input field after clicking on edit button for a particular user*/
        $('input[name = vendors_name]').val(user_data.vendor_info_by_id.vendors_name);
        //$('input[name = customers_email]').val(user_data.customer_info_by_id.customers_email);
        $('input[name = vendors_present_address]').val(user_data.vendor_info_by_id.vendors_present_address);
        $('input[name = vendors_permanent_address]').val(user_data.vendor_info_by_id.vendors_permanent_address);
        $('input[name = vendors_phone_1]').val(user_data.vendor_info_by_id.vendors_phone_1);
        $('input[name = vendors_phone_2]').val(user_data.vendor_info_by_id.vendors_phone_2);
        $('input[name = vendors_national_id]').val(user_data.vendor_info_by_id.vendors_national_id);
        if(user_data.vendor_info_by_id.vendor_image!="" && user_data.vendor_info_by_id.vendor_image!=null)
        {
            $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+user_data.vendor_info_by_id.vendor_image+"'>");
        }
        else
        {
            $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/shop_images/no_image.png'>");
        }
        $('#user_form_update').ajaxForm({
            beforeSubmit: function() {    
            },
            success: function(msg) {
                if(msg == "Give vendor name")
                {
                    $('#no_vendorname').slideDown();
                    setTimeout( function(){$('#no_vendorname').slideUp()}, 1500 );exit;
                }
                else if(msg == "Give vendor phone1")
                {
                    $('#no_phone').slideDown();
                    setTimeout( function(){$('#no_phone').slideUp()}, 1500 );exit;
                }
            },
            complete: function(xhr) {
                $('#update_success').slideDown();
                setTimeout( function(){$('#update_success').slideUp()}, 1500 );
                $('#form_header_text').text("<?php echo $this->lang->line('add_new_customer'); ?>");

                $('#user_form_update').attr('action', "<?php echo site_url('/Vendor/createAccount');?>");
                $('#user_form_update').attr('id', 'shop_form');
                $('#current_user_id').remove();
                //$('#user_form_update').attr('id', 'shop_form');
                $('input[name="vendors_name"]').val('');
                $('input[name="vendors_phone_1"]').val('');
                $('input[name="vendors_phone_2"]').val('');
                $('input[name="vendors_present_address"]').val('');
                $('input[name="vendors_permanent_address"]').val('');
                $('input[name="vendors_national_id"]').val('');
                $('.fileinput-preview').html("<img src=''>");
                table_user.ajax.reload();
                return false;
            }
        }); 
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
//Code for showing user data on form for edit customer button end
//Code for clear the form on clicking cancel button start
$('#cancel_update').click(function(event) {
    $('input[name = customers_name]').val("");
    $('input[name = customers_email]').val("");
    $('input[name= customers_present_address]').val("");
    $('input[name = customers_permanent_address]').val("");
    $('input[name = customers_phone_1]').val("");
    $('input[name = customers_phone_2]').val("");
    $('input[name = customers_national_id]').val("");
    $('.fileinput-preview').html("<img src=''>");
});
//Code for clear the form on clicking cancel button end
//Code for delete user with confirmation modal start
var user_id = '' ;
var user_name_selected = '';
$('table').on('click', '.delete_user',function(event) {
    event.preventDefault();
    user_id = $(this).attr('id').split('_')[1];
    user_name_selected = $(this).parent().parent().find('td:first').text();
    $('#user_name_selected').html(user_name_selected);
});
$('#responsive_modal').on('click', '#user_delete_confirm_y', function(event) {
    event.preventDefault();
    var post_data ={
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        'user_id' : user_id
    }; 
    $.post('<?php echo site_url();?>/Vendor/delete_vendor_info/'+user_id,post_data).done(function(data) {
        if(data == "No permission")
        {
            $('#not_permitted').slideDown();
            setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            $('#responsive_modal').modal('hide');
        }
        else if(data = "success")
        {
            $('#delete_success_from_list').slideDown();
            setTimeout( function(){$('#delete_success_from_list').slideUp()}, 1500 );
            table_user.row($('#delete_'+user_id).parents('tr')).remove().draw();
            // $('#responsive_modal').modal('hide');
            $('#responsive_modal').attr('data-dismiss', 'modal');
            $('input[name = customers_name]').val("");
            $('input[name = customers_email]').val("");
            $('input[name= customers_present_address]').val("");
            $('input[name = customers_permanent_address]').val("");
            $('input[name = customers_phone_1]').val("");
            $('input[name = customers_phone_2]').val("");
            $('input[name = customers_national_id]').val("");
            $('.fileinput-preview').html("<img src=''>");
        }
        else
        {
            alert("An error has occured. pLease try again");                
        }
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});
//Code for update customer information start
// $('.portlet-body.form').on('click', '#user_form_update input[type="submit"]', function(event){
// 	event.preventDefault();

// 	var data = {};
// 	data.csrf_test_name = $('input[name=csrf_test_name]').val();

// 	data.customers_id = user_id;
// 	data.customers_name = $('input[name = customers_name]').val();
// 	data.customers_present_address = $('input[name= customers_present_address]').val();
// 	data.customers_permanent_address = $('input[name = customers_permanent_address]').val();
// 	data.customers_phone_1 = $('input[name = customers_phone_1]').val();
// 	data.customers_phone_2 = $('input[name = customers_phone_2]').val();
// 	data.customers_national_id = $('input[name = customers_national_id]').val();

// 	$.post("<?php echo site_url('/Customers/update_customer_info');?>",data).done(function(data1, textStatus, xhr) {
// 		if(data1 == "info_updated"){
// 			table_user.ajax.reload();
// 			$('#update_success').slideDown();
// 			setTimeout( function(){$('#update_success').slideUp()}, 1500 );
// 		}

// 		else if(data1 == "Username not set")
// 		{
// 			$('#no_username').slideDown();
// 			setTimeout( function(){$('#no_username').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Username already exists"){
// 			$('#username_exist').slideDown();
// 			setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Email already exists"){
// 			$('#email_exist').slideDown();
// 			setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Mobile no. 1 can not be Empty"){
// 			$('#no_mobile').slideDown();
// 			setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
// 		}

// 	}).error(function() {
// 		$('#insert_failure').slideDown();
// 		setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
// 	});	
// });
//Code for update customer information end
//Printer Setting Starts
$('input:radio[name="printer_type"]').change(
    function(){
        var data = {};
        data.active_printer = $(this).val(),
        data.csrf_test_name = $('input[name=csrf_test_name]').val(),
        $.post("<?php echo site_url('/shop_info/save_printer_type');?>",data).done(function(data1, textStatus, xhr) {
           var result = $.parseJSON(data1);
           if(result.success =="yes"){
            $('#printer_added_success').slideDown();
            setTimeout( function(){$('#printer_added_success').slideUp()}, 5000 );
        }
        else{
            $('#printer_added_failure').slideDown();
            setTimeout( function(){$('#printer_added_failure').slideUp()}, 5000 );
        }
    });
    });
var curr_type = "<?php echo $shop_info['active_printer']?>"
$('input:radio[value="' + curr_type + '"]').prop('checked', true);
</script>