
<div class="row">
	<div class="col-md-12">
		<!-- 1st Form Starts -->
		<div class="tab-content" id="basic_info">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Registration Form
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->
						<?php $form_attrib = array('id' => 'basic_info_form','class' => 'form-horizontal');
                        echo form_open('',  $form_attrib, '');?>

                        <!--     <form action="#" class="form-horizontal"> -->
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Account's Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control input-circle" name="accounts_name" placeholder="Enter text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Owner's Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control input-circle"  name="owner_name" placeholder="Enter text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input type="email" class="form-control input-circle-right" name="owner_email" placeholder="Email Address">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Password</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="password" class="form-control input-circle-left" name="owner_password" placeholder="Password">
                                        <span class="input-group-addon input-circle-right">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Local PIN</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="password" class="form-control input-circle-left" name="local_pin_code" placeholder="Password">
                                        <span class="input-group-addon input-circle-right">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle blue" id="save_basic_info">Submit</button>
                                    <button type="button" class="btn btn-circle default">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <!-- </form> -->
        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <!-- 1st Form Ends -->

        <!-- 2nd Form Starts -->
        <div class="tab-content" style ="display: none" id="package_info">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Package Information
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="#" class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Text</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control input-circle" placeholder="Enter text">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email Address</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="email" class="form-control input-circle-right" placeholder="Email Address">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Input With Spinner</label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control spinner input-circle" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <label class="col-md-3 control-label">Static Control</label>
                                    <div class="col-md-4">
                                        <span class="form-control-static">
                                            email@example.com 
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-circle blue">Submit</button>
                                        <button type="button" class="btn btn-circle default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- 2nd Form Ends -->

    </div>
</div>

<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<script>
    $('#basic_info').on('click', '#save_basic_info', function(event) {
        event.preventDefault();
        $('#basic_info').hide();
        $('#package_info').show();
    });

</script>