<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><div id="form_header_text">নতুন সাইজ যোগ করুন</div>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form id="brand_form" class="form-horizontal form-bordered" action="<?php echo site_url('/Brand/save_brand_info');?>" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">সাইজের  নাম </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sizes_name" placeholder="সাইজের  নাম ">
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn green">সেভ করুন</button>
                        <button type="button" class="btn default">বাতিল করুন</button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>সকল সাইজের তালিকা 
                </div>
                <div class="tools">
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                সাইজের  নাম 
                            </th>
                            <th>
                                পরিবর্তন / ডিলিট
                            </th>

                        </tr>
                    </thead>
                    <tbody>

        <?php foreach ($all_sizes as $v_sizes) 
        {
        ?>
                            <tr>
                                <td>
            <?php echo $v_sizes['sizes_name'];?>
                                </td>


                                <td>
                                    <a class="btn btn-primary edit_brand" id="edit_<?php echo $v_sizes['sizes_id'];?>" >    পরিবর্তন </a>
                                    <a class="btn btn-danger delete_brand" id="delete_<?php echo $v_sizes['sizes_id'];?>" >    ডিলিট </a>
                                </td>

                            </tr>

        <?php } ?>
                        </tbody>

                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->    
    <script>
        table_brand = $('#sample_2').DataTable();        
        // function initTable2() {
        //     var table = $('#sample_2');

        //     /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

        //     /* Set tabletools buttons and button container */

        //     $.extend(true, $.fn.DataTable.TableTools.classes, {
        //         "container": "btn-group tabletools-btn-group pull-right",
        //         "buttons": {
        //             "normal": "btn btn-sm default",
        //             "disabled": "btn btn-sm default disabled"
        //         }
        //     });

        //     var oTable = table.dataTable({

  //           // Internationalisation. For more info refer to http://datatables.net/manual/i18n
  //           "language": {
  //               "aria": {
  //                   "sortAscending": ": activate to sort column ascending",
  //                   "sortDescending": ": activate to sort column descending"
  //               },
  //               "emptyTable": "No data available in table",
  //               "info": "Showing _START_ to _END_ of _TOTAL_ entries",
  //               "infoEmpty": "No entries found",
  //               "infoFiltered": "(filtered1 from _MAX_ total entries)",
  //               "lengthMenu": "Show _MENU_ entries",
  //               "search": "Search:",
  //               "zeroRecords": "No matching records found"
  //           },

  //           "order": [
  //           [0, 'asc']
  //           ],
  //           "lengthMenu": [
  //           [5, 10, 15, 20, -1],
  //               [5, 10, 15, 20, "All"] // change per page values here
  //               ],

  //           // set the initial value
  //           "pageLength": 10,
  //           "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

  //           // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
  //           // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
  //           // So when dropdowns used the scrollable div should be removed. 
  //           //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

  //           "tableTools": {
  //               "sSwfPath": "<?php echo base_url()?>assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
  //               "aButtons": [{
  //                   "sExtends": "pdf",
  //                   "sButtonText": "PDF"
  //               }, {
  //                   "sExtends": "csv",
  //                   "sButtonText": "CSV"
  //               }, {
  //                   "sExtends": "xls",
  //                   "sButtonText": "Excel"
  //               }, {
  //                   "sExtends": "print",
  //                   "sButtonText": "Print",
  //                   "sInfo": 'Please press "CTRL+P" to print or "ESC" to quit',
  //                   "sMessage": "Generated by DataTables"
  //               }, {
  //                   "sExtends": "copy",
  //                   "sButtonText": "Copy"
  //               }]
  //           }
  //       });

  //       var tableWrapper = $('#sample_2_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
  //       tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
  //   }
</script>

<script type="text/javascript">
    

    $('table').on('click', '.edit_brand',function(event) {
        event.preventDefault();

        var brand_id = $(this).attr('id').split('_')[1];
        $('#current_brand_id').remove();

        $.get('<?php echo site_url();?>/Brand/get_brand_info/'+brand_id).done(function(data) {

            var brand_data = $.parseJSON(data);
            var html = "<input id='current_brand_id' type='hidden' class='form-control' name='sizes_id' value='"+brand_id+"'>";
            $('#brand_form').append(html);
            $('#brand_form').attr('action', "<?php echo site_url('/Brand/update_brand_info');?>");
            $('#form_header_text').text("poriborton");
            $('input[name = brands_name]').val(brand_data.brand_info_by_id.brands_name);
            $('input[name = brands_description]').val(brand_data.brand_info_by_id.brands_description);

            if(brand_data.brand_info_by_id.brands_logo!="" && brand_data.brand_info_by_id.brands_logo!=null)
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+brand_data.brand_info_by_id.brands_logo+"'>");
            }
            else
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/brand_images/no_image.png'>");
            }

        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

</script>
<script type="text/javascript">
    
    $('table').on('click', '.delete_brand',function(event) {
        event.preventDefault();
        var sizes_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'sizes_id' : sizes_id

        }; 

        $.post('<?php echo site_url();?>/Brand/delete_brand/'+sizes_id,post_data).done(function(data) {
            if(data = "success")
            {
                alert("success");
                table_brand.row($('#delete_'+sizes_id).parents('tr')).remove().draw();
            }
            else
            {
                alert("An error has occured. pLease try again");                
            }
            // var brand_data = $.parseJSON(data);
            // var html = "<input id='current_brand_id' type='hidden' class='form-control' name='sizes_id' value='"+brand_id+"'>";
            // $('#brand_form').append(html);
            // $('#brand_form').attr('action', "<?php echo site_url('/Brand/update_brand_info');?>");
            // $('#form_header_text').text("poriborton");
            // $('input[name = brands_name]').val(brand_data.brand_info_by_id.brands_name);
            // $('input[name = brands_description]').val(brand_data.brand_info_by_id.brands_description);

        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });


</script>