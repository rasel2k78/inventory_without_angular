<?php 
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
$this->output->set_header("Pragma: no-cache");

/**
 * Form Name:"Loan  Form" 
 *
 * This is form for insert , edit ,adjust and update loan informations.
 * 
 * @link   Loan/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>

<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->


<div class="row">
    <div class="col-md-6">

        <div class="alert alert-danger" id="adjust_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_adjust_failed');?></div>
        <div class="alert alert-success" id="adjust_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_adjust_success');?> </div>
        
        <div class="alert alert-success" id="payout_success" style="display:none" role="alert"><?php echo $this->lang->line('payout_success');?> </div>
        <div class="alert alert-danger" id="delete_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_failure');?> </div>
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
                </div>
                
            </div>
            <div class="portlet-body form" id="loan_insert_div">
                <!-- BEGIN FORM-->
                
                <?php $form_attrib = array('id' => 'loan_form','class' => 'form-horizontal');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
        <!--             <h3 class="form-section">বর্তমান টাকাঃ <b><span id="current_amount_sum" style="color:red;"><?php echo $final_cashbox_info['total_cashbox_amount'] ?>৳</span></b></h3>
    -->
    
    

    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
        <div class="col-md-9">
            <input type="number" step="any" min="0" class="form-control" name="loan_amount" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>">
            <input type="hidden" value="loan" name="loan_type">
            <span class="help-block"></span>
            <!-- <pre class="normalize help-block"></pre> -->
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_loan_description');?></label>
        <div class="col-md-9">
            <textarea class="form-control" name="loan_description" placeholder="<?php echo $this->lang->line('left_loan_description_placeholder');?>" rows="3" cols="50"></textarea>
            <span class="help-block"></span>
        </div>
    </div>
</div>
<div class="form-actions">
    <button type="reset" class="btn default pull-right"><?php echo $this->lang->line('left_cancle');?></button>
    <button type="submit" id="save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
</div>
<?php echo form_close();?>
<!-- END FORM-->
</div>
<div class="portlet-body form" id="loan_edit_div" style="display:none">

    <?php $form_attrib = array('id' => 'loan_edit_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>

    <input type="text" class="form-control" name="loan_id" style="display:none">

    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
        <div class="col-md-9">
            <input type="number" min="1" step="any" class="form-control" name="loan_amount_edit" >
            <!-- <pre class="normalize help-block"></pre> -->
            <input type="hidden" value="loan" name="cash_type_edit" id="cash_type_id_edit">
            <span class="help-block"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_loan_description');?></label>
        <div class="col-md-9">
            <textarea class="form-control" name="loan_description_edit"  rows="3"></textarea>
            <pre class="normalize help-block"></pre>
        </div>
    </div>
    <div class="form-actions">
        <button type="button" class="btn default pull-right" style="margin-left: 8px" id="edit_loan_cancle"><?php echo $this->lang->line('left_cancle');?></button>
        <button type="submit" id="edit_save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
    </div>
    <?php echo form_close();?>

</div>
<div class="portlet-body form" id="payout_loan_div" style="display:none" >
    <!-- BEGIN FORM-->

    <?php $form_attrib = array('id' => 'payout_loan_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>

    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?></label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="loan_amount" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>" readonly="true">
            <input type="hidden" value="loan" id="cash_type_id" name="cash_type">
            <!-- <span class="help-block"></span> -->

        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-3"><?php echo $this->lang->line('left_loan_description');?></label>
        <div class="col-md-9">
            <textarea class="form-control" name="loan_description" placeholder="<?php echo $this->lang->line('left_loan_description_placeholder');?>" rows="3" cols="50" readonly="true"></textarea>
            <span class="help-block"></span>
        </div>
    </div>
    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo $this->lang->line('total_loan_paid');?></label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="total_payout_amount" readonly="true" placeholder="<?php echo $this->lang->line('total_loan_paid');?>">
                <!--         <span class="help-block"></span> -->
                <pre class="normalize help-block"></pre>
            </div>
        </div>
    </div>
    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo $this->lang->line('left_payout');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
            <div class="col-md-9">
                <input type="number" min="0" step="any" class="form-control" id="payout_amount_check" name="payout_amount" placeholder="<?php echo $this->lang->line('left_payout');?>">
                <!--         <span class="help-block"></span> -->
                <pre class="normalize help-block"></pre>
            </div>
        </div>
    </div>
    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo $this->lang->line('left_loan_description');?></label>
            <div class="col-md-9">
                <textarea type="text" class="form-control" name="payout_comments" placeholder="<?php echo $this->lang->line('left_loan_description_placeholder');?>" rows="3" cols="50"></textarea>
                <pre class="normalize help-block"></pre>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <button type="button" class="btn default pull-right" style="margin-left: 8px" id="payout_loan_cancle"><?php echo $this->lang->line('left_cancle');?></button>
        <button type="submit" id="payout_btn" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
    </div>
    <?php echo form_close();?>
    <!-- END FORM-->
</div>


</div>
</div>
<div class="col-md-6">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->lang->line('right_type');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('right_amount');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('loan_paid');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('remaining_loan');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('right_description');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('right_date');?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('right_edit_delete');?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <!-- <td>test</td> -->
                </tbody>
                <tfoot>
                    <tr>
                        <th><?php echo $this->lang->line('right_type');?></th>
                        <th><?php echo $this->lang->line('right_amount');?></th>
                        <th><?php echo $this->lang->line('loan_paid');?></th>
                        <th>
                            <?php echo $this->lang->line('remaining_loan');?>
                        </th>
                        <th><?php echo $this->lang->line('right_description');?></th>
                        <th>
                            <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control dont-search" name="from_create_date">
                                <span class="input-group-addon">
                                    to
                                </span>
                                <input type="text" class="form-control dont-search" name="to_create_date">
                            </div>
                        </th>
                        <th><?php echo $this->lang->line('right_edit_delete');?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

</div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>

<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>

<div id="responsive_modal_adjust" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_adjust_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_adjust_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
    <button type="button" id="adjust_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
</div>
</div>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>-->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>

<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- tooltip -->
<script>
    /*loading tooltip*/
    jQuery(document).ready(function($) {
        $("[data-toggle='popover']").popover();
    });

    /*lading calendar*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });


    // table = $('#sample_2').DataTable();
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 4 || idx ==6) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });

    var table = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/loan/all_loan_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [5, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/

    });
    /*data table customize search starts*/
    table.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });

        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/


    /* Payout LOan Cancle Start*/
    $('form').on('click', '#payout_loan_cancle', function(event) {
        event.preventDefault();

        $('#payout_loan_form').hide(200);
        // $('#payout_loan_form input').val('');
        $('input[name=loan_amount]').val('');
        $('textarea[name=loan_description]').val('');
        $('input[name=payout_amount]').val('');
        $('textarea[name=payout_comments]').val('');
        $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
        $('#loan_insert_div').show(400);

    });
    /* Payout Loan Cancle End*/

    $('table').on('click', '.edit_loan',function(event) {

        event.preventDefault();
        var loan_id = $(this).attr('id').split('_')[1];
        $('#current_loan_id').remove();
        $('.has-error').removeClass('has-error');
        $('.help-block').text('');

        $.get('<?php echo site_url();?>/loan/get_loan_info/'+loan_id).done(function(data) {

            var loan_data = $.parseJSON(data);
            if(loan_data =="No Permission"){
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
            }
            else{

                $('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
            // $('select[name = cash_type_edit]').val(loan_data.all_cashbox_by_id.cash_type);
            $('input[name = loan_amount_edit]').val(loan_data.all_loan_by_id.loan_amount);
            $('textarea[name = loan_description_edit]').val(loan_data.all_loan_by_id.loan_description);
            $('#loan_insert_div').slideUp('fast');
            $('#payout_loan_div').slideUp('fast');
            $('#loan_edit_div').slideDown('slow');
            $('input[name=loan_id]').val(loan_id);
        }


    }).error(function() {
        alert("<?php echo $this->lang->line('default_error');?>");
    });
});

    $('#loan_edit_form').submit(function(event) {

        event.preventDefault();
        $('#responsive_modal_edit').modal('show');

    });

    $('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {

        event.preventDefault();
        $('#edit_save_id').prop('disabled', true);
        var post_data={};
        // post_data.cash_type_edit = $('#cash_type_id_edit').val();
        post_data.loan_amount_edit = $('input[name = loan_amount_edit]').val();
        post_data.loan_description_edit = $('textarea[name = loan_description_edit]').val();
        post_data.loan_id = $('input[name= loan_id]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/loan/update_loan_info',post_data).done(function(data, textStatus, xhr) {

            var data2 = $.parseJSON(data);

            if(data2['success'])
            {
                $('#update_success').slideDown();
                $('#responsive_modal_edit').modal('hide');
                setTimeout( function(){$('#update_success').slideUp()}, 5000 );
                table.ajax.reload(null, false);
                $('#loan_insert_div').slideDown('slow');
                $('#loan_edit_div').slideUp('fast');
                $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
                $('#edit_save_id').prop('disabled', false);

            }
            else if(!Object.keys(data2.error).length)
            {
                $('#edit_save_id').prop('disabled', false);
                alert("<?php echo $this->lang->line('alert_edit_failed');?>");  
            }
            else
            {
                $('#responsive_modal_edit').modal('hide');
                $('#edit_save_id').prop('disabled', false);
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('default_error');?>");
            $('#edit_save_id').prop('disabled', false);

        });
    });


    /* Cancle loan edit form starts*/
    $('#edit_loan_cancle').click(function(event) {
        event.preventDefault();
        $('input[name=loan_amount_edit]').val('');
        $('textarea[name=loan_description_edit]').val('');
        $('#loan_insert_div').slideDown('slow');
        $('#loan_edit_div').slideUp('fast');
        $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");

    });
    /* Cancle loan edit form ends*/


    /*  ajax delete starts */
    var loan_id = '' ;
    var selected_name = '';
    $('table').on('click', '.delete_loan',function(event) {
        event.preventDefault();
        loan_id = $(this).attr('id').split('_')[1];
        selected_name = $(this).parent().parent().find('td:first').text();
        $('#selected_name').html(selected_name);
    });

    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
    // var loan_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'loan_id' : loan_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/loan/delete_loan/'+loan_id,post_data).done(function(data)
    {

        if(data == "No Permission"){
            $('#responsive_modal_delete').modal('hide');
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else if(data == "success")
        {
            $('#responsive_modal_delete').modal('hide');
            $('#delete_success').slideDown();
            setTimeout( function(){$('#delete_success').slideUp()}, 5000 );
            table.ajax.reload(null, false);
        }
        else if(data == "failed")
        {
            $('#responsive_modal_delete').modal('hide');
            alert("<?php echo $this->lang->line('alert_delete_failure');?>");        
            
        }

    }).error(function() {
        alert("<?php echo $this->lang->line('alert_delete_failure');?>");                
    });
});
    /*ajax delete end*/

// ajax form submit starts....
$('#loan_form').submit(function(event) {
    event.preventDefault();
    $('#save_id').prop('disabled', true);

    var data = {};
    
    // data.cash_type = $('#cash_type_id').val();
    data.loan_type = $('input[name=loan_type]').val();
    data.loan_amount = $('input[name=loan_amount]').val();
    data.loan_description = $('textarea[name=loan_description]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/Loan/save_loan_amount_info',data).done(function(data1, textStatus, xhr) {

        var data2 = $.parseJSON(data1);

        if(data2.success==0)
        {
            $('#save_id').prop('disabled', false);
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }

        else{

            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('#save_id').prop('disabled', false);

            $('input[name=loan_amount]').val('');
            $('textarea[name=loan_description]').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 5000 );
            table.ajax.reload(null, false);
            if(data.cash_type=="loan")
            {
                var c_amount = parseFloat($('#current_amount_sum').html());
                c_amount+=parseFloat(data.loan_amount);
                $('#current_amount_sum').html(c_amount);                
            }
            /*else
            {
                var c_amount = parseFloat($('#current_amount_sum').html());
                c_amount-=parseFloat(data.loan_amount);
                $('#current_amount_sum').html(c_amount);
            }*/

        }
    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
        $('#save_id').prop('disabled', false);

    });    
});

// ajax form submit end.......

// ajax loan payout starts here 

var loan_id;
$('table').on('click', '.payout_loan',function(event) {
    event.preventDefault();



    loan_id = $(this).attr('id').split('_')[1];
    $('#current_loan_id').remove();

    $.get('<?php echo site_url();?>/loan/get_loan_info/'+loan_id).done(function(data) {

        var loan_data = $.parseJSON(data);

        if(loan_data == "No Permission"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else{
            $('#loan_insert_div').hide('fast');
            $('#loan_edit_div').hide('fast');

            $('#form_header_text').text("<?php echo $this->lang->line('payout_header');?>");
            $('#payout_loan_div').show('fast');

            $('input[name = loan_amount]').val(loan_data.all_loan_by_id.loan_amount);
            $('textarea[name = loan_description]').val(loan_data.all_loan_by_id.loan_description);
            $('input[name = total_payout_amount]').val(loan_data.all_loan_by_id.total_payout_amount);
            $('input[name = loan_id]').val(loan_id);

        }


    }).error(function() {
        alert("<?php echo $this->lang->line('default_error');?>");
    });

});


$('#payout_loan_form').submit(function(event) {
    event.preventDefault();
    $('#payout_btn').prop('disabled', true);

    var data = {};
    data.loan_amount = $('input[name=loan_amount]').val();
    data.loan_description = $('textarea[name=loan_description]').val();
    data.loan_id = $('input[name=loan_id]').val();
    data.payout_amount = $('input[name=payout_amount]').val();
    data.payout_comments = $('textarea[name=payout_comments]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/loan/save_payout_loan',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.success==0)
        {
            $('#payout_btn').prop('disabled', false);
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();

            });
            // var row_data = table.row( $('#payout_'+post_data.loan_id).closest('tr')[0] ).data();
            // row_data[0] = '';
            // table.row( $('#payout_'+post_data.loan_id).closest('tr')[0] ).data(row_data).draw();
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('#payout_btn').prop('disabled', false);
            $('#payout_loan_form input').val('');
            $('#payout_loan_div').hide(200);
            $('input[name=loan_amount]').val('');
            $('textarea[name=loan_description]').val('');
            $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
            $('#loan_insert_div').show(400);
            // var row_data = [];
            // row_data['0']= 'loan';
            // row_data['1'] = data.loan_amount;
            // row_data['2'] = data.payout_amount;
            // row_data['3'] = data.loan_description;
            // row_data['4'] =    '';
            // row_data['5'] = ''; 
            // table.row( $('#payout_'+data.loan_id).closest('tr')[0]).data(row_data).draw()
            table.ajax.reload(null, false);
            $('#payout_success').slideDown();
            setTimeout( function(){$('#payout_success').slideUp();}, 5000 );
            //  location.reload();

        }

    }).error(function() {
        $('#payout_btn').prop('disabled', false);
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
    });    

});

// ajax loan_adjust starts

var selected_name = '';
$('table').on('click', '.adjust_loan',function(event) {
    event.preventDefault();
    loan_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});


$('#responsive_modal_adjust').on('click', '#adjust_confirmation',function(event) {
    event.preventDefault();
    // var loan_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'loan_id' : loan_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/loan/adjust_loan/'+loan_id,post_data).done(function(data)
    {

        if(data == "No Permission"){
            $('#responsive_modal_adjust').modal('hide');
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else if(data == "success")
        {
            $('#responsive_modal_adjust').modal('hide');
            $('#adjust_success').slideDown();
            setTimeout( function(){$('#delete_success').slideUp()}, 5000 );

            table.row($('#delete_'+loan_id).parents('tr')).remove().draw();
        }
        else if(data == "failed")
        {
            $('#responsive_modal_adjust').modal('hide');

            $('#adjust_failure').slideDown();
            setTimeout( function(){$('#delete_failure').slideUp()}, 5000 );        
        }

    }).error(function() {
        alert("<?php echo $this->lang->line('default_error');?>");                
    });
});
/*ajax loan_adjust end*/




// $(document).on('keyup', '#payout_amount_check', function(event) {
// 	event.preventDefault();
// 	var input_amount = $(this).val();

// 	$.get('<?php echo site_url() ?>/loan/get_loan_info/'+loan_id, function(data) {
// 		var data_quantity = $.parseJSON(data);


// 		var due_loan = parseFloat(data_quantity.all_loan_by_id.loan_amount) - parseFloat(data_quantity.all_loan_by_id.total_payout_amount);
// 		if( parseFloat(input_amount) > parseFloat(due_loan)){
// 			$('#payout_btn').prop("disabled",true);
// 			$("input[name='payout_amount']").parent().parent().addClass('has-error');
// 			alert ("<?php echo $this->lang->line('alert_more_paid');?>");
// 		}
// 		else {
// 			$('#payout_btn').prop("disabled",false);
// 			$("input[name='payout_amount']").parent().parent().removeClass('has-error');
// 		}
// 	});

// });	
</script>