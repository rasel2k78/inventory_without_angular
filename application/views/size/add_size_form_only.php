<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title_size'); ?></span>
        </div>
    </div>
    <div class="portlet-body form" id="size_insert_div">
        <!-- BEGIN FORM-->
    <?php $form_attrib = array('id' => 'size_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>

        <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('Size_name'); ?>*</label>
                <div class="col-md-9">

                    <input type="text" class="form-control" name="sizes_name" placeholder="<?php echo $this->lang->line('Size_name'); ?>">
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn green"><?php echo $this->lang->line('save'); ?></button>
            <button data-dismiss="modal" class="btn dark btn-outline" type="button"><?php echo $this->lang->line('cancle'); ?></button>
        </div>
    <?php echo form_close();?>
        <!-- END FORM-->
    </div>
</div>

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->	