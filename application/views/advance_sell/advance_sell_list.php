<?php
/**
 * "All Sell List Page" 
 *
 * This is page display all sell lists for checking individual sell and  delete.
 * 
 * @link   Sell_list_all/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
// echo $this->input->cookie('printer_settings');exit;
?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->

<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo CURRENT_ASSET;?>assets/ppp.css" rel="stylesheet" type="text/css" media="print"/>


<style type="text/css">
	.lasttablebg { float: right; overflow: hidden; }
	.signaturebg { width: 960px; overflow: hidden; }
	.signaturebg p { 
		border-top: 1px solid #ddd;
		width: 240px;
		padding-bottom: 0;
		line-height: 40px;
		font-size: 16px;
		font-family: arial;
		margin-top: 70px;
		text-align: center;
	}
	.thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
	.thankyousms p { margin: 15px 0 0 0; padding: 0; }
	.thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
	#invoice{border:1px solid cadetblue;padding: 20px;}



	@media print {
		body {
			/* zoom:80%;  */
			height: auto;
			font-size: 12px;
			width: auto;
			margin: 1.6cm;
			/* transform: scale(.5); */
		}


		.header, .hide { visibility: hidden }
		display: none;
		/* zoom:80%; */
		table {page-break-inside: avoid;}
	}
</style>

<style type="text/css" media="print">
    /* body {
        zoom:85%; or whatever percentage you need, play around with this number
    } */

    
    .header, .hide { visibility: hidden }
    display: none;
    table {page-break-inside: avoid;}
    a{visibility: hidden;}

</style>

<?php date_default_timezone_set("Asia/Dhaka");?>
<div class="row hidden-print">
	<div class="col-md-6">
		<div class="alert alert-success" id="delete_success" style="display:none" role="alert">Advance Invoice Deleted Successfully </div>
		<div class="alert alert-danger" id="delete_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_failed');?> </div>
		<div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?> </div>

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green-haze">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-list"></i><?php echo $this->lang->line('adv_sell_list');?>
				</div>

			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_2">
					<thead>
						<tr>
							<th><?php echo $this->lang->line('left_customer');?></th>
							<th><?php echo $this->lang->line('customer_phone');?></th>
							<th>
								<?php echo $this->lang->line('left_voucher_no');?>
							</th>
							<th>Delivery Date </th>
							<th>
								<?php echo $this->lang->line('left_details_delete');?>
							</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<th><?php echo $this->lang->line('left_customer');?></th>
							<th><?php echo $this->lang->line('customer_phone');?></th>
							<th><?php echo $this->lang->line('left_voucher_no');?></th>
							<th>
								<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control dont-search" name="from_create_date">
									<span class="input-group-addon">
										to
									</span>
									<input type="text" class="form-control dont-search" name="to_create_date">
								</div>
							</th>
							<th><?php echo $this->lang->line('left_details_delete');?></th>
						</tr>
					</tfoot>

				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->

	</div>
	<div class="col-md-6">
		<div class="portlet box red" id="adv_edit_div" style="display: none">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i><span id="form_header_text" >Edit Advance Sell</span>
				</div>
			</div>
			<div class="portlet-body form"  >
				<!-- BEGIN FORM-->

				<?php $form_attrib = array('id' => 'adv_edit_form','class' => 'form-horizontal form-bordered');
				echo form_open('',  $form_attrib, '');?>

				<div class="form-body" style="display: none">
					<div class="form-group">
						<label class="control-label col-md-3">Custom Voucher No</label>
						<div class="col-md-9">
							<input type="text" class="form-control"  name="custom_voucher_no">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3">Total Advance Paid Amount</label>
						<div class="col-md-9">
							<input type="text" class="form-control" min="1" step="any" name="adv_paid_amt" required >
							<input type="hidden" class="form-control" name="adv_sell_id"  readonly="true" >

						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Next Delivery Date </label>
					<div class="col-md-3">
						<input class="form-control form-control-inline input-medium date-picker" size="16" data-date-start-date="+1d" type="text" id="delivery_date_select" value="" name="next_delivery_date" placeholder="<?php echo $this->lang->line('delivery_date');?>" />
						<span class="help-block" id="delivery_help_block"></span>
					</div>
				</div>
				<div class="form-actions">
					<button type="button" class="btn default pull-right" id="edit_cancel" style="margin-left: 8px">Cancel</button>
					<button type="submit" id="save_id" class="btn green pull-right">Update</button>
				</div>
				<?php echo form_close();?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="portlet box red" id="sell_voucher_details_form" style="display:none">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i><span id="form_header_text" >Advance Sell Details</span>
				</div>
			</div>
			<div class="portlet-body form" id="sell_due_paymentmade_div" >
				<!-- BEGIN FORM-->

				<?php $form_attrib = array('id' => 'adv_voucher_details','class' => 'form-horizontal form-bordered');
				echo form_open('/Advance_sell_list/complete_adv_sell',  $form_attrib, '');?>

				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="sell_local_voucher_no"  readonly="true">
							<input type="hidden" class="form-control" name="customer_name"  readonly="true">
							<input type="hidden" class="form-control" name="customer_id"  readonly="true" >
							<input type="hidden" class="form-control" name="adv_sell_id"  readonly="true" >
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3">Custom Voucher</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="custom_voucher_no"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_grand_total');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="grand_total"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_discount');?> </label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="discount"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_net_payable');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="net_payable"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_paid');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="paid"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_due');?></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="due"  readonly="true">
						</div>
					</div>
				</div>
				<div class="form-group">
					<table id="table_items" class="table table-hover control-label col-md-3">
						<thead>
							<tr >
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_item_name');?></th>
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_quantity');?></th>
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_price');?> </th>
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_sub_total');?></th>
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_type');?></th>
								<th class="control-label col-md-2"><?php echo $this->lang->line('right_dis_amount');?></th>
								<th class="control-label"><?php echo $this->lang->line('imei_or_serial');?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<th>
					<button type="submit" class="btn btn-success" style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;">Complete Order</button>
					<!-- <a style="margin-left: 50%;margin-top: 10px;margin-bottom: 10px;" href = "<?php echo site_url();?>/sell" id="complete_sell_adv" class="btn btn-success"><i class="fa fa-list"></i> Complete Order </a> -->
				</th>
				<?php echo form_close();?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
</div>
<div class="portlet light" style="display: none" id="voucher_preview">
	<div class="portlet-body" style="padding:30px;">
		<div class="invoice" id="invoice">
		</div>
	</div>
</div>
<!-- Modal view starts -->
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?>: <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->lang->line('alert_delete_details');?>
			</div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
	<button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>


<div class="portlet light" style="display: none" id="voucher_preview">
	<div class="portlet-body" style="padding:30px;">
		<div class="invoice" id="invoice">


            <!---header will replaced---!>


            <div class="row" class="col-xs-12">
                <div class="col-xs-12">
                    <div class="col-xs-8">
                        <p>No.</p>
                    </div>
                    <div class="col-xs-4">
                        <p>Date: <?php echo date('d-M-Y');?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-6">
                    	<!-- <p>Name : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </p> -->



                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>

            <!-- Modal view ends -->

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
            <!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
            <!-- 	 <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
            <!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>

            <!-- date picker js starts -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
            <!-- date picker js ends -->

            <!-- Boostrap modal starts-->
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
            <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
            <!-- Boostrap modal end -->
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
            <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->	


            <script>

            	jQuery(document).ready(function() {    
        		// $('.date-picker').datepicker();
        		$('.date-picker').datepicker({
        			format: 'yyyy-mm-dd',
        			rtl: Metronic.isRTL(),
        			orientation: "left",
        			autoclose: true
        		});
        		UIExtendedModals.init();
        	});

            	$('#sample_2 tfoot th').each(function (idx,elm){
            		if (idx == 0 || idx ==1 || idx == 2 || idx == 4 ) { 
            			var title = $('#example tfoot th').eq( $(this).index() ).text();
            			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            		}
            	});
        // table_sell_list_all = $('#sample_2').DataTable();    
        var table_sell_list_all = $('#sample_2').DataTable({
        	"processing": true,
        	"serverSide": true,
        	"ajax": "<?php echo site_url(); ?>/Advance_sell_list/advance_sell_list_info_for_datatable/",
        	"lengthMenu": [
        	[10, 15, 20,30],
        	[10, 15, 20,30] 
        	],
        	"pageLength": 10,
        	"language": {
        		"lengthMenu": " _MENU_ records",
        		"paging": {
        			"previous": "Prev",
        			"next": "Next"
        		}
        	},
        	"columnDefs": [{  
        		'orderable': true,
        		'targets': [0]
        	}, {
        		"searchable": true,
        		"targets": [0]
        	}],
        	"order": [
        	[3, "asc"]
        	]
        });    

        /*data table customize search starts*/
        table_sell_list_all.columns().every( function () {
        	var that = this;
        	$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
        		if (!($(this).hasClass('dont-search'))) {
        			that
        			.search( this.value )
        			.draw();
        		}
        	});

        	$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
        		var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
        		var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

        		var final_string = '';
        		if(date_from !='' || date_to != ''){
        			if (date_from!='' && date_to=='') {
        				final_string = date_from+'_0';
        			}
        			if (date_to!='' && date_from=='') {
        				final_string = '0_'+date_to;
        			}
        			if (date_from!='' && date_to!='') {
        				final_string = date_from+'_'+date_to;
        			}
        		}
        		else{
        			final_string = '0_0';
        		}
        		that.search(final_string).draw();
        	});
        });
        /*datatable customize search ends*/

    </script>
    <script type="text/javascript">


    	$('table').on('click', '.adv_sell_voucher_details',function(event) {
    		event.preventDefault();
    		// alert("hello");

    		var adv_sell_details_id = $(this).attr('id').split('_')[1];
    		$('#current_adv_sell_details_id').remove();
    		$.get('<?php echo site_url();?>/Advance_sell_list/get_adv_sell_info/'+adv_sell_details_id).done(function(data) {
    			var sell_data = $.parseJSON(data);
    			console.log(sell_data);
    			$('#sell_voucher_details_form').show(400);
    			$('#adv_edit_div').hide(400);

    			$('input[name = customer_name]').val(sell_data.customer_info.customers_name);
    			$('input[name = customer_id]').val(sell_data.customer_info.customers_id);
    			$('input[name = adv_sell_id]').val(sell_data.voucher_number.adv_sell_details_id);
    			
    			$('input[name = sell_local_voucher_no]').val(sell_data.all_sell_info_by_id[0].sell_local_voucher_no);
    			$('input[name = custom_voucher_no]').val(sell_data.all_sell_info_by_id[0].custom_voucher_no);
    			
    			$('input[name = net_payable]').val(sell_data.all_sell_info_by_id[0].net_payable);
    			$('input[name = paid]').val(sell_data.all_sell_info_by_id[0].paid);
    			$('input[name = due]').val(sell_data.all_sell_info_by_id[0].due);
    			$('input[name = discount]').val(sell_data.all_sell_info_by_id[0].discount);
    			$('input[name = grand_total]').val(sell_data.all_sell_info_by_id[0].grand_total);
    			$('#sell_voucher_print').val(adv_sell_details_id);
    			$('#print_now').attr('href', '#voucher_info@'+adv_sell_details_id);
    			var table_html = '';
    			$.each(sell_data.all_sell_info_by_id, function(index, val) {
    				table_html += '<tr>'
    				table_html += '<td>'
    				table_html += val.spec_set;
    				table_html += '</td><td>';
    				table_html += val.quantity;
    				table_html += '</td><td>';
    				table_html += val.selling_price;
    				table_html += '</td><td>';
    				table_html += val.sub_total;
    				table_html += '</td><td>';
    				table_html += val.discount_type;
    				table_html += '</td><td>';
    				table_html += val.discount_amount;
    				table_html += '</td><td>';
    				table_html += val.imei_barcode;
    				table_html += '</td>';
    				table_html += '</tr>'
    			});
    			$('#table_items tbody').html(table_html);

    		}).error(function() {
    			alert("An error has occured. pLease try again");
    		});
    	});


    	$("#print_new").click(function(event) 
    	{
    		$("#voucher_preview").show();
    		setTimeout(function(){ window.print(); $("#voucher_preview").hide();}, 500);    
    	});



// Ajax Delete 
var adv_sell_details_id = '' ;
var selected_name = '';

$('table').on('click', '.delete_adv_sell',function(event) {
	event.preventDefault();
	adv_sell_details_id = $(this).attr('id').split('_')[1];
	selected_name = $(this).parent().parent().find('td:first').text();
	$('#selected_name').html(selected_name);
});


$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
	event.preventDefault();
    // var buy_details_id = $(this).attr('id').split('_')[1];
    var post_data ={
    	'adv_sell_details_id' : adv_sell_details_id,
    	'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/Advance_sell_list/delete_adv_sell/'+adv_sell_details_id,post_data).done(function(data)
    {
    	var sell_data = $.parseJSON(data);

    	if(sell_data == "success"){
    		$('#responsive_modal_delete').modal('hide');
    		$('#delete_success').slideDown();
    		setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
    		table_sell_list_all.ajax.reload();
    	}    
    	if(sell_data == "error transaction"){
    		$('#responsive_modal_delete').modal('hide');
    		alert("Transaction Error");
    	}
    }).error(function() {
    	alert("An error has occurred. pLease try again");                
    });
});

$(document).on('click', '.adv_sell_voucher_edit', function(event) {
	event.preventDefault();
	var adv_sell_details_id = $(this).attr('id').split('_')[1];
	$('#current_adv_sell_details_id').remove();
	$.get('<?php echo site_url();?>/Advance_sell_list/get_adv_sell_info//'+adv_sell_details_id).done(function(data) {
		var sell_data = $.parseJSON(data);
		//console.log(sell_data);
		if(sell_data == "No Permission"){
			$('#access_failure').slideDown();
			setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
		}
		else{
			$('#sell_voucher_details_form').hide(400);
			$('#adv_edit_div').show(400);
			$('input[name = adv_paid_amt]').val(sell_data.all_sell_info_by_id[0].paid);
			$('input[name = adv_sell_id]').val(adv_sell_details_id);
			$('input[name = next_delivery_date]').val(sell_data.all_sell_info_by_id[0].delivery_date);
			$('input[name = custom_voucher_no]').val(sell_data.all_sell_info_by_id[0].custom_voucher_no);

		}
	}).error(function() {
		alert("An error has occurred. Please try again");
	});
});

$(document).on('submit', '#adv_edit_form', function(event) {
	event.preventDefault();
	// alert("Hello Data");
	var data = {};
	data.paid = $('input[name=adv_paid_amt]').val();
	data.adv_sell_details_id = $('input[name=adv_sell_id]').val();
	data.delivery_date = $('input[name=next_delivery_date]').val();
	data.custom_voucher_no = $('input[name=custom_voucher_no]').val();

	$.post('<?php echo site_url() ?>/Advance_sell_list/update_adv_sell_info',data).done(function(data1, textStatus, xhr) {
		var data2 = $.parseJSON(data1);
		if(data2 == "success"){
			$('#adv_edit_div').hide(400);
			table_sell_list_all.ajax.reload();
			$('input[name="adv_paid_amt"]').val('');
			$('input[name="adv_sell_id"]').val('');
			$('input[name="next_delivery_date"]').val('');
			$('input[name=custom_voucher_no]').val('');
			alert("Updated Successfully");
		}
		if(data2 == "error transaction"){
			alert("Transaction Error");
		}
	}).error(function() {
		$('#insert_failure').slideDown();
		setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
	});

});

</script>