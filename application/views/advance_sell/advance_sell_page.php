<?php
/**
 * Form Name:"Sell Form" 
 *
 * This is form for selling items.
 * 
 * @link   Sell/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<!-- END PAGE LEVEL STYLES -->
<style type="text/css">


	.container { max-width: 630px; min-height: 900px; background: #fff; margin: 0 auto; }
	/* .main_body { overflow: hidden; width: 595px; height: 842px; margin: 0 auto; background: #eee;}  */
	.balnk_top { min-height: 150px; }
	.address_part { overflow: hidden; margin: 15px;} 
	.left_part { 
		float: left;
		width: 270px;
		background-image: linear-gradient(#000000, #000000), linear-gradient(#000000, #000000), linear-gradient(#000000, #000000), linear-gradient(#000000, #000000);
		background-repeat: no-repeat;
		background-size: 8px 3px;
		background-position: top left, top right, bottom left, bottom right;
		border: solid #000000;
		text-align: justify;
		border-width: 0 3px;
		display: inline-block;
		vertical-align: top;
		padding: 0 0 0 0;
	} 
	.left_part ul { margin: 0 auto; padding: 10px 0px 10px 20px;}
	.left_part ul li { list-style: none; color: #222; line-height: 30px; font-size: 15px; font-family: arial;}
	.right_part { float: right; width: 270px;}
	.right_part table tr td { border:1px solid #c1c1c1; padding: 8px 0 8px 8px; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 13px; } .back_col { width: 90px; background: #ddd;}
	.right_col { width: 160px;} .description { margin: 15px; overflow: hidden;} .description p { font-family: Arial, Helvetica, sans-serif}
	.fist_border { border:1px solid #000; float: left; width: 30px; padding: 5px; height: 330px;} 
	.second_border { height: 330px; border:1px solid #000; float: left;  width: 255px; padding: 5px;}
	.third_border { height: 330px; border:1px solid #000; float: left; width: 60px; padding: 5px;}
	.fourth_border { height: 330px; border:1px solid #000; float: left; width: 97px; padding: 5px;} 
	.fifth_border { height: 330px; border:1px solid #000; float: left; width: 98px; padding: 5px;}
	.description table tr td { font-family: Arial, Helvetica, sans-serif; font-size: 13px;}
	.description table { margin-top: 15px; margin-bottom: 5px;}
	.total_amount { float: right;} .total_amount ul { margin: 0; padding: 0;} 
	.total_amount ul li { list-style: none; display: inline; border-right: 1px solid #000; border-bottom: 1px solid; margin: 0; float: left; text-align: left; width: 109px;
		font-family: arial; font-size: 11px; padding: 10px 0 10px 0;}
		.total_amount ul li.last-child { border-right: none;} .bottom_text { margin: 15px;} .bottom_text { font-family: Arial, Helvetica, sans-serif; font-size: 14px;}

		.nmpd-grid {border: none; padding: 20px;}
		.nmpd-grid>tbody>tr>td {border: none;}
		#voucher_preview{font-size: 9px;}
		/* Some custom styling for Bootstrap */
		.qtyInput {display: block;
			width: 100%;
			padding: 6px 12px;
			color: #555;
			background-color: white;
			border: 1px solid #ccc;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		}
		/* Tutorial Class Starts*/
		.focus_tutorial_element{
			position: relative;
			z-index: 100000;
			background-color: #f1f3fa;
		}


		.tutorial_tooltip{
			height: 180px;
			width: 220px;
			position: fixed;
			z-index: 1000000;
			top: 100px;
			left: 100px;
			background: #fff;
		}

		.tutorial_tooltip_edge{
			height: 20px;
			width: 20px;
			position: absolute;
			top: 10px;
			left: -10px;
			background-color: #fff;
			-ms-transform: rotate(45deg);
			-webkit-transform: rotate(45deg);
			transform: rotate(45deg);
			z-index: -1;
		}

		.tutorial_tooltip_content{
			padding: 10px;
		}

		.lasttablebg table tbody tr td {
			/* border: 1px solid #ccc; */
			text-align: center;
			width: 231px;
		}
	/* table {    
		font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
	} */
	.lasttablebg { float: right; overflow: hidden; }
	.signaturebg { width: 960px; overflow: hidden; }
	.signaturebg p { 
		border-top: 1px solid #ddd;
		width: 240px;
		padding-bottom: 0;
		line-height: 40px;
		font-size: 16px;
		font-family: arial;
		margin-top: 70px;
		text-align: center;
	}
	.thankyousms { width: 960px; text-align: center; border: 1px solid #ddd; margin-top: 20px; }
	.thankyousms p { margin: 15px 0 0 0; padding: 0; }
	.thankyousms h1 { margin: 15px 0 0 0; padding: 0 0 15px 0; }
	#invoice{border:1px solid cadetblue;padding: 20px;}



	@media print {
		body {
			/* zoom:80%;  */
			height: auto;
			font-size: 8px;
			width: auto;
			margin: 1.6cm;
			/* transform: scale(.5); */
		}


		.header, .hide { visibility: hidden }
		display: none;
		/* zoom:80%; */
		table {page-break-inside: avoid;}
	}



</style>
<style type="text/css" media="print">
	/* body {
		zoom:85%; or whatever percentage you need, play around with this number
	} */


	.header, .hide { visibility: hidden }
	display: none;
	table {page-break-inside: avoid;}
	a{visibility: hidden;}

</style>


<!--<script type="text/javascript">
	document.getElementById('footer').style.display = 'none';
</script> -->
<?php date_default_timezone_set("Asia/Dhaka");?>
<script src="<?php echo base_url()?>assets/typed.js" type="text/javascript"></script>
<style>

	.typed-cursor{
		opacity: 1;
		font-weight: 100;
		-webkit-animation: blink 1.7s infinite;
		-moz-animation: blink 0.7s infinite;
		-ms-animation: blink 0.7s infinite;
		-o-animation: blink 0.7s infinite;
		animation: blink 0.7s infinite;
	}
</style>
<div id="typed-strings" style="display:none">
	<span>This is test html.</span>
</div>
<div id="typed-value" style="display:none">
	<span>This is test html.</span>
</div>
<span id="typed" style="white-space:pre;"></span> 

<div class="row hidden-print">
	<div class="col-md-6">
		<select id='holdingInvoiceSelection' class='form-control' style="display: none;">
		</select>
		<script type="text/javascript">
			var allHoldingInvoices;
			var setHoldingInvoices = function() {
				allHoldingInvoices = localStorage.getItem('hodldingInvoices');
				if(allHoldingInvoices == null)
				{
					allHoldingInvoices = [];
				}
				else
				{
					allHoldingInvoices = JSON.parse(allHoldingInvoices);
				}

				var html = "<option value=''><?php echo $this->lang->line('select_hold_invoice'); ?></option>";

				$.each(allHoldingInvoices, function(index, val) {
					html += "<option value='"+index+"'>"+val.holding_sn+"</option>";
				});

				$('#holdingInvoiceSelection').html(html);
			}

			setHoldingInvoices();
		</script>
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('adv_sell_title'); ?>
				</div>
			</div>
			<div class="portlet-body form">
				<?php
				$form_attrib = array('class' => 'form-horizontal form-bordered', 'autocomplete'=>'off');
				echo form_open('', $form_attrib); ?>
				<div class="form-body">
					<div class="form-group" style="display: none">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_sell_type'); ?>
						</label>

						<div class="col-md-9 input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_type_tooltip');?>"></i>
							<select class="form-control" id="sell_type_id" name="sell_type">

								<option value="retail"><?php echo $this->lang->line('left_sell_type_retail'); ?></option>
								<option value="wholesale"><?php echo $this->lang->line('left_sell_type_wholesale'); ?></option>

							</select>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_item_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
						</label>
						<div class="col-md-7 input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_item_select_tt');?>"></i>
							<input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('left_item_name_placeholder'); ?>" id="item_select" class="form-control">
							<input type="hidden" autocomplete="off" name="item_spec_set_id" id="spec_id_for_get" class="form-control">
							<table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
							</table>
						</div>
					</div>
					<br/>
					<div class="form-group" id="current_qty" style="display: none">
						<label class="control-label col-md-3"><?php echo $this->lang->line('current_quantity'); ?> </label>
						<div class="col-md-9 input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('current_quantity_tooltip');?>"></i>
							<input type="text" id="current_quantity" name="current_quantity_inventory" class="form-control" readonly="true">

						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_quantity'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9">
							<!-- <input type="text" class="form-control" name="quantity"  placeholder="মোট পরিমাণ" > -->
							<div class="input-group">
								<input type="number" min="1" step="1" pattern="^[0-9]" id="numpadButton1"  name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('left_quantity'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn1" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_quantity_err_msg'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_selling_price'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9">
							<div class="input-group">
								<input type="number"  class="form-control selling_price_and_numberpad" step="any" min="0"   name="selling_price" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_price'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn2" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_selling_price_err_msg'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_comments'); ?>
						</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="sell_comments"  placeholder="<?php echo $this->lang->line('left_comments_placeholder'); ?>">
						</div>
					</div>

					<div class="form-group" style="display: none">
						<div class="panel-group accordion" id="accordion3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">

											<?php echo $this->lang->line('left_extra_fields'); ?>
										</a>
									</h4>
								</div>
								<div id="collapse_3_1" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="extra_fields" >

											<div class="form-group">
												<label class="control-label col-md-3"><?php echo $this->lang->line('left_discount_type'); ?>
												</label>
												<div class="col-md-3 input-icon">
													<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sell_dicount_tt');?>"></i>
													<select class="form-control" id="dis_type" name="discount_type">
														<option value=""><?php echo $this->lang->line('select_one')?></option>
														<option value="amount"><?php echo $this->lang->line('left_discount_type_taka'); ?></option>
														<option value="percentage"><?php echo $this->lang->line('left_discount_type_percentage'); ?></option>
														<option value="free_quantity"><?php echo $this->lang->line('left_discount_type_quantity'); ?></option>
													</select>

												</div>
												<div class="col-md-6">
													<input type="number" min="0" step="any" name="discount_amount" id="dis_amt_id" class="form-control" placeholder="<?php echo $this->lang->line('left_discount_type_placeholder'); ?>">

												</div>
											</div>


										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="form-actions">
					<button  id="cancle_item" type="button" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle_button'); ?></button>
					<button id="add_item" type="button" class="btn green  pull-right"><?php echo $this->lang->line('left_save_button'); ?></button>
					<button id="update_item" style="display:none" type="button" class="btn yellow pull-right"><?php echo $this->lang->line('left_edit_save_button'); ?></button>
				</div>
				<?php echo form_close(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div id="success_messages">
			<div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('customer_insert_success'); ?></div>
			<div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
			<div class="alert alert-success" id="sell_success" style="display:none" role="alert"><?php echo $this->lang->line('sell_success');?></div>
		</div>
		<div id="error_messages">

			<div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?></div>
			<div class="alert alert-danger" id="no_username_customer" style="display:none" role="alert"><?php echo $this->lang->line('no_username_customer'); ?></div>
			<div class="alert alert-danger" id="no_phone" style="display:none" role="alert"><?php echo $this->lang->line('no_phone'); ?></div>
			<div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>
			<div class="alert alert-danger" id="no_permission_left" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>

			<div class="alert alert-danger" id="paid_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_failure');?></div>
			<div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
			<div class="alert alert-danger" id="price_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('price_amount_failure');?></div>
			<div class="alert alert-danger" id="vat_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('vat_amount_failure');?></div>
			<div class="alert alert-danger" id="vat_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('vat_not_numeric');?></div>
			<div class="alert alert-danger" id="dis_not_valid" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_valid');?></div>
			<div class="alert alert-danger" id="dis_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('dis_not_numeric');?></div>
			<div class="alert alert-danger" id="item_amount_failure" style="display:none" role="alert"><?php echo $this->lang->line('item_amount_failure');?></div>
			<div class="alert alert-danger" id="paid_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('paid_not_numeric');?></div>
			<div class="alert alert-danger" id="item_info_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_info_empty');?></div>
			<div class="alert alert-danger" id="no_card_voucher" style="display:none" role="alert"><?php echo $this->lang->line('no_card_voucher');?></div>
			<div class="alert alert-danger" id="card_voucher_not_numeric" style="display:none" role="alert"><?php echo $this->lang->line('card_voucher_not_numeric');?></div>
			<div class="alert alert-danger" id="select_card_only" style="display:none" role="alert"><?php echo $this->lang->line('select_card_only');?></div>
			<div class="alert alert-danger" id="card_amount_more_than_total" style="display:none" role="alert"><?php echo $this->lang->line('card_amount_more_than_total');?></div>

			<div class="alert alert-danger" id="select_customer_for_due_transaction" style="display:none" role="alert"><?php echo $this->lang->line('select_customer_for_due_transaction');?></div>
			<div class="alert alert-danger" id="total_calculation_mismatch" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch');?></div>

			<div class="alert alert-danger" id="total_calculation_mismatch_only_in_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_only_in_discount');?></div>
			<div class="alert alert-danger" id="total_calculation_mismatch_after_discount" style="display:none" role="alert"><?php echo $this->lang->line('total_calculation_mismatch_after_discount');?></div>
			<div class="alert alert-danger" id="item_quantity_empty" style="display:none" role="alert"><?php echo $this->lang->line('item_quantity_empty');?></div>
			<div class="alert alert-danger" id="no_name" style="display:none" role="alert"><?php echo $this->lang->line('sales_rep_no_name');?></div>
			<div class="alert alert-danger" id="calculation_mismatch" style="display:none" role="alert"><?php echo $this->lang->line('default_calculation_erro');?></div>

			<div class="alert alert-danger" id="discount_is_greater_than_grand_total" style="display:none" role="alert"><?php echo $this->lang->line('discount_is_greater_than_grand_total');?></div>

		</div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green-haze" id="right_box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title'); ?> 
				</div>

			</div>
			<div class="portlet-body form">
				<form id="sell_form" class="form-horizontal form-bordered" action="" method="get" accept-charset="utf-8">
					<div class="form-body">
						<!-- For custom voucher number by user -->

					<!-- <div class="form-group">
					<label class="control-label col-md-3"><?php echo $this->lang->line('right_sales_rep'); ?>
					</label>
					<div class="col-md-6">
						<select class="form-control" name="sales_rep_id" id="sales_rep_select" data-live-search="true">
						</select>
					</div>
				</div> -->
				<div class="form-group" style="display: none">
					<label class="control-label col-md-3"><?php echo $this->lang->line('right_sales_rep'); ?>
					</label>
					<div class="col-md-6 input-icon">
						<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sales_rep_tt');?>"></i>
						<input type="text" autocomplete="off" name="sales_rep_name" placeholder="<?php echo $this->lang->line('right_sales_rep_placeholder'); ?>" id="sales_rep_select" class="form-control">
						<input type="hidden" autocomplete="off" name="sales_rep_id"  class="form-control">
						<table class="table table-condensed table-hover table-bordered clickable" id="sales_rep_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
						</table>
					</div>
					<div class=" col-md-3">
						<a href="#sales_rep_form_modal" title="<?php echo $this->lang->line('right_sales_rep_title'); ?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>

				<!--             <div class="form-group">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_customer');?>
				</label>
				<div class="col-md-6">
					<select class="form-control" name="customers_id" id="customer_select" data-live-search="true">
					</select>
				</div>

			</div> -->
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_customer');?>
				</label>
				<div class="col-md-6 input-icon">
					<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('customer_tt');?>"></i>
					<input type="text" autocomplete="off" name="customers_name" placeholder="<?php echo $this->lang->line('right_customer_placeholder');?>" id="customers_select" class="form-control">
					<span class="help-block" id="customer_help_block" ></span>
					<input type="hidden" autocomplete="off" name="customers_id"  class="form-control">
					<table class="table table-condensed table-hover table-bordered clickable" id="customers_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
					</table>
				</div>
				<div class=" col-md-3 col-sm-4">
					<a href="#customer_form_modal" title="<?php echo $this->lang->line('right_customer_title');?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
						<i class="fa fa-plus"></i>
					</a>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo $this->lang->line('delivery_date');?> </label>
				<div class="col-md-3">
					<input class="form-control form-control-inline input-medium date-picker" size="16" data-date-start-date="+1d" type="text" id="delivery_date_select" value="" name="delivery_date" placeholder="<?php echo $this->lang->line('delivery_date');?>" />
					<span class="help-block" id="delivery_help_block"></span>
				</div>
			</div> 
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no'); ?></label>
				<div class="col-md-9 input-form">
					<input type="text" class="form-control" name="voucher_no" id="voucher_no_id" placeholder="<?php echo $this->lang->line('right_voucher_no'); ?>">

				</div>
			</div> 
<!-- 			<div class="form-group">
				<label class="control-label col-md-3">Delivery Date </label>
				<div class="col-md-3">
					<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="delivery_date" data-date-start-date="+0d" placeholder="<?php echo $this->lang->line('left_date');?>" />
					<span class="help-block"></span>
				</div>
			</div>  -->
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
				<div class="radio-list">
					<label class="radio-inline">
						<input type="radio" name="payment_type" id="options_cash" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
					</label>
					<label class="radio-inline">
						<input type="radio" name="payment_type" id="options_card" value="card"><?php echo $this->lang->line('right_type_card'); ?>
					</label>
					<label class="radio-inline">
						<input type="radio" name="payment_type" id="options_cash_card_both" value="cash_card_both"><?php echo $this->lang->line('right_type_both'); ?>
					</label>
					<label class="radio-inline">
						<input type="radio" name="payment_type" id="options_cheque" value="cheque"><?php echo $this->lang->line('cheque'); ?>
					</label>
				</div>
			</div>
			<div class="form-group" style="display: none" id="ac_select_div">
				<div class="col-md-6 form-group">
					<label class="control-label col-md-5"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
					</label>
					<div class="col-md-7">
						<input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
						<input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
						<span class="help-block" id="bank_acc_help_block" ></span>
						<table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
						</table>
					</div>
				</div>
				<div class="col-md-6 form-group">
					<label class="control-label col-md-6">Charge</label>
					<div class="col-md-6">
						<input type="text"  class="form-control" name="bank_charge_percent" id="charge_percent_id">
					</div>
				</div>
			</div>
			<div class="form-group" style=" display:none" id="card_voucher_no">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_card_voucher_no'); ?></label>
				<div class="col-md-6">

					<input type="text" class="form-control" name="card_payment_voucher_no" id="card_pay_voucher_id" placeholder="<?php echo $this->lang->line('right_card_voucher_no'); ?>">
					<span class="help-block"></span>
				</div>
			</div>

			<div class="form-group" style=" display:none" id="card_amount_for_both">
				<label class="control-label col-md-3"><?php echo $this->lang->line('right_card_amount'); ?></label>
				<div class="col-md-6">

					<input type="number" min="0" step="any" class="form-control" name="card_amount" id="card_amount_id" placeholder="<?php echo $this->lang->line('right_card_amount'); ?>">
					<span class="help-block"></span>
				</div>
			</div>
			<div class="form-group" style=" display:none" id="cheque_num">
				<label class="control-label col-md-3"><?php echo $this->lang->line('chk_number');?></label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="cheque_number" id="chk_num" placeholder="<?php echo $this->lang->line('chk_number');?>">
					<span class="help-block"></span>
				</div>
			</div>
			<table id="sell_table" class="table table-condensed">
				<thead>
					<tr>
						<th><?php echo $this->lang->line('right_item_name');?></th>
						<th><?php echo $this->lang->line('right_quantity');?></th>
						<th><?php echo $this->lang->line('right_sell_price');?></th>
						<th><?php echo $this->lang->line('right_sub_total');?></th>
						<!-- <th><?php echo $this->lang->line('right_discount_type');?></th> -->
						<th><?php echo $this->lang->line('right_delete_edit');?></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th><?php echo $this->lang->line('right_total');?>:</th>
						<th id="total_amount"></th>
						<!-- <th></th> -->
						<th></th>
					</tr>
					<tr style="display: none">
						<th></th>
						<th></th>
						<th><?php echo $this->lang->line('right_vat');?> (%):</th>
						<th ><input class='form-control keyup_not_triggered' readonly="true" id="total_vat" name="tax_percentage"  placeholder="<?php echo $this->lang->line('right_vat');?>"> 

							<input type="checkbox" id="inclusive_vat_checkbox"> <b><?php echo $this->lang->line('inclusive_vat');?> </b><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('inclusive_vat_tt');?>"></i>
						</th> 
						<!-- <th></th> -->
						<th></th>
					</tr>
					<tr style="display: none">
						<th></th>
						<th></th>
						<th><?php echo $this->lang->line('right_grand_total');?>:</th>
						<th id="total_amount_with_vat"></th>
						<!-- <th></th> -->
						<th></th>
					</tr>
					<tr style="display: none">
						<th></th>
						<th></th>
						<th><?php echo $this->lang->line('right_discount');?>:</th>
						<th >
							<input class='form-control keyup_not_triggered' type="number" min="0" step="any" id="total_discount" name="discount"  placeholder="<?php echo $this->lang->line('right_discount');?>"> 
							<span class="help-block"></span>
							<input type="checkbox" id="discount_percentage"> <b><?php echo $this->lang->line('discount_percentage');?> </b>
						</th>
						<!-- <th></th> -->
						<th></th>

					</tr>
					<tr style="display: none">
						<th></th>
						<th></th>
						<th><?php echo $this->lang->line('right_net_payable');?>:</th>
						<th  id="total_net_payable" style="font-size: 35px;"></th>
						<!-- <th></th> -->
						<th></th>
					</tr>    
					<!--             <tr>
					<th></th>
					<th></th>
					<th><?php echo $this->lang->line('right_received_money');?>:</th>
					<th ><input class='form-control keyup_not_triggered' type="number" min="0" step="any" id="total_received" name="received_money"  placeholder="<?php echo $this->lang->line('right_received_money');?>"></th>
					<th></th>
					<th></th>
				</tr>
			-->

			<tr >
				<th></th>
				<th></th>
				<th style="font-size: 20px;"><?php echo $this->lang->line('right_paid');?> <sup><i class="fa fa-star custom-required"></i></sup>:</th>
				<th >
					<input style="font-size: 35px;font-weight: 700;" class='form-control' type="number" min="0" step="any" id="total_paid" name="paid" >    
					<span class="help-block" ></span>
				</th>
				<!-- <th></th> -->
				<th></th>  
			</tr>    
			<tr style="display: none">
				<th></th>
				<th></th>
				<th><?php echo $this->lang->line('right_due');?>:</th>
				<th id="total_due"></th>
				<!-- <th></th> -->
				<th></th>
			</tr>
			<tr style="display: none">
				<th></th>
				<th></th>
				<th><?php echo $this->lang->line('right_return_change_money');?>:</th>
				<th id="total_change"></th>
				<!-- <th></th> -->
				<th></th>
			</tr>    
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<!-- <th></th> -->
				<th>
					<button type="submit" id="save_sell" class="btn green"><?php echo $this->lang->line('right_save');?></button>
					<a href = "#save_sell_info" id="print_now" style="display:none"></a>
				</th>
			</tr>
		</tfoot>
	</table>
</div>    

<div class ="form-actions">

</div>

</form>
</div>    
</div>
</div>
</div>
<div class="portlet light" style="display: none" id="voucher_preview">
	<div class="portlet-body" style="padding:30px;">
		<div class="invoice" id="invoice">


			<!---header will replaced


			<div class="row" class="col-xs-12">
				<div class="col-xs-12">
					<div class="col-xs-8">
						<p>No.</p>
					</div>
					<div class="col-xs-4">
						<p>Date: <?php echo date('d-M-Y');?></p>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="col-xs-6">
						<p>Name : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </p> -->



					</div>
				</div>

				<!-- END EXAMPLE TABLE PORTLET-->
			</div>


			<!-- Customer Modal Form -->
			<div aria-hidden="true" role="dialog" tabindex="-1" id="customer_form_modal" class="modal fade">	
				<div class="modal-content">
					<div class="modal-body">
						<?php $this->load->view('customer/add_customer_form_only');?>
					</div>
				</div>
			</div>
			<!-- Customer Modal Form End -->

			<!-- Sales Rep Modal Form -->
			<div aria-hidden="true" role="dialog" tabindex="-1" id="sales_rep_form_modal" class="modal fade">	
				<div class="modal-content">
					<div class="modal-body">
						<?php $this->load->view('sales_rep/add_sales_rep_form_only');?>
					</div>
				</div>
			</div>
			<!-- Sales Rep Modal Form End -->
			<!-- <span id="newText">sfdsdf</span> -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.numpad.js"></script>
			<!-- date picker js starts -->
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
			<!-- date picker js ends -->
			<script src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.min.js" type="text/javascript"></script>

			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->	
			<script>
				/*lading date picker*/
				jQuery(document).ready(function() {    
        		// $('.date-picker').datepicker();
        		$('.date-picker').datepicker({
        			format: 'yyyy-mm-dd',
        			rtl: Metronic.isRTL(),
        			orientation: "left",
        			autoclose: true
        		});
        		UIExtendedModals.init();
        	});
				/*loading date picker ends*/
				var csrf = $("input[name='csrf_test_name']").val();
				jQuery(document).ready(function($) {
					$.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
					$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
					$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
					$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
					$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
					$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
					$('#numpadButton-btn1').numpad({
						target: $('#numpadButton1')
					});        
					$('#numpadButton-btn2').numpad({
						target: $('.selling_price_and_numberpad')
					});    
				});
				table = $('#sample_2').DataTable();        
				// Customize Boostrap Select for Item Selection.
				var is_imei_used = "";
				var temp_item_id = "";
				var timer;
				$("#item_select").keyup(function(event) 
				{
					$("#item_select_result").show();
					$("#item_select_result").html('');
					clearTimeout(timer);
					timer = setTimeout(function() 
					{
						var search_item = $("#item_select").val();
						var html = '';
						$.post('<?php echo site_url(); ?>/advance_sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
							data = JSON.parse(data);
							if(data.imei_barcode_yes != null && data.imei_barcode_yes != undefined){
								$.each(data, function(index, val) {
									if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
										val.item_spec_set_image = "images/item_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
									html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
								});
								$("#item_select_result").html(html);
								is_imei_used= "yes";
								$('#item_select_result').find('td').eq(0).click();
								$('input[name=quantity]').val(1);
								$('#add_item').prop('disabled', false);
								temp_item_id= new Date().getUTCMilliseconds();
								render_data['imei_serial_data'].push({
									'item_spec_set_id': data.imei_barcode_yes.item_spec_set_id,
									'imei_barcode': search_item,
									'temp_id':temp_item_id,
								});
								is_imei_used= "yes";
								is_unique_barcode= "";
							}
							else if(data.barcode_yes != null && data.barcode_yes != undefined){
								$.each(data, function(index, val) {
									if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
										val.item_spec_set_image = "images/item_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
									html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
								});
								$("#item_select_result").html(html);
								is_imei_used= "no";
								$('#item_select_result').find('td').eq(0).click();
								is_imei_used="no";

								if(is_unique_barcode != "yes" && is_imei_used== "no"){
									$('input[name=quantity]').val(1);
									$('#add_item').prop('disabled', false);
								}
								else{	
									$('input[name=quantity]').val('');
									$('#add_item').prop('disabled', true);
								}
								is_unique_barcode="";
								// $('input[name=quantity]').val(1);
								// $('#add_item').prop('disabled', false);
							}
							else{
								$.each(data, function(index, val) {
									if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
										val.item_spec_set_image = "images/item_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
									html+= '<tr><td is_unique="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
								});
								$("#item_select_result").html(html);
								is_imei_used= "no";
								is_unique_barcode="";

							}
							// if ($('#item_select').val().length >= 17) {
								//     $('#item_select_result').find('td').eq(0).click();
								//     $('input[name=quantity]').val(1);
								//     $('#add_item').prop('disabled', false);
								// }
							});
					},
					500);
				});
				/* Select Sales Representative through Customize Boostrap Select*/
				$("#sales_rep_select").keyup(function(event) 
				{
					$("#sales_rep_select_result").show();
					$("#sales_rep_select_result").html('');
					clearTimeout(timer);
					timer = setTimeout(function() 
					{
						var search_sales_rep = $("#sales_rep_select").val();
						var html = '';
						$.post('<?php echo site_url(); ?>/advance_sell/search_sales_rep_by_name',{q: search_sales_rep,csrf_test_name: csrf}, function(data, textStatus, xhr) {
							data = JSON.parse(data);
							$.each(data, function(index, val) {
								if(val.sales_rep_image == null || val.sales_rep_image == ''){
									val.sales_rep_image = "images/sales_rep_images/no_image.png";
								}
								var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.sales_rep_image+'">';
								html+= '<tr><td data="'+val.sales_rep_id+'">'+image_html+' '+ val.sales_rep_name+'</td></tr>';
							});
							$("#sales_rep_select_result").html(html);
						});
					}, 500);
				});
				$("#sales_rep_select_result").on('click', 'td', function(event) {
					$('input[name="sales_rep_name"]').val($(this).text());
					$('input[name="sales_rep_id"]').val($(this).attr('data'));
					$("#sales_rep_select_result").hide();
				});
				/*Select Customer Through Customize Boostrap Select*/
				$("#customers_select").keyup(function(event) 
				{
					$("#customers_select_result").show();
					$("#customers_select_result").html('');
					clearTimeout(timer);
					timer = setTimeout(function() 
					{
						var search_customers = $("#customers_select").val();
						var html = '';
						$.post('<?php echo site_url(); ?>/advance_sell/search_customer_by_name',{q: search_customers,csrf_test_name: csrf}, function(data, textStatus, xhr) {
							data = JSON.parse(data);
							$.each(data, function(index, val) {
								if(val.customers_image == null || val.customers_image == ''){
									val.customers_image = "images/user_images/no_image.png";
								}
								var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.customers_image+'">';
								html+= '<tr><td data="'+val.customers_id+'">'+image_html+' '+ val.customers_name+' -- '+val.customers_phone_1+'</td></tr>';
							});
							$("#customers_select_result").html(html);
						});
					}, 500);
				});
				$("#customers_select_result").on('click', 'td', function(event) {
					$('input[name="customers_name"]').val($(this).text());
					$('input[name="customers_id"]').val($(this).attr('data'));
					$("#customers_select_result").hide();
				});

				$('#inclusive_vat_checkbox').prop('checked', true);
//  Script for adding product through AJAX.
var render_data= {
	'item_info' : [],
	'all_basic_data':{},
	'imei_serial_data':[],
};

var add_to_cart = function(event,relevent_data) {
	
	var error_found = false;
	var dis_type;
	var indivi_dis_amt;

	if(relevent_data == null)
	{
		event.preventDefault();
		if($('input[name="item_spec_set_id"]').val()==null || $('input[name="item_spec_set_id"]').val()== "")
		{
			$('input[name="item_spec_set_id"]').closest('.form-group').addClass('has-error');
			$('input[name="item_spec_set_id"]').parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name="item_spec_set_id"]').closest('.form-group').removeClass('has-error');
			$('input[name="item_spec_set_id"]').parent().find('.help-block').hide();

		}
		if($('input[name=quantity]').val().trim()=="")
		{
			$('input[name=quantity]').closest('.form-group').addClass('has-error');
			$('input[name=quantity]').parent().parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name=quantity]').closest('.form-group').removeClass('has-error');
			$('input[name=quantity]').parent().parent().find('.help-block').hide();
		}
		if($('input[name=selling_price]').val().trim()=="")
		{
			$('input[name=selling_price]').closest('.form-group').addClass('has-error');
			$('input[name=selling_price]').parent().parent().find('.help-block').show();
			error_found=true;
		}
		else
		{
			$('input[name=selling_price]').closest('.form-group').removeClass('has-error');
			$('input[name=selling_price]').parent().parent().find('.help-block').hide();
		}
		dis_type = $('#dis_type').val();
		indivi_dis_amt= $('#dis_amt_id').val();
	}
	else
	{
		dis_type = relevent_data.discount_type;
		indivi_dis_amt = relevent_data.discount_amount;
		is_imei_used = relevent_data.imei_used;
	}

	if(dis_type != "" && indivi_dis_amt == ""){
		alert("<?php echo $this->lang->line('enter_dis_amt')?>");
		error_found =true;
	}
	if(dis_type == "" && indivi_dis_amt != ""){
		alert("<?php echo $this->lang->line('enter_dis_type')?>");
		error_found =true;
	}
	if(error_found)
	{
		return false;
	}
	var found = false;
	if(is_imei_used == "yes")
	{
		if(relevent_data == null)
		{
			render_data['item_info'].push({
				'sell_type'  : $('#sell_type_id').val(),
				'item_spec_set_id' : $ ('input[name="item_spec_set_id"]').val(),
				'item_name' : $('input[name="items_name"]').val(),
				'actual_quantity' : $('input[name="quantity"]').val(),
				'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),
				'sub_total' : $('#dis_type').val()=='free_quantity' ? Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)) : ($('#dis_type').val()=='amount' ? Number((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val()))-parseFloat($ ('input[name="discount_amount"]').val()).toFixed(2)) : Number(((parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()))-((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val())*parseFloat($ ('input[name="discount_amount"]').val()))/100)).toFixed(2))) || Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)),
				'selling_price' : $('input[name="selling_price"]').val(),
				'product_warranty' : product_warranty,
				'sell_comments' : $ ('input[name="sell_comments"]').val(),
				'discount_amount' : $ ('input[name="discount_amount"]').val(),
				'discount_type' : $('#dis_type').val(),
				'imei_used': is_imei_used,
				'temp_id_info': temp_item_id,
			});
		}
		else
		{
			render_data['item_info'].push({
				'sell_type'  : relevent_data.sell_type,
				'item_spec_set_id' : relevent_data.item_spec_set_id,
				'item_name' : relevent_data.item_name,
				'actual_quantity' : relevent_data.actual_quantity,
				'quantity' : relevent_data.quantity,
				'sub_total' : relevent_data.sub_total,
				'selling_price' : relevent_data.selling_price,
				'product_warranty' : relevent_data.product_warranty,
				'sell_comments' : relevent_data.sell_comments,
				'discount_amount' : relevent_data.discount_amount,
				'discount_type' : relevent_data.discount_type,
				'imei_used': relevent_data.imei_used,
				'temp_id_info': relevent_data.temp_id_info,
			});
		}
	}
	else{

		if(relevent_data == null)
		{
			$.each(render_data['item_info'], function(index, val) {
				if(val.item_spec_set_id == $('input[name="item_spec_set_id"]').val())
				{
					render_data['item_info'][index]['actual_quantity'] =parseFloat(render_data['item_info'][index]['actual_quantity']) + parseFloat($('input[name="quantity"]').val());
					render_data['item_info'][index]['quantity'] =parseFloat(render_data['item_info'][index]['quantity']) + parseFloat($('input[name="quantity"]').val());
					render_data['item_info'][index]['sub_total'] += parseFloat($('input[name="quantity"]').val()*$('input[name="selling_price"]').val());
					render_data['item_info'][index]['discount_amount'] = $('input[name="discount_amount"]').val();
					render_data['item_info'][index]['discount_type']= $('#dis_type').val();
					found = true;
				}
			});
			if(!found){
				render_data['item_info'].push({
					'sell_type'  : $('#sell_type_id').val(),
					'item_spec_set_id' : $ ('input[name="item_spec_set_id"]').val(),
					'item_name' : $('input[name="items_name"]').val(),
					'actual_quantity' : $('input[name="quantity"]').val(),
					'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),
					'sub_total' : $('#dis_type').val()=='free_quantity' ? Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)) : ($('#dis_type').val()=='amount' ? Number((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val()))-parseFloat($ ('input[name="discount_amount"]').val()).toFixed(2)) : Number(((parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()))-((parseFloat($('input[name="selling_price"]').val())*parseFloat($('input[name="quantity"]').val())*parseFloat($ ('input[name="discount_amount"]').val()))/100)).toFixed(2))) || Number(parseFloat($('input[name="selling_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)),
					'selling_price' : $('input[name="selling_price"]').val(),
					'product_warranty' : product_warranty,
					'sell_comments' : $ ('input[name="sell_comments"]').val(),
					'discount_amount' : $ ('input[name="discount_amount"]').val(),
					'discount_type' : $('#dis_type').val(),
					'imei_used': is_imei_used,
					'temp_id_info': temp_item_id,
				});
			}
		}
		else
		{
			render_data['item_info'].push({
				'sell_type'  : relevent_data.sell_type,
				'item_spec_set_id' : relevent_data.item_spec_set_id,
				'item_name' : relevent_data.item_name,
				'actual_quantity' : relevent_data.actual_quantity,
				'quantity' : relevent_data.quantity,
				'sub_total' : relevent_data.sub_total,
				'selling_price' : relevent_data.selling_price,
				'product_warranty' : relevent_data.product_warranty,
				'sell_comments' : relevent_data.sell_comments,
				'discount_amount' : relevent_data.discount_amount,
				'discount_type' : relevent_data.discount_type,
				'imei_used': relevent_data.imei_used,
				'temp_id_info': relevent_data.temp_id_info
			});
		}

	}
	sell_table_render();
	$('input[name="items_name"]').val('');
	$('input[name="item_spec_set_id"]').val('');
	$('input[name="quantity"]').val('');
	$('input[name="selling_price"]').val('');
	$('input[name="sell_comments"]').val('');
	$('input[name="current_quantity_inventory"]').val('');
	$('input[name="discount_amount"]').val('');
	$('#dis_type').val('');
	$('#add_item').prop("disabled",true);
	$('input[name=items_name]').focus();

}
var is_unique_barcode = "";

$(document).on('change', '#holdingInvoiceSelection', function(event) {
	clear_items_from_cart();
	var id = $(this).val();
	render_data.imei_serial_data = allHoldingInvoices[id].render_data.imei_serial_data;
	$.each(allHoldingInvoices[id].render_data.item_info, function(index, val) {
		add_to_cart(null,val);
	});
});

render_data['all_basic_data']['due_after_paid']=0;

$('.form-actions').on('click', '#add_item', add_to_cart);

$('.form-actions').on('click', '#cancle_item', function(event) {
	event.preventDefault();
	$('#add_item').show('fast');
	$('#update_item').hide();
	$('#current_qty').show('fast');
	$('#add_item').prop("disabled",true);
	$('#item_select_field').show('fast');
	$('#item_select').prop("disabled",false);
	$('#sell_type_id').prop("disabled",false);
	$('input[name="items_name"]').val('');
	$('input[name="item_spec_set_id"]').val('');
	$('input[name="quantity"]').val('');
	$('input[name="current_quantity_inventory"]').val('');
	$('input[name="selling_price"]').val('');
	$('input[name="sell_comments"]').val('');
	$('input[name="discount_amount"]').val('');
	$('#dis_type').val('');
});
var current_active_vat_percentage =0;
var total_amount = 0;
var total_net_payable= 0;
var net_pay_after_discount = 0;
$(document).on('change', '.table_quantity', function(event) {
	event.preventDefault();
	var new_quantity = $(this).val();
	var row_number = $(this).attr('name').split('_')[1];
	var current_item_spec_id = render_data['item_info'][row_number]['item_spec_set_id'];
	$.get('<?php echo site_url() ?>/advance_sell/quantity_by_item_from_inventory/'+current_item_spec_id, function(data) {
		var curr_item_quantity = $.parseJSON(data);
		if( parseFloat(new_quantity) > parseFloat(curr_item_quantity)){
			alert ("<?php echo $this->lang->line('alert_not_enough_quantity');?>");
			$('input[name=tablequantity_'+row_number+']').val(render_data['item_info'][row_number]['quantity']);
		}
		else {
			render_data['item_info'][row_number]['actual_quantity'] = new_quantity;
			render_data['item_info'][row_number]['quantity'] = new_quantity;
			render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['selling_price']*new_quantity;
			sell_table_render();
			// $('input[name=items_name]').focus();
		}
	});
});
$(document).on('change', '.table_price', function(event) {
	event.preventDefault();
	var new_price = $(this).val();
	var row_number = $(this).attr('name').split('_')[1];
	render_data['item_info'][row_number]['selling_price'] = new_price;
	render_data['item_info'][row_number]['sub_total'] = render_data['item_info'][row_number]['quantity']*new_price;
	sell_table_render();
	// $('input[name=items_name]').focus();
});
function sell_table_render () {
	var sell_table_html = '';
	total_amount = 0;
	var subtotal_amount = 0;
	$('#total_paid').val("");
	$('#total_discount').val('');
	$.each(render_data['item_info'], function(index, val) {
		sell_table_html += '<tr class="sell_item_row_table" id="'+index+'">';
		sell_table_html += '<td>'+val.item_name+'</td>';
		if(val.imei_used == "yes"){
			if(val.discount_type =="" || val.discount_type =="amount" || val.discount_type =="percentage" ){
				sell_table_html += '<td><input type="number" min="1" readonly="true" step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+val.quantity+'"></td>';
			}
			else if(val.discount_type =="free_quantity"){
			// var total_quantity_with_free = parseFloat(val.quantity)+parseFloat(val.discount_amount);
			var total_quantity_with_free = parseInt(val.actual_quantity);
			sell_table_html += '<td><input type="number" min="1" readonly="true" step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+total_quantity_with_free+'"></td>';
			// sell_table_html += '<td>'+total_quantity_with_free+'</td>';
			// render_data['item_info']['quantity']= total_quantity_with_free;
		}
	}
	else{
		if(val.discount_type =="" || val.discount_type =="amount" || val.discount_type =="percentage" ){
			sell_table_html += '<td><input type="number" min="1"  step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+val.quantity+'"></td>';
		}
		else if(val.discount_type =="free_quantity"){
			// var total_quantity_with_free = parseFloat(val.quantity)+parseFloat(val.discount_amount);
			var total_quantity_with_free = parseInt(val.actual_quantity);
			sell_table_html += '<td><input type="number" min="1"  step="any" class="form-control table_quantity" name="tablequantity_'+index+'" value="'+total_quantity_with_free+'"></td>';
			// sell_table_html += '<td>'+total_quantity_with_free+'</td>';
			// render_data['item_info']['quantity']= total_quantity_with_free;
		}
	}
	sell_table_html += '<td><input type="number" min="0.1" class="form-control table_price" name="tableprice_'+index+'" value="'+Number(val.selling_price).toFixed(2)+'"></td>';
	if (val.discount_type =="") {
		sell_table_html += '<td>'+Number(val.actual_quantity*val.selling_price).toFixed(2)+'</td>';
		subtotal_amount+= val.actual_quantity*val.selling_price;
		Number(subtotal_amount.toFixed(2));
	}
	else if(val.discount_type =="amount"){
		var total= val.quantity*val.selling_price;
		total -= val.discount_amount;
		sell_table_html += '<td>'+Number(total).toFixed(2)+'</td>';
		subtotal_amount+= total;
		Number(subtotal_amount.toFixed(2));    
	}
	else if (val.discount_type  =="percentage"){
		var total_sub_total= val.quantity*val.selling_price;
		var total =total_sub_total*val.discount_amount;
		total /=100;
		total_sub_total-=total;
		sell_table_html += '<td>'+Number(total_sub_total).toFixed(2)+'</td>';
		subtotal_amount+= total_sub_total;
		Number(subtotal_amount.toFixed(2));
	}
	else if(val.discount_type =="free_quantity"){
		sell_table_html += '<td>'+Number(val.actual_quantity*val.selling_price).toFixed(2)+'</td>';
		subtotal_amount+= val.actual_quantity*val.selling_price;
		Number(subtotal_amount.toFixed(2));
	}
	sell_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
	sell_table_html += '<td><a id="cart_item_info" class="glyphicon glyphicon-info-sign" style="color:blue;"></a><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red; margin-left:10px"></a>';
	sell_table_html += '</td>';
	sell_table_html += '</tr>';
	total_amount = subtotal_amount;
	Number(total_amount.toFixed(2));
});
	if($('#inclusive_vat_checkbox').is(":checked")){
		render_data['all_basic_data']['net_payable']=Number(this.total_amount.toFixed(2));;
		$('#sell_table tbody').html(sell_table_html);
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		var current_vat_percent = Number($('#total_vat').val());
		var vat_on_sell = parseFloat(total_amount) * parseFloat(current_vat_percent)||0 ;
		vat_on_sell /=100;
		final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
		$('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));    
		$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
		$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= current_vat_percent;
	}
	else{
		render_data['all_basic_data']['net_payable']=Number(this.total_amount.toFixed(2));
		$('#sell_table tbody').html(sell_table_html);
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		$('#total_amount_with_vat').text(Number(total_amount.toFixed(2)));    
		$('#total_net_payable').text(Number(total_amount.toFixed(2)));
		$('#total_due').text(Number(total_amount.toFixed(2)));
		render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= 0;
	}
}
render_data['all_basic_data']['discount_by_percentage']='yes';
$('#discount_percentage').prop('checked', true);
/* Discount with percentage before final submission starts*/
$(document).on('change', '#discount_percentage', function(event) {
	/* Act on the event */
	if($('#discount_percentage').is(":checked")){
		var discount_percent = $('#total_discount').val()||0;
		var discount_calculation = (parseFloat($('#total_amount_with_vat').text()) * discount_percent)/100 ;
		var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_calculation)||0;
		$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
		$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
		if($('#total_paid').val() !=""){
			render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		}
		$('#total_change').text(0);
		render_data['all_basic_data']['discount']=parseFloat(discount_percent)||0;
		render_data['all_basic_data']['discount_by_percentage']='yes';
		render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
		render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
		$('#total_paid').val('');
	}
	else{
		var discount_amt = $('#total_discount').val()||0; 
		if (discount_amt > Number($('#total_amount_with_vat').text()) ) {
			alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
			$('#total_discount').val(''); 
			$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
			$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		}
		var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_amt)||0;
		$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
		$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
		if($('#total_paid').val() !=""){
			render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		}
		$('#total_change').text(0);
		render_data['all_basic_data']['discount']=parseFloat(discount_amt)||0;
		render_data['all_basic_data']['discount_by_percentage']='no';
		render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
		render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
		$('#total_paid').val('');
	}
});
/* Discount percentage ends*/
$(document).on('change', '#inclusive_vat_checkbox', function(event) {
	if ($('#inclusive_vat_checkbox').is(":checked")) {       
		render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));;
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		// if($('#total_paid').val() !=""){
		// 	render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		// }
		var current_vat_percent = Number($('#total_vat').val());
		var vat_on_sell = parseFloat(total_amount) * parseFloat(current_vat_percent)||0 ;
		vat_on_sell /=100;
		final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
		$('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));    
		$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
		$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
		// render_data['all_basic_data']['due']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= current_vat_percent;
		render_data['all_basic_data']['due_after_paid']= Number($('#total_due').text());
		$('#total_paid').val('');
	}
	else{
		render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));;
		// if($('#total_paid').val() !=""){
		// 	render_data['all_basic_data']['paid']= Number($('#total_paid').val());
		// }
		// $('#sell_table tbody').html(sell_table_html);
		$('#total_amount').html(Number(total_amount.toFixed(2)));
		$('#total_amount_with_vat').text(Number(total_amount.toFixed(2)));    
		$('#total_net_payable').text(Number(total_amount.toFixed(2)));
		$('#total_due').text(Number(total_amount.toFixed(2)));
		render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['tax_percentage']= 0;
		render_data['all_basic_data']['due_after_paid']= Number($('#total_due').text());
		$('#total_paid').val('');
	}
});
$('#sell_table').on('click', '.item_edit', function () {
	var clicked_id = $(this).closest('tr').attr('id');
	var editing_item_info;
	editing_item_info = render_data['item_info'][clicked_id];
	$('select[name=sell_type]').val(editing_item_info.sell_type);
	$('select[name=discount_type]').val(editing_item_info.discount_type);
	$('#item_select_field').hide('fast');
	$('#sell_type_id').prop("disabled",true);
	$('#item_select').prop("disabled",true);
	$('#current_qty').hide('fast');
	$('input[name="item_spec_set_id"]').val(editing_item_info.item_spec_set_id),
	$('input[name="items_name"]').val(editing_item_info.item_name),
	$('input[name="quantity"]').val(editing_item_info.actual_quantity);
	$('input[name="sell_comments"]').val(editing_item_info.sell_comments);
	$('input[name="discount_amount"]').val(editing_item_info.discount_amount);
	$('input[name="discount_type"]').val(editing_item_info.discount_type);
	$('input[name="selling_price"]').val(editing_item_info.selling_price);
	$('#add_item').hide();
	$('#update_item').show('slow');
	$('#update_item').attr('selected-id', clicked_id);
});
$('.form-actions').on('click', '#update_item', function(event) {
	event.preventDefault();
	var change_item_index_array = $(this).attr('selected-id');
	// render_data.item_info[change_item_index_array].item_spec_set_id    = $('#sell_type_id').val();
	render_data.item_info[change_item_index_array].sell_type    = $('#sell_type_id').val();
	// render_data.item_info[change_item_index_array].item_spec_set_id = $('#item_select').val();
	// render_data.item_info[change_item_index_array].item_name = $('#item_select option:selected').text();
	render_data.item_info[change_item_index_array].item_spec_set_id = $('input[name="item_spec_set_id"]').val();
	render_data.item_info[change_item_index_array].item_name = $('input[name="items_name"]').val();

	render_data.item_info[change_item_index_array].quantity = ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val();
	render_data.item_info[change_item_index_array].actual_quantity = $('input[name="quantity"]').val();

	render_data.item_info[change_item_index_array].comments = $('input[name="comments"]').val();
	render_data.item_info[change_item_index_array].discount_type = $('#dis_type').val();
	render_data.item_info[change_item_index_array].selling_price = $('input[name="selling_price"]').val();
	render_data.item_info[change_item_index_array].discount_amount = $('input[name="discount_amount"]').val();
	sell_table_render();
	$('#add_item').show('fast');
	$('#update_item').hide();
	// $('#current_quantity').show('fast');
	$('#current_qty').show('fast');
	$('#add_item').prop("disabled",true);
	$('#item_select_field').show('fast');
	$('#item_select').prop("disabled",false);
	$('#sell_type_id').prop("disabled",false);
	// $('#sell_type_id').val('');
	// $('#item_select').selectpicker('val','');
	$('input[name="items_name"]').val('');
	$('input[name="item_spec_set_id"]').val('');
	$('input[name="quantity"]').val('');
	$('input[name="current_quantity_inventory"]').val('');
	$('input[name="selling_price"]').val('');
	$('input[name="sell_comments"]').val('');
	$('input[name="discount_amount"]').val('');
	$('#dis_type').val('');
	// $('#extra_fields').show();
});
var final_amount_with_vat;
// $( "#sell_table" ).on( "keyup", "#total_vat", function(e) {
	// 	$(this).removeClass('keyup_not_triggered');
	// 	var vat_on_sell = parseFloat(total_amount) * parseFloat($(this).val())||0 ;
	// 	vat_on_sell /=100;
	// 	final_amount_with_vat = parseFloat(total_amount) + parseFloat(vat_on_sell);
	// 	$('#total_amount_with_vat').text(Number(final_amount_with_vat.toFixed(2)));	
	// 	$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
	// 	$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
	// 	render_data['all_basic_data']['net_pay_with_vat']=Number(final_amount_with_vat.toFixed(2));
	// 	render_data['all_basic_data']['net_pay_after_discount']=Number(final_amount_with_vat.toFixed(2));
	// });
	$.get('<?php echo site_url() ?>/advance_sell/get_current_vat_percentage/', function(data) {
		var vat_data = $.parseJSON(data);
		$('#total_vat').val(0);
		current_active_vat_percentage = 0;
	});
	$('input[name="discount"]').val('');
	$('#total_discount').keyup(function(event) {
		if($('#discount_percentage').is(":checked")){
			var discount_percent = $('#total_discount').val()||0;
			if( discount_percent >100){
				alert("Discount not available more than 100");
				$('#total_discount').val('');
				$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
				$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
			}
			else{
				var discount_calculation = (parseFloat($('#total_amount_with_vat').text()) * discount_percent)/100 ;
				var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(discount_calculation)||0;
				$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
				$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
				if($('#total_paid').val() !=""){
					render_data['all_basic_data']['paid']= Number($('#total_paid').val());
				}
				$('#total_change').text(0);
				render_data['all_basic_data']['discount']=parseFloat(discount_percent)||0;
				render_data['all_basic_data']['discount_by_percentage']='yes';
				render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
				render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
				$('#total_paid').val('');
			}
		}
		else{
			var dis_amt = $('#total_discount').val();
			if (dis_amt > Number($('#total_amount_with_vat').text()) ) {
				alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
				$('#total_discount').val('');
				$('#total_net_payable').text(Number(final_amount_with_vat.toFixed(2)));
				$('#total_due').text(Number(final_amount_with_vat.toFixed(2)));
			}
			var net_pay_after_discount = parseFloat($('#total_amount_with_vat').text()) - parseFloat(dis_amt)||0;
			$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
			$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
			if($('#total_paid').val() !=""){
				render_data['all_basic_data']['paid']= Number($('#total_paid').val());
			}
			$('#total_change').text(0);
			render_data['all_basic_data']['discount']=parseFloat($(this).val())||0;
			render_data['all_basic_data']['discount_by_percentage']='no';
			render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
			render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
			$('#total_paid').val('');
		}
	});
// $('#total_received').keyup(function(event) {

	// 	if (event.keyCode == '9') {
		// 		return false;
		// 	};
		// 	var total_change_after_receive = parseFloat($( this ).val()) - parseFloat($('#total_net_payable').text())||0;
		// 	$('#total_change').text(Number(total_change_after_receive.toFixed(2)));
		// 	render_data['all_basic_data']['received_money']=parseFloat($(this).val());
		// 	render_data['all_basic_data']['return_change']=Number(total_change_after_receive.toFixed(2));
		// });
		$('#total_paid').keyup(function(event) {
			if (event.keyCode == '9') {
				return false;
			};
			if($("#discount_percentage").is(":checked") == true){
				render_data['all_basic_data']['discount_by_percentage']='yes';
			}
			if($("#discount_percentage").is(":checked") == false){
				render_data['all_basic_data']['discount_by_percentage']='no';
			}
			var total_change_after_receive = parseFloat($( this ).val()) - parseFloat($('#total_net_payable').text())||0;
			if (total_change_after_receive <=0) {
				$('#total_change').text(0);
				render_data['all_basic_data']['return_change']=0;
			} else {

				$('#total_change').text(Number(total_change_after_receive.toFixed(2)));
				render_data['all_basic_data']['return_change']=Number(total_change_after_receive.toFixed(2));
			}
			render_data['all_basic_data']['received_money']=parseFloat($(this).val());
			var due_after_paid = parseFloat($('#total_net_payable').text()) - parseFloat($(this).val())||0;
			if (due_after_paid < 0) {
				$('#total_due').text(0);
				render_data['all_basic_data']['due_after_paid']=0;
			} else {

				$('#total_due').text(Number(due_after_paid.toFixed(2)));
				render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));
			}
	// if($('#total_vat').hasClass('keyup_not_triggered')){
		//     render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
		//     // render_data['all_basic_data']['net_pay_after_discount']=total_amount;        commented this after solving discount and net_pay_after_discount problem 
		//     if ($('#total_discount').val() == '') {
			//         render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
			//     }
			// }
			// if($('#total_discount').hasClass('keyup_not_triggered')){
				//     if($('#total_discount').val() == ''){
					//         render_data['all_basic_data']['net_pay_with_vat']=Number(total_amount.toFixed(2));
					//     }
					//     if ($('#total_discount').val() == '') {
						//         render_data['all_basic_data']['net_pay_after_discount']=Number(total_amount.toFixed(2));
						//     }
						// }
						render_data['all_basic_data']['paid']=parseFloat($(this).val());
					});
		$('#sell_table').on('click', '#item_remove', function () {
			var clicked_id = $(this).closest('tr').attr('id');
			var temp_id_remove = render_data['item_info'][clicked_id].temp_id_info;
			$.each(render_data['imei_serial_data'], function(index, val) {
				if(val.temp_id == temp_id_remove){
					render_data['imei_serial_data'].splice(index,1);
					return false;
				}
			});
			render_data['item_info'].splice(clicked_id,1);
			sell_table_render();
		});
		$('#sell_table').on('click', '#cart_item_info', function(event) {
			var clicked_id = $(this).closest('tr').attr('id');
			var cart_item_id = render_data['item_info'][clicked_id].item_spec_set_id;
			$.get('<?php echo site_url() ?>/advance_sell/quantity_by_item_from_inventory/'+cart_item_id, function(data) {
				var cart_item_available_qty = $.parseJSON(data);
				console.log(cart_item_available_qty);
				var all_imei = "";
				$.each(cart_item_available_qty.available_imei, function(index, val) {
					all_imei+=val.imei_barcode+"\n";
				});
				alert("Available Quantity :  "+cart_item_available_qty.final_quantiy+"\n"+ "Buying Price: "+cart_item_available_qty.buying_price+"\n"+ "Average Buying Price: "+cart_item_available_qty.avg_buy_price+"\n"+"IMEI/Serial: "+"\n"+(all_imei)
					);
			});
		});
		$(document).on('click', '#save_sell', function(event) {
			event.preventDefault();
			$('#save_sell').prop("disabled",true);
			render_data.all_basic_data.customers_id = $('input[name="customers_id"]').val();
			render_data.all_basic_data.sales_rep_id = $('input[name="sales_rep_id"]').val();
			render_data.all_basic_data.custom_voucher_no = $('input[name="voucher_no"]').val();
			render_data.all_basic_data.discount = $('input[name="discount"]').val()|| 0;
			render_data.all_basic_data.received_money = $('input[name="paid"]').val();
			render_data.all_basic_data.tax_percentage = $('input[name="tax_percentage"]').val() || 0;
			render_data.all_basic_data.payment_type = $('input[name="payment_type"]:checked').val();
			render_data.all_basic_data.card_amount = $('input[name="card_amount"]').val();
			render_data.all_basic_data.card_payment_voucher_no = $('input[name="card_payment_voucher_no"]').val();
			render_data.all_basic_data.paid = $('input[name="paid"]').val();
			render_data.all_basic_data.cheque_number = $('input[name="cheque_number"]').val();
			render_data.all_basic_data.bank_acc_id = $('input[name=bank_acc_id]').val();
			render_data.all_basic_data.delivery_date = $('input[name=delivery_date]').val();
			render_data.all_basic_data.card_charge_percent= $('input[name=bank_charge_percent]').val() || 0;
			render_data.all_basic_data.custom_voucher_no = $('input[name=voucher_no]').val() || 0;
			var all_data= {
				'csrf_test_name' :$('input[name=csrf_test_name]').val(),
				'all_data' : render_data,
			}
			$.post('<?php echo site_url() ?>/advance_sell/save_sell_info',all_data ).done(function(data, textStatus, xhr) {
				var result = $.parseJSON(data);
				var sell_id = result[0].split(' ')[1];
				console.log(result);
				remove_all_errors();
				if (result[0].split(' ')[0].search("success")!=-1)
				{
					var invoice_header = '';
					invoice_header+= '<div class="row invoice"><div class="col-xs-12" style="text-align: center;"><div>';
					<?php 
					if(!empty($store_details['shop_image']))
					{
						?>
						invoice_header+= '<img style="height: 20%; width: 20%" src="<?php echo base_url();?><?php echo $store_details['shop_image']?>" /></div><div>';
						<?php
					}
					?>
					invoice_header+= '<h3 style="text-align: center;"><?php echo $store_details['name']?></h3>';
					invoice_header+= '<ul class="list-styled" style="list-style:none">';
					invoice_header+= '<li>';
					invoice_header+= '<?php echo $store_details['address']?>';
					invoice_header+= '</li>';
					invoice_header+= '<li>';
					invoice_header+= '<?php echo $store_details['phone']?>';
					invoice_header+= '</li>';
					invoice_header+= '<li>';
					invoice_header+= '<?php echo $store_details['website']?>';
					invoice_header+= '</li>';
					invoice_header+= '<li>';
					invoice_header+= '</li>';
					invoice_header+= '</ul></div></div></div>';
					if (!render_data.all_basic_data.customers_id) 
					{
						invoice_header+= '<span>Voucher No : '+result[1].voucher_number.sell_local_voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a") ?></p><p>Customer Name : </p><p>Customer Address : </p><p>Delivery Date: '+render_data.all_basic_data.delivery_date+'</p></div>';
					}
					else
					{
						invoice_header+= '<span>Voucher No:  '+result[1].voucher_number.sell_local_voucher_no+'</span><p style="float:right">Date : <?php echo date("F j, Y, g:i a")?></p><p>Customer Name : '+result[1].customer_info.customers_name+', Phone Number: '+result[1].customer_info.customers_phone_1+'</p><span>Customer Address : '+result[1].customer_info.customers_present_address+'</span><p style="float:right">Delivery Date: '+render_data.all_basic_data.delivery_date+'</p></div>';
					}
					invoice_header+= '</div></div><div class="row"><div class="col-md-12">';
					invoice_header+= '<table class="table table-striped table-bordered table-hover">';
					invoice_header+= '<thead><tr><th>SL.</th><th>Item</th><th>Imei</th><th>Remarks</th><th class="hidden-480">Discount</th> <th class="hidden-480">Qty</th><th class="hidden-480"> Unit Cost</th><th>Total Price</th></tr></thead><tbody>';



					var i=1;
					$.each(render_data['item_info'],function(index, val)
					{
						invoice_header+= ' <tr><td>'+i+' </td>';
						invoice_header+= '<td>'+val.item_name+'</td>';


						if(val.temp_id_info!="")
						{
							$.each(render_data['imei_serial_data'],function(index, is_imei) 
							{
								if(val.temp_id_info===is_imei.temp_id)
								{
									invoice_header+= '<td>'+is_imei.imei_barcode+'</td>';
								}
							});
						}
						else
						{
							invoice_header+= '<td></td>';
						}


						if(val.product_warranty != ""){
							invoice_header+= '<td>'+val.product_warranty+' Months Warranty'+'</td>';
						}
						else{
							invoice_header+= '<td>'+'--'+'</td>';
						}
						if(val.discount_type == "percentage"){
							invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' %'+ '</td>';
						}
						if(val.discount_type == "amount"){
							invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' Tk'+ '</td>';
						}
						if(val.discount_type == "free_quantity"){
							invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+' pcs'+ '</td>';
						}
						if(val.discount_type == ""){
							invoice_header+= '<td class="hidden-480">'+(val.discount_amount)+ '</td>';
						}
						invoice_header+= '<td class="hidden-480">'+val.quantity+'</td>';
						invoice_header+= '<td class="hidden-480">'+val.selling_price+'</td>';
						invoice_header+= '<td>'+val.sub_total+'</td></tr>';
						i++;
					});

					invoice_header+= '</tbody></table></div></div>';
					invoice_header+='<div class="lasttablebg">';
					invoice_header+='<table class="table table-striped table-bordered table-hover"><tbody>';
					invoice_header+='<tr>';
					invoice_header+='<td>Sub-Total</td>';
					invoice_header+='<td>'+result[1].voucher_number.sub_total+'</td>';
					invoice_header+='</tr>';
					invoice_header+='<tr>';
					invoice_header+='<td>VAT ('+result[1].voucher_number.tax_percentage+'%)</td>';
					invoice_header+='<td>'+result[1].voucher_number.vat_amount+'</td>';
					invoice_header+='</tr>';
					if(result[1].voucher_number.discount_percentage == "no"){
						invoice_header+='<tr>';
						invoice_header+='<td>Discount</td>';
						invoice_header+='<td>'+result[1].voucher_number.discount+'</td>';
						invoice_header+='</tr>';
					}
					else{
						var discount_amt= ((result[1].voucher_number.grand_total)-(result[1].voucher_number.net_payable)).toFixed(2);
						invoice_header+='<tr>';
						invoice_header+='<td>Discount ('+result[1].voucher_number.discount+'%)</td>';
						invoice_header+='<td>'+discount_amt +'</td>';
						invoice_header+='</tr>';
					}
					invoice_header+='<tr>';
					invoice_header+='<td>Grand Total</td>';
					invoice_header+='<td>'+result[1].voucher_number.net_payable+'</td>';
					invoice_header+='</tr>';
					invoice_header+='<tr>';
					invoice_header+='<td>Paid</td>';
					invoice_header+='<td>'+result[1].voucher_number.paid+'</td>';
					invoice_header+='</tr>';
					invoice_header+='<tr>';
					invoice_header+='<td>Due</td>';
					invoice_header+='<td>'+result[1].voucher_number.due+'</td>';
					invoice_header+='</tr>';
					invoice_header+='</tbody></table></div>';
					invoice_header+= '<div class="row signaturebg"><p> Signature </p> </div><p style="color:green"><?php echo $store_details['policy_plan']?></p><p style="text-align: center"> Thank you for shopping with us</p>';
					invoice_header+= '<div id="bcTarget" style="clear:both; width: 100%; background-color: #FFFFFF; color: #000000; text-align: center; font-size: 10px; margin-top: 5px;"></div';
					$("#invoice").html(invoice_header);
// $("#voucher_preview").show();exit;

var barcode1=result[1].voucher_number.voucher_unique_barcode;
$("#bcTarget").barcode(barcode1, "code128",{barWidth:1, barHeight:44});
$('#discount_percentage').prop('checked', true);
$('#discount_percentage').parent().addClass('checked');
$('input[name="sales_rep_name"]').val('');
$('input[name="customers_name"]').val('');
$('input[name=delivery_date]').val('');
$('input[name="sales_rep_id"]').val('');
$('input[name="customers_id"]').val('');
$('.sell_item_row_table').remove();
render_data['item_info'] = [];
render_data['imei_serial_data'] = [];
render_data['all_basic_data'] = {};
$('input[name="tax_percentage"]').val('');
$('input[name="discount"]').val('');
$('input[name="received_money"]').val('');
$('input[name="paid"]').val('');
$('input[name="card_payment_voucher_no"]').val('');
$('input[name="card_amount"]').val('');
$('input[name="voucher_no"]').val('');
$('#chk_num').val('');
$('#total_amount').html('');
$('#total_amount_with_vat').html('');
$('#total_net_payable').html('');
$('#total_change').html('');
$('#total_due').html('');
$('input[name="customers_id"]').val('');
$('input[name="sales_rep_id"]').val('');
$('#save_sell').prop("disabled",false);
$('input[name=items_name]').focus();
$('input[name="bank_acc_num"]').val('');
$('input[name="bank_acc_id"]').val('');
$('input[name=bank_charge_percent]').val('');
// $('input[name=delivery_date]').val('');
$('#sell_success').slideDown();
setTimeout( function(){$('#sell_success').slideUp()}, 5000 );  
$('#total_vat').val(current_active_vat_percentage);
if(!$('#inclusive_vat_checkbox').is(":checked")){
	$('#inclusive_vat_checkbox').prop('checked', true);
	$('#inclusive_vat_checkbox').parent().addClass('checked');
}
$("#options_cash").attr('checked', 'checked').click();
$('#card_voucher_no').hide('fast');
$('#ac_select_div').hide('fast');
$('#card_amount_for_both').hide('fast');
$('#cheque_num').hide('fast'); 

if($('#holdingInvoiceSelection').val() != '')
{
	allHoldingInvoices.splice($('#holdingInvoiceSelection').val(),1);
	localStorage.setItem('hodldingInvoices',JSON.stringify(allHoldingInvoices));
	setHoldingInvoices();						
}

var selected_printer_type = "<?php echo $this->session->userdata('printer_type')?>";
if(selected_printer_type == "pos")
{
	$('#print_now').attr('href','#save_sell_info@'+result[0].split(' ')[1]);
	$('#print_now')[0].click();
	$('#save_sell').prop("disabled",false);
}
else if(selected_printer_type == "normal")
{
	$("#holdingInvoiceSelection").hide();
	$("#voucher_preview").show();
	setTimeout(function(){ window.print(); $("#voucher_preview").hide();}, 500);

}
} 
else {
	$('#save_sell').prop("disabled",false); 
	var error_array = [];
	$.each(result, function(index, val) {
		error_array.push(val);
		if(val == "Total Paid Amount is Empty or Not Numeric"){
			$('table tfoot tr').eq(5).css("color","red").addClass('has-error');
			$('#total_paid').siblings().show().text("<?php echo $this->lang->line('paid_not_numeric'); ?>");
		}
		if(val == "Discount Amount Is Not Valid"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).text("<?php echo $this->lang->line('dis_not_numeric'); ?>");
		}
		if(val == "discount cannot be greater than 100 percentage"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).text("<?php echo $this->lang->line('more_than_100_per'); ?>");
		}
		if(val == "Discount Amount Is Not Numeric"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).text("<?php echo $this->lang->line('dis_not_numeric'); ?>");
		}
		if(val == "Paid Amount Is Not Numeric"){
			$('table tfoot tr').eq(5).css("color","red").addClass('has-error');
			$('#total_paid').siblings().show().text("<?php echo $this->lang->line('paid_not_numeric'); ?>")
		}
		if(val == "Card Voucher Is Empty For Both Type Payment"){
			$('#card_pay_voucher_id').closest('.form-group').addClass('has-error');
			$('#card_pay_voucher_id').siblings().text("<?php echo $this->lang->line('card_voucher_not_numeric')?>");
		}
		if(val == "Card Amount Is Empty For Both Type Payment"){
			$('#card_amount_id').closest('.form-group').addClass('has-error');
			$('#card_amount_id').siblings().text("<?php echo $this->lang->line('enter_valid_amount_both_type_payment')?>");
		}
		if(val == "Card Vocuher Is Not Numeric"){
			$('#card_pay_voucher_id').closest('.form-group').addClass('has-error');
			$('#card_pay_voucher_id').siblings().text("<?php echo $this->lang->line('card_voucher_not_numeric')?>");
		}
		if(val == "Please Select Card Option"){
		}
		if(val == "Card Amount Is More Than Total Amount"){
			$('#card_amount_id').closest('.form-group').addClass('has-error');
			$('#card_amount_id').siblings().text("<?php echo $this->lang->line('card_amount_more_than_total')?>");
		}
		if(val == "Card Voucher Is Empty"){
			$('#card_pay_voucher_id').closest('.form-group').addClass('has-error');
			$('#card_pay_voucher_id').siblings().text("<?php echo $this->lang->line('card_voucher_is_empty')?>");
		}
		if(val == "Cheque Number Empty"){
			$('#chk_num').closest('.form-group').addClass('has-error');
			$('#chk_num').siblings().text("<?php echo $this->lang->line('chk_num_empty')?>");
		}
		if(val == "Total Amount Can Not Less Than Card Amount"){
			$('#save_sell').prop("disabled",false);
		}
		if(val == "Due And Paid Zero But Net Payable Not Zero"){
			$('#save_sell').prop("disabled",false);
		}
		if(val == "Cheque Number Already Exist"){
			$('#chk_num').closest('.form-group').addClass('has-error');
			$('#chk_num').siblings().text("<?php echo $this->lang->line('chk_num_already_exist')?>");
		}
		if(val == "Select Customer for Due Transaction"){
			$('#customers_select').closest('.form-group').addClass('has-error');
			$('#customer_help_block').text("<?php echo $this->lang->line('select_customer_for_advance_sell')?>");
		}
		if(val == "Select Delivery Date"){
			$('#delivery_date_select').closest('.form-group').addClass('has-error');
			$('#delivery_help_block').text("<?php echo $this->lang->line('select_delivery_date')?>");
		}
		if(val == "Total Calculation mismatch"){
			$('table tfoot tr').eq(5).css("color","red").addClass('has-error');
			$('#total_paid').siblings().show().text("<?php echo $this->lang->line('total_calculation_mismatch'); ?>");
		}
		if(val == "Total Calculation mismatch after discount"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).text("total_calculation_mismatch_after_discount");
		}
		if(val == "Total Calculation mismatch in discount field"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).text("total_calculation_mismatch_only_in_discount");

		}
		if(val == "Calculation Mismatch"){
			$('table tfoot tr').eq(5).css("color","red").addClass('has-error');
			$('#total_paid').siblings().show().text("<?php echo $this->lang->line('total_calculation_mismatch'); ?>")
		}
		if(val == "Discount is greater than grand total"){
			$('table tfoot tr').eq(3).css("color","red").addClass('has-error');
			$('#total_discount').siblings().eq(0).show().text("discount_is_greater_than_grand_total");
		}
		if (val == "Bank Acc Not Select") {
			$('#bank_acc_select').closest('.form-group').addClass('has-error');
			$('#bank_acc_help_block').text("<?php echo $this->lang->line('bank_acc_not_select')?>");
		}
	});

$.each(error_array, function(index, val) {
	alert(val);
	$('#save_sell').prop("disabled",false); 
});


}
}).error(function() {
	alert("<?php echo $this->lang->line('alert_failed_msg');?>");
	$('#save_sell').prop("disabled",false);
});
});
function remove_all_errors() {
	// $('#right_box').siblings('#error_messages').slideUp(1);
	$('#right_box').find('.has-error').removeClass('has-error');
	$('table tfoot tr').eq(5).css("color","black").removeClass('has-error');
	$('table tfoot tr').eq(3).css("color","black").removeClass('has-error');
	$('#total_paid').siblings().show().text("");
	$('#customer_help_block').text("");
	$('#total_discount').siblings().eq(0).text("");
	$('#card_amount_id').closest('.form-group').removeClass('has-error');
	$('#card_amount_id').siblings().text("");
	$('#delivery_date_select').closest('.form-group').removeClass('has-error');
	$('#delivery_help_block').text("");
	$('#card_pay_voucher_id').closest('.form-group').removeClass('has-error');
	$('#card_pay_voucher_id').siblings().text("");
	$('#chk_num').closest('.form-group').removeClass('has-error');
	$('#chk_num').siblings().text("");
	$('#bank_acc_select').siblings().text('');
}
var product_warranty = "";
$('#item_select_result').on('click', 'td', function(event) {
	event.preventDefault();
	$('input[name="items_name"]').val($(this).text());
	$('input[name="item_spec_set_id"]').val($(this).attr('data'));
	is_unique_barcode= $(this).attr('is_unique');
	// if(is_unique_barcode == "yes" && is_imei_used== "no"){
	// 	alert("Use IMEI/ Serial for selling the item");
	// 	$("#item_select_result").hide();
	// 	$('input[name="items_name"]').val('');
	// 	$('input[name="item_spec_set_id"]').val('');
	// 	$('input[name="quantity"]').val('');
	// 	$('input[name="current_quantity_inventory"]').val('');
	// 	$('input[name="selling_price"]').val('');
	// 	$('input[name="sell_comments"]').val('');
	// 	$('input[name="discount_amount"]').val('');
	// 	$('#dis_type').val('');
	// 	$('input[name=items_name]').focus();
	// }
	// else{
		$("#item_select_result").hide();
		var sell_type_id = $('#sell_type_id').val();
		var item_select_val = $('input[name="item_spec_set_id"]').val();
		$.get('<?php echo site_url() ?>/advance_sell/get_item_price_info/'+item_select_val, function(data) {
			var data_get = $.parseJSON(data);
			$('#current_quantity').val(99999);
			// product_warranty = data_get[0].product_warranty;
		// $('#numpadButton1').attr("placeholder", "Type your answer here");
		if (sell_type_id == 'wholesale') {
			$('.selling_price_and_numberpad').val(0);
		}
		else if(sell_type_id == 'retail'){
			$('.selling_price_and_numberpad').val(0);
		};
		$('#add_item').click();
		//is_imei_used="";
		temp_item_id="";
	});
	// }
});
$('#add_item').prop("disabled",true);
$(document).on('keyup', '#numpadButton1', function(event) {
	event.preventDefault();
	var input_quantity = $(this).val();
	var item_select_val = $('input[name="item_spec_set_id"]').val();
	$.get('<?php echo site_url() ?>/advance_sell/quantity_by_item_from_inventory/'+item_select_val, function(data) {
		var data_quantity = $.parseJSON(data);
		$('#update_item').prop("disabled",false);
		$('#add_item').prop("disabled",false);
		$("input[name='quantity']").parent().parent().parent().removeClass('has-error');
		$('#add_item').prop("disabled",false);
		
	});
});		
$('input:radio[name="payment_type"]').change(
	function(){
		if ( $(this).val() == 'card' ) {
			$('#card_voucher_no').show('fast');
			$('#ac_select_div').show('fast');
			$('#card_amount_for_both').hide('fast');   
			$('#cheque_num').hide('fast'); 
			$('#chk_num').val('');   
			$('#card_amount_id').val('');
		}    
		if($(this).val() == 'cash_card_both'){
			$('#card_voucher_no').show('fast');
			$('#ac_select_div').show('fast');
			$('#card_amount_for_both').show('fast'); 
			$('#cheque_num').hide('fast');    
			$('#chk_num').val(''); 
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
		if($(this).val() == 'cash'){
			$('#card_voucher_no').hide('fast');
			$('#ac_select_div').hide('fast');
			$('#card_amount_for_both').hide('fast');
			$('#cheque_num').hide('fast');   
			$('#chk_num').val(''); 
			$('#card_pay_voucher_id').val('');
			$('#card_amount_id').val('');
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
		if($(this).val() == 'cheque'){
			$('#card_voucher_no').hide('fast');
			$('#ac_select_div').hide('fast');
			$('#card_amount_for_both').hide('fast');    
			$('#cheque_num').show('fast');    
			$('#card_pay_voucher_id').val('');
			$('#card_amount_id').val('');
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
		}
	});
$('input[name=items_name]').focus();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
  	$("#bank_acc_select_result").show();
  	$("#bank_acc_select_result").html('');
  	clearTimeout(timer);
  	timer = setTimeout(function() 
  	{
  		var seachBankAcc = $("#bank_acc_select").val();
  		var html = '';
  		$.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
  			data = JSON.parse(data);
  			$.each(data, function(index, val) {
  				html+= '<tr><td height="40" charge_percent="'+val.charge_percentage+'" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
  			});
  			$("#bank_acc_select_result").html(html);
  		});
  	}, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
  	$('input[name="bank_acc_num"]').val($(this).html());
  	$('input[name="bank_acc_id"]').val($(this).attr('data'));
  	$('#charge_percent_id').val($(this).attr('charge_percent'));
  	$("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
  $('#dis_amt_id').keyup(function(event) {
  	var dis_type = $('#dis_type').val();
  	if (dis_type == ""){
  		alert("<?php echo $this->lang->line('enter_dis_type');?>");
  		$('#dis_amt_id').val('');
  	}
  });
  $('#dis_type').change(function(event) {
  	var dis_type = $('#dis_type').val();
  	var indivi_dis_amt= $('#dis_amt_id').val();
  	if(dis_type == "" && indivi_dis_amt != ""){
  		alert("<?php echo $this->lang->line('enter_dis_type_and_amt');?>");
  		$('#dis_amt_id').val('');
  	}
  });

  var hold_invoice_data= {
  	'individual_invoice_data' : [],
  };
  var date_data = new Date();
  var month = date_data.getMonth()+1;
  var day = date_data.getDate();
  var date_output = date_data.getFullYear() + '/' +(month<10 ? '0' : '') + month + '/' +(day<10 ? '0' : '') + day;

  $('#sell_table').on('click', '#hold_invoice_id', function(event) {
  	event.preventDefault();
  	$("#holdingInvoiceSelection").show();

  	// hold_invoice_data['individual_invoice_data'].push(render_data);
  	var allHoldingInvoices = localStorage.getItem('hodldingInvoices');
  	
  	if(allHoldingInvoices == null)
  	{
  		allHoldingInvoices = [];
  	}else{
  		allHoldingInvoices = JSON.parse(allHoldingInvoices);
  	}

  	if(render_data['item_info'].length == 0){
  		alert ("No Item Available In This Invoice");
  	}
  	else{
  		hold_invoice_temp_id= new Date().getUTCMilliseconds();
  		invoice_data = {
  			'holding_sn' : hold_invoice_temp_id+" --> "+date_output,
  			'render_data' : render_data
  		}
  		allHoldingInvoices.push(invoice_data);
  		localStorage.setItem('hodldingInvoices',JSON.stringify(allHoldingInvoices));
  		setHoldingInvoices();
  		clear_items_from_cart();
  		alert("Success.Invoice Hold For Further Use");
  	}
  	
  });

  function clear_items_from_cart() {
  	$('#discount_percentage').prop('checked', true);
  	$('#discount_percentage').parent().addClass('checked');
  	$('input[name="sales_rep_name"]').val('');
  	$('input[name="customers_name"]').val('');
  	$('input[name=delivery_date]').val('');
  	$('input[name="sales_rep_id"]').val('');
  	$('input[name="customers_id"]').val('');
  	$('.sell_item_row_table').remove();
  	render_data['item_info'] = [];
  	render_data['imei_serial_data'] = [];
  	render_data['all_basic_data'] = {};
  	$('input[name="tax_percentage"]').val('');
  	$('input[name="discount"]').val('');
  	$('input[name="received_money"]').val('');
  	$('input[name="paid"]').val('');
  	$('input[name="card_payment_voucher_no"]').val('');
  	$('input[name="card_amount"]').val('');
  	$('input[name="voucher_no"]').val('');
  	$('#chk_num').val('');
  	$('#total_amount').html('');
  	$('#total_amount_with_vat').html('');
  	$('#total_net_payable').html('');
  	$('#total_change').html('');
  	$('#total_due').html('');
  	$('input[name="customers_id"]').val('');
  	$('input[name="sales_rep_id"]').val('');
  	$('#save_sell').prop("disabled",false);
  	$('input[name=items_name]').focus();
  	$('input[name="bank_acc_num"]').val('');
  	$('input[name="bank_acc_id"]').val('');
  	$('#total_vat').val(current_active_vat_percentage);
  	if(!$('#inclusive_vat_checkbox').is(":checked")){
  		$('#inclusive_vat_checkbox').prop('checked', true);
  		$('#inclusive_vat_checkbox').parent().addClass('checked');
  	}
  	$("#options_cash").attr('checked', 'checked').click();
  	$('#card_voucher_no').hide('fast');
  	$('#ac_select_div').hide('fast');
  	$('#card_amount_for_both').hide('fast');
  	$('#cheque_num').hide('fast');  
  };
// jQuery(function(){
	// 	jQuery('#item_select').click();
	// 	// $('#item_select').trigger('click');
	// });
// $(document).ready(function(){ 
	// 	$("input").attr("autocomplete", "off"); 
	// });
	/* Tutorial Starts */
// var string_html = $("#newText").html();
// $(function(){
	// 	$("#typed").typed({
		//             // strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
		//             stringsElement: $("#newText"),
		//             typeSpeed: 30,
		//             backDelay: 500,
		//             loop: false,
		//             contentType: 'html', // or text
		//             // defaults to false for infinite loop
		//             loopCount: false,
		//             callback: function(){},
		//             resetCallback: function() {}
		//         });
	// });

</script>