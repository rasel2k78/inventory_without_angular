<?php
/**
 * Form Name:"Buy Form" 
 * 
 * This is Buy form for buying items form vendors.
 * 
 * @link   Buy/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- END PAGE LEVEL STYLES -->
<style type="text/css">
	.nmpd-grid {border: none; padding: 20px;}
	.nmpd-grid>tbody>tr>td {border: none;}

	/* Some custom styling for Bootstrap */
	.qtyInput {display: block;
		width: 100%;
		padding: 6px 12px;
		color: #555;
		background-color: white;
		border: 1px solid #ccc;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
		-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
		-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	}
</style>
<div class="row">
	<div class="col-md-6">        
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title_buy'); ?>
				</div>
			</div>
			<div class="portlet-body form">
				<?php 
				$form_attrib = array('class' => 'form-horizontal form-bordered', 'autocomplete'=>'off');
				echo form_open('',  $form_attrib);
				?>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9 input-icon input-form">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('voucher_tt'); ?>"></i> 
							<input type="text" class="form-control" name="voucher_no" id="voucher_no_id" value="<?php echo $voucher_no?>" placeholder="<?php echo $this->lang->line('right_voucher_no_placeholder'); ?>" readonly>
							<span class="help-block" id="voucher_help_block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo $this->lang->line('left_item_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
						</label>
						<div class="col-md-6 input-form input-icon">
							<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_select_tt'); ?>"></i> 
							<input type="text" name="items_name" placeholder="<?php echo $this->lang->line('left_item_name_placeholder'); ?>" id="item_select" class="form-control">
							<input type="hidden" name="items_id"  class="form-control">
							<table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
							</table>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_item_name_placeholder'); ?></span>
							<input type="hidden" name="is_unique_barcode" id="is_unique_barcode">
						</div>
						<div class="col-md-3 col-sm-4">
							<a href="#item_form_modal" title="<?php echo $this->lang->line('left_item_title'); ?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
								<i class="fa fa-plus"></i>
							</a>
						</div>
					</div>
					<br/>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_quantity'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
						</label>
						<div class="col-md-9">
							<div class="input-icon input-group ">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('quantity_tt'); ?>"></i>
								<input type="number" min="1" id="numpadButton1"  name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('left_quantity'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn1" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_quantity_err_msg');?></span>
						</div>
					</div>
					<div class="form-group" id="unique_barcode_div">

					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_buying_price'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
						</label>
						<div class="col-md-9">
							<div class="input-group input-icon">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('pp_tt'); ?>"></i>
								<input type="number" step="any" min="0"  name="buying_price" id="numpadButton2" class="form-control" placeholder="<?php echo $this->lang->line('left_buying_price_placeholder'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn2" type="button"><i class="glyphicon glyphicon-th numpadButton-btn"></i></button>
								</span>
							</div>
							<span class="help-block" style="display:none"><?php echo $this->lang->line('left_buying_price_err_msg'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_selling_item_retail'); ?></label>
						<div class="col-md-9">

							<div class="input-group input-icon">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sp_tt'); ?>"></i>
								<input type="number" min="0" step="any" min="0"  name="retail_price" id="numpadButton3" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_item_retail_placeholder'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn3" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> <?php echo $this->lang->line('left_selling_item_wholesale'); ?></label>
						<div class="col-md-9">
							<div class="input-group input-icon">
								<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('wp_tt'); ?>"></i>
								<input type="number" step="any" min="0"  name="whole_sale_price" id="numpadButton4" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_item_wholesale_placeholder'); ?>" aria-describedby="numpadButton-btn">
								<span class="input-group-btn">
									<button class="btn btn-default" id="numpadButton-btn4" type="button"><i class="glyphicon glyphicon-th"></i></button>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="panel-group accordion" id="accordion3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
											<?php echo $this->lang->line('left_extra_fields'); ?>
										</a>
									</h4>
								</div>
								<div id="collapse_3_1" class="panel-collapse collapse">
									<div class="panel-body">
										<div id="extra_fields" >
											<div class="form-group">
												<label class="control-label col-md-3"><?php echo $this->lang->line('left_discount_type'); ?>
												</label>
												<div class="col-md-3">
													<select class="form-control" id="dis_type" name="discount_type">
														<option value=""><?php echo $this->lang->line('select_one'); ?></option>
														<option value="amount"><?php echo $this->lang->line('left_discount_type_taka'); ?></option>
														<option value="percentage"><?php echo $this->lang->line('left_discount_type_percentage'); ?></option>
														<option value="free_quantity"><?php echo $this->lang->line('left_discount_type_quantity'); ?></option>
													</select>
												</div>
												<div class="col-md-6 input-form input-icon">
													<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('dt_tt'); ?>"></i> 
													<input type="number" min="0" step="any" name="discount_amount" class="form-control" placeholder="<?php echo $this->lang->line('left_discount_type_placeholder'); ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3"><?php echo $this->lang->line('left_comments'); ?></label>
												<div class="col-md-9 input-icon input-form">
													<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('comm_tt'); ?>"></i> 
													<input type="text" class="form-control" name="comments"  placeholder="<?php echo $this->lang->line('left_comments_placeholder'); ?>">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close();?>
				<div class="form-actions">
					<button id="cancle_item" type="button" class="btn  default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle_button'); ?></button>    
					<button id="add_item" type="button" class="btn green pull-right"><?php echo $this->lang->line('left_save_button'); ?></button>
					<button id="update_item" style="display:none" type="button" class="btn yellow pull-right"><?php echo $this->lang->line('left_edit_save_button'); ?></button>
					<button type="button" class="btn btn-primary" id="tutorial_start">Tutorial</button>

				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="alert alert-danger" id="item_quantity_invalid" style="display:none" role="alert"><?php echo $this->lang->line('item_quantity_invalid');?></div>
		<div class="alert alert-danger" id="buying_price_empty" style="display:none" role="alert"><?php echo $this->lang->line('buying_price_empty');?></div>
		<div class="alert alert-danger" id="item_not_selected" style="display:none" role="alert"><?php echo $this->lang->line('item_not_selected');?></div>
		<div class="alert alert-danger" id="voucher_no_empty" style="display:none" role="alert"><?php echo $this->lang->line('voucher_no_empty');?></div>
		<div class="alert alert-danger" id="dis_amount_invalid" style="display:none" role="alert"><?php echo $this->lang->line('dis_amount_invalid');?></div>
		<div class="alert alert-danger" id="landed_cost_invalid" style="display:none" role="alert"><?php echo $this->lang->line('landed_cost_invalid');?></div>
		<div class="alert alert-danger" id="paid_amount_invalid" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_invalid');?></div>
		<div class="alert alert-danger" id="paid_amount_empty" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_empty');?></div>
		<div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?></div>
		<div class="alert alert-success" id="buy_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_sus_msg_of_item_add');?></div>
		<div class="alert alert-success" id="insert_success_item" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_succeded'); ?></div>
		<div class="alert alert-danger" id="for_discount_calculation_error" style="display:none" role="alert"><?php echo $this->lang->line('for_discount_calculation_error'); ?></div>
		<div class="alert alert-danger" id="for_no_discount_calculation_error" style="display:none" role="alert"><?php echo $this->lang->line('for_no_discount_calculation_error'); ?></div>
		<div class="alert alert-danger" id="select_vendor_for_due_transaction" style="display:none" role="alert"><?php echo $this->lang->line('select_vendor_for_due_transaction'); ?></div>
		<div class="alert alert-danger" id="voucher_exist" style="display:none" role="alert"><?php echo $this->lang->line('voucher_exist'); ?></div>
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green-haze">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title_buy'); ?>
				</div>
			</div>
			<div class="portlet-body form" id="basic_buy_info">
				<form class="form-horizontal" >
					<div class="form-body ">
<!-- 					<div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_buy_type'); ?></label>
                        <div class="radio-list">
                            <label class="radio-inline">
                                <input type="radio" name="buy_type" id="optionsRadios4" value="cash_buy" checked><?php echo $this->lang->line('right_buy_type_cash'); ?>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="buy_type" id="optionsRadios5" value="advance_buy"><?php echo $this->lang->line('right_buy_type_advance'); ?>
                            </label>

                        </div>
                    </div> -->
<!--                     <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 input-icon input-form">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('voucher_tt'); ?>"></i> 
                            <input type="text" class="form-control" name="voucher_no" id="voucher_no_id" placeholder="<?php echo $this->lang->line('right_voucher_no_placeholder'); ?>">
                            <span class="help-block" id="voucher_help_block"></span>
                        </div>
                    </div> -->
    <!--                 <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_select_vendor'); ?> 
                        </label>
                        <div class="col-md-6">
                            <select class="form-control" name="vendors_id" id="vendor_select" data-live-search="true">
                            </select>

                        </div>
                    </div> -->
                    <div class="form-group">
                    	<label class="control-label col-md-3"> <?php echo $this->lang->line('right_select_vendor'); ?> 
                    	</label>
                    	<div class="col-md-6 input-form input-icon">
                    		<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('vendor_tt'); ?> "></i> 
                    		<input type="text" autocomplete="off" name="vendors_name" value="<?php echo $vendor_name?>" readonly placeholder="<?php echo $this->lang->line('right_select_vendor_placeholder'); ?> " id="vendor_select" class="form-control">
                    		<span class="help-block" id="vendor_help_block" ></span>
                    		<input type="hidden" name="vendors_id" value="<?php echo $vendor_id?>" class="form-control" readonly>
                    		<table class="table table-condensed table-hover table-bordered clickable" id="vendor_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                    		</table>
                    	</div>

                    </div>
                    <div class="form-group" style="display: none">
                    	<label class="control-label col-md-3"> <?php echo $this->lang->line('right_landed_cost'); ?></label>
                    	<div class="col-md-9 input-form input-icon">
                    		<i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('landed_tt'); ?>"></i> 
                    		<input type="number" min="0" value="0" step="any" class="form-control"  name="landed_cost" id="landed_cost_id" placeholder="<?php echo $this->lang->line('right_landed_cost_placeholder'); ?>">
                    		<span class="help-block" id="landed_help_block" ></span>
                    	</div>
                    </div>
                    <div class="form-group">
                    	<label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                    	<div class="radio-list">
                    		<label class="radio-inline">
                    			<input type="radio" name="payment_type" id="cash_id" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                    		</label>
                    		<label class="radio-inline" style="display: none">
                    			<input type="radio" name="payment_type" id="chk_id" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                    		</label>
                    	</div>
                    </div>
                    <div class="form-group" style="display: none" id="ac_select_div">
                    	<label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                    	</label>
                    	<div class="col-md-9">
                    		<input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
                    		<input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                    		<span class="help-block" id="bank_acc_help_block" ></span>
                    		<table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                    		</table>
                    	</div>
                    </div>
                    <div class="form-group" style=" display:none" id="chk_page_div">
                    	<label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                    	<div class="col-md-6">
                    		<input type="text" class="form-control" name="cheque_page_num" id="chk_page" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                    		<span class="help-block" id="cheque_help_block"></span>
                    	</div>
                    </div>
                    <!-- For Advance Complete Advance Payment Start-->
                    <?php if(isset($vendor_id)){ ?>
                    <input type="hidden" autocomplete="off" name="adv_buy_id" value="<?php echo $adv_buy_id?>" readonly>
                    <input type="hidden" autocomplete="off" name="adv_amt"  value="<?php echo $paid?>" readonly>
                    <?php  }?>
                    <!--  For Advance Complete Advance Payment End -->
                    <div class="form-group">
                    	<label class="control-label col-md-3"><?php echo $this->lang->line('left_date');?> </label>
                    	<div class="col-md-3">
                    		<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="purchase_date" placeholder="<?php echo $this->lang->line('left_date');?>" />
                    		<span class="help-block"></span>
                    	</div>
                    </div> 

                    <table id="buy_table" class="table table-condensed">
                    	<thead>
                    		<tr>
                    			<th><?php echo $this->lang->line('right_item_name'); ?></th>
                    			<th><?php echo $this->lang->line('right_quantity'); ?></th>
                    			<th><?php echo $this->lang->line('right_buying_price'); ?></th>
                    			<th><?php echo $this->lang->line('right_sub_total'); ?></th>
                    			<th><?php echo $this->lang->line('right_discount_type'); ?></th>
                    			<th><?php echo $this->lang->line('right_delete_edit'); ?></th>

                    		</tr>
                    	</thead>
                    	<tbody>
                    	</tbody>
                    	<tfoot>
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th><?php echo $this->lang->line('right_grand_total'); ?>:</th>
                    			<th id="total_amount"></th>
                    			<th></th>
                    			<th></th>

                    		</tr>
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th><?php echo $this->lang->line('right_discount'); ?>:</th>
                    			<th ><div class="input-form input-icon"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('discount_tt'); ?>"></i> <input class='form-control' type="number" min="0" step="any" id="total_discount" name="discount"  placeholder="<?php echo $this->lang->line('right_discount');?>"></div>
                    				<span class="help-block" id="total_discount_help_block" ></span>
                    			</th>
                    			<th></th>
                    			<th></th>

                    		</tr>
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th>Advance :</th>
                    			<th id="total_advance"><?php echo $paid?></th>
                    			<th></th>
                    			<th></th>
                    		</tr> 
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th><?php echo $this->lang->line('right_net_payable');?> :</th>
                    			<th id="total_net_payable"></th>
                    			<th></th>
                    			<th></th>

                    		</tr>
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th><?php echo $this->lang->line('right_paid'); ?> <sup><i class="fa fa-star custom-required"></i></sup>:</th>
                    			<th ><div class="input-icon input-form"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('paid_tt'); ?>"></i><input class='form-control' type="number" min="0" step="any" id="total_paid" name="paid" placeholder="<?php echo $this->lang->line('right_paid');?>"></div>
                    				<span class="help-block" id="total_paid_help_block" ></span>
                    			</th>
                    			<th></th>
                    			<th></th>

                    		</tr>    
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th><?php echo $this->lang->line('right_due'); ?>:</th>
                    			<th id="total_due"></th>
                    			<th></th>
                    			<th></th>
                    		</tr>
                    		<tr>
                    			<th></th>
                    			<th></th>
                    			<th></th>
                    			<th></th>
                    			<th></th>
                    			<th><button type="submit" id="save_buy" class="btn green"><?php echo $this->lang->line('right_save');?></button></th>
                    		</tr>

                    	</tfoot>
                    </table>
                </div>    
            </form>

        </div>
    </div>
</div>
</div>

<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="vendor_form_modal" class="modal fade">	
	<div class="modal-content">
		<div class="modal-body">
			<?php $this->load->view('vendor/add_vendor_form_only');?>
		</div>
	</div>
</div>
<!-- Customer Modal Form End -->

<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="item_form_modal" class="modal fade">	
	<div class="modal-content">
		<div class="modal-body">
			<?php $this->load->view('item/add_item_form_only');?>
		</div>
	</div>
</div>
<!-- Customer Modal Form End -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.numpad.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
	/*lading date picker*/
	jQuery(document).ready(function() {    
        // $('.date-picker').datepicker();
        $('.date-picker').datepicker({
        	format: 'yyyy-mm-dd',
        	rtl: Metronic.isRTL(),
        	orientation: "left",
        	autoclose: true
        });
        UIExtendedModals.init();
    });
	/*loading date picker ends*/
	jQuery(document).ready(function($) {
		$.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
		$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
		$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
		$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
		$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
		$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
		$('#numpadButton-btn1').numpad({
			target: $('#numpadButton1')
		});        
		$('#numpadButton-btn2').numpad({
			target: $('#numpadButton2')
		});    
		$('#numpadButton-btn3').numpad({
			target: $('#numpadButton3')
		});    
		$('#numpadButton-btn4').numpad({
			target: $('#numpadButton4')
		});    
	});
	var timer;
	var csrf = $("input[name='csrf_test_name']").val();

	table = $('#sample_2').DataTable();   
   // var unique_barcode = "";     
        // Customize Boostrap Select for Item Selection.
        $("#item_select").keyup(function(event) 
        {

        	$("#item_select_result").show();
        	$("#item_select_result").html('');
        	$("#unique_barcode_div").html("");
        	clearTimeout(timer);
        	timer = setTimeout(function() 
        	{
        		var search_item = $("#item_select").val();
        		var html = '';
        		$.post('<?php echo site_url(); ?>/buy/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
        			data = JSON.parse(data);
        			if(data.barcode_yes != null){
        				$.each(data, function(index, val) {
        					if(val.item_spec_set_image == null || val.item_spec_set_image == '')
        					{
        						val.item_spec_set_image    = "images/item_images/no_image.png";
        					}
        					var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
        					html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
        				});
        				$("#item_select_result").html(html);
        				$('#item_select_result').find('td').eq(0).click();
        				$('input[name=quantity]').focus();
        			}
        			else
        			{
        				$.each(data, function(index, val) 
        				{
        					if(val.item_spec_set_image == null || val.item_spec_set_image == '')
        					{
        						val.item_spec_set_image    = "images/item_images/no_image.png";
        					}
        					var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
        					html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
        				});
        				$("#item_select_result").html(html);
        			}
                    // if ($('#item_select').val().length >= 17) {
                    //     $('#item_select_result').find('td').eq(0).click();
                    //     $('input[name=quantity]').focus();
                    // }
                });
        	}, 500);
        });

        $("#item_select_result").on('click', 'td', function(event) 
        {
        	$('input[name="items_name"]').val($(this).text());
        	$('input[name="items_id"]').val($(this).attr('data'));
        	$('input[name="is_unique_barcode"]').val($(this).attr('unique_barcode'));
        	$("#item_select_result").hide();
        });

        $("#numpadButton1").keyup(function(event) 
        {
        	var item_quantity = $(this).val();
        	clearTimeout(timer);
        	timer = setTimeout(function() 
        	{
        		var unique_barcode_count = $('input[name*="unique_barcode_value[]"]').length;
        		if($("#is_unique_barcode").val() == "yes")
        		{

        			if(item_quantity=="")
        			{
        				$("#unique_barcode_div").html("");
        			}
        			else if(unique_barcode_count==0)
        			{
        				var html = "";
        				for(var i=1;i<=item_quantity;)
        				{
        					html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
        					html+= '</label><div class="col-md-9 input-form input-icon"><input ';
        					html+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
        					html+= 'Number '+i+'"></div></div>';
        					i++;
        				}
        				$("#unique_barcode_div").html(html);
        			}
        			else if(unique_barcode_count>item_quantity)
        			{
        				$(".imei_remove_class").show();
        			}  
        			else if(unique_barcode_count<item_quantity)
        			{
        				var html_new = "";
        				for(var i=unique_barcode_count;i<item_quantity;)
        				{
        					html_new+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
        					html_new+= '</label><div class="col-md-9 input-form input-icon"><input ';
        					html_new+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
        					html_new+= 'Number '+i+'"></div></div>';
        					i++;
        				}
        				$("#unique_barcode_div").append(html_new);
        			}              
        		}
        	}, 500);
        });



        /* Boostrap Customize Select Ends*/
        $("#vendor_select").keyup(function(event) 
        {
        	$("#vendor_select_result").show();
        	$("#vendor_select_result").html('');

        	clearTimeout(timer);
        	timer = setTimeout(function() 
        	{
        		var search_vendor = $("#vendor_select").val();
        		var html = '';
        		$.post('<?php echo site_url(); ?>/buy/search_vendor_by_name',{q: search_vendor,csrf_test_name: csrf}, function(data, textStatus, xhr) {
        			data = JSON.parse(data);
        			$.each(data, function(index, val) {
        				if(val.vendor_image == null || val.vendor_image == ''){
        					val.vendor_image = "images/user_images/no_image.png";
        				}
        				var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.vendor_image+'">';
        				html+= '<tr><td data="'+val.vendors_id+'">'+image_html+' '+ val.vendors_name+' -- '+val.vendors_phone_1+'</td></tr>';

        			});
        			$("#vendor_select_result").html(html);
        		});
        	}, 500);
        });

        $("#vendor_select_result").on('click', 'td', function(event) {
        	$('input[name="vendors_name"]').val($(this).text());
        	$('input[name="vendors_id"]').val($(this).attr('data'));
        	$("#vendor_select_result").hide();
        });


        var render_data= {
        	'imei_barcode' :[],
        	'item_info' : [],
        	'all_basic_data':{},
        };
        var temp_item_id = "";

        $('.form-actions').on('click', '#add_item', function(event) {
        	event.preventDefault();
        	var error_found=false;
        	if($('input[name=voucher_no]').val().trim()=="")
        	{
        		$('input[name=voucher_no]').closest('.form-group').addClass('has-error');

        		$('#voucher_help_block').text("<?php echo $this->lang->line('right_voucher_err_msg'); ?>")
            // $('input[name=voucher_no]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
        	$('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
            // $('input[name=voucher_no]').parent().find('.help-block').hide();
            $('#voucher_help_block').text("")
        }

        if($('input[name="items_id"]').val()==null || $('input[name="items_id"]').val()=="")
        {
        	$('input[name="items_id"]').closest('.form-group').addClass('has-error');
        	$('input[name="items_id"]').parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name="items_id"]').closest('.form-group').removeClass('has-error');
        	$('input[name="items_id"]').parent().find('.help-block').hide();

        }
        if($('input[name=quantity]').val().trim()=="")
        {
        	$('input[name=quantity]').closest('.form-group').addClass('has-error');
        	$('input[name=quantity]').parent().parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name=quantity]').closest('.form-group').removeClass('has-error');
        	$('input[name=quantity]').parent().parent().find('.help-block').hide();
        }
        if($('input[name=buying_price]').val().trim()=="")
        {
        	$('input[name=buying_price]').closest('.form-group').addClass('has-error');
        	$('input[name=buying_price]').parent().parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name=buying_price]').closest('.form-group').removeClass('has-error');
        	$('input[name=buying_price]').parent().parent().find('.help-block').hide();
        }

        /* Demo coding for new barcode integration*/
        /* Demo coding ends*/
        temp_item_id= new Date().getUTCMilliseconds();
        var imei_error = false;
        var imei_duplicate = false;
        if($("#is_unique_barcode").val()=="yes")
        {
        	var imei_barcodes = [];
        	$("input[name='unique_barcode_value[]']").each(function() 
        	{
        		var new_imei = $(this).val();
        		$.each(render_data['imei_barcode'], function(index, val) {

        			$.each(render_data['imei_barcode'][index], function(index2, val2) {
        				if(render_data['imei_barcode'][index][index2].imei_barcode == new_imei){
        					alert("Duplicate IMEI Found In Cart. (" +new_imei+")");
        					imei_duplicate = true;
        				}
        			});
        		});
        		if(imei_duplicate == true){
        			return false;
        		}
        		else if($(this).val()=="")
        		{
        			$(this).css("border-color", "red"); 
        			imei_error = true;
        		}
        		else
        		{
        			$(this).css("border-color", "");
        			imei_barcodes.push({
        				'item_spec_set_id' : $ ('input[name="items_id"]').val(),
        				'imei_barcode': $(this).val(),
        				'imei_temp_id':temp_item_id,
        			})
        		}

        	});
        	if(imei_duplicate == true){

        	}
        	else{
        		render_data['imei_barcode'].push(imei_barcodes); 
        	}
        }

        if(error_found)
        {
        	return false;
        }

        if(imei_duplicate == true){
        	return false;
        }

        if(imei_error)
        {
        	if(confirm("Want to serial number empty?"))
        	{
        		imei_error = true;
               // $("#unique_barcode_div").html("");
               //return true;
           }
           else
           {
           	return false;
           }
       }

       if($("#is_unique_barcode").val()=="yes")
       {
       	var unique_barcode_exist = $('input[name*="unique_barcode_value[]"]').length;
       	var current_quantity = $('input[name="quantity"]').val();
       	if(current_quantity!=unique_barcode_exist)
       	{
       		alert("Please make equal quantity of imei field");
       		$(".imei_remove_class").show();
       		return false;
       	}
       }

       render_data['item_info'].push({
            // 'item_spec_set_id' : $('#item_select').val(),
            // 'item_name' : $('#item_select option:selected').text(),
            'item_spec_set_id' : $ ('input[name="items_id"]').val(),
            'item_name' : $('input[name="items_name"]').val(),
            'actual_quantity' : $('input[name="quantity"]').val(),
            'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),

            'sub_total' : $('#dis_type').val()=='free_quantity' ? Number(parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)) : ($('#dis_type').val()=='amount' ? Number((parseFloat($('input[name="buying_price"]').val())*parseFloat($('input[name="quantity"]').val()))-parseFloat($ ('input[name="discount_amount"]').val()).toFixed(2)) : Number(((parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()))-((parseFloat($('input[name="buying_price"]').val())*parseFloat($('input[name="quantity"]').val())*parseFloat($ ('input[name="discount_amount"]').val()))/100)).toFixed(2))) || Number(parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)),

            'buying_price' : $('input[name="buying_price"]').val(),
            // 'buying_price_with_landed_cost' : $('input[name="buying_price_with_landed_cost"]').val(),
            'retail_price' : $ ('input[name="retail_price"]').val(),
            'whole_sale_price' : $ ('input[name="whole_sale_price"]').val(),
            'expire_date' : $ ('input[name="expire_date"]').val(),
            'comments' : $ ('input[name="comments"]').val(),
            'discount_amount' : $ ('input[name="discount_amount"]').val(),
            'discount_type' : $('#dis_type').val(),
            'unique_barcode' : $("#is_unique_barcode").val(),
            // 'quantity_with_free' : $('#dis_type').val(),
            'cart_temp_id':temp_item_id,
        })

       buy_table_render();

        //  clear the whole form rather than individual        **

        // $('#item_select').selectpicker('val','');
        $('input[name="items_name"]').val('');
        $('input[name="items_id"]').val('');
        $("#unique_barcode_div").html("");
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
        $('input[name="expire_date"]').val('');
        $('input[name="comments"]').val('');
        $('input[name="discount_amount"]').val('');
        $("#is_unique_barcode").val('');
        $('#dis_type').val('');
        // if($('#extra_fields').show()){
        //     $('#extra_fields').hide();
        // }
        // $('#extra_fields').show();
        $('input[name=items_name]').focus();
    });


var total_amount = 0;
var total_net_payable= 0;
var due_after_advance = 0;
var total_quantity_with_free = 0 ; 
function buy_table_render () {
	var buy_table_html = '';
	total_amount = 0;
	var subtotal_amount = 0;
	$.each(render_data['item_info'], function(index, val) {
		buy_table_html += '<tr class="item_row_table" id="'+index+'">';
		buy_table_html += '<td>'+val.item_name+'</td>';
		if(val.discount_type =="" ||val.discount_type =="amount" || val.discount_type =="percentage" ){
			buy_table_html += '<td>'+val.quantity+'</td>';
		}
		else if(val.discount_type =="free_quantity"){
			total_quantity_with_free = parseInt(val.actual_quantity);
			buy_table_html += '<td>'+total_quantity_with_free+'</td>';
		}
		buy_table_html += '<td>'+Number(val.buying_price).toFixed(2)+'</td>';
		if (val.discount_type =="") {
			buy_table_html += '<td>'+Number(val.quantity*val.buying_price).toFixed(2)+'</td>';
			subtotal_amount+= val.quantity*val.buying_price;
			Number(subtotal_amount.toFixed(2));
		}
		else if(val.discount_type =="amount"){
			var total= val.quantity*val.buying_price;
			total -= val.discount_amount;
			buy_table_html += '<td>'+Number(total).toFixed(2)+'</td>';
			subtotal_amount+= total;
			Number(subtotal_amount.toFixed(2));
		}
		else if (val.discount_type  =="percentage"){
			var total_sub_total= val.quantity*val.buying_price;
			var total =total_sub_total*val.discount_amount;
			total /=100;
			total_sub_total-=total;
			buy_table_html += '<td>'+Number(total_sub_total).toFixed(2)+'</td>';
			subtotal_amount+= total_sub_total;
			Number(subtotal_amount.toFixed(2));
		}
		else if(val.discount_type =="free_quantity"){
			buy_table_html += '<td>'+val.actual_quantity*val.buying_price+'</td>';
			subtotal_amount+= val.actual_quantity*val.buying_price;
			Number(subtotal_amount.toFixed(2));
		}
		buy_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
		buy_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a> <a id="" class="glyphicon glyphicon-pencil item_edit" style="color:blue;display:none"></a>';
		buy_table_html += '</td>';
		buy_table_html += '</tr>';
		total_amount = subtotal_amount;
		Number(total_amount.toFixed(2));
	});
	due_after_advance = Number(total_amount.toFixed(2)) - Number($('#total_advance').html());
	render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));
	render_data['all_basic_data']['due']=Number(total_amount.toFixed(2));
	$('#total_paid').val("");
	$('#total_discount').val(0);
	$('#buy_table tbody').html(buy_table_html);
	$('#total_amount').html(Number(total_amount.toFixed(2)));
	$('#total_net_payable').html(due_after_advance);
	$('#total_due').html(due_after_advance);
}
render_data['all_basic_data']['discount']=0;
render_data['all_basic_data']['net_pay_after_discount']=0;
$('#total_discount').keyup(function(event) {
	if (event.keyCode == '9') {
		return false;
	};
	if ($(this).val() > Number($('#total_amount').text()) ) {
		alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
		$(this).val(0);
		render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));
		render_data['all_basic_data']['due']=Number(total_amount.toFixed(2));
	}
	var net_pay_after_discount = parseFloat(total_amount) - parseFloat($( this ).val()) - parseFloat($('#total_advance').html())||0;
	$('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
	$('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
	render_data['all_basic_data']['discount']=parseFloat($(this).val());
	render_data['all_basic_data']['due']=Number(net_pay_after_discount.toFixed(2));
	render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
	render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
	$('#total_paid').val('');
});
var current_cashbox_amount = <?php echo $cashbox_amount['total_cashbox_amount'];?>;
$('#total_paid').keyup(function(event) {
	if (event.keyCode == '9') {
		return false;
	};
    // if (event.keyCode == '8') {
    //     return false;
    // };
    if (parseFloat($(this).val()) < current_cashbox_amount)
    {
    	var due_after_paid = Number(total_amount.toFixed(2)) - parseFloat($(this).val()) - parseFloat($('#total_advance').html())- parseFloat($('#total_discount').val())||0;
    	$('#total_due').text(Number(due_after_paid.toFixed(2)));
    	render_data['all_basic_data']['paid']=parseFloat($(this).val());
    	render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));
    	render_data['all_basic_data']['advance_amount']=Number($('#total_advance').html());
    }
    else{
        // alert("<?php echo $this->lang->line('alert_cashbox_err_msg'); ?>");
        var due_after_paid = Number(total_amount.toFixed(2)) - parseFloat($(this).val() || 0)- parseFloat($('#total_advance').html()) - parseFloat($('#total_discount').val());
        $('#total_due').text(Number(due_after_paid.toFixed(2)));
        render_data['all_basic_data']['paid']=parseFloat($(this).val());
        render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));
        render_data['all_basic_data']['advance_amount']=Number($('#total_advance').html());
    }
});
$('#cancle_item').click(function(event) {
    // $('#item_select').selectpicker('val','');
    $('input[name="items_name"]').val('');
    $('input[name="items_id"]').val('');
    $('input[name="quantity"]').val('');
    $('input[name="buying_price"]').val('');
    $('input[name="retail_price"]').val('');
    $('input[name="whole_sale_price"]').val('');
    $('input[name="expire_date"]').val('');
    $('input[name="comments"]').val('');
    $('input[name="discount_amount"]').val('');
    $('#dis_type').val('');
    $("#unique_barcode_div").html("");
    if($('#update_item').show()){
    	$('#update_item').hide();
    	$('#add_item').show('fast');
    }
    $("#is_unique_barcode").val('');
});
$('#buy_table').on('click', '#item_remove', function () {
	var clicked_id = $(this).closest('tr').attr('id');
	var temp_id_remove = render_data['item_info'][clicked_id].cart_temp_id;
	$.each(render_data['imei_barcode'], function(index, val) {
		if(render_data['imei_barcode'][index][0].imei_temp_id == temp_id_remove){
			render_data['imei_barcode'].splice(index,1);
			return false;
		}
	});
	render_data['item_info'].splice(clicked_id,1);
	buy_table_render();
  //  $("#is_unique_barcode").val('');
});
$('#unique_barcode_div').on('click', '#imei_remove', function () {
	$(this).closest(".form-group").remove();
});

$('#buy_table').on('click', '.item_edit', function () {
	var clicked_id = $(this).closest('tr').attr('id');
	var editing_item_info;
	editing_item_info = render_data['item_info'][clicked_id];
	$("#unique_barcode_div").html("");
	if(editing_item_info.unique_barcode=="yes")
	{
		$("#is_unique_barcode").val("yes");
	}
	else
	{
		$("#is_unique_barcode").val("no");
	}

	if($("#is_unique_barcode").val()== "yes")
	{
		var html = "";
		var i=1;
		$.each(render_data['imei_barcode'],function(index, val) 
		{
			$.each(render_data['imei_barcode'][index],function(index1, el)
			{
				if(editing_item_info.item_spec_set_id==el.item_spec_set_id)
				{
					html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
					html+= '</label><div class="col-md-9 input-form input-icon"><input ';
					html+='type="text" name="unique_barcode_value[]" class="form-control" value="'+el.imei_barcode+'" placeholder="Serial ';
					html+= 'Number '+i+'"><a style="display:none;color:red" id="imei_remove" class="glyphicon glyphicon-remove imei_remove_class" style="color:red;"></a></div></div>';
					i++;
				}
			});
		});
		for(j=i;j<=editing_item_info.actual_quantity;)
		{
			html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
			html+= '</label><div class="col-md-9 input-form input-icon"><input ';
			html+='type="text" name="unique_barcode_value[]" class="form-control" value="" placeholder="Serial ';
			html+= 'Number '+i+'"><a style="display:none;color:red" id="imei_remove" class="glyphicon glyphicon-remove imei_remove_class" style="color:red;"></a></div></div>';
			j++;
		}
		$("#unique_barcode_div").html(html);
	}



	$('#item_select').prop("disabled",true);
	$('input[name="items_id"]').val(editing_item_info.item_spec_set_id),
	$('input[name="items_name"]').val(editing_item_info.item_name),
	$('input[name="buying_price"]').val(editing_item_info.buying_price);
	$('select[name=discount_type]').val(editing_item_info.discount_type);
	$('input[name="buying_price"]').val(editing_item_info.buying_price);
	$('input[name="quantity"]').val(editing_item_info.actual_quantity);
	$('input[name="comments"]').val(editing_item_info.comments);
	$('input[name="discount_amount"]').val(editing_item_info.discount_amount);
	$('input[name="expire_date"]').val(editing_item_info.expire_date);
	$('input[name="retail_price"]').val(editing_item_info.retail_price);
	$('input[name="whole_sale_price"]').val(editing_item_info.whole_sale_price);
	$('#add_item').hide();
	$('#update_item').show('slow');
	$('#update_item').attr('selected-id', clicked_id);

});
$('.form-actions').on('click', '#update_item', function(event) {
	event.preventDefault();

	var error_found=false;
	if($('input[name=voucher_no]').val().trim()=="")
	{
		$('input[name=voucher_no]').closest('.form-group').addClass('has-error');

		$('#voucher_help_block').text("<?php echo $this->lang->line('right_voucher_err_msg'); ?>")
            // $('input[name=voucher_no]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
        	$('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
            // $('input[name=voucher_no]').parent().find('.help-block').hide();
            $('#voucher_help_block').text("")
        }

        if($('input[name="items_id"]').val()==null || $('input[name="items_id"]').val()=="")
        {
        	$('input[name="items_id"]').closest('.form-group').addClass('has-error');
        	$('input[name="items_id"]').parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name="items_id"]').closest('.form-group').removeClass('has-error');
        	$('input[name="items_id"]').parent().find('.help-block').hide();

        }
        if($('input[name=quantity]').val().trim()=="")
        {
        	$('input[name=quantity]').closest('.form-group').addClass('has-error');
        	$('input[name=quantity]').parent().parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name=quantity]').closest('.form-group').removeClass('has-error');
        	$('input[name=quantity]').parent().parent().find('.help-block').hide();
        }
        if($('input[name=buying_price]').val().trim()=="")
        {
        	$('input[name=buying_price]').closest('.form-group').addClass('has-error');
        	$('input[name=buying_price]').parent().parent().find('.help-block').show();
        	error_found=true;
        }
        else
        {
        	$('input[name=buying_price]').closest('.form-group').removeClass('has-error');
        	$('input[name=buying_price]').parent().parent().find('.help-block').hide();
        }



        /* Demo coding for new barcode integration*/

        /* Demo coding ends*/
       // var temp_item_id = "";


       var imei_error = false;
       var indexPoint ;
       var item_spec_set_id = $ ('input[name="items_id"]').val();
       $.each(render_data['imei_barcode'],function(index, val) 
       {
       	if($.inArray(item_spec_set_id, render_data['imei_barcode'][index]))
       	{
       		render_data['imei_barcode'][index].length=0
       		indexPoint = index;
       		return false;
       	}
       });
       console.log(indexPoint);

       if($("#is_unique_barcode").val()=="yes")
       {

       	var imei_barcodes = [];
       	$("input[name='unique_barcode_value[]']").each(function() 
       	{
       		if($(this).val()=="")
       		{

       			$(this).css("border-color", "red"); 
       			imei_error = true;
       		}
       		else
       		{
       			$(this).css("border-color", "");
       			render_data['imei_barcode'][indexPoint].push({
       				'item_spec_set_id' : $ ('input[name="items_id"]').val(),
       				'imei_barcode': $(this).val(),
       				'temp_id':temp_item_id,
       			})
       		}

       	});
          //  render_data['imei_barcode'][indexPoint].push(imei_barcodes); 
      }

      console.log(render_data['imei_barcode']);


      if(error_found)
      {
      	return false;
      }


      if(imei_error)
      {
      	if(confirm("Want to serial number empty?"))
      	{
      		imei_error = true;
               // $("#unique_barcode_div").html("");
               //return true;
           }
           else
           {
           	return false;
           }
       }

       if($("#is_unique_barcode").val()=="yes")
       {
       	var unique_barcode_exist = $('input[name*="unique_barcode_value[]"]').length;
       	var current_quantity = $('input[name="quantity"]').val();
       	if(current_quantity!=unique_barcode_exist)
       	{
       		alert("Please make equal quantity of imei field");
       		$(".imei_remove_class").show();
       		return false;
       	}
       }


       var change_item_index_array = $(this).attr('selected-id');
        // render_data.item_info[change_item_index_array].item_spec_set_id = $('#item_select').val();
        // render_data.item_info[change_item_index_array].item_name = $('#item_select option:selected').text();
        render_data.item_info[change_item_index_array].item_spec_set_id = $('input[name="items_id"]').val();
        render_data.item_info[change_item_index_array].item_name = $('input[name="items_name"]').val();
        // render_data.item_info[change_item_index_array].quantity = $('input[name="quantity"]').val();
        render_data.item_info[change_item_index_array].quantity = ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val();
        render_data.item_info[change_item_index_array].actual_quantity = $('input[name="quantity"]').val();
        render_data.item_info[change_item_index_array].discount_type = $('#dis_type').val();
        render_data.item_info[change_item_index_array].discount_amount = $('input[name="discount_amount"]').val();
        render_data.item_info[change_item_index_array].retail_price = $('input[name="retail_price"]').val();
        render_data.item_info[change_item_index_array].whole_sale_price = $('input[name="whole_sale_price"]').val();
        render_data.item_info[change_item_index_array].buying_price = $('input[name="buying_price"]').val();
        render_data.item_info[change_item_index_array].comments = $('input[name="comments"]').val();
        $("#unique_barcode_div").html("");
        buy_table_render();
        $('#item_select_field').show('fast');
        $('#add_item').show('fast');
        $('#update_item').hide();
        $("#unique_barcode_div").html("");
        $('#item_select').prop("disabled",false);
        // $('#item_select').selectpicker('val','');
        $('input[name="items_name"]').val('');
        $('input[name="items_id"]').val('');
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
        $('input[name="expire_date"]').val('');
        $('input[name="comments"]').val('');
        $('input[name="discount_amount"]').val('');
        $("#is_unique_barcode").val('');
        $('#dis_type').val('');

   // }


});
$('#save_buy').click(function(event) {
	event.preventDefault();
	var keyCode = event.keyCode || event.which;
	if (keyCode === 13) { 
		event.preventDefault();
		return false;
	}
	$('#save_buy').prop("disabled",false);
	render_data.all_basic_data.payment_type = $('input[name=payment_type]:checked').val();
	render_data.all_basic_data.voucher_no = $('input[name="voucher_no"]').val();
	render_data.all_basic_data.vendor_id = $('input[name="vendors_id"]').val();
	render_data.all_basic_data.vendor_name =$('input[name="vendors_name"]').val();
	render_data.all_basic_data.total_cost = $('input[name="landed_cost"]').val();
	render_data.all_basic_data.discount = $('input[name="discount"]').val();
	render_data.all_basic_data.paid = $('input[name="paid"]').val();
	render_data.all_basic_data.bank_acc_id = $('input[name=bank_acc_id]').val();
	render_data.all_basic_data.cheque_page_num = $('input[name="cheque_page_num"]').val();
	render_data.all_basic_data.purchase_date = $('input[name=purchase_date]').val();
	render_data.all_basic_data.adv_buy_id= $('input[name=adv_buy_id]').val() || 0;
	render_data.all_basic_data.adv_amt= $('input[name=adv_amt]').val() || 0;
	// render_data.all_basic_data.custom_voucher_no= $('input[name=voucher_no]').val() || 0;
	var all_data= {
		'csrf_test_name' :$('input[name=csrf_test_name]').val(),
		'all_data' : render_data,
	}
	$.post('<?php echo site_url() ?>/Advance_buy/save_adv_buy_info',all_data ).done(function(data, textStatus, xhr) {
		var result = $.parseJSON(data);
		remove_all_errors();
		if (result== "success") {
			render_data['item_info'] = [];
			render_data['imei_barcode'] = [];
			render_data['all_basic_data'] = {};
			$("#cash_id").attr('checked', 'checked').click();
			$('#chk_page_div').hide('fast');
			$('#ac_select_div').hide('fast');    
			$('input[name=purchase_date]').val('');
			$('input[name="vendors_name"]').val('');
			$('input[name="vendors_id"]').val('');
			$('input[name="bank_acc_num"]').val('');
			$('input[name="bank_acc_id"]').val('');
			$('input[name="cheque_page_num"]').val('');
			$('input[name="voucher_no"]').val('');
			$('input[name="landed_cost"]').val(0);
			$('input[name="discount"]').val('');
			$('input[name="paid"]').val('');
			$('#total_net_payable').val('');
			$('.item_row_table').remove();
			$("#is_unique_barcode").val('');

			$('#total_amount').html('');
			$('#total_net_payable').html('');
			$('#total_due').html('');
			alert("Advance Order Completed Successfully");
			window.location.href = "<?php echo site_url();?>/Buy";
			// $('#buy_success').slideDown();
			// setTimeout( function(){$('#buy_success').slideUp()}, 3000 );        
			$('#save_buy').prop("disabled",false);
			$('#total_discount').val(0);
		}
		else{
			var error_array = [];
			$.each(result, function(index, val) {
				error_array.push(val);
				if(val =="Enter actual paid amount"){
                      //  alert("Enter actual paid amount");
                     //   error_array.push('enter_actual_paid_amount');
                     $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                     $('#total_paid_help_block').text("<?php echo $this->lang->line('more_than_actual_amount'); ?>");
                 }
                //  if(val == "duplicate imei inserted"){
                //     alert("Duplicate IMEI/SERIAL Inserted");
                // }
                // if(val == "already sold once and status deactivated"){
                //     alert("Particular IMEI or SERIAL Already Sold Once");

                // }
                    // if(val == "Item Quantity is Invalid"){
                    //     error_array.push('item_quantity_invalid');
                    //     alert("<?php echo $this->lang->line('item_quantity_invalid'); ?>");
                    // }
                    // if(val == "Item Buying Price is Empty"){
                    //     error_array.push('buying_price_empty');
                    //     alert("<?php echo $this->lang->line('buying_price_empty'); ?>");
                    // }

                    // if(val == "Item Not Selected"){
                    // //    error_array.push('item_not_selected');
                    // //    alert("<?php echo $this->lang->line('item_not_selected'); ?>");
                    // }

                    if(val == "Voucher No Cannot Be Empty"){
                    //error_array.push('voucher_no_empty');
                   // alert("Voucher No Cannot Be Empty");
                   $('#voucher_no_id').closest('.form-group').addClass('has-error');
                   $('#voucher_help_block').text("<?php echo $this->lang->line('voucher_no_empty'); ?>");
               }
               if(val == "Voucher No Already Exist"){
                    // error_array.push('voucher_exist');
                        // alert();
                        $('#voucher_no_id').closest('.form-group').addClass('has-error');
                        $('#voucher_help_block').text("<?php echo $this->lang->line('voucher_exist'); ?>");
                    }
                    
                    if(val == "Discount Amount Is Not Valid"){
                    //    error_array.push('dis_amount_invalid');
                    $('table tfoot tr').eq(1).css("color","red").addClass('has-error');
                    $('#total_discount_help_block').text("<?php echo $this->lang->line('dis_amount_invalid'); ?>");

                }
                if(val == "Landed Cost Is Not Numeric"){
                    //    error_array.push('landed_cost_invalid');
                    $('#landed_cost_id').closest('.form-group').addClass('has-error');
                    $('#landed_help_block').text("<?php echo $this->lang->line('landed_cost_invalid'); ?>");
                }
                if(val == "Paid Amount Is Not Numeric"){
                    //    error_array.push('paid_amount_invalid');
                    $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                    $('#total_paid_help_block').text("<?php echo $this->lang->line('paid_amount_invalid'); ?>");
                }
                if(val == "Total Paid Amount is Empty or Not Numeric"){
                    //    error_array.push('paid_amount_empty');
                    $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                    $('#total_paid_help_block').text("<?php echo $this->lang->line('paid_amount_empty'); ?>");
                }
                if(val == "Select Vendor for Due Transaction"){
                    //    error_array.push('select_vendor_for_due_transaction');
                    $('#vendor_select').closest('.form-group').addClass('has-error');
                    $('#vendor_help_block').text("<?php echo $this->lang->line('select_vendor_for_due_transaction')?>");
                }
                if (val == "Bank Acc Not Select") {
                   //  error_array.push('bank_acc_not_select');
                   $('#bank_acc_select').closest('.form-group').addClass('has-error');
                   $('#bank_acc_help_block').text("<?php echo $this->lang->line('bank_acc_not_select')?>");
               }
               if(val == "Not Enough Balance in Selected Account"){
               	$('#bank_acc_select').closest('.form-group').addClass('has-error');
               	$('#bank_acc_help_block').text("<?php echo $this->lang->line('not_enough_balance')?>");
               }
               if(val == "Cheque Page Number Empty"){
                  //  error_array.push('cheque_page_empty');
                  $('#chk_page').closest('.form-group').addClass('has-error');
                  $('#cheque_help_block').text("<?php echo $this->lang->line('cheque_page_empty'); ?>");
              }
              if(val == "Calculation Mismatch"){
                  //  error_array.push('for_no_discount_calculation_error');
                  $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                  $('#total_paid_help_block').text("<?php echo $this->lang->line('for_no_discount_calculation_error'); ?>");
              }
              if(val == "Calculation Mismatch for discount"){
                  //  error_array.push('for_discount_calculation_error');
                  $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                  $('#total_paid_help_block').text("<?php echo $this->lang->line('for_no_discount_calculation_error'); ?>");
                  $('table tfoot tr').eq(1).css("color","red").addClass('has-error');
                  $('#total_discount_help_block').text("<?php echo $this->lang->line('for_discount_calculation_error'); ?>");
              }
          });
$.each(error_array, function(index, val) {
                    // $('#'+val).slideDown();
                    // setTimeout( function(){$('#'+val).slideUp()}, 5000 );
                    alert(val);
                    $('#save_buy').prop("disabled",false);
                });

}
}).error(function() {
	alert("<?php echo $this->lang->line('alert_failed_msg');?>");
	$('#save_buy').prop("disabled",false);
});
});
function remove_all_errors() {
	$('#bank_acc_select').closest('.form-group').removeClass('has-error');
	$('#vendor_help_block').text("");

	$('#vendor_select').closest('.form-group').removeClass('has-error');
	$('#bank_acc_help_block').text("");

	$('#chk_page').closest('.form-group').removeClass('has-error');
	$('#cheque_help_block').text("");

	$('table tfoot tr').eq(3).css("color","black").removeClass('has-error');
	$('#total_paid_help_block').text("");

	$('table tfoot tr').eq(1).css("color","black").removeClass('has-error');
	$('#total_discount_help_block').text("");

	$('#landed_cost_id').closest('.form-group').removeClass('has-error');
	$('#landed_help_block').text("");

	$('#voucher_no_id').closest('.form-group').removeClass('has-error');
	$('#voucher_help_block').text("");
}

$('input[name=voucher_no]').focus();

$('#total_discount').val(0);

/* Initial value provided while load the buy page for blank buy form submit and error display*/
render_data['all_basic_data']['net_payable']=Number(0);
render_data['all_basic_data']['due']=Number(0);
render_data['all_basic_data']['due_after_paid']=Number(0);
/**/
// Start of AJAX Booostrap for Category selection
var options = {
	ajax          : {
		url     : '<?php echo site_url(); ?>/category/search_category_by_name',
		type    : 'POST',
		dataType: 'json',
        // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
        // automatically replace it with the value of the search query.
        data    : {
        	q: '{{{q}}}',
        	csrf_test_name: csrf
        }
    },
    locale        : {
    	emptyTitle: '<?php echo $this->lang->line('main_category_name'); ?>'
    },
    log           : 3,
    preprocessData: function (data) {
    	var i, l = data.length, array = [];
    	if (l) {
    		for (i = 0; i < l; i++) {
    			array.push($.extend(true, data[i], {
    				text : data[i].categories_name,
    				value: data[i].categories_id,
    				data : {
    					subtext: ''
    				}
    			}));
    		}
    	}
        // You must always return a valid array when processing data. The
        // data argument passed is a clone and cannot be modified directly.
        return array;
    }
};
$('#select_category').selectpicker().ajaxSelectPicker(options);
$('#select_category').trigger('change');
// End of AJAX Boostrap for Category selection

$('input:radio[name="payment_type"]').change(
	function(){
		if ( $(this).val() == 'cheque' ) {
			$('#chk_page_div').show('fast');
			$('#ac_select_div').show('fast');    
		}    
		if($(this).val() == 'cash'){
			$('#chk_page_div').hide('fast');
			$('#ac_select_div').hide('fast');    

		}
	});
var timer;
var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
  	$("#bank_acc_select_result").show();
  	$("#bank_acc_select_result").html('');
  	clearTimeout(timer);
  	timer = setTimeout(function() 
  	{
  		var seachBankAcc = $("#bank_acc_select").val();
  		var html = '';
  		$.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
  			data = JSON.parse(data);
  			$.each(data, function(index, val) {
  				html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
  			});
  			$("#bank_acc_select_result").html(html);
  		});
  	}, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
  	$('input[name="bank_acc_num"]').val($(this).html());
  	$('input[name="bank_acc_id"]').val($(this).attr('data'));
  	$("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
</script>