<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/jquery.numpad.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">



<div class="row">
    <div class="col-md-6">

        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>
            </div>
            <div class="portlet-body form" id="form1">
                <?php $form_attrib = array('id' => 'exchange_customers_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?> </label>
                        <div class="col-md-9">
                            <input type="text"  class="form-control" name="sell_voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>">
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green"><?php echo $this->lang->line('find'); ?> </button>
                    <button type="button" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>

    <div class="portlet box red" id="sell_voucher_details_form" style="display:none" >
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><?php echo $this->lang->line('buy_details'); ?>
            </div>

        </div>
        <div class="portlet-body form"  >
            <!-- BEGIN FORM-->

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('voucher_no'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="voucher_no" placeholder="<?php echo $this->lang->line('voucher_no'); ?>" readonly="true">
                        <input type="hidden" name="sell_details_id" id="sell_details_id">
                    </div>
                </div>
            </div>



            <table id="table_items" class="table table-hover control-label col-md-3">
                <thead>
                    <tr>
                        <th class="control-label col-md-3"><?php echo $this->lang->line('item_name'); ?></th>
                        <th class="control-label col-md-2"><?php echo $this->lang->line('quantity'); ?></th>                    
                        <th class="control-label col-md-2"><?php echo $this->lang->line('selling_price'); ?></th>
                        <th class="control-label col-md-2">Discount in amount</th>
                        <th class="control-label col-md-3"><?php echo $this->lang->line('sub_total'); ?></th>
                        <th class="control-label col-md-3" style="display: none"></th>
                        <th class="control-label col-md-3">Imei</th>
                        <th class="control-label col-md-3"><?php echo $this->lang->line('return_quantity'); ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>


            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Grand Total</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="grand_total" id="grand_total" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Net Payable</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="net_payable" placeholder="<?php echo $this->lang->line('total_bill'); ?>" readonly="true">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Total Discount</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="total_discount" id="total_discount" placeholder="Total Discount" readonly="true">
                    </div>
                </div>

                
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('total_paid'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="paid_new" placeholder="<?php echo $this->lang->line('total_paid'); ?>" readonly="true">

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('total_due'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control"  name="total_due" placeholder="<?php echo $this->lang->line('total_receivable'); ?>" readonly="true">
                    </div>
                </div>

                <div class="form-group" id="return_money_div" style="display: none;">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('return_money'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="return_money" placeholder="<?php echo $this->lang->line('return_money'); ?>" readonly="true">
                    </div>

                </div>
            </div>
            
            <div class="form-actions">
                <button type="button" class="btn green" id="save_sell"> <?php echo $this->lang->line('save'); ?></button>
            </div>
            <?php echo form_close();?>
            <!-- END FORM-->
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">

        <div class="portlet box green-haze" id="returned_item_form" style="display: none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>
                    <?php echo $this->lang->line('returned_items'); ?>  
                </div>
            </div>
            <div class="portlet-body">
             <table id="buy_table" class="table table-condensed">
                <thead>
                    <tr>
                        <th>
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Exchanged items list"></i>&nbsp;<?php echo $this->lang->line('item_name'); ?>                        </th>
                            <th><?php echo $this->lang->line('quantity'); ?></th>
                            <th><?php echo $this->lang->line('selling_price'); ?></th>
                            <th><?php echo $this->lang->line('sub_total'); ?></th>
                            <th>Net Sub-total</th>
                            <th>Imei Number</th>
                            <!-- <th><?php echo $this->lang->line('delete'); ?></th> -->
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Exchanged items total price"></i>&nbsp;<?php echo $this->lang->line('total'); ?>:</th>
                            <th id="total_exchanged"></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>


        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
        <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->	
        <script>
            var csrf = $("input[name='csrf_test_name']").val();
            var timer;
            function change_form()
            {
                $('#form1').hide();
                $('#form2').show();
            }

            table_brand = $('#sample_2').DataTable();


            var render_data= {
                'item_info' : [],
                'all_basic_data':{},
            };

            var render_new_data= {
                'item_info_new' : [],
                'all_basic_data':{},
            };
            var total_amount = 0;
            var total_net_payable= 0;

            function sell_table_render () {
                var sell_table_html = '';
                total_amount = 0;
                var subtotal_amount = 0;
                $.each(render_data['item_info'], function(index, val) 
                {
                    sell_table_html += '<tr id="'+index+'">';

                    sell_table_html += '<td>'+val.item_name+'</td>';
                    if(val.discount_type =="" ||val.discount_type =="amount" || val.discount_type =="percentage" ){
                        sell_table_html += '<td>'+val.quantity+'</td>';
                    }
                    else if(val.discount_type =="free_quantity"){
                        var total_quantity_with_free = parseFloat(val.quantity)+parseFloat(val.discount_amount);
                        sell_table_html += '<td>'+total_quantity_with_free+'</td>';
                    }

                    sell_table_html += '<td>'+val.selling_price+'</td>';
                    if (val.discount_type =="") {
                        sell_table_html += '<td>'+val.quantity*val.selling_price+'</td>';
                        subtotal_amount+= val.quantity*val.selling_price;
                    }
                    else if(val.discount_type =="amount"){

                        var total= val.quantity*val.selling_price;
                        total -= val.discount_amount;
                        sell_table_html += '<td>'+total+'</td>';
                        subtotal_amount+= total;
                    }
                    else if (val.discount_type  =="percentage"){

                        var total_sub_total= val.quantity*val.selling_price;
                        var total =total_sub_total*val.discount_amount;
                        total /=100;
                        total_sub_total-=total;

                        sell_table_html += '<td>'+total_sub_total+'</td>';
                        subtotal_amount+= total_sub_total;
                    }
                    else if(val.discount_type =="free_quantity"){

                        sell_table_html += '<td>'+val.quantity*val.selling_price+'</td>';
                        subtotal_amount+= val.quantity*val.selling_price;
                    }
                    sell_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
                    // sell_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a>';
                    // sell_table_html += '</td>';
                    sell_table_html += '</tr>';
                    total_amount = subtotal_amount;
                });

                render_data['all_basic_data']['net_payable'] = total_amount;
                render_data['all_basic_data']['due'] = total_amount;

                $('#sell_table2 tbody').html(sell_table_html);
                $('#total_amount').html(total_amount);
                $('#total_net_payable').html(total_amount);
                $('#total_due').html(total_amount);
                $('#total_new_amount').html(total_amount);
                var new_payable = parseFloat($('#total_new_amount').html()) - parseFloat($('#total_amount_new').html());
                $('#total_payable_with_returned').html(new_payable);
                var paid = 0;
                paid += $('#total_paid').html();
                $('#total_difference').html(parseFloat(new_payable)-parseFloat(paid));
            }

            render_data['all_basic_data']['discount']=0;
            render_data['all_basic_data']['net_pay_after_discount']=0;

            $('#total_discount').keyup(function(event) {
                var net_pay_after_discount = parseFloat(total_amount) - parseFloat($( this ).val());
                $('#total_net_payable').text(net_pay_after_discount);
                $('#total_due').text(net_pay_after_discount);
                render_data['all_basic_data']['discount']=parseFloat($(this).val());
                render_data['all_basic_data']['net_pay_after_discount']=net_pay_after_discount;
            });

            var current_cashbox_amount = <?php echo $cashbox_amount['total_cashbox_amount']?>;
            $('#save_sell').click(function(event)
            {


                event.preventDefault();
                voucher_id = $('input[name="voucher_no"]').val();
                var post_data ={
                    'voucher_id' : voucher_id,
                    'returned_items' : render_new_data['item_info_new'],
                    csrf_test_name: csrf
                }; 
                $.post('<?php echo site_url() ?>/Return_money/save_sell_info',post_data ).done(function(datar) 
                {
                    $('#final_calculation').show();
                    render_new_data['item_info_new'].length = 0;
                    $("#buy_table > tbody").html("");
                    $('#returned_item_form').hide();
                    if(datar == "success")
                    {
                     render_new_data.length = 0;
                     $("#return_money_div").hide();
                     $("#buy_table > tbody").html("");
                     $('#returned_item_form').hide();
                     alert ("<?php echo $this->lang->line('return_success'); ?>");
                     $("#sell_voucher_details_form").hide();
                     $('input[name="sell_voucher_no"]').val('');
                     $('input[name="voucher_no"]').val('');
                     $('input[name="grand_total"]').val('');
                     $('input[name="paid_new"]').val('');
                     $('input[name="due"]').val('');
                     $('input[name="total_due"]').val('');
                     $('input[name="return_money"]').val('');
                     $("#table_items > tbody").html("");
                     $("#total_exchanged").html("");
                 }    
                 else if(datar == "no return")
                 {
                    alert("<?php echo $this->lang->line('no_return_amount'); ?>");
                    $("#sell_voucher_details_form").hide();
                    $('input[name="sell_voucher_no"]').val('');
                    return false;
                } 
                else if(datar == "No items returned")
                {
                    alert("<?php echo $this->lang->line('no_return_amount'); ?>");
                    return false;
                } <?php  ?> 
                else if(datar =="insufficient amount on stock")
                {
                    alert("Some item's returned quantity exceeds inventory!! Please make partial return");
                    return false;
                }
                else if(datar == "Please Due First.Money return is not possible now")
                {
                    alert("<?php echo $this->lang->line('due_first'); ?>");
                    return false;
                }  
            }).error(function() {
                alert("দুঃখিত!!");
            });
        });

            $('#exchange_customers_form').submit(function(event) {
                event.preventDefault();
                var data = {};
                data.sell_voucher_no = $('input[name=sell_voucher_no]').val();
                data.csrf_test_name = $('input[name=csrf_test_name]').val();
                render_new_data['item_info_new'].length = 0;
                $("#buy_table > tbody").html("");
                $('#returned_item_form').hide();
                $.post('<?php echo site_url() ?>/Return_money/get_voucher_details_info',data).done(function(data1, textStatus, xhr) {
                    var data2 = $.parseJSON(data1);
                    console.log(data2);

                    if(data2.success==0)
                    {
                        alert("<?php echo $this->lang->line('give_voucher'); ?>");
                    }
                    if(data2.all_sell_info_by_id.length == 0)
                    {
                        alert("<?php echo $this->lang->line('no_voucher'); ?>");
                        return false;
                    }

                    if(data2.all_sell_info_by_id[0].paid==0)
                    {
                        alert("<?php echo $this->lang->line('no_return_amount'); ?>");
                    }

                    if(data2 == "No permission")
                    {
                        $('#not_permitted').slideDown();
                        setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                    }

                    else if(data2.success==0)
                    {
                        $.each(data2.error, function(index, val) {
                            $('input[name='+index+']').parent().parent().addClass('has-error');
                            $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                        });
                    }
                    else
                    {
                        $('#sell_voucher_details_form').show(400);
                        $('input[name = voucher_no]').val(data2.all_sell_info_by_id[0].sell_local_voucher_no);
                        $('input[name = net_payable]').val(data2.all_sell_info_by_id[0].net_payable);
                        $('input[name = grand_total]').val(data2.all_sell_info_by_id[0].grand_total);
                        $('input[name = discount]').val(data2.all_sell_info_by_id[0].discount);
                        $('input[name = paid_new]').val(data2.all_sell_info_by_id[0].paid);
                        $('input[name = due]').val(data2.all_sell_info_by_id[0].due);

                        if(data2.all_sell_info_by_id[0].discount_percentage=="yes")
                        {
                            discount_in_amount = Number(parseFloat(data2.all_sell_info_by_id[0].discount/100)*(data2.all_sell_info_by_id[0].grand_total)).toFixed(2);
                            $('input[name = total_discount]').val(discount_in_amount);
                        }
                        else
                        {
                            $('input[name = total_discount]').val(data2.all_sell_info_by_id[0].discount);
                        }




                        $('input[name = sell_details_id]').val(data2.all_sell_info_by_id[0].sell_details_id);
                        $('input[name = return_money]').val(0);
                        // if(data2.all_sell_info_by_id[0].due>0)
                        // {
                        //     $('input[name = return_money]').val(data2.all_sell_info_by_id[0].paid);
                        // }
                        // else
                        // {
                        //     $('input[name = return_money]').val(data2.all_sell_info_by_id[0].paid);
                        // }

                        $('input[name = total_due]').val(data2.all_sell_info_by_id[0].due);
                        
                        var table_html = '';
                        var discount_in_amount = 0;
                        $.each(data2.all_sell_info_by_id, function(index, val) {
                            table_html += '<tr>'
                            table_html += '<td id="item_name_"'+val.item_spec_set_id+'>'
                            table_html += val.spec_set;
                            table_html += '</td><td id="item_quantity_"'+val.item_spec_set_id+'>';
                            table_html += val.quantity;
                            table_html += '</td><td>';
                            table_html += val.selling_price;

                            if(val.discount_type=="amount")
                            {
                                table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                                table_html += val.discount_amount;
                            }
                            else if(val.discount_type=="percentage")
                            {
                                discount_in_amount = Number(parseFloat(val.discount_amount/100)*(val.quantity*val.selling_price)).toFixed(2);
                                table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                                table_html += discount_in_amount;
                            }
                            else
                            {
                                table_html += '</td><td id="selling_price_"'+val.item_spec_set_id+'>';
                                table_html += val.discount_amount;
                            }
                            table_html += '</td><td>';
                            table_html += val.sub_total;
                            table_html += '</td>';

                            table_html += '</td><td style="display:none">';
                            table_html += val.quantity;
                            table_html += '</td><td>';
                            table_html += val.imei_barcode?val.imei_barcode:"";
                            table_html += '</td><td>';

                            // table_html += '<td><input type="text" style="border:1px solid #45b6af" class="form-control return_quantity_customer" name="new_amount" id="returned_items_'+val.item_spec_set_id+'"></td>';
                            // table_html += '</tr>'


                            table_html += '<input style="border:1px solid #45b6af !important" type="text" name="new_amount" class="form-control return_quantity_customer">';
                            table_html += '</td>';
                            table_html += '<td><button style="border:1px solid #45b6af !important" type="button" class="form-control ok_button" id="returned_items_'+val.item_spec_set_id+'">Ok</button></td>';
                            table_html += '</tr>';
                        });

                        $('#table_items tbody').html(table_html);
                    }

                }).error(function() {

                    $('#insert_failure').slideDown();
                    setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
                });    
            });    

var total_amount = 0;
var total_net_payable= 0;

function buy_table_new_render (){
    var buy_table_new_html = '';
    total_amount = 0;

    var subtotal_amount = 0;

    $.each(render_new_data['item_info_new'], function(index, val) {
        subtotal_amount+= val.quantity*val.selling_price;
        buy_table_new_html += '<tr id="'+index+'">';
        buy_table_new_html += '<td>'+val.item_name+'</td>';
        buy_table_new_html += '<td>'+val.quantity+'</td>';
        buy_table_new_html += '<td>'+val.selling_price+'</td>';
        buy_table_new_html += '<td>'+val.quantity*val.selling_price+'</td>';
        buy_table_new_html += '<td>'+Number(parseFloat(val.quantity*val.selling_price_final)).toFixed(2)+'</td>';
        buy_table_new_html += '<td>'+val.item_imei_number || ""+'</td>';
        //buy_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
        // buy_table_new_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a>';
        // buy_table_new_html += '</td>';
        buy_table_new_html += '</tr>';
        total_amount = subtotal_amount;
    });

    render_new_data['all_basic_data']['net_payable']=total_amount;
    render_new_data['all_basic_data']['due']=total_amount;

    $('#buy_table tbody').html(buy_table_new_html);
    $('#total_exchanged').html(subtotal_amount);
    $('#total_net_payable_new').html(total_amount);
    $('#total_due_new').html(total_amount);


    $('#save_sell_new').click(function(event) {
        var net_amount =0;
        var return_payment = render_data['all_basic_data']['net_payable'];
        var new_payment = render_new_data['all_basic_data']['net_payable'];
        net_amount = (return_payment) -(new_payment);
        $('#return_payment').html(return_payment);
        $('#new_payment').html(new_payment);
        $('#new_net_amount').html(net_amount);

    });
}

clearTimeout(timer);
timer = setTimeout(function() 
{

    $('table').on('click', '.ok_button',function(event) {
        $("#returned_item_form").show();
        updated_buy_item_id = $(this).attr('id').split('_')[2];
        updated_item_name = $(this).parent().parent().find("td:nth-child(1)").text();
        current_item_quantity = $(this).parent().parent().find("td:nth-child(2)").text();
        item_quantity_left = $(this).parent().parent().find("td:nth-child(6)").text();/*********For Checking quantity validation ..hidden.************/
        item_imei_number = $(this).parent().parent().find("td:nth-child(7)").text();

        updated_item_retail_price = $(this).parent().parent().find("td:nth-child(3)").text();
    //item_individual_discont = $(this).parent().parent().find("td:nth-child(4)").text();
    item_subtotal = $(this).parent().parent().find("td:nth-child(5)").text();

    updated_item_quantity = $(this).parent().parent().find("input:first").val();

    current_total = $('input[name=net_payable]').val();
    total_paid = $('input[name=paid_new]').val();
    total_receivable = $('input[name=due]').val();
    total_due = $('input[name=total_due]').val();
    total_return = $('input[name=return_money]').val();
    $("#return_money_div").show();
    //returned_items_price = (updated_item_quantity*updated_item_retail_price);


    /***********Calculation of Returned item price with total discount and partial item discount Starts**************************/

    var grand_total = $("#grand_total").val();
    var total_discount = $("#total_discount").val() ||0;



    var unit_price_with_total_discount = Number(total_discount/grand_total).toFixed(5)||0;
    
    var sub_total_after_total_discount = Number(parseFloat(item_subtotal -parseFloat(item_subtotal*unit_price_with_total_discount))).toFixed(2) ||0;

    var item_unit_price_after_total_individual_discount = parseFloat(sub_total_after_total_discount/current_item_quantity) ||0;

    returned_items_price = Number(parseFloat(item_unit_price_after_total_individual_discount*updated_item_quantity)).toFixed(2) ||0;

    // alert(returned_items_price);exit;
    // alert(total_due);

    /***********Calculation of Returned item price with total discount and partial item discount Ends**************************/

    if(updated_item_quantity=="" || 0)
    {
        return false;
    }

    if (parseFloat(updated_item_quantity)>parseFloat(item_quantity_left)) 
    {
        alert("<?php echo $this->lang->line('return_quantity_exceded'); ?>");    
        // $(this).parent().parent().find("input:first").val('');
        return false;
    }
    else
    {

        if(parseFloat(returned_items_price)>parseFloat(total_due))
        {
            // alert(item_unit_price_after_total_individual_discount);
            // alert(returned_items_price);

            $('input[name=total_due]').val(0);   
            $('input[name=net_payable]').val(Number(parseFloat(current_total)-(updated_item_quantity*item_unit_price_after_total_individual_discount)).toFixed(2));
            $('input[name=paid_new]').val(Number(parseFloat(total_paid)+parseFloat(returned_items_price-total_due)).toFixed(2));
            if(total_due==0)
            {
                $('input[name=paid_new]').val(Number(parseFloat(total_paid)-parseFloat(returned_items_price)).toFixed(2));
                $('input[name=return_money]').val(Number(parseFloat(total_return)+parseFloat(returned_items_price)).toFixed(2));
            }
            else
            {
                $('input[name=paid_new]').val(Number(parseFloat(total_paid)-parseFloat(returned_items_price-total_due)).toFixed(2));
                $('input[name=return_money]').val(Number(parseFloat(total_return) + parseFloat(returned_items_price-total_due)).toFixed(2));
            }
        }
        else if(parseFloat(total_due)>= parseFloat(returned_items_price))
        {
            $('input[name=total_due]').val(Number(total_due-returned_items_price).toFixed(2));
            $('input[name=net_payable]').val(Number(parseFloat(current_total)-(updated_item_quantity*item_unit_price_after_total_individual_discount)).toFixed(2));
            $('input[name=return_money]').val(0);
        }

        $(this).parent().parent().find("td:nth-child(6)").text(item_quantity_left-updated_item_quantity);
        // $(this).parent().parent().find("input:first").val('');
        var item_spec_set_id = updated_buy_item_id;
        var exchanged_quantity = updated_item_quantity;
        var voucher_no = $('input[name=voucher_no]').val();
        var post_data ={
            'item_spec_set_id' : item_spec_set_id,
            'voucher_no' : voucher_no,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        var found = false;
        $.post('<?php echo site_url();?>/Exchange_with_customers/check_buy_cart_items/'+item_spec_set_id,post_data).done(function(data) {
            var data2 = $.parseJSON(data);
            // console.log(data2);exit;
            // if(parseFloat(data2)>=parseFloat(exchanged_quantity))
            // {
                $.each(render_new_data['item_info_new'], function(index, val) {
                    if(!val.item_imei_number)
                    {
                        if(val.item_spec_set_id == item_spec_set_id)
                        {
                            render_new_data['item_info_new'][index]['quantity'] =parseFloat(render_new_data['item_info_new'][index]['quantity']) + parseInt(updated_item_quantity);
                            found = true;
                        }
                    }
                });
                if(found == true)
                {

                }
                else
                {
                    render_new_data['item_info_new'].push({
                        'item_spec_set_id' : updated_buy_item_id,
                        'item_name' : updated_item_name,
                        'quantity' : updated_item_quantity,
                        'selling_price' : updated_item_retail_price,
                        'selling_price_final' : item_unit_price_after_total_individual_discount,
                        'item_imei_number' : item_imei_number?item_imei_number:"",
                    })
                }

                $("#table_items").find("input:first").val('');
                buy_table_new_render();
            // }
            // else
            // {
            //     alert("<?php echo $this->lang->line('return_quantity_exceded'); ?>");
            //     $("#table_items").find("input:first").val('');
            // }
        }).error(function() {
            alert("<?php echo $this->lang->line('sorry'); ?>");
            $("#table_items").find("input:first").val('');                
        });    
    }
});

}, 500);

$('#buy_table').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    render_new_data['item_info_new'].splice(clicked_id,1);
    buy_table_new_render();
});

$('#save_sell_new').click(function(event) {

    var net_amount =0;
    var return_payment = render_data['all_basic_data']['net_payable'];
    var new_payment = render_new_data['all_basic_data']['net_payable'];
    net_amount = (return_payment) -(new_payment);
    $('#return_payment').html(return_payment);
    $('#new_payment').html(new_payment);
    $('#new_net_amount').html(net_amount);

});


$('#item_select').change(function(event) 
{
    //alert($(this).val());
    var item_spec_set_id = $(this).val();


    var post_data ={
        'item_spec_set_id' : $(this).val(),
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/Exchange_with_customers/find_item_price/'+item_spec_set_id,post_data).done(function(data) {
        var data2 = $.parseJSON(data);
        $('input[name="retail_price"]').val(data2.retail_price);

    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});
</script>

