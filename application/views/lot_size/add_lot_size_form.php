<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->


<div class="row">
    <div class="col-md-6">
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failed'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('insert_succeded'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_succeded'); ?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>                
            </div>
            <div class="portlet-body form" id="lot_size_insert_div">
                <!-- BEGIN FORM-->
                
                <?php $form_attrib = array('id' => 'lot_size_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('lot_size_name'); ?> *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="lot_size_name" placeholder="<?php echo $this->lang->line('lot_size_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green"><?php echo $this->lang->line('save'); ?></button>
                    <button type="reset" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="lot_size_edit_div" style="display:none">

                <?php $form_attrib = array('id' => 'lot_size_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <input type="text" class="form-control" name="lot_size_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('lot_size_name'); ?>*</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="lot_size_name_edit" placeholder="<?php echo $this->lang->line('lot_size_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green"><?php echo $this->lang->line('save'); ?></button>
                    <button type="button" class="btn default" id="edit_lot_size_cancle"><?php echo $this->lang->line('cancle'); ?></button>
                </div>
                <?php echo form_close();?>

            </div>

        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i><?php echo $this->lang->line('right_portlet_title'); ?> 
                </div>
                
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('lot_size_name'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('edit'); ?> / <?php echo $this->lang->line('delete'); ?>
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
</div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    // table_lot_size = $('#sample_2').DataTable();        
    var table_lot_size = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/lot_size/all_lot_size_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ]
    });

</script>

<script type="text/javascript">


    $('table').on('click', '.edit_lot_size',function(event) {
        event.preventDefault();

        $('.has-error').removeClass('has-error');
        $('.help-block').text('');

        var lot_size_id = $(this).attr('id').split('_')[1];
        $('#current_lot_size_id').remove();

        $.get('<?php echo site_url();?>/Lot_size/get_lot_size_info/'+lot_size_id).done(function(data) {

            var lot_size_data = $.parseJSON(data);
            if(lot_size_data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
            $('#form_header_text').text("লট সাইজের তথ্য পরিবর্তন করুন");
            $('input[name = lot_size_name_edit]').val(lot_size_data.lot_size_info_by_id.lot_size_name);
            $('#lot_size_insert_div').slideUp('fast');
            $('#lot_size_edit_div').slideDown('slow');
            $('input[name=lot_size_id]').val(lot_size_id);

        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

    $('#lot_size_edit_form').submit(function(event) {
        event.preventDefault();
        var post_data={};
        post_data.lot_size_name_edit = $('input[name = lot_size_name_edit]').val();
        post_data.lot_size_id = $('input[name= lot_size_id]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/Lot_size/update_lot_size_info',post_data).done(function(data, textStatus, xhr) {

            var data2 = $.parseJSON(data);

                // if(data=="success")
                if(data2['success'])
                {
                    $('#update_success').slideDown();
                    setTimeout( function(){$('#update_success').slideUp()}, 3000 );
                    var row_data = table_lot_size.row( $('#edit_'+post_data.lot_size_id).closest('tr')[0] ).data();
                    row_data[0] = post_data.lot_size_name_edit;
                    table_lot_size.row( $('#edit_'+post_data.lot_size_id).closest('tr')[0] ).data(row_data).draw();
                    $('#lot_size_insert_div').slideDown('slow');
                    $('#lot_size_edit_div').slideUp('fast');
                    $('#form_header_text').text("নতুন সাইজ যোগ করুন");
                }
                else if(!Object.keys(data2.error).length)
                {
                    alert("Lot size update failed");                
                }
                else
                {
                    $.each(data2.error, function(index, val) {
                        $('input[name='+index+']').parent().parent().addClass('has-error');
                        $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    });
                }
            }).error(function() {

                alert("Connection failure.");
            });

        });

$('#edit_lot_size_cancle').click(function(event) {
    event.preventDefault();
    $('#lot_size_insert_div').slideDown('slow');
    $('#lot_size_edit_div').slideUp('fast');
    $('#form_header_text').text("নতুন সাইজ যোগ করুন");

});
</script>
<script type="text/javascript">

    var lot_size_id = '' ;
    $('table').on('click', '.delete_lot_size',function(event) {
        event.preventDefault();
        lot_size_id = $(this).attr('id').split('_')[1];
    });

    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        //var lot_size_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'lot_size_id' : lot_size_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Lot_size/delete_lot_size/'+lot_size_id,post_data).done(function(data) {
            if(data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                $('#responsive_modal_delete').modal('hide');
            }
            else if(data = "success")
            {
                // alert("Successfully Deleted");
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                $('#responsive_modal_delete').modal('hide');
                table_lot_size.row($('#delete_'+lot_size_id).parents('tr')).remove().draw();
            }
            else
            {
                // alert("An error has occured. pLease try again");        
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );        
            }


        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });
// ajax form submit starts....

$('#lot_size_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.lot_size_name = $('input[name=lot_size_name]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/Lot_size/save_lot_size_info',data).done(function(data1, textStatus, xhr) {

        var data2 = $.parseJSON(data1);

        if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }

        else{

            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();

            $('input[name=lot_size_name]').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table_lot_size.row.add({
                "0":       data.lot_size_name,
                "1":   "<a class='btn btn-primary edit_lot_size' id='edit_"+data2.data+"' >    পরিবর্তন </a><a class='btn btn-danger delete_lot_size' id='delete_"+data2.data+"'>ডিলিট </a>"
            }).draw();
        }

    }).error(function() {

        $('#insert_failure').slideDown();

        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
});

// ajax form submit end.......
</script>