<!DOCTYPE html>
<!-- 

License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>pos2in</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo CURRENT_ASSET;?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <!-- <link href="<?php echo CURRENT_ASSET;?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo CURRENT_ASSET;?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>


    <script type="text/javascript">
        document.onkeydown = function(event) {
            if(event.keyCode == 123) {
                return false;
            }
            if(event.ctrlKey && event.shiftKey && event.keyCode == 'I'.charCodeAt(0)){
                return false;
            }
            if(event.ctrlKey && event.shiftKey && event.keyCode == 'J'.charCodeAt(0)){
                return false;
            }
            if(event.ctrlKey && event.shiftKey && event.keyCode == 'S'.charCodeAt(0)){
                return false;
            }
            if(event.ctrlKey && event.keyCode == 'U'.charCodeAt(0)){
                return false;
            }
            if(event.ctrlKey && event.keyCode == 'S'.charCodeAt(0)){
                return false;
            }
        };
    </script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- <body oncontextmenu="return false;" class="login"> -->
<body  class="login">

    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="menu-toggler sidebar-toggler">
    </div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="index.html">
            <!-- <img src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/img/logo-big.png" alt=""/> -->
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <?php 
        $attributes = array(
            'id' => 'login_form', 
            'class' => 'login-form'
            );
        $hidden = array();
        echo form_open('Login/login', $attributes, $hidden); 
        ?>
        <h3 class="form-title"><?php echo $this->lang->line('sign_in_title'); ?></h3>
        

        <div class="alert alert-danger" id="empty_pass" style="display:none" role="alert"><?php echo $this->lang->line('empty_alert'); ?></div>
        <div class="alert alert-danger" id="invalid_pass" style="display:none" role="alert"><?php echo $this->lang->line('invalid_alert'); ?></div>
        <div class="alert alert-danger" id="no_net" style="display:none" role="alert"><?php echo $this->lang->line('net_alert'); ?></div>
        <div class="alert alert-danger" id="no_mac" style="display:none" role="alert"><?php echo $this->lang->line('mac_alert'); ?></div>
        <div class="alert alert-success" id="eng_alert" style="display:none" role="alert">Language english selected</div>
        <div class="alert alert-success" id="bang_alert" style="display:none" role="alert">বাংলা ভাষা নির্বাচিত হয়েছে</div>


        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('email_place_holder'); ?></label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php echo $this->lang->line('email_place_holder'); ?>" name="email"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('password_place_holder'); ?></label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo $this->lang->line('password_place_holder'); ?>" name="password"/>
        </div>
        <div class="form-actions">
            <button type="submit" id="login_button" class="btn btn-success uppercase"><?php echo $this->lang->line('login_button'); ?></button>
            <!-- <label class="rememberme check">
            <input type="checkbox" name="remember" value="1"/><?php echo $this->lang->line('remember_check'); ?> </label> -->
            <a href="javascript:;" id="forget-password" class="forget-password"><?php echo $this->lang->line('forgot_password_link'); ?></a>
        </div>


        <?php echo form_close(); ?>

        <?php 
        $attributes = array(
            'id' => 'forget-form', 
            'class' => 'forget-form'
            );
        $hidden = array();
        echo form_open('', $attributes, $hidden); 
        ?>


        <h3><?php echo $this->lang->line('forgot_password_title'); ?></h3><br>
        
        <div class="form-group">
            <div class="clearfix" id="decission_buttons_div">
                <div class="col-md-6">
                    <button type="button" id="recover_by_email_button" class="btn green"><?php echo $this->lang->line('recover_email_button'); ?></button>
                </div>
        <!--         <div class="col-md-6">
                    <button type="button" id="recover_by_phone_button" class="btn green"><?php echo $this->lang->line('recover_phone_button'); ?></button>
                </div> -->
            </div>
            <div class="clearfix">
                <div class="col-md-12">
                    <center>
                        <input class="form-control placeholder-no-fix" type="text" style="display:none" autocomplete="off" id="email" placeholder="<?php echo $this->lang->line('forget_email_placeholder'); ?>" name="email"/>
                        <input class="form-control placeholder-no-fix" type="text" style="display:none" autocomplete="off" id="recover_by_phone" placeholder="<?php echo $this->lang->line('forget_phone_placeholder'); ?>" name="recover_by_phone"/>
                    </center>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <center>
                <button type="button" id="back-btn" class="btn btn-default"><?php echo $this->lang->line('back_button'); ?></button>
                <button type="submit" id="forget_pass_submit" style="display:none" class="btn btn-success "><?php echo $this->lang->line('submit_button'); ?></button>
            </center>
        </div>


        <?php echo form_close(); ?>


        <!-- END REGISTRATION FORM -->
    </div>
    
    <div class="copyright">
        <a href="<?php echo site_url();?>/Login/changeLangEng" id="lang_eng"><span>English</span> </a>&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url();?>/Login/changeLangBang" id="lang_bang"><span>বাংলা</span> </a>
    </div>

    <div class="copyright">
        v 1.0.1<br>
        pos2in © 2017 
    </div>

    <!-- END LOGIN -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {     
Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init();

jQuery('#forget-password').click(function() {
    jQuery('.login-form').hide();
    jQuery('.forget-form').show();
    $('#forget_pass_submit').hide();
    $('#forget_pass_submit').attr("disabled", false);
    $('#recover_by_email_button').hide();
    $('#email').show('slow');
    $('#forget_pass_submit').show('slow');
});


});
</script>
<!-- END JAVASCRIPTS -->

</body>

</html>

<script type="text/javascript">
    $('#recover_by_email_button').click(function(event) {
        $('#decission_buttons_div').hide();
        $('#recover_by_phone').hide();    
        $('#email').show('slow');
        $('#forget_pass_submit').show('slow');
    });

    $('#recover_by_phone_button').click(function(event) {
        $('#decission_buttons_div').hide();
        $('#email').hide();
        $('#recover_by_phone').show('slow');
        $('#forget_pass_submit').show('slow');
    });


    $(document).on('submit', '#forget-form', function(event) {
        event.preventDefault();
        dataString = $("#forget-form").serialize();
        $.ajax({
            type:"POST",
            url:"<?php echo site_url();?>/Login/checkMailForForgetPass",
            data:dataString,
            success:function (data) {
                if(data == 1){
                    $('#forget_pass_submit').attr("disabled", true);
                    alert("<?php echo $this->lang->line('check_email'); ?>");
                    $('#wait').hide();
                }else if(data == "fail"){
                    alert("<?php echo $this->lang->line('email_not_found'); ?>");
                }else if(data == "empty mail"){
                    alert("<?php echo $this->lang->line('empty_email'); ?>");
                    $('#forget_pass_submit').attr("enabled", true);
                }else if(data == "No net"){
                    alert("<?php echo $this->lang->line('no_net'); ?>");
                }else if(data == "Phone number does not exist"){
                    alert("Phone number not found in record");
                }
            }
        });
    });

    $('#back-btn').click(function(event) {
        $('#email').hide();
        $('#recover_by_phone').hide();    
        $('#decission_buttons_div').show('slow');

        $('#forget-form').hide();
        $('#login_form').show('200');
    });


    $('#login_form').submit(function (event) {
        dataString = $("#login_form").serialize();
        $.ajax({
            type:"POST",
            url:"<?php echo site_url();?>/Login/login",
            data:dataString,

            success:function (data) {

                if(data == 1){
                    window.location = "<?php echo site_url();?>/Sell";
                    return false;
                }

                else if(data == 2){
                    window.location = "<?php echo site_url();?>/Dashboard";
                    return false;
                }

                else if(data == "Invalid email or password"){
                    $('#invalid_pass').slideDown();
                    setTimeout( function(){$('#invalid_pass').slideUp()}, 1500 );    
                }
                else if(data == "Input fields can't be empty"){
                    $('#empty_pass').slideDown();
                    setTimeout( function(){$('#empty_pass').slideUp()}, 1500 );    
                }
                else if(data == "Internet connection required for first time log in"){
                    $('#no_net').slideDown();
                    setTimeout( function(){$('#no_net').slideUp()}, 1500 );    
                }
                else if(data == "Sorry something went wrong"){
                    $('#no_mac').slideDown();
                    setTimeout( function(){$('#no_mac').slideUp()}, 1500 );    
                }

            }
        });
        event.preventDefault();
    });

// $('#lang_eng').click(function(event) {
// 	$('#eng_alert').slideDown();
// 	setTimeout( function(){$('#eng_alert').slideUp()}, 1500 );	

// 	$.ajax({
// 		url: "<?php echo site_url();?>/Login/changeLangEng",
// 		// event.preventDefault();
// 		// success: location.reload();
// 	});

// });

// 


</script>