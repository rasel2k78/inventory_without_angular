	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption left">
				<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
            </div>

        </div>
        <div class="portlet-body form" id="spec_name_insert_div">
            <!-- BEGIN FORM-->
    <?php $form_attrib = array('id' => 'spec_name_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>

            <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="spec_name" placeholder="<?php echo $this->lang->line('left_name_placeholder');?>">
                        <span class="help-block"></span></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_description');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="spec_description" placeholder="<?php echo $this->lang->line('left_description_placeholder');?>">
                        </div>
                    </div>
                    <!--     <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('right_spec_default_type'); ?></label>
                            <div class="radio-list">
                                <label class="radio-inline">
                                    <input type="radio" name="spec_default_status" id="optionsRadios4" value="auto_selected" ><?php echo $this->lang->line('right_default_status_selected'); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="spec_default_status" id="optionsRadios5" value="not_selected" checked><?php echo $this->lang->line('right_default_status_not_selected'); ?>
                                </label>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn default pull-right" id="spec_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                        <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                        
                    </div>
        <?php echo form_close();?>
                    <!-- END FORM-->
                </div>

            </div>

            <script type="text/javascript">
                
                $('#spec_name_form').on('click', '#spec_cancel', function(event) {
                    event.preventDefault();
                    $('#specificationForm').modal('hide');
                    $('input[name= spec_name]').val('');
                    $('input[name= spec_description]').val('');
                    
                    /* Act on the event */
                });
            </script>