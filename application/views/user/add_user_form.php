<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-6">

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?> </span></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failure'); ?> </span></div>
        <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?> </span></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?> </span></div>


        <div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?> </span></div>
        <div class="alert alert-danger" id="no_mobile" style="display:none" role="alert"><?php echo $this->lang->line('no_mobile'); ?> </span></div>
        <div class="alert alert-danger" id="no_password" style="display:none" role="alert"><?php echo $this->lang->line('no_password'); ?> </span></div>
        <div class="alert alert-danger" id="no_match" style="display:none" role="alert"><?php echo $this->lang->line('no_match'); ?> </span></div>
        <div class="alert alert-danger" id="no_email" style="display:none" role="alert"><?php echo $this->lang->line('no_email'); ?> </span></div>
        <div class="alert alert-danger" id="email_exist" style="display:none" role="alert"><?php echo $this->lang->line('email_exist'); ?> </span></div>
        <div class="alert alert-danger" id="username_exist" style="display:none" role="alert"><?php echo $this->lang->line('username_exist'); ?> </span></div>
        <div class="alert alert-danger" id="short_pass" style="display:none" role="alert"><?php echo $this->lang->line('short_pass'); ?> </span></div>
        <div class="alert alert-danger" id="no_permission_u_create" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_u_create'); ?> </span></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"> <?php echo $this->lang->line('header_text_left'); ?> </span>
                </div>
                
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'user_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/User/createAccount'),  $form_attrib, '');
                ?>

                <div class="form-body">
                    <div class="form-group ">

                        <label class="control-label col-md-3"><?php echo $this->lang->line('username'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            
                            <input type="text" class="form-control" name="username" placeholder="<?php echo $this->lang->line('username'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('user_role'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            
                            <select class="form-control" id="user_role" name="user_role" >
                                <option value="staff" class="form-control" ><?php echo $this->lang->line('staff'); ?></option>
                                <option value="manager" class="form-control" ><?php echo $this->lang->line('manager'); ?></option>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group pass_hide">
                        <label  class="control-label col-md-3"><?php echo $this->lang->line('password'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            
                            <input type="password" class="form-control"  name="password" placeholder="<?php echo $this->lang->line('password'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group pass_hide">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('repass'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            
                            <input type="password" class="form-control"  name="repass" placeholder="<?php echo $this->lang->line('repass'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('email'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            
                            <input type="email" id="email" class="form-control" name="email" placeholder="<?php echo $this->lang->line('email'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('phone'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            
                            <input type="number" class="form-control" name="phone" placeholder="<?php echo $this->lang->line('phone'); ?> ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('phone2'); ?></label>
                        <div class="col-md-9 ">
                            
                            <input type="number" class="form-control" name="phone2" placeholder="<?php echo $this->lang->line('phone2'); ?> ">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('present_address'); ?></label>
                        <div class="col-md-9 ">
                            
                            <input type="text" class="form-control" name="present_address"  placeholder="<?php echo $this->lang->line('present_address'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('permanent_address'); ?></label>
                        <div class="col-md-9 ">
                            
                            <input type="text" class="form-control" name="permanent_address"  placeholder="<?php echo $this->lang->line('permanent_address'); ?>">
                        </div>
                    </div>
                    <div id="image_of_user" class="form-group ">
                        <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('user_image'); ?> </label>
                        <div class="col-md-9">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div id="user_image" class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                </div>



                                <div>
                                    <span id="user_image_select" class="btn default btn-file">
                                        <span  class="fileinput-new">
                                            <?php echo $this->lang->line('user_image_select'); ?>
                                        </span>
                                        <span class="fileinput-exists">
                                            <?php echo $this->lang->line('change'); ?> 
                                        </span>
                                        <input type="file" name="user_image">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        <?php echo $this->lang->line('delete');?> 
                                    </a>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                    <input type="submit" value="<?php echo $this->lang->line('save'); ?>" name="image_upload" id="image_upload" class="btn green pull-right"/>
                    <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
                    <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="alert alert-success" id="delete_success_from_list" style="display:none" role="alert"><?php echo $this->lang->line('user_deleted'); ?></div>
    <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('user_not_deleted'); ?></div>
    <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_right'); ?></div>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-users"></i><?php echo $this->lang->line('all_user_list'); ?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->lang->line('name_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('mobile_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('edit_delete_th'); ?>
                        </th>

                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>
    <a class="btn btn-lg btn-success"  style="width:100%" href="<?php echo site_url();?>/User_access/"> 
        <h3><?php echo $this->lang->line('manage_u_acc'); ?></h3>
    </a>

</div>
</div>


<div id="responsive_modal" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete_user'); ?> <span id="user_name_selected" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('sure_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('no'); ?></button>
    <button type="button" id="user_delete_confirm_y" class="btn blue"><?php echo $this->lang->line('yes'); ?></button>
</div>
</div>

<?php //var_dump($this->cache->cache_info()); ?>
<!-- Time: <?php echo $this->benchmark->elapsed_time(); ?>
Mem: <?php echo $this->benchmark->memory_usage();
print_r($this->session->userdata()); ?> -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {    
        UIExtendedModals.init();
    });
</script>  

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>

//Code for data table start
var table_user = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/User/all_users_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for data table end


//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#user_form').ajaxForm({
    beforeSubmit: function() {    
        $('input[name = username]').closest('.form-group').removeClass('has-error');
        $('input[name = username]').siblings().text("");

        $('input[name = password]').closest('.form-group').removeClass('has-error');
        $('input[name = password]').siblings().text("");

        $('input[name = repass]').closest('.form-group').removeClass('has-error');
        $('input[name = repass]').siblings().text("");

        $('input[name = email]').closest('.form-group').removeClass('has-error');
        $('input[name = email]').siblings().text("");

        $('input[name = phone]').closest('.form-group').removeClass('has-error');
        $('input[name = phone]').siblings().text("");

        

    },
    success: function(msg) {
        if(msg == "Username not set")
        {
            $('input[name = username]').closest('.form-group').addClass('has-error');
            $('input[name = username]').siblings().text("<?php echo $this->lang->line('no_username'); ?>");
            // $('#no_username').slideDown();
            // setTimeout( function(){$('#no_username').slideUp()}, 1500 );
        }
        else if(msg == "Username already exists"){

            $('input[name = username]').closest('.form-group').addClass('has-error');
            $('input[name = username]').siblings().text("<?php echo $this->lang->line('username_exist'); ?>");
            // $('#username_exist').slideDown();
            // setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
        }
        else if(msg == "Email already exists"){
            $('input[name = email]').closest('.form-group').addClass('has-error');
            $('input[name = email]').siblings().text("<?php echo $this->lang->line('email_exist'); ?>");
            // $('#email_exist').slideDown();
            // setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
        }
        else if(msg == "Mobile no. 1 can not be Empty"){
            $('input[name = phone]').closest('.form-group').addClass('has-error');
            $('input[name = phone]').siblings().text("<?php echo $this->lang->line('no_mobile'); ?>");
            // $('#no_mobile').slideDown();
            // setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
        }
        else if(msg == "Email can not be empty"){
            $('input[name = email]').closest('.form-group').addClass('has-error');
            $('input[name = email]').siblings().text("<?php echo $this->lang->line('no_email'); ?>");
            // $('#no_email').slideDown();
            // setTimeout( function(){$('#no_email').slideUp()}, 1500 );
        }
        else if(msg == "Password can not be Empty"){
            $('input[name = password]').closest('.form-group').addClass('has-error');
            $('input[name = password]').siblings().text("<?php echo $this->lang->line('no_password'); ?>");
            // $('#no_password').slideDown();
            // setTimeout( function(){$('#no_password').slideUp()}, 1500 );
        }
        else if(msg == "Password did not match"){
            $('input[name = repass]').closest('.form-group').addClass('has-error');
            $('input[name = repass]').siblings().text("<?php echo $this->lang->line('no_match'); ?>");
            // $('#no_match').slideDown();
            // setTimeout( function(){$('#no_match').slideUp()}, 1500 );
        }
        else if(msg == "Password should atleast 4 charecters"){
            $('input[name = password]').closest('.form-group').addClass('has-error');
            $('input[name = password]').siblings().text("<?php echo $this->lang->line('short_pass'); ?>");
            // $('#short_pass').slideDown();
            // setTimeout( function(){$('#short_pass').slideUp()}, 1500 );
        }
        else if(msg == "success"){
            $('#user_insert_success').slideDown();
            setTimeout( function(){$('#user_insert_success').slideUp()}, 1500 );

            
            $('input[name = username]').val("");
            $('select[name= user_role]').val("");
            $('input[name = email]').val("");
            $('input[name = password]').val("");
            $('input[name = repass]').val("");
            $('input[name = phone]').val("");
            $('input[name = phone2]').val("");
            $('input[name = present_address]').val("");
            $('input[name = permanent_address]').val("");
            $('.fileinput-preview').html("<img src=''>");
        }
        else if(msg == "No permission"){
            $('#no_permission_u_create').slideDown();
            setTimeout( function(){$('#no_permission_u_create').slideUp()}, 1500 );
        }
    },
    complete: function(xhr) {
        table_user.ajax.reload(null, false);
        return false;
    }

}); 
//Ajax form submit end

</script>

<script type="text/javascript">

//Code for showing data on form for edit user start
var user_id ='';
$('table').on('click', '.edit_user',function(event) {
    console.log('1');
    event.preventDefault();
    console.log('2');

    user_id = $(this).attr('id').split('_')[1];
    $('#current_user_id').remove();

    $.get('<?php echo site_url();?>/User/get_user_info/'+user_id).done(function(data) 
    {
        console.log('3');


        var user_data = $.parseJSON(data);

        if(user_data == "No permission"){
            $('#no_permission').slideDown();
            setTimeout( function(){$('#no_permission').slideUp()}, 1500 );
        }else{

            var html = "<input id='current_user_id' type='hidden' class='form-control' name='user_id' value='"+user_id+"'>";
            $('#user_form').append(html);
            console.log('4');

            // $('#user_form').attr('action', "<?php echo site_url('/User/update_user_info');?>");
            //
            $('#user_form').attr('id', 'user_form_update');
            $('#form_header_text').text('<?php echo $this->lang->line('change_user_info'); ?>');
            $('#add_or_update').text('<?php echo $this->lang->line('infor_update'); ?>');
            $('#email').prop('disabled', true);
            $('.pass_hide').hide('slow');
            $('#user_image_select').hide('slow');
            $('#change_pic').hide('slow');
            $("#user_image").prop('disabled', false);
            $("#image_of_user_label").text('<?php echo $this->lang->line('change_user_image'); ?>');


            /*To set the value on input field after clicking on edit button for a particular user*/
            $('input[name = username]').val(user_data.user_info_by_id.username);
            $('select[name = user_role]').val(user_data.user_info_by_id.user_role);
            $('input[name = email]').val(user_data.user_info_by_id.email);
            $('input[name = phone]').val(user_data.user_info_by_id.phone_1);
            $('input[name = phone2]').val(user_data.user_info_by_id.phone_2);
            $('input[name = present_address]').val(user_data.user_info_by_id.present_address);
            $('input[name = permanent_address]').val(user_data.user_info_by_id.permanent_address);

            if(user_data.user_info_by_id.image!="" && user_data.user_info_by_id.image!=null)
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+user_data.user_info_by_id.image+"'>");
            }
            else
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/user_images/no_image.png'>");
            }

        }

    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
//Code for showing data on form for edit user end


//Code for clear the form on clicking cancel button start
$('#cancel_update').click(function(event) {
    $('input[name = username]').val("");
    $('select[name= user_role]').val("");
    $('input[name = email]').val("");
    $('input[name = password]').val("");
    $('input[name = repass]').val("");
    $('input[name = phone]').val("");
    $('input[name = phone2]').val("");
    $('input[name = present_address]').val("");
    $('input[name = permanent_address]').val("");
    $('.fileinput-preview').html("<img src=''>");
});
//Code for clear the form on clicking cancel button end



//Code for delete user with confirmation modal satrt
var user_id = '' ;
var user_name_selected = '';
$('table').on('click', '.delete_user',function(event) {
    event.preventDefault();
    user_id = $(this).attr('id').split('_')[1];
    // user_name_selected = "GUIHUIHU";
    user_name_selected = $(this).parent().parent().find('td:first').text();
    $('#user_name_selected').html(user_name_selected);
});


$('#responsive_modal').on('click', '#user_delete_confirm_y', function(event) {
    event.preventDefault();
    var post_data ={
        'user_id' : user_id
    }; 
    var something = $("input[name='csrf_test_name']").val();

    $.post('<?php echo site_url();?>/User/delete_user/'+user_id, {'data' : post_data, 'csrf_test_name':something}).done(function(data) {
        if(data == "Deletion success")
        {
            $('#delete_success_from_list').slideDown();
            setTimeout( function(){$('#delete_success_from_list').slideUp()}, 1500 );
            $('#responsive_modal').modal('hide');
            table_user.row($('#delete_'+user_id).parents('tr')).remove().draw();

            $('input[name = username]').val("");
            $('select[name= user_role]').val("");
            $('input[name = email]').val("");
            $('input[name = password]').val("");
            $('input[name = repass]').val("");
            $('input[name = phone]').val("");
            $('input[name = phone2]').val("");
            $('input[name = present_address]').val("");
            $('input[name = permanent_address]').val("");
            $('.fileinput-preview').html("<img src=''>");
        } else if(data == "No permission"){
            $('#responsive_modal').modal('hide');
            $('#no_permission').slideDown();
            setTimeout( function(){$('#no_permission').slideUp()}, 1500 );    
        }
        else
        {
            alert("An errorrrr has occured. pLease try again");                
        }

    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});
//Code for delete user with confirmation modal end


//Code for update user information start
$('.portlet-body.form').on('click', '#user_form_update input[type="submit"]', function(event){
    event.preventDefault();

    var data = {};
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    data.user_id = user_id;
    data.username = $('input[name = username]').val();
    data.user_role = $('select[name= user_role]').val();
    data.email = $('input[name = email]').val();
    data.phone = $('input[name = phone]').val();
    data.phone2 = $('input[name = phone2]').val();
    data.present_address = $('input[name = present_address]').val();
    data.permanent_address = $('input[name = permanent_address]').val();
    $.post("<?php echo site_url('/User/update_user_info');?>",data).done(function(data1, textStatus, xhr) {
        if(data1 == "user_info_updated"){
            table_user.ajax.reload(null, false);
            $('#update_success').slideDown();
            setTimeout( function(){$('#update_success').slideUp()}, 5000 );

            $('input[name = username]').closest('.form-group').removeClass('has-error');
            $('input[name = username]').siblings().text("");

            $('input[name = phone]').closest('.form-group').removeClass('has-error');
            $('input[name = phone]').siblings().text("");

            $('input[name = username]').val("");
            $('select[name= user_role]').val("");
            $('input[name = email]').val("");
            $('input[name = password]').val("");
            $('input[name = repass]').val("");
            $('input[name = phone]').val("");
            $('input[name = phone2]').val("");
            $('input[name = present_address]').val("");
            $('input[name = permanent_address]').val("");
            $('.fileinput-preview').html("<img src=''>");


            
        }

        else if(data1 == "Username not set")
        {
            $('input[name = username]').closest('.form-group').addClass('has-error');
            $('input[name = username]').siblings().text("<?php echo $this->lang->line('no_username'); ?>");

            
            // $('#no_username').slideDown();
            // setTimeout( function(){$('#no_username').slideUp()}, 1500 );
        }
        else if(data1 == "Username already exists"){
            $('input[name = username]').closest('.form-group').addClass('has-error');
            $('input[name = username]').siblings().text("<?php echo $this->lang->line('username_exist'); ?>");
            // $('#username_exist').slideDown();
            // setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
        }
        else if(data1 == "Email already exists"){
            // $('#email_exist').slideDown();
            // setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
        }
        else if(data1 == "Mobile no. 1 can not be Empty"){
            $('input[name = phone]').closest('.form-group').addClass('has-error');
            $('input[name = phone]').siblings().text("<?php echo $this->lang->line('no_mobile'); ?>");
            // $('#no_mobile').slideDown();
            // setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
        }
    }).error(function() {
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
    });    
});
//Code for update user information end

</script>
