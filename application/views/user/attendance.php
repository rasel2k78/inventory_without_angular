<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-ui/jquery-ui.min.css"/>

<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div id="attendance_entry_div" class="col-md-6">

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?> </span></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"></div>

        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?> </span></div>


        <div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?> </span></div>
        <div class="alert alert-danger" id="no_mobile" style="display:none" role="alert"><?php echo $this->lang->line('no_mobile'); ?> </span></div>
        <div class="alert alert-danger" id="no_password" style="display:none" role="alert"><?php echo $this->lang->line('no_password'); ?> </span></div>
        <div class="alert alert-danger" id="no_match" style="display:none" role="alert"><?php echo $this->lang->line('no_match'); ?> </span></div>
        <div class="alert alert-danger" id="no_email" style="display:none" role="alert"><?php echo $this->lang->line('no_email'); ?> </span></div>
        <div class="alert alert-danger" id="email_exist" style="display:none" role="alert"><?php echo $this->lang->line('email_exist'); ?> </span></div>
        <div class="alert alert-danger" id="username_exist" style="display:none" role="alert"><?php echo $this->lang->line('username_exist'); ?> </span></div>
        <div class="alert alert-danger" id="short_pass" style="display:none" role="alert"><?php echo $this->lang->line('short_pass'); ?> </span></div>
        <div class="alert alert-danger" id="no_permission_u_create" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_u_create'); ?> </span></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"> <?php echo $this->lang->line('box_header_left'); ?> </span>
                </div>
                
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'attendance_entry_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/Attendance/attendanceEntry'),  $form_attrib, '');
                ?>

                <div class="form-body">
                    <div class="form-group ">

                        <label class="control-label col-md-3"><?php echo $this->lang->line('employee_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input id="username" type="text" class="form-control" name="username" placeholder="<?php echo $this->lang->line('employee_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <input type="hidden" id="user_id" name="user_id" >

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('date'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            <input name="date" id="date_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('date');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group pass_hide">
                        <label  class="control-label col-md-3"><?php echo $this->lang->line('in_time'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input name="in_time" id="in_time_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('in_time');?>">                         
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group pass_hide">
                        <label  class="control-label col-md-3"><?php echo $this->lang->line('out_time'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input name="out_time" id="out_time_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('in_time');?>">                         
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('remark'); ?></label>
                        <div class="col-md-9 ">

                            <input type="text" id="remark" class="form-control" name="remark" placeholder="<?php echo $this->lang->line('remark'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    
                </div>
                <div class="form-actions">
                    <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                    <input type="submit" value="<?php echo $this->lang->line('save'); ?>" class="btn green pull-right"/>
                    <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
                    <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>







  <div id="attendance_edit_div" style="display: none;" class="col-md-6">

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"> Edit attendance </span>
                </div>
                
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'attendance_entry_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/Attendance/attendanceEntry'),  $form_attrib, '');
                ?>

                <div class="form-body">
                    <div class="form-group ">

                        <label class="control-label col-md-3"><?php echo $this->lang->line('employee_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input id="username" type="text" class="form-control" name="username" placeholder="<?php echo $this->lang->line('employee_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <input type="hidden" id="user_id" name="user_id" >

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('date'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">
                            <input name="date" id="date_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('date');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group pass_hide">
                        <label  class="control-label col-md-3"><?php echo $this->lang->line('in_time'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input name="in_time" id="in_time_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('in_time');?>">                         
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group pass_hide">
                        <label  class="control-label col-md-3"><?php echo $this->lang->line('out_time'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9 ">

                            <input name="out_time" id="out_time_picker" class="form-control" type="text" placeholder="<?php echo $this->lang->line('in_time');?>">                         
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('remark'); ?></label>
                        <div class="col-md-9 ">

                            <input type="text" id="remark" class="form-control" name="remark" placeholder="<?php echo $this->lang->line('remark'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    
                </div>
                <div class="form-actions">
                    <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                    <input type="submit" value="<?php echo $this->lang->line('save'); ?>" class="btn green pull-right"/>
                    <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
                    <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>






<div class="col-md-6">
    <div class="alert alert-success" id="delete_success_from_list" style="display:none" role="alert"><?php echo $this->lang->line('user_deleted'); ?></div>
    <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('user_not_deleted'); ?></div>
    <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_right'); ?></div>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-users"></i><?php echo $this->lang->line('box_header_right'); ?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->lang->line('name_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('date_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('in_time_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('out_time_th'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('action_th'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>
</div>

</div>



<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>



<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>

    $('#username').focus();
    $('#date_picker').datepicker({ dateFormat: 'dd-M-yy' });
    $('#date_picker').val($.datepicker.formatDate('dd-M-yy', new Date()));

    $('#in_time_picker').timepicker({
        defaultTime: 'current',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown'
    });

    $('#out_time_picker').timepicker({
        defaultTime: 'current',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown'
    });

    $( "#username" ).autocomplete({
        source: "<?php echo site_url('/');?>Attendance/getUserList",
        minLength: 1,
        select: function( event, ui ) {
            $('#user_id').val(ui.item.id);
        }
    });

//Code for data table start
var table_user = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/Attendance/all_users_attendance_info/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for data table end


//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#attendance_entry_form').ajaxForm({
    beforeSubmit: function() {    

    },
    success: function(msg) {

        if(msg == 'no_user_selected'){
            $('#insert_failure').html('Please select a user first');
            insertFailAlertShow();
        }else if(msg == 'date_required'){
            $('#insert_failure').html('Date required');
            insertFailAlertShow();
        }else if(msg == 'in_time_required'){
            $('#insert_failure').html('In time required');
            insertFailAlertShow();
        }else if(msg == 'out_time_required'){
            $('#insert_failure').html('Out time required');
            insertFailAlertShow();
        }else if(msg == 'duplicate_entry_for_today'){
            $('#insert_failure').html('Duplicate entry for today for this user');
            insertFailAlertShow();
        }else if(msg == 'success'){
            $('#insert_success').html('Data saved successfully');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
            $('#username').val('');
            $('#user_id').val('');
        }else if(msg == 'fail'){
            $('#insert_failure').html('Something went wrong');
            insertFailAlertShow();
        }else if(msg == 'invalid_user'){
            $('#insert_failure').html('Invalid user');
            insertFailAlertShow();
        }
    },
    complete: function(xhr) {
        table_user.ajax.reload();
        // return false;
    }

}); 
//Ajax form submit end

function insertFailAlertShow() {
    $('#insert_failure').slideDown();
    setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
}

$('table').on('click', '.edit_attendance',function(event) {
    $('#attendance_entry_div').hide();
    $('#attendance_edit_div').show('400');
});




</script>