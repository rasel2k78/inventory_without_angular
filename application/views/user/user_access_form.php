<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-5">

        <div class="alert alert-danger" id="user_not_selected" style="display:none" role="alert"><?php echo $this->lang->line('user_not_selected'); ?></div>
        <div class="alert alert-success" id="access_added" style="display:none" role="alert"><?php echo $this->lang->line('access_added'); ?></div>
        <div class="alert alert-danger" id="access_adding_fail" style="display:none" role="alert"><?php echo $this->lang->line('access_adding_fail'); ?></div>
        <div class="alert alert-success" id="access_remove_success" style="display:none" role="alert"><?php echo $this->lang->line('access_remove_success'); ?></div>
        <div class="alert alert-danger" id="access_remove_fail" style="display:none" role="alert"><?php echo $this->lang->line('access_remove_fail'); ?></div>
        <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission'); ?></div>

        <div class="alert alert-danger" id="owner_remove_fail" style="display:none" role="alert"><?php echo $this->lang->line('owner_remove_fail'); ?></div>



        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i><?php echo $this->lang->line('all_user'); ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered " id="sample_1">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('name'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('role'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('edit'); ?>
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>


    </div>

    <div class="col-md-7">



        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box red">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-users"></i><span id="header_text"><?php echo $this->lang->line('page_access'); ?></span>
                </div>

            </div>
            <div class="portlet-body">
                <?php
                $form_attrib = array('id' => 'user_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/User_access/set_user_access'),  $form_attrib, '');
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover access_table" id="table_2">
                            <thead>
                                <tr>
                                    <th class="col-md-3">
                                        <?php echo $this->lang->line('page_name'); ?>
                                    </th>
                                    <th class="col-md-2">

                                    </th>
                                    <th class="col-md-2">

                                    </th>
                                    <th class="col-md-2">

                                    </th>
                                    <th class="col-md-2">

                                    </th>
                                </tr>
                            </thead>

                            <!-- Code for show all check boxes at first time when page load__start -->
                            <tbody>
                                <?php 
                                $CI =& get_instance();

                                foreach ($page_list as $key => $page_title) {
                                    $page_count= $page_title['count(pages_group)'];
                                    echo '<tr>';
                                    $page_actions = $CI->show_all_check_box($page_title['pages_group']);
                                    //$page_actions = $CI->show_all_check_box($page_title['pages_group']);
                                    
                                    foreach ($page_actions as $k => $value) 
                                    {
                                        $pageId = $value['pages_id'];
                                        if($k==0) {
                                            echo '<td style="font-weight: bold;">'.$this->lang->line($value['pages_group']).'</td>';
                                        }
                                        echo '<td><input class="cb" type="checkbox" id="chkbx_'.$pageId.'" name="checkbox" value="'.$pageId.'"> '.$value['actions'].'</td>';
                                        // echo count($page_actions);
                                        if($k == (count($page_actions)-1)&&$k<4) {
                                            for($i=0;$i<(3-$k);$i++)
                                            {
                                                echo "<td></td>";
                                            }
                                        }
                                    }
                                    echo '</tr>';
                                }
                                ?>
                            </tbody>
                            <!-- Code for show all check boxes at first time when page load__end -->
                        </table>
                    </div>
                </div>
            </form>
        </div>

    </div>

    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>




<!-- <?php //var_dump($this->cache->cache_info()); ?>
Time: <?php echo $this->benchmark->elapsed_time(); ?>
Mem: <?php echo $this->benchmark->memory_usage(); ?> -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {    
        UIExtendedModals.init();
    });
</script>  

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>

//Code for user data table start
var table_user = $('#sample_1').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/User_access/all_users_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for user data table end




//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#user_form').ajaxForm({
    beforeSubmit: function() {    

    },
    success: function(msg) {
        if(msg == "Username not set")
        {
            $('#no_username').slideDown();
            setTimeout( function(){$('#no_username').slideUp()}, 1500 );
        }
        else if(msg == "Username already exists"){
            $('#username_exist').slideDown();
            setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
        }
        else if(msg == "Email already exists"){
            $('#email_exist').slideDown();
            setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
        }
        else if(msg == "Mobile no. 1 can not be Empty"){
            $('#no_mobile').slideDown();
            setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
        }
        else if(msg == "Password can not be Empty"){
            $('#no_password').slideDown();
            setTimeout( function(){$('#no_password').slideUp()}, 1500 );
        }
        else if(msg == "Password did not match"){
            $('#no_match').slideDown();
            setTimeout( function(){$('#no_match').slideUp()}, 1500 );
        }
        else if(msg == "success"){
            $('#user_insert_success').slideDown();
            setTimeout( function(){$('#user_insert_success').slideUp()}, 1500 );
        }
    },
    complete: function(xhr) {
        table_user.ajax.reload();
        return false;
    }

}); 
//Ajax form submit end

</script>

<script type="text/javascript">

//Code for showing checkbox on form for edit user start
var user_id ='';

$('table').on('click', '.edit_user',function(event) {
    event.preventDefault();

    $('#sample_1 tr').attr('bgcolor', '');
    $(this).closest('tr').attr('bgcolor','background-color: rgba(41, 171, 159, .50)');

    user_id = $(this).attr('id').split('_')[1];
    $('.cb').prop('checked', false);

    user_name = $(this).closest('tr').find('td').eq(0).text().trim();
    $('#current_user_id').remove();
    // $('#header_text').html('Page access for '+user_name);
    $('#header_text').html(user_name);
    $.get('<?php echo site_url();?>/User_access/get_page_access_by_user_id/'+user_id).done(function(data) 
    {
        if(data == "No permission"){
            $('#no_permission').slideDown();
            setTimeout( function(){$('#no_permission').slideUp()}, 1500 );
        }else{
            $.each(jQuery.parseJSON(data), function(index, val) {
                $("#chkbx_"+val.pages_id).prop('checked',true);
            });
        }


    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
//Code for showing checkbox on form for edit user end


//Code for set page access by user_id start
$(document).on('click', '.cb', function(event) {

    var sess_data = '<?php echo $this->session->userdata('user_role'); ?>';
    
    var data = {};
    data.page_id = $(this).attr('id').split('_')[1];

    if(user_id == ""){
        $('#user_not_selected').slideDown();
        setTimeout( function(){$('#user_not_selected').slideUp()}, 1000 );
    }else if(data.page_id == "1a6c05a3-b918-11e5-a956-eca86bfd5e5e" && sess_data == "owner"){

        $('#chkbx_1a6c05a3-b918-11e5-a956-eca86bfd5e5e').prop('checked', true);

        $('#owner_remove_fail').slideDown();
        setTimeout( function(){$('#owner_remove_fail').slideUp()}, 2000 );
    }
    else{

        data.csrf_test_name = $('input[name=csrf_test_name]').val();
        data.edited_user_id = user_id;
        data.checked_or_unchecked = $(this).attr('checked');


        $.post("<?php echo site_url('/User_access/set_or_remove_user_access');?>",data).done(function(data1, textStatus, xhr) {
            event.preventDefault();

            if(data1 == "Access added"){
                $('#access_added').slideDown();
                setTimeout( function(){$('#access_added').slideUp()}, 1000 );
            }
            else if(data1 == "Access add fail"){
                $('#access_adding_fail').slideDown();
                setTimeout( function(){$('#access_adding_fail').slideUp()}, 1000 );
            }
            else if(data1 == "Access removed"){
                $('#access_remove_success').slideDown();
                setTimeout( function(){$('#access_remove_success').slideUp()}, 1000 );
            }
            else if(data1 == "Access remove fail"){
                $('#access_remove_fail').slideDown();
                setTimeout( function(){$('#access_remove_fail').slideUp()}, 1000 );
            }
            else if(data1 == "No permission"){
               $('#no_permission').slideDown();
               setTimeout( function(){$('#no_permission').slideUp()}, 1000 );
           }
       });
    }
});

//Code for set page access by user_id end

</script>
