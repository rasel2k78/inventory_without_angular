<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-danger" id="update_failure" style="display:none" role="alert"><?php echo $this->lang->line('update_failure'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_success'); ?></div>
        <div class="alert alert-danger" id="no_username" style="display:none" role="alert"><?php echo $this->lang->line('no_username'); ?></div>
        <div class="alert alert-danger" id="no_mobile" style="display:none" role="alert"><?php echo $this->lang->line('no_mobile'); ?></div>
        <div class="alert alert-danger" id="no_email" style="display:none" role="alert"><?php echo $this->lang->line('no_email'); ?></div>


        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-edit"></i><?php echo $this->lang->line('change_information'); ?>
                </div>
            </div>

            <div class="portlet-body form">
                <!-- BEGIN MY PROFILE FORM-->

                <?php
                error_reporting(0);
                $form_attrib = array('id' => 'my_profile_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/User/updateMyProfile'),  $form_attrib, '');
                ?>
                <!-- <form id="my_profile_form" class="form-horizontal form-bordered" action="<?php echo site_url('/User/updateMyProfile');?>" method="post" enctype="multipart/form-data"> -->

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" id="username" class="form-control" name="username" value="<?php echo $user_info->username?>" placeholder = "<?php echo $this->lang->line('name'); ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('email'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" id="email" class="form-control" name="email" value="<?php echo$user_info->email?>" placeholder="<?php echo $this->lang->line('email'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('mobile1'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="number" id="phone_1" class="form-control" name="phone1" value="<?php echo $user_info->phone_1?>" placeholder="<?php echo $this->lang->line('mobile1'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('mobile2'); ?></label>
                        <div class="col-md-9">
                            <input type="number" id="phone_2" class="form-control" name="phone2" value="<?php echo $user_info->phone_2?>" placeholder="<?php echo $this->lang->line('mobile2'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('present_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="present_address" class="form-control" name="present_address" value="<?php echo $user_info->present_address?>"  placeholder="<?php echo $this->lang->line('present_address'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('permanent_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="permanent_address" class="form-control" name="permanent_address" value="<?php echo $user_info->permanent_address?>"  placeholder="<?php echo $this->lang->line('permanent_address'); ?>">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('image'); ?></label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                    <img src="<?php if($user_info->image =="" || $user_info->image == null) {
                                        echo base_url().'/images/user_images/no_image.png';
                                    } else{
                                        echo base_url().$user_info->image;
                                    }  ?>"/>
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            <?php echo $this->lang->line('select_image'); ?>
                                        </span>
                                        <span class="fileinput-exists">
                                            <?php echo $this->lang->line('change_image'); ?> 
                                        </span>
                                        <input type="file" name="user_image">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        <?php echo $this->lang->line('cancel'); ?> 
                                    </a>
                                </div>
                            </div>
                                <!-- <div class="clearfix margin-top-10">
                                    <span class="label label-danger">
                                        NOTE! 
                                    </span>
                                    Image preview only works in IE10+, FF3.6+, Safari6.0+, Chrome6.0+ and Opera11.1+. In older browsers the filename is shown instead.
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" id="update_my_info" class="btn green"><?php echo $this->lang->line('update_information'); ?></button>
                        <button type="button" id="cancel_my_info_update"  class="btn default cancel_button"><?php echo $this->lang->line('cancel'); ?></button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>

    <!-- Change Password Block Start -->
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="alert alert-danger" id="pass_update_failure" style="display:none" role="alert"><?php echo $this->lang->line('pass_update_failure'); ?></div>
        <div class="alert alert-success" id="pass_update_success" style="display:none" role="alert"><?php echo $this->lang->line('pass_update_success'); ?></div>
        <div class="alert alert-danger" id="new_pass_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('new_pass_not_matched'); ?></div>
        <div class="alert alert-danger" id="old_pass_not_matched" style="display:none" role="alert"><?php echo $this->lang->line('old_pass_not_matched'); ?></div>
        <div class="alert alert-danger" id="empty_pass" style="display:none" role="alert"><?php echo $this->lang->line('empty_pass'); ?></div>
        <div class="alert alert-danger" id="short_pass" style="display:none" role="alert"><?php echo $this->lang->line('short_pass'); ?></div>

        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-key"></i><?php echo $this->lang->line('change_password'); ?> 
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    $form_attrib = array('id' => 'change_password_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                    echo form_open(site_url('/User/changePassword'),  $form_attrib, '');
                    ?>
                    <!-- <form id="change_password_form" class="form-horizontal form-bordered" action="<?php echo site_url('/User/changePassword');?>" method="post" enctype="multipart/form-data"> -->

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('old_password'); ?></label>
                            <div class="col-md-9">
                                <input id = "old_pass" type="password" class="form-control" name="old_pass"  placeholder="<?php echo $this->lang->line('old_password'); ?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('new_password'); ?></label>
                            <div class="col-md-9">
                                <input id = "new_pass" type="password" id="email" class="form-control" name="new_pass"  placeholder="<?php echo $this->lang->line('new_password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('new_password_again'); ?></label>
                            <div class="col-md-9">
                                <input id = "new_pass_again" type="password" class="form-control" name="new_pass_again"  placeholder="<?php echo $this->lang->line('new_password_again'); ?>">
                            </div>
                        </div>


                    </div>
                    <div class="form-actions">
                        <button type="submit" id="update_my_info" class="btn green"><?php echo $this->lang->line('change_password'); ?></button>
                        <button type="button" id="cancel_password_btn" class="btn default"><?php echo $this->lang->line('cancel'); ?></button>
                    </div>
                </form>
                <!-- END FORM-->
            </div>

        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

</div>
<!-- Change Password Block End -->

</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script type="text/javascript">


//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#my_profile_form').ajaxForm({
    beforeSubmit: function() {    

    },
    success: function(msg) {
        if(msg == "Username can not be empty")
        {
            $('#no_username').slideDown();
            setTimeout( function(){$('#no_username').slideUp()}, 1500 );
        }
        else if(msg == "Mobile can not be empty"){
            $('#no_mobile').slideDown();
            setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
        }
        else if(msg == "Email can not be empty"){
            $('#no_email').slideDown();
            setTimeout( function(){$('#no_email').slideUp()}, 1500 );
        }
        else if(msg == "success"){


            // alert("<?php echo $this->session->userdata('image') ?>");exit;

            // $('.img-circle').attr('src',"<?php echo base_url().'images/user_images/'.$this->session->userdata('image') ?>");
            $('.img-circle').attr('src',"<?php echo base_url().'images/user_images/'.$this->session->userdata('image') ?>");
            //$('.img-circle').attr('src',"http://www.pos2in.com/assets/images/icon3.png");
            $('#update_success').slideDown();
            setTimeout( function(){$('#update_success').slideUp()}, 2500 );
            location.reload(true);
        }
    },
    complete: function(xhr) {
        
    }

}); 
//Ajax form submit end



// Code for cancel button on profile info form start
$('#cancel_my_info_update').click(function(event) {

    $('#username').val('');
    $('#email').val('');
    $('#phone_1').val('');
    $('#phone_2').val('');
    $('#present_address').val('');
    $('#permanent_address').val('');
    $('.fileinput-preview').html("<img src=''>");

});
// Code for cancel button on profile info form end


// Code for Ajax password changing form start
$('#change_password_form').submit(function (event) {
    dataString = $("#change_password_form").serialize();
    $.ajax({
        type:"POST",
        url:"<?php echo site_url();?>/User/changePassword",
        data:dataString,

        success:function (data) {

            if(data == "Password change successfully"){
                $('#pass_update_success').slideDown();
                setTimeout( function(){$('#pass_update_success').slideUp()}, 1500 );    
            }
            else if(data == "Password didn't changed"){
                $('#pass_update_failure').slideDown();
                setTimeout( function(){$('#pass_update_failure').slideUp()}, 1500 );
            }
            else if(data == "New password didn't match"){
                $('#new_pass_not_matched').slideDown();
                setTimeout( function(){$('#new_pass_not_matched').slideUp()}, 1500 );
            }
            else if(data == "Old paasword didn't match"){
                $('#old_pass_not_matched').slideDown();
                setTimeout( function(){$('#old_pass_not_matched').slideUp()}, 1500 );
            }
            else if(data == "Password field can't be empty"){
                $('#empty_pass').slideDown();
                setTimeout( function(){$('#empty_pass').slideUp()}, 1500 );
            }
            else if(data == "Password should atleast 4 charecters"){
                $('#short_pass').slideDown();
                setTimeout( function(){$('#short_pass').slideUp()}, 1500 );
            }

            $('#old_pass').val('');
            $('#new_pass').val('');
            $('#new_pass_again').val('');
        }
    });
    event.preventDefault();
});
// Code for Ajax password changing form end



// Code for cancel button on password form start
$('#cancel_password_btn').click(function(event) {
    $('#change_password_form input[type=password]').val("");
});
// Code for cancel button on password form end
</script>