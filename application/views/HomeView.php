<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Home</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<?php 
    $attributes = array(
        'id' => 'create_account', 
        'class' => 'form-horizontal'
        );
    echo form_open('Home/showAccountForm', $attributes); 
    ?>

    <input type="submit" value="Create new user">
    <?php echo form_close(); ?>
</body>
</html>