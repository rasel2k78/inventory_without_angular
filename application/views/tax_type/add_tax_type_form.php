<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert">সফলভাবে  ট্যাক্সের  তথ্য ডিলিট হয়েছে</div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert">দুঃখিত !!  ট্যাক্সের  তথ্য সেভ হয়নি</div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert">সফলভাবে  ট্যাক্সের  তথ্য সেভ হয়েছে</div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert">সফলভাবে  ট্যাক্সের  তথ্য আপডেট হয়েছে</div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>নতুন ট্যাক্সের ধরন  যোগ করুন
                </div>
            </div>
            <div class="portlet-body form" id="tax_type_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'tax_type_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের ধরনের নাম *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_name" placeholder="ট্যাক্সের ধরনের নাম">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের পরিমাণ *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_percentage" placeholder="ট্যাক্সের পরিমাণ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের ধরনের বিবরণ</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_description" placeholder="ট্যাক্সের ধরনের বিবরণ">
                        </div>
                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green">সেভ করুন</button>
                    <button type="button" class="btn default">বাতিল করুন</button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="tax_type_edit_div" style="display:none">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'tax_type_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');
                ?>

                <input type="text" class="form-control" name="tax_types_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের ধরনের নাম *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_name_edit" placeholder="ট্যাক্সের ধরনের নাম">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের পরিমাণ *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_percentage_edit" placeholder="ট্যাক্সের পরিমাণ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">ট্যাক্সের ধরনের বিবরণ</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="tax_types_description_edit" placeholder="ট্যাক্সের ধরনের বিবরণ">
                        </div>
                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green">সেভ করুন</button>
                    <button type="button" class="btn default">বাতিল করুন</button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>সকল ট্যাক্সের তালিকা 
                </div>
                <div class="tools">
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                ট্যাক্সের  নাম 
                            </th>
                            <!-- <th>
                                ট্যাক্সের  পরিমাণ (%)
                            </th> -->
                            <th>
                                পরিবর্তন / ডিলিট
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>


<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">ডিলিট নিশ্চিতকরণঃ <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                আপনি কি ট্যাক্সের ধরন এই তথ্য ডিলিট করতে চান ? 
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
    <button type="button" id="delete_confirmation" class="btn blue">Yes</button>
</div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>


<script src="<?php echo CURRENT_ASSET;?>assets/build/react.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/react-dom.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/browser.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/unit.js"></script>
<!-- <script type="text/babel" src="<?php echo CURRENT_ASSET;?>assets/components/unit.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
//table_tax_type = $('#sample_2').DataTable();		
        //  There was some commented code,please check add_unit_form.php or add_brand_form.php for those lines of code
        var table_tax_type = $('#sample_2').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo site_url(); ?>/Tax_type/all_tax_type_info_for_datatable/",
            "lengthMenu": [
            [10, 15, 20,30],
            [10, 15, 20,30] 
            ],
            "pageLength": 10,
            "language": {
                "lengthMenu": " _MENU_ records",
                "paging": {
                    "previous": "Prev",
                    "next": "Next"
                }
            },
            "columnDefs": [{  
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
            [0, "desc"]
            ]
        });
    </script>

    <script type="text/javascript">


        $('table').on('click', '.edit_tax_type',function(event) {
            event.preventDefault();

            var tax_type_id = $(this).attr('id').split('_')[1];
            $('#current_tax_type_id').remove();

            $.get('<?php echo site_url();?>/tax_type/get_tax_type_info/'+tax_type_id).done(function(data) {

                var tax_type_data = $.parseJSON(data);
                $('#form_header_text').text("ট্যাক্সের তথ্য পরিবর্তন করুন");
                $('input[name = tax_types_name_edit]').val(tax_type_data.tax_type_info_by_id.tax_types_name);
                $('input[name = tax_types_percentage_edit]').val(tax_type_data.tax_type_info_by_id.tax_types_percentage);
                $('input[name = tax_types_description_edit]').val(tax_type_data.tax_type_info_by_id.tax_types_description);
                
                $('#tax_type_insert_div').slideUp('fast');
                $('#tax_type_edit_div').slideDown('slow');
                $('input[name=tax_types_id]').val(tax_type_id);

            }).error(function() {
                alert("An error has occured. pLease try again");
            });
        });

        $('#tax_type_edit_form').submit(function(event) {

            event.preventDefault();
            var post_data={};
            
            post_data.tax_types_name_edit = $('input[name = tax_types_name_edit]').val();
            post_data.tax_types_percentage_edit = $('input[name = tax_types_percentage_edit]').val();
            post_data.tax_types_description_edit = $('input[name = tax_types_description_edit]').val();
            post_data.tax_types_id = $('input[name=tax_types_id]').val();
            post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

            console.log(post_data);
            $.post('<?php echo site_url() ?>/tax_type/update_tax_type_info',post_data).done(function(data, textStatus, xhr) {

                var data2 = $.parseJSON(data);
                if(data2['success'])
                {
                    $('#update_success').slideDown();
                    setTimeout( function(){$('#update_success').slideUp()}, 3000 );
                    var row_data = table_tax_type.row( $('#edit_'+post_data.tax_types_id).closest('tr')[0] ).data();
                    row_data[0] = post_data.tax_types_name_edit;
                    table_tax_type.row( $('#edit_'+post_data.tax_types_id).closest('tr')[0] ).data(row_data).draw();
                    $('#tax_type_insert_div').slideDown('fast');
                    $('#tax_type_edit_div').slideUp('slow');
                    $('#form_header_text').text("নতুন ট্যাক্সের ধরন  যোগ করুন");
                    $('.help-block').hide();
                }

                else if(!Object.keys(data2.error).length)
                {
                    alert("ট্যাক্সের তথ্য আপডেট হয়নি");                
                }

                else
                {
                    $.each(data2.error, function(index, val) {
                        $('input[name='+index+']').parent().parent().addClass('has-error');
                        $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                        $('.help-block').show();
                    });            
                }
            }).error(function() {

                alert("Connection failed.");

            });

        });
    </script>

    <script type="text/javascript">
        
        var tax_types_id = '' ;
        $('table').on('click', '.delete_tax_type',function(event) {
            event.preventDefault();
            tax_types_id = $(this).attr('id').split('_')[1];
        });


        $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
            event.preventDefault();
        //var tax_types_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'tax_types_id' : tax_types_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/tax_type/delete_tax_type/'+tax_types_id,post_data).done(function(data) {
            if(data = "success")
            {
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                $('#responsive_modal_delete').modal('hide');
                table_tax_type.row($('#delete_'+tax_types_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }

        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });
// ajax form submit starts....

$('#tax_type_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.tax_types_name = $('input[name=tax_types_name]').val();
    data.tax_types_percentage = $('input[name=tax_types_percentage]').val();
    data.tax_types_description = $('input[name=tax_types_description]').val();
    
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/tax_type/save_tax_type_info',data).done(function(data1, textStatus, xhr) {

        var data2 =$.parseJSON(data1);
        if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }

        else{
            $('.form-group.has-errorf').removeClass('has-error');
            $('.help-block').hide();

            $('input[name= tax_types_name]').val('');
            $('input[name= tax_types_percentage]').val('');
            $('input[name= tax_types_description]').val('');

            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table_tax_type.row.add({
                "0":       data.tax_types_name,
                "1":   "<a class='btn btn-primary edit_tax_type' id='edit_"+data2.data+"' >    পরিবর্তন </a><a class='btn btn-danger delete_tax_type' id='delete_"+data2.data+"'>ডিলিট </a>"
            }).draw();
        }
    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );

    });    
});

// ajax form submit end.......
</script>