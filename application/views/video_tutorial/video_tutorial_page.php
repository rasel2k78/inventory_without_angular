<style type="text/css" media="screen">
  .modal_fade_bg { top: -3% !important; }
  .main_modal_bg { 
    left: 10% !important;
    bottom: auto;
    right: auto;
    padding: 0;
    width: 100% !important;
    margin-left: 0px !important;
    background: none !important; 
    border: none !important; 
    border: none !important; 
    border-radius: none !important;
    -webkit-box-shadow: none !important; 
    box-shadow: none !important; 
    background-clip: padding-box; 
  }
  .modal_header_style { 
    border: none !important;
    width: 90% !important;
    min-height: 8px !important;
    padding: 23px 10px 5px 0 !important;
  }
  video::-internal-media-controls-download-button {
    display:none;
}
video::-webkit-media-controls-enclosure {
    overflow:hidden;
}
video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
</style>
<div class="row fisrt-part-vdo">
  <?php
  $Buy= $this->lang->line("Buy");
  $Sell= $this->lang->line("Sell");
  $Item=$this->lang->line("Item");
  $Category=$this->lang->line("Category");
  $Customer=$this->lang->line("Customer");
  $Expenditures=$this->lang->line("Expenditures");
  $User=$this->lang->line("User");
  $Damage=$this->lang->line("Damage");
  $Expenditure_type=$this->lang->line("Expenditure_type");
  $Loan=$this->lang->line("Loan");
  $Money_return_with_customer=$this->lang->line("Money_return_with_customer");
  $Money_return_with_vendor=$this->lang->line("Money_return_with_vendor");
  $Sales_representative=$this->lang->line("Sales_representative");
  $Shop_info=$this->lang->line("Shop_info");
  $Specification=$this->lang->line("Specification");
  $User_access_panel=$this->lang->line("User_access_panel");
  $VAT=$this->lang->line("VAT");
  $Vendor=$this->lang->line("Vendor");
  $Deposit_withdrawal=$this->lang->line("Deposit_withdrawal");
  $Update=$this->lang->line("Update");
  $Transfer_item=$this->lang->line("Transfer_item");
  $Exchange_with_customer=$this->lang->line("Exchange_with_customer");
  $Exchange_with_vendor=$this->lang->line("Exchange_with_vendor");
  $Report=$this->lang->line("Report");

  $video_name = array("add_item.mp4"=>"$Item","buy.mp4"=>"$Buy","sell.mp4"=>"$Sell","add_catagory.mp4"=>"$Category","add_customer.mp4"=>"$Customer","add_expenditures.mp4"=>"$Expenditures","add_user.mp4"=>"$User","damage_and_lost.mp4"=>"$Damage","expenditure_type.mp4"=>"$Expenditure_type","loan_final.mp4"=>"$Loan","money_return_with_customer.mp4"=>"$Money_return_with_customer","money_return_with_vendor.mp4"=>"$Money_return_with_vendor","sales_representative.mp4"=>"$Sales_representative","shop_info.mp4"=>"$Shop_info","specification.mp4"=>"$Specification","user_access_panel.mp4"=>"$User_access_panel","vat.mp4"=>"$VAT","vendor.mp4"=>"$Vendor","widrawal_deposit.mp4"=>"$Deposit_withdrawal","report.mp4"=>"$Report","exchange_with_customer.mp4"=>"$Exchange_with_customer","exchange_with_vendor.mp4"=>"$Exchange_with_vendor","transfer_item.mp4"=>"$Transfer_item","update_and_sync.mp4"=>"$Update");
  foreach ($video_name as $key => $value)
  {
    $filename = 'video/'.$key;
    if (file_exists($filename))
    {
      $split_type = explode(".", $key);
      ?>    
      <div class="col-md-2 col-sm-2 col-lg-2 col-xs-2">
        <div class="video-1">
          <a href="#" id="<?php echo $split_type[0]?>" class="videos_list" data-toggle="modal" data-target="#video_play">
            <div class="vidwo-bg1">
              <p><img src="<?php echo CURRENT_ASSET;?>assets/video/images/<?php echo $split_type[0]?>.png" width="262" height="154"></p>
            </div>
          </a>
          <p><?php echo $value?></p>
        </div>
      </div>
      <?php
    }
    else 
    {
   $split_type = explode(".", $key);
  ?>
            <div class="col-md-2 col-sm-2 col-lg-2 col-xs-2">
              <div class="video-1">
                <a href="#<?php echo $split_type[0]?>">
                  <div class="vidwo-bg1">
                      <p><img src="<?php echo CURRENT_ASSET;?>assets/video/images/<?php echo $split_type[0]?>_d.png" width="262" height="154"></p>
                    </div>
                </a>
                <p><?php echo $value?></p>
              </div>
             </div>
<?php
    }
  }
  ?>
</div>
<div id="video_play" class="modal main_modal_bg fade modal_fade_bg" role="dialog">
  <div class="modal-dialog full-widthbg">
    <div class="modal-header modal_header_style">
      <button type="button" class="close colos-partbg" data-dismiss="modal"> <i class="fa fa-close"></i> close</button>

    </div>
    <div class="modal-content modal-contentbg">
      <div class="modal-body">
        <video id="myHTMLvideo" controls preload="none" width="80%" height="80%">
          <source id="playVideo" type="video/mp4">
          </video>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <script>
   $('.videos_list').click(function() 
   {
    var video_name = $(this).attr('id')+'.mp4';
    var src = "<?php echo base_url()?>video/" + video_name;
    var player = document.getElementById('myHTMLvideo');
    var play_video = document.getElementById('playVideo');
    play_video.src = src;
    player.load();
    player.play(); 
  });
   $(".modal-header").click(function(event)
   {
    var player = document.getElementById('myHTMLvideo');
    var play_video = document.getElementById('playVideo');
    player.load();
    $('video').trigger('pause');  
  });



 </script>
 