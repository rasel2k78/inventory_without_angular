<?php
/**
 * "Header Page" 
 *
 *This page load header part of design.
 * Loaded in  "header_footer_helper.php" helper.
 * 
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>


<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.1.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta http-equiv="cache-control" content="no-cache">
    <meta charset="utf-8"/>
    <title>MAX Inventory Solution</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <!-- <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/> -->
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN PAGE STYLES -->
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
    <link href="<?php echo CURRENT_ASSET;?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link href="<?php echo CURRENT_ASSET;?>assets/custom/main.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.js"></script>  
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support 
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<!--<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/index.js" type="text/javascript"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>-->
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/img/logo-default.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions hide">
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle red-pink dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-bar-chart"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;
                        </span>&nbsp;<i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:;">
                                <i class="icon-user"></i> New User </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-present"></i> New Event <span class="badge badge-success">4
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-basket"></i> New order </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-flag"></i> Pending Orders <span class="badge badge-danger">4
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-users"></i> Pending Users <span class="badge badge-warning">12
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- <div class="btn-group">
                <button type="button" class="btn btn-circle green-haze dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-bell"></i>&nbsp;<span class="hidden-sm hidden-xs">Post&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="javascript:;">
                            <i class="icon-docs"></i> New Post </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <i class="icon-tag"></i> New Comment </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-share"></i> Share </a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <i class="icon-flag"></i> Comments <span class="badge badge-success">4
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-users"></i> Feedbacks <span class="badge badge-danger">2
                                </span>
                            </a>
                        </li>
                    </ul>
                </div> -->
            </div>
            <!-- END PAGE ACTIONS -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                <!-- BEGIN HEADER SEARCH BOX -->
                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                <!-- <form class="search-form search-form-expanded" action="extra_search.html" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search..." name="query">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                        </span>
                    </div>
                </form> -->


                <!-- END HEADER SEARCH BOX -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-left">
                        
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <h2>

                                <a style="color:red;font" href="javascript:;" class="dropdown-toggle form-section" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-wallet"></i>
                                    <b><?php echo $final_cashbox_info['total_cashbox_amount'] ?>৳</b>
                                </a>
                            </h2>    
                        </li>
                        <li>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url();?>/ChangeLanguage/changeLangEng">
                                        <img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/us.png"> English 
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>/ChangeLanguage/changeLangBang">
                                        <img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/bd.png"> বাংলা 
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- END LANGUAGE BAR -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        
                    </ul>
                    <ul class="nav navbar-nav pull-right">

                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <a href="#" class="dropdown-toggle" dropdown-menu-hover data-toggle="dropdown" data-close-others="true">
                                <i class="icon-bell"></i>
                                <span class="badge badge-default">
                                    7 
                                </span>
                            </a>
                            <ul class="dropdown-menu">

                                
                                <li class="external">
                                    <h3><span class="bold"><?php echo count($check_receivable_transfer_details);?> pending</span> notifications</h3>
                                    <!-- <a href="#/profile/dashboard">view all</a> -->
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">

            <?php 
            if($check_receivable_transfer_details) {
                foreach ($check_receivable_transfer_details as $key => $value) 
                {
                    $store_name = $CI->Transfer_model->get_shop_by_id($value['transfer_from'])->row_array();
                    //echo $store_name['name'];exit;
                    ?>
                    <li>
                    <a href="<?php echo site_url();?>/Transfer_items/receivable_transfer_details/<?php echo $value['transfer_details_id'];?>">
                 <span class="time">just now</span>
                 <span class="details">
                  <span class="label label-sm label-icon label-success">
                   <i class="fa fa-plus"></i>
                  </span>
                  New transfer from <?php echo $store_name['name'];?>
                 </span>
                </a>
               </li>
                <?php
                }
            }
                                        
            ?>
                                        <!-- <li>
                                            <a href="<?php echo site_url();?>/Receive_transfer_items">
                                                <span class="time">just now</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-success">
                                                        <i class="fa fa-plus"></i>
                                                    </span>
                                                    New transfer request.
                                                </span>
                                            </a>
                                        </li> -->
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- END NOTIFICATION DROPDOWN -->
                        <!-- BEGIN INBOX DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END INBOX DROPDOWN -->
                        <!-- BEGIN TODO DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END TODO DROPDOWN -->
                        <!-- BEGIN LANGUAGE BAR -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-language">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span class="langname">
                                    Language
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url();?>/ChangeLanguage/changeLangEng">
                                        <img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/us.png"> English 
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url();?>/ChangeLanguage/changeLangBang">
                                        <img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/bd.png"> বাংলা 
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- END LANGUAGE BAR -->
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="<?php echo base_url().$this->session->userdata('iamge') ?>"/>
                                <span class="username username-hide-on-mobile">
            <?php echo $this->session->userdata('username');
                                                    // $id = $this->session->userdata('user_id');
            ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>

                                        <!-- <a href='<?php echo site_url("/User/myProfile/$id") ?>'> -->
                                        <a href='<?php echo site_url("/User/myProfile") ?>'>                                                        

                                            <i class="icon-user"></i> My Profile </a>
                                        </li>



                                        <li class="divider">
                                        </li>

                                        <li>
                                            <a href='<?php echo site_url("/User/logOut") ?>'>
                                                <i class="icon-key"></i> Log Out </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <!-- END USER LOGIN DROPDOWN -->
                                    <!-- BEGIN USER LOGIN DROPDOWN -->
                                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                                    <!-- END USER LOGIN DROPDOWN -->
                                </ul>
                            </div>
                            <!-- END TOP NAVIGATION MENU -->
                        </div>
                        <!-- END PAGE TOP -->
                    </div>
                    <!-- END HEADER INNER -->
                </div>
                <!-- END HEADER -->

                <div class="clearfix">
                </div>

                <!-- BEGIN CONTAINER -->
                <div class="">
                    <div class="page-container">
                        <!-- BEGIN SIDEBAR -->
                        <div class="page-sidebar-wrapper">
                            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                            <div class="page-sidebar navbar-collapse collapse">
                                <!-- BEGIN SIDEBAR MENU -->
                                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                                <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                                    <li class="<?php echo url_active(array('#'))?>" >
                                        <a href="#">
                                            <i class="icon-home"></i>
                                            <span class="title"><?php echo $this->lang->line('dashboard');?>
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                    </li>

                                    <li class="<?php echo url_active(array('nagadanboi', 'Loan'))?>" >
                                        <a href="<?php echo site_url('/nagadanboi');?>">
                                            <i class="icon-rocket"></i>
                                            <span class="title"><?php echo $this->lang->line('cashbox');?> 
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/nagadanboi');?>">
                <?php echo $this->lang->line('deposit_withdrawal');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Loan');?>">
                <?php echo $this->lang->line('loan');?>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Item', 'Brand', 'Category', 'Unit', 'Vendor', 'Size', 'Lot_size'))?>" >
                                        <a href="<?php echo site_url('/Item');?>">
                                            <i class="icon-basket"></i>
                                            <span class="title"><?php echo $this->lang->line('item');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/Item');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_item');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Brand');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_brand');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Category');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_category');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Unit');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_unit');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Vendor');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_vendor');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Size');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_size');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Lot_size');?>">
                                                    <i class="icon-home"></i>
                <?php echo $this->lang->line('add_lot_size');?>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Buy','Buy_list','Buy_list_all','Advance_buy'))?>" >
                                        <a href="<?php echo site_url('/Buy');?>">
                                            <i class="icon-rocket"></i>
                                            <span class="title"> <?php echo $this->lang->line('buy');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/Buy');?>">
                <?php echo $this->lang->line('buy_items');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Buy_list');?>">
                <?php echo $this->lang->line('buy_due_list');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Buy_list_all');?>">
                <?php echo $this->lang->line('buy_all_list');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Advance_buy');?>">
                <?php echo $this->lang->line('advance_buy');?>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Sell','Sell_list_due','Sell_list_all','Tax_type','Customers','Demage_and_lost'))?>" >
                                        <a href="<?php echo site_url('/Sell');?>">
                                            <i class="icon-puzzle"></i>
                                            <span class="title"> <?php echo $this->lang->line('sell');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/Sell');?>">
                <?php echo $this->lang->line('sell_items');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Sell_list_due');?>">
                <?php echo $this->lang->line('sell_due_list');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Sell_list_all');?>">
                <?php echo $this->lang->line('sell_all_list');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Tax_type');?>">
                <?php echo $this->lang->line('add_tax_type');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Customers');?>">
                <?php echo $this->lang->line('add_customer');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Demage_and_lost');?>">
                <?php echo $this->lang->line('demage_and_lost');?>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Expenditure','Expenditure_type'))?>" >
                                        <a href="<?php echo site_url('/Expenditure');?>">
                                            <i class="icon-briefcase"></i>
                                            <span class="title"><?php echo $this->lang->line('expenditure');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/Expenditure_type');?>">
                <?php echo $this->lang->line('add_exp_type');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Expenditure');?>">
                <?php echo $this->lang->line('add_exp');?>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Exchange_with_vendors','Exchange_with_customers'))?>" >
                                        <a href="<?php echo site_url('/Exchange_with_vendors');?>">
                                            <i class="icon-wallet"></i>
                                            <span class="title"><?php echo $this->lang->line('exchange_return');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/Exchange_with_vendors');?>">
                <?php echo $this->lang->line('exchange_with_vendor');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Exchange_with_customers');?>">
                <?php echo $this->lang->line('exchange_with_customer');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/Return_money');?>">
                <?php echo $this->lang->line('money_exchange');?>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('User','User_access'))?>" >
                                        <a href="<?php echo site_url('/User');?>">
                                            <i class="icon-wallet"></i>
                                            <span class="title"><?php echo $this->lang->line('user');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('/User');?>">
                <?php echo $this->lang->line('add_user');?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('/User_access');?>">
                <?php echo $this->lang->line('user_access_panel');?>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="<?php echo url_active(array('Transfer'))?>" >
                                        <a href="<?php echo site_url('/Transfer_items');?>">
                                            <i class="icon-rocket"></i>
                                            <span class="title"><?php echo $this->lang->line('transfer');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                    </li>

                                    <li class="<?php echo url_active(array('Sync'))?>"   >
                                        <a id="sync_tab" href="<?php echo site_url('/Sync');?>">
                                            <!-- <i style="color:red" class="fa fa-refresh"></i> -->
                                            <i class="fa fa-refresh"></i>
                                            <span class="title"><?php echo $this->lang->line('sync');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                    </li>

                                    <li class="<?php echo url_active(array('Report'))?>" >
                                        <a href="javascript:;">
                                            <i class="icon-briefcase"></i>
                                            <span class="title"><?php echo $this->lang->line('report');?>
                                            </span>
                                            <span class="arrow ">
                                            </span>
                                            <span class="selected">
                                            </span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="table_basic.html">
                                                    Basic Datatables
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                </ul>
                                <!-- END SIDEBAR MENU -->
                            </div>
                        </div>
                        <!-- END SIDEBAR -->

                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                                <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Modal title</h4>
                                            </div>
                                            <div class="modal-body">
                                                Widget settings form goes here
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn blue">Save changes</button>
                                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                
                                <!-- END STYLE CUSTOMIZER -->
                                <!-- BEGIN PAGE HEADER-->
                                <h3 class="page-title">
                                    Dashboard</h3>
                                    <!-- <button class="btn btn-primary">test</button><button class="btn btn-danger">sell</button> -->

                                    <!-- END PAGE HEADER-->
                                    <script src="" type="text/javascript" charset="utf-8" async defer>

                                                                                                                        // $( "#sync_tab" ).click(function(event) {


                                                                                                                        // });
</script>


