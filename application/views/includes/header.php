<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta http-equiv="cache-control" content="no-cache">
	<meta charset="utf-8"/>
	<title>pos2in</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<!-- <link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/> -->
	<link href="<?php echo CURRENT_ASSET;?>assets/video/css/style.css" rel="stylesheet" />

	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->
	<!-- BEGIN PAGE STYLES -->
	<link href="<?php echo CURRENT_ASSET;?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE STYLES -->
	<!-- BEGIN THEME STYLES -->
	<!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
	<link href="<?php echo CURRENT_ASSET;?>assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo CURRENT_ASSET;?>assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->
	<link href="<?php echo CURRENT_ASSET;?>assets/custom/main.css" rel="stylesheet" type="text/css"/>
	<link rel="shortcut icon" href="favicon.ico"/>
	<!-- Toastr Notification Starts -->
	<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-toastr/toastr.min.css"/>
	<!-- Toastr Notification Ends -->
<!-- 	<script type="text/javascript">
		document.onkeydown = function(event) {
			if(event.keyCode == 123) {
				return false;
			}
			if(event.ctrlKey && event.shiftKey && event.keyCode == 'I'.charCodeAt(0)){
				return false;
			}
			if(event.ctrlKey && event.shiftKey && event.keyCode == 'J'.charCodeAt(0)){
				return false;
			}
			if(event.ctrlKey && event.keyCode == 'U'.charCodeAt(0)){
				return false;
			}
			if(event.ctrlKey && event.keyCode == 'S'.charCodeAt(0)){
				return false;
			}
		};
	</script> -->
</head>
<!-- END HEAD -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/keyboardshortcut.js" type="text/javascript"></script>
<script type="text/javascript">$.ajaxSetup({ cache: false });</script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery-barcode.js"></script>  
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>/bower_components/angular/angular.min.js"></script> -->
<!-- Toastr Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-toastr.js"></script>
<!-- Toastr Ends -->
<!-- END PAGE LEVEL SCRIPTS -->


<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner">
			<!-- BEGIN LOGO -->
			<div class="page-logo" style="text-align: center;background: #d21f2b;">
				<a href="#">
					<img src="<?php echo base_url()?>images/pos2in_logo.jpg" alt="logo" class="logo-default" style="width: 120px;margin-top: 10px;margin-left: 0px;">
					<!-- for previous logo width was 50px -->
				</a>
				<div class="menu-toggler sidebar-toggler">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div> 
				<!-- END LOGO 
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
				</a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<div class="page-actions hide">
					<div class="btn-group">
						<button type="button" class="btn btn-circle red-pink dropdown-toggle" data-toggle="dropdown">
							<i class="icon-bar-chart"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;
						</span>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="javascript:;">
								<i class="icon-user"></i> New User </a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-present"></i> New Event <span class="badge badge-success">4
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="icon-basket"></i> New order </a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-flag"></i> Pending Orders <span class="badge badge-danger">4
								</span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="icon-users"></i> Pending Users <span class="badge badge-warning">12
							</span>
						</a>
					</li>
				</ul>
			</div>
			<!-- <div class="btn-group">
				<button type="button" class="btn btn-circle green-haze dropdown-toggle" data-toggle="dropdown">
					<i class="icon-bell"></i>&nbsp;<span class="hidden-sm hidden-xs">Post&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="javascript:;">
							<i class="icon-docs"></i> New Post </a>
						</li>
						<li>
							<a href="javascript:;">
								<i class="icon-tag"></i> New Comment </a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-share"></i> Share </a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="javascript:;">
										<i class="icon-flag"></i> Comments <span class="badge badge-success">4
									</span>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-users"></i> Feedbacks <span class="badge badge-danger">2
								</span>
							</a>
						</li>
					</ul>
				</div> -->
			</div>
			<!-- END PAGE ACTIONS -->
			<!-- BEGIN PAGE TOP -->
			<div class="page-top">
				<!-- BEGIN HEADER SEARCH BOX -->
				<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
				<!-- <form class="search-form search-form-expanded" action="extra_search.html" method="GET">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search..." name="query">
						<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
						</span>
					</div>
				</form> -->


				<!-- END HEADER SEARCH BOX -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-left">
						
		<!-- 				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
							<h2>

								<a style="color:red;font" href="javascript:;" class="dropdown-toggle form-section" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<i class="icon-wallet"></i>
									<b><?php echo $this->lang->line('money_sign');?><?php echo $final_cashbox_info['total_cashbox_amount'] ?></b>
								</a>
							</h2>	
						</li> -->
						<li>
							<input type="checkbox" name="vat_included" id="vat_include_id">
						</li>
						<li>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo site_url();?>/ChangeLanguage/changeLangEng">
										<img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/us.png"> English 
									</a>
								</li>
								<li>
									<a href="<?php echo site_url();?>/ChangeLanguage/changeLangBang">
										<img alt="" src="<?php echo CURRENT_ASSET;?>assets/global/img/flags/bd.png"> বাংলা 
									</a>
								</li>
							</ul>
						</li>


						<!-- END LANGUAGE BAR -->
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
						
					</ul>
					<ul class="nav navbar-nav pull-right">

						<!-- BEGIN NOTIFICATION DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

						
						<!-- END NOTIFICATION DROPDOWN -->
						<!-- BEGIN INBOX DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

						<!-- END INBOX DROPDOWN -->
						<!-- BEGIN TODO DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

						<!-- END TODO DROPDOWN -->
						<!-- BEGIN LANGUAGE BAR -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
						<?php 
						$english_lang = '<img alt="" src="'.base_url().'assets/global/img/flags/us.png"> English'  ;

						$bang_lang = '<img alt="" src="'.base_url().'assets/global/img/flags/bd.png"> বাংলা'  ;

						?>
						<li class="dropdown dropdown-language">
							<a href="" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<span class="langname">
									<?php echo ($this->input->cookie('language', TRUE)=="english") ? $english_lang : $bang_lang ; ?> 
								</span>
								<i class="fa fa-angle-down"></i>
							</a>

							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo site_url();?>/ChangeLanguage/changeLangEng">
										<?php echo $english_lang; ?>
										
									</a>
								</li>
								<li>
									<a href="<?php echo site_url();?>/ChangeLanguage/changeLangBang">
										<?php echo $bang_lang; ?>
									</a>
								</li>
							</ul>
						</li>
						
				<!-- 		<li class="dropdown" style="margin-top: 20px">
							<?php echo date("jS\, F Y ");?>
						</li> -->


						<!-- END LANGUAGE BAR -->
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
						<li class="dropdown dropdown-user">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<?php 

								
								if(($this->session->userdata('image') == null) || ($this->session->userdata('image') == "") ) {  ?>
								<img alt="" class="img-circle" src="<?php echo base_url()?>/images/user_images/no_image.png"/>
								<?php } 
								else{?>
								<img alt="" class="img-circle" src="<?php echo base_url().$this->session->userdata('image') ?>"/>
								<?php }?>

								<span class="username username-hide-on-mobile">
									<?php echo $this->session->userdata('username');
													// $id = $this->session->userdata('user_id');
									?> </span>
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-default">
									<li>

										<!-- <a href='<?php echo site_url("/User/myProfile/$id") ?>'> -->
										<a href='<?php echo site_url("/User/myProfile") ?>'>														

											<i class="icon-user"></i> My Profile </a>
										</li>



										<li class="divider">
										</li>

										<li>
											<a href='<?php echo site_url("/User/logOut") ?>'>
												<i class="icon-key"></i> Log Out </a>
											</li>
										</ul>
									</li>
									<!-- END USER LOGIN DROPDOWN -->
									<!-- BEGIN USER LOGIN DROPDOWN -->
									<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

									<!-- END USER LOGIN DROPDOWN -->
								</ul>
							</div>
							<!-- END TOP NAVIGATION MENU -->
						</div>
						<!-- END PAGE TOP -->
					</div>
					<!-- END HEADER INNER -->
				</div>
				<!-- END HEADER -->

				<div class="clearfix">
				</div>

				<!-- BEGIN CONTAINER -->
				<div class="">
					<div class="page-container">
						<!-- BEGIN SIDEBAR -->
						<div class="page-sidebar-wrapper">
							<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
							<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
							<div class="page-sidebar navbar-collapse collapse">
								<!-- BEGIN SIDEBAR MENU -->
								<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
								<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
								<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
								<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
								<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
								<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
								<ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
									<li class="<?php echo url_active(array('Dashboard'))?>" >
										<a href="<?php echo site_url('/Dashboard')?>">
											<i class="icon-home"></i>
											<span class="title"><?php echo $this->lang->line('dashboard');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
									</li>


									<?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager"): ?>
										<li class="<?php echo url_active(array('nagadanboi', 'Loan'))?>" >
											<a href="<?php echo site_url('/nagadanboi');?>">
												<i class="fa fa-money"></i>
												<span class="title"><?php echo $this->lang->line('cashbox');?> 
												</span>
												<span class="arrow ">
												</span>
												<span class="selected">
												</span>
											</a>
											<ul class="sub-menu">
												<li>
													<a href="<?php echo site_url('/nagadanboi');?>">
														<?php echo $this->lang->line('deposit_withdrawal');?>
													</a>
												</li>
												<li>
													<a href="<?php echo site_url('/Loan');?>">
														<?php echo $this->lang->line('loan');?>
													</a>
												</li>
											</ul>
										</li>
									<?php endif ?>

									<li class="<?php echo url_active(array('Item', 'Brand', 'Category', 'Unit',  'Size', 'Lot_size','Color'))?>" >
										<a href="<?php echo site_url('/Item');?>">
											<i class="fa fa-tags"></i>
											<span class="title"><?php echo $this->lang->line('item');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Spec_name');?>">
													<i ></i>
													<?php echo $this->lang->line('add_spec_left_nav');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Category');?>">
													<i ></i>
													<?php echo $this->lang->line('add_category');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Item');?>">
													<i ></i>
													<?php echo $this->lang->line('add_item');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Itemsearch');?>">
													<i ></i>
													Items by category
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/sell/multi_barcode');?>">
													<i ></i>
													Multiple Items Barcode Print
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Item/quick_item_buy');?>">
													<i ></i>
													Quick Item and Buy
												</a>
											</li>
										</ul>
									</li>

									<li class="<?php echo url_active(array('Buy','Buy_list', 'Vendor', 'Buy_list_all','Advance_buy','Advance_buy_list'))?>" >
										<a href="<?php echo site_url('/Buy');?>">
											<i class="icon-basket"></i>
											<span class="title"> <?php echo $this->lang->line('buy');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Buy');?>">
													<?php echo $this->lang->line('buy_items');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Buy_list');?>">
													<?php echo $this->lang->line('buy_due_list');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Buy_list_all');?>">
													<?php echo $this->lang->line('buy_all_list');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Vendor');?>">
													<i ></i>
													<?php echo $this->lang->line('add_vendor');?>
												</a>
											</li>
											<!-- <li>
												<a href="<?php echo site_url('/Advance_buy');?>">
													Advance Buy
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Advance_buy_list');?>">
													Advance Buy List
												</a>
											</li>   -->
											
										</ul>
									</li>

									<li class="<?php echo url_active(array('Sell','Sell_list_due','Sell_list_all','Tax_type','Customers','Demage_and_lost','Sell_deleted_invoices','Advance_sell','Advance_sell_list'))?>" >
										<a href="<?php echo site_url('/Sell');?>">
											<i class="icon-puzzle"></i>
											<span class="title"> <?php echo $this->lang->line('sell');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Sell');?>">
													<?php echo $this->lang->line('sell_items');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Sell_list_due');?>">
													<?php echo $this->lang->line('sell_due_list');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Sell_list_all');?>">
													<?php echo $this->lang->line('sell_all_list');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Sell_deleted_invoices');?>">
													Sell Deleted Invoices
												</a>
											</li>
<!-- 											<li>
												<a href="<?php echo site_url('/Advance_sell');?>">
													Advance Sell
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Advance_sell_list');?>">
													Advance Sell List
												</a>
											</li>   -->
											<li>
												<a href="<?php echo site_url('/Customers');?>">
													<?php echo $this->lang->line('add_customer');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Demage_and_lost');?>">
													<?php echo $this->lang->line('demage_and_lost');?>
												</a>
											</li>
										</ul>
									</li>

									<li class="<?php echo url_active(array('Expenditure','Expenditure_type'))?>" >
										<a href="<?php echo site_url('/Expenditure');?>">
											<i class="fa fa-file-text-o"></i>
											<span class="title"><?php echo $this->lang->line('expenditure');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Expenditure_type');?>">
													<?php echo $this->lang->line('add_exp_type');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Expenditure');?>">
													<?php echo $this->lang->line('add_exp');?>
												</a>
											</li>
										</ul>
									</li>
									<li class="<?php echo url_active(array('Bank_create','Bank_acc_create'))?>" >
										<a href="<?php echo site_url('/Bank_create');?>">
											<i class="fa fa-university"></i>
											<span class="title"><?php echo $this->lang->line('bank');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Bank_create');?>">
													<?php echo $this->lang->line('add_new_bank');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Bank_acc_create');?>">
													<?php echo $this->lang->line('add_bank_accounts');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Bank_dpst_wdrl');?>">
													<?php echo $this->lang->line('bank_depo_with');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Pending_cheques');?>">
													<?php echo $this->lang->line('pending_cheques');?>
												</a>
											</li> 
										</ul>
									</li> 

									<li class="<?php echo url_active(array('Exchange_with_vendors','Exchange_with_customers','Return_money', 'Vendor_money_return'))?>" >
										<a href="<?php echo site_url('/Exchange_with_vendors');?>">
											<i class="fa fa-exchange"></i>
											<span class="title"><?php echo $this->lang->line('exchange_return');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo site_url('/Exchange_with_vendors');?>">
													<?php echo $this->lang->line('exchange_with_vendor');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Exchange_with_customers');?>">
													<?php echo $this->lang->line('exchange_with_customer');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Return_money');?>">
													<?php echo $this->lang->line('money_return_customer');?>
												</a>
											</li>
											<li>
												<a href="<?php echo site_url('/Vendor_money_return');?>">
													<?php echo $this->lang->line('money_return_vendor');?>
												</a>
											</li>

										</ul>
									</li>
									<li class="<?php echo url_active(array('Sync'))?>"   >
										<!-- <a id="sync_tab" onclick="force_infinite_sync()"> -->
										<a id="sync_tab">
											<!--  href="<?php echo site_url('/Sync');?>" -->
											<!-- <i style="color:red" class="fa fa-refresh"></i> -->
											<i class="fa fa-refresh"></i>
											<span class="title"><?php echo $this->lang->line('sync');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<li id="sync_status" >
												<a>
													Sync status	
												</a>
											</li>

										</ul>
										<script type="text/javascript">
											// var intervalTime = 10000;
											// var timeout = 0;
											// var lastSyncSuccess = true;
											// var infinite_sync = function() {
											// 	clearTimeout(timeout);
											// 	// debugger;
											// 	console.log('internet availability checking');
											// 	if (window.navigator.onLine === true) {
											// 		//console.log('last sync success checking');
											// 		if (lastSyncSuccess) {
											// 			lastSyncSuccess = false;
											// 			$('#sync_tab .fa.fa-refresh').addClass('fa-spin');
											// 			$.get('<?php echo site_url('/Sync') ?>')
											// 			.success(function(data) {
											// 				data = JSON.parse(data);
											// 				console.log(data);
											// 				if (data.success === true) {
											// 					intervalTime = 10000;
											// 					$('#sync_tab .fa.fa-refresh').removeClass('fa-spin');
											// 					lastSyncSuccess = true;
											// 				}
											// 				else
											// 				{
											// 					if (data.type === 'no_sync_data_available') {
											// 						intervalTime = 3600000;
											// 					};
											// 					if (data.type === 'invalid_user') {
											// 						alert('Invalid user');
											// 						return false;
											// 					};
											// 					if (data.type === 'access_denied') {
											// 						$('#sync_tab .fa.fa-refresh').removeClass('fa-spin');
											// 						alert("<?php echo $this->lang->line('no_permission');?>");
											// 					};

											// 					$('#sync_tab .fa.fa-refresh').removeClass('fa-spin');
											// 				}
											// 			})
											// 			.error(function(data) {
											// 				$('#sync_tab .fa.fa-refresh').removeClass('fa-spin');
											// 			});
											// 		}
											// 		timeout = setTimeout(infinite_sync,intervalTime);
											// 	}
											// };

											// var force_infinite_sync = function() {
											// 	lastSyncSuccess = true;
											// 	infinite_sync();
											// };

											// // infinite_sync();
											// force_infinite_sync();
										</script>
									</li>



									<li>
										<a href="javascript:;">
											<i class="fa fa-cogs"></i>
											<span class="title"><?php echo $this->lang->line('settings_report');?></span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
										<ul class="sub-menu">
											<?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager"): ?>
												<li class="<?php echo url_active(array('User','User_access','Sales_rep'))?>" >
													<a href="<?php echo site_url('/User');?>">
														<i ></i>
														<span class="title"><?php echo $this->lang->line('user');?>
														</span>
														<span class="arrow ">
														</span>
														<span class="selected">
														</span>
													</a>
													<ul class="sub-menu">

														<li>
															<a href="<?php echo site_url('/User');?>">
																<?php echo $this->lang->line('add_user');?>
															</a>
														</li>
														<li>
															<a href="<?php echo site_url('/Sales_rep');?>">
																<?php echo $this->lang->line('add_sales_rep');?>
															</a>
														</li>
														<li>
															<a href="<?php echo site_url('/User_access');?>">
																<?php echo $this->lang->line('user_access_panel');?>
															</a>
														</li>


													</ul>
												</li>

												<li class="<?php echo url_active(array('Transfer'))?>" >
													<a href="<?php echo site_url('/Transfer_warehouse');?>">
														<i ></i>
														<span class="title"><?php echo $this->lang->line('Warehouse');?>
														</span>
														<span class="arrow ">
														</span>
														<span class="selected">
														</span>
													</a>
													<ul class="sub-menu">

														<li>
															<a href="<?php echo site_url('/Transfer_warehouse');?>">
																Send Item's
															</a>
														</li>
														<li>
															<a href="<?php echo site_url('/Transfer_warehouse/transfer_list');?>">
																Send List's
															</a>
														</li>
														<li>
															<a href="<?php echo site_url('/Transfer_warehouse/receivable_transfer_details');?>">
																<?php echo $this->lang->line('warehouse_receive_list');?>
															</a>
														</li>

													</ul>
												</li>

												<!-- <li class="<?php echo url_active(array('Transfer'))?>" >
													<a href="<?php echo site_url('/Transfer_items');?>">
														<i ></i>
														<span class="title"><?php echo $this->lang->line('transfer');?>
														</span>
														<span class="arrow ">
														</span>
														<span class="selected">
														</span>
													</a>
													<ul class="sub-menu"> -->
													<!-- 	<li>
															<a href="<?php echo site_url('/Transfer_items/receivable_store_items');?>">
																Recive store items?
															</a>
														</li> -->
														<li>
															<a href="<?php echo site_url('/Transfer_items/pull_items');?>">
																Pull Items from Shops
															</a>
														</li> 
														<li>
															<a href="<?php echo site_url('/GetItems/loginCheck');?>">
																Item master
															</a>
														</li>
														<!-- <li>
															<a href="<?php echo site_url('/Transfer_items/receivable_transfer_details');?>">
																<?php echo $this->lang->line('transfer_receive_list');?>
															</a>
														</li>

														<li>
															<a href="<?php echo site_url('/Transfer_items/transfer_list');?>">
																<?php echo $this->lang->line('transfer_list');?>
															</a>
														</li>
														<li>
															<a href="<?php echo site_url('/Transfer_items');?>">
																<?php echo $this->lang->line('transfer');?>
															</a>
														</li> -->

												<!-- 	</ul>
											</li> -->
											<li class="<?php echo url_active(array('Vat'))?>" >
												<a href="<?php echo site_url('/Vat');?>">
													<i ></i>
													<span class="title"><?php echo $this->lang->line('vat');?>
													</span>
													<span class="arrow ">
													</span>
													<span class="selected">
													</span>
												</a>

											</li>
											<li>
												<a href="<?php echo site_url('/Shop_info');?>">
													<?php echo $this->lang->line('shop_info');?>
												</a>
											</li>
											<li class="<?php echo url_active(array('DB Backup'))?>" >
												<a href="<?php echo site_url('/Database_backup')?>">
													<i ></i>
													<span class="title">
														<?php echo $this->lang->line('database_backup');?>
													</span>
													<span class="arrow ">
													</span>
													<span class="selected">
													</span>
												</a>

											</li>
										<?php endif ?>
										<li class="<?php echo url_active(array('Update'))?>" >
											<a href="#update_software">
												<i ></i>
												<span class="title">
													<?php echo $this->lang->line('update');?>
												</span>
												<span class="arrow ">
												</span>
												<span class="selected">
												</span>
											</a>

										</li>

										<li>
											<a href="<?php echo site_url('/Contact_us');?>">
												<?php echo $this->lang->line('contact_us');?>
											</a>
										</li>
									</ul>
								</li>
								<?php if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager"): ?>
									<li class="<?php echo url_active(array('Reports'))?>" >
										<a href="<?php echo site_url('/Reports');?>">
											<i class="fa fa-bar-chart" ></i>
											<span class="title"><?php echo $this->lang->line('report');?>
											</span>
											<span class="arrow ">
											</span>
											<span class="selected">
											</span>
										</a>
									</li>
								<?php endif ?>
								<li class="<?php echo url_active(array('Video_tutorial'))?>" >
									<a href="<?php echo site_url('/Video_tutorial');?>">
										<i class="fa fa-file-video-o" ></i>
										<span class="title"><?php echo $this->lang->line('video_tut');?>
										</span>
										<span class="arrow ">
										</span>
										<span class="selected">
										</span>
									</a>
								</li>
							</ul>
							<!-- END SIDEBAR MENU -->
						</div>
					</div>
					<!-- END SIDEBAR -->

					<!-- BEGIN CONTENT -->
					<div class="page-content-wrapper">
						<div class="page-content">
							<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
							<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Modal title</h4>
										</div>
										<div class="modal-body">
											Widget settings form goes here
										</div>
										<div class="modal-footer">
											<button type="button" class="btn blue">Save changes</button>
											<button type="button" class="btn default" data-dismiss="modal">Close</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>

							<div id="sync_status_modal" class="modal fade">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Sync status</h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div id="sync_status_text" class="col-md-12">

										</div>
									</div>
								</div>
							</div>

							<!-- END STYLE CUSTOMIZER -->
							<!-- BEGIN PAGE HEADER-->
					<!-- 		<?php if ($this->uri->segment(1)!="Dashboard"  ): ?>

								<div class="col-md-12">
									<div class="col-md-3">
									</div>

									<div class="col-md-3">
										<a href="<?php echo site_url('/Buy');?>">
											<div class="dashboard-stat blue">
												<div class="visual">
													<i class="fa fa-bar-chart-o"></i>
												</div>
												<div class="details">
													<div class="number">
														<span data-counter="counterup" data-value="12,5"><?php echo $this->lang->line('buy')?></span>
													</div>

												</div>

											</div>
										</a>
									</div>

									<div class="col-md-3">
										<a href="<?php echo site_url('/Sell');?>">
											<div class="dashboard-stat purple" >
												<div class="visual">
													<i class="fa fa-bar-chart-o"></i>
												</div>
												<div class="details">
													<div class="number">
														<span data-counter="counterup" data-value="12,5"><?php echo $this->lang->line('sell')?></span> 
													</div>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-3">
									</div>
								</div>
							<?php endif ?> -->

							<script type="text/javascript">

								shortcut.add("Ctrl+Shift+B",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Buy')
								});

								
								shortcut.add("Ctrl+Shift+C",function() {
										window.location.assign('<?php echo base_url() ?>index.php/nagadanboi')  // Cash Box
									});

								shortcut.add("Ctrl+Shift+D",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Dashboard')
								});

								shortcut.add("Ctrl+Shift+I",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Item')
								});

								shortcut.add("Ctrl+Shift+X",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Exchange_with_vendors')
								});

								shortcut.add("Ctrl+Shift+E",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Expenditure')
								});

								shortcut.add("Ctrl+Shift+T",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Transfer_items')
								});

								shortcut.add("Ctrl+Shift+Y",function() {
									window.location.assign('<?php echo base_url() ?>index.php/Sync')
								});

								shortcut.add("Ctrl+Shift+U",function() {
									window.location.assign('<?php echo base_url() ?>index.php/User')
								});

								$is_vat = '<?php echo $this->session->userdata('is_vat_included')?>';
								if($is_vat == "yes"){
									$('#vat_include_id').prop('checked', true);
								}

								$(document).on('change', '#vat_include_id', function(event) {
									if ($('#vat_include_id').is(":checked")){
										$.post( "<?php echo site_url(); ?>/Dashboard/set_vat_include_yes" );	
										location.reload();

									}
									else{
										$.post( "<?php echo site_url(); ?>/Dashboard/set_vat_include_no" );	
										location.reload();	
									}

								});

								$("#sync_status").click(function(){
									$('#sync_status_modal').modal('show');
									$.get("<?php echo site_url(); ?>/Sync/syncCount", function(data, status){
										var result = JSON.parse(data);
										$('#sync_status_text').html("<strong>Total no. of synced data: " + result.synced + "<br><br>Total no. of unsynced data: " + result.unsynced+"</strong>");
									});
								});

							</script>


							<script language='VBScript'>
								Sub Print()
								OLECMDID_PRINT = 6
								OLECMDEXECOPT_DONTPROMPTUSER = 2
								OLECMDEXECOPT_PROMPTUSER = 1
								call WB.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,1)
								End Sub
								document.write "<object ID='WB' WIDTH=0 HEIGHT=0 CLASSID='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>"
							</script>


