<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN QUICK SIDEBAR -->
<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<!--Cooming Soon...-->
<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">

	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
</div>

<script>
	jQuery(document).ready(function() {    
  	Metronic.init(); // 
	Layout.init(); // init layout
	Demo.init(); // init demo features 
	QuickSidebar.init(); // init quick sidebar
});

</script>



<div id="responsive_modal" class="modal fade in animated shake" tabindex="-1" data-width="460">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Please login again <span id="user_name_selected" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				Sure to login
			</div>
      <!-- <div class="col-md-6">
  </div> -->
</div>
</div>
<div class="modal-footer">
	<button type="button" data-dismiss="modal" class="btn btn-default">No</button>
	<button type="button" id="user_delete_confirm_y" class="btn blue">Yes</button>
</div>
</div>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>


