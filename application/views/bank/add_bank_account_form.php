<?php
/**
 * Form Name:"Bank Account Form" 
 *
 * This is form for insert , edit and update expenditures informations.
 * 
 * @link   Bank_acc_create/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assest/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/> -->
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css">
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-danger" id="insert_failure_model" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed_modal');?></div>
        <div class="alert alert-success" id="insert_success_model" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success_modal');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="alert alert-success" id="active_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_active_success');?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption left">
                    <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
                </div>
            </div>
            <div class="portlet-body form" id="bank_acc_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'bank_acc_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_exp_type');?> <sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-9 input-icon">
                            <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('type_select_tt'); ?>"></i> 
                            <input type="text" autocomplete="off" name="banks_name" placeholder="<?php echo $this->lang->line('left_exp_type_placeholder');?> " id="bank_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="banks_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="bank_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_acc_name')?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_name" placeholder="<?php echo $this->lang->line('left_acc_name_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_acc')?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_num" placeholder="<?php echo $this->lang->line('left_acc_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_branch')?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_branch" placeholder="<?php echo $this->lang->line('left_branch_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?> 
                        </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_des" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" id="bank_acc_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="bank_acc_edit_div" style="display:none">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'bank_acc_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');
                ?>
                <input type="text" class="form-control" name="bank_acc_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_portlet_title');?> <sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="banks_id_edit" id="bank_select_edit" data-live-search="true">
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_acc_name')?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_name_edit" placeholder="<?php echo $this->lang->line('left_acc_name_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_acc')?><sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_num_edit" placeholder="<?php echo $this->lang->line('left_acc_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_branch')?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_branch_edit" placeholder="<?php echo $this->lang->line('left_branch_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_description');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="bank_acc_des_edit" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" id="acc_edit_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption right">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('right_exp_type');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_ac');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_balance');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_branch');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_date');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_edit_delete');?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('right_exp_type');?></th>
                            <th> <?php echo $this->lang->line('right_ac');?></th>
                            <th><?php echo $this->lang->line('right_balance');?></th>
                            <th><?php echo $this->lang->line('right_branch');?></th>
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th><?php echo $this->lang->line('right_edit_delete');?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>
<div id="responsive_modal_active" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_activate');?> <span id="selected_name_activated" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_activate_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="activation_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<!-- Expenditure Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="expenditure_type_form_modal" class="modal fade">    
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('expenditure_type/add_expenditure_type_form_only');?>
        </div>
    </div>
</div>
<!-- Expenditure MOdal Form End -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->   
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).on('submit', '#bank_create_form', function(event) {
        event.preventDefault();
        var data = {};
        data.expenditure_types_name = $('input[name=expenditure_types_name_2]').val();
        data.bank_acc_des = $('input[name=bank_acc_des]').val();
        data.bank_acc_num = $('input[name=bank_acc_num]').val();
        data.bank_acc_branch = $('input[name=bank_acc_branch]').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();
        $.post('<?php echo site_url() ?>/banks_create/save_bank_info',data).done(function(data1, textStatus, xhr) {
            var data2 = $.parseJSON(data1);
            if(data2 =="No Permisssion")
            {
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            }
            else if(data2.success==0)
            {
                $.each(data2.error, function(index, val) 
                {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    $('input[name='+index+']').parent().parent().find('.help-block').show();
                });
            }
            else
            {
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();
                $('#expenditure_type_form_modal').modal('hide');
                $('input[name= expenditure_types_name_2]').val('');
                $('input[name= expenditure_types_description]').val('');
                $('#insert_success_model').slideDown();
                setTimeout( function(){$('#insert_success_model').slideUp();}, 3000 );
            }
        }).error(function() {
            $('#insert_failure_model').slideDown();
            setTimeout( function(){$('#insert_failure_model').slideUp()}, 3000 );
        });    
    });    
</script>
<script>
    /*loading tooltip*/
    jQuery(document).ready(function($) {
        $("[data-toggle='popover']").popover();
    });
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 2 || idx == 3 || idx == 5) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table_bank_accounts = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/bank_acc_create/all_bank_acc_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [4, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/
    });
    /*data table customize search starts*/
    table_bank_accounts.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
    $('table').on('click', '.edit_acc',function(event) {
        event.preventDefault();
        var bank_acc_id = $(this).attr('id').split('_')[1];
        $('#current_bank_acc_id').remove();
        $.get('<?php echo site_url();?>/bank_acc_create/get_bank_acc_info/'+bank_acc_id).done(function(data) {
            var bank_acc_data = $.parseJSON(data);
            if(bank_acc_data =="No Permission"){
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );            
            }
            else {
                $('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
                $('select[name = banks_id_edit]').val(bank_acc_data.bank_acc_info_by_id.banks_id);
                $('#bank_select_edit').html('<option value="'+bank_acc_data.bank_acc_info_by_id.banks_id+'" selected="selected">'+bank_acc_data.bank_acc_info_by_id.banks_name+'</option>');
                $('#bank_select_edit').trigger('change');
                $('input[name = bank_acc_num_edit]').val(bank_acc_data.bank_acc_info_by_id.bank_acc_num);
                $('input[name = bank_acc_name_edit]').val(bank_acc_data.bank_acc_info_by_id.bank_acc_name);
                $('input[name = bank_acc_branch_edit]').val(bank_acc_data.bank_acc_info_by_id.bank_acc_branch);
                $('input[name = bank_acc_des_edit]').val(bank_acc_data.bank_acc_info_by_id.bank_acc_des);
                $('#bank_acc_insert_div').slideUp('fast');
                $('#bank_acc_edit_div').slideDown('slow');
                $('input[name=bank_acc_id]').val(bank_acc_id);
            }
        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });
    $('#bank_acc_edit_form').submit(function(event) {
        event.preventDefault();
        $('#responsive_modal_edit').modal('show');
    });
    $('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
        event.preventDefault();
        var post_data={};
        post_data.bank_acc_num_edit = $('input[name = bank_acc_num_edit]').val();
        post_data.bank_acc_name_edit = $('input[name = bank_acc_name_edit]').val();
        post_data.bank_acc_branch_edit = $('input[name = bank_acc_branch_edit]').val();
        post_data.bank_acc_des_edit = $('input[name = bank_acc_des_edit]').val();
        post_data.bank_acc_id = $('input[name=bank_acc_id]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
        $.post('<?php echo site_url() ?>/bank_acc_create/update_bank_acc_info',post_data).done(function(data, textStatus, xhr) {
            var data2= $.parseJSON(data);
            if(data2['success'])
            {
                $('#update_success').slideDown();
                $('#responsive_modal_edit').modal('hide');
                setTimeout( function(){$('#update_success').slideUp()}, 3000 );
                table_bank_accounts.ajax.reload(null, false);
                $('#bank_acc_insert_div').slideDown('slow');
                $('#bank_acc_edit_div').slideUp('fast');
                $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
                $('.help-block').hide();
            }
            else if(!Object.keys(data2.error).length)
            {
                alert("<?php echo $this->lang->line('alert_edit_failed');?>");                
            }
            else
            {
                $('#responsive_modal_edit').modal('hide');
                $('.help-block').hide();
                $('.form-group.has-error').removeClass('has-error');
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    $('input[name='+index+']').parent().parent().find('.help-block').show();
                });            
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('alert_edit_failed');?>");
        });
    });
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX Booostrap for Expenditure Type Selection
  $("#bank_select").keyup(function(event) 
  {
      $("#bank_select_result").show();
      $("#bank_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachExpendtureType = $("#bank_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_acc_create/search_bank_by_name',{q: seachExpendtureType,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.banks_id+'">'+val.banks_name+'</td></tr>';
              });
              $("#bank_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_select_result").on('click', 'td', function(event) {
      $('input[name="banks_name"]').val($(this).html());
      $('input[name="banks_id"]').val($(this).attr('data'));
      $("#bank_select_result").hide();
  });

  var expenditures_id = '' ;
  var selected_name = '';
  $('table').on('click', '.delete_acc',function(event) {
    event.preventDefault();
    bank_acc_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});
  $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
        // var expenditures_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'bank_acc_id' : bank_acc_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        $.post('<?php echo site_url();?>/bank_acc_create/delete_bank_acc/'+bank_acc_id,post_data).done(function(data) {
            if(data == "No Permission"){
                $('#responsive_modal_delete').modal('hide');
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                table_bank_accounts.row($('#delete_'+expenditures_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('alert_delete_failure');?>");                
        });
    });
// ajax form submit starts....
$('#bank_acc_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.banks_id = $('input[name=banks_id]').val();
    data.bank_acc_num = $('input[name=bank_acc_num]').val();
    data.bank_acc_name = $('input[name=bank_acc_name]').val();
    data.bank_acc_branch = $('input[name=bank_acc_branch]').val();
    data.bank_acc_des = $('input[name=bank_acc_des]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/bank_acc_create/save_bank_acc_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 == "No Permission"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else if(data2.success==0)
        {
            $('.help-block').hide();
            $('.form-group.has-error').removeClass('has-error');
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-danger');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();
            });
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('input[name= bank_acc_num]').val('');
            $('input[name= bank_acc_name]').val('');
            $('input[name= banks_id]').val('');
            $('input[name= bank_acc_branch]').val('');
            $('input[name= bank_acc_des]').val('');
            $('#bank_select').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table_bank_accounts.ajax.reload(null, false);
        }
    }).error(function() {
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
    $('#bank_select').selectpicker('val','');
});
// ajax form submit end.......
$('#bank_acc_edit_form').on('click', '#acc_edit_cancel', function(event) {
    event.preventDefault();
    $('#bank_acc_insert_div').slideDown('slow');
    $('#bank_acc_edit_div').slideUp('fast');
    $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
    /* Act on the event */
});
$('.date-picker').datepicker({
    format: 'yyyy-mm-dd',
    rtl: Metronic.isRTL(),
    orientation: "left",
    autoclose: true
});
$("#bank_acc_form").on('click', '#bank_acc_cancel', function(event) {
    event.preventDefault();
    $('.help-block').hide();
    $('.form-group.has-error').removeClass('has-error');
    $('#bank_select').val('');
    $('input[name= bank_acc_num]').val('');
    $('input[name= bank_acc_name]').val('');
    $('input[name= bank_acc_branch]').val('');
    $('input[name= bank_acc_des]').val('');
});
$('table').on('click', '.active_acc', function(event) {
    event.preventDefault();
    bank_acc_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name_activated').html(selected_name);
});
$('#responsive_modal_active').on('click', '#activation_confirmation',function(event) {
    event.preventDefault();
    var post_data ={
        'bank_acc_id' : bank_acc_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/bank_acc_create/activate_acc/'+bank_acc_id,post_data).done(function(data)
    {
        if(data == "No Permission"){
            $('#responsive_modal_active').modal('hide');
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else if(data == "success")
        {
            $('#responsive_modal_active').modal('hide');
            $('#active_success').slideDown();
            setTimeout( function(){$('#active_success').slideUp()}, 5000 );
            table_bank_accounts.ajax.reload(null, false); 
        }
        else if(data == "failed")
        {
            $('#responsive_modal_active').modal('hide');
            $('#active_failure').slideDown();
            setTimeout( function(){$('#active_failure').slideUp()}, 5000 );        
        }
    }).error(function() {
        alert("<?php echo $this->lang->line('alert_insert_failed');?>");                
    });
});
</script>