<?php 
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
$this->output->set_header("Pragma: no-cache");
/**
 * Form Name: "Bank Deposit Withdrawal Form" 
 *
 * This is form for insert , edit and update bank accounts.
 * 
 * @link   Bank_dpst_wdrl/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
                </div>
            </div>
            <div class="portlet-body form" id="bank_dw_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'bank_dw_form','class' => 'form-horizontal');
                echo form_open('',  $form_attrib, '');?>
                <div class="form-body">
                    <!--         <h3 class="form-section">বর্তমান টাকাঃ <b><span id="current_amount_sum" style="color:red;"><?php echo $final_cashbox_info['total_cashbox_amount'] ?>৳</span></b></h3> -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_type');?> <sup><i class="fa fa-star custom-required"></i></sup>
                                </label>
                                <div class="col-md-9">
                                    <select class="form-control" id="cash_type_id" name="cash_type">
                                        <option value="deposit"><?php echo $this->lang->line('left_type_deposit');?></option>
                                        <option  value="withdrawal"><?php echo $this->lang->line('left_type_withdrawal');?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc')?> <sup><i class="fa fa-star custom-required"></i></sup>
                        </label>
                        <div class="col-md-9">
                            <input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('select_acc_pl');?> " id="bank_acc_select" class="form-control">
                            <input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                            <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="number" step="any" min="0" class="form-control" id="depo_withdr_amountt" name="deposit_withdrawal_amount" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>">
                                    <span class="normalize help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_date'); ?></label>
                                <div class="col-md-9">
                                    <input class="form-control form-control-inline input-medium date-picker" autocomplete="off" \
                                    size="16" type="text" value="" name="date" placeholder="<?php echo $this->lang->line('left_date');?>" />
                                    <span class="help-block"></span>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_description');?></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="cash_description" placeholder="<?php echo $this->lang->line('left_description_placeholder');?>" rows="3" cols="50"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="reset" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" id="save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="bank_dw_edit_div" style="display:none">
                <?php $form_attrib = array('id' => 'bank_dw_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <input type="text" class="form-control" name="bank_statement_id" style="display:none">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_type');?><sup><i class="fa fa-star custom-required"></i></sup>
                    </label>
                    <div class="col-md-9">
                        <select class="form-control" id="cash_type_id_edit" name="cash_type_edit">
                            <option value="deposit"><?php echo $this->lang->line('left_type_deposit');?></option>
                            <option value="withdrawal"><?php echo $this->lang->line('left_type_withdrawal');?></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_portlet_title');?> <sup><i class="fa fa-star custom-required"></i></sup>
                    </label>
                    <div class="col-md-4">
                        <select class="form-control" name="bank_acc_id_edit" id="bank_acc_select_edit" data-live-search="true">
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_amount');?><sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="number" step="any" min="0" class="form-control" name="deposit_withdrawal_amount_edit" placeholder="<?php echo $this->lang->line('left_amount_placeholder');?>">
                        <pre class="normalize help-block"></pre>
                        <!-- <span class="help-block"></span> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_description');?></label>
                    <div class="col-md-9">
                        <textarea class="form-control" name="cash_description_edit" placeholder="<?php echo $this->lang->line('left_description_placeholder');?>" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn default pull-right" style="margin-left: 8px" id="edit_dw_cancle"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" id="edit_save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>   
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('right_type');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_ac');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_amount');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_description');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_date');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_edit_delete');?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <td>test</td> -->
                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('right_type');?></th>
                            <th>
                             <?php echo $this->lang->line('bank_dw');?>
                         </th>
                         <th><?php echo $this->lang->line('right_amount');?></th>
                         <th><?php echo $this->lang->line('right_description');?></th>
                         <th>
                            <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control dont-search" name="from_create_date">
                                <span class="input-group-addon">
                                    to
                                </span>
                                <input type="text" class="form-control dont-search" name="to_create_date">
                            </div>
                        </th>
                        <th><?php echo $this->lang->line('right_edit_delete');?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
        <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
    </div>
</div>
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>-->

<!-- For customize datatable Starts -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!-- For Customize Datatable End -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- tooltip -->
<script>
    /*loading tooltip*/
    jQuery(document).ready(function($) {
        $("[data-toggle='popover']").popover();

    });
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });
    // table = $('#sample_2').DataTable();
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 2 || idx == 3 || idx == 5) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/bank_dpst_wdrl/all_bank_dw_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [4, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/
    });
    /*data table customize search starts*/
    table.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
</script>
<script type="text/javascript">

    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
      $("#bank_acc_select_result").show();
      $("#bank_acc_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
      $('input[name="bank_acc_num"]').val($(this).html());
      $('input[name="bank_acc_id"]').val($(this).attr('data'));
      $("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection

  $('table').on('click', '.edit_statement',function(event) {
    event.preventDefault();
    $('.has-error').removeClass('has-error');
    $('.help-block').text('');
    var bank_statement_id = $(this).attr('id').split('_')[1];
    $('#current_statement_id').remove();
    $.get('<?php echo site_url();?>/bank_dpst_wdrl/get_statement_info/'+bank_statement_id).done(function(data) {
        var statement_data = $.parseJSON(data);
        if(statement_data == "No Permission"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
        }
        else{
            // $('#responsive_modal_edit').modal('hide');
            $('#form_header_text').text("<?php echo $this->lang->line('edit_cashbox');?>");
            $('select[name = bank_acc_id_edit]').val(statement_data.all_bank_statement_by_id.bank_acc_id);
            $('#bank_acc_select_edit').html('<option value="'+statement_data.all_bank_statement_by_id.bank_acc_id+'" selected="selected">'+statement_data.all_bank_statement_by_id.bank_acc_num+'</option>');
            $('#bank_acc_select_edit').trigger('change');
            $('select[name = cash_type_edit]').val(statement_data.all_bank_statement_by_id.cash_type);
            $('input[name = deposit_withdrawal_amount_edit]').val(statement_data.all_bank_statement_by_id.deposit_withdrawal_amount);
            $('textarea[name = cash_description_edit]').val(statement_data.all_bank_statement_by_id.cash_description);
            $('#bank_dw_insert_div').slideUp('fast');
            $('#bank_dw_edit_div').slideDown('slow');
            $('input[name=bank_statement_id]').val(bank_statement_id);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
  $('#bank_dw_edit_form').submit(function(event) {
    event.preventDefault();
    $('#responsive_modal_edit').modal('show');
});
  $('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
    event.preventDefault();
    $('#edit_save_id').prop('disabled', true);
    var post_data={};
    post_data.bank_acc_id_edit = $('select[name = bank_acc_id_edit]').val();
    post_data.cash_type_edit = $('#cash_type_id_edit').val();
    post_data.deposit_withdrawal_amount_edit = $('input[name = deposit_withdrawal_amount_edit]').val();
    post_data.cash_description_edit = $('textarea[name = cash_description_edit]').val();
    post_data.bank_statement_id = $('input[name= bank_statement_id]').val();
    post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/bank_dpst_wdrl/update_statement_info',post_data).done(function(data, textStatus, xhr) {
        var data2 = $.parseJSON(data);
        if(data2['success'])
        {
            $('#edit_save_id').prop('disabled', false);
            $('#update_success').slideDown();
            $('#responsive_modal_edit').modal('hide');
            setTimeout( function(){$('#update_success').slideUp()}, 3000 );
            var row_data = table.row( $('#edit_'+post_data.cashbox_id).closest('tr')[0] ).data();
            row_data[0] = post_data.cash_type_edit;
            table.row( $('#edit_'+post_data.cashbox_id).closest('tr')[0] ).data(row_data).draw();
            $('#bank_dw_insert_div').slideDown('slow');
            $('#bank_dw_edit_div').slideUp('fast');
            $('#form_header_text').text("<?php echo $this->lang->line('right_portlet_title');?>");
        }
        else if(!Object.keys(data2.error).length)
        {
            $('#edit_save_id').prop('disabled', false);
            alert("Cashbox update failed");                
        }
        else
        {
            $('#edit_save_id').prop('disabled', false);
            $('#responsive_modal_edit').modal('hide');
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }
    }).error(function() {
        $('#edit_save_id').prop('disabled', false);
        alert("Connection failure.");
    });
});
  /* cancel button clear all from data*/
  $('#edit_dw_cancle').click(function(event) {
    event.preventDefault();
    $('#bank_dw_insert_div').slideDown('slow');
    $('#bank_dw_edit_div').slideUp('fast');
    $('#form_header_text').text("<?php echo $this->lang->line('right_portlet_title');?>");
});
  /* cancle button end*/
  /* Delete Cashbox Info  Starts*/
  var cashbox_id = '' ;
  var selected_name = '';
  $('table').on('click', '.delete_statement',function(event) {
    event.preventDefault();
    statement_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});
  $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
    // var cashbox_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'bank_statement_id' : statement_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/bank_dpst_wdrl/delete_statement/'+statement_id,post_data).done(function(data) {
        if(data == "No Permission")
        {
            $('#responsive_modal_delete').modal('hide');
            // $('#access_failure').slideDown();
            // setTimeout( function(){$('#access_failure').slideUp()}, 3000 );     
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"};    
        }
        else if(data == "success")
        {
            // $('#delete_success').slideDown();
            // setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
            toastr.success('<?php echo $this->lang->line('alert_delete_success');?>', 'Success', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            $('#responsive_modal_delete').modal('hide');
            table.row($('#delete_'+cashbox_id).parents('tr')).remove().draw();
        }
        else
        {
            // $('#delete_failure').slideDown();
            // setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );   
            toastr.error('Delete Failed. Try Again', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});
  /* Delete Cashbox Info End*/
// ajax form submit starts....
$('#bank_dw_form').submit(function(event) {
    event.preventDefault();
    $('#save_id').prop('disabled', true);
    var data = {};
    data.cash_type = $('#cash_type_id').val();
    data.bank_acc_id = $('input[name=bank_acc_id]').val();
    data.deposit_withdrawal_amount = $('input[name=deposit_withdrawal_amount]').val();
    data.date = $('input[name=date]').val();
    data.cash_description = $('textarea[name=cash_description]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/bank_dpst_wdrl/save_deposit_withdrawal_amount_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 == "Not Allowed"){
            $('#save_id').prop('disabled', false);
            // $('#withdrawal_failure').slideDown();
            // setTimeout( function(){$('#withdrawal_failure').slideUp()}, 5000 );
            toastr.error('Withdrawal Failed. Refresh And Try Again', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else if(data2 =="No Permission"){
            $('#save_id').prop('disabled', false);
            // $('#access_failure').slideDown();
            // setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
            toastr.error('<?php echo $this->lang->line('alert_access_failure');?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else if(data2.success==0)
        {
            $('#save_id').prop('disabled', false);
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }
        else{
            $('#save_id').prop('disabled', false);
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('input[name=deposit_withdrawal_amount]').val('');
            $('input[name= bank_acc_id]').val('');
            $('input[name= bank_acc_num]').val('');
            $('textarea[name=cash_description]').val('');
            $('input[name= date]').val('');
            // $('#insert_success').slideDown();
            // setTimeout( function(){$('#insert_success').slideUp();}, 5000 );
            toastr.success('<?php echo $this->lang->line('alert_insert_success');?>', 'Success', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            table.ajax.reload(null, false);
            $('#cash_type_id').val('deposit');
        }
    }).error(function() {
        $('#save_id').prop('disabled', false);
        // $('#insert_failure').slideDown();
        // setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
        toastr.error('<?php echo $this->lang->line('alert_insert_failed');?>', 'Failed', {timeOut: 6000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    });    
});
</script>