<?php
/**
 * Form Name:"Bank Form" 
 *
 * This is form for insert , edit and update bank info.
 * 
 * @link   Bank_create/index
 * @author shoaib <shofik.shoaib@gmail.com>
 */
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- For Responsive Datatable Ends -->
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption left">
                    <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title');?></span>
                </div>
            </div>
            <div class="portlet-body form" id="bank_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'bank_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>
                <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="banks_name" placeholder="<?php echo $this->lang->line('left_bank_name_placeholder');?>">
                            <span class="help-block"></span></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_abbe');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="banks_abbe" placeholder="<?php echo $this->lang->line('left_bank_abbe');?>">
                                <span class="help-block"></span></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('charge_percentage');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="number" min="0" step=".01" class="form-control" name="bank_charge_percentage" placeholder="<?php echo $this->lang->line('charge_percentage');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_description');?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="banks_description" placeholder="<?php echo $this->lang->line('left_bank_description_placeholder');?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="reset" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                            <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                        </div>
                        <?php echo form_close();?>
                        <!-- END FORM-->
                    </div>
                    <div class="portlet-body form" id="bank_edit_div" style="display:none">
                        <!-- BEGIN FORM-->
                        <?php
                        $form_attrib = array('id' => 'bank_edit_form','class' => 'form-horizontal form-bordered');
                        echo form_open('',  $form_attrib, '');
                        ?>
                        <input type="text" class="form-control" name="banks_id" style="display:none">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="banks_name_edit" placeholder="<?php echo $this->lang->line('left_bank_name_placeholder');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_abbe');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="banks_abbe_edit" placeholder="<?php echo $this->lang->line('left_bank_abbe');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('charge_percentage');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="number" min="0" step=".1" class="form-control" name="bank_charge_percentage_edit" placeholder="<?php echo $this->lang->line('charge_percentage');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_bank_description');?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="banks_description_edit" placeholder="<?php echo $this->lang->line('left_bank_description_placeholder');?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" class="btn default pull-right" id="bank_edit_cancel" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                            <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('left_update');?></button>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <div class="caption right">
                            <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title');?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('right_bank');?></th>
                                    <th><?php echo $this->lang->line('right_description');?></th>
                                    <th><?php echo $this->lang->line('right_date');?></th>
                                    <th>
                                        <?php echo $this->lang->line('right_edit_delete');?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>  
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?php echo $this->lang->line('right_bank');?></th>
                                    <th><?php echo $this->lang->line('right_description');?></th>
                                    <th>
                                        <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control dont-search" name="from_create_date">
                                            <span class="input-group-addon">
                                                to
                                            </span>
                                            <input type="text" class="form-control dont-search" name="to_create_date">
                                        </div>
                                    </th>
                                    <th><?php echo $this->lang->line('right_edit_delete');?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $this->lang->line('alert_delete_details');?>
                    </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>
<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>-->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- <script src="<?php echo CURRENT_ASSET;?>assets/build/react.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/react-dom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/expenditure_type.js"></script>
<script type="text/babel" src="<?php echo CURRENT_ASSET;?>assets/components/expenditure_type.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    /*loading tooltip*/
    jQuery(document).ready(function($) {
        $("[data-toggle='popover']").popover();

    });
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 3) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table_bank = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/bank_create/all_bank_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/
    });
    /*data table customize search starts*/
    table_bank.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
    $('table').on('click', '.edit_bank',function(event) {
        event.preventDefault();
        var banks_id = $(this).attr('id').split('_')[1];
        $('#current_banks_id').remove();
        $.get('<?php echo site_url();?>/bank_create/get_bank_info/'+banks_id).done(function(data) {
            var bank_data = $.parseJSON(data);
            if(bank_data =="No Permisssion"){
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 5000 );                
            }
            else {
                $('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
                $('input[name = banks_name_edit]').val(bank_data.bank_info_by_id.banks_name);
                $('input[name = banks_abbe_edit]').val(bank_data.bank_info_by_id.banks_abbe);
                $('input[name = banks_description_edit]').val(bank_data.bank_info_by_id.banks_description);
                $('input[name = bank_charge_percentage_edit]').val(bank_data.bank_info_by_id.charge_percentage);
                $('#bank_insert_div').slideUp('fast');
                $('#bank_edit_div').slideDown('slow');
                $('input[name=banks_id]').val(banks_id);
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('comm_err');?>");                
        });
    });
    $('#bank_edit_form').submit(function(event) {
        event.preventDefault();
        $('#responsive_modal_edit').modal('show');
    });
    $('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
        event.preventDefault();
        var post_data={};
        post_data.banks_name_edit = $('input[name = banks_name_edit]').val();
        post_data.banks_abbe_edit = $('input[name = banks_abbe_edit]').val();
        post_data.banks_description_edit = $('input[name = banks_description_edit]').val();
        post_data.banks_id = $('input[name=banks_id]').val();
        post_data.charge_percentage_edit= $('input[name=bank_charge_percentage_edit]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/bank_create/update_bank_info',post_data).done(function(data, textStatus, xhr) {
            var data2= $.parseJSON(data);
            if(data2['success'])
            {
                $('#update_success').slideDown();
                $('#responsive_modal_edit').modal('hide');
                setTimeout( function(){$('#update_success').slideUp()}, 5000 );
                table_bank.ajax.reload(null, false);
                $('#bank_insert_div').slideDown('fast');
                $('#bank_edit_div').slideUp('slow');
                $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
                $('.help-block').hide();
                $('.form-group.has-error').removeClass('has-error');
            }
            else if(!Object.keys(data2.error).length)
            {
                alert("<?php echo $this->lang->line('alert_edit_failed');?>");                
            }
            else
            {   
                $('#responsive_modal_edit').modal('hide');
                $('.help-block').hide();
                $('.form-group.has-error').removeClass('has-error');
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    $('input[name='+index+']').parent().parent().find('.help-block').show();
                });            
            }
        }).error(function() {

            alert("<?php echo $this->lang->line('alert_edit_failed');?>");
        });

    });
    /* Delete Bank Info Starts*/
    var banks_id = '' ;
    var selected_name = '';
    $('table').on('click', '.delete_bank',function(event) {
        event.preventDefault();
        banks_id = $(this).attr('id').split('_')[1];
        selected_name = $(this).parent().parent().find('td:first').text();
        $('#selected_name').html(selected_name);
    });
    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        var post_data ={
            'banks_id' : banks_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 
        $.post('<?php echo site_url();?>/bank_create/delete_bank/'+banks_id,post_data).done(function(data) {
            if(data == "No Permisssion"){
                $('#responsive_modal_delete').modal('hide');
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 5000 );
                table_bank.ajax.reload(null, false);
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 5000 );                
            }
        }).error(function() {
            alert("<?php echo $this->lang->line('comm_err');?>");                
        });
    });
// ajax form submit starts....
$('#bank_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.banks_name = $('input[name=banks_name]').val();
    data.banks_abbe = $('input[name=banks_abbe]').val();
    data.charge_percentage = $('input[name=bank_charge_percentage]').val();;
    data.banks_description = $('input[name=banks_description]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/bank_create/save_bank_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 =="No Permisssion"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
        else if(data2.success==0)
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('input[name='+index+']').parent().parent().find('.help-block').show();
            });
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('input[name= banks_name]').val('');
            $('input[name= banks_abbe]').val('');
            $('input[name= banks_description]').val('');
            $('input[name= bank_charge_percentage]').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 5000 );
            table_bank.ajax.reload(null, false);
        }
    }).error(function() {
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
    });    
});
// ajax form submit end.......
$('#bank_edit_form').on('click', '#bank_edit_cancel', function(event) {
    event.preventDefault();
    $('#bank_insert_div').slideDown('slow');
    $('#bank_edit_div').slideUp('fast');
    $('#form_header_text').text("<?php echo $this->lang->line('left_portlet_title');?>");
    $('.form-group.has-error').removeClass('has-error');
    $('.help-block').hide();
    /* Act on the event */
});

</script>