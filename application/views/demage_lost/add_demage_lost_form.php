<?php 
$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
$this->output->set_header("Pragma: no-cache");

/**
 * Form Name:"Demage and lost Form" 
 *
 * This is  form for insert,edit and delete demage and lost informations.
 * 
 * @link   Demage_and_lost/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/css/ajax-bootstrap-select.css"/>
<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/bootstrap-select.min.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<!-- END PAGE LEVEL STYLES -->


<div class="row">
    <div class="col-md-6">

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_header');?></span>
                </div>
                
            </div>
            <div class="portlet-body form" id="demage_lost_insert_div">
                <!-- BEGIN FORM-->
                
                <?php $form_attrib = array('id' => 'demage_lost_form','class' => 'form-horizontal');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_type');?>
                                </label>
                                <div class="col-md-9">
                                    <select class="form-control" id="demage_lost_type_id" name="demage_lost_type">

                                        <option value="damage"><?php echo $this->lang->line('demage');?></option>
                                        <option value="lost"><?php echo $this->lang->line('lost');?></option>
                                        
                                    </select>
                                </div>
                            </div>

                <!--             <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_item');?></label>
                                <div class="col-md-9">
                                    <select class="form-control" name="items_id" id="item_select" data-live-search="true">
                                    </select>
                                    <span class="help-block" style="display:none"><?php echo $this->lang->line('left_valid_item');?></span>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_item'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                                </label>
                                <div class="col-md-6 input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_select_tt'); ?>"></i> 
                                    <input type="text" autocomplete="off" name="items_name" placeholder="<?php echo $this->lang->line('left_item'); ?>" id="item_select" class="form-control">
                                    <input type="hidden" autocomplete="off" name="item_spec_set_id" id="spec_id_for_get" class="form-control">
                                    <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                    </table>
                                    <span class="help-block" style="display:none"><?php echo $this->lang->line('left_valid_item');?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_quantity');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="number" min="1" step="1" class="form-control" name="demage_lost_quantity"  placeholder="<?php echo $this->lang->line('left_quantity');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group" style="display: none" id="imei_entry_div">
                                <label class="control-label col-md-3">IMEI/ Serial</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="demaged_imei" id="demaged_imei_number"  placeholder="IMEI Barcode">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_buying_price');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-9">
                                    <input type="number" min="1" step="1" class="form-control"  name="item_buying_price" id="buying_price_id" placeholder="<?php echo $this->lang->line('left_buying_price');?>">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_description');?></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="demage_lost_description" placeholder="<?php echo $this->lang->line('left_description');?>" rows="3" cols="50"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="reset" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" id="save_id" class="btn green pull-right"><?php echo $this->lang->line('left_save');?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="demage_lost_edit_div" style="display:none">

                <?php $form_attrib = array('id' => 'demage_lost_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <input type="text" class="form-control" name="demage_lost_id" style="display:none">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_type');?>
                    </label>
                    <div class="col-md-9">
                        <select class="form-control" id="demage_lost_type_id_edit" name="demage_lost_type_edit">

                            <option value="damage"><?php echo $this->lang->line('demage');?></option>
                            <option value="lost"><?php echo $this->lang->line('lost');?></option>

                        </select>
                    </div>
                </div>

<!-- 				<div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_item');?></label>
                    <div class="col-md-9">
                        <select class="form-control" name="items_id_edit" id="item_select_edit" data-live-search="false" readonly="true">
                        </select>
                        <span class="help-block" style="display:none"><?php echo $this->lang->line('left_valid_item');?></span>
                    </div>
                </div> -->
<!-- 				<div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_item'); ?>
                    </label>
                    <div class="col-md-6">

                        <input type="text" autocomplete="off" name="items_name_edit" placeholder="<?php echo $this->lang->line('left_item'); ?>" id="item_select_edit" class="form-control">
                        <input type="hidden" autocomplete="off" name="item_spec_set_id_edit" id="spec_id_for_get_edit" class="form-control">
                        <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                        </table>
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_item'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                    </label>
                    <div class="col-md-9">
                        <select class="form-control" name="item_spec_set_id_edit" id="item_select_edit" data-live-search="true">
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_quantity');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="number" min="1" step="1" class="form-control" name="demage_lost_quantity_edit" id="quantity_edit_id" placeholder="<?php echo $this->lang->line('left_quantity');?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group" style="display: none" id="imei_entry_edit_div">
                    <label class="control-label col-md-3">IMEI/ Serial</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="demaged_imei_edit" id="demaged_imei_number_edit" readonly="true"  placeholder="IMEI Barcode">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_buying_price');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="number" min="1" step="1" class="form-control" readonly="true" name="item_buying_price_edit" id="buying_price_id_edit" placeholder="<?php echo $this->lang->line('left_buying_price');?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_description');?></label>
                    <div class="col-md-9">
                        <textarea class="form-control" name="demage_lost_description_edit" placeholder="<?php echo $this->lang->line('left_description');?>" rows="3" cols="50"></textarea>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="button" class="btn default pull-right" style="margin-left: 8px" id="edit_demage_lost_cancle"><?php echo $this->lang->line('left_cancle');?></button>
                    <button type="submit" class="btn green pull-right " id="edit_save_id" ><?php echo $this->lang->line('left_save');?></button>
                    
                </div>
                <?php echo form_close();?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_header');?>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('right_type');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_item_name');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_quantity');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_description');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_date');?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('right_edit_delete');?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('right_type');?></th>
                            <th><?php echo $this->lang->line('right_item_name');?></th>
                            <th><?php echo $this->lang->line('right_quantity');?></th>
                            <th><?php echo $this->lang->line('right_description');?></th>
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th><?php echo $this->lang->line('right_edit_delete');?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>

<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?><span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>-->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/ajax_bootstrap_select/js/ajax-bootstrap-select.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->

<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->

<!-- Boostrap Tooltip -->
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/components-form-tools.js"></script>
<!-- End of Tooltip -->
<!-- tooltip -->
<script>
    var csrf = $("input[name='csrf_test_name']").val();


// Start of AJAX Booostrap for item selection
// var options = {
// 	ajax          : {
// 		url     : '<?php echo site_url(); ?>/buy/search_item_by_name',
// 		type    : 'POST',
// 		dataType: 'json',
//             	// Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
//             	// automatically replace it with the value of the search query.
//             	data    : {
//             		q: '{{{q}}}',
//             		csrf_test_name: csrf,
//             	}
//             },
//             locale        : {
//             	emptyTitle: "<?php echo $this->lang->line('left_valid_item');?>"
//             },
//             log           : 3,
//             preprocessData: function (data) {
//             	var i, l = data.length, array = [];
//             	if (l) {
//             		for (i = 0; i < l; i++) {
//             			array.push($.extend(true, data[i], {
//             				text : data[i].items_name,
//             				value: data[i].items_id,
//             				data : {
//             					subtext: '',
//             					content: "<img height='50px' width='50px' src='<?php echo base_url() ?>"+data[i].item_image+"'>&nbsp;"+data[i].items_name
//             				}
//             			}));
//             		}
//             	}
//             	// You must always return a valid array when processing data. The
//             	// data argument passed is a clone and cannot be modified directly.
//             	return array;
//             }
//         };

//         $('#item_select').selectpicker().ajaxSelectPicker(options);
//         $('#item_select').trigger('change');
//         $('#item_select_edit').selectpicker().ajaxSelectPicker(options);
//         $('#item_select_edit').trigger('change');
        // End of AJAX Boostrap for Unit selection

        /*loading tooltip*/
        jQuery(document).ready(function($) {
            $("[data-toggle='popover']").popover();

        });

        /*lading date picker*/
        jQuery(document).ready(function() {    
            $('.date-picker').datepicker();
            UIExtendedModals.init();
        });

    // table = $('#sample_2').DataTable();
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 2 || idx == 3 || idx == 5) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });

    var table = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/Demage_and_lost/all_demage_lost_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [4, "desc"]
        ],
        /*for display tooltip message*/
        "drawCallback": function( settings ) {
            $("[data-toggle='popover']").popover();
        }
        /* tooltip message ends*/

    });
    /*data table customize search starts*/
    table.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });

        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/

// ajax form submit starts....
$('#demage_lost_form').submit(function(event) {
    $('#save_id').prop('disabled', true);
    event.preventDefault();
    var data = {};
    data.item_spec_set_id =  $('#spec_id_for_get').val();
    data.demaged_imei = $('#demaged_imei_number').val();
    data.demage_lost_type = $('#demage_lost_type_id').val();
    data.demage_lost_quantity = $('input[name=demage_lost_quantity]').val();
    data.demage_lost_description = $('textarea[name=demage_lost_description]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    data.item_buying_price = $('input[name=item_buying_price]').val();
    $.post('<?php echo site_url() ?>/Demage_and_lost/save_demage_lost',data).done(function(data1, textStatus, xhr) {

        var data2 = $.parseJSON(data1);
        if(data2.success==0)
        {
            $('#save_id').prop('disabled', false);
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }

        else if(data2.permission=="no"){
            $('#save_id').prop('disabled', false);
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 3000 );
        }
        else{
            $('#save_id').prop('disabled', false);
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();
            $('input[name=item_spec_set_id]').val('');
            $('#demage_lost_form').trigger("reset");
            $('#item_select').selectpicker('val','');
            $('#imei_entry_div').hide();
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table.row.add({
                "0":       data.demage_lost_type,
                "1":     '',
                "2" :  data.demage_lost_quantity,
                "3" : data.demage_lost_description,
                "4":   '',
                "5": "<a class='btn btn-info edit_nagadanboi' id='edit_"+data2.data+"' >    বিস্তারিত  </a>"
            }).draw();

        }

    }).error(function() {
        $('#save_id').prop('disabled', false);
        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    


});
// ajax form submit end.......

/* Delete Starts */
var demage_lost_id = '' ;
var selected_name = '';
$('table').on('click', '.delete_demage_lost',function(event) {
    event.preventDefault();
    demage_lost_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});

$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
        // var expenditures_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'demage_lost_id' : demage_lost_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Demage_and_lost/delete_demage_lost/'+demage_lost_id,post_data).done(function(data) {

            if(data == "No Permisssion"){

                $('#responsive_modal_delete').modal('hide');
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );

            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );

                table.row($('#delete_'+demage_lost_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }

        }).error(function() {
            alert("<?php echo $this->lang->line('alert_delete_failure');?>");                
        });
    });
/* Delete Ends */
/* Edit Starts*/
$('table').on('click', '.edit_demage_lost',function(event) {
    event.preventDefault();
    var demage_lost_id = $(this).attr('id').split('_')[1];
    $('#current_demage_lost_id').remove();
    $.get('<?php echo site_url();?>/Demage_and_lost/get_demage_lost_info/'+demage_lost_id).done(function(data) {
        var demage_lost_data = $.parseJSON(data);
        console.log(demage_lost_data);
        if(demage_lost_data.permission =="no"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 3000 );            
        }
        else {
            $('#form_header_text').text("<?php echo $this->lang->line('left_edit');?>");
            $('#demage_lost_type_id_edit').prop('disabled', true);
            $('#item_select_edit').prop('disabled', true);
            if(demage_lost_data.all_demage_lost_by_id.imei_barcode != null){
                $('#imei_entry_edit_div').show();
                $('#demaged_imei_number_edit').val(demage_lost_data.all_demage_lost_by_id.imei_barcode);
                $('#quantity_edit_id').attr('readonly', 'true');
                $('#edit_save_id').prop("disabled",true);
            }
            else{
                $('#demaged_imei_number_edit').val('');
                $('#imei_entry_edit_div').hide();
                $('#quantity_edit_id').removeAttr('readonly', 'false');
                $('#edit_save_id').prop("disabled",false);
            }
            $('select[name = demage_lost_type_edit]').val(demage_lost_data.all_demage_lost_by_id.demage_lost_type);
            $('select[name = item_spec_set_id_edit]').val(demage_lost_data.all_demage_lost_by_id.item_spec_set_id);
            $('#item_select_edit').html('<option value="'+demage_lost_data.all_demage_lost_by_id.item_spec_set_id+'" selected="selected">'+demage_lost_data.all_demage_lost_by_id.spec_set+'</option>');
            $('#item_select_edit').trigger('change');
            $('input[name = demage_lost_quantity_edit]').val(demage_lost_data.all_demage_lost_by_id.demage_lost_quantity);
            $('input[name = item_buying_price_edit]').val(demage_lost_data.all_demage_lost_by_id.item_buying_price);
            $('textarea[name = demage_lost_description_edit]').val(demage_lost_data.all_demage_lost_by_id.demage_lost_description);
            $('#demage_lost_insert_div').slideUp('fast');
            $('#demage_lost_edit_div').slideDown('slow');
            $('input[name=demage_lost_id]').val(demage_lost_id);
        }
    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});
$('#demage_lost_edit_form').submit(function(event) {
    event.preventDefault();
    $('#responsive_modal_edit').modal('show');
});
$('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {
    event.preventDefault();
    $('#edit_save_id').prop('disabled', true);
    var post_data={};
    post_data.demage_lost_type_edit = $('select[name = demage_lost_type_edit]').val();
    post_data.item_spec_set_id_edit = $('select[name = item_spec_set_id_edit]').val();
    post_data.demage_lost_quantity_edit = $('input[name = demage_lost_quantity_edit]').val();
    post_data.item_buying_price_edit = $('input[name = item_buying_price_edit]').val();
    post_data.demage_lost_description_edit = $('textarea[name = demage_lost_description_edit]').val();
    post_data.demage_lost_id = $('input[name= demage_lost_id]').val();
    post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/Demage_and_lost/update_demage_lost_info/',post_data).done(function(data, textStatus, xhr) {

        var data2= $.parseJSON(data);
        if(data2.success ==1)
        {
            $('#edit_save_id').prop('disabled', false);
            $('#responsive_modal_edit').modal('hide');
            $('#update_success').slideDown();
            setTimeout( function(){$('#update_success').slideUp()}, 3000 );
            var row_data = table.row( $('#edit_'+post_data.demage_lost_id).closest('tr')[0] ).data();
            row_data[0] = '';
            row_data[1] = '';
            row_data[2] = '';
            row_data[3] = '';
            row_data[4] = '';
            row_data[5] = '';

            table.row( $('#edit_'+post_data.demage_lost_id).closest('tr')[0] ).data(row_data).draw();
            $('#demage_lost_insert_div').slideDown('slow');
            $('#demage_lost_edit_div').slideUp('fast');
            $('#form_header_text').text("<?php echo $this->lang->line('left_header');?>");
            $('.help-block').hide();

        }
        else if(!Object.keys(data2.error).length)
        {
            $('#edit_save_id').prop('disabled', false);
            alert("Update Failed");                
        }
        else
        {
            $('#edit_save_id').prop('disabled', false);
            $('#responsive_modal_edit').modal('hide');
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }

    }).error(function() {
        $('#edit_save_id').prop('disabled', false);
        alert("<?php echo $this->lang->line('alert_edit_failed');?>");
    });
});

/*Edit Ends*/
        // Customize Boostrap Select for Item Selection.
        var timer;
        $("#item_select").keyup(function(event) 
        {
           $("#item_select_result").show();
           $("#item_select_result").html('');

           clearTimeout(timer);
           timer = setTimeout(function() 
           {
               var search_item = $("#item_select").val();
               var html = '';
               $.post('<?php echo site_url(); ?>/sell/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                // console.log(data);
                data = JSON.parse(data);
                // console.log(data);
                $.each(data, function(index, val) {
                    if(val.item_spec_set_image == null || val.item_spec_set_image == ''){
                        val.item_spec_set_image = "images/item_images/no_image.png";
                    }

                    // console.log(data['item_image']);
                    var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                    html+= '<tr><td data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';

                });

                $("#item_select_result").html(html);
            });
           }, 500);
       });

        $('#item_select_result').on('click', 'td', function(event) {
           event.preventDefault();

           $('input[name="items_name"]').val($(this).text());
           $('input[name="item_spec_set_id"]').val($(this).attr('data'));
           $("#item_select_result").hide();

           var sell_type_id = $('#sell_type_id').val();
           var item_select_val = $('input[name="item_spec_set_id"]').val();
    // console.log(item_select_val);
    // return false;
    $.get('<?php echo site_url() ?>/sell/get_item_price_info/'+item_select_val, function(data) {
        var data_get = $.parseJSON(data);
        // console.log(data_get);
        $('#current_quantity').val(data_get[0].quantity);
        // $('#numpadButton1').attr("placeholder", "Type your answer here");
        if (sell_type_id == 'wholesale') {
            $('.selling_price_and_numberpad').val(data_get[0].whole_sale_price);
        }
        else if(sell_type_id == 'retail'){
            $('.selling_price_and_numberpad').val(data_get[0].retail_price);
        };
    });
});
        $('#demage_lost_edit_form').on('click', '#edit_demage_lost_cancle', function(event) {
           event.preventDefault();
           $('#demage_lost_insert_div').slideDown('slow');
           $('#demage_lost_edit_div').slideUp('fast');
           $('#form_header_text').text("<?php echo $this->lang->line('left_header');?>");
       });
        $('#item_select_result').on('click', 'td', function(event) {
            event.preventDefault(); 
            var item_select_val = $('input[name="item_spec_set_id"]').val();
            $.get('<?php echo site_url() ?>/buy/get_item_buying_price/'+item_select_val, function(data) {
                var data_get = $.parseJSON(data);
              //  console.log(data_get);
              $('#buying_price_id').val(data_get.buying_price.buying_price);
              if(data_get.is_unique_barcode.unique_barcode == "yes"){
                $('#imei_entry_div').show();
            }
            else{
                $('#imei_entry_div').hide();
            }
        });
        });
    </script>