	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption left">
				<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_expense_type_name_new');?></span>
            </div>

        </div>
        <div class="portlet-body form" id="expenditure_type_insert_div">
            <!-- BEGIN FORM-->
    <?php $form_attrib = array('id' => 'expenditure_type_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>

            <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_expense_type_name');?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="expenditure_types_name_2" placeholder="<?php echo $this->lang->line('left_expense_type_name');?>">
                        <span class="help-block"></span></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('right_description');?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="expenditure_types_description" placeholder="<?php echo $this->lang->line('left_expense_description_placeholder');?>">
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" data-dismiss="modal" class="btn default pull-right" style="margin-left: 8px" id="exp_type_cancel"> <?php echo $this->lang->line('cancel');?></button>
                    <button type="submit" class="btn green pull-right" ><?php echo $this->lang->line('left_save');?></button>
                    
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
