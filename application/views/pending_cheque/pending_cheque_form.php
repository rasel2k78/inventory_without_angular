<?php
/**
 * Form Name:"Pending Cheque Page" 
 *
 * Display all pending cheque lists.
 * 
 * @link   Pending_cheques/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/> -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- Date Range Picker css ends -->
<!-- For Responsive Datatable Starts-->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="cash_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_success');?></div>
        <div class="alert alert-danger" id="cash_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_failure');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('left_portlet_title'); ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                             <?php echo $this->lang->line('sell_voucher'); ?>
                         </th>
                         <th>
                             <?php echo $this->lang->line('cheque_number'); ?>
                         </th>
                         <th>
                           <?php echo $this->lang->line('date'); ?>
                       </th>
                       <th>
                         <?php echo $this->lang->line('cash_or_acc'); ?>
                     </th>
                 </tr>
             </thead>
             <tbody>
             </tbody>
             <tfoot>
                <tr>
                    <th>   <?php echo $this->lang->line('sell_voucher'); ?></th>
                    <th>  <?php echo $this->lang->line('cheque_number'); ?></th>
                    <th>
                        <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control dont-search" name="from_create_date">
                            <span class="input-group-addon">
                                to
                            </span>
                            <input type="text" class="form-control dont-search" name="to_create_date">
                        </div>
                    </th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<div class="col-md-6">
    <div class="portlet box red" id="chk_clr_by_acc_form" style="display:none">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><span id="form_header_text" > <?php echo $this->lang->line('chk_clr_by_acc'); ?></span>
            </div>
        </div>
        <div class="portlet-body form" id="bank_dw_insert_div">
            <!-- BEGIN FORM-->
            <?php $form_attrib = array('id' => 'bank_dw_form','class' => 'form-horizontal');
            echo form_open('',  $form_attrib, '');?>
            <div class="form-body">
             <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                </label>
                <div class="col-md-9">
                    <input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('select_acc_pl');?> " id="bank_acc_select" class="form-control">
                    <input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                    <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                    </table>
                    <span class="normalize help-block"></span>
                </div>
                <input type="hidden" class="form-control" name="pending_cheque_id"  readonly="true">
            </div>
            <div class="form-actions">
                <button type="reset" id="chk_clr_cancel" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                <button type="submit" id="chk_clr_by_acc" class="btn green pull-right"><?php echo $this->lang->line('save'); ?></button>
            </div>
        </div>
        <?php echo form_close();?>
        <!-- END FORM-->
    </div>
</div>
</div>
</div>
<!-- Modal view starts -->
<div id="responsive_modal_cash" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_confirmation'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_details'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_no'); ?></button>
    <button type="button" id="cash_confirmation" class="btn blue"><?php echo $this->lang->line('alert_yes'); ?></button>
</div>
</div>
<!-- Modal view ends -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--  <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Date Range Picker starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Date Range Picker Ends -->
<!-- Responsive Datatable Scripts Starts -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- Responsive Datatable Scripts Ends -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script> 
    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });
    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 3 ) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });
    var table_pending_cheques= $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/Pending_cheques/all_pending_cheques_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [2, "desc"]
        ]
    });    
    /*data table customize search starts*/
    table_pending_cheques.columns().every( function () {
        var that = this;
        $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
            if (!($(this).hasClass('dont-search'))) {
                that
                .search( this.value )
                .draw();
            }
        });
        $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
            var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
            var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();
            var final_string = '';
            if(date_from !='' || date_to != ''){
                if (date_from!='' && date_to=='') {
                    final_string = date_from+'_0';
                }
                if (date_to!='' && date_from=='') {
                    final_string = '0_'+date_to;
                }
                if (date_from!='' && date_to!='') {
                    final_string = date_from+'_'+date_to;
                }
            }
            else{
                final_string = '0_0';
            }
            that.search(final_string).draw();
        });
    });
    /*datatable customize search ends*/
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
      $("#bank_acc_select_result").show();
      $("#bank_acc_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
      $('input[name="bank_acc_num"]').val($(this).html());
      $('input[name="bank_acc_id"]').val($(this).attr('data'));
      $("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
// ajax form view starts....
$('table').on('click', '.cleared_by_acc',function(event) {
    event.preventDefault();
    var pending_cheque_id = $(this).attr('id').split('_')[1];
    $('#current_pending_cheque_id').remove();
    $.get('<?php echo site_url();?>/pending_cheques/get_all_buy_info/'+pending_cheque_id).done(function(data) {
    // console.log(buy_details);
    var buy_data = $.parseJSON(data);
    if(buy_data =="No Permission"){
        $('#access_failure').slideDown();
        setTimeout( function(){$('#access_failure').slideUp()}, 5000 );        
    }
    else{
        $('#chk_clr_by_acc_form').show(400);
        $('input[name=pending_cheque_id]').val(pending_cheque_id);
        $('input[name=bank_acc_num]').parent().parent().removeClass('has-error');
        $('input[name=bank_acc_num]').parent().parent().find('.help-block').text("");
    }
}).error(function() {
    alert("<?php echo $this->lang->line('error_msg')?>");
});
});
// Ajax Cheque cleared by cash
var pending_cheque_id = '' ;
var selected_name = '';
$('table').on('click', '.cleared_by_cash',function(event) {
    event.preventDefault();
    pending_cheque_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});
$('#responsive_modal_cash').on('click', '#cash_confirmation',function(event) {
    event.preventDefault();
    var post_data ={
        'pending_cheque_id' : pending_cheque_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 
    $.post('<?php echo site_url();?>/pending_cheques/chk_cleared_by_cash/'+pending_cheque_id,post_data).done(function(data) {
     var chk_data = $.parseJSON(data);
     if( chk_data['success'] == "yes")
     {
        $('#chk_clr_by_acc_form').hide();
        $('#responsive_modal_cash').modal('hide');
        $('#cash_success').slideDown();
        setTimeout( function(){$('#cash_success').slideUp()}, 5000 );
        table_pending_cheques.ajax.reload(null, false);
    }
    else
    {
        $('#responsive_modal_cash').modal('hide');
        $('#cash_failure').slideDown();
        setTimeout( function(){$('#cash_failure').slideUp()}, 5000 );                
    }
}).error(function() {
    alert("<?php echo $this->lang->line('error_msg')?>");     
});
});
$(document).on('click', '#chk_clr_by_acc', function(event) {
    event.preventDefault();
    var data = {};
    data.bank_acc_id = $('input[name=bank_acc_id]').val();
    data.pending_cheque_id = $('input[name=pending_cheque_id]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    $.post('<?php echo site_url() ?>/pending_cheques/chk_cleared_by_acc',data).done(function(data1) {
       var data2 = $.parseJSON(data1);
       if(data2['success'] == "yes"){
        $('#chk_clr_by_acc_form').hide();
        $('#bank_acc_select').val('');
        $('input[name=bank_acc_id]').val('');
        $('#cash_success').slideDown();
        setTimeout( function(){$('#cash_success').slideUp()}, 5000 );
        table_pending_cheques.ajax.reload(null, false);
    }
    else if(data2['error'] == "no_acc_selected"){
        $('input[name=bank_acc_num]').parent().parent().addClass('has-error');
        $('input[name=bank_acc_num]').parent().parent().find('.help-block').text("<?php echo $this->lang->line('no_acc_selected')?>");
    }
    else{
      $('#cash_failure').slideDown();
      setTimeout( function(){$('#cash_failure').slideUp()}, 5000 );     
  }
}).error(function() {
    alert("<?php echo $this->lang->line('error_msg')?>");
});
});
$('#bank_dw_form').on('click', '#chk_clr_cancel', function(event) {
    event.preventDefault();
    $('#chk_clr_by_acc_form').hide();
    $('input[name=pending_cheque_id]').val('');
    $('#bank_acc_select').val('');
    $('input[name=bank_acc_id]').val('');
    $('input[name=bank_acc_num]').parent().parent().removeClass('has-error');
    $('input[name=bank_acc_num]').parent().parent().find('.help-block').text("");
    /* Act on the event */
});
</script>