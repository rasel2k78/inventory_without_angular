<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-6">

        <!-- CODE FOR SLIDER ALERT -->
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_success');?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_failed');?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_insert_success');?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_success');?></div>
        <div class="alert alert-danger" id="access_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_access_failure');?></div>

        <div class="alert alert-danger" id="delete_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_delete_failure');?></div>
        <div class="alert alert-danger" id="update_failure" style="display:none" role="alert"><?php echo $this->lang->line('alert_edit_failed');?></div>
        <div class="alert alert-danger" id="no_name" style="display:none" role="alert"><?php echo $this->lang->line('no_name');?></div>
        

        <!-- CODE FOR SLIDER ALERT END-->

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('add_new_sales_rep'); ?></span>
                </div>
                
            </div>
            <div class="portlet-body form" id="sales_rep_div">

                <!-- BEGIN OF sales_rep FORM-->

                <?php
                $form_attrib = array('id' => 'sales_rep_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
                echo form_open(site_url('/Sales_rep/save_sales_rep'),  $form_attrib, '');
                ?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="sales_rep_name" placeholder="<?php echo $this->lang->line('sales_rep_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_email'); ?></label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="sales_rep_email" placeholder="<?php echo $this->lang->line('sales_rep_email'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_phone'); ?></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="1" class="form-control" name="sales_rep_phone_1"  placeholder="<?php echo $this->lang->line('sales_rep_phone'); ?> ">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_phone_2'); ?></label>
                        <div class="col-md-9">
                            <input type="number" min="0" step="1" class="form-control" name="sales_rep_phone_2"  placeholder="<?php echo $this->lang->line('sales_rep_phone_2'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_present_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="sales_rep_present_address"  placeholder="<?php echo $this->lang->line('sales_rep_present_address'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_permanent_address'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="sales_rep_permanent_address"  placeholder="<?php echo $this->lang->line('sales_rep_permanent_address'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_nid'); ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="sales_rep_national_id"  placeholder="<?php echo $this->lang->line('sales_rep_nid'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                    <div id="image_of_user" class="form-group ">
                        <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_image'); ?></label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div id="sales_rep_image" class="fileinput-preview thumbnail" name="sales_rep_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            <?php echo $this->lang->line('select_image'); ?>
                                        </span>
                                        <span class="fileinput-exists">
                                            <?php echo $this->lang->line('change'); ?> 
                                        </span>
                                        <input type="file" name="sales_rep_image">
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                        <?php echo $this->lang->line('delete'); ?> 
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                    <input type="submit" value="<?php echo $this->lang->line('save'); ?>"  class="btn green pull-right"/>
                </div>
            </form>
        </div>
        <!-- END OF sales_rep FORM-->

        <!-- Edit Form of Sales Represnetatives Starts-->

        <!-- BEGIN OF sales_rep FORM-->
        <div class="portlet-body form" id="sales_rep_edit_div" style="display:none">
            <?php
            $form_attrib = array('id' => 'sales_rep_form_edit','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
            echo form_open(site_url('/Sales_rep/update_sales_rep'),  $form_attrib, '');
            ?>

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_name_edit" placeholder="<?php echo $this->lang->line('sales_rep_name'); ?>">
                        <input type="hidden" class="form-control" name="sales_rep_id" >
                        
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_email'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_email_edit" placeholder="<?php echo $this->lang->line('sales_rep_email'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_phone'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_phone_1_edit"  placeholder="<?php echo $this->lang->line('sales_rep_phone'); ?> ">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_phone_2'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_phone_2_edit"  placeholder="<?php echo $this->lang->line('sales_rep_phone_2'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_present_address'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_present_address_edit"  placeholder="<?php echo $this->lang->line('sales_rep_present_address'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_permanent_address'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_permanent_address_edit"  placeholder="<?php echo $this->lang->line('sales_rep_permanent_address'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_nid'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_national_id_edit"  placeholder="<?php echo $this->lang->line('sales_rep_nid'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="image_of_user" class="form-group ">
                    <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_image'); ?></label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div id="sales_rep_image" class="fileinput-preview thumbnail" name="sales_rep_image_edit" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new">
                                        <?php echo $this->lang->line('select_image'); ?>
                                    </span>
                                    <span class="fileinput-exists">
                                        <?php echo $this->lang->line('change'); ?> 
                                    </span>
                                    <input type="file" name="sales_rep_image_edit">
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                    <?php echo $this->lang->line('delete'); ?> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                <a id="edit_form_submit" class="btn green pull-right"/><?php echo $this->lang->line('save'); ?></a>
            </div>
        </form>

        <!-- END OF sales_rep FORM-->
        <!-- Edit Form of Sales Representatives Ends -->
    </div>
</div>
</div>
<div class="col-md-6">
    <div class="alert alert-success" id="delete_success_from_list" style="display:none" role="alert"><?php echo $this->lang->line('deletion_success'); ?></div>
    <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('no_deletion'); ?></div>
    <div class="alert alert-danger" id="no_permission_right" style="display:none" role="alert"><?php echo $this->lang->line('no_permission_left'); ?></div>

    <!-- BEGIN sales_rep TABLE PORTLET-->
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><?php echo $this->lang->line('all_sales_rep'); ?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->lang->line('name'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('mobile'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('date'); ?>
                        </th>
                        
                        <th>
                            <?php echo $this->lang->line('change_delete'); ?>
                        </th>

                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr>
                        <th><?php echo $this->lang->line('name');?></th>
                        <th> <?php echo $this->lang->line('mobile');?></th>
                        
                        <th>
                            <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control dont-search" name="from_create_date">
                                <span class="input-group-addon">
                                    to
                                </span>
                                <input type="text" class="form-control dont-search" name="to_create_date">
                            </div>
                        </th>
                        <th><?php echo $this->lang->line('change_delete');?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- END sales_rep TABLE PORTLET-->
</div>
</div>


<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_delete_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_delete_details');?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_delete_no');?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('alert_delete_yes');?></button>
</div>
</div>

<div id="responsive_modal_edit" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('alert_edit_confirmation');?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('alert_edit_details');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('alert_edit_no');?></button>
        <button type="button" id="edit_confirmation" class="btn blue"><?php echo $this->lang->line('alert_edit_yes');?></button>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>


<script>
    jQuery(document).ready(function() {    
        UIExtendedModals.init();
    });

    /*lading date picker*/
    jQuery(document).ready(function() {    
        $('.date-picker').datepicker();
        UIExtendedModals.init();
    });

    $('#sample_2 tfoot th').each(function (idx,elm){
        if (idx == 0 || idx == 1 || idx == 3) { 
            var title = $('#example tfoot th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
        }
    });

//Code for data table start
var table_sales_rep = $('#sample_2').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/Sales_rep/all_sales_rep_info_for_datatable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for data table end

/*data table customize search starts*/
table_sales_rep.columns().every( function () {
    var that = this;
    $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
        if (!($(this).hasClass('dont-search'))) {
            that
            .search( this.value )
            .draw();
        }
    });

    $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
        var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
        var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

        var final_string = '';
        if(date_from !='' || date_to != ''){
            if (date_from!='' && date_to=='') {
                final_string = date_from+'_0';
            }
            if (date_to!='' && date_from=='') {
                final_string = '0_'+date_to;
            }
            if (date_from!='' && date_to!='') {
                final_string = date_from+'_'+date_to;
            }
        }
        else{
            final_string = '0_0';
        }
        that.search(final_string).draw();
    });
});
/*datatable customize search ends*/


//Ajax form submit start and slider shown after validation check based on msg which is comes from controller
$('#sales_rep_form').ajaxForm({
    beforeSubmit: function() {    
        $('input[name = sales_rep_name]').closest('.form-group').removeClass('has-error');
        $('input[name = sales_rep_name]').siblings().text("");
        
    },
    success: function(msg) {
        // $("#sales_rep_form").trigger('reset');
        $('#sales_rep_form')[0].reset();

        if(msg == "failed")
        {
            $('#insert_failure').slideDown();
            setTimeout( function(){$('#insert_failure').slideUp()}, 5000 );
        }
        else if(msg == "no_name"){
            $('input[name = sales_rep_name]').closest('.form-group').addClass('has-error');
            $('input[name = sales_rep_name]').siblings().text("<?php echo $this->lang->line('no_name');?>");
            // $('#no_name').slideDown();
            // setTimeout( function(){$('#no_name').slideUp()}, 1500 );
        }
        else if(msg == "success"){
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp()}, 3000 );
        }else if(msg == "sales_rep already exist"){
            $('#sales_rep_exist').slideDown();
            setTimeout( function(){$('#sales_rep_exist').slideUp()}, 5000 );
        }
        else if(msg == "No permission"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 5000 );
        }
    },
    complete: function(xhr) {
        table_sales_rep.ajax.reload(null, false);
        return false;
    }

}); 
//Ajax form submit end


$('table').on('click', '.edit_sales_rep',function(event) {
    event.preventDefault();

    var sales_rep_id = $(this).attr('id').split('_')[1];
    $('#current_sales_rep_id').remove();

    $.get('<?php echo site_url();?>/sales_rep/get_sales_rep_info/'+sales_rep_id).done(function(data) {

        var sales_rep_data = $.parseJSON(data);
        if(sales_rep_data.permission =="no"){
            $('#access_failure').slideDown();
            setTimeout( function(){$('#access_failure').slideUp()}, 3000 );                
        }
        else {

            $('#sales_rep_div').hide();
            $('#sales_rep_edit_div').show();

            $('#form_header_text').text("<?php echo $this->lang->line('edit_header');?>");
            $('input[name = sales_rep_name_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_name);
            $('input[name = sales_rep_email_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_email);
            $('input[name = sales_rep_phone_1_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_phone_1);
            $('input[name = sales_rep_phone_2_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_phone_2);
            $('input[name = sales_rep_national_id_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_national_id);
            $('input[name = sales_rep_present_address_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_present_address);
            $('input[name = sales_rep_permanent_address_edit]').val(sales_rep_data.sales_rep_info_by_id.sales_rep_permanent_address);
            $('input[name=sales_rep_id]').val(sales_rep_id);

            if(sales_rep_data.sales_rep_info_by_id.sales_rep_image!="" && sales_rep_data.sales_rep_info_by_id.sales_rep_image!=null)
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+sales_rep_data.sales_rep_info_by_id.sales_rep_image+"'>");
            }
            else
            {
                $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/sales_rep_images/no_image.png'>");
            }
        }

    }).error(function() {
        alert("An error has occured. pLease try again");
    });
});

$('#edit_form_submit').click(function(event) {
    $('#responsive_modal_edit').modal('show');
});


$('#responsive_modal_edit').on('click', '#edit_confirmation',function(event) {

    event.preventDefault();
    // alert ("test");
    var a_form = $('#sales_rep_form_edit').ajaxForm({
        beforeSubmit: function() {    
            $('input[name = sales_rep_name_edit]').closest('.form-group').removeClass('has-error');
            $('input[name = sales_rep_name_edit]').siblings().text("");
        },
        success: function(msg) {
            // $('#responsive_modal_edit').modal('hide');
            // $("#sales_rep_form").trigger('reset');
            // $('sales_rep_edit_div').hide();
            // $('#sales_rep_div').show();
            // $('#sales_rep_form_edit')[0].reset();

            if(msg == "failed")
            {
                $('#update_failure').slideDown();
                setTimeout( function(){$('#update_failure').slideUp()}, 5000 );
            }
            else if(msg == "no_name"){
                $('input[name = sales_rep_name_edit]').closest('.form-group').addClass('has-error');
                $('input[name = sales_rep_name_edit]').siblings().text("<?php echo $this->lang->line('no_name');?>");
            }
            else if(msg == "success"){
                $('#update_success').slideDown();
                setTimeout( function(){$('#update_success').slideUp()}, 3000 );

                $('#sales_rep_form_edit')[0].reset();
                $('#sales_rep_edit_div').hide();
                $('#form_header_text').text("<?php echo $this->lang->line('add_new_sales_rep'); ?>");
                $('.fileinput-preview').html("<img src=''>");
                $('#sales_rep_div').show();


            }else if(msg == "sales_rep already exist"){
                $('#sales_rep_exist').slideDown();
                setTimeout( function(){$('#sales_rep_exist').slideUp()}, 5000 );
            }
            else if(msg == "No permission"){
                $('#no_permission').slideDown();
                setTimeout( function(){$('#no_permission').slideUp()}, 5000 );
            }
        },
        complete: function(xhr) {
            table_sales_rep.ajax.reload(null, false);
            return false;
        }
    }); 

    $('#sales_rep_form_edit').submit();
    $('#responsive_modal_edit').modal('hide');



});



// //Code for showing user data on form for edit sales_rep button start

// $('table').on('click', '.edit_user',function(event) {
// 	event.preventDefault();
// 	user_id = $(this).attr('id').split('_')[1];
// 	$('#current_user_id').remove();

// 	$.get('<?php echo site_url();?>/sales_rep/sales_rep_info_by_id/'+user_id).done(function(data) 
// 	{

// 		var user_data = $.parseJSON(data);

// 		var html = "<input id='current_user_id' type='hidden' class='form-control' name='user_id' value='"+user_id+"'>";
// 		$('#sales_rep_form').append(html);
// 		$('#sales_rep_form').attr('id', 'user_form_update');
// 		$('#user_form_update').attr('action', "<?php echo site_url('/sales_rep/update_sales_rep_info');?>"+"/"+user_id);



// 		$('#form_header_text').text("<?php echo $this->lang->line('change_sales_rep_info'); ?>");
// 		$('#add_or_update').text("<?php echo $this->lang->line('update_info'); ?>");
// 		$('#email').prop('disabled', true);
// 		$('.pass_hide').hide('slow');
// 		$('#change_pic').hide('slow');
// 		$("#sales_rep_image").prop('disabled', false);
// 		$("#image_of_user_label").text("<?php echo $this->lang->line('image_of_sales_rep'); ?>");

// 		/*To set the value on input field after clicking on edit button for a particular user*/
// 		$('input[name = sales_rep_name]').val(user_data.sales_rep_info_by_id.sales_rep_name);
// 		$('input[name = sales_rep_email]').val(user_data.sales_rep_info_by_id.sales_rep_email);
// 		$('input[name = sales_rep_present_address]').val(user_data.sales_rep_info_by_id.sales_rep_present_address);
// 		$('input[name = sales_rep_permanent_address]').val(user_data.sales_rep_info_by_id.sales_rep_permanent_address);
// 		$('input[name = sales_rep_phone_1]').val(user_data.sales_rep_info_by_id.sales_rep_phone_1);
// 		$('input[name = sales_rep_phone_2]').val(user_data.sales_rep_info_by_id.sales_rep_phone_2);
// 		$('input[name = sales_rep_national_id]').val(user_data.sales_rep_info_by_id.sales_rep_national_id);

// 		if(user_data.sales_rep_info_by_id.sales_rep_image!="" && user_data.sales_rep_info_by_id.sales_rep_image!=null)
// 		{
// 			$('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+user_data.sales_rep_info_by_id.sales_rep_image+"'>");
// 		}
// 		else
// 		{
// 			$('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/sales_rep_images/no_image.png'>");
// 		}

// 		$('#user_form_update').ajaxForm({
// 			beforeSubmit: function() {	

// 			},
// 			success: function(msg) {
// 				if(msg == "sales_rep not set")
// 				{
// 					$('#no_username').slideDown();
// 					setTimeout( function(){$('#no_username').slideUp()}, 1500 );
// 				}
// 				else if(msg == "success"){
// 					$('#user_insert_success').slideDown();
// 					setTimeout( function(){$('#user_insert_success').slideUp()}, 1500 );
// 				}else if(msg == "update_fail"){
// 					$('#insert_failure').slideDown();
// 					setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
// 				}else if(msg == "No permission"){
// 					$('#no_permission').slideDown();
// 					setTimeout( function(){$('#no_permission').slideUp()}, 1500 );
// 				}
// 			},
// 			complete: function(xhr) {
// 				table_sales_rep.ajax.reload(null, false);
// 				return false;
// 			}

// 		}); 

// 	}).error(function() {
// 		alert("An error has occured. pLease try again");
// 	});
// });
// //Code for showing user data on form for edit sales_rep button end


//Code for clear the form on clicking cancel button start
$('#cancel_update').click(function(event) {
    $('input[name = sales_rep_name]').val("");
    $('input[name = sales_rep_email]').val("");
    $('input[name= sales_rep_present_address]').val("");
    $('input[name = sales_rep_permanent_address]').val("");
    $('input[name = sales_rep_phone_1]').val("");
    $('input[name = sales_rep_phone_2]').val("");
    $('input[name = sales_rep_national_id]').val("");
    $('.fileinput-preview').html("<img src=''>");
});
//Code for clear the form on clicking cancel button end


/* Delete Sales Representatories  Info  Starts*/

var sales_rep_id = '' ;
var selected_name = '';
$('table').on('click', '.delete_sales_rep',function(event) {
    event.preventDefault();
    sales_rep_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});

$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
        // var expenditures_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'sales_rep_id' : sales_rep_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Sales_rep/delete_sales_rep/'+sales_rep_id,post_data).done(function(data) {

            if(data == "No Permission"){

                $('#responsive_modal_delete').modal('hide');
                $('#access_failure').slideDown();
                setTimeout( function(){$('#access_failure').slideUp()}, 3000 );

            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                table_sales_rep.row($('#delete_'+sales_rep_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#responsive_modal_delete').modal('hide');
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }

        }).error(function() {
            alert("<?php echo $this->lang->line('alert_delete_failure');?>");                
        });
    });
// ajax form submit starts....

//Code for update sales_rep information start
// $('.portlet-body.form').on('click', '#user_form_update input[type="submit"]', function(event){
// 	event.preventDefault();

// 	var data = {};
// 	data.csrf_test_name = $('input[name=csrf_test_name]').val();

// 	data.sales_rep_id = user_id;
// 	data.sales_rep_name = $('input[name = sales_rep_name]').val();
// 	data.sales_rep_present_address = $('input[name= sales_rep_present_address]').val();
// 	data.sales_rep_permanent_address = $('input[name = sales_rep_permanent_address]').val();
// 	data.sales_rep_phone_1 = $('input[name = sales_rep_phone_1]').val();
// 	data.sales_rep_phone_2 = $('input[name = sales_rep_phone_2]').val();
// 	data.sales_rep_national_id = $('input[name = sales_rep_national_id]').val();

// 	$.post("<?php echo site_url('/sales_rep/update_sales_rep_info');?>",data).done(function(data1, textStatus, xhr) {
// 		if(data1 == "info_updated"){
// 			table_sales_rep.ajax.reload(null, false);
// 			$('#update_success').slideDown();
// 			setTimeout( function(){$('#update_success').slideUp()}, 1500 );
// 		}

// 		else if(data1 == "Username not set")
// 		{
// 			$('#no_username').slideDown();
// 			setTimeout( function(){$('#no_username').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Username already exists"){
// 			$('#username_exist').slideDown();
// 			setTimeout( function(){$('#username_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Email already exists"){
// 			$('#email_exist').slideDown();
// 			setTimeout( function(){$('#email_exist').slideUp()}, 1500 );
// 		}
// 		else if(data1 == "Mobile no. 1 can not be Empty"){
// 			$('#no_mobile').slideDown();
// 			setTimeout( function(){$('#no_mobile').slideUp()}, 1500 );
// 		}

// 	}).error(function() {
// 		$('#insert_failure').slideDown();
// 		setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
// 	});	
// });
//Code for update sales_rep information end


$('#sales_rep_form_edit').on('click', '#cancel_update', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('#sales_rep_form_edit')[0].reset();
    $('#sales_rep_edit_div').hide();
    $('#form_header_text').text("<?php echo $this->lang->line('add_new_sales_rep'); ?>");
    $('.fileinput-preview').html("<img src=''>");
    $('#sales_rep_div').show();


});

</script>
