	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('add_new_sales_rep'); ?></span>
            </div>

        </div>

        <div class="portlet-body form" id="sales_rep_div">

            <!-- BEGIN OF sales_rep FORM-->

    <?php
    $form_attrib = array('id' => 'sales_rep_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
    echo form_open(site_url('/Sales_rep/save_sales_rep'),  $form_attrib, '');
    ?>

            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_name" id="sales_rep_name_id" placeholder="<?php echo $this->lang->line('sales_rep_name'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_phone'); ?></label>
                    <div class="col-md-9">
                        <input type="number" min="0" step="1" class="form-control" name="sales_rep_phone_1"  placeholder="<?php echo $this->lang->line('sales_rep_phone'); ?> ">
                        <span class="help-block"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_present_address'); ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="sales_rep_present_address"  placeholder="<?php echo $this->lang->line('sales_rep_present_address'); ?>">

                    </div>
                </div>

                <div id="image_of_user" class="form-group ">
                    <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('sales_rep_image'); ?></label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div id="sales_rep_image" class="fileinput-preview thumbnail" name="sales_rep_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new">
            <?php echo $this->lang->line('select_image'); ?>
                                    </span>
                                    <span class="fileinput-exists">
            <?php echo $this->lang->line('change'); ?> 
                                    </span>
                                    <input type="file" name="sales_rep_image">
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
            <?php echo $this->lang->line('delete'); ?> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" data-dismiss="modal" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
                <input type="submit" value="<?php echo $this->lang->line('save'); ?>"  class="btn green pull-right"/>
            </div>
    <?php echo form_close(); ?>
        </div>

        <!-- END PAGE LEVEL PLUGINS -->
        <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
        <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

        <script type="text/javascript">

    //Ajax form submit start and slider shown after validation check based on msg which is comes from controller
    $('#sales_rep_form').ajaxForm({
        beforeSubmit: function() {    
            $('#sales_rep_name_id').closest('.form-group').removeClass('has-error');
            $('#sales_rep_name_id').siblings().text("");
        },
        success: function(msg) {

            if(msg == "no_name")
            {
                $('#sales_rep_name_id').closest('.form-group').addClass('has-error');
                $('#sales_rep_name_id').siblings().text("<?php echo $this->lang->line('sales_rep_no_name'); ?>");
            //    $('#no_username').slideDown();
            //    setTimeout( function(){$('#no_username').slideUp()}, 1500 );
        }
        else if(msg == "success"){
            $('#sales_rep_form_modal').modal('hide');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
            $('#sales_rep_form').trigger("reset");

        }
        // else if(){

            // $('#sales_rep_form_modal').modal('hide');
            // $('#no_name').slideDown();
            // setTimeout( function(){$('#no_name').slideUp()}, 1500 );
            // $('#sales_rep_form').trigger("reset");
            
        //     }

    },
    complete: function(xhr) {
        table_user.ajax.reload();
        return false;
    }

}); 

//Ajax form submit end


// $('#cancel_update').click(function(event) {
// 	event.preventDefault();
// 	// alert("test");
// 	// alert("test");
// 	$('#sales_rep_form_modal').modal('hide');
// });
</script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->