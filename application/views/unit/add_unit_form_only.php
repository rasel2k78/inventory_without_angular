<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption left">
			<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('left_portlet_title_unit'); ?></span>
        </div>

    </div>
    <div id="unit_insert_div" class="portlet-body form">
        <!-- BEGIN FORM-->
        <!-- <form accept-charset="utf-8" method="post" class="form-horizontal form-bordered" id="unit_form" action="http://localhost/inventory/index.php/Unit"> -->

    <?php $form_attrib = array('id' => 'unit_form','class' => 'form-horizontal form-bordered');
    echo form_open('',  $form_attrib, '');?>
        <input type="hidden" style="display:none;" value="2537779c82cd6962420da5cd890b2f79" name="csrf_test_name">

        <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('unit_name'); ?>*</label>
                <div class="col-md-9">
                    <input type="text" placeholder="<?php echo $this->lang->line('unit_name'); ?>" name="units_name" class="form-control">
                    <span class="help-block"></span>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button class="btn green" type="submit"><?php echo $this->lang->line('save'); ?></button>
            <!-- <button class="btn default" type="reset">বাতিল করুন</button> -->
            <button data-dismiss="modal" class="btn dark btn-outline" type="button"><?php echo $this->lang->line('cancle'); ?></button>
        </div>
    <?php echo form_close();?>        <!-- END FORM-->
    </div>
</div>

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->	