<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/buttons.dataTables.min.css">
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<style type="text/css">
.custom-tag-input:focus{
    outline:0px !important;
    -webkit-appearance:none;
    -moz-appearance:none;
}
div + img {
    background-color: yellow;
    height:50px;
}
.row-selected{
    background-color: #bbb;
}
.td-middle-center{
    vertical-align: middle !important;
    text-align: center !important;
}
</style>

<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="col-md-6">


        <div class="alert alert-danger" id="barcode_invalid" style="display:none" role="alert"><?php echo $this->lang->line('barcode_invalid'); ?></span></div>
        <div class="alert alert-danger" id="barcode_exist" style="display:none" role="alert"><?php echo $this->lang->line('barcode_exist'); ?></span></div>
        <div class="alert alert-danger" id="inventory_exist" style="display:none" role="alert"><?php echo $this->lang->line('inventory_exist'); ?></span></div>
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('product_delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_failed'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_succeded'); ?></div>
        <div class="alert alert-success" id="insert_success_category" style="display:none" role="alert"><?php echo $this->lang->line('category_insert_success'); ?></div>
        <div class="alert alert-success" id="insert_success_spec_name" style="display:none" role="alert"><?php echo $this->lang->line('spec_insert_success'); ?></div>
        <div class="alert alert-success" id="insert_success_unit" style="display:none" role="alert"><?php echo $this->lang->line('unit_insert_success'); ?></div>
        <div class="alert alert-success" id="insert_success_size" style="display:none" role="alert"><?php echo $this->lang->line('size_insert_success'); ?></div>
        <div class="alert alert-success" id="insert_success_color" style="display:none" role="alert"><?php echo $this->lang->line('color_insert_success'); ?></div>
        <div class="alert alert-success" id="insert_success_lotsize" style="display:none" role="alert"><?php echo $this->lang->line('lot_insert_success'); ?></div>
        <div class="alert alert-danger" id="no_spec" style="display:none" role="alert"><?php echo $this->lang->line('choose_spec'); ?></div>
        <!-- <div class="alert alert-danger" id="category_add" style="display:none" role="alert">Please choose a category</div> -->
        <div class="alert alert-danger" id="one_combination" style="display:none" role="alert"><?php echo $this->lang->line('max_spec'); ?></div>

        <div class="alert alert-danger" id="no_item" style="display:none" role="alert"><?php echo $this->lang->line('item_name_required'); ?></span></div>
        <!-- <div class="alert alert-danger" id="no_color" style="display:none" role="alert">Please select item color</span></div>
        <div class="alert alert-danger" id="no_size" style="display:none" role="alert">Please slect item size</span></div>
    -->
    <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('product_update_succeded'); ?></div>
    <div class="portlet box red">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-pencil-square-o"></i><span id="header_text"><?php echo $this->lang->line('left_portlet_title_product'); ?></span>
            </div>
        </div>
        
        <input name="item_new_name" type="hidden" id="item_new_name">
        <div class="portlet-body form">
            <?php $form_attrib = array('id' => 'item_form','class' => 'form-horizontal form-bordered');
            echo form_open_multipart('Item/save_item_info',  $form_attrib);?>
            <div class="form-body">


                <div class="form-group <?php if(isset($error['items_name'])) { echo 'has-error'; 
            } ?> " id="itemAdd">
            <label class="control-label col-md-3">
                <?php echo $this->lang->line('product_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
            </label>
            <div class="col-md-9 input-form input-icon">
                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_name_lang'); ?>"></i>
                <input autocomplete="off" type="text" id="search_key" class="form-control" name="items_name" placeholder="<?php echo $this->lang->line('product_name'); ?>" value="<?php if(isset($previous_data['items_name'])) { echo $previous_data['items_name']; 
            } ?>" >
            <!-- <input type="file" name="upload_pictures">  -->
            <div id="suggesstion-box"></div>
            <span class="help-block"><?php if(isset($error['items_name'])) { echo $error['items_name']; 
        } ?></span>
    </div>
</div>

<div class="form-group <?php if(isset($error['items_name'])) { echo 'has-error'; 
} ?> " id="itemDelete" style="display:none">

<label class="control-label col-md-3">
    <?php echo $this->lang->line('product_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
</label>
<div class="col-md-9 input-form input-icon">
    <input name="items_name_delete" class="form-control" readonly="true" autocomplete="off" type="text"  value="<?php if(isset($previous_data['items_name'])) { echo $previous_data['items_name']; 
} ?>" >
<!-- <input type="file" name="upload_pictures">  -->
<div id="suggesstion-box"></div>
<span class="help-block"><?php if(isset($error['items_name'])) { echo $error['items_name']; 
} ?></span>
</div>
</div>



<style type="text/css">.spec-tags{margin-top: 13%}</style>

<input type="hidden" name="is_edit" id="is_edit">

<!-- <div class="form-group select-tag" id="specAdd"> -->
<!--*********** No need for new designed Item **************
 <div class="form-group " id="specAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('select_specification'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
    <div class="col-md-9 input-form input-icon">
        <div style="overflow: hidden; padding: 1px 5px;" id="spec-tag-field">
            <div class="col-sm-1">
                <a href="#specificationForm" title="<?php echo $this->lang->line('add_spec'); ?>" data-toggle="modal" class="btn btn-default"><?php echo $this->lang->line('specification')?></a>
            </div>
            <div class="col-sm-1" style="position: absolute;left: 160px;z-index: 11;">
                <a href="#specificationList" title="<?php echo $this->lang->line('add_spec'); ?>" data-toggle="modal" class="btn btn-default specificationList"><?php echo $this->lang->line('spec_list');?></a>
            </div>
            <a class="btn btn-default select-spec-combination" style="position: absolute;right: 16px;z-index: 11;"> 
                <?php echo $this->lang->line('ok'); ?>
                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_ok'); ?>"></i> 
            </a>
            <div style="height:50px;"></div>
            <div class="col-md-12 spec-box" style="padding: 10px 0px;">
            </div>
            <hr />
            <div class="col-md-12" style="padding: 10px 0px;">
                <div class="col-md-12">
                    <input type="text" id="inputType" class="custom-tag-input" placeholder="<?php echo $this->lang->line('select_specification'); ?>" style="float: left;height: 36px;width:60%; border: 1px solid rgb(226, 226, 226);
                    background:#FFFFFF; padding-left: 25px;float:left;margin-left:0px;"; autocomplete="off">
                </div>
                <div class="col-md-12">
                    <table class="table table-condensed table-hover table-bordered clickable" id="spec_select_result" style="z-index: 10;background-color: #fff;">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>  *********** No need for new designed Item **************-->



<!-- <div class="form-group" id="specDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('specifications'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
    <div class="col-md-9 input-form input-icon">
        <div style="overflow: hidden; padding: 1px 5px;" id="spec-tag-field-delete">

        </div>
    </div>
</div> -->

<div class="select-tag" id="selectTags">
    <?php 
    foreach ($all_specs_autoselected as $key => $value) 
    {
        ?>
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo $value['spec_name']; ?></label>
            <div class="col-md-4">
                <input type="text" autocomplete="off" name="" id="val_<?php echo $value['spec_name_id']; ?>" class="form-control set_val">

                <span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;display:none" 
                spec-id-data="<?php echo $value['spec_name_id']; ?>" spec-value-data="">
            </span>
        </div>
    </div>
    <?php
}
?>
</div>


<div class="select-tag" id="selectTagsView" style="display:none">
    <?php 
    foreach ($all_specs_autoselected as $key => $value) 
    {
        ?>
        <div class="form-group">
            <label class="control-label col-md-3"><?php echo $value['spec_name']; ?></label>
            <div class="col-md-4">
                <input disabled="trure" type="text" autocomplete="off" name="" id="view_<?php echo $value['spec_name_id']; ?>" class="form-control set_val">
            </span>
        </div>
    </div>
    <?php
}
?>
</div>




<div class="form-group">
    <label class="control-label col-md-3"><?php echo $this->lang->line('custom_barcode'); ?></label>
    <div class="col-md-9 input-form input-icon">
        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Give barcode of your item if you want"></i>
        <input autocomplete="off" type="text"  class="form-control" name="barcode_number" placeholder="<?php echo $this->lang->line('custom_barcode_placeholder'); ?>" value="">
        <span class="help-block"></span>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_warranty'); ?></label>
    <div class="col-md-9 input-form">
        <input autocomplete="off" type="text"  class="form-control" name="product_warranty" placeholder="<?php echo $this->lang->line('product_warranty_placeholder'); ?>" value="">
        <span class="help-block"></span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-6"><b>Want to add IMEI/Serial number during buy ? </b></label>
    <div class="col-md-3 input-form input-icon">
        <input type="checkbox" style="width: 20px;height: 20px;" id="unique_barcode" name="unique_barcode">
    </div>
</div>

<input type="hidden" name="is_unique" id="is_unique">

<!-- <div class="testheightdiv" style="height:30px;"></div> -->
<div class="form-group" id="categoryAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('select_category'); ?> 
</label>
<div class="col-md-6">
    <input type="text" autocomplete="off" name="category" id="category_select" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">
    <input type="hidden" autocomplete="off" name="categories_id" id="category_select" class="form-control">
    <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
    </table>
</div>
<div class="col-sm-1">
    <a href="#categoryForm" title="<?php echo $this->lang->line('category_title'); ?> " data-toggle="modal" class="btn btn-icon-only btn-circle green"><i class="fa fa-plus"></i></a>
</div>
</div>
<div class="form-group" id="categoryDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('category'); ?> 
</label>
<div class="col-md-4">
    <input type="text" disabled="true" autocomplete="off" name="category_name_delete" id="category_select" class="form-control">
    <input type="hidden" autocomplete="off" name="categories_id_delete" id="category_select" class="form-control">
</div>
</div>

<div class="form-group" id="descriptionAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_description'); ?> </label>
    <div class="col-md-9">
        <textarea style="height:100px;" class="form-control" name="items_description" placeholder="<?php echo $this->lang->line('product_description'); ?>"></textarea>
    </div>
</div>
<div class="form-group" id="descriptionDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_description'); ?> </label>
    <div class="col-md-9">
        <textarea readonly="true" style="height:100px;" class="form-control" name="items_description_delete"></textarea>
    </div>
</div>

<div id="edit_image_of_user" class="form-group ">
    <label id="image_of_user_label" class="control-label col-md-3"> <?php echo $this->lang->line('product_pic'); ?></label>
    <div class="col-md-9">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div id="item_image" class="fileinput-preview thumbnail" name="item_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                <img src='<?php echo base_url() ?>images/item_images/no_image.png'>
            </div>
            <div>
                <span class="btn default btn-file">
                    <span class="fileinput-new">
                        <?php echo $this->lang->line('product_pic'); ?>
                    </span>
                    <span class="fileinput-exists">
                        <?php echo $this->lang->line('edit'); ?> 
                    </span>
                    <input type="file" name="edit_item_image">
                </span>
                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                    <?php echo $this->lang->line('delete'); ?>  
                </a>
            </div>
        </div>
    </div>
</div>

<div id="edit_image_of_user_delete" class="form-group " style="display:none" disabled="trure">
    <label id="image_of_user_label" class="control-label col-md-3"> <?php echo $this->lang->line('product_pic'); ?></label>
    <div class="col-md-9">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div id="item_image" class="fileinput-preview thumbnail" name="item_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
            </div>

        </div>
    </div>
</div>

</div>
<div class="form-actions" id="actionAdd">
    <button type="button" id="cancle_item" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancle'); ?></button>
    <button type="button" id="final_submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?> </button>
    <!-- <button type="button" id="viewAddItem" class="btn green pull-right">Add New Item</button> -->
</div>
<div class="form-actions" id="actionDelete" style="display:none">
    <button type="button" id="viewAddItem" class="btn green pull-right"><?php echo $this->lang->line('new_item'); ?></button>
</div>
</form>
</div>
</div>
</div>
<div class="col-md-6">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->

    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title_product'); ?>
            </div>

        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->lang->line('product_name'); ?>
                        </th>
                        <th>
                            <?php echo $this->lang->line('item_quantity'); ?>
                        </th>
                        <th>
                            Selling Price
                        </th>
                        <th>
                            <?php echo $this->lang->line('edit'); ?> / <?php echo $this->lang->line('delete'); ?> / <?php echo $this->lang->line('barcode'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

</div>
</div>

<!-- Modal -->

<div aria-hidden="true" role="dialog" tabindex="-1" id="barcode_quantity_form" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('barcode/add_barcode_quantity_form');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="update_price_form" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item/item_update_price_form');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="categoryForm" class="modal fade">   
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('category/add_category_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="unitForm" class="modal fade">   
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('unit/add_unit_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="brandForm" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('brand/add_brand_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="sizeForm" class="modal fade">   
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('size/add_size_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="colorForm" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item_color/add_color_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="lotForm" class="modal fade">    
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('lot_size/add_lot_size_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="specificationForm" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item/spec_name_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="specificationList" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item/spec_name_list');?>
        </div>
    </div>
</div>

<style type="text/css">
#specificationList
{
    width: 36%;
}
</style>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
            </div> -->
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
        <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
    </div>
</div>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- script for datatable export buttoon start -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/buttons.html5.min.js"></script>
<!-- script for datatable export buttoon end -->
<!-- Script for print button -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/buttons.print.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-tags/bootstrap-tagsinput.css">
<!-- Boostrap modal end -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->   
<script>
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
    $('input[name = unique_barcode]').val("off");
    //$('input[name = unique_barcode]').parents('div').removeClass('checker');
    $("#specDelete").hide();
    $("#categoryDelete").hide();
    $("#edit_image_of_user_delete").hide();
    $("#itemDelete").hide();
    $("#descriptionDelete").hide();

    $("#specAdd").show();
    $("#categoryAdd").show();
    $("#itemAdd").show();
    $("#descriptionAdd").show();
    $("#actionAdd").show();




    $(document).ready(function(){

        $("#suggesstion-box").html('');
        $("#search_key").keyup(function(){
            if ($(this).val() == '') {
                $("#suggesstion-box").html('');
            }
            else{
                $.ajax({
                    type: "POST",
                    url: "item/search",
                    data    : {
                        'keyword' : $(this).val(),
                        csrf_test_name: csrf
                    },

                    beforeSend: function(){
                        $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        var parsedData = JSON.parse(data);
                        //$("#suggesstion-box").html("");
                        var html_data = '';
                        html_data+= '<ul  role="menu" class="list-group">';                        
                        for (var i in parsedData) 
                        {
                            html_data+= '<a style="color:black;text-decoration:none;" href="#" onclick="set_value(\''+parsedData[i].items_name+'\');"><li class="list-group-item" data-original-index="'+i+'">'+parsedData[i].items_name+'</li></a><div style="height:3px;"></div>';
                        }
                        html_data+='</ul>';
                        $("#suggesstion-box").html(html_data);
                    }
                });
            }
        });
    });

    function set_value(search_value)
    {
        $('#search_key').val(search_value);
        $("#suggesstion-box").html('');
        $("#suggesstion-box ul").html('');
    }


    var csrf = $("input[name='csrf_test_name']").val();

    $("#category_select").keyup(function(event) 
    {
        $("#category_select_result").show();
        $("#category_select_result").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result").html(html);
            });
        }, 500);
    });

    $("#category_select_result").on('click', 'td', function(event) 
    {
        $('input[name="category"]').val($(this).html());
        $('input[name="categories_id"]').val($(this).attr('data'));
        $("#category_select_result").hide();
    });


    var dropdownPosition;
    var rowSelected = 0;
    $("#spec_select_result").html('');
    $(".custom-tag-input").keyup(function(event) {
        clearTimeout(timer);
        event.preventDefault();

        if (event.keyCode == 27) {
            $(".custom-tag-input").val('');
            $('#spec_select_result').html('');
        }
        else if(event.keyCode == 13) {
            if ($('#spec_select_result tr').hasClass('row-selected')) {
                var specId = $('#spec_select_result tr.row-selected td').attr('data');
                var specName = $('#spec_select_result tr.row-selected td').text();
                $("#inputType").val(specName);

                var html='';
                html += '<tr><td class="selected-spec-name td-middle-center" data="'+specId+'">'+specName+'</td><td class="td-middle-center"> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td class="td-middle-center"> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button> </td></tr>';    
                $("#spec_select_result").html(html);
                $(".input-spec-value").focus();

            } 
            else
            {

            };
            return false;
        }
        else if (event.keyCode == 38) 
        { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr:first').hasClass('row-selected')) {
        } else{
            rowSelected--;
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
    }
    else if (event.keyCode == 40) 
    { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr').length == $('#spec_select_result tr.row-selected').index()+1) {

        } else{
            if (!!$('#spec_select_result tr.row-selected').length) {
                rowSelected++;
            };
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
        console.log(rowSelected);
    }
    else
    {
        dropdownPosition = $(".custom-tag-input").position();
        rowSelected = 0;
        $("#spec_select_result").css({
            top: (dropdownPosition.top + 35)+'px',
            left: dropdownPosition.left+'px',
            width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
        }).html('<tr><td class="text-center"><i class="fa fa-refresh fa-spin"></i></td></tr>');
        timer = setTimeout(searchSpec, 200);
    }
});

    var searchSpec = function() {
        var searchType = $("#inputType").val();
        $("#spec_select_result").show();

        var html = '';

        if(searchType=='')
        {
            $("#spec_select_result").hide();
        }

        $.get('<?php echo site_url(); ?>/Item/search_spec_by_name/',{name: searchType}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                html+= '<tr><td class="spec-list" data="'+val.spec_name_id+'">'+val.spec_name+'</td></tr>';
            });
            $("#spec_select_result").css({
                top: (dropdownPosition.top + 35)+'px',
                left: dropdownPosition.left+'px',
                width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
            }).html(html);
        });
    };

    var spec_set_list = [];

    $('#spec_select_result_list').DataTable();

    $("#spec_select_result_list").on('click', '.spec-list', function(event) {
        $("#inputType").val($(this).text());
        var html='';
        html += '<tr ><td class="selected-spec-name" data="'+$(this).attr('data')+'" style="padding-top: 15px;text-align: center;">'+$(this).text()+'</td><td> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button></td></tr>';    
        $("#spec_select_result").html(html);
        $('#specificationList').modal('hide');
        $(".input-spec-value").focus();
    });



    $("#spec_select_result").on('click', '.spec-list', function(event) {
        $("#inputType").val($(this).text());
        var html='';
        html += '<tr ><td class="selected-spec-name" data="'+$(this).attr('data')+'" style="padding-top: 15px;text-align: center;">'+$(this).text()+'</td><td> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button></td></tr>';    
        $("#spec_select_result").html(html);
        $(".input-spec-value").focus();
    });



    $("#spec_select_result").on('keyup', '.input-spec-value', function(event) {
        event.preventDefault();
        if(event.keyCode == 13) {
            var html='';
            var spec_name = $("#spec_select_result td.selected-spec-name").text();
            var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
            var spec_name_value = $("#spec_select_result input").val();
        //alert(spec_name_value.toUpperCase());
        var myElements = $(".select-tag .spec-tags");
        if(myElements.length>0)
        {
            for (var i=0;i<myElements.length;i++) 
            {
                spec_data = {};
                spec_data.id = (myElements.eq(i).attr("spec-id-data"));
                spec_data.value = (myElements.eq(i).attr("spec-value-data"));
                if((spec_name_id == spec_data.id) && (spec_name_value.toUpperCase() == spec_data.value.toUpperCase()))
                {
                    alert('<?php echo $this->lang->line('exist_spec'); ?>');
                    return false;
                }
            }
        }

        html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <a href="#" class="remove_tag"><i class="fa fa-close clickable"></i></a> </span>';    
        $(html).appendTo('.spec-box');
        $("#spec_select_result").html('');
        $("#itemTooltip").hide();
        $(".custom-tag-input").val('').focus();
    }
    if (event.keyCode == 27) {
        $('#spec_select_result').html('');
        $(".custom-tag-input").focus();
        searchSpec();
    }
});


    $("#spec_select_result").on('click', '.select-spec-value', function(event) 
    {
    //sizeIds.push($(this).attr('data'));
    var html='';
    var spec_name = $("#spec_select_result td.selected-spec-name").text();
    var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
    var spec_name_value = $("#spec_select_result input").val();

    var myElements = $(".select-tag .spec-tags");
    if(myElements.length>0)
    {
        for (var i=0;i<myElements.length;i++) 
        {
            spec_data = {};
            spec_data.id = (myElements.eq(i).attr("spec-id-data"));
            spec_data.value = (myElements.eq(i).attr("spec-value-data"));
            if((spec_name_id == spec_data.id) && (spec_name_value.toUpperCase() == spec_data.value.toUpperCase()))
            {
                alert('<?php echo $this->lang->line('exist_spec'); ?>');
                return false;
            }
        }
    }

    html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <i class="fa fa-close clickable remove_tag"></i> </span>';    
    $(html).appendTo('.spec-box');
    $("#spec_select_result").html('');
    $(".custom-tag-input").val('').focus();
    $("#itemTooltip").hide();
    $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
    if (event.keyCode == 27)
    {
        alert('Esc key pressed.');
    }

});

    var spec_name_id = new Array();
    var spec_value = new Array();
    var img =1;
    $('.select-spec-combination').click(function() {
        var is_edit_form = $("#is_edit").val();
        if(is_edit_form==1)
        {
            $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
            if(spec_set_list.length>=1)
            {
                alert('<?php echo $this->lang->line('max_spec'); ?>');
                $('.select-tag .spec-tags').remove();
                return false;
            }
        }

        var myElements = $(".select-tag .spec-tags");

        $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
        if(myElements.length==0)
        {
            $("#item_form .specificationList").css('border', '1px solid red');
            alert('<?php echo $this->lang->line('select_spec_first'); ?>');return false;
        }
        $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
        $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
        var html = '<div class="form-group added" id="new_combinations_|'+spec_set_list.length+'"><label class="control-label col-md-3"><?php echo $this->lang->line('spec_combination'); ?></label><div style="max-width:55%" class="col-md-9">';
        $(".select-tag .spec-tags").each(function(index, el) 
        {
            html += $(el)[0].outerHTML;
        });

        html += '</div></div>';

        if(is_edit_form==1)
        {

        }
        else
        {
            $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
            html+= '<div style="position:absolute;right:20px;margin-top:-45px;">';
        //html+= $("#item_image")[0].outerHTML;
        html += '</div></div>';
        html+= '<div style="position:absolute;right:80px;margin-top:-45px;">';
        html+= '<div id="image_of_user" class="form-group " style="">';
        //html+= '<label id="image_of_user_label" class="control-label col-md-3"> Product picture</label>';
        html+= '<div class="col-md-9">';
        html+=     '<div class="fileinput fileinput-new" data-provides="fileinput" >';
        html+=     '<div style="height:50px;width:60px;margin-top:-20px;" id="item_image" class="fileinput-preview thumbnail " name="item_image" data-trigger="fileinput">';
        html+=     '</div>';
        html+= '<div>';
        html+= '<span class="btn default btn-file">';
        html+= '<span class="fileinput-new" style="right:60px;"><?php echo $this->lang->line('product_pic'); ?></span>';
        html+= '<span class="fileinput-exists" style="width:50px">Edit</span>';
        html+= '<input type="file"  name="combination_images_'+img+'"></span>';
        html+= '<a style="padding:10px;" href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Delete </a>';
        html+= '</div></div></div></div>';
        html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    }

    html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    $(html).insertAfter(".select-tag");
    $('.select-tag .spec-tags').remove();

    var spec_set = [];
    var spec_values = '';
    var item_new_combination = '';
    var item_name = $('input[name="items_name"]').val();
    for (var i=0;i<myElements.length;i++) {
        spec_data = {};
        spec_data.id = (myElements.eq(i).attr("spec-id-data"));
        spec_data.value = (myElements.eq(i).attr("spec-value-data"));
        //spec_data.image = imageName;
        spec_set.push(spec_data);
        spec_values += '-';
        spec_values +=     (myElements.eq(i).attr("spec-value-data"));
    }
    
    spec_set_list.push(spec_set);
    img++;
    item_new_combination = item_name+'-'+spec_values;
    $("#item_new_name").html(item_new_combination);
    return false;
});

    $('#item_form').on('click', '.remove_tag', function () 
    {
        if($(this).closest('.form-group').hasClass('added')){    
            var set_id = $(this).closest('.form-group').attr('id').split('|')[1];
            var  se =  $(this).parent('.spec-tags');
            $.each(spec_set_list[set_id], function(index, val) {
                if(val.id == se.attr('spec-id-data'))
                {
                    spec_set_list[set_id].splice(index, 1);
                    return false;
                }
            });
        }
        $(this).parent('.spec-tags').remove();
    });



    var table_item = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : 'Bfrltip',
        "draw": 1,
        "StateSave": true, 
        "buttons" : [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5',
        'print'
        ],
        "ajax": "<?php echo site_url(); ?>/item/all_item_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ]
    });



    $('#cancle_item').click(function(event) {
        spec_set_list.length =0;
        $("div").remove(".added");
        $("div").remove(".testheightdiv");
        $('input[name="barcode_number"]').val('');
        $('input[name="product_warranty"]').val('');
        $("#selectTags .set_val").val(''); /******for new item page design*******/
        $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
        $("#selectTags").show();        /******for new item page design*******/
        $("#selectTagsView").hide();   /******for new item page design*******/

        $('#item_form').attr('action', "<?php echo site_url('/Item/save_item_info');?>");
        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");

        $("#item_form #image_of_user").remove();
        $("#edit_image_of_user").hide();
        $('input[name="items_name"]').val('');
        $('input[name="category"]').val('');
        $('textarea[name="items_description"]').val('');
        $('input[name="product_warranty"]').val('');
        $('#category_select').val('');
        $('input[name="unique_id"]').val('');
        $(".spec-tags").remove();
        $("#image_of_user").remove();

    });


    $('#item_form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });


    $('#final_submit').click(function(event) {

        var spec_set = [];
        var spec_values = '';
        var item_new_combination = '';
        var myElements = $(".select-tag .spec-tags");
        var item_name = $('input[name="items_name"]').val();
        var barcode = $('input[name="barcode_number"]').val();
        $('input[name = barcode_number]').removeAttr('disabled');

        if(barcode!='')
        {
            if(barcode.length<4)
            {
                // $('#barcode_invalid').slideDown();
                // setTimeout( function(){$('#barcode_invalid').slideUp()}, 4500 );
                toastr.error('<?php echo $this->lang->line('barcode_invalid'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
                return false;
            }
        }
        for (var i=0;i<myElements.length;) {
            spec_data = {};
            spec_data.id = (myElements.eq(i).attr("spec-id-data"));
            spec_data.value = (myElements.eq(i).attr("spec-value-data"));
            if(spec_data.value=="")
            {
                i++;
            }
            else
            {
               spec_set.push(spec_data);
               i++;
           }

       }

       spec_set_list.push(spec_set);
       if(spec_set_list.length==0)
       {
        alert("please give at least one specification value");
    }
    else
    {
       console.log(spec_set_list); 
       $('#item_form').submit();      
   }
});


    $('#item_form').ajaxForm({
        data: {
            specs_values: spec_set_list
        }, 
        beforeSubmit: function() {   
           // console.log(spec_set_list); exit;
       },
       success: function(msg) {

        if(msg == "No permission")
        {
            // $('#not_permitted').slideDown();
            // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            
        }
                // if(msg == "success"){spec_set
                    //     $('#insert_success').slideDown();
                    //     setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
                    // }

                    else if(msg == "success")
                    {

                        $("#selectTags .set_val").val(''); /******for new item page design*******/
                        $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
                        $("#selectTags").show();        /******for new item page design*******/
                        $("#selectTagsView").hide();   /******for new item page design*******/

                        $('#no_item').hide();
                        $('#no_spec').hide();
                        $('input[name="items_name"]').css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});

                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        // $('#insert_success').slideDown();
                        // setTimeout( function(){$('#insert_success').slideUp()}, 2500 );
                        toastr.success('<?php echo $this->lang->line('product_insert_succeded'); ?>', 'Success', {timeOut: 6000});
                        toastr.options = {"positionClass": "toast-top-right"}; 
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $("#item_form #image_of_user").remove();
                        $('input[name="items_name"]').val('');
                        $('input[name="category"]').val('');
                        $('input[name="barcode_number"]').val('');
                        $('input[name="product_warranty"]').val('');
                        $('textarea[name="items_description"]').val('');
                        $('#category_select').val('');
                        $('input[name="unique_id"]').val('');
                        $('input[name = unique_barcode]').parents('span').removeClass('checked');
                        $('input[name = unique_barcode]').val("off");
                        $('input[name = is_unique]').val("no");
                        $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                       // $(".spec-tags").remove(); /*-----Not need to remove fore new designed system----*/
                       $("#image_of_user").remove();

                   }
                   else if(msg =="failed")
                   {
                    // $('#insert_failure').slideDown();
                    // setTimeout( function(){$('#insert_success').slideUp()}, 4500 );  *** 2 types of error message
                    toastr.error('<?php echo $this->lang->line('product_insert_failed'); ?>', 'Failed', {timeOut: 6000});
                    toastr.options = {"positionClass": "toast-top-right"}; 
                }

                if(msg=="barcode_exist")
                {
                    spec_set_list.length =0;
                    // $('#barcode_exist').slideDown();
                    // setTimeout( function(){$('#barcode_exist').slideUp()}, 5500 );
                    toastr.error('<?php echo $this->lang->line('barcode_exist'); ?>', 'Failed', {timeOut: 6000});
                    toastr.options = {"positionClass": "toast-top-right"}; 
                }

                if(msg == "Give a item name")
                {
                    spec_set_list.length =0;
                    $('#no_item').slideDown();
                    $('input[name="items_name"]').css({"border": "1px solid red"});
                        //setTimeout( function(){$('#no_item').slideUp()}, 1500 );
                    }
                    if(msg == "Choose item spec combination")
                    {
                        spec_set_list.length =0;
                        $("#item_form .select-spec-combination").css({"border": "1px solid red", "color": "red"});
                        // $('#no_spec').slideDown();
                        // setTimeout( function(){$('#no_spec').slideUp()}, 1500 );
                        toastr.error('<?php echo $this->lang->line('choose_spec'); ?>', 'Failed', {timeOut: 6000});
                        toastr.options = {"positionClass": "toast-top-right"}; 

                    }

                    if(msg == "Update Success")
                    {
                     $("#selectTags .set_val").val(''); /******for new item page design*******/
                     $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
                     $("#selectTags").show();        /******for new item page design*******/
                     $("#selectTagsView").hide();   /******for new item page design*******/

                     $('#no_item').hide();
                     $('#no_spec').hide();
                     $('input[name="items_name"]').css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});

                        // $('#update_success').slideDown();
                        // setTimeout( function(){$('#update_success').slideUp()}, 1500 );

                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        $('#item_form').attr('action', "<?php echo site_url('/Item/save_item_info');?>");
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $('input[name="items_name"]').val('');
                        $("#is_edit").val('');
                        $('input[name="category"]').val('');
                        $('input[name="items_description"]').val('');
                        $('input[name="barcode_number"]').val('');
                        $('input[name="product_warranty"]').val('');
                        $('#category_select').val('');

                        $('input[name="unique_id"]').val('');
                        $('#unique_barcode').prop('checked', false);
                        $('input[name = unique_barcode]').parents('span').removeClass('checked');
                        $('input[name = unique_barcode]').val("off");
                        $('input[name = is_unique]').val("no");

                        // $('#update_success').slideDown();
                        // setTimeout( function(){$('#update_success').slideUp()}, 1500 );
                        toastr.success('<?php echo $this->lang->line('product_update_succeded'); ?>', 'Success', {timeOut: 6000});
                        toastr.options = {"positionClass": "toast-top-right"}; 

                        //$(".spec-tags").remove();         /*************It is not necessary for new item design***********/
                       // $("#edit_image_of_user").hide();  /*************It is not necessary for new item design***********/

                       $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>"); 


                       table_item.ajax.reload( null, false);
                      //table_item.row(index).data(data).draw(false);
                      return false;
                  }
                    // if(msg == "Give a item category")
                    // {
                    //     $('#category_add').slideDown();
                    //     setTimeout( function(){$('#category_add').slideUp()}, 1500 );
                    // }
                    if(msg == "Please choose atmost one combination")
                    {
                        spec_set_list.length =0;
                        // $('#one_combination').slideDown();
                        // setTimeout( function(){$('#one_combination').slideUp()}, 1500 );
                        toastr.error('<?php echo $this->lang->line('max_spec'); ?>', 'Failed', {timeOut: 6000});
                        toastr.options = {"positionClass": "toast-top-right"}; 
                    }
                },
                complete: function(xhr) {

                    table_item.ajax.reload( null, false);
                    return false;
                }
            }); 

/* **********************  
------------------------- 
********************** */

</script>

<script type="text/javascript">


    $("#unique_barcode").click(function(event) 
    {
        var checkValue = $('input[name = unique_barcode]').val();
        console.log(checkValue);
        if(checkValue==='on')
        {
            $('input[name = unique_barcode]').parents('span').removeClass('checked');
            $('input[name = unique_barcode]').val("off");
            $('input[name = is_unique]').val("no");
        }
        else
        {
            $('input[name = unique_barcode]').parents('span').addClass('checked');
            $('input[name = unique_barcode]').val("on");
            $('input[name = is_unique]').val("yes");
        }
    });

    $('table').on('click', '.edit_item',function(event) 
    {
        event.preventDefault();
        $('input[name = unique_barcode]').parents('div').removeClass('checker');
        $("#itemTooltip").hide();
        $("#specDelete").hide();
        $("#categoryDelete").hide();
        $("#edit_image_of_user_delete").hide();
        $("#itemDelete").hide();
        $("#descriptionDelete").hide();
        $("#viewAddItem").hide();
        $("#selectTags").show();        /******for new item page design*******/
        $("#selectTagsView").hide();   /******for new item page design*******/
        $("#specAdd").show();
        $("#categoryAdd").show();
        $("#itemAdd").show();
        $("#descriptionAdd").show();
        $("#actionAdd").show();

        var item_id = $(this).attr('id').split('_')[1];
        $('#current_item_id').remove();
        $( "div" ).remove( ".added" );
        $("#image_of_user").remove();
        
        $("#is_edit").val(1);
        $.get('<?php echo site_url();?>/Item/get_item_info/'+item_id).done(function(data) 
        {
            var htm='';
            var item_data = $.parseJSON(data);
            if(item_data == "No permission")
            {
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else
            {
                $("#edit_image_of_user").show();
                var spec_values = item_data.spec_sets;
                var all_specs_autoselected = item_data.all_specs_autoselected;
                $.each(all_specs_autoselected, function(index, val) 
                {
                    $("#val_"+val.spec_name_id).val("");
                    $("#val_"+val.spec_name_id).next($('.spec-tags')).attr('spec-value-data',"");
                });
                $.each(spec_values, function(index, val) 
                {
                    $("#val_"+val.spec_name_id).val(val.spec_value);
                    $("#val_"+val.spec_name_id).next($('.spec-tags')).attr('spec-value-data',val.spec_value);
                });

                $(".custom-tag-input").val('').focus();
                var html = "<input id='current_item_id' type='hidden' class='form-control' name='items_id' value='"+item_data.item_info_by_id.items_id+"'>";
                var htm = "<input id='current_spec_id' type='hidden' class='form-control' name='item_spec_set_id' value='"+item_data.item_info_by_id.item_spec_set_id+"'>";
                $('#item_form').append(html);
                $('#item_form').append(htm);
                $('#item_form').attr('action', "<?php echo site_url('/Item/update_item_info');?>");
                $('#header_text').text("<?php echo $this->lang->line('product_edit'); ?>");
                $('input[name = barcode_number]').removeAttr('disabled');
                $('input[name = items_name]').val(item_data.item_info_by_id.items_name);
                $('input[name = category]').val(item_data.item_info_by_id.categories_name);
                $('input[name = barcode_number]').val(item_data.item_info_by_id.barcode_number);
                if(item_data.item_info_by_id.unique_barcode=="yes")
                { 
                    $('input[name = unique_barcode]').parents('span').addClass('checked');
                    $('input[name = unique_barcode]').val("on");
                    $('input[name = is_unique]').val("yes");
                    $('#unique_barcode').attr('checked', true); // Checks it
                }
                else
                {
                    $('input[name = unique_barcode]').parents('span').removeClass('checked');
                    $('input[name = unique_barcode]').val("off");
                    $('input[name = is_unique]').val("no");
                    $('#unique_barcode').attr('checked', false); // Checks it
                }

                $('input[name = product_warranty]').val(item_data.item_info_by_id.product_warranty);
                $('input[name = categories_id]').val(item_data.item_info_by_id.categories_id);
                $('textarea[name = items_description]').val(item_data.item_info_by_id.items_description);
                $('#category_select').html('<option value="'+item_data.item_info_by_id.categories_id+'" data-subtext="'+item_data.item_info_by_id.categories_id+'" selected="selected">'+item_data.item_info_by_id.categories_name +'</option>');
                $('#category_select').trigger('change');
                $('select[name=items_status]').val(item_data.item_info_by_id.items_status);
                if(item_data.item_info_by_id.item_spec_set_image != "" && item_data.item_info_by_id.item_spec_set_image!=null)
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+item_data.item_info_by_id.item_spec_set_image+"'>");
                }
                else
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                }

                // var post_data={};
                // post_data.items_name_edit = $('input[name = item_new_name]').val();
                // post_data.item_spec_set_id = $('input[name=item_spec_set_id]').val();
                // post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
                // var row_data = table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data();
                // row_data[0] = post_data.items_name_edit;
                // table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data(row_data).draw();
            }

        });
});


$("#viewAddItem").click(function(event) 
{
    $(this).hide();
    $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
    $('input[name = barcode_number]').removeAttr('disabled');
    $('input[name = product_warranty]').removeAttr('disabled');
    $('input[name = barcode_number]').val('');
    $('input[name = product_warranty]').val('');
    $('input[name="items_name"]').val('');
    $("#is_edit").val('');
    $("#specDelete").hide();
    $("#categoryDelete").hide();
    $("#edit_image_of_user_delete").hide();
    $("#itemDelete").hide();
    $("#descriptionDelete").hide();

    $("#selectTags").show();        /******for new item page design*******/
    $("#selectTagsView").hide();   /******for new item page design*******/
    $("#selectTags .set_val").val(''); /******for new item page design*******/
    $("#selectTagsView .set_val").val(''); /******for new item page design*******/
    $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/




    $("#specAdd").show();
    $("#categoryAdd").show();
    $("#itemAdd").show();
    $("#descriptionAdd").show();
    $("#actionAdd").show();
});


$('table').on('click', '.view_item',function(event) {

    event.preventDefault();
    $("#itemTooltip").hide();
    var item_id = $(this).attr('id').split('_')[1];
    $("#viewAddItem").show();
    $("#specAdd").hide();
    $("#categoryAdd").hide();
    $("#itemAdd").hide();
    $("#descriptionAdd").hide();
    $("#actionAdd").hide();

    $("#selectTags").hide();        /******for new item page design*******/
    $("#selectTagsView").show();   /******for new item page design*******/

    $("#specDelete").show();
    $("#categoryDelete").show();
    $("#edit_image_of_user_delete").show();
    $("#itemDelete").show();
    $("#descriptionDelete").show();
    $("#actionDelete").show();
    

    $("#edit_image_of_user").hide();
    $('#current_item_id').remove();
        //$("#spec-tag-field").remove(".spec-tags");
        //$( ".spec-tags" ).remove();  /**********not needed for new design******************/
        $( "div" ).remove( ".added" );
        $("#image_of_user").remove();
        
        $("#is_edit").val(2);
        $.get('<?php echo site_url();?>/Item/get_item_info/'+item_id).done(function(data) {
            var html='';
            var item_data = $.parseJSON(data);
            if(item_data == "No permission")
            {
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else
            {

                var spec_values = item_data.spec_sets;
                var all_spec_values = item_data.all_specs_autoselected;
                /******************** Not needed for new design ***********************/
                // $.each(spec_values, function(index, val) {
                //     html+='<span class="label label-success spec-tags"';
                //     html+='style="float: left;top: 24px; margin-top: 9px;margin-right:5px;"';
                //     html+='spec-id-data="'+val.spec_name_id+'"';    
                //     html+= 'spec-value-data="'+val.spec_value+'">'+val.spec_name+'-'+val.spec_value; 
                //     html+= '<a href="#" class="remove_tag">';
                //     html+= '<i class="fa fa-close clickable"></i></a>'; 
                //     html+= '</span>';
                // });
                /******************** Not needed for new design ***********************/
                // var all_specs_autoselected = item_data.all_specs_autoselected;

                $.each(all_spec_values, function(index, val) 
                {
                    $("#view_"+val.spec_name_id).val("");
                });

                $.each(spec_values, function(index, val) 
                {
                    $("#view_"+val.spec_name_id).val(val.spec_value);
                });

                // $(html).insertAfter(".custom-tag-input");

                $("#spec-tag-field-delete").html(html);
                $("#spec_select_result").html('');
                $(".custom-tag-input").val('').focus();
                $("#specDelete .remove_tag").remove();
                // var html = "<input id='current_item_id' type='hidden' class='form-control' name='items_id' value='"+item_data.item_info_by_id.items_id+"'>";
                // var htm = "<input id='current_spec_id' type='hidden' class='form-control' name='item_spec_set_id' value='"+item_data.item_info_by_id.item_spec_set_id+"'>";
                // $('#item_form').append(html);
                // $('#item_form').append(htm);
                // $('#item_form').attr('action', "<?php echo site_url('/Item/update_item_info');?>");
                $('#header_text').text("<?php echo $this->lang->line('product_view'); ?>");
                $('input[name = items_name_delete]').val(item_data.item_info_by_id.items_name);
                $('input[name = category_name_delete]').val(item_data.item_info_by_id.categories_name);
                $('input[name = barcode_number]').val(item_data.item_info_by_id.barcode_number);
                $('input[name = barcode_number]').attr('disabled','disabled');
                $('input[name = product_warranty]').val(item_data.item_info_by_id.product_warranty);
                $('input[name = product_warranty]').attr('disabled','disabled');
                //$('input[name = unique_id]').val(item_data.item_info_by_id.barcode_number);
                $('input[name = categories_id_delete]').val(item_data.item_info_by_id.categories_id);
                $('textarea[name = items_description_delete]').val(item_data.item_info_by_id.items_description);
                $('#category_select').html('<option value="'+item_data.item_info_by_id.categories_id+'" data-subtext="'+item_data.item_info_by_id.categories_id+'" selected="selected">'+item_data.item_info_by_id.categories_name +'</option>');
                $('#category_select').trigger('change');
                $('select[name=items_status]').val(item_data.item_info_by_id.items_status);
                //$("#final_submit").hide();
                if(item_data.item_info_by_id.item_spec_set_image != "" && item_data.item_info_by_id.item_spec_set_image!=null)
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+item_data.item_info_by_id.item_spec_set_image+"'>");
                }
                else
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                }

                // var post_data={};
                // post_data.items_name_edit = $('input[name = item_new_name]').val();
                // post_data.item_spec_set_id = $('input[name=item_spec_set_id]').val();
                // post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

                // var row_data = table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data();
                // row_data[0] = post_data.items_name_edit;
                // table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data(row_data).draw();
            }
        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

$("#selectTags .set_val").keyup(function(event) 
{
    var value = $(this).val();
    $(this).next("span.spec-tags").attr("spec-value-data",value);
});
</script>
<script type="text/javascript">
    var items_id = '' ;
    $('table').on('click', '.delete_item',function(event) {
        event.preventDefault();
        items_id = $(this).attr('id').split('_')[1];
    });


    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        //var items_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'items_id' : items_id,
            csrf_test_name: csrf

        }; 

        $.post('<?php echo site_url();?>/Item/delete_item/'+items_id,post_data).done(function(data) {
            if(data == "No permission")
            {
                $('#responsive_modal_delete').modal('hide');
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else if(data == "success")
            {
                $('#responsive_modal_delete').modal('hide');
                // $('#delete_success').slideDown();
                // setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                toastr.success('<?php echo $this->lang->line('product_delete_success'); ?>', 'Success', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
                table_item.row($('#delete_'+items_id).parents('tr')).remove().draw();
            }
            else if(data == "inventory_exist")
            {
                // $('#inventory_exist').slideDown();
                // setTimeout( function(){$('#inventory_exist').slideUp()}, 6000 );
                $('#responsive_modal_delete').modal('hide');
                toastr.error('<?php echo $this->lang->line('inventory_exist'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else
            {
                alert("An error has occured. pLease try again");                
            }
        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });


// ajax form submit starts....

$('#spec_name_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.spec_name = $('input[name=spec_name]').val();
    data.spec_description = $('input[name=spec_description]').val();
    data.spec_default_status = $('input[name=spec_default_status]:checked').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/spec_name/save_spec_name_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 =="No Permisssion"){
            // $('#not_permitted').slideDown();
            // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('.help-block').show();
            });
        }

        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();

            $('input[name= spec_name]').val('');
            $('input[name= spec_description]').val('');
            // $('#insert_success_spec_name').slideDown();
            // setTimeout( function(){$('#insert_success_spec_name').slideUp();}, 1500 );
            toastr.success('<?php echo $this->lang->line('spec_insert_success'); ?>', 'Success', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            $('#specificationForm').modal('hide');
            $("#spec_select_result_list > tbody").append('<tr><td class="spec-list sorting_1" data="'+data2.data+'">'+data.spec_name+'</td></tr>');
            // <td class="spec-list sorting_1" data="59dd87e1-bac4-11e6-b419-fcaa14ede82b">SDSD</td>
            //spec_select_result_list.ajax.reload();
            // $('#spec_select_result_list').DataTable();
        }

    }).error(function() {

        // $('#insert_failure').slideDown();
        // setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
        toastr.error('<?php echo $this->lang->line('product_insert_failed'); ?>', 'Failed', {timeOut: 6000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    });    
});

// ajax form submit end.......


    // ajax form submit starts....

    $('#category_form').submit(function(event) {
        event.preventDefault();
        var data = {};
        data.categories_name = $('input[name=categories_name]').val();
        data.categories_description = $('input[name=categories_description]').val();
        data.parent_categories_id = $('input[name=parent_categories_id]').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/category/save_category_info',data).done(function(data1, textStatus, xhr) {

            var data2 = $.parseJSON(data1);

            if(data2.success==0)
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }
            else{
                $('.form-group.has-errorf').removeClass('has-error');
                $('.help-block').hide();

                $('input[name= categories_name]').val('');
                $('input[name= categories_description]').val('');
                $('input[name= parent_categories_id]').val('');
                $('#categoryForm').modal('hide');
                // $('#insert_success_category').slideDown();
                // setTimeout( function(){$('#insert_success_category').slideUp();}, 3000 );
                toastr.success('<?php echo $this->lang->line('category_insert_success'); ?>', 'Success', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
                table_category.row.add({
                    "0":       data.categories_name,
                    "1":   "<a class='btn btn-primary edit_category' id='edit_"+data2.data+"' >    à¦ªà¦°à¦¿à¦¬à¦°à§à¦¤à¦¨ </a><a class='btn btn-danger delete_category' id='delete_"+data2.data+"'>à¦¡à¦¿à¦²à¦¿à¦Ÿ </a>"
                }).draw();
            }

        }).error(function() {

            // $('#insert_failure').slideDown();
            // setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
            toastr.error('<?php echo $this->lang->line('product_insert_failed'); ?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        });    
    });

// ajax form submit end.......

// ajax form submit starts....
$(document).on('click', '.update_item_price', function(event) {
    event.preventDefault();
    $('#update_price_form').modal('show');
    var item_id = $(this).attr('id').split('_')[1];
    $('#price_item_id').val(item_id);

    $.get('<?php echo site_url();?>/Item/get_item_price_info/'+item_id).done(function(data) {
        var item_data = $.parseJSON(data);
        $('input[name=retail_price]').val(item_data.retail_price);
        $('input[name=whole_sale_price]').val(item_data.whole_sale_price);
    }).error(function() {
        alert("An error has occured. Please try again");
    });
});

$('#item_update_price_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.retail_price = $('input[name=retail_price]').val() || 0;
    data.whole_sale_price = $('input[name=whole_sale_price]').val() || 0;
    data.item_id = $('#price_item_id').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/item/save_item_update_price_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 ==  "success"){
         $('#update_price_form').modal('hide');
         $('#price_item_id').val('');
         $('#item_update_price_form')[0].reset();
         toastr.success('Item Price Updated Successfully', 'Success', {timeOut: 6000});
         toastr.options = {"positionClass": "toast-top-right"}; 
     }
     else if(data2 == "not_updated"){
        toastr.error('Item Price Not Updated', 'Failed', {timeOut: 6000});
        toastr.options = {"positionClass": "toast-top-right"}; 
    }
    else
    {
        toastr.error('Failed. Item Price Not Updated', 'Failed', {timeOut: 6000});
        toastr.options = {"positionClass": "toast-top-right"}; 

    }

}).error(function() {
 toastr.error('Failed. Item Price Not Updated', 'Failed', {timeOut: 6000});
 toastr.options = {"positionClass": "toast-top-right"}; 
});    
});

$(document).on('click', '#price_edit_cancle', function(event) {
    event.preventDefault();
    /* Act on the event */
    $('#update_price_form').modal('hide');
    $('#price_item_id').val('');
    $('#item_update_price_form')[0].reset();
});

// ajax form submit end.......


/* Item-wise Barcode Printing Starts */
// $('.barcode_print').click(function(event) {

    //     alert ("Barcode Print");
    // });

    /* Barcode Print Ends*/

// $(document).on('click', '.item_checkbox', function(event) {
    //     // event.preventDefault();

    //     alert ("item selected");
    // });
    $('table').on('click', '.item_barcode', function(event) {
        event.preventDefault();
        $('.has-error').removeClass('has-error');
        $('.help-block').text('');
        var items_id = $(this).attr('id').split('_')[1];
        $('#current_item_id').remove();
        $.get('<?php echo site_url();?>/Item/get_item_barcode_info/'+items_id).done(function(data) {
            var item_data = $.parseJSON(data);
            $('#barcode_quantity_form').modal('show');
            $('input[name=unique_id]').val(item_data.barcode_number);
        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });
    /* Barcode Print Starts*/
    $('#barcode_quantity_form').submit(function(event) {
        event.preventDefault();
        var post_data={};
        post_data.barcode_quantity = $('input[name = print_quantity]').val();
        post_data.unique_id = $('input[name=unique_id]').val();
        post_data.page_size  = $('input[name="page_type"]:checked').val();
        post_data.company_name_add =  $('input[name="add_company"]:checked').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
        if (post_data.barcode_quantity == "") {
            $('input[name= print_quantity]').parent().parent().addClass('has-error');
            $('input[name= print_quantity]').parent().parent().find('.help-block').text("<?php echo $this->lang->line('barcode_quantity_alert'); ?>");
        }
        else if(!$('input[name="page_type"]:checked').val()){
            alert("<?php echo $this->lang->line('page_size_alert'); ?>");
        }
        else {
            location.href="<?php echo site_url() ?>/item/print_barcode/"+post_data.unique_id+"/"+post_data.barcode_quantity+"/"+post_data.page_size+"/"+post_data.company_name_add;
        }
    });
    /* Barcode Print Ends*/
    $('body').tooltip({
        selector: '.fa.fa-question-circle.tooltips'
    });
</script>