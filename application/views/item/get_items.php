<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<div style="display:none;" class="row">	
	<div class="col-md-6">
		<select class="col-md-12 col-sm-12 col-lg-12 form-control" id="item_type_selector" class="form-control input-inline">
			<option value="">Select item type</option>
			<option value="Cloths">Cloths</option>
			<option value="Grocery">Grocery</option>
			<option value="Electronics">Electronics</option>
		</select> <br><br><br>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	<!-- <?php echo "<pre>";  print_r($items); ?> -->
		<div class="portlet box green">
			<div class="portlet-title">
				<div id="table_header_enlisted" class="caption">Collected items
					
				</div>
				
				<div style="display: none;" id="table_header_enlisted" class="caption pull-right">
					<input class="btn default" type="button" id="insert_all" value="Insert all">
				</div>

			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dataTable" id="item_list" style="z-index: 10;background-color: #fff;">
					<thead>
						<th>Item Name</th>
						<th style="display:none;" >JSON</th>
						<th>Action</th>

					</thead>
					<tbody>
						<?php 
						foreach ($items as $key => $value) {

							?><tr><?php

							?>

							<td><?php echo $value['spec_set']?></td><?php
							?><td style="display:none;" ><?php echo $value['item_json']?></td><?php

							?><td><button class="btn btn-sm green enlist_item">ADD</button></td><?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<!-- <script type="text/javascript" src="<?php echo base_url(); ?>/assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/scripts/datatable.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script type="text/javascript">


	$('#item_type_selector').change(function(event) {
		var get_type = $('#item_type_selector').val();
		if(get_type == 'Cloths'){
			$('#table_header_enlisted').html('Cloths items (Enlisted)');
		}else if(get_type == 'Grocery' || get_type == ''){
			$('#table_header_enlisted').html('Grocery items (Enlisted)');
		}else if(get_type == 'Electronics'){
			$('#table_header_enlisted').html('Electronics items (Enlisted)');
		}
	});

	$('#item_list').dataTable();

	$("#item_list").on('click', '.enlist_item', function(event) {
		event.preventDefault();
		var item_json_str = $(this).parent().siblings(":nth-child(2)").text();

		$.post('<?php echo site_url()?>/GetItems/callInsertItem/', {'itme_json': item_json_str}, function(data, textStatus, xhr) {
			if(data == "success"){
				toastr.success('Item added successfully', '', {timeOut: 2000});
				toastr.options = {"positionClass": "toast-top-right"};
			}else if(data == "exist"){
				toastr.warning('This item allready exist', '', {timeOut: 2000});
				toastr.options = {"positionClass": "toast-top-right"};

			}else if(data == "fail"){
				toastr.error('Item adding failed', '', {timeOut: 2000});
				toastr.options = {"positionClass": "toast-top-right"};
			}
		});

	});

	$("#insert_all").click(function(event) {
		alert('All');
	});

</script>

