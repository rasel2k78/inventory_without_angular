	<div>
		<div class="alert alert-success" id="insert_success_category" style="display:none" role="alert"><?php echo $this->lang->line('category_insert_success'); ?></div>    <div class="alert alert-danger" id="insert_failure_category" style="display:none" role="alert"><?php echo $this->lang->line('category_insert_failure'); ?></div>    
        
        <div class="alert alert-success" id="insert_success_spec" style="display:none" role="alert"><?php echo $this->lang->line('spec_insert_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure_spec" style="display:none" role="alert"><?php echo $this->lang->line('spec_insert_failure'); ?></div>
        
    </div>
    <div class="portlet box red">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-pencil-square-o"></i><span id="header_text"><?php echo $this->lang->line('left_portlet_title_product'); ?></span>
            </div>
        </div>

        <input name="item_new_name" type="hidden" id="item_new_name">
        <div class="portlet-body form">

    <?php $form_attrib = array('id' => 'item_form','class' => 'form-horizontal form-bordered', 'autocomplete' => 'off');
    echo form_open_multipart('Item/save_item_info',  $form_attrib);?>
            <div class="form-body">
                <div class="form-group <?php if(isset($error['items_name'])) { echo 'has-error'; 
               } ?> ">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('product_name'); ?> *</label>
                    <div class="col-md-9">
                        <input autocomplete="off" type="text" id="search_key" class="form-control" name="items_name" placeholder="<?php echo $this->lang->line('product_name'); ?>" value="<?php if(isset($previous_data['items_name'])) { echo $previous_data['items_name']; 
                       } ?>" >

                        <div id="suggesstion-box"></div>
                        <span class="help-block"><?php if(isset($error['items_name'])) { echo $error['items_name']; 
                       } ?></span>
                    </div>
                </div>
                <input type="hidden" name="is_edit" id="is_edit">

                <div class="form-group select-tag">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('specification'); ?>  *</label>
                    <div class="col-md-7">
                        <div style="overflow: hidden; padding: 1px 5px;" id="spec-tag-field">

                            <a class="btn btn-default select-spec-combination" style="position: absolute;right: 16px;z-index: 11;"><?php echo $this->lang->line('ok'); ?></a>
                            <input type="text" id="inputType" class="custom-tag-input" placeholder="<?php echo $this->lang->line('specification'); ?>" style="float: left;height: 36px;width:60%;margin-left: 5px;border:0px;" autocomplete="off">



                            <table class="table table-condensed table-hover table-bordered clickable" id="spec_select_result" style="position: absolute;z-index: 10;width:210px;background-color: #fff;">
                            </table>
                        </div>
                    </div>
                    <div class="fa-item col-sm-1">
                        <a href="#specificationForm" data-toggle="modal" class="btn btn-circle btn-icon-only green"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="form-group" id="combination_specs">
                </div>
                <div class="testheightdiv" style="height:30px;"></div>
                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('select_category'); ?> 
                    </label>
                    <div class="col-md-6">

                        <input type="text" autocomplete="off" name="category" id="category_select" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">
                        <input type="hidden" autocomplete="off" name="categories_id" id="category_select" class="form-control">
                        <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                        </table>

                    </div>
                    <div class="fa-item col-sm-1">
                        <a href="#categoryForm" data-toggle="modal" class="btn btn-circle btn-icon-only green"><i class="fa fa-plus"></i></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3"><?php echo $this->lang->line('product_description'); ?> </label>
                    <div class="col-md-9">
                        <!-- <input type="text" class="form-control" name="items_description" placeholder="<?php echo $this->lang->line('product_description'); ?> "> -->

                        <textarea style="height:100px;" class="form-control" name="items_description" placeholder="<?php echo $this->lang->line('product_description'); ?>"></textarea>
                    </div>
                </div>

                <div id="edit_image_of_user" class="form-group " style="display:none">
                    <label id="image_of_user_label" class="control-label col-md-3"> <?php echo $this->lang->line('product_pic'); ?></label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div id="item_image" class="fileinput-preview thumbnail" name="item_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                            </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new">
            <?php echo $this->lang->line('product_pic'); ?>
                                    </span>
                                    <span class="fileinput-exists">
            <?php echo $this->lang->line('edit'); ?> 
                                    </span>
                                    <input type="file" name="edit_item_image">
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
            <?php echo $this->lang->line('delete'); ?>  
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <button type="button" data-dismiss="modal" class="btn default pull-right" style="margin-left: 8px" id="cancle_item" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancle'); ?></button>
                <button type="button" id="final_submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?> </button>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
</div>
<div aria-hidden="true" role="dialog" tabindex="-1" id="categoryForm" class="modal fade">	
    <div class="modal-content">
        <div class="modal-body">
    <?php $this->load->view('category/add_category_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="specificationForm" class="modal fade">	
    <div class="modal-content">
        <div class="modal-body">
    <?php $this->load->view('spec_name/add_spec_name_form_only');?>
        </div>
    </div>
</div>


<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>



<script>
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();

    $(document).ready(function(){

        $("#suggesstion-box").html('');
        $("#search_key").keyup(function(){
            if ($(this).val() == '') {
                $("#suggesstion-box").html('');
            }
            else{
                $.ajax({
                    type: "POST",
                    url: "item/search",
                    data    : {
                        'keyword' : $(this).val(),
                        csrf_test_name: csrf
                    },

                    beforeSend: function(){
                        $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        var parsedData = JSON.parse(data);
                        //$("#suggesstion-box").html("");
                        var html_data = '';
                        html_data+= '<ul  role="menu" class="list-group">';                        
                        for (var i in parsedData) 
                        {
                            html_data+= '<a style="color:black;text-decoration:none;" href="#" onclick="set_value(\''+parsedData[i].items_name+'\');"><li class="list-group-item" data-original-index="'+i+'">'+parsedData[i].items_name+'</li></a><div style="height:3px;"></div>';
                        }
                        html_data+='</ul>';
                        $("#suggesstion-box").html(html_data);
                    }
                });
            }
        });
    });

    var csrf = $("input[name='csrf_test_name']").val();

    $("#category_select").keyup(function(event) 
    {
        $("#category_select_result").show();
        $("#category_select_result").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result").html(html);
            });
        }, 500);
    });

    $("#category_select_result").on('click', 'td', function(event) {
        $('input[name="category"]').val($(this).html());
        $('input[name="categories_id"]').val($(this).attr('data'));
        $("#category_select_result").hide();
    });



    var dropdownPosition;
    var rowSelected = 0;
    $("#spec_select_result").html('');
    $(".custom-tag-input").keyup(function(event) {
        clearTimeout(timer);
        event.preventDefault();

        if (event.keyCode == 27) {
            $(".custom-tag-input").val('');
            $('#spec_select_result').html('');
        }
        else if(event.keyCode == 13) {
            if ($('#spec_select_result tr').hasClass('row-selected')) {
                var specId = $('#spec_select_result tr.row-selected td').attr('data');
                var specName = $('#spec_select_result tr.row-selected td').text();
                var html='';
                html += '<tr><td class="selected-spec-name td-middle-center" data="'+specId+'">'+specName+'</td><td class="td-middle-center"> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td class="td-middle-center"> <button type="button" class="btn btn-default select-spec-value">OK</button> </td></tr>';    
                $("#spec_select_result").html(html);
                $(".input-spec-value").focus();
            } else{

            };
            return false;
        }
        else if (event.keyCode == 38) 
        { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr:first').hasClass('row-selected')) {
        } else{
            rowSelected--;
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
    }
    else if (event.keyCode == 40) 
    { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr').length == $('#spec_select_result tr.row-selected').index()+1) {

        } else{
            if (!!$('#spec_select_result tr.row-selected').length) {
                rowSelected++;
            };
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
        console.log(rowSelected);
    }
    else
    {
        dropdownPosition = $(".custom-tag-input").position();
        rowSelected = 0;
        $("#spec_select_result").css({
            top: (dropdownPosition.top + 35)+'px',
            left: dropdownPosition.left+'px',
            width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
        }).html('<tr><td class="text-center"><i class="fa fa-refresh fa-spin"></i></td></tr>');
        timer = setTimeout(searchSpec, 200);
    }
});

    var searchSpec = function() {
        var searchType = $("#inputType").val();
        var html = '';
        $.get('<?php echo site_url(); ?>/Item/search_spec_by_name/',{name: searchType}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                html+= '<tr><td class="spec-list" data="'+val.spec_name_id+'">'+val.spec_name+'</td></tr>';
            });
            $("#spec_select_result").css({
                top: (dropdownPosition.top + 35)+'px',
                left: dropdownPosition.left+'px',
                width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
            }).html(html);
        });
    };

    var spec_set_list = [];

    $("#spec_select_result").on('click', '.spec-list', function(event) {
        var html='';
        html += '<tr ><td class="selected-spec-name" data="'+$(this).attr('data')+'" style="padding-top: 15px;text-align: center;">'+$(this).text()+'</td><td> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td> <button type="button" class="btn btn-default select-spec-value">OK</button> </td></tr>';    
        $("#spec_select_result").html(html);
        $(".input-spec-value").focus();
    });

    $("#spec_select_result").on('keyup', '.input-spec-value', function(event) {
        event.preventDefault();
        if(event.keyCode == 13) {
            var html='';
            var spec_name = $("#spec_select_result td.selected-spec-name").text();
            var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
            var spec_name_value = $("#spec_select_result input").val();
            html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <a href="#" class="remove_tag"><i class="fa fa-close clickable"></i></a> </span>';    
            $(html).insertBefore(".custom-tag-input");
            $("#spec_select_result").html('');
            $(".custom-tag-input").val('').focus();
        }
        if (event.keyCode == 27) {
            $('#spec_select_result').html('');
            $(".custom-tag-input").focus();
            searchSpec();
        }
    });


    $("#spec_select_result").on('click', '.select-spec-value', function(event) {
    //sizeIds.push($(this).attr('data'));
    var html='';
    var spec_name = $("#spec_select_result td.selected-spec-name").text();
    var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
    var spec_name_value = $("#spec_select_result input").val();
    html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <i class="fa fa-close clickable remove_tag"></i> </span>';    

    $(html).insertBefore(".custom-tag-input");
    $("#spec_select_result").html('');
    $(".custom-tag-input").val('').focus();
    if (event.keyCode == 27) {
        alert('Esc key pressed.');
    }

});

    var spec_name_id = new Array();
    var spec_value = new Array();
    var img =1;
    $('.select-spec-combination').click(function() {
        var is_edit_form = $("#is_edit").val();

        if(is_edit_form==1)
        {
            if(spec_set_list.length>=1)
            {
                alert('Please add atmost one spec combination');
                $('.select-tag .spec-tags').remove();
                return false;
            }
        }

        var myElements = $(".select-tag .spec-tags");


        if(myElements.length==0)
        {
            alert('Please add specs first');return false;
        }

        var html = '<div class="form-group added" id="new_combinations_|'+spec_set_list.length+'"><label class="control-label col-md-3"><?php echo $this->lang->line('spec_combination'); ?></label><div class="col-md-9">';
        $(".select-tag .spec-tags").each(function(index, el) 
        {
            html += $(el)[0].outerHTML;
        });

        html += '</div></div>';

        if(is_edit_form==1)
        {

        }
        else
        {
            html+= '<div style="position:absolute;right:20px;margin-top:-45px;">';
        //html+= $("#item_image")[0].outerHTML;
        html += '</div></div>';
        html+= '<div style="position:absolute;right:80px;margin-top:-45px;">';
        html+= '<div id="image_of_user" class="form-group " style="">';
        //html+= '<label id="image_of_user_label" class="control-label col-md-3"> Product picture</label>';
        html+= '<div class="col-md-9">';
        html+=     '<div class="fileinput fileinput-new" data-provides="fileinput" >';
        html+=     '<div style="height:50px;width:60px;margin-top:-20px;" id="item_image" class="fileinput-preview thumbnail " name="item_image" data-trigger="fileinput">';
        html+=     '</div>';
        html+= '<div>';
        html+= '<span class="btn default btn-file">';
        html+= '<span class="fileinput-new" style="right:60px;"><?php echo $this->lang->line('product_pic'); ?></span>';
        html+= '<span class="fileinput-exists" style="width:50px">Edit</span>';
        html+= '<input type="file"  name="combination_images_'+img+'"></span>';
        html+= '<a style="padding:10px;" href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Delete </a>';
        html+= '</div></div></div></div>';
        html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    }

    html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    $(html).insertAfter(".select-tag");
    $('.select-tag .spec-tags').remove();

    var spec_set = [];
    var spec_values = '';
    var item_new_combination = '';
    var item_name = $('input[name="items_name"]').val();
    for (var i=0;i<myElements.length;i++) {
        spec_data = {};
        spec_data.id = (myElements.eq(i).attr("spec-id-data"));
        spec_data.value = (myElements.eq(i).attr("spec-value-data"));
        //spec_data.image = imageName;
        spec_set.push(spec_data);
        spec_values += '-';
        spec_values +=     (myElements.eq(i).attr("spec-value-data"));
    }
    
    spec_set_list.push(spec_set);
    img++;
    item_new_combination = item_name+'-'+spec_values;
    $("#item_new_name").html(item_new_combination);
    return false;
});

    $('#item_form').on('click', '.remove_tag', function () 
    {
        if($(this).closest('.form-group').hasClass('added')){    
            var set_id = $(this).closest('.form-group').attr('id').split('|')[1];
            var  se =  $(this).parent('.spec-tags');
            $.each(spec_set_list[set_id], function(index, val) {
                if(val.id == se.attr('spec-id-data'))
                {
                    spec_set_list[set_id].splice(index, 1);
                    return false;
                }
            });
        }
        $(this).parent('.spec-tags').remove();
    });

    function set_value(search_value)
    {
        $('#search_key').val(search_value);
        $("#suggesstion-box ul").html('');
    }



    $('#item_form').on('click', '#cancle_item', function(event) {
        event.preventDefault();

        // alert("test");
        // $('#item_form_modal').modal('hide');
        spec_set_list.length =0;
        $("div").remove(".added");
        $("div").remove(".testheightdiv");

        $("#item_form #image_of_user").remove();

        $('input[name="items_name"]').val('');
        $('input[name="category"]').val('');
        $('textarea[name="items_description"]').val('');

        $('#category_select').selectpicker('val','');
        $('#brand_select').selectpicker('val','');
        $('#unit_select').selectpicker('val','');
        $('#size_select').selectpicker('val','');
        $('#lot_size_select').selectpicker('val','');
        $('input[name="unique_id"]').val('');
                        //$('.fileinput-preview').html("<img src=''>");
                        //$('.fileinput-new').html("<img src=''>");
                        $(".spec-tags").remove();
                        $("#image_of_user").remove();

                    });

    

    $('#item_form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });


    $('#final_submit').click(function(event) {
        $('#item_form').submit();            
    });


    $('#item_form').ajaxForm({

        data: {
            specs_values: spec_set_list
        }, 
        beforeSubmit: function() {    
        },
        success: function(msg) {

            if(msg == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
                // if(msg == "success"){spec_set
                    //     $('#insert_success').slideDown();
                    //     setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
                    // }

                    else if(msg == "success")
                    {
                        $('#item_form_modal').modal('hide');
                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        $('#insert_success_item').slideDown();
                        setTimeout( function(){$('#insert_success_item').slideUp()}, 1500 );
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $("#item_form #image_of_user").remove();

                        $('input[name="items_name"]').val('');
                        $('input[name="category"]').val('');
                        $('textarea[name="items_description"]').val('');

                        $('#category_select').selectpicker('val','');

                        $('input[name="unique_id"]').val('');
                        //$('.fileinput-preview').html("<img src=''>");
                        //$('.fileinput-new').html("<img src=''>");
                        $(".spec-tags").remove();
                        $("#image_of_user").remove();


                    }
                    else if(msg =="failed")
                        {$('#item_form_modal').modal('hide');
                    $('#insert_failure').slideDown();
                    setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
                }

                if(msg == "Give a item name")
                {
                    alert("<?php echo $this->lang->line('alert_item_name_need'); ?>");
                    // $('#no_item').slideDown();
                    // setTimeout( function(){$('#no_item').slideUp()}, 1500 );
                }
                if(msg == "Choose item spec combination")
                {
                    alert("<?php echo $this->lang->line('alert_spec_name_need'); ?>");
                    // $('#no_spec').slideDown();
                    // setTimeout( function(){$('#no_spec').slideUp()}, 1500 );
                }

                if(msg == "Update Success")
                {
                        // $('#update_success').slideDown();
                        // setTimeout( function(){$('#update_success').slideUp()}, 1500 );
                        $('#update_success').slideDown();
                        setTimeout( function(){$('#update_success').slideUp()}, 1500 );
                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        $('#item_form').attr('action', "<?php echo site_url('/Item/save_item_info');?>");
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $('input[name="items_name"]').val('');
                        $("#is_edit").val('');
                        $('input[name="category"]').val('');
                        $('input[name="items_description"]').val('');
                        $('#category_select').selectpicker('val','');
                        $('#brand_select').selectpicker('val','');
                        $('#unit_select').selectpicker('val','');
                        $('#size_select').selectpicker('val','');
                        $('#lot_size_select').selectpicker('val','');
                        $('input[name="unique_id"]').val('');
                        //$('.fileinput-preview').html("<img src=''>");
                        //$('.fileinput-new').html("<img src=''>");
                        $(".spec-tags").remove();
                        $("#edit_image_of_user").hide();
                        table_item.ajax.reload();
                        return false;
                    }
                    // if(msg == "Give a item category")
                    // {
                    //     $('#category_add').slideDown();
                    //     setTimeout( function(){$('#category_add').slideUp()}, 1500 );
                    // }
                    if(msg == "Please choose atmost one combination")
                    {
                        $('#one_combination').slideDown();
                        setTimeout( function(){$('#one_combination').slideUp()}, 1500 );
                    }
                },
                complete: function(xhr) {

                    table_item.ajax.reload();
                    return false;
                }
            }); 

    $('#category_form').submit(function(event) {
        event.preventDefault();
        var data = {};
        data.categories_name = $('input[name=categories_name]').val();
        data.categories_description = $('input[name=categories_description]').val();
        data.parent_categories_id = $('select[name=parent_categories_id]').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/category/save_category_info',data).done(function(data1, textStatus, xhr) {

            var data2 = $.parseJSON(data1);

            if(data2.success==0)
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }
            else{
                $('.form-group.has-errorf').removeClass('has-error');
                $('.help-block').hide();
                $('#categoryForm').modal('hide');
                $('input[name= categories_name]').val('');
                $('input[name= categories_description]').val('');
                $('select[name= parent_categories_id]').val('');
                $('#insert_success_category').slideDown();
                setTimeout( function()
                    {$('#insert_success_category').slideUp();}, 3000 );
                

            }

        }).error(function() {

            $('#insert_failure_category').slideDown();
            setTimeout( function(){$('#insert_failure_category').slideUp()}, 3000 );
        });    
    });

    // ajax form submit starts....

    $('#spec_name_form').submit(function(event) {
        event.preventDefault();
        var data = {};
        data.spec_name = $('input[name=spec_name]').val();
        data.spec_description = $('input[name=spec_description]').val();
        data.spec_default_status = $('input[name=spec_default_status]:checked').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/spec_name/save_spec_name_info',data).done(function(data1, textStatus, xhr) {
            var data2 = $.parseJSON(data1);
            if(data2 =="No Permisssion"){
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
            else if(data2.success==0)
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                    $('.help-block').show();
                });
            }

            else
            {
                $('.form-group.has-error').removeClass('has-error');
                $('.help-block').hide();
                $('#specificationForm').modal('hide');
                $('input[name= spec_name]').val('');
                $('input[name= spec_description]').val('');
                $('#insert_success_spec').slideDown();
                setTimeout( function(){$('#insert_success_spec').slideUp();}, 1500 );

            }

        }).error(function() {
            $('#specificationForm').modal('hide');
            $('#insert_failure_spec').slideDown();
            setTimeout( function(){$('#insert_failure_spec').slideUp()}, 1500 );
        });    
    });

// ajax form submit end.......



</script>