<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- date picker css starts-->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- date picker css ends -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/custom/css/buttons.dataTables.min.css">
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->

<style type="text/css">
    .custom-tag-input:focus{
        outline:0px !important;
        -webkit-appearance:none;
        -moz-appearance:none;
    }
    div + img {
        background-color: yellow;
        height:50px;
    }
    .row-selected{
        background-color: #bbb;
    }
    .td-middle-center{
        vertical-align: middle !important;
        text-align: center !important;
    }
</style>

<!-- END PAGE LEVEL STYLES -->
<div class="row">
    <div class="alert alert-danger" id="barcode_invalid" style="display:none" role="alert"><?php echo $this->lang->line('barcode_invalid'); ?></span></div>
    <div class="alert alert-danger" id="barcode_exist" style="display:none" role="alert"><?php echo $this->lang->line('barcode_exist'); ?></span></div>
    <div class="alert alert-danger" id="inventory_exist" style="display:none" role="alert"><?php echo $this->lang->line('inventory_exist'); ?></span></div>
    <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>
    <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('product_delete_success'); ?></div>
    <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_failed'); ?></div>
    <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_succeded'); ?></div>
    <div class="alert alert-success" id="insert_success_category" style="display:none" role="alert"><?php echo $this->lang->line('category_insert_success'); ?></div>
    <div class="alert alert-success" id="insert_success_spec_name" style="display:none" role="alert"><?php echo $this->lang->line('spec_insert_success'); ?></div>
    <div class="alert alert-success" id="insert_success_unit" style="display:none" role="alert"><?php echo $this->lang->line('unit_insert_success'); ?></div>
    <div class="alert alert-success" id="insert_success_size" style="display:none" role="alert"><?php echo $this->lang->line('size_insert_success'); ?></div>
    <div class="alert alert-success" id="insert_success_color" style="display:none" role="alert"><?php echo $this->lang->line('color_insert_success'); ?></div>
    <div class="alert alert-success" id="insert_success_lotsize" style="display:none" role="alert"><?php echo $this->lang->line('lot_insert_success'); ?></div>
    <div class="alert alert-danger" id="no_spec" style="display:none" role="alert"><?php echo $this->lang->line('choose_spec'); ?></div>
    <!-- <div class="alert alert-danger" id="category_add" style="display:none" role="alert">Please choose a category</div> -->
    <div class="alert alert-danger" id="one_combination" style="display:none" role="alert"><?php echo $this->lang->line('max_spec'); ?></div>

    <div class="alert alert-danger" id="no_item" style="display:none" role="alert"><?php echo $this->lang->line('item_name_required'); ?></div>
    
    <div class="col-md-6">
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('product_update_succeded'); ?></div>
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-pencil-square-o"></i><span id="header_text"><?php echo $this->lang->line('left_portlet_title_product'); ?></span>
                </div>
            </div>

            <input name="item_new_name" type="hidden" id="item_new_name">
            <div class="portlet-body form">
                <?php $form_attrib = array('id' => 'item_form','class' => 'form-horizontal form-bordered');
                echo form_open_multipart('Item/save_item_info',  $form_attrib);?>
                <div class="form-body">


                    <div class="form-group <?php if(isset($error['items_name'])) { echo 'has-error'; 
                } ?> " id="itemAdd">
                <label class="control-label col-md-3">
                    <?php echo $this->lang->line('product_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                </label>
                <div class="col-md-9 input-form input-icon">
                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_name_lang'); ?>"></i>
                    <input autocomplete="off" type="text" id="search_key" class="form-control" name="items_name" placeholder="<?php echo $this->lang->line('product_name'); ?>" value="<?php if(isset($previous_data['items_name'])) { echo $previous_data['items_name']; 
                } ?>" >
                <!-- <input type="file" name="upload_pictures">  -->
                <div id="suggesstion-box"></div>
                <span class="help-block"><?php if(isset($error['items_name'])) { echo $error['items_name']; 
                } ?></span>
            </div>
        </div>

        <div class="form-group <?php if(isset($error['items_name'])) { echo 'has-error'; 
    } ?> " id="itemDelete" style="display:none">

    <label class="control-label col-md-3">
        <?php echo $this->lang->line('product_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
    </label>
    <div class="col-md-9 input-form input-icon">
        <input name="items_name_delete" class="form-control" readonly="true" autocomplete="off" type="text"  value="<?php if(isset($previous_data['items_name'])) { echo $previous_data['items_name']; 
    } ?>" >
    <!-- <input type="file" name="upload_pictures">  -->
    <div id="suggesstion-box"></div>
    <span class="help-block"><?php if(isset($error['items_name'])) { echo $error['items_name']; 
    } ?></span>
</div>
</div>



<style type="text/css">.spec-tags{margin-top: 13%}</style>

<input type="hidden" name="is_edit" id="is_edit">

<div class="form-group select-tag" id="specAdd">
 <div class="form-group " id="specAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('select_specification'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
    <div class="col-md-9 input-form input-icon">
        <div class="row">
            <div class="col-md-1">

            </div>
            <div class="col-md-2">
                <a href="#specificationForm" title="<?php echo $this->lang->line('add_spec'); ?>" data-toggle="modal" class="btn btn-icon-only btn-circle green"><i class="fa fa-plus"></i></a>
            </div>
            <div class="col-md-3">
                <a href="#specificationList" title="Specification List" data-toggle="modal" class="btn btn-icon-only btn-circle green specificationList"><i class="fa fa-list"></i></a>
            </div>
            <div class="col-md-4">
                <a class="btn btn-success select-spec-combination" > 
                    <?php echo $this->lang->line('ok'); ?>
                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_ok'); ?>"></i> 
                </a>
            </div>
        </div>
        <div style="overflow: hidden; padding: 1px 5px;" id="spec-tag-field">
            <!-- <div style="height:50px;"></div> -->
            <div class="col-md-12 spec-box" style="padding: 10px 0px;">
            </div>
            <hr />
            <div class="col-md-12" style="padding: 10px 0px;">
                <div class="col-md-12">
                    <input type="text" id="inputType" class="custom-tag-input" placeholder="Search Specifications" style="float: left;height: 36px;width:100%; border: 1px solid rgb(226, 226, 226);
                    background:#FFFFFF; padding-left: 25px;float:left;margin-left:0px;"; autocomplete="off">
                </div>
                <div class="col-md-12">
                    <table class="table table-condensed table-hover table-bordered clickable" id="spec_select_result" style="z-index: 10;background-color: #fff;">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>  
</div>

<div class="form-group" id="specDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('specifications'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
    <div class="col-md-9 input-form input-icon">
        <div style="overflow: hidden; padding: 1px 5px;" id="spec-tag-field-delete">

        </div>
    </div>
</div>

<div class="form-group" style="display: none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('custom_barcode'); ?></label>
    <div class="col-md-9 input-form input-icon">
        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="Give barcode of your item if you want"></i>
        <input autocomplete="off" type="text"  class="form-control" name="barcode_number" placeholder="<?php echo $this->lang->line('custom_barcode_placeholder'); ?>" value="">
        <span class="help-block"></span>
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_warranty'); ?></label>
    <div class="col-md-9 input-form">
        <input autocomplete="off" type="text"  class="form-control" name="product_warranty" placeholder="<?php echo $this->lang->line('product_warranty_placeholder'); ?>" value="">
        <span class="help-block"></span>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-6"><b>Want to add IMEI/Serial number during buy ? </b></label>
    <div class="col-md-3 input-form input-icon">
        <input type="checkbox" style="width: 20px;height: 20px;" id="unique_barcode" name="unique_barcode">
    </div>
</div>

<input type="hidden" name="is_unique" id="is_unique">

<!-- <div class="testheightdiv" style="height:30px;"></div> -->
<div class="form-group" id="categoryAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('select_category'); ?> 
    </label>
    <div class="col-md-6">
        <input type="text" autocomplete="off" name="category" id="category_select" class="form-control" placeholder="<?php echo $this->lang->line('select_category'); ?> ">
        <input type="hidden" autocomplete="off" name="categories_id" id="category_select" class="form-control">
        <table class="table table-condensed table-hover table-bordered clickable" id="category_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
        </table>
    </div>
    <div class="col-sm-1">
        <a href="#categoryForm" title="<?php echo $this->lang->line('category_title'); ?> " data-toggle="modal" class="btn btn-icon-only btn-circle green"><i class="fa fa-plus"></i></a>
    </div>
</div>
<div class="form-group" id="categoryDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('category'); ?> 
    </label>
    <div class="col-md-4">
        <input type="text" disabled="true" autocomplete="off" name="category_name_delete" id="category_select" class="form-control">
        <input type="hidden" autocomplete="off" name="categories_id_delete" id="category_select" class="form-control">
    </div>
</div>

<div class="form-group" id="descriptionAdd">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_description'); ?> </label>
    <div class="col-md-9">
        <textarea  class="form-control" name="items_description" placeholder="<?php echo $this->lang->line('product_description'); ?>"></textarea>
    </div>
</div>
<div class="form-group" id="descriptionDelete" style="display:none">
    <label class="control-label col-md-3"><?php echo $this->lang->line('product_description'); ?> </label>
    <div class="col-md-9">
        <textarea readonly="true" style="height:100px;" class="form-control" name="items_description_delete"></textarea>
    </div>
</div>

<div class="form-actions">
    <button type="button" data-dismiss="modal" class="btn default pull-right" style="margin-left: 8px" id="cancle_item" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancle'); ?></button>
    <button type="button" id="final_submit" class="btn green pull-right"><?php echo $this->lang->line('save'); ?> </button>
</div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>

<div class="col-md-6">
    <div class="row">
        <div class="col-md-12">        
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-pencil-square-o"></i><?php echo $this->lang->line('left_portlet_title_buy'); ?>
                    </div>
                </div>
                <div class="portlet-body form">
                    <?php 
                    $form_attrib = array('class' => 'form-horizontal form-bordered', 'autocomplete'=>'off');
                    echo form_open('',  $form_attrib);
                    ?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('right_voucher_no'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                            <div class="col-md-9 input-icon input-form">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('voucher_tt'); ?>"></i> 
                                <input type="text" class="form-control" name="voucher_no" id="voucher_no_id" placeholder="<?php echo $this->lang->line('right_voucher_no_placeholder'); ?>">
                                <span class="help-block" id="voucher_help_block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?php echo $this->lang->line('left_item_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                            </label>
                            <div class="col-md-9 input-form input-icon">
                                <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_select_tt'); ?>"></i> 
                                <input type="text" name="items_name" placeholder="<?php echo $this->lang->line('left_item_name_placeholder'); ?>" id="item_select" class="form-control">
                                <input type="hidden" name="items_id"  class="form-control">
                                <table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                </table>
                                <span class="help-block" style="display:none"><?php echo $this->lang->line('left_item_name_placeholder'); ?></span>
                                <input type="hidden" name="is_unique_barcode" id="is_unique_barcode">
                            </div>
                        </div>
                        <br/>
                        <div class="form-group">
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('left_quantity'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                            </label>
                            <div class="col-md-9">
                                <div class="input-icon input-group ">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('quantity_tt'); ?>"></i>
                                    <input type="number" min="1" id="numpadButton1"  name="quantity" class="form-control" placeholder="<?php echo $this->lang->line('left_quantity'); ?>" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="numpadButton-btn1" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                    </span>
                                </div>
                                <span class="help-block" style="display:none"><?php echo $this->lang->line('left_quantity_err_msg');?></span>
                            </div>
                        </div>
                        <div class="form-group" id="unique_barcode_div">

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('left_buying_price'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
                            </label>
                            <div class="col-md-9">
                                <div class="input-group input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('pp_tt'); ?>"></i>
                                    <input type="number" step="any" min="0"  name="buying_price" id="numpadButton2" class="form-control" placeholder="<?php echo $this->lang->line('left_buying_price_placeholder'); ?>" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="numpadButton-btn2" type="button"><i class="glyphicon glyphicon-th numpadButton-btn"></i></button>
                                    </span>
                                </div>
                                <span class="help-block" style="display:none"><?php echo $this->lang->line('left_buying_price_err_msg'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('left_selling_item_retail'); ?></label>
                            <div class="col-md-9">

                                <div class="input-group input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('sp_tt'); ?>"></i>
                                    <input type="number" min="0" step="any" min="0"  name="retail_price" id="numpadButton3" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_item_retail_placeholder'); ?>" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="numpadButton-btn3" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"> <?php echo $this->lang->line('left_selling_item_wholesale'); ?></label>
                            <div class="col-md-9">
                                <div class="input-group input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('wp_tt'); ?>"></i>
                                    <input type="number" step="any" min="0"  name="whole_sale_price" id="numpadButton4" class="form-control" placeholder="<?php echo $this->lang->line('left_selling_item_wholesale_placeholder'); ?>" aria-describedby="numpadButton-btn">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="numpadButton-btn4" type="button"><i class="glyphicon glyphicon-th"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <div class="panel-group accordion" id="accordion3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                                <?php echo $this->lang->line('left_extra_fields'); ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div id="extra_fields" >
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_discount_type'); ?>
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select class="form-control" id="dis_type" name="discount_type">
                                                            <option value=""><?php echo $this->lang->line('select_one'); ?></option>
                                                            <option value="amount"><?php echo $this->lang->line('left_discount_type_taka'); ?></option>
                                                            <option value="percentage"><?php echo $this->lang->line('left_discount_type_percentage'); ?></option>
                                                            <option value="free_quantity"><?php echo $this->lang->line('left_discount_type_quantity'); ?></option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 input-form input-icon">
                                                        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('dt_tt'); ?>"></i> 
                                                        <input type="number" min="0" step="any" name="discount_amount" class="form-control" placeholder="<?php echo $this->lang->line('left_discount_type_placeholder'); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?php echo $this->lang->line('left_comments'); ?></label>
                                                    <div class="col-md-9 input-icon input-form">
                                                        <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('comm_tt'); ?>"></i> 
                                                        <input type="text" class="form-control" name="comments"  placeholder="<?php echo $this->lang->line('left_comments_placeholder'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                    <div class="form-actions">
                        <button id="cancle_item" type="button" class="btn  default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('left_cancle_button'); ?></button>    
                        <button id="add_item" type="button" class="btn green pull-right"><?php echo $this->lang->line('left_save_button'); ?></button>
                        <button id="update_item" style="display:none" type="button" class="btn yellow pull-right"><?php echo $this->lang->line('left_edit_save_button'); ?></button>
                        <!-- <button type="button" class="btn btn-primary" id="tutorial_start">Tutorial</button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="alert alert-danger" id="item_quantity_invalid" style="display:none" role="alert"><?php echo $this->lang->line('item_quantity_invalid');?></div>
            <div class="alert alert-danger" id="buying_price_empty" style="display:none" role="alert"><?php echo $this->lang->line('buying_price_empty');?></div>
            <div class="alert alert-danger" id="item_not_selected" style="display:none" role="alert"><?php echo $this->lang->line('item_not_selected');?></div>
            <div class="alert alert-danger" id="voucher_no_empty" style="display:none" role="alert"><?php echo $this->lang->line('voucher_no_empty');?></div>
            <div class="alert alert-danger" id="dis_amount_invalid" style="display:none" role="alert"><?php echo $this->lang->line('dis_amount_invalid');?></div>
            <div class="alert alert-danger" id="landed_cost_invalid" style="display:none" role="alert"><?php echo $this->lang->line('landed_cost_invalid');?></div>
            <div class="alert alert-danger" id="paid_amount_invalid" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_invalid');?></div>
            <div class="alert alert-danger" id="paid_amount_empty" style="display:none" role="alert"><?php echo $this->lang->line('paid_amount_empty');?></div>
            <div class="alert alert-success" id="user_insert_success" style="display:none" role="alert"><?php echo $this->lang->line('user_insert_success'); ?></div>
            <div class="alert alert-success" id="buy_success" style="display:none" role="alert"><?php echo $this->lang->line('alert_sus_msg_of_item_add');?></div>
            <div class="alert alert-success" id="insert_success_item" style="display:none" role="alert"><?php echo $this->lang->line('product_insert_succeded'); ?></div>
            <div class="alert alert-danger" id="for_discount_calculation_error" style="display:none" role="alert"><?php echo $this->lang->line('for_discount_calculation_error'); ?></div>
            <div class="alert alert-danger" id="for_no_discount_calculation_error" style="display:none" role="alert"><?php echo $this->lang->line('for_no_discount_calculation_error'); ?></div>
            <div class="alert alert-danger" id="select_vendor_for_due_transaction" style="display:none" role="alert"><?php echo $this->lang->line('select_vendor_for_due_transaction'); ?></div>
            <div class="alert alert-danger" id="voucher_exist" style="display:none" role="alert"><?php echo $this->lang->line('voucher_exist'); ?></div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title_buy'); ?>
                    </div>
                </div>
                <div class="portlet-body form" id="basic_buy_info">
                    <form class="form-horizontal" >
                        <div class="form-body ">
                            <div class="form-group">
                                <label class="control-label col-md-3"> <?php echo $this->lang->line('right_select_vendor'); ?> 
                                </label>
                                <div class="col-md-6 input-form input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('vendor_tt'); ?> "></i> 
                                    <input type="text" autocomplete="off" name="vendors_name" placeholder="<?php echo $this->lang->line('right_select_vendor_placeholder'); ?> " id="vendor_select" class="form-control">
                                    <span class="help-block" id="vendor_help_block" ></span>
                                    <input type="hidden" name="vendors_id"  class="form-control">
                                    <table class="table table-condensed table-hover table-bordered clickable" id="vendor_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
                                    </table>
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <a href="#vendor_form_modal" title="<?php echo $this->lang->line('right_vendor_title'); ?>" class="btn btn-icon-only btn-circle green" data-toggle="modal">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"> <?php echo $this->lang->line('right_landed_cost'); ?></label>
                                <div class="col-md-9 input-form input-icon">
                                    <i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('landed_tt'); ?>"></i> 
                                    <input type="number" min="0" step="any" class="form-control"  name="landed_cost" id="landed_cost_id" placeholder="<?php echo $this->lang->line('right_landed_cost_placeholder'); ?>">
                                    <span class="help-block" id="landed_help_block" ></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('right_cash_type'); ?></label>
                                <div class="col-md-9 radio-list">
                                    <label class="radio-inline">
                                        <input type="radio" name="payment_type" id="cash_id" value="cash" checked><?php echo $this->lang->line('right_type_cash'); ?>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="payment_type" id="chk_id" value="cheque"><?php echo $this->lang->line('right_type_cheque'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" style="display: none" id="ac_select_div">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('select_acc'); ?><sup><i class="fa fa-star custom-required"></i></sup>
                                </label>
                                <div class="col-md-9">
                                    <input type="text" autocomplete="off" name="bank_acc_num" placeholder="<?php echo $this->lang->line('bank_acc_not_select'); ?>" id="bank_acc_select" class="form-control">
                                    <input type="hidden" autocomplete="off" name="bank_acc_id"  class="form-control">
                                    <span class="help-block" id="bank_acc_help_block" ></span>
                                    <table class="table table-condensed table-hover table-bordered clickable" id="bank_acc_select_result" style="width:95%;position: absolute;z-index: 10;background-color: #fff;">
                                    </table>
                                </div>
                            </div>
                            <div class="form-group" style=" display:none" id="chk_page_div">
                                <label class="control-label col-md-3"> <?php echo $this->lang->line('enter_chk_num'); ?><sup><i class="fa fa-star custom-required"></i></sup></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="cheque_page_num" id="chk_page" placeholder="<?php echo $this->lang->line('cheque_page_empty'); ?>">
                                    <span class="help-block" id="cheque_help_block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo $this->lang->line('left_date');?> </label>
                                <div class="col-md-3">
                                    <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="purchase_date" placeholder="<?php echo $this->lang->line('left_date');?>" />
                                    <span class="help-block"></span>
                                </div>
                            </div> 

                            <table id="buy_table" class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->lang->line('right_item_name'); ?></th>
                                        <th><?php echo $this->lang->line('right_quantity'); ?></th>
                                        <th><?php echo $this->lang->line('right_buying_price'); ?></th>
                                        <th><?php echo $this->lang->line('right_sub_total'); ?></th>
                                        <th><?php echo $this->lang->line('right_discount_type'); ?></th>
                                        <th><?php echo $this->lang->line('right_delete_edit'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $this->lang->line('right_grand_total'); ?>:</th>
                                        <th id="total_amount"></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $this->lang->line('right_discount'); ?>:</th>
                                        <th ><div class="input-form input-icon"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('discount_tt'); ?>"></i> <input class='form-control' type="number" min="0" step="any" id="total_discount" name="discount"  placeholder="<?php echo $this->lang->line('right_discount');?>"></div>
                                            <span class="help-block" id="total_discount_help_block" ></span>
                                        </th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $this->lang->line('right_net_payable');?> :</th>
                                        <th id="total_net_payable"></th>
                                        <th></th>
                                        <th></th>

                                    </tr>    
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $this->lang->line('right_paid'); ?> <sup><i class="fa fa-star custom-required"></i></sup>:</th>
                                        <th ><div class="input-icon input-form"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('paid_tt'); ?>"></i><input class='form-control' type="number" min="0" step="any" id="total_paid" name="paid" placeholder="<?php echo $this->lang->line('right_paid');?>"></div>
                                            <span class="help-block" id="total_paid_help_block" ></span>
                                        </th>
                                        <th></th>
                                        <th></th>

                                    </tr>    
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><?php echo $this->lang->line('right_due'); ?>:</th>
                                        <th id="total_due"></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><button type="submit" id="save_buy" class="btn green"><?php echo $this->lang->line('right_save');?></button></th>
                                    </tr>

                                </tfoot>
                            </table>
                        </div>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<!-- Customer Modal Form -->
<div aria-hidden="true" role="dialog" tabindex="-1" id="vendor_form_modal" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('vendor/add_vendor_form_only');?>
        </div>
    </div>
</div>
<!-- Customer Modal Form End -->

<div aria-hidden="true" role="dialog" tabindex="-1" id="barcode_quantity_form" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('barcode/add_barcode_quantity_form');?>
        </div>
    </div>
</div>


<div aria-hidden="true" role="dialog" tabindex="-1" id="categoryForm" class="modal fade">   
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('category/add_category_form_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="specificationForm" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item/spec_name_only');?>
        </div>
    </div>
</div>

<div aria-hidden="true" role="dialog" tabindex="-1" id="specificationList" class="modal fade">  
    <div class="modal-content">
        <div class="modal-body">
            <?php $this->load->view('item/spec_name_list');?>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #specificationList
    {
        width: 36%;
    }
</style>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
</div>
</div>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!-- date picker js starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- date picker js ends -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- script for datatable export buttoon start -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/buttons.html5.min.js"></script>
<!-- script for datatable export buttoon end -->
<!-- Script for print button -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/buttons.print.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-tags/bootstrap-tagsinput.css">
<!-- Boostrap modal end -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->   
<script>
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();
    $('input[name = unique_barcode]').val("off");
    //$('input[name = unique_barcode]').parents('div').removeClass('checker');
    $("#specDelete").hide();
    $("#categoryDelete").hide();
    $("#edit_image_of_user_delete").hide();
    $("#itemDelete").hide();
    $("#descriptionDelete").hide();

    $("#specAdd").show();
    $("#categoryAdd").show();
    $("#itemAdd").show();
    $("#descriptionAdd").show();
    $("#actionAdd").show();




    $(document).ready(function(){

        $("#suggesstion-box").html('');
        $("#search_key").keyup(function(){
            if ($(this).val() == '') {
                $("#suggesstion-box").html('');
            }
            else{
                $.ajax({
                    type: "POST",
                    url: "item/search",
                    data    : {
                        'keyword' : $(this).val(),
                        csrf_test_name: csrf
                    },

                    beforeSend: function(){
                        $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        var parsedData = JSON.parse(data);
                        //$("#suggesstion-box").html("");
                        var html_data = '';
                        html_data+= '<ul  role="menu" class="list-group">';                        
                        for (var i in parsedData) 
                        {
                            html_data+= '<a style="color:black;text-decoration:none;" href="#" onclick="set_value(\''+parsedData[i].items_name+'\');"><li class="list-group-item" data-original-index="'+i+'">'+parsedData[i].items_name+'</li></a><div style="height:3px;"></div>';
                        }
                        html_data+='</ul>';
                        $("#suggesstion-box").html(html_data);
                    }
                });
            }
        });
    });

    function set_value(search_value)
    {
        $('#search_key').val(search_value);
        $("#suggesstion-box").html('');
        $("#suggesstion-box ul").html('');
    }


    var csrf = $("input[name='csrf_test_name']").val();

    $("#category_select").keyup(function(event) 
    {
        $("#category_select_result").show();
        $("#category_select_result").html('');

        clearTimeout(timer);
        timer = setTimeout(function() 
        {
            var seachCategory = $("#category_select").val();
            var html = '';
            $.post('<?php echo site_url(); ?>/Category/search_category_by_name',{q: seachCategory,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                console.log(data);
                data = JSON.parse(data);
                $.each(data, function(index, val) {
                    html+= '<tr><td data="'+val.categories_id+'">'+val.categories_name+'</td></tr>';
                });
                $("#category_select_result").html(html);
            });
        }, 500);
    });

    $("#category_select_result").on('click', 'td', function(event) 
    {
        $('input[name="category"]').val($(this).html());
        $('input[name="categories_id"]').val($(this).attr('data'));
        $("#category_select_result").hide();
    });


    var dropdownPosition;
    var rowSelected = 0;
    $("#spec_select_result").html('');
    $(".custom-tag-input").keyup(function(event) {
        clearTimeout(timer);
        event.preventDefault();

        if (event.keyCode == 27) {
            $(".custom-tag-input").val('');
            $('#spec_select_result').html('');
        }
        else if(event.keyCode == 13) {
            if ($('#spec_select_result tr').hasClass('row-selected')) {
                var specId = $('#spec_select_result tr.row-selected td').attr('data');
                var specName = $('#spec_select_result tr.row-selected td').text();
                $("#inputType").val(specName);

                var html='';
                html += '<tr><td class="selected-spec-name td-middle-center" data="'+specId+'">'+specName+'</td><td class="td-middle-center"> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td class="td-middle-center"> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button> </td></tr>';    
                $("#spec_select_result").html(html);
                $(".input-spec-value").focus();

            } 
            else
            {

            };
            return false;
        }
        else if (event.keyCode == 38) 
        { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr:first').hasClass('row-selected')) {
        } else{
            rowSelected--;
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
    }
    else if (event.keyCode == 40) 
    { 
        //console.log(rowSelected);
        if ($('#spec_select_result tr').length == $('#spec_select_result tr.row-selected').index()+1) {

        } else{
            if (!!$('#spec_select_result tr.row-selected').length) {
                rowSelected++;
            };
        };
        $('#spec_select_result tr').removeClass('row-selected').eq(rowSelected).addClass('row-selected');
        console.log(rowSelected);
    }
    else
    {
        dropdownPosition = $(".custom-tag-input").position();
        rowSelected = 0;
        $("#spec_select_result").css({
            top: (dropdownPosition.top + 35)+'px',
            left: dropdownPosition.left+'px',
            width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
        }).html('<tr><td class="text-center"><i class="fa fa-refresh fa-spin"></i></td></tr>');
        timer = setTimeout(searchSpec, 200);
    }
});

    var searchSpec = function() {
        var searchType = $("#inputType").val();
        $("#spec_select_result").show();

        var html = '';

        if(searchType=='')
        {
            $("#spec_select_result").hide();
        }

        $.get('<?php echo site_url(); ?>/Item/search_spec_by_name/',{name: searchType}, function(data, textStatus, xhr) {
            data = JSON.parse(data);
            $.each(data, function(index, val) {
                html+= '<tr><td class="spec-list" data="'+val.spec_name_id+'">'+val.spec_name+'</td></tr>';
            });
            $("#spec_select_result").css({
                top: (dropdownPosition.top + 35)+'px',
                left: dropdownPosition.left+'px',
                width: ($(".custom-tag-input").closest('div').width()-(dropdownPosition.left-23))+'px'
            }).html(html);
        });
    };

    var spec_set_list = [];

    $('#spec_select_result_list').DataTable();

    $("#spec_select_result_list").on('click', '.spec-list', function(event) {
        $("#inputType").val($(this).text());
        var html='';
        html += '<tr ><td class="selected-spec-name" data="'+$(this).attr('data')+'" style="padding-top: 15px;text-align: center;">'+$(this).text()+'</td><td> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button></td></tr>';    
        $("#spec_select_result").html(html);
        $('#specificationList').modal('hide');
        $(".input-spec-value").focus();
    });



    $("#spec_select_result").on('click', '.spec-list', function(event) {
        $("#inputType").val($(this).text());
        var html='';
        html += '<tr ><td class="selected-spec-name" data="'+$(this).attr('data')+'" style="padding-top: 15px;text-align: center;">'+$(this).text()+'</td><td> <input type="text" name="" id="valueSearch" class="form-control input-spec-value" placeholder="value"> </td><td> <button type="button" class="btn btn-default select-spec-value"><i class="fa fa-question-circle tooltips font-green" data-container="body" data-placement="top" data-original-title="<?php echo $this->lang->line('item_set_value'); ?>"></i>&nbsp;Set Value</button></td></tr>';    
        $("#spec_select_result").html(html);
        $(".input-spec-value").focus();
    });



    $("#spec_select_result").on('keyup', '.input-spec-value', function(event) {
        event.preventDefault();
        if(event.keyCode == 13) {
            var html='';
            var spec_name = $("#spec_select_result td.selected-spec-name").text();
            var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
            var spec_name_value = $("#spec_select_result input").val();
        //alert(spec_name_value.toUpperCase());
        var myElements = $(".select-tag .spec-tags");
        if(myElements.length>0)
        {
            for (var i=0;i<myElements.length;i++) 
            {
                spec_data = {};
                spec_data.id = (myElements.eq(i).attr("spec-id-data"));
                spec_data.value = (myElements.eq(i).attr("spec-value-data"));
                if((spec_name_id == spec_data.id) || (spec_name_value.toUpperCase() == spec_data.value.toUpperCase()))
                {
                    alert('<?php echo $this->lang->line('exist_spec'); ?>');
                    $("#spec_select_result").html('');
                    $(".custom-tag-input").val('').focus();
                    $("#itemTooltip").hide();
                    $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
                    return false;
                }
            }
        }

        html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <a href="#" style="color:#cb5a5e" class="remove_tag"><i class="fa fa-close clickable"></i></a> </span>';    
        $(html).appendTo('.spec-box');
        $("#spec_select_result").html('');
        $("#itemTooltip").hide();
        $(".custom-tag-input").val('').focus();
    }
    if (event.keyCode == 27) {
        $('#spec_select_result').html('');
        $(".custom-tag-input").focus();
        searchSpec();
    }
});


    $("#spec_select_result").on('click', '.select-spec-value', function(event) 
    {
    //sizeIds.push($(this).attr('data'));
    var html='';
    var spec_name = $("#spec_select_result td.selected-spec-name").text();
    var spec_name_id = $("#spec_select_result td.selected-spec-name").attr('data');
    var spec_name_value = $("#spec_select_result input").val();

    var myElements = $(".select-tag .spec-tags");
    if(myElements.length>0)
    {
        for (var i=0;i<myElements.length;i++) 
        {
            spec_data = {};
            spec_data.id = (myElements.eq(i).attr("spec-id-data"));
            spec_data.value = (myElements.eq(i).attr("spec-value-data"));
            if((spec_name_id == spec_data.id) || (spec_name_value.toUpperCase() == spec_data.value.toUpperCase()))
            {
                alert('<?php echo $this->lang->line('exist_spec'); ?>');
                $("#spec_select_result").html('');
                $(".custom-tag-input").val('').focus();
                $("#itemTooltip").hide();
                $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
                return false;
            }
        }
    }

    html += '<span class="label label-success spec-tags" style="float: left;top: 24px; margin-top: 9px;margin-right:5px;" spec-id-data="'+spec_name_id+'" spec-value-data="'+spec_name_value+'">'+spec_name+'-'+spec_name_value+' <i class="fa fa-close clickable remove_tag"></i> </span>';    
    $(html).appendTo('.spec-box');
    $("#spec_select_result").html('');
    $(".custom-tag-input").val('').focus();
    $("#itemTooltip").hide();
    $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
    if (event.keyCode == 27)
    {
        alert('Esc key pressed.');
    }

});

    var spec_name_id = new Array();
    var spec_value = new Array();
    var img =1;
    $('.select-spec-combination').click(function() {
        var is_edit_form = $("#is_edit").val();
        if(is_edit_form==1)
        {
            $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
            if(spec_set_list.length>=1)
            {
                alert('<?php echo $this->lang->line('max_spec'); ?>');
                $('.select-tag .spec-tags').remove();
                return false;
            }
        }

        var myElements = $(".select-tag .spec-tags");

        $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
        if(myElements.length==0)
        {
            $("#item_form .specificationList").css('border', '1px solid red');
            alert('<?php echo $this->lang->line('select_spec_first'); ?>');return false;
        }
        $("#item_form .specificationList").css('border', '1px solid rgb(226, 226, 226)');
        $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
        var html = '<div class="form-group added" id="new_combinations_|'+spec_set_list.length+'"><label class="control-label col-md-3"><?php echo $this->lang->line('spec_combination'); ?></label><div style="max-width:55%" class="col-md-9">';
        $(".select-tag .spec-tags").each(function(index, el) 
        {
            html += $(el)[0].outerHTML;
        });

        html += '</div></div>';

        if(is_edit_form==1)
        {

        }
        else
        {
            $("#item_form .select-spec-combination").css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});
            html+= '<div style="position:absolute;right:20px;margin-top:-45px;">';
        //html+= $("#item_image")[0].outerHTML;
        html += '</div></div>';
        html+= '<div style="position:absolute;right:80px;margin-top:-45px;">';
        html+= '<div id="image_of_user" class="form-group " style="">';
        //html+= '<label id="image_of_user_label" class="control-label col-md-3"> Product picture</label>';
        html+= '<div class="col-md-9">';
        html+=     '<div class="fileinput fileinput-new" data-provides="fileinput" >';
        html+=     '<div style="height:50px;width:60px;margin-top:-35px;" id="item_image" class="fileinput-preview thumbnail " name="item_image" data-trigger="fileinput">';
        html+=     '</div>';
        html+= '<div>';
        html+= '<span class="btn default btn-file">';
        html+= '<span class="fileinput-new" style="right:60px;"><?php echo $this->lang->line('product_pic'); ?></span>';
        html+= '<span class="fileinput-exists" style="width:50px">Edit</span>';
        html+= '<input type="file"  name="combination_images_'+img+'"></span>';
        html+= '<a style="padding:10px;" href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Delete </a>';
        html+= '</div></div></div></div>';
        html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    }

    html+= '</div><div class="testheightdiv" style="height:30px;"></div>';
    $(html).insertAfter(".select-tag");
    $('.select-tag .spec-tags').remove();

    var spec_set = [];
    var spec_values = '';
    var item_new_combination = '';
    var item_name = $('input[name="items_name"]').val();
    for (var i=0;i<myElements.length;i++) {
        spec_data = {};
        spec_data.id = (myElements.eq(i).attr("spec-id-data"));
        spec_data.value = (myElements.eq(i).attr("spec-value-data"));
        //spec_data.image = imageName;
        spec_set.push(spec_data);
        spec_values += '-';
        spec_values +=     (myElements.eq(i).attr("spec-value-data"));
    }
    
    spec_set_list.push(spec_set);
    img++;
    item_new_combination = item_name+'-'+spec_values;
    $("#item_new_name").html(item_new_combination);
    return false;
});

    $('#item_form').on('click', '.remove_tag', function () 
    {
        if($(this).closest('.form-group').hasClass('added')){    
            var set_id = $(this).closest('.form-group').attr('id').split('|')[1];
            var  se =  $(this).parent('.spec-tags');
            $.each(spec_set_list[set_id], function(index, val) {
                if(val.id == se.attr('spec-id-data'))
                {
                    spec_set_list[set_id].splice(index, 1);
                    return false;
                }
            });
        }
        $(this).parent('.spec-tags').remove();
    });



    var table_item = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "dom" : 'Bfrltip',
        "draw": 1,
        "StateSave": true, 
        "buttons" : [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5',
        'print'
        ],
        "ajax": "<?php echo site_url(); ?>/item/all_item_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ]
    });



    $('#cancle_item').click(function(event) {
        spec_set_list.length =0;
        $("div").remove(".added");
        $("div").remove(".testheightdiv");
        $('input[name="barcode_number"]').val('');
        $('input[name="product_warranty"]').val('');
        $("#selectTags .set_val").val(''); /******for new item page design*******/
        $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
        $("#selectTags").show();        /******for new item page design*******/
        $("#selectTagsView").hide();   /******for new item page design*******/

        $('#item_form').attr('action', "<?php echo site_url('/Item/save_item_info');?>");
        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");

        $("#item_form #image_of_user").remove();
        $("#edit_image_of_user").hide();
        $('input[name="items_name"]').val('');
        $('input[name="category"]').val('');
        $('textarea[name="items_description"]').val('');
        $('input[name="product_warranty"]').val('');
        $('#category_select').val('');
        $('input[name="unique_id"]').val('');
        $(".spec-tags").remove();
        $("#image_of_user").remove();

    });


    $('#item_form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });


    $('#final_submit').click(function(event) {

        var spec_set = [];
        var spec_values = '';
        var item_new_combination = '';
        var myElements = $(".select-tag .spec-tags");
        var item_name = $('input[name="items_name"]').val();
        var barcode = $('input[name="barcode_number"]').val();
        $('input[name = barcode_number]').removeAttr('disabled');

        if(barcode!='')
        {
            if(barcode.length<4)
            {
                $('#barcode_invalid').slideDown();
                setTimeout( function(){$('#barcode_invalid').slideUp()}, 4500 );
                return false;
            }
        }
        for (var i=0;i<myElements.length;) {
            spec_data = {};
            spec_data.id = (myElements.eq(i).attr("spec-id-data"));
            spec_data.value = (myElements.eq(i).attr("spec-value-data"));
            if(spec_data.value=="")
            {
                i++;
            }
            else
            {
               spec_set.push(spec_data);
               i++;
           }

       }

       spec_set_list.push(spec_set);
       if(spec_set_list.length==0)
       {
        alert("please give at least one specification value");
    }
    else
    {
       $('#item_form').submit();      
   }
});


    $('#item_form').ajaxForm({
        data: {
            specs_values: spec_set_list
        }, 
        beforeSubmit: function() {   
           // console.log(spec_set_list); exit;
       },
       success: function(msg) {

        if(msg == "No permission")
        {
            // $('#not_permitted').slideDown();
            // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
                // if(msg == "success"){spec_set
                    //     $('#insert_success').slideDown();
                    //     setTimeout( function(){$('#insert_success').slideUp()}, 1500 );
                    // }
                    else if(msg == "success")
                    {

                        $("#selectTags .set_val").val(''); /******for new item page design*******/
                        $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
                        $("#selectTags").show();        /******for new item page design*******/
                        $("#selectTagsView").hide();   /******for new item page design*******/

                        $('#no_item').hide();
                        $('#no_spec').hide();
                        $('input[name="items_name"]').css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});

                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        // $('#insert_success').slideDown();
                        // setTimeout( function(){$('#insert_success').slideUp()}, 2500 );
                        toastr.success('<?php echo $this->lang->line('product_insert_succeded'); ?>', 'Success', {timeOut: 6000});
                        toastr.options = {"positionClass": "toast-top-right"}; 
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $("#item_form #image_of_user").remove();
                        $('input[name="items_name"]').val('');
                        $('input[name="category"]').val('');
                        $('input[name="barcode_number"]').val('');
                        $('input[name="product_warranty"]').val('');
                        $('textarea[name="items_description"]').val('');
                        $('#category_select').val('');
                        $('input[name="unique_id"]').val('');
                        $('input[name = unique_barcode]').parents('span').removeClass('checked');
                        $('input[name = unique_barcode]').val("off");
                        $('input[name = is_unique]').val("no");
                        $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                       // $(".spec-tags").remove(); /*-----Not need to remove fore new designed system----*/
                       $("#image_of_user").remove();

                   }
                   else if(msg =="failed")
                   {
                    // $('#insert_failure').slideDown();
                    // setTimeout( function(){$('#insert_success').slideUp()}, 4500 );
                    toastr.error('<?php echo $this->lang->line('product_insert_failed'); ?>', 'Failed', {timeOut: 6000});
                    toastr.options = {"positionClass": "toast-top-right"}; 
                }

                if(msg=="barcode_exist")
                {
                    spec_set_list.length =0;
                    $('#barcode_exist').slideDown();
                    setTimeout( function(){$('#barcode_exist').slideUp()}, 5500 );
                }

                if(msg == "Give a item name")
                {
                    spec_set_list.length =0;
                    $('#no_item').slideDown();
                    $('input[name="items_name"]').css({"border": "1px solid red"});
                        //setTimeout( function(){$('#no_item').slideUp()}, 1500 );
                    }
                    if(msg == "Choose item spec combination")
                    {
                        spec_set_list.length =0;
                        $("#item_form .select-spec-combination").css({"border": "1px solid red", "color": "red"});
                        $('#no_spec').slideDown();
                        setTimeout( function(){$('#no_spec').slideUp()}, 1500 );

                    }

                    if(msg == "Update Success")
                    {
                     $("#selectTags .set_val").val(''); /******for new item page design*******/
                     $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/
                     $("#selectTags").show();        /******for new item page design*******/
                     $("#selectTagsView").hide();   /******for new item page design*******/

                     $('#no_item').hide();
                     $('#no_spec').hide();
                     $('input[name="items_name"]').css({"border": "1px solid rgb(226, 226, 226)", "color": "inherit"});

                        // $('#update_success').slideDown();
                        // setTimeout( function(){$('#update_success').slideUp()}, 1500 );

                        $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
                        $('#item_form').attr('action', "<?php echo site_url('/Item/save_item_info');?>");
                        spec_set_list.length =0;
                        $("div").remove(".added");
                        $("div").remove(".testheightdiv");
                        $('input[name="items_name"]').val('');
                        $("#is_edit").val('');
                        $('input[name="category"]').val('');
                        $('input[name="items_description"]').val('');
                        $('input[name="barcode_number"]').val('');
                        $('input[name="product_warranty"]').val('');
                        $('#category_select').val('');

                        $('input[name="unique_id"]').val('');
                        $('#unique_barcode').prop('checked', false);
                        $('input[name = unique_barcode]').parents('span').removeClass('checked');
                        $('input[name = unique_barcode]').val("off");
                        $('input[name = is_unique]').val("no");

                        $('#update_success').slideDown();
                        setTimeout( function(){$('#update_success').slideUp()}, 1500 );

                        //$(".spec-tags").remove();         /*************It is not necessary for new item design***********/
                       // $("#edit_image_of_user").hide();  /*************It is not necessary for new item design***********/

                       $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>"); 


                       table_item.ajax.reload();
                      //table_item.row(index).data(data).draw(false);
                      return false;
                  }
                    // if(msg == "Give a item category")
                    // {
                    //     $('#category_add').slideDown();
                    //     setTimeout( function(){$('#category_add').slideUp()}, 1500 );
                    // }
                    if(msg == "Please choose atmost one combination")
                    {
                        spec_set_list.length =0;
                        $('#one_combination').slideDown();
                        setTimeout( function(){$('#one_combination').slideUp()}, 1500 );
                    }
                },
                complete: function(xhr) {

                    table_item.ajax.reload();
                    return false;
                }
            }); 

/* **********************  
------------------------- 
********************** */

</script>

<script type="text/javascript">


    $("#unique_barcode").click(function(event) 
    {
        var checkValue = $('input[name = unique_barcode]').val();
        console.log(checkValue);
        if(checkValue==='on')
        {
            $('input[name = unique_barcode]').parents('span').removeClass('checked');
            $('input[name = unique_barcode]').val("off");
            $('input[name = is_unique]').val("no");
        }
        else
        {
            $('input[name = unique_barcode]').parents('span').addClass('checked');
            $('input[name = unique_barcode]').val("on");
            $('input[name = is_unique]').val("yes");
        }
    });

    $('table').on('click', '.edit_item',function(event) 
    {
        event.preventDefault();
        $('input[name = unique_barcode]').parents('div').removeClass('checker');
        $("#itemTooltip").hide();
        $("#specDelete").hide();
        $("#categoryDelete").hide();
        $("#edit_image_of_user_delete").hide();
        $("#itemDelete").hide();
        $("#descriptionDelete").hide();
        $("#viewAddItem").hide();
        $("#selectTags").show();        /******for new item page design*******/
        $("#selectTagsView").hide();   /******for new item page design*******/
        $("#specAdd").show();
        $("#categoryAdd").show();
        $("#itemAdd").show();
        $("#descriptionAdd").show();
        $("#actionAdd").show();

        var item_id = $(this).attr('id').split('_')[1];
        $('#current_item_id').remove();
        $( "div" ).remove( ".added" );
        $("#image_of_user").remove();
        
        $("#is_edit").val(1);
        $.get('<?php echo site_url();?>/Item/get_item_info/'+item_id).done(function(data) 
        {
            var htm='';
            var item_data = $.parseJSON(data);
            if(item_data == "No permission")
            {
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else
            {
                $("#edit_image_of_user").show();
                var spec_values = item_data.spec_sets;
                var all_specs_autoselected = item_data.all_specs_autoselected;
                $.each(all_specs_autoselected, function(index, val) 
                {
                    $("#val_"+val.spec_name_id).val("");
                    $("#val_"+val.spec_name_id).next($('.spec-tags')).attr('spec-value-data',"");
                });
                $.each(spec_values, function(index, val) 
                {
                    $("#val_"+val.spec_name_id).val(val.spec_value);
                    $("#val_"+val.spec_name_id).next($('.spec-tags')).attr('spec-value-data',val.spec_value);
                });

                $(".custom-tag-input").val('').focus();
                var html = "<input id='current_item_id' type='hidden' class='form-control' name='items_id' value='"+item_data.item_info_by_id.items_id+"'>";
                var htm = "<input id='current_spec_id' type='hidden' class='form-control' name='item_spec_set_id' value='"+item_data.item_info_by_id.item_spec_set_id+"'>";
                $('#item_form').append(html);
                $('#item_form').append(htm);
                $('#item_form').attr('action', "<?php echo site_url('/Item/update_item_info');?>");
                $('#header_text').text("<?php echo $this->lang->line('product_edit'); ?>");
                $('input[name = barcode_number]').removeAttr('disabled');
                $('input[name = items_name]').val(item_data.item_info_by_id.items_name);
                $('input[name = category]').val(item_data.item_info_by_id.categories_name);
                $('input[name = barcode_number]').val(item_data.item_info_by_id.barcode_number);
                if(item_data.item_info_by_id.unique_barcode=="yes")
                { 
                    $('input[name = unique_barcode]').parents('span').addClass('checked');
                    $('input[name = unique_barcode]').val("on");
                    $('input[name = is_unique]').val("yes");
                    $('#unique_barcode').attr('checked', true); // Checks it
                }
                else
                {
                    $('input[name = unique_barcode]').parents('span').removeClass('checked');
                    $('input[name = unique_barcode]').val("off");
                    $('input[name = is_unique]').val("no");
                    $('#unique_barcode').attr('checked', false); // Checks it
                }

                $('input[name = product_warranty]').val(item_data.item_info_by_id.product_warranty);
                $('input[name = categories_id]').val(item_data.item_info_by_id.categories_id);
                $('textarea[name = items_description]').val(item_data.item_info_by_id.items_description);
                $('#category_select').html('<option value="'+item_data.item_info_by_id.categories_id+'" data-subtext="'+item_data.item_info_by_id.categories_id+'" selected="selected">'+item_data.item_info_by_id.categories_name +'</option>');
                $('#category_select').trigger('change');
                $('select[name=items_status]').val(item_data.item_info_by_id.items_status);
                if(item_data.item_info_by_id.item_spec_set_image != "" && item_data.item_info_by_id.item_spec_set_image!=null)
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+item_data.item_info_by_id.item_spec_set_image+"'>");
                }
                else
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                }

                // var post_data={};
                // post_data.items_name_edit = $('input[name = item_new_name]').val();
                // post_data.item_spec_set_id = $('input[name=item_spec_set_id]').val();
                // post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
                // var row_data = table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data();
                // row_data[0] = post_data.items_name_edit;
                // table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data(row_data).draw();
            }

        });
});


$("#viewAddItem").click(function(event) 
{
    $(this).hide();
    $('#header_text').text("<?php echo $this->lang->line('left_portlet_title_product'); ?>");
    $('input[name = barcode_number]').removeAttr('disabled');
    $('input[name = product_warranty]').removeAttr('disabled');
    $('input[name = barcode_number]').val('');
    $('input[name = product_warranty]').val('');
    $('input[name="items_name"]').val('');
    $("#is_edit").val('');
    $("#specDelete").hide();
    $("#categoryDelete").hide();
    $("#edit_image_of_user_delete").hide();
    $("#itemDelete").hide();
    $("#descriptionDelete").hide();

    $("#selectTags").show();        /******for new item page design*******/
    $("#selectTagsView").hide();   /******for new item page design*******/
    $("#selectTags .set_val").val(''); /******for new item page design*******/
    $("#selectTagsView .set_val").val(''); /******for new item page design*******/
    $("#selectTags .spec-tags").attr('spec-value-data',''); /******for new item page design*******/




    $("#specAdd").show();
    $("#categoryAdd").show();
    $("#itemAdd").show();
    $("#descriptionAdd").show();
    $("#actionAdd").show();
});


$('table').on('click', '.view_item',function(event) {

    event.preventDefault();
    $("#itemTooltip").hide();
    var item_id = $(this).attr('id').split('_')[1];
    $("#viewAddItem").show();
    $("#specAdd").hide();
    $("#categoryAdd").hide();
    $("#itemAdd").hide();
    $("#descriptionAdd").hide();
    $("#actionAdd").hide();

    $("#selectTags").hide();        /******for new item page design*******/
    $("#selectTagsView").show();   /******for new item page design*******/

    $("#specDelete").show();
    $("#categoryDelete").show();
    $("#edit_image_of_user_delete").show();
    $("#itemDelete").show();
    $("#descriptionDelete").show();
    $("#actionDelete").show();
    

    $("#edit_image_of_user").hide();
    $('#current_item_id').remove();
        //$("#spec-tag-field").remove(".spec-tags");
        //$( ".spec-tags" ).remove();  /**********not needed for new design******************/
        $( "div" ).remove( ".added" );
        $("#image_of_user").remove();
        
        $("#is_edit").val(2);
        $.get('<?php echo site_url();?>/Item/get_item_info/'+item_id).done(function(data) {
            var html='';
            var item_data = $.parseJSON(data);
            if(item_data == "No permission")
            {
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else
            {

                var spec_values = item_data.spec_sets;
                var all_spec_values = item_data.all_specs_autoselected;
                /******************** Not needed for new design ***********************/
                // $.each(spec_values, function(index, val) {
                //     html+='<span class="label label-success spec-tags"';
                //     html+='style="float: left;top: 24px; margin-top: 9px;margin-right:5px;"';
                //     html+='spec-id-data="'+val.spec_name_id+'"';    
                //     html+= 'spec-value-data="'+val.spec_value+'">'+val.spec_name+'-'+val.spec_value; 
                //     html+= '<a href="#" class="remove_tag">';
                //     html+= '<i class="fa fa-close clickable"></i></a>'; 
                //     html+= '</span>';
                // });
                /******************** Not needed for new design ***********************/
                // var all_specs_autoselected = item_data.all_specs_autoselected;

                $.each(all_spec_values, function(index, val) 
                {
                    $("#view_"+val.spec_name_id).val("");
                });

                $.each(spec_values, function(index, val) 
                {
                    $("#view_"+val.spec_name_id).val(val.spec_value);
                });

                // $(html).insertAfter(".custom-tag-input");

                $("#spec-tag-field-delete").html(html);
                $("#spec_select_result").html('');
                $(".custom-tag-input").val('').focus();
                $("#specDelete .remove_tag").remove();
                // var html = "<input id='current_item_id' type='hidden' class='form-control' name='items_id' value='"+item_data.item_info_by_id.items_id+"'>";
                // var htm = "<input id='current_spec_id' type='hidden' class='form-control' name='item_spec_set_id' value='"+item_data.item_info_by_id.item_spec_set_id+"'>";
                // $('#item_form').append(html);
                // $('#item_form').append(htm);
                // $('#item_form').attr('action', "<?php echo site_url('/Item/update_item_info');?>");
                $('#header_text').text("<?php echo $this->lang->line('product_view'); ?>");
                $('input[name = items_name_delete]').val(item_data.item_info_by_id.items_name);
                $('input[name = category_name_delete]').val(item_data.item_info_by_id.categories_name);
                $('input[name = barcode_number]').val(item_data.item_info_by_id.barcode_number);
                $('input[name = barcode_number]').attr('disabled','disabled');
                $('input[name = product_warranty]').val(item_data.item_info_by_id.product_warranty);
                $('input[name = product_warranty]').attr('disabled','disabled');
                //$('input[name = unique_id]').val(item_data.item_info_by_id.barcode_number);
                $('input[name = categories_id_delete]').val(item_data.item_info_by_id.categories_id);
                $('textarea[name = items_description_delete]').val(item_data.item_info_by_id.items_description);
                $('#category_select').html('<option value="'+item_data.item_info_by_id.categories_id+'" data-subtext="'+item_data.item_info_by_id.categories_id+'" selected="selected">'+item_data.item_info_by_id.categories_name +'</option>');
                $('#category_select').trigger('change');
                $('select[name=items_status]').val(item_data.item_info_by_id.items_status);
                //$("#final_submit").hide();
                if(item_data.item_info_by_id.item_spec_set_image != "" && item_data.item_info_by_id.item_spec_set_image!=null)
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>"+item_data.item_info_by_id.item_spec_set_image+"'>");
                }
                else
                {
                    $('.fileinput-preview').html("<img src='<?php echo base_url() ?>images/item_images/no_image.png'>");
                }

                // var post_data={};
                // post_data.items_name_edit = $('input[name = item_new_name]').val();
                // post_data.item_spec_set_id = $('input[name=item_spec_set_id]').val();
                // post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

                // var row_data = table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data();
                // row_data[0] = post_data.items_name_edit;
                // table_item.row( $('#edit_'+post_data.item_spec_set_id).closest('tr')[0] ).data(row_data).draw();
            }
        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

$("#selectTags .set_val").keyup(function(event) 
{
    var value = $(this).val();
    $(this).next("span.spec-tags").attr("spec-value-data",value);
});
</script>
<script type="text/javascript">
    var items_id = '' ;
    $('table').on('click', '.delete_item',function(event) {
        event.preventDefault();
        items_id = $(this).attr('id').split('_')[1];
    });


    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        //var items_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'items_id' : items_id,
            csrf_test_name: csrf

        }; 

        $.post('<?php echo site_url();?>/Item/delete_item/'+items_id,post_data).done(function(data) {
            if(data == "No permission")
            {
                $('#responsive_modal_delete').modal('hide');
                // $('#not_permitted').slideDown();
                // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
                toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
                toastr.options = {"positionClass": "toast-top-right"}; 
            }
            else if(data == "success")
            {
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                $('#responsive_modal_delete').modal('hide');
                table_item.row($('#delete_'+items_id).parents('tr')).remove().draw();
            }
            else if(data == "inventory_exist")
            {
                $('#inventory_exist').slideDown();
                setTimeout( function(){$('#inventory_exist').slideUp()}, 6000 );
                $('#responsive_modal_delete').modal('hide');
            }
            else
            {
                alert("An error has occured. pLease try again");                
            }
        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });


// ajax form submit starts....

$('#spec_name_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.spec_name = $('input[name=spec_name]').val();
    data.spec_description = $('input[name=spec_description]').val();
    data.spec_default_status = $('input[name=spec_default_status]:checked').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();

    $.post('<?php echo site_url() ?>/spec_name/save_spec_name_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2 =="No Permisssion"){
            // $('#not_permitted').slideDown();
            // setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            toastr.error('<?php echo $this->lang->line('not_permitted'); ?>', 'Failed', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
        }
        else if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                $('.help-block').show();
            });
        }

        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();

            $('input[name= spec_name]').val('');
            $('input[name= spec_description]').val('');
            $('#insert_success_spec_name').slideDown();
            setTimeout( function(){$('#insert_success_spec_name').slideUp();}, 1500 );
            $('#specificationForm').modal('hide');
            $("#spec_select_result_list > tbody").append('<tr><td class="spec-list sorting_1" data="'+data2.data+'">'+data.spec_name+'</td></tr>');
            // <td class="spec-list sorting_1" data="59dd87e1-bac4-11e6-b419-fcaa14ede82b">SDSD</td>
            //spec_select_result_list.ajax.reload();
            // $('#spec_select_result_list').DataTable();
        }

    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 1500 );
    });    
});

// ajax form submit end.......


    // ajax form submit starts....

    $('#category_form').submit(function(event) {
        event.preventDefault();
        var data = {};
        data.categories_name = $('input[name=categories_name]').val();
        data.categories_description = $('input[name=categories_description]').val();
        data.parent_categories_id = $('input[name=parent_categories_id]').val();
        data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/category/save_category_info',data).done(function(data1, textStatus, xhr) {

            var data2 = $.parseJSON(data1);

            if(data2.success==0)
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }
            else{
                $('.form-group.has-errorf').removeClass('has-error');
                $('.help-block').hide();

                $('input[name= categories_name]').val('');
                $('input[name= categories_description]').val('');
                $('input[name= parent_categories_id]').val('');
                $('#insert_success_category').slideDown();
                setTimeout( function()
                    {$('#insert_success_category').slideUp();}, 3000 );
                $('#categoryForm').modal('hide');
                table_category.row.add({
                    "0":       data.categories_name,
                    "1":   "<a class='btn btn-primary edit_category' id='edit_"+data2.data+"' >    পরিবর্তন </a><a class='btn btn-danger delete_category' id='delete_"+data2.data+"'>ডিলিট </a>"
                }).draw();
            }

        }).error(function() {

            $('#insert_failure').slideDown();
            setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
        });    
    });

// ajax form submit end.......


/* Item-wise Barcode Printing Starts */
// $('.barcode_print').click(function(event) {

    //     alert ("Barcode Print");
    // });

    /* Barcode Print Ends*/

// $(document).on('click', '.item_checkbox', function(event) {
    //     // event.preventDefault();

    //     alert ("item selected");
    // });
    $('table').on('click', '.item_barcode', function(event) {
        event.preventDefault();
        $('.has-error').removeClass('has-error');
        $('.help-block').text('');
        var items_id = $(this).attr('id').split('_')[1];
        $('#current_item_id').remove();
        $.get('<?php echo site_url();?>/Item/get_item_barcode_info/'+items_id).done(function(data) {
            var item_data = $.parseJSON(data);
            $('#barcode_quantity_form').modal('show');
            $('input[name=unique_id]').val(item_data.barcode_by_id.barcode_number);
        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });
    /* Barcode Print Starts*/
    $('#barcode_quantity_form').submit(function(event) {
        event.preventDefault();
        var post_data={};
        post_data.barcode_quantity = $('input[name = print_quantity]').val();
        post_data.unique_id = $('input[name=unique_id]').val();
        post_data.page_size  = $('input[name="page_type"]:checked').val();
        post_data.company_name_add =  $('input[name="add_company"]:checked').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();
        if (post_data.barcode_quantity == "") {
            $('input[name= print_quantity]').parent().parent().addClass('has-error');
            $('input[name= print_quantity]').parent().parent().find('.help-block').text("<?php echo $this->lang->line('barcode_quantity_alert'); ?>");
        }
        else if(!$('input[name="page_type"]:checked').val()){
            alert("<?php echo $this->lang->line('page_size_alert'); ?>");
        }
        else {
            location.href="<?php echo site_url() ?>/item/print_barcode/"+post_data.unique_id+"/"+post_data.barcode_quantity+"/"+post_data.page_size+"/"+post_data.company_name_add;
        }
    });
    /* Barcode Print Ends*/
    $('body').tooltip({
        selector: '.fa.fa-question-circle.tooltips'
    });
</script>
<!-- Script For Buy Starts -->
<script>
    /*lading date picker*/
    jQuery(document).ready(function() {    
        // $('.date-picker').datepicker();
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
        UIExtendedModals.init();
    });
    /*loading date picker ends*/
    jQuery(document).ready(function($) {
        $.fn.numpad.defaults.gridTpl = '<table class="table modal-content"></table>';
        $.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
        $.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
        $.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn btn-default"></button>';
        $.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn" style="width: 100%;"></button>';
        $.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};
        $('#numpadButton-btn1').numpad({
            target: $('#numpadButton1')
        });        
        $('#numpadButton-btn2').numpad({
            target: $('#numpadButton2')
        });    
        $('#numpadButton-btn3').numpad({
            target: $('#numpadButton3')
        });    
        $('#numpadButton-btn4').numpad({
            target: $('#numpadButton4')
        });    
    });
    var timer;
    var csrf = $("input[name='csrf_test_name']").val();

    table = $('#sample_2').DataTable();   
   // var unique_barcode = "";     
        // Customize Boostrap Select for Item Selection.
        $("#item_select").keyup(function(event) 
        {

            $("#item_select_result").show();
            $("#item_select_result").html('');
            $("#unique_barcode_div").html("");
            clearTimeout(timer);
            timer = setTimeout(function() 
            {
                var search_item = $("#item_select").val();
                var html = '';
                $.post('<?php echo site_url(); ?>/buy/search_item_by_name',{q: search_item,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                    data = JSON.parse(data);
                    if(data.barcode_yes != null){
                        $.each(data, function(index, val) {
                            if(val.item_spec_set_image == null || val.item_spec_set_image == '')
                            {
                                val.item_spec_set_image    = "images/item_images/no_image.png";
                            }
                            var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                            html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                        });
                        $("#item_select_result").html(html);
                        $('#item_select_result').find('td').eq(0).click();
                        $('input[name=quantity]').focus();
                    }
                    else
                    {
                        $.each(data, function(index, val) 
                        {
                            if(val.item_spec_set_image == null || val.item_spec_set_image == '')
                            {
                                val.item_spec_set_image    = "images/item_images/no_image.png";
                            }
                            var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.item_spec_set_image+'">';
                            html+= '<tr><td unique_barcode="'+val.unique_barcode+'" data="'+val.item_spec_set_id+'">'+image_html+' '+ val.spec_set+'</td></tr>';
                        });
                        $("#item_select_result").html(html);
                    }
                    // if ($('#item_select').val().length >= 17) {
                    //     $('#item_select_result').find('td').eq(0).click();
                    //     $('input[name=quantity]').focus();
                    // }
                });
            }, 500);
        });

        $("#item_select_result").on('click', 'td', function(event) 
        {
            $('input[name="items_name"]').val($(this).text());
            $('input[name="items_id"]').val($(this).attr('data'));
            $('input[name="is_unique_barcode"]').val($(this).attr('unique_barcode'));
            $("#item_select_result").hide();
        });

        $("#numpadButton1").keyup(function(event) 
        {
            var item_quantity = $(this).val();
            clearTimeout(timer);
            timer = setTimeout(function() 
            {
                var unique_barcode_count = $('input[name*="unique_barcode_value[]"]').length;
                if($("#is_unique_barcode").val() == "yes")
                {

                 if(item_quantity=="")
                 {
                    $("#unique_barcode_div").html("");
                }
                else if(unique_barcode_count==0)
                {
                    var html = "";
                    for(var i=1;i<=item_quantity;)
                    {
                        html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
                        html+= '</label><div class="col-md-9 input-form input-icon"><input ';
                        html+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
                        html+= 'Number '+i+'"></div></div>';
                        i++;
                    }
                    $("#unique_barcode_div").html(html);
                }
                else if(unique_barcode_count>item_quantity)
                {
                 $(".imei_remove_class").show();
             }  
             else if(unique_barcode_count<item_quantity)
             {
                var html_new = "";
                for(var i=unique_barcode_count;i<item_quantity;)
                {
                    html_new+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
                    html_new+= '</label><div class="col-md-9 input-form input-icon"><input ';
                    html_new+='type="text" name="unique_barcode_value[]" class="form-control" placeholder="Serial ';
                    html_new+= 'Number '+i+'"></div></div>';
                    i++;
                }
                $("#unique_barcode_div").append(html_new);
            }              
        }
    }, 500);
        });



        /* Boostrap Customize Select Ends*/
        $("#vendor_select").keyup(function(event) 
        {
            $("#vendor_select_result").show();
            $("#vendor_select_result").html('');

            clearTimeout(timer);
            timer = setTimeout(function() 
            {
                var search_vendor = $("#vendor_select").val();
                var html = '';
                $.post('<?php echo site_url(); ?>/buy/search_vendor_by_name',{q: search_vendor,csrf_test_name: csrf}, function(data, textStatus, xhr) {
                    data = JSON.parse(data);
                    $.each(data, function(index, val) {
                        if(val.vendor_image == null || val.vendor_image == ''){
                            val.vendor_image = "images/user_images/no_image.png";
                        }
                        var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.vendor_image+'">';
                        html+= '<tr><td data="'+val.vendors_id+'">'+image_html+' '+ val.vendors_name+' -- '+val.vendors_phone_1+'</td></tr>';

                    });
                    $("#vendor_select_result").html(html);
                });
            }, 500);
        });

        $("#vendor_select_result").on('click', 'td', function(event) {
            $('input[name="vendors_name"]').val($(this).text());
            $('input[name="vendors_id"]').val($(this).attr('data'));
            $("#vendor_select_result").hide();
        });


        var render_data= {
            'imei_barcode' :[],
            'item_info' : [],
            'all_basic_data':{},
        };
        var temp_item_id = "";

        $('.form-actions').on('click', '#add_item', function(event) {
            event.preventDefault();
            var error_found=false;
            if($('input[name=voucher_no]').val().trim()=="")
            {
                $('input[name=voucher_no]').closest('.form-group').addClass('has-error');

                $('#voucher_help_block').text("<?php echo $this->lang->line('right_voucher_err_msg'); ?>")
            // $('input[name=voucher_no]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
            // $('input[name=voucher_no]').parent().find('.help-block').hide();
            $('#voucher_help_block').text("")
        }

        if($('input[name="items_id"]').val()==null || $('input[name="items_id"]').val()=="")
        {
            $('input[name="items_id"]').closest('.form-group').addClass('has-error');
            $('input[name="items_id"]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name="items_id"]').closest('.form-group').removeClass('has-error');
            $('input[name="items_id"]').parent().find('.help-block').hide();

        }
        if($('input[name=quantity]').val().trim()=="")
        {
            $('input[name=quantity]').closest('.form-group').addClass('has-error');
            $('input[name=quantity]').parent().parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=quantity]').closest('.form-group').removeClass('has-error');
            $('input[name=quantity]').parent().parent().find('.help-block').hide();
        }
        if($('input[name=buying_price]').val().trim()=="")
        {
            $('input[name=buying_price]').closest('.form-group').addClass('has-error');
            $('input[name=buying_price]').parent().parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=buying_price]').closest('.form-group').removeClass('has-error');
            $('input[name=buying_price]').parent().parent().find('.help-block').hide();
        }

        /* Demo coding for new barcode integration*/
        /* Demo coding ends*/
        temp_item_id= new Date().getUTCMilliseconds();
        var imei_error = false;
        var imei_duplicate = false;
        if($("#is_unique_barcode").val()=="yes")
        {
            var imei_barcodes = [];
            $("input[name='unique_barcode_value[]']").each(function() 
            {
                var new_imei = $(this).val();
                $.each(render_data['imei_barcode'], function(index, val) {

                    $.each(render_data['imei_barcode'][index], function(index2, val2) {
                        if(render_data['imei_barcode'][index][index2].imei_barcode == new_imei){
                            alert("Duplicate IMEI Found In Cart. (" +new_imei+")");
                            imei_duplicate = true;
                        }
                    });
                });
                if(imei_duplicate == true){
                    return false;
                }
                else if($(this).val()=="")
                {
                    $(this).css("border-color", "red"); 
                    imei_error = true;
                }
                else
                {
                    $(this).css("border-color", "");
                    imei_barcodes.push({
                        'item_spec_set_id' : $ ('input[name="items_id"]').val(),
                        'imei_barcode': $(this).val(),
                        'imei_temp_id':temp_item_id,
                    })
                }

            });
            if(imei_duplicate == true){

            }
            else{
                render_data['imei_barcode'].push(imei_barcodes); 
            }
        }

        if(error_found)
        {
            return false;
        }

        if(imei_duplicate == true){
            return false;
        }

        if(imei_error)
        {
            if(confirm("Want to serial number empty?"))
            {
                imei_error = true;
               // $("#unique_barcode_div").html("");
               //return true;
           }
           else
           {
            return false;
        }
    }

    if($("#is_unique_barcode").val()=="yes")
    {
        var unique_barcode_exist = $('input[name*="unique_barcode_value[]"]').length;
        var current_quantity = $('input[name="quantity"]').val();
        if(current_quantity!=unique_barcode_exist)
        {
            alert("Please make equal quantity of imei field");
            $(".imei_remove_class").show();
            return false;
        }
    }

    render_data['item_info'].push({
            // 'item_spec_set_id' : $('#item_select').val(),
            // 'item_name' : $('#item_select option:selected').text(),
            'item_spec_set_id' : $ ('input[name="items_id"]').val(),
            'item_name' : $('input[name="items_name"]').val(),
            'actual_quantity' : $('input[name="quantity"]').val(),
            'quantity' : ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val(),

            'sub_total' : $('#dis_type').val()=='free_quantity' ? Number(parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)) : ($('#dis_type').val()=='amount' ? Number((parseFloat($('input[name="buying_price"]').val())*parseFloat($('input[name="quantity"]').val()))-parseFloat($ ('input[name="discount_amount"]').val()).toFixed(2)) : Number(((parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()))-((parseFloat($('input[name="buying_price"]').val())*parseFloat($('input[name="quantity"]').val())*parseFloat($ ('input[name="discount_amount"]').val()))/100)).toFixed(2))) || Number(parseFloat($('input[name="buying_price"]').val()) * parseFloat($('input[name="quantity"]').val()).toFixed(2)),

            'buying_price' : $('input[name="buying_price"]').val(),
            // 'buying_price_with_landed_cost' : $('input[name="buying_price_with_landed_cost"]').val(),
            'retail_price' : $ ('input[name="retail_price"]').val(),
            'whole_sale_price' : $ ('input[name="whole_sale_price"]').val(),
            'expire_date' : $ ('input[name="expire_date"]').val(),
            'comments' : $ ('input[name="comments"]').val(),
            'discount_amount' : $ ('input[name="discount_amount"]').val(),
            'discount_type' : $('#dis_type').val(),
            'unique_barcode' : $("#is_unique_barcode").val(),
            // 'quantity_with_free' : $('#dis_type').val(),
            'cart_temp_id':temp_item_id,
        })

    buy_table_render();

        //  clear the whole form rather than individual        **

        // $('#item_select').selectpicker('val','');
        $('input[name="items_name"]').val('');
        $('input[name="items_id"]').val('');
        $("#unique_barcode_div").html("");
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
        $('input[name="expire_date"]').val('');
        $('input[name="comments"]').val('');
        $('input[name="discount_amount"]').val('');
        $("#is_unique_barcode").val('');
        $('#dis_type').val('');
        // if($('#extra_fields').show()){
        //     $('#extra_fields').hide();
        // }
        // $('#extra_fields').show();
        $('input[name=items_name]').focus();
    });

// function autoSelect () {
//  $('.caret:first').click();
// }
var total_amount = 0;
var total_net_payable= 0;
var total_quantity_with_free = 0 ; 
function buy_table_render () {
    var buy_table_html = '';
    total_amount = 0;
    var subtotal_amount = 0;
    $.each(render_data['item_info'], function(index, val) {
        buy_table_html += '<tr class="item_row_table" id="'+index+'">';
        buy_table_html += '<td>'+val.item_name+'</td>';
        if(val.discount_type =="" ||val.discount_type =="amount" || val.discount_type =="percentage" ){
            buy_table_html += '<td>'+val.quantity+'</td>';
        }
        else if(val.discount_type =="free_quantity"){
            total_quantity_with_free = parseInt(val.actual_quantity);
            buy_table_html += '<td>'+total_quantity_with_free+'</td>';
        }
        buy_table_html += '<td>'+Number(val.buying_price).toFixed(2)+'</td>';
        if (val.discount_type =="") {
            buy_table_html += '<td>'+Number(val.quantity*val.buying_price).toFixed(2)+'</td>';
            subtotal_amount+= val.quantity*val.buying_price;
            Number(subtotal_amount.toFixed(2));
        }
        else if(val.discount_type =="amount"){
            var total= val.quantity*val.buying_price;
            total -= val.discount_amount;
            buy_table_html += '<td>'+Number(total).toFixed(2)+'</td>';
            subtotal_amount+= total;
            Number(subtotal_amount.toFixed(2));
        }
        else if (val.discount_type  =="percentage"){
            var total_sub_total= val.quantity*val.buying_price;
            var total =total_sub_total*val.discount_amount;
            total /=100;
            total_sub_total-=total;
            buy_table_html += '<td>'+Number(total_sub_total).toFixed(2)+'</td>';
            subtotal_amount+= total_sub_total;
            Number(subtotal_amount.toFixed(2));
        }
        else if(val.discount_type =="free_quantity"){
            buy_table_html += '<td>'+val.actual_quantity*val.buying_price+'</td>';
            subtotal_amount+= val.actual_quantity*val.buying_price;
            Number(subtotal_amount.toFixed(2));
        }
        buy_table_html += '<td>'+val.discount_amount+" "+val.discount_type+'</td>';
        buy_table_html += '<td><a id="item_remove" class="glyphicon glyphicon-remove" style="color:red;"></a> <a id="" class="glyphicon glyphicon-pencil item_edit" style="color:blue;display:none"></a>';
        buy_table_html += '</td>';
        buy_table_html += '</tr>';
        total_amount = subtotal_amount;
        Number(total_amount.toFixed(2));
    });
    render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));
    render_data['all_basic_data']['due']=Number(total_amount.toFixed(2));
    $('#total_paid').val("");
    $('#total_discount').val(0);
    $('#buy_table tbody').html(buy_table_html);
    $('#total_amount').html(Number(total_amount.toFixed(2)));
    $('#total_net_payable').html(Number(total_amount.toFixed(2)));
    $('#total_due').html(Number(total_amount.toFixed(2)));
}
render_data['all_basic_data']['discount']=0;
render_data['all_basic_data']['net_pay_after_discount']=0;
$('#total_discount').keyup(function(event) {
    if (event.keyCode == '9') {
        return false;
    };
    if ($(this).val() > Number($('#total_amount').text()) ) {
        alert ("<?php echo $this->lang->line('discount_is_greater_than_grand_total')?>");
        $(this).val(0);
        render_data['all_basic_data']['net_payable']=Number(total_amount.toFixed(2));
        render_data['all_basic_data']['due']=Number(total_amount.toFixed(2));
    }
    var net_pay_after_discount = parseFloat(total_amount) - parseFloat($( this ).val())||0;
    $('#total_net_payable').text(Number(net_pay_after_discount.toFixed(2)));
    $('#total_due').text(Number(net_pay_after_discount.toFixed(2)));
    render_data['all_basic_data']['discount']=parseFloat($(this).val());
    render_data['all_basic_data']['due']=Number(net_pay_after_discount.toFixed(2));
    render_data['all_basic_data']['due_after_paid']=Number(net_pay_after_discount.toFixed(2));
    render_data['all_basic_data']['net_pay_after_discount']=Number(net_pay_after_discount.toFixed(2));
    $('#total_paid').val('');
});
var current_cashbox_amount = <?php echo $cashbox_amount['total_cashbox_amount']?>;
$('#total_paid').keyup(function(event) {
    if (event.keyCode == '9') {
        return false;
    };

    if (parseFloat($(this).val()) < current_cashbox_amount)
    {
        var due_after_paid = parseFloat($('#total_net_payable').text()) - parseFloat($(this).val())||0;

        $('#total_due').text(Number(due_after_paid.toFixed(2)));

        render_data['all_basic_data']['paid']=parseFloat($(this).val());
        render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));
    }
    else{
        var due_after_paid = parseFloat($('#total_net_payable').text()) - parseFloat($(this).val()||0);

        $('#total_due').text(Number(due_after_paid.toFixed(2)));

        render_data['all_basic_data']['paid']=parseFloat($(this).val());
        render_data['all_basic_data']['due_after_paid']=Number(due_after_paid.toFixed(2));
    }
});
$('#cancle_item').click(function(event) {
    $('input[name="items_name"]').val('');
    $('input[name="items_id"]').val('');
    $('input[name="quantity"]').val('');
    $('input[name="buying_price"]').val('');
    $('input[name="retail_price"]').val('');
    $('input[name="whole_sale_price"]').val('');
    $('input[name="expire_date"]').val('');
    $('input[name="comments"]').val('');
    $('input[name="discount_amount"]').val('');
    $('#dis_type').val('');
    $("#unique_barcode_div").html("");
    if($('#update_item').show()){
        $('#update_item').hide();
        $('#add_item').show('fast');
    }
    $("#is_unique_barcode").val('');
});
$('#buy_table').on('click', '#item_remove', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    var temp_id_remove = render_data['item_info'][clicked_id].cart_temp_id;
    $.each(render_data['imei_barcode'], function(index, val) {
        if(render_data['imei_barcode'][index][0].imei_temp_id == temp_id_remove){
            render_data['imei_barcode'].splice(index,1);
            return false;
        }
    });
    render_data['item_info'].splice(clicked_id,1);
    buy_table_render();
});
$('#unique_barcode_div').on('click', '#imei_remove', function () {
    $(this).closest(".form-group").remove();
});

$('#buy_table').on('click', '.item_edit', function () {
    var clicked_id = $(this).closest('tr').attr('id');
    var editing_item_info;
    editing_item_info = render_data['item_info'][clicked_id];
    $("#unique_barcode_div").html("");
    if(editing_item_info.unique_barcode=="yes")
    {
        $("#is_unique_barcode").val("yes");
    }
    else
    {
        $("#is_unique_barcode").val("no");
    }

    if($("#is_unique_barcode").val()== "yes")
    {
     var html = "";
     var i=1;
     $.each(render_data['imei_barcode'],function(index, val) 
     {
        $.each(render_data['imei_barcode'][index],function(index1, el)
        {
            if(editing_item_info.item_spec_set_id==el.item_spec_set_id)
            {
                html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
                html+= '</label><div class="col-md-9 input-form input-icon"><input ';
                html+='type="text" name="unique_barcode_value[]" class="form-control" value="'+el.imei_barcode+'" placeholder="Serial ';
                html+= 'Number '+i+'"><a style="display:none;color:red" id="imei_remove" class="glyphicon glyphicon-remove imei_remove_class" style="color:red;"></a></div></div>';
                i++;
            }
        });
    });
     for(j=i;j<=editing_item_info.actual_quantity;)
     {
        html+='<div class="form-group"><label class="control-label col-md-3"> Serial #'+i;
        html+= '</label><div class="col-md-9 input-form input-icon"><input ';
        html+='type="text" name="unique_barcode_value[]" class="form-control" value="" placeholder="Serial ';
        html+= 'Number '+i+'"><a style="display:none;color:red" id="imei_remove" class="glyphicon glyphicon-remove imei_remove_class" style="color:red;"></a></div></div>';
        j++;
    }
    $("#unique_barcode_div").html(html);
}

$('#item_select').prop("disabled",true);

$('input[name="items_id"]').val(editing_item_info.item_spec_set_id),
$('input[name="items_name"]').val(editing_item_info.item_name),
$('input[name="buying_price"]').val(editing_item_info.buying_price);
$('select[name=discount_type]').val(editing_item_info.discount_type);
$('input[name="buying_price"]').val(editing_item_info.buying_price);
$('input[name="quantity"]').val(editing_item_info.actual_quantity);
$('input[name="comments"]').val(editing_item_info.comments);
$('input[name="discount_amount"]').val(editing_item_info.discount_amount);
$('input[name="expire_date"]').val(editing_item_info.expire_date);
$('input[name="retail_price"]').val(editing_item_info.retail_price);
$('input[name="whole_sale_price"]').val(editing_item_info.whole_sale_price);
$('#add_item').hide();
$('#update_item').show('slow');
$('#update_item').attr('selected-id', clicked_id);

});
$('.form-actions').on('click', '#update_item', function(event) {
    event.preventDefault();

    var error_found=false;
    if($('input[name=voucher_no]').val().trim()=="")
    {
        $('input[name=voucher_no]').closest('.form-group').addClass('has-error');

        $('#voucher_help_block').text("<?php echo $this->lang->line('right_voucher_err_msg'); ?>")
            // $('input[name=voucher_no]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=voucher_no]').closest('.form-group').removeClass('has-error');
            // $('input[name=voucher_no]').parent().find('.help-block').hide();
            $('#voucher_help_block').text("")
        }

        if($('input[name="items_id"]').val()==null || $('input[name="items_id"]').val()=="")
        {
            $('input[name="items_id"]').closest('.form-group').addClass('has-error');
            $('input[name="items_id"]').parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name="items_id"]').closest('.form-group').removeClass('has-error');
            $('input[name="items_id"]').parent().find('.help-block').hide();

        }
        if($('input[name=quantity]').val().trim()=="")
        {
            $('input[name=quantity]').closest('.form-group').addClass('has-error');
            $('input[name=quantity]').parent().parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=quantity]').closest('.form-group').removeClass('has-error');
            $('input[name=quantity]').parent().parent().find('.help-block').hide();
        }
        if($('input[name=buying_price]').val().trim()=="")
        {
            $('input[name=buying_price]').closest('.form-group').addClass('has-error');
            $('input[name=buying_price]').parent().parent().find('.help-block').show();
            error_found=true;
        }
        else
        {
            $('input[name=buying_price]').closest('.form-group').removeClass('has-error');
            $('input[name=buying_price]').parent().parent().find('.help-block').hide();
        }



        /* Demo coding for new barcode integration*/

        /* Demo coding ends*/
       // var temp_item_id = "";


       var imei_error = false;
       var indexPoint ;
       var item_spec_set_id = $ ('input[name="items_id"]').val();
       $.each(render_data['imei_barcode'],function(index, val) 
       {
        if($.inArray(item_spec_set_id, render_data['imei_barcode'][index]))
        {
            render_data['imei_barcode'][index].length=0
            indexPoint = index;
            return false;
        }
    });
       console.log(indexPoint);

       if($("#is_unique_barcode").val()=="yes")
       {
        var imei_barcodes = [];
        $("input[name='unique_barcode_value[]']").each(function() 
        {
            if($(this).val()=="")
            {

                $(this).css("border-color", "red"); 
                imei_error = true;
            }
            else
            {
                $(this).css("border-color", "");
                render_data['imei_barcode'][indexPoint].push({
                    'item_spec_set_id' : $ ('input[name="items_id"]').val(),
                    'imei_barcode': $(this).val(),
                    'temp_id':temp_item_id,
                })
            }

        });
    }
    if(error_found)
    {
        return false;
    }
    if(imei_error)
    {
        if(confirm("Want to serial number empty?"))
        {
            imei_error = true;
        }
        else
        {
            return false;
        }
    }

    if($("#is_unique_barcode").val()=="yes")
    {
        var unique_barcode_exist = $('input[name*="unique_barcode_value[]"]').length;
        var current_quantity = $('input[name="quantity"]').val();
        if(current_quantity!=unique_barcode_exist)
        {
            alert("Please make equal quantity of imei field");
            $(".imei_remove_class").show();
            return false;
        }
    }
    var change_item_index_array = $(this).attr('selected-id');
    render_data.item_info[change_item_index_array].item_spec_set_id = $('input[name="items_id"]').val();
    render_data.item_info[change_item_index_array].item_name = $('input[name="items_name"]').val();
    render_data.item_info[change_item_index_array].quantity = ($('#dis_type').val()=='free_quantity') ? parseInt($('input[name="quantity"]').val())+parseInt($ ('input[name="discount_amount"]').val()) : $('input[name="quantity"]').val();
    render_data.item_info[change_item_index_array].actual_quantity = $('input[name="quantity"]').val();
    render_data.item_info[change_item_index_array].discount_type = $('#dis_type').val();
    render_data.item_info[change_item_index_array].discount_amount = $('input[name="discount_amount"]').val();
    render_data.item_info[change_item_index_array].retail_price = $('input[name="retail_price"]').val();
    render_data.item_info[change_item_index_array].whole_sale_price = $('input[name="whole_sale_price"]').val();
    render_data.item_info[change_item_index_array].buying_price = $('input[name="buying_price"]').val();
    render_data.item_info[change_item_index_array].comments = $('input[name="comments"]').val();
    $("#unique_barcode_div").html("");
    buy_table_render();
    $('#item_select_field').show('fast');
    $('#add_item').show('fast');
    $('#update_item').hide();
    $("#unique_barcode_div").html("");
    $('#item_select').prop("disabled",false);
        // $('#item_select').selectpicker('val','');
        $('input[name="items_name"]').val('');
        $('input[name="items_id"]').val('');
        $('input[name="quantity"]').val('');
        $('input[name="buying_price"]').val('');
        $('input[name="retail_price"]').val('');
        $('input[name="whole_sale_price"]').val('');
        $('input[name="expire_date"]').val('');
        $('input[name="comments"]').val('');
        $('input[name="discount_amount"]').val('');
        $("#is_unique_barcode").val('');
        $('#dis_type').val('');
    });
$('#save_buy').click(function(event) {
    event.preventDefault();
    var keyCode = event.keyCode || event.which;
    if (keyCode === 13) { 
        event.preventDefault();
        return false;
    }
    $('#save_buy').prop("disabled",false);
    render_data.all_basic_data.payment_type = $('input[name=payment_type]:checked').val();
    render_data.all_basic_data.voucher_no = $('input[name="voucher_no"]').val();
    render_data.all_basic_data.vendor_id = $('input[name="vendors_id"]').val();
    render_data.all_basic_data.vendor_name =$('input[name="vendors_name"]').val();
    render_data.all_basic_data.total_cost = $('input[name="landed_cost"]').val();
    render_data.all_basic_data.discount = $('input[name="discount"]').val();
    render_data.all_basic_data.paid = $('input[name="paid"]').val();
    render_data.all_basic_data.bank_acc_id = $('input[name=bank_acc_id]').val();
    render_data.all_basic_data.cheque_page_num = $('input[name="cheque_page_num"]').val();
    render_data.all_basic_data.purchase_date = $('input[name=purchase_date]').val();
    var all_data= {
        'csrf_test_name' :$('input[name=csrf_test_name]').val(),
        'all_data' : render_data,
    }
    $.post('<?php echo site_url() ?>/buy/save_buy_info',all_data ).done(function(data, textStatus, xhr) {
        var result = $.parseJSON(data);
        remove_all_errors();
        if (result== "success") {
            render_data['item_info'] = [];
            render_data['imei_barcode'] = [];
            render_data['all_basic_data'] = {};
            $("#cash_id").attr('checked', 'checked').click();
            $('#chk_page_div').hide('fast');
            $('#ac_select_div').hide('fast');    
            $('input[name=purchase_date]').val('');
            $('input[name="vendors_name"]').val('');
            $('input[name="vendors_id"]').val('');
            $('input[name="bank_acc_num"]').val('');
            $('input[name="bank_acc_id"]').val('');
            $('input[name="cheque_page_num"]').val('');
            $('input[name="voucher_no"]').val('');
            $('input[name="landed_cost"]').val('');
            $('input[name="discount"]').val('');
            $('input[name="paid"]').val('');
            $('#total_net_payable').val('');
            $('.item_row_table').remove();
            $("#is_unique_barcode").val('');

            $('#total_amount').html('');
            $('#total_net_payable').html('');
            $('#total_due').html('');                
            toastr.success('<?php echo $this->lang->line('alert_sus_msg_of_item_add');?>', 'Success', {timeOut: 6000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            $('#save_buy').prop("disabled",false);
            $('#total_discount').val(0);
        }
        else{
            var error_array = [];
            $.each(result, function(index, val) {
                error_array.push(val);
                if(val =="Enter actual paid amount"){
                   $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                   $('#total_paid_help_block').text("<?php echo $this->lang->line('more_than_actual_amount'); ?>");
               }
               if(val == "Voucher No Cannot Be Empty"){
                 $('#voucher_no_id').closest('.form-group').addClass('has-error');
                 $('#voucher_help_block').text("<?php echo $this->lang->line('voucher_no_empty'); ?>");
             }
             if(val == "Voucher No Already Exist"){
                $('#voucher_no_id').closest('.form-group').addClass('has-error');
                $('#voucher_help_block').text("<?php echo $this->lang->line('voucher_exist'); ?>");
            }

            if(val == "Discount Amount Is Not Valid"){
                $('table tfoot tr').eq(1).css("color","red").addClass('has-error');
                $('#total_discount_help_block').text("<?php echo $this->lang->line('dis_amount_invalid'); ?>");

            }
            if(val == "Landed Cost Is Not Numeric"){
                $('#landed_cost_id').closest('.form-group').addClass('has-error');
                $('#landed_help_block').text("<?php echo $this->lang->line('landed_cost_invalid'); ?>");
            }
            if(val == "Paid Amount Is Not Numeric"){
                $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                $('#total_paid_help_block').text("<?php echo $this->lang->line('paid_amount_invalid'); ?>");
            }
            if(val == "Total Paid Amount is Empty or Not Numeric"){
                $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
                $('#total_paid_help_block').text("<?php echo $this->lang->line('paid_amount_empty'); ?>");
            }
            if(val == "Select Vendor for Due Transaction"){
                $('#vendor_select').closest('.form-group').addClass('has-error');
                $('#vendor_help_block').text("<?php echo $this->lang->line('select_vendor_for_due_transaction')?>");
            }
            if (val == "Bank Acc Not Select") {
             $('#bank_acc_select').closest('.form-group').addClass('has-error');
             $('#bank_acc_help_block').text("<?php echo $this->lang->line('bank_acc_not_select')?>");
         }
         if(val == "Not Enough Balance in Selected Account"){
            $('#bank_acc_select').closest('.form-group').addClass('has-error');
            $('#bank_acc_help_block').text("<?php echo $this->lang->line('not_enough_balance')?>");
        }
        if(val == "Cheque Page Number Empty"){
          $('#chk_page').closest('.form-group').addClass('has-error');
          $('#cheque_help_block').text("<?php echo $this->lang->line('cheque_page_empty'); ?>");
      }
      if(val == "Calculation Mismatch"){
          $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
          $('#total_paid_help_block').text("<?php echo $this->lang->line('for_no_discount_calculation_error'); ?>");
      }
      if(val == "Calculation Mismatch for discount"){
          $('table tfoot tr').eq(3).css("color","red").addClass('has-error');
          $('#total_paid_help_block').text("<?php echo $this->lang->line('for_no_discount_calculation_error'); ?>");
          $('table tfoot tr').eq(1).css("color","red").addClass('has-error');
          $('#total_discount_help_block').text("<?php echo $this->lang->line('for_discount_calculation_error'); ?>");
      }
  });
            $.each(error_array, function(index, val) {
                alert(val);
                $('#save_buy').prop("disabled",false);
            });

        }
    }).error(function() {
        alert("<?php echo $this->lang->line('alert_failed_msg');?>");
        $('#save_buy').prop("disabled",false);
    });
});
function remove_all_errors() {
    $('#bank_acc_select').closest('.form-group').removeClass('has-error');
    $('#vendor_help_block').text("");

    $('#vendor_select').closest('.form-group').removeClass('has-error');
    $('#bank_acc_help_block').text("");

    $('#chk_page').closest('.form-group').removeClass('has-error');
    $('#cheque_help_block').text("");

    $('table tfoot tr').eq(3).css("color","black").removeClass('has-error');
    $('#total_paid_help_block').text("");

    $('table tfoot tr').eq(1).css("color","black").removeClass('has-error');
    $('#total_discount_help_block').text("");

    $('#landed_cost_id').closest('.form-group').removeClass('has-error');
    $('#landed_help_block').text("");

    $('#voucher_no_id').closest('.form-group').removeClass('has-error');
    $('#voucher_help_block').text("");
}

$('input[name=voucher_no]').focus();

$('#total_discount').val(0);

/* Initial value provided while load the buy page for blank buy form submit and error display*/
render_data['all_basic_data']['net_payable']=Number(0);
render_data['all_basic_data']['due']=Number(0);
render_data['all_basic_data']['due_after_paid']=Number(0);
/**/
// Start of AJAX Booostrap for Category selection
var options = {
    ajax          : {
        url     : '<?php echo site_url(); ?>/category/search_category_by_name',
        type    : 'POST',
        dataType: 'json',
        // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
        // automatically replace it with the value of the search query.
        data    : {
            q: '{{{q}}}',
            csrf_test_name: csrf
        }
    },
    locale        : {
        emptyTitle: '<?php echo $this->lang->line('main_category_name'); ?>'
    },
    log           : 3,
    preprocessData: function (data) {
        var i, l = data.length, array = [];
        if (l) {
            for (i = 0; i < l; i++) {
                array.push($.extend(true, data[i], {
                    text : data[i].categories_name,
                    value: data[i].categories_id,
                    data : {
                        subtext: ''
                    }
                }));
            }
        }
        // You must always return a valid array when processing data. The
        // data argument passed is a clone and cannot be modified directly.
        return array;
    }
};
$('#select_category').selectpicker().ajaxSelectPicker(options);
$('#select_category').trigger('change');
// End of AJAX Boostrap for Category selection

$('input:radio[name="payment_type"]').change(
    function(){
        if ( $(this).val() == 'cheque' ) {
            $('#chk_page_div').show('fast');
            $('#ac_select_div').show('fast');    
        }    
        if($(this).val() == 'cash'){
           $('#chk_page_div').hide('fast');
           $('#ac_select_div').hide('fast');    

       }
   });
var timer;
var csrf = $("input[name='csrf_test_name']").val();
  // Start of AJAX  for Bank Account Selection
  $("#bank_acc_select").keyup(function(event) 
  {
      $("#bank_acc_select_result").show();
      $("#bank_acc_select_result").html('');
      clearTimeout(timer);
      timer = setTimeout(function() 
      {
          var seachBankAcc = $("#bank_acc_select").val();
          var html = '';
          $.post('<?php echo site_url(); ?>/bank_dpst_wdrl/search_bank_acc_by_name',{q: seachBankAcc,csrf_test_name: csrf}, function(data, textStatus, xhr) {
              data = JSON.parse(data);
              $.each(data, function(index, val) {
                  html+= '<tr><td height="40" data="'+val.bank_acc_id+'">'+val.bank_acc_num+'</td></tr>';
              });
              $("#bank_acc_select_result").html(html);
          });
      }, 500);
  });
  $("#bank_acc_select_result").on('click', 'td', function(event) {
      $('input[name="bank_acc_num"]').val($(this).html());
      $('input[name="bank_acc_id"]').val($(this).attr('data'));
      $("#bank_acc_select_result").hide();
  });
  //End of AJAX  for Bank Account Selection
</script>