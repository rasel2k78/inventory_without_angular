	<div class="portlet box red">
		<div class="portlet-title">
			<div class="caption left">
				<i class="fa fa-pencil-square-o"></i><span id="form_header_text">Update Item Price</span>
            </div>
        </div>
        <div class="portlet-body form" id="item_update_price_div">
            <!-- BEGIN FORM-->
            <?php $form_attrib = array('id' => 'item_update_price_form','class' => 'form-horizontal form-bordered');
            echo form_open('',  $form_attrib, '');?>

            <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

                <div class="form-body">
                   <input type="hidden" name="item_id_price"  id="price_item_id">
                   <div class="form-group">
                    <label class="control-label col-md-3">Retail Price</label>
                    <div class="col-md-9">
                        <input type="number" min="1" step="1" class="form-control" name="retail_price" placeholder="<?php echo $this->lang->line('left_selling_item_retail'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Wholesale Price</label>
                    <div class="col-md-9">
                        <input type="number" min="1" step="1" class="form-control" name="whole_sale_price" placeholder="<?php echo $this->lang->line('left_selling_item_wholesale'); ?>">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button class="btn dark btn-outline pull-right" style="margin-left: 8px" type="button" id="price_edit_cancle"><?php echo $this->lang->line('cancle')?></button>
                <button type="submit" class="btn green pull-right"><?php echo $this->lang->line('save')?></button>
            </div>
            <?php echo form_close();?>
            <!-- END FORM-->
        </div>
    </div>

    <!-- END PAGE LEVEL PLUGINS -->
    <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
    <script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->