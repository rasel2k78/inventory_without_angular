<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>


<!-- END PAGE LEVEL STYLES -->

<div class="row">
    <div class="col-md-5">

        <div class="alert alert-danger" id="user_not_selected" style="display:none" role="alert"><?php echo $this->lang->line('user_not_selected'); ?></div>
        <div class="alert alert-success" id="access_added" style="display:none" role="alert"><?php echo $this->lang->line('access_added'); ?></div>
        <div class="alert alert-danger" id="access_adding_fail" style="display:none" role="alert"><?php echo $this->lang->line('access_adding_fail'); ?></div>
        <div class="alert alert-success" id="access_remove_success" style="display:none" role="alert"><?php echo $this->lang->line('access_remove_success'); ?></div>
        <div class="alert alert-danger" id="access_remove_fail" style="display:none" role="alert"><?php echo $this->lang->line('access_remove_fail'); ?></div>
        <div class="alert alert-danger" id="no_permission" style="display:none" role="alert"><?php echo $this->lang->line('no_permission'); ?></div>

        <div class="alert alert-danger" id="owner_remove_fail" style="display:none" role="alert"><?php echo $this->lang->line('owner_remove_fail'); ?></div>



        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cubes icon-state-success"></i>All categories
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered " id="sample_1">
                    <thead>
                        <tr>
                            <th>
                                Category
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Get items
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <div class="col-md-7">



        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box red">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tags"></i><span id="header_text_items"></span>
                </div>

            </div>
            <div class="portlet-body">
             <table class="table table-bordered " id="item_table">
                <thead>
                    <tr>
                        <th>
                            Item
                        </th>
                        <th>
                            Avaiable qty
                        </th>
                        <th>
                            Buying price
                        </th>
                        <th>
                            Retail price
                        </th>
                        <th>
                            Wholesale price
                        </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>

    </div>

    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>




<!-- <?php //var_dump($this->cache->cache_info()); ?>
Time: <?php echo $this->benchmark->elapsed_time(); ?>
Mem: <?php echo $this->benchmark->memory_usage(); ?> -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>



<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script type="text/javascript">

//Code for user data table start
var table_user = $('#sample_1').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": "<?php echo site_url(); ?>/Itemsearch/allCatgoryForDataTable/",
    "lengthMenu": [
    [10, 15, 20,30],
    [10, 15, 20,30] 
    ],
    "pageLength": 10,
    "language": {
        "lengthMenu": " _MENU_ records",
        "paging": {
            "previous": "Prev",
            "next": "Next"
        }
    },
    "columnDefs": [{  
        'orderable': true,
        'targets': [0]
    }, {
        "searchable": true,
        "targets": [0]
    }],
    "order": [
    [0, "desc"]
    ]
});
//Code for user data table end




var id_of_category = '';
var table_dt;

var table_item = function(){ 
  return $('#item_table').DataTable({
    "processing": true,
    "serverSide": true,

    "ajax": "<?php echo site_url(); ?>/Itemsearch/allItemsByCategory/"+"?id_cat="+id_of_category,

    "lengthMenu": [
    [10, 15, 20,500],
    [10, 15, 20,500] 
    ],
    "pageLength": 10,
    "language": {
      "lengthMenu": " _MENU_ records",
      "paging": {
        "previous": "Prev",
        "next": "Next"
    }
},
"order": [
[0, "desc"]
]
});
}



//Code for showing checkbox on form for edit user start
var category_id ='';

$('table').on('click', '.get_item_by_cat',function(event) {
    event.preventDefault();

    category_id = $(this).attr('id').split('_')[1];
    categor_name = $(this).closest('tr').find('td').eq(0).text().trim();
    $('#header_text_items').html('Items under the category '+categor_name);

    id_of_category = category_id;

    if(typeof table_dt!='undefined')
    {
        table_dt.destroy();
    }
    table_dt = table_item();

});
//Code for showing checkbox on form for edit user end




</script>
