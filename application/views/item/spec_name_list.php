
<div class="portlet-title">
	<!-- <div class="caption left">
		<i class="fa fa-pencil-square-o"></i><span id="form_header_text">Specification List</span>
	</div> -->

</div>
<div class="portlet-body">
	<div class="form-group">
		<table class="table table-condensed table-hover table-bordered clickable" id="spec_select_result_list" style="background-color: rgb(255, 255, 255); top: 51px; left: 20px; width: 299px;">

			<thead>
				<tr>
					<th style="text-align: center;">
						<?php echo $this->lang->line('spec_name_list'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach ($all_specs_autoselected as $key => $value) {
                    ?>
                    <tr>
                        <td  class="spec-list" data="<?php echo $value['spec_name_id']?>"><?php echo $value['spec_name']?></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    $('#spec_name_form').on('click', '#spec_cancel', function(event) {
        event.preventDefault();
        $('#specificationForm').modal('hide');
        $('input[name= spec_name]').val('');
        $('input[name= spec_description]').val('');
    });
</script>