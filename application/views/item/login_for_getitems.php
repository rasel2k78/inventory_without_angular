<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>


<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-modal/2.2.6/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<div class="row">	
	<div class="col-md-6">
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-lock"></i> <span id="form_header_text"> CRM Credentials for get item master </span>
				</div>

			</div>
			<div class="portlet-body form">
				<?php
				$form_attrib = array('id' => 'crm_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
				echo form_open(site_url('/GetItems/getItems'),  $form_attrib, '');
				?>

				<div class="form-body">
					<div class="form-group ">
						<label class="control-label col-md-3">Email <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9 ">
							<input type="email" class="form-control" name="email" placeholder="Email">
							<span class="help-block"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Password <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9 ">
							<input type="password" class="form-control" name="password" placeholder="Password">
							<span class="help-block"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Select type <sup><i class="fa fa-star custom-required"></i></sup></label>
						<div class="col-md-9 ">
							<select class="col-md-9 col-sm-9 col-lg-9 form-control" id="item_type_selector" name="item_type" class="form-control input-inline">
								<option value="Cloths">Cloths</option>
								<option value="Grocery">Grocery</option>
								<option value="Electronics">Electronics</option>
							</select>
						</div>
					</div>


				</div>
				<div class="form-actions">
					<button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px">Cancel</button>
					<input type="submit" value="Submit" class="btn green pull-right"/>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="col-md-6">

	<div class="alert alert-danger" style="display: none;" id="error_id" role="alert">Choose correct file format / Something went wrong</span></div>
	<div class="alert alert-success" style="display: none;" id="success_id" role="alert">Data imported successfully</span></div>
	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-file"></i> <span id="form_header_text"> Item import from file </span>
			</div>

		</div>
		<div class="portlet-body form">
			<?php
			$form_attrib = array('id' => 'file_import_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
			echo form_open(site_url('/Item_importer'),  $form_attrib, '');
			?>

			<div class="form-body">

				<div class="form-group ">
					<label class="control-label col-md-3">File preview</label>
					<div class="col-md-9 ">
						<input type="button" id="preview_btn" type="button" value="Preview" class="btn green pull-left"/>
					</div>
				</div>

				<div class="form-group ">
					<label class="control-label col-md-3">Sample file download</label>
					<div class="col-md-9 ">
						<input type="button" id="download_btn" value="Download" class="btn green pull-left"/>
					</div>
				</div>

				<div class="form-group ">
					<label class="control-label col-md-3">Select CSV file <sup><i class="fa fa-star custom-required"></i></sup></label>
					<div class="col-md-9 ">

						<input type="file" class="form-control" name="item_file">
						<span class="help-block"></span>
					</div>
				</div>

				<div style="display: none;" id="loader_icon" class="form-group ">
					<img class="col-md-offset-5 col-sm-offset-5 col-lg-offset-5" src="<?php echo base_url();?>images/loader.gif" style="width: 50px; height: 50px;">
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px">Cancel</button>
				<input type="submit" value="Submit" class="btn green pull-right"/>
			</div>
		</form>
	</div>
</div>
</div>
</div>


<div style="display: none;" id="preview_div" class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-file"></i> <span id="form_header_text"> Item master file preview </span>
				</div>

			</div>
			<div class="portlet-body form">
				<div style="text-align: center;">
					<img  src="<?php echo base_url()?>images/item master preview.png">
				</div>
				
				<div class="form-actions">
					<div style="text-align: center;">
						<button id="close_btn" style="text-align: center;" type="button" id="cancel_update">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script> -->
<script type="text/javascript">

	$('#file_import_form').ajaxForm({
		beforeSubmit: function() {    
			$('#loader_icon').show();
		},
		success: function(msg) {
			if(msg == "Something went wrong")
			{
				$('#error_id').slideDown();
				setTimeout( function(){$('#error_id').slideUp()}, 1500 );
			}else{
				$('#success_id').slideDown();
				setTimeout( function(){$('#success_id').slideUp()}, 1500 );
				$('#loader_icon').hide();	
			}
		},
		complete: function(xhr) {
			$('#loader_icon').hide();
		}

	}); 

	$("#preview_btn").click(function(event) {
		$("#preview_div").show('250', function() {
		});
	});
	
	$("#close_btn").click(function(event) {
		$("#preview_div").hide('250', function() {
		});
	});

	$("#download_btn").click(function(event) {
		window.open('<?php echo base_url();?>item_list_sample.csv', '_blank');	
	});

</script>

