<!-- CODE FOR SLIDER ALERT END-->

<div class="portlet box red">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-user-plus"></i> <span id="form_header_text"><?php echo $this->lang->line('add_new_customer'); ?></span>
        </div>
        
    </div>
    <div class="portlet-body">

        <!-- BEGIN OF CUSTOMER FORM-->

        <?php
        $form_attrib = array('id' => 'only_vendor_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
        echo form_open(site_url('/Vendor/createAccount'),  $form_attrib, '');
        ?>

        <div class="form-body">
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_name'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="vendors_name" id="vendor_name_id" placeholder="<?php echo $this->lang->line('customer_name'); ?>">
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_phone'); ?> <sup><i class="fa fa-star custom-required"></i></sup></label>
                <div class="col-md-9">
                    <input type="number" min="0" step="1" class="form-control" name="vendors_phone_1"  placeholder="<?php echo $this->lang->line('customer_phone'); ?> ">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3"><?php echo $this->lang->line('customer_present_address'); ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="vendors_present_address"  placeholder="<?php echo $this->lang->line('customer_present_address'); ?>">
                    <span class="help-block"></span>
                </div>
            </div>

            <div id="image_of_user" class="form-group ">
                <label id="image_of_user_label" class="control-label col-md-3"><?php echo $this->lang->line('customer_image'); ?></label>
                <div class="col-md-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div id="user_image" class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                        </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">
                                    <?php echo $this->lang->line('select_image'); ?>
                                </span>
                                <span class="fileinput-exists">
                                    <?php echo $this->lang->line('change'); ?> 
                                </span>
                                <input type="file" name="user_image">
                            </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                <?php echo $this->lang->line('delete'); ?> 
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="cancel_update" class="btn default pull-right" style="margin-left: 8px"><?php echo $this->lang->line('cancel'); ?></button>
            <!-- <button type="submit"  class="btn green pull-right" ><?php echo $this->lang->line('save'); ?></button> -->
            
            <input type="submit" value="<?php echo $this->lang->line('save'); ?>"  class="btn green"/>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<!-- END OF CUSTOMER FORM-->

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

<!-- AJAX Form Post Starts Here -->
<script type="text/javascript">

    $('#only_vendor_form').ajaxForm({
        beforeSubmit: function() {    
            $('#vendor_name_id').closest('.form-group').removeClass('has-error');
            $('#vendor_name_id').siblings().text("");
            $('input[name = vendors_phone_1]').closest('.form-group').removeClass('has-error');
            $('input[name = vendors_phone_1]').siblings().text("");
        },
        success: function(msg) {
            if(msg == "Give vendor name")
            {
                $('#vendor_name_id').closest('.form-group').addClass('has-error');
                $('#vendor_name_id').siblings().text("<?php echo $this->lang->line('validation_name'); ?>");
            // $('#no_vendorname').slideDown();
            // setTimeout( function(){$('#no_vendorname').slideUp()}, 1500 );exit;
        }
        else if(msg == "Give vendor phone1")
        {
            $('input[name = vendors_phone_1]').closest('.form-group').addClass('has-error');
            $('input[name = vendors_phone_1]').siblings().text("<?php echo $this->lang->line('no_phone'); ?>");
            // $('#no_phone').slideDown();
            // setTimeout( function(){$('#no_phone').slideUp()}, 1500 );exit;
        }
        else if(msg.substring(0, 7) == "success"){
            $('#vendor_form_modal').modal('hide');
            toastr.success('<?php echo $this->lang->line('user_insert_success'); ?>', 'Success', {timeOut: 5000});
            toastr.options = {"positionClass": "toast-top-right"}; 
            // $('#user_insert_success').slideDown();
            // setTimeout( function(){$('#user_insert_success').slideUp()}, 3000 );
            var vendor_name = $('#vendor_name_id').val();
            var vendor_id = msg.substring(7, 44);
            $('#vendor_select').val(vendor_name);
            $('input[name="vendors_id"]').val(vendor_id);
            $('#only_vendor_form').trigger("reset");

        }

        else if(msg == "No permission"){
            $('#vendor_form_modal').modal('hide');
            $('#only_vendor_form').trigger("reset");
            $('#no_permission_left').slideDown();
            setTimeout( function(){$('#no_permission_left').slideUp()}, 5000 );

        }
    },
    complete: function(xhr) {
      //  table_user.ajax.reload();
      return false;
  }

}); 

    $('#cancel_update').click(function(event) {
        event.preventDefault();
        $('#vendor_form_modal').modal('hide');
    });
</script>
