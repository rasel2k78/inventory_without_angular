<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<div class="row">
    <div class="col-md-6">
        <div class="alert alert-danger" id="not_permitted" style="display:none" role="alert"><?php echo $this->lang->line('not_permitted'); ?> </span></div>

        <div class="alert alert-success" id="delete_success" style="display:none" role="alert"><?php echo $this->lang->line('delete_success'); ?></div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert"><?php echo $this->lang->line('insert_failed'); ?></div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert"><?php echo $this->lang->line('insert_succeded'); ?></div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert"><?php echo $this->lang->line('update_succeded'); ?></div>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption left">
                    <i class="fa fa-pencil-square-o"></i><span id="header_text"> <?php echo $this->lang->line('left_portlet_title'); ?></span>
                </div>

            </div>
            <div class="portlet-body form" id="color_insert_div">
                <!-- BEGIN FORM-->
                <?php $form_attrib = array('id' => 'color_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <!-- <form id="size_form" class="form-horizontal form-bordered"  method="post"> -->

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('color_name'); ?> *</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="colors_name" placeholder="<?php echo $this->lang->line('color_name_placeholder'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green"><?php echo $this->lang->line('save'); ?></button>
                    <button type="reset" class="btn default"><?php echo $this->lang->line('cancle'); ?></button>
                </div>
                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
            <div class="portlet-body form" id="color_edit_div" style="display:none">
                <!-- BEGIN FORM-->
                <?php
                $form_attrib = array('id' => 'color_edit_form','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');
                ?>

                <input type="text" class="form-control" name="colors_id" style="display:none">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $this->lang->line('color_name'); ?>*</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="colors_name_edit" placeholder="<?php echo $this->lang->line('color_name'); ?>">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green"><?php echo $this->lang->line('save'); ?></button>
                    <button type="button" class="btn default" id="edit_color_cancle"><?php echo $this->lang->line('cancle'); ?></button>
                </div>

                <?php echo form_close();?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption right">
                    <i class="fa fa-list"></i><?php echo $this->lang->line('right_portlet_title'); ?>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                <?php echo $this->lang->line('color_name'); ?>
                            </th>
                            <th>
                                <?php echo $this->lang->line('edit'); ?> / <?php echo $this->lang->line('delete'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>

<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><?php echo $this->lang->line('confirm_delete'); ?> <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <?php echo $this->lang->line('want_to_delete'); ?>
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default"><?php echo $this->lang->line('No'); ?></button>
    <button type="button" id="delete_confirmation" class="btn blue"><?php echo $this->lang->line('Yes'); ?></button>
</div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>


<script src="<?php echo CURRENT_ASSET;?>assets/build/react.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/react-dom.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/browser.min.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/build/color.js"></script>
<!-- <script type="text/babel" src="<?php echo CURRENT_ASSET;?>assets/components/color.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
    // table_color = $('#sample_2').DataTable();        

    var table_color = $('#sample_2').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo site_url(); ?>/Color/all_colors_info_for_datatable/",
        "lengthMenu": [
        [10, 15, 20,30],
        [10, 15, 20,30] 
        ],
        "pageLength": 10,
        "language": {
            "lengthMenu": " _MENU_ records",
            "paging": {
                "previous": "Prev",
                "next": "Next"
            }
        },
        "columnDefs": [{  
            'orderable': true,
            'targets': [0]
        }, {
            "searchable": true,
            "targets": [0]
        }],
        "order": [
        [0, "desc"]
        ]
    });

</script>

<script type="text/javascript">
    $('table').on('click', '.edit_color',function(event) {
        event.preventDefault();
        $('.has-error').removeClass('has-error');
        $('.help-block').text('');

        var color_id = $(this).attr('id').split('_')[1];
        $('#current_color_id').remove();

        $.get('<?php echo site_url();?>/Color/get_color_info/'+color_id).done(function(data) {

            var color_data = $.parseJSON(data);

            if(color_data == "No permission")
            {
                $('#not_permitted').slideDown();
                setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            }
            //$('#form_header_text').text("ইউনিটের তথ্য পরিবর্তন করুন");
            $('#header_text').text("<?php echo $this->lang->line('edit_color'); ?>");
            $('input[name = colors_name_edit]').val(color_data.color_info_by_id.colors_name);
            $('#color_insert_div').slideUp('fast');
            $('#color_edit_div').slideDown('slow');
            $('input[name=colors_id]').val(color_id);

        }).error(function() {
            alert("An error has occured. pLease try again");
        });
    });

    $('#color_edit_form').submit(function(event) {
        event.preventDefault();
        var post_data={};
        post_data.colors_name_edit = $('input[name = colors_name_edit]').val();

        post_data.colors_id = $('input[name=colors_id]').val();
        post_data.csrf_test_name = $('input[name=csrf_test_name]').val();

        $.post('<?php echo site_url() ?>/Color/update_color_info',post_data).done(function(data, textStatus, xhr) {

            var data2 = $.parseJSON(data);
            
            if(data2['success'])
            {
                $('#update_success').slideDown();
                setTimeout( function(){$('#update_success').slideUp()}, 3000 );
                var row_data = table_color.row( $('#edit_'+post_data.colors_id).closest('tr')[0] ).data();
                row_data[0] = post_data.colors_name_edit;
                table_color.row( $('#edit_'+post_data.colors_id).closest('tr')[0] ).data(row_data).draw();
                $('#color_insert_div').slideDown('slow');
                $('#color_edit_div').slideUp('fast');
                $('#form_header_text').text("নতুন ইউনিট  যোগ করুন");
            }
            else if(!Object.keys(data2.error).length)
            {
                alert("color update failed");                
            }
            else
            {
                $.each(data2.error, function(index, val) {
                    $('input[name='+index+']').parent().parent().addClass('has-error');
                    $('input[name='+index+']').parent().parent().find('.help-block').text(val);
                });
            }

        }).error(function() {

            alert("Connection failure.");
        });

    });

    $('#edit_color_cancle').click(function(event) {
        event.preventDefault();
        $('#color_insert_div').slideDown('slow');
        $('#color_edit_div').slideUp('fast');
        $('#form_header_text').text("নতুন ইউনিট  যোগ করুন");

    });
</script>

<script type="text/javascript">
    
    var colors_id = '' ;
    $('table').on('click', '.delete_color',function(event) {
        event.preventDefault();
        colors_id = $(this).attr('id').split('_')[1];
    });

    $('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
        event.preventDefault();
        //var colors_id = $(this).attr('id').split('_')[1];
        var post_data ={
            'colors_id' : colors_id,
            'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        }; 

        $.post('<?php echo site_url();?>/Color/delete_color/'+colors_id,post_data).done(function(data) {
            // if(data == "No permission")
            // {
            //     $('#not_permitted').slideDown();
            //     setTimeout( function(){$('#not_permitted').slideUp()}, 1500 );
            //     $('#responsive_modal_delete').modal('hide');
            // }
            if(data = "success")
            {
                $('#delete_success').slideDown();
                setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
                $('#responsive_modal_delete').modal('hide');
                table_color.row($('#delete_'+colors_id).parents('tr')).remove().draw();
            }
            else
            {
                $('#delete_failure').slideDown();
                setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
            }
            

        }).error(function() {
            alert("An error has occured. pLease try again");                
        });
    });
// ajax form submit starts....

$('#color_form').submit(function(event) {
    event.preventDefault();
    var data = {};
    data.colors_name = $('input[name=colors_name]').val();
    data.csrf_test_name = $('input[name=csrf_test_name]').val();
    

    $.post('<?php echo site_url() ?>/color/save_color_info',data).done(function(data1, textStatus, xhr) {
        var data2 = $.parseJSON(data1);
        if(data2.success==0)
        {
            $.each(data2.error, function(index, val) {
                $('input[name='+index+']').parent().parent().addClass('has-error');
                $('input[name='+index+']').parent().parent().find('.help-block').text(val);
            });
        }
        else
        {
            $('.form-group.has-error').removeClass('has-error');
            $('.help-block').hide();

            $('input[name= colors_name]').val('');
            $('#insert_success').slideDown();
            setTimeout( function(){$('#insert_success').slideUp();}, 3000 );
            table_color.row.add({
                "0":       data.colors_name,
                "1":   "<a class='btn btn-primary edit_color' id='edit_"+data2.data+"' >    পরিবর্তন </a><a class='btn btn-danger delete_color' id='delete_"+data2.data+"'>ডিলিট </a>"
            }).draw();
        }

    }).error(function() {

        $('#insert_failure').slideDown();
        setTimeout( function(){$('#insert_failure').slideUp()}, 3000 );
    });    
});

// ajax form submit end.......
</script>