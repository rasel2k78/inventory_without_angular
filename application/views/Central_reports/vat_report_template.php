 <!-- BEGIN PAGE CONTENT-->
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-xs-12">
					<h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
                    <p style="text-align: center;"><?php echo $store_info['address']?></p>
                    <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <p><strong>Report:</strong> VAT Report</p>
                    </div>
                              <div class="col-xs-6">
                        <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Voucher No
                                </th>
                                <th>
                                    Amount
                                </th>
                            </tr>
                        </thead>
                        <tbody id="buy_report_template">
        <?php foreach ($vat_data as $v_vat_data): ?>
                                <tr>
                                    <td>
            <?php echo $v_vat_data['sell_local_voucher_no']?>
                                    </td>
                                    <td>
            <?php echo $v_vat_data['vat']?>
                                    </td>
                                </tr>
        <?php endforeach ?>
                                    <tr style="font-weight: bold; font-size: 14px">
                                <td>
                                    Total
                                </td>
                                <td>
            <?php echo $vat_details['vat_amount']?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8 invoice-block">
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                        Print <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT