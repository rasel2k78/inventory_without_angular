 <!-- BEGIN PAGE CONTENT-->
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-xs-12">
					<h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
                    <p style="text-align: center;"><?php echo $store_info['address']?></p>
                    <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <p><strong>Report:</strong> Profit/Revenue Report </p>
                    </div>
                              <div class="col-xs-6">
                        <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Credit
                                </th>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Debit
                                </th>
                                <th>
                                    Amount
                                </th>
                            </tr>
                        </thead>
                        <tbody id="buy_report_template">
                                    <tr>
                                    <td>
                                        Sell 
                                    </td>
                                    <td>
            <?php echo number_format($total_sell_amount['paid'], 2)?>
                                    </td>
                                    <td>
                                        Buy
                                    </td>
                                    <td class="hidden-480">
            <?php echo number_format($total_buy_amount['paid'], 2)?>
                                    </td>
                                        <td class="hidden-480">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Expense
                                    </td>
                                    <td class="hidden-480">
            <?php echo number_format($total_expense_amount['expenditures_amount'], 2)?>
                                    </td>
                                        <td class="hidden-480">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Damage-Lost
                                    </td>
                                    <td class="hidden-480">
            <?php echo number_format($total_damage_amount['damage_lost_amount'], 2)?>
                                    </td>
                                        <td class="hidden-480">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Landed Cost
                                    </td>
                                    <td class="hidden-480">
            <?php echo number_format($total_landed_cost['landed_cost'], 2)?>
                                    </td>
                                        <td class="hidden-480">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        VAT
                                    </td>
                                    <td class="hidden-480">
            <?php echo number_format($total_vat_amount['vat_amount'], 2)?>
                                    </td>
                                        <td class="hidden-480">
                                    </td>
                                </tr>
                            <tr style="font-weight: bold; font-size: 14px">
                                <td>
                                    Total
                                </td>
                                <td><?php echo number_format($total_sell_amount['paid'], 2)  ?> </td>
                                <td>
                                </td>
                                <td> <?php echo number_format($total_buy_amount['paid']+$total_expense_amount['expenditures_amount']+ $total_damage_amount['damage_lost_amount'] + $total_landed_cost['landed_cost']+$total_vat_amount['vat_amount'], 2)?></td>
                                <td class="hidden-480">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8 invoice-block">
                    <ul class="list-unstyled amounts">
                    <li>
                        <strong>Total Credit:<?php echo nbs(2),number_format($total_sell_amount['paid'], 2)  ?> </strong> 
                        </li>
                        <li>
                        <strong>Total Debit:<?php echo nbs(3),number_format($total_debit_amount, 2)?></strong>   
                        </li>
        <?php if ($total_debit_amount > $total_sell_amount['paid']) : ?>
                        <li>
                        <strong style="color: red">Loss:<?php echo nbs(12),number_format($total_debit_amount-$total_sell_amount['paid'], 2)?></strong>   
                        </li>
        <?php else: ?>
                        <li>
                        <strong style="color: green">Revenue:<?php echo nbs(6),number_format($total_sell_amount['paid']-$total_debit_amount, 2)?></strong>
                        </li>
        <?php endif ?>
                    </ul>
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                        Print <i class="fa fa-print"></i>
                    </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT