<?php
/**
 * Form Name:"Advance Buy Form" 
 *
 * For advance payment for purchasing items.
 * 
 * @link   Advance_buy/index
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

?>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- Boostrap modal css starts -->
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<!-- Boostrap modal css ends -->
<!-- Date Range Picker css starts -->
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<!-- Date Range Picker css ends -->

<!-- END PAGE LEVEL STYLES -->



<div class="row">
    <div class="col-md-6">
        <div class="alert alert-success" id="delete_success" style="display:none" role="alert">সফলভাবে ক্রয়ের তথ্য ডিলিট হয়েছে</div>
        <div class="alert alert-danger" id="insert_failure" style="display:none" role="alert">দুঃখিত! বাকি টাকা পরিশোধ হয়নি</div>
        <div class="alert alert-success" id="insert_success" style="display:none" role="alert">সফলভাবে বাকি টাকা পরিশোধ হল</div>
        <div class="alert alert-success" id="update_success" style="display:none" role="alert">সফলভাবে বাকি টাকা পরিশোধ হল</div>
        <div class="alert alert-success" id="delete_failure" style="display:none" role="alert">দুঃখিত!!ক্রয়ের তথ্য ডিলিট হয়নি </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i>অগ্রিম পণ্য ক্রয়ের তালিকা
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>
                                ক্রয়ের রসিদ নং
                            </th>
                            <th>
                                তারিখ
                            </th>
                            <th>
                                বিস্তারিত / ডিলিট
                            </th>

                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Voucher</th>
                            
                            <th>
                                <div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control dont-search" name="from_create_date">
                                    <span class="input-group-addon">
                                        to
                                    </span>
                                    <input type="text" class="form-control dont-search" name="to_create_date">
                                </div>
                            </th>
                            <th>Change/Delete</th>
                        </tr>
                    </tfoot>

                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
    <div class="col-md-6">
        

        <div class="portlet box red" id="advance_buy_details_form" style="display:none">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i><span id="form_header_text" >পণ্য ক্রয়ের বিবরণ</span>
                </div>

            </div>
            <div class="portlet-body form"  >
                <!-- BEGIN FORM-->

                <?php $form_attrib = array('id' => 'all_advance_buy_from','class' => 'form-horizontal form-bordered');
                echo form_open('',  $form_attrib, '');?>

                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">রসিদ নং</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="voucher_no" placeholder="রসিদ নং" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">মোট বিল</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="grand_total" placeholder="মোট বিল" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">মূল্যছাড় </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="discount" placeholder="মূল্যছাড়" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">প্রদেয় </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="net_payable" placeholder="প্রদেয়" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">মোট পরিশোধ</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="paid" placeholder="মোট পরিশোধ" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">মোট বাকি</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="due" placeholder="মোট বাকি" readonly="true">

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">বাকি পরিশোধ *</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="due_paymentmade_amount" placeholder="বাকি পরিশোধ">
                        <span class="help-block"></span>
                    </div>
                </div>

                
                <div class="form-group">
                    <label class="control-label col-md-3">বিবরণ</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="due_paymentmade_comments" placeholder="বিবরণ">

                    </div>
                </div>
                
                

                <div class="form-group">
                    <table id="table_items" class="table table-hover control-label col-md-3">
                        <thead>
                            <tr >
                                <th class="control-label col-md-3">পণ্যের নাম</th>
                                <th class="control-label col-md-3">পরিমাণ</th>
                                <th class="control-label col-md-3">Receive Amount</th>
                                <th class="control-label col-md-3">ক্রয় মূল্য /ইউনিট </th>
                                <th class="control-label col-md-3">সাব-টোটাল</th>
                                <th class="control-label col-md-3">Receive</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form-actions">
                    <button id="buy_due_paymentmade_cancle" type="button" class="btn default pull-right">বাতিল করুন</button>
                    <button type="button" id="save_received_quantity" class="btn green pull-right">সেভ করুন</button>
                    
                </div>

                <?php echo form_close();?>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
<!-- Modal view starts -->
<div id="responsive_modal_delete" class="modal fade in animated shake" tabindex="-1" data-width="460">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">ডিলিট নিশ্চিতকরণঃ <span id="selected_name" class="caption-subject bold font-yellow-casablanca uppercase"></span></h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                আপনি কি জমা / উত্তোলনের এই তথ্য ডিলিট করতে চান ? 
            </div>
            <!-- <div class="col-md-6">
        </div> -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
    <button type="button" id="delete_confirmation" class="btn blue">Yes</button>
</div>
</div>
<!-- Modal view ends -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/select2/select2.min.js"></script>
<!--<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>  -->
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/custom/js/jquery.dataTables.min.js"></script>
<!--  <script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/all.min.js"></script> -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- Boostrap modal starts-->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<!-- Boostrap modal end -->
<!-- Date Range Picker starts -->
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Date Range Picker Ends -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->	
<script>
        // table_buy_list_all = $('#sample_2').DataTable();    
        /*lading date picker*/
        jQuery(document).ready(function() {    
            $('.date-picker').datepicker();
            UIExtendedModals.init();
        });

        
        $('#sample_2 tfoot th').each(function (idx,elm){
            if (idx == 0 || idx == 2 ) { 
                var title = $('#example tfoot th').eq( $(this).index() ).text();
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
            }
        });

        var table_buy_list_all= $('#sample_2').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo site_url(); ?>/advance_buy/all_advance_buy_info_for_datatable/",
            "lengthMenu": [
            [10, 15, 20,30],
            [10, 15, 20,30] 
            ],
            "pageLength": 10,
            "language": {
                "lengthMenu": " _MENU_ records",
                "paging": {
                    "previous": "Prev",
                    "next": "Next"
                }
            },
            "columnDefs": [{  
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
            [0, "desc"]
            ]
        });    

        /*data table customize search starts*/
        table_buy_list_all.columns().every( function () {
            var that = this;
            $('input[type="text"]', this.footer() ).on( 'keyup change', function () {
                if (!($(this).hasClass('dont-search'))) {
                    that
                    .search( this.value )
                    .draw();
                }
            });

            $('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
                var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]').val();
                var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]').val();

                var final_string = '';
                if(date_from !='' || date_to != ''){
                    if (date_from!='' && date_to=='') {
                        final_string = date_from+'_0';
                    }
                    if (date_to!='' && date_from=='') {
                        final_string = '0_'+date_to;
                    }
                    if (date_from!='' && date_to!='') {
                        final_string = date_from+'_'+date_to;
                    }
                }
                else{
                    final_string = '0_0';
                }
                that.search(final_string).draw();
            });
        });
        /*datatable customize search ends*/

    </script>
    <script type="text/javascript">


// ajax form view starts....
var buy_data;
$('table').on('click', '.advance_buy_details',function(event) {
    event.preventDefault();
    $('#advance_buy_details_form').show(400);

    var advance_buy_details_id = $(this).attr('id').split('_')[1];
    $('#current_advance_buy_details_id').remove();

    $.get('<?php echo site_url();?>/advance_buy/get_advacne_buy_info/'+advance_buy_details_id).done(function(data) {

    // console.log(buy_details);

    buy_data = $.parseJSON(data);
    
    $('input[name = voucher_no]').val(buy_data.advance_buy_info_by_id[0].voucher_no);
    $('input[name = net_payable]').val(buy_data.advance_buy_info_by_id[0].net_payable);
    $('input[name = grand_total]').val(buy_data.advance_buy_info_by_id[0].grand_total);
    $('input[name = discount]').val(buy_data.advance_buy_info_by_id[0].discount);
    $('input[name = paid]').val(buy_data.advance_buy_info_by_id[0].paid);
    $('input[name = due]').val(buy_data.advance_buy_info_by_id[0].due);

    var table_html = '';
    $.each(buy_data.advance_buy_info_by_id, function(index, val) {
        table_html += '<tr items-id="'+val.items_id+'" id="'+index+'">'
        table_html += '<td>'
        table_html += val.items_name;
        table_html += '</td><td>';
        table_html += val.quantity;
        table_html += '</td><td>';
        if(true){
            table_html += '<input type="text" name="" id="rec_'+index+'" class="form-control input-sm rec_input">';
        }
        else{
            table_html+='#rec_quantity';
        }
        table_html += '</td><td>';
        table_html += val.buying_price;
        table_html += '</td><td>';
        table_html += val.buying_price * val.quantity;
        table_html += '</td><td id="'+val.advance_buy_details_id+'">';
        if(true){
            table_html += '<button type="button" id="rec_btn_ed_'+index+'" class="btn red rec_btn_edit"><span class="fa fa-pencil"></span></button>';
        }
        else{
            table_html+= 'Fully Received';
        }
        table_html += '</td>';
        table_html += '</tr>'
    });
    $('#table_items tbody').html(table_html);


}).error(function() {
    alert("An error has occured. pLease try again");
});
});
var clicked_edit_row ;
$('#table_items').on('click', '.rec_btn_edit', function(event) {
    event.preventDefault();
    clicked_edit_row = $(this).attr('id').split('_').reverse()[0];
    var clicked_buy_data = buy_data.advance_buy_info_by_id[clicked_edit_row];
    $('#rec_'+clicked_edit_row).val(clicked_buy_data.quantity);
});

// Ajax Delete 

var buy_details_id = '' ;
var selected_name = '';
$('table').on('click', '.delete_buy_info',function(event) {
    event.preventDefault();
    buy_details_id = $(this).attr('id').split('_')[1];
    selected_name = $(this).parent().parent().find('td:first').text();
    $('#selected_name').html(selected_name);
});


$('#responsive_modal_delete').on('click', '#delete_confirmation',function(event) {
    event.preventDefault();
    // var buy_details_id = $(this).attr('id').split('_')[1];
    var post_data ={
        'buy_details_id' : buy_details_id,
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
    }; 

    $.post('<?php echo site_url();?>/buy_list_all/delete_buy/'+buy_details_id,post_data).done(function(data) {
        if(data = "success")
        {
            $('#delete_success').slideDown();
            setTimeout( function(){$('#delete_success').slideUp()}, 3000 );
            $('#responsive_modal_delete').modal('hide');

            table_buy_list_all.row($('#delete_'+buy_details_id).parents('tr')).remove().draw();
        }
        else
        {
            $('#delete_failure').slideDown();
            setTimeout( function(){$('#delete_failure').slideUp()}, 3000 );                
        }

    }).error(function() {
        alert("An error has occured. pLease try again");                
    });
});

$('#advance_buy_details_form').on('click', '#save_received_quantity', function(event) {
    event.preventDefault();
    var final_array = {};
    var rec_array = [];


    final_array.voucher_no = buy_data.advance_buy_info_by_id[0].voucher_no;
    final_array.due_paymentmade_amount = $('input[name="due_paymentmade_amount"]').val();
    final_array.due_paymentmade_comments = $('input[name="due_paymentmade_comments"]').val();
    final_array.due_paymentmade_comments = $('input[name="due_paymentmade_comments"]').val();


    $.each($('#table_items tbody tr'), function(index,elm){
        var clicked_items_id = $(elm).attr('items-id');
        var clicked_advance_buy_details_id = $(elm).find('td:last').attr('id');
        var received_quantity = $('#rec_'+index).val();
        rec_array.push({
            'voucher_no' : buy_data.advance_buy_info_by_id[0].voucher_no,
            'advance_buy_details_id' : clicked_advance_buy_details_id,
            'items_id' : clicked_items_id,
            'received_quantity' : received_quantity
        });
    });

    final_array.received_quantity_of_items = rec_array;
    
    var all_data= {
        'csrf_test_name' : $('input[name=csrf_test_name]').val(),
        'all_data' : final_array,
    }


    $.post('<?php echo site_url()?>/advance_buy/update_receive_info', all_data).done(function(data, textStatus, xhr) {
        
        alert ("post data");

    }).error(function() {

        alert ("failed post");

    });

});

</script>