
<div class="portlet-body form">

	<!-- BEGIN OF CUSTOMER FORM-->

	<?php
    $form_attrib = array('id' => 'customer_form','method'=>"post", 'class' => 'form-horizontal form-bordered','enctype'=>'multipart/form-data');
    echo form_open(site_url('/Customers/createAccount'),  $form_attrib, '');
    ?>

    <div class="form-body">
        <div class="form-group">
            <label class="control-label col-md-3">কাস্টমারের নাম *</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_name" placeholder="কাস্টমারের নাম ">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">কাস্টমারের বর্তমান ঠিকানা</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_present_address"  placeholder="কাস্টমারের বর্তমান ঠিকানা">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">কাস্টমারের স্থায়ী ঠিকানা</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_permanent_address"  placeholder="কাস্টমারের স্থায়ী ঠিকানা">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">কাস্টমারের মোবাইল নাম্বার </label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_phone_1"  placeholder="কাস্টমারের মোবাইল নাম্বার ">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">কাস্টমারের মোবাইল নাম্বার ২ </label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_phone_2"  placeholder="কাস্টমারের মোবাইল নাম্বার ২ ">
                <span class="help-block"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">জাতীয় পরিচয় নম্বর</label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="customers_national_id"  placeholder="জাতীয় পরিচয় নম্বর">
                <span class="help-block"></span>
            </div>
        </div>


        <div id="image_of_user" class="form-group ">
            <label id="image_of_user_label" class="control-label col-md-3"> কাস্টমারের ছবি</label>
            <div class="col-md-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div id="user_image" class="fileinput-preview thumbnail" name="user_image" data-trigger="fileinput" style="width: 200px; height: 150px;">
                    </div>
                    <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new">
                                ছবি নির্বাচন করুন
                            </span>
                            <span class="fileinput-exists">
                                পরিবর্তন 
                            </span>
                            <input type="file" name="user_image">
                        </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                            ডিলিট 
                        </a>
                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/>
        <!-- <input type="submit" value="সেভ করুন" name="image_upload" id="image_upload" class="btn green"/> -->
        <!-- <button type="submit" id="add_or_update" class="btn green">নতুন ব্যবহারকারী যোগ করুন</button> -->
        <button type="button" id="cancel_update" class="btn default">বাতিল করুন</button>
    </div>
</form>
<!-- END OF CUSTOMER FORM-->

<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
<script src="<?php echo CURRENT_ASSET;?>assets/global/scripts/jquery.form.js" type="text/javascript"></script>

<script type="text/javascript">
    
    //Ajax form submit start and slider shown after validation check based on msg which is comes from controller
    $('#customer_form').ajaxForm({
        beforeSubmit: function() {    

        },
        success: function(msg) {
            if(msg == "Customer not set")
            {
                $('#no_username').slideDown();
                setTimeout( function(){$('#no_username').slideUp()}, 1500 );
            }
            else if(msg == "success"){
                $('#user_insert_success').slideDown();
                $('#customer_form_modal').modal('hide');
                $('#customer_form').trigger("reset");
                
                setTimeout( function(){$('#user_insert_success').slideUp()}, 1500 );
            }

            else if(msg == "No permission"){
                $('#customer_form_modal').modal('hide');
                $('#customer_form').trigger("reset");
                $('#no_permission_left').slideDown();
                setTimeout( function(){$('#no_permission_left').slideUp()}, 1500 );
                
            }
        },
        complete: function(xhr) {
            table_user.ajax.reload();
            return false;
        }

    }); 

    $('#cancel_update').click(function(event) {
        event.preventDefault();
        $('#customer_form_modal').modal('hide');
    });
//Ajax form submit end
</script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->