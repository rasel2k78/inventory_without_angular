<?php
/**
 * All sell due list Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * This controller show all due sell lists in a AJAX datatable. People can check details of individual sell and payment made the due amount.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/
class Sell_list_due extends CI_Controller
{
    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
      parent::__construct();

      $this->load->model('Inventory_model');
      $this->load->model('Bank_model');
      $this->load->model('User_access_check_model');
      $cookie = $this->input->cookie('language', true);
      $this->lang->load('sell_due_list_lang', $cookie);
      $this->lang->load('access_message_lang', $cookie);
      $this->lang->load('left_side_nev_lang', $cookie);

      $user_id = $this->session->userdata('user_id');
      if ($user_id == null) {

        redirect('Login', 'refresh');
      }
    }
    /**
     * This function is used for loading a AJAX datatable of all due sells.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
      load_header();
      $user_id_logged = $this->session->userdata('user_id');
      $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $this->load->view('sell/add_sell_list_due_form');
      }
      else {
        $this->load->view('access_deny/not_permitted');
      }
      load_footer();    
    }
    /**
     * This function is used for AJAX datatable. This function load all data for datatable.
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_sell_list_due_for_datatable()
    {
      $filters = $this->input->get();
      $all_data = $this->Inventory_model->all_sell_list_due_info($filters);
      $all_data_without_limit = $this->Inventory_model->all_sell_list_due_info($filters, true);
      $all_data_final = $this->Inventory_model->all_sell_list_due_info($filters, true);
      $output_data=[];
      $output_data["draw"]=$filters['draw'];
      $output_data["recordsTotal"]=$all_data_without_limit;
      $output_data["recordsFiltered"]=$all_data_final;
      $output_data["data"]=$all_data;
      echo json_encode($output_data);
    }
    /**
     * This function collect all informations of individual sell by parameter.
     * 
     * @param string $sell_details_id
     *
     * @return array[] data of individual sell
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_sell_info($sell_details_id)
    {
      $user_id_logged = $this->session->userdata('user_id');
      $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $body_data= array(
          'sell_info_by_id' => $this->Inventory_model->sell_info_by_id($sell_details_id),
          );
        echo json_encode($body_data); 
      }
      else{
        echo json_encode("No Permission");
      }
    }
    /**
     * This function save all sell due paymentmade informations  in database.
     *
     * @return array[] 
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_due_paymentmade()
    {

      userActivityTracking();

      $output=array();
      $output['success'] = 1;
      $output['error'] = array();
      if($this->input->post('due_paymentmade_amount', true)==null || !is_numeric($this->input->post('due_paymentmade_amount', true)) ) {
        $output['error']['due_paymentmade_amount'] =  $this->lang->line('validation_amount');
        $output['success'] = 0;
      }
      $sell_details_id= $this->input->post('sell_details_id', true);
      $sell_paymentmade_amount = $this->input->post('due_paymentmade_amount', true);
      $currnet_sell_due = $this->Inventory_model->sell_paid_and_due_amount($sell_details_id);
      if($sell_paymentmade_amount > $currnet_sell_due['due']) {
        $output['error']['due_paymentmade_amount'] =  $this->lang->line('extra_sell_due_amount');
        $output['success'] = 0;
      }
      $payment_type= $this->input->post('payment_type', true);
      $card_voucher_no = $this->input->post('card_payment_voucher_no', true);
      $card_amount = $this->input->post('card_amount', true);
      if($payment_type == 'card' && ($card_voucher_no== null || !is_numeric($card_voucher_no))) {
        $output['error']['card_payment_voucher_no'] =  $this->lang->line('card_voucher_err_msg');
        $output['success'] = 0;
      }        
      if($payment_type == 'cash_card_both' && ($card_voucher_no== null || !is_numeric($card_voucher_no))) {
        $output['error']['card_payment_voucher_no'] =  $this->lang->line('card_voucher_err_msg');
        $output['success'] = 0;
      }
      if($payment_type == 'cash_card_both') {
        if($sell_paymentmade_amount < $card_amount){
          $output['error']['due_paymentmade_amount'] =  $this->lang->line('paymentmade_less_than_card_amt');
          $output['success'] = 0;
        }
      }
      if($payment_type == 'cheque'){
        $cheque_number = $this->input->post('cheque_number', true);
        if($cheque_number == ""){
          $output['error']['cheque_number'] =  $this->lang->line('cheque_number_empty');
          $output['success'] = 0;
        }
        if($this->Inventory_model->cheque_number_availability($cheque_number)){
          $output['error']['cheque_number'] =  $this->lang->line('cheque_number_already_exist');
          $output['success'] = 0;
        }
      }
      $payment_made_amount = $this->input->post('due_paymentmade_amount', true);
      if($payment_type == 'cash_card_both' &&  $card_amount > $payment_made_amount ) {
        $output['error']['card_amount'] =  $this->lang->line('card_amount_larger');
        $output['success'] = 0;
      }
      if($output['success']==0) {
        echo json_encode($output);
        exit();
      }
      else
      {
        $data= array(
         'due_paymentmade_type' => 'sell',
         'buy_sell_id' => $this->input->post('sell_details_id', true),
         'buy_sell_voucher_no' =>$this->input->post('voucher_no', true),
         'due_paymentmade_amount' => $this->input->post('due_paymentmade_amount', true),
         'due_paymentmade_comments' => $this->input->post('due_paymentmade_comments', true),
         'user_id' => $this->session->userdata('user_id'),
         );
        $is_adjust= $this->input->post('adjust_due', TRUE);
        $charge_percent = $this->input->post('card_charge_percent', TRUE);;
        $this->db->trans_begin();
        $sell_details_id= $data['buy_sell_id'];
        $current_paid_and_due = $this->Inventory_model->sell_paid_and_due_amount($sell_details_id);
        if($payment_type == "card"){
          $charge_amt = round(($data['due_paymentmade_amount'] * $charge_percent)/100,2);
        }
        else{
         $charge_amt= 0; 
       }
       if($is_adjust == "not_adjust"){
         $updated_data = array(
           'paid' => $current_paid_and_due['paid'] + $data['due_paymentmade_amount'],
           'due'  => $current_paid_and_due['due'] - $data['due_paymentmade_amount'],
           'card_charge_amt' => $current_paid_and_due['card_charge_amt'] + $charge_amt,
           );
       }
       if($is_adjust == "adjust")
       {
        if($current_paid_and_due['discount_percentage'] == "no")
        {

          $adjusted_amount = $current_paid_and_due['due'] - $data['due_paymentmade_amount'];
          $updated_data = array(
            'discount' => $current_paid_and_due['discount'] + $adjusted_amount,
            'net_payable' => $current_paid_and_due['net_payable'] - $adjusted_amount,
            'paid' => $current_paid_and_due['paid'] + $data['due_paymentmade_amount'],
            'due' => 0,
            'card_charge_amt' => $current_paid_and_due['card_charge_amt'] + $charge_amt,

            );
        }
        if($current_paid_and_due['discount_percentage'] == "yes")
        {
         $adjusted_amount = $current_paid_and_due['due'] - $data['due_paymentmade_amount'];
         $new_netpayable = $current_paid_and_due['net_payable'] - $adjusted_amount;
         $dis_amt = $current_paid_and_due['grand_total']-  $new_netpayable;
         $dis_parcentage = round(($dis_amt*100)/$current_paid_and_due['grand_total'],2);

         $updated_data = array(
          'discount' => $dis_parcentage,
          'net_payable' => $current_paid_and_due['net_payable'] - $adjusted_amount,
          'paid' => $current_paid_and_due['paid'] + $data['due_paymentmade_amount'],
          'due' => 0,
          'card_charge_amt' => $current_paid_and_due['card_charge_amt'] + $charge_amt,
          
          );
       }
       $batta_history = array(
        'buy_sell_id' => $sell_details_id,
        'buy_or_sell' => 'sell',
        'adjusted_amount' => $adjusted_amount,
        'user_id' => $this->session->userdata('user_id'),
        );
       $this->Inventory_model->save_batta_history($batta_history);  
     }
     $this->Inventory_model->update_sell_after_due_paymentmade($sell_details_id, $updated_data);
     $output['data'] = $this->Inventory_model->save_due_paymentmade_info($data);

     if($payment_type == 'cash') {
      $payment_method_details =array(
       'sell_details_id' => $sell_details_id,
       'payment_type' => $this->input->post('payment_type', true),
       'amount' => $this->input->post('due_paymentmade_amount', true),
       'user_id' => $this->session->userdata('user_id'),
       );    
      $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);
      $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
      $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $data['due_paymentmade_amount'];
      $this->Inventory_model->update_total_cashbox_info($final_data);
      $cash_data['cash_type']= "sell_due_paymentmade";
      $cash_data['user_id'] = $this->session->userdata('user_id');
      $cash_data['cash_description'] = $data['due_paymentmade_comments'];
      $cash_data['deposit_withdrawal_amount'] = $data['due_paymentmade_amount'] ;
      $cash_data['buy_or_sell_details_id']= $sell_details_id;
      $this->Inventory_model->save_cash_box_info($cash_data);
          // $paymentmade_data= array(
          //  'due_paymentmade_type' => 'sell',
          //  'buy_sell_id' => $sell_details_id,
          //  'due_paymentmade_amount' => $data['due_paymentmade_amount'],
          //  'user_id' => $this->session->userdata('user_id'),
          //  );
          // $this->Inventory_model->save_due_paymentmade_info($paymentmade_data);
    }
    if($payment_type == 'card'){
      $bank_acc_id= $this->input->post('bank_acc_id', true);
      $curr_bank_account= $this->Inventory_model->get_current_bank_amt_info($bank_acc_id);

      $bank_data= array(
       'buy_sell_and_other_id' => $sell_details_id,
       'cash_type' => 'sell_due_paymentmade',
       'deposit_withdrawal_amount' => $data['due_paymentmade_amount'] - $charge_amt,
       'bank_acc_id' => $bank_acc_id,
       'user_id' => $this->session->userdata('user_id'),
       );
      $this->Inventory_model->save_card_payment_info($bank_data);
      $final_amount['final_amount']=$curr_bank_account['final_amount']+$data['due_paymentmade_amount'] - $charge_amt;
      $this->Inventory_model->update_total_amt_info_of_acc($final_amount,$bank_acc_id);
      $payment_method_details =array(
        'sell_details_id' => $sell_details_id ,
        'payment_type' => $this->input->post('payment_type', true),
        'amount' => $data['due_paymentmade_amount'],
        'card_voucher_or_actual_voucher_number' => $this->input->post('card_payment_voucher_no', true),
        'user_id' => $this->session->userdata('user_id'),
        'card_charge_percent' => $charge_percent,
        );
      $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);  
    }

    if($payment_type == 'cheque'){
     $cheque_data=array(
      'sell_details_id' => $sell_details_id,
      'cheque_number'  => $this->input->post('cheque_number', true),
      'user_id' => $this->session->userdata('user_id'), 
      'cheque_amount' => $data['due_paymentmade_amount'],
      );
     $this->Bank_model->save_pending_cheque_details_info($cheque_data);
     $payment_method_details =array(
       'sell_details_id' => $sell_details_id ,
       'payment_type' => $this->input->post('payment_type', true),
       'amount' => $data['due_paymentmade_amount'],
       'card_voucher_or_actual_voucher_number' => $this->input->post('payment_type', true),
       'user_id' => $this->session->userdata('user_id'),
       );
     $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);  
   }


   if($payment_type == 'cash_card_both') {
    $due_apymentmade_amount = $this->input->post('due_paymentmade_amount', true);
    $payment_by_card = $this->input->post('card_amount', true);
    $cash_payment = $due_apymentmade_amount - $payment_by_card;
    $cashbox_data=array(
      'user_id' => $this->session->userdata('user_id'),
      'deposit_withdrawal_amount' => $cash_payment,
      'cash_type' => 'sell_due_paymentmade',
      'buy_or_sell_details_id' => $sell_details_id,
      );
    $this->Inventory_model->save_cash_box_info($cashbox_data);
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $cash_payment;
    $this->Inventory_model->update_total_cashbox_info($final_data);
    $curr_bank_account= $this->Inventory_model->curr_bank_for_card_receive();
    $bank_acc_id= $curr_bank_account['bank_acc_id'];
    $bank_data= array(
     'buy_sell_and_other_id' => $sell_details_id ,
     'cash_type' => 'sell_due_paymentmade',
     'deposit_withdrawal_amount' => $payment_by_card,
     'bank_acc_id' => $bank_acc_id,
     'user_id' => $this->session->userdata('user_id'),
     );
    $this->Inventory_model->save_card_payment_info($bank_data);
    $final_amount['final_amount']=$curr_bank_account['final_amount']+ $payment_by_card;
    $this->Inventory_model->update_total_amt_info_of_acc($final_amount,$bank_acc_id);
    $payment_method_details =array(
     'sell_details_id' => $sell_details_id ,
     'payment_type' => $this->input->post('payment_type', true),
     'amount' => $data['due_paymentmade_amount'],
     'card_voucher_or_actual_voucher_number' => $this->input->post('card_payment_voucher_no', true),
     'user_id' => $this->session->userdata('user_id'),
     );
    $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);  
  }

  echo json_encode($output);
  if ($this->db->trans_status() === false) {
   $this->db->trans_rollback();
 }
 else
 {
   $this->db->trans_commit();
 }
}
}
    /**
     * Collect customers total payment details for all vouchers.
     *
     * @param string $customers_id
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function get_due_info_by_customer($customers_id)
    {
      $body_data= array(
       'customer_due_by_id' => $this->Inventory_model->details_customer_due_by_id($customers_id),
       );
      echo json_encode($body_data);
    }
/**
 * Bunch payment by customers. Update vouchers due amount at a time. No individual due payment made
 *
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function bunch_payment_by_customer()
{
  userActivityTracking();
  
  $output=array();
  $output['success'] = 1;
  $output['error'] = array();
  if($this->input->post('due_paymentmade_amount', true)==null || !is_numeric($this->input->post('due_paymentmade_amount', true))) {
    $output['error']['bunch_due_paymentmade_amount'] = $this->lang->line('validation_msg');
    $output['success'] = 0;
  }
  $sell_details_id =$this->input->post('sell_details_id', true);
  $customers_id= $this->input->post('customers_id', true);
  $total_due = $this->Inventory_model->customer_payment_history($customers_id);
  $paymentade_amount = $this->input->post('due_paymentmade_amount', true);
  if($paymentade_amount > $total_due['due']) {
    $output['error']['bunch_due_paymentmade_amount'] = $this->lang->line('extra_amount_inserted');
    $output['success'] = 0;
  }
  if($output['success']==0) {
    echo json_encode($output);
    exit();
  }
  else
  {
    $customers_id= $this->input->post('customers_id', true);
    $bunch_amount = $this->input->post('due_paymentmade_amount', true);
    $this->db->trans_begin();
    $data= array(
     'due_paymentmade_amount' => $this->input->post('due_paymentmade_amount', true),
     'due_paymentmade_comments' => trim($this->input->post('due_paymentmade_comments', true)),
     'user_id' => $this->session->userdata('user_id'),
     );
    $all_due_vouchers = $this->Inventory_model->all_due_vouchers_by_customer_id($customers_id);
    foreach ($all_due_vouchers as $v_due_vouchers) {
      $sell_details_id = $v_due_vouchers['sell_details_id'];
      if($bunch_amount >= $v_due_vouchers['due']){
        $updated_data = array(
         'paid' => $v_due_vouchers['paid'] + $v_due_vouchers['due'],
         'due'  => 0,
         );
        $bunch_amount -=$v_due_vouchers['due'];
        $this->Inventory_model->update_sell_after_due_paymentmade($sell_details_id, $updated_data);
        $payment_method_details =array(
         'sell_details_id' => $sell_details_id,
         'payment_type' => 'cash',
         'amount' => $v_due_vouchers['due'],
         'user_id' => $this->session->userdata('user_id'),
         );    
        $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);
        $due_data= array(
         'due_paymentmade_type' => 'sell',
         'buy_sell_id' => $sell_details_id,
         'due_paymentmade_amount' => $v_due_vouchers['due'],
         'user_id' => $this->session->userdata('user_id'),
         );
        $this->Inventory_model->save_due_paymentmade_info($due_data);
        $cash_data['cash_type']= "sell_due_paymentmade";
        $cash_data['user_id'] = $this->session->userdata('user_id');
        $cash_data['deposit_withdrawal_amount'] = $v_due_vouchers['due'];
        $cash_data['buy_or_sell_details_id']= $sell_details_id;
        $this->Inventory_model->save_cash_box_info($cash_data);
      }
      else{
        $updated_data = array(
         'due' => $v_due_vouchers['due'] - $bunch_amount ,
         'paid'  => $v_due_vouchers['paid']+$bunch_amount,
         );
                // $bunch_amount = $v_due_vouchers['due']-$bunch_amount;
        $this->Inventory_model->update_sell_after_due_paymentmade($sell_details_id, $updated_data);
        $payment_method_details =array(
         'sell_details_id' => $sell_details_id,
         'payment_type' => 'cash',
         'amount' => $bunch_amount,
         'user_id' => $this->session->userdata('user_id'),
         );    
        $this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);
        $due_data= array(
          'due_paymentmade_type' => 'sell',
          'buy_sell_id' => $sell_details_id,
          'due_paymentmade_amount' => $bunch_amount,
          'user_id' => $this->session->userdata('user_id'),
          );
        $this->Inventory_model->save_due_paymentmade_info($due_data);  
        $cash_data['cash_type']= "sell_due_paymentmade";
        $cash_data['user_id'] = $this->session->userdata('user_id');
        $cash_data['deposit_withdrawal_amount'] = $bunch_amount;
        $cash_data['buy_or_sell_details_id']= $sell_details_id;
        $this->Inventory_model->save_cash_box_info($cash_data);
        break;
      }
    }
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    $payment_amt = $this->input->post('due_paymentmade_amount', true);
    $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $payment_amt;
    $this->Inventory_model->update_total_cashbox_info($final_data);
    if ($this->db->trans_status() === false)
    {
      $this->db->trans_rollback();
      echo json_encode(array('success' =>'no'));
    }
    else
    {
      $this->db->trans_commit();
      echo json_encode(array('success' => 'yes'));
    }
  }
}

}
/* End of file Sell_list.php */
/* Location: ./application/controllers/Sell_list_due.php */