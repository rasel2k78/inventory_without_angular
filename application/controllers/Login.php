<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Login extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('file');
    $this->load->model('Loginmodel');
    error_reporting(0);
  }


    /**
     * index function is for load login view with language cookie.
     * If cookie is null it will get english for default.
 *
     * @param  null
     * @return string
     * @author Musabbir
     **/
    public function index()
    {

     $agent_check = $_SERVER['HTTP_USER_AGENT'];

     $findme   = 'Trident';
     $pos = strpos($agent_check, $findme);

     // if($pos == true){
     if ($this->input->cookie('language') == null) {
      $cookie = array(
       'name'   => 'language',
       'value'  => 'english',
       'expire' => 10*365*24*3600,
       'secure' => true
       );

      $this->input->set_cookie($cookie);
      $this->lang->load('login_page_lang', $cookie['value']);
      $this->load->view('Login');
    }
    else{

      $this->lang->load('login_page_lang', $cookie['value']);
      $this->load->view('Login');
    }
    // }else{
    //   echo "<h1>ACCESS DENIED</h1>";
    // }
  }


    /**
     * changeLangEng function is for change the cookie variable to English
 *
     * @param  null
     * @return string
     * @author Musabbir 
     **/
    public function changeLangEng()
    {
     $cookie = array(
      'name'   => 'language',
      'value'  => 'english',
      'expire' => 10*365*24*3600,
      );
     $this->input->set_cookie($cookie);
     $this->lang->load('login_page_lang', $cookie['value']);
     $this->load->view('Login');    

   }



    /**
     * changeLangBang function is for change the cookie variable to English
 *
     * @param  null
     * @return string
     * @author Musabbir 
     **/
    public function changeLangBang()
    {
     $cookie = array(
      'name'   => 'language',
      'value'  => 'bangla',
      'expire' => 10*365*24*3600,
      );
     $this->input->set_cookie($cookie);
     $this->lang->load('login_page_lang', $cookie['value']);
     $this->load->view('Login');    
   }


    /**
     * login function is for check user provided data valid or not and if valid then user will able to enter the software
     * 
     * @return string
     * @author Musabbir 
     **/
    public function login()
    {    
     $email = trim($this->input->post("email",true));
     $password = $this->input->post("password",true);

     if($email == "" || $email == null || $password == "" || $password == null) {
      echo "Input fields can't be empty";
    }
    else{

      $emptyUserCheck = $this->Loginmodel->checkEmptyUser();
      if($emptyUserCheck) {

                $result = $this->Loginmodel->login($email, sha1($password));   //check on local database

                // print_r($result);

                if( ($result['email'] == $email || $result['phone_1'] == $email) && $result['password'] == sha1($password)) {

                  $printer_type_info= $this->Loginmodel->get_printer_info();

                  $session_data= array(
                   'email' => $result['email'],
                   'user_id' => $result['user_id'],
                   'user_role' => $result['user_role'],
                   'username' => $result['username'],
                   'image' => $result['image'],
                   'logged_in' =>true,
                   'printer_type' => $printer_type_info['active_printer'],
                   );
                  $this->session->set_userdata($session_data);

                  if ($this->session->userdata('user_role')=="owner" || $this->session->userdata('user_role')=="manager") {
                   echo "2";
                 }
                 else {
                  echo "1";
                }
                exit();
              }else{
                echo "Invalid email or password";
              }

            }else{

              $connected = @fsockopen("www.google.com", 80); 
              if ($connected) {

                $mac_address = $this->getMacAddress();

                $this->load->library('curl');
                
                    $url = "https://registration_check.pos2in.com/index.php/Login";  //check on online database

                    $all_data = array(
                     'email'=>$email,
                     'password'=>$password,
                     'mac_address'=>$mac_address
                     );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $all_data);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $output = curl_exec($ch);
                    curl_close($ch);

                    // $output = $this->curl->simple_post($url, $all_data);


                    if($output == "Sorry something went wrong") {
                     echo "Sorry something went wrong";
                   }else{
                    if(count($output)>0) {

                      $jsonValues = json_decode($output);
                      $owner_name = $jsonValues->owner_name;
                      $owner_email = $jsonValues->owner_email;
                      $local_pin = $jsonValues->local_pin;
                      $shop_id = $jsonValues->shop_id;
                      $folder_name = $jsonValues->folder_name;


                      $shop_name = $jsonValues->shop_name;
                      $shop_address = $jsonValues->shop_address;
                      $vat_reg_no = $jsonValues->vat_reg_no;

                      
                      if($local_pin == "" || $local_pin == null) {
                        echo "Sorry something went wrong";
                      }

                      else if($owner_name == "" && $owner_email == "") {
                       echo "Invalid email or password";
                     }

                     else{
                      $this->edit_store_id($shop_id);
                                    $result = $this->Loginmodel->firstTimeLogin($owner_name, $owner_email, $local_pin);  //If it is first time login then data will be saved on local database which will come from online 
                                    if($result) {
                                      $ownerMail = $this->Loginmodel->getOwnerInfo($owner_email);

                                      $session_data= array(
                                       'email' => $ownerMail['email'],
                                       'user_id' => $ownerMail['user_id'],
                                       'user_role' => $ownerMail['user_role'],
                                       'username' => $ownerMail['username'],
                                       'logged_in' =>true
                                       );
                                      $this->session->set_userdata($session_data);
                                         $this->Loginmodel->setAllAccessForOwner($ownerMail['user_id']); //To set all access for owner

                                         $this->set_folder_name($folder_name);
                                         $this->Loginmodel->insertStoreData($shop_id, $shop_name, $shop_address, $vat_reg_no);

                                         echo "1";

                                       }else{
                                        echo "Invalid email or password";
                                      }
                                    }
                                  }else{
                                   echo "Invalid email or password";
                                 }
                               }
                             }else{
                               echo "Internet connection required for first time log in";
                             }

                           }
                         }
                       }


                       public function getMacAddress()
                       {
                         $pc_name = getenv("username");
                         $myfile = fopen("C:\\pos2inserver\\mac_address.ini", "r") or die("Unable to open file!");
         //$myfile = fopen(getenv("HOMEDRIVE") . getenv("HOMEPATH")."\\Documents\\pos2in\\mac_address.ini", "r") or die("Unable to open file!");
                         $myMac = fgets($myfile);
                         fclose($myfile); 
                         return trim($myMac);
                       }


    /**
     * checkMailForForgetPass is for check user mail if he forget his password. Password will recover via email with a randomly generated string
     * internet connection is required to execute this method successfully
 *
     * @param  null
     * @return string
     * @author Musabbir 
     **/
    public function checkMailForForgetPass()
    {
     $connected = @fsockopen("www.google.com", 80); 
     if ($connected) {
      $email = $this->input->post("email");
      $phone = $this->input->post("recover_by_phone");

      if(($email==""||$email==null) && (strlen($phone)>3)) {
        $this->sendSMSToMobile($phone);
      }
      else if($email==""||$email==null) {
        echo "empty mail";
      }else{
        $result = $this->Loginmodel->checkMailForForgetPass($this->input->post("email"));
        if($result == true) {
                    $new_pass = $this->generateRandomString();  //Generating a new random password
                    $result_create_newpass = $this->Loginmodel->createNewPass($this->input->post("email"), sha1($new_pass));  //Saving the new generated password at database
                    if($result_create_newpass) {
                         // $user_pass = $this->Loginmodel->getUserPass($this->input->post("email")); //Get the new password
                         // $user_pass = $user_pass['password'];

                         $this->load->library('curl');  //Send this password to online controller 'PasswordRecovery/sendMail' and it will send mail to user
                         //$url = "http://registration_check.pos2in.com/index.php/PasswordRecovery/sendMail"; 
                         $url = "https://www.pos2in.com/PassRecoForLocalLogin/sendMail"; 
                         $all_data = array ($this->input->post("email"),$new_pass);

                         $ch = curl_init();
                         curl_setopt($ch, CURLOPT_URL, $url);
                         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                         curl_setopt($ch, CURLOPT_POST, 1);
                         curl_setopt($ch, CURLOPT_POSTFIELDS, $all_data);
                         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                         $output = curl_exec($ch);
                         curl_close($ch);



                         //$output = $this->curl->simple_post($url, $all_data);
                         //if($output){
                         echo 1;
                         exit();
                         //}
                       }else{
                         echo "fail";
                       }


                     }else if($result == false) {
                       echo "fail";
                     }
                   }
                 }else{
                   echo "No net";
                 }

               }

    /**
     * sendSMSToMobile is for check user phone no. if he forget his password. Password will recover via poone number with a randomly generated string
     * internet connection is required to execute this method successfully
 *
     * @param  $phone
     * @return string
     * @author Musabbir 
     **/
    public function sendSMSToMobile($phone)
    {
     $result = $this->Loginmodel->checkPhoneForForgetPass($phone);
     if($result == true) {

            $new_pass = $this->generateRandomString();  //Generating a new random password

            $result_create_newpass = $this->Loginmodel->createNewPassForPhone($phone, sha1($new_pass));  //Saving the new generated password at database
            if($result_create_newpass) {

                //exit();
                // $phone = "8801966662630"; 

                $username = 'raselrider@yahoo.com';                  //Account email created on textlocal
                $hash = '61e4a44ce28ae4d54b356899dc2c9445c480f8ac';  //Hash key provided by textlocal after creating an account


                $numbers = array($phone);
                $sender = urlencode('MAX Group');
                $message = rawurlencode('Your password for pos2in is: '.$new_pass);

                $numbers = implode(',', $numbers);

                $data = array('username' => $username, 'hash' => $hash, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

                $ch = curl_init('http://api.txtlocal.com/send/');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);

                echo $response;

                echo "<script> alert('A message sent ot your phone number.'; </script>";

              }else{
                echo "Something went wrong";
                exit();
              }

            }else{
              echo "Phone number does not exist";
            }
          }


    /**
     * generateRandomString is for create a random string for making a new password
 *
     * @param  null
     * @return string
     * @author Musabbir 
     **/
    public function generateRandomString()
    {
     $length = 8;
     $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
     $randomString = '';
     for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }


    /**
     * edit_store_id is to get store-id from a file and change all store_id column 
     * in database 
 *
     * @param  null
     * @return string
     * @author Musabbir 
     **/
    public function edit_store_id($shop_id)
    {
     $path = base_url();
     $tables =array();
     $store_id = $shop_id;
     $tables = array(
      "account_folder",
      "advance_buy_cart_items",
      "advance_buy_details",
      "benchmark_table",
      "banks",
      "bank_accounts",
      "bank_statement",
      "brands",
      "buy_cart_items",
      "buy_details",
      "cashbox",
      "categories",
      "cheque_details",
      "customers",
      "demage_lost",
      "due_paymentmade",
      "exchange_return_history",
      "expenditures",
      "expenditure_types",
      "final_cashbox_amount",
      "imei_barcode",
      "inventory",
      "items",
      "item_spec_set",
      "loan",
      "lot_size",
      "pages",
      "pages_permission",
      "pages_permission_history",
      "payout_loan",
      "pending_cheque_details",
      "return_from_customer",
      "return_to_vendor",
      "sales_representative",
      "sell_cart_items",
      "sell_details",
      "sell_payment_details",
      "sizes",
      "spec_name_table",
      "spec_set_connector_with_values",
      "spec_value",
      "tax_types",
      "transfer",
      "transfer_details",
      "units",
      "users",
      "vat",
      "vendors",
      );
     foreach($tables as $table)
     {
      $this->Loginmodel->edit_store_id_format($table,$store_id);
      $this->Loginmodel->edit_store_id($table,$store_id);
    }

  }


  public function set_folder_name($folder_name)
  {
         //$query = $this->db->query("ALTER TABLE `users` CHANGE `folder_name` `folder_name` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '$folder_name' ");
    $this->Loginmodel->set_folder_name($folder_name);
    
  }

  function delete_recursively($path,$match)
  {
    static $deleted = 0, 
    $dsize = 0;
    $dirs = glob($path."*");
    $files = glob($path.$match);
    print_r($files);exit;
    foreach($files as $file){
      if(is_file($file)) {
        $deleted_size += filesize($file);
        unlink($file);
        $deleted++;
      }
    }
  }

  public function dump_data()
  {
   $this->load->library('zip');
   $all_tables = $this->Loginmodel->get_table_name();


   foreach ($all_tables as $key => $value)
   {
    $tab = $value['table_name'];   
              //                 //table namew
              //                //$query = $this->db->query("ALTER TABLE $tab  ADD `stores_id` INT NOT NULL DEFAULT 5");        // Add deafult column with value in all column in this loop ...hidden
    passthru("mysqldump  --host=localhost --user=root --password=root --where='stores_id'=5 inventory $tab > tables/$tab-login.sql");
              //                /*if(in_array($value['table_name'], array("sync","tax_types","units","vendors"))){continue;}*/   
  }
  $path = 'tables';
  $this->zip->read_dir($path, false);
  $this->zip->archive('../databaseBackup/my_db.zip');
}



}

/* End of file login.php */
/* Location: ./application/controllers/login.php */