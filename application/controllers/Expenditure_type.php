<?php 

/**
 * Expenditure Type Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for add new expenditure-type, edit expenditure-type , update expenditure-type info or delete expenditure-type info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/

class expenditure_type extends CI_Controller
{

    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/


    public function __construct()
    {
        parent::__construct();

        $this->load->model('Inventory_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('expenditure_type_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        
        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }

    }

    /**
     * This function is used for loading expenditure type form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function index()
    {

        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            //$this->output->cache(1440);
            $this->load->view('expenditure_type/add_expenditure_type_form');

        }
        else {
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();    
    }

    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/


    public function all_expenditure_type_info_for_datatable()
    {        
        $filters = $this->input->get();
        // print_r($filters);
        // exit();
        $this->load->model('Inventory_model');
        $all_data = $this->Inventory_model->all_expenditure_type_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_expenditure_type_info($filters, true);
        $all_data_final = $this->Inventory_model->all_expenditure_type_info($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);

    }


    /**
     * This function collect all informations of individual expenditure type by parameter.
     * 
     * @param string $expenditure_types_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_expenditure_type_info($expenditure_types_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
               'expenditure_type_info_by_id' => $this->Inventory_model->expenditure_type_info_by_id($expenditure_types_id),
               );

            echo json_encode($body_data);
            
        }
        else {
            echo json_encode("No Permisssion");
        }

    }

    /**
     * This function is used for update expenditure type's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function update_expenditure_type_info()
    {
        userActivityTracking();

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('expenditure_types_name_edit', true)==null ||  trim($this->input->post('expenditure_types_name_edit'))=="" ) {
            $output['error']['expenditure_types_name_edit'] = "<?php echo $this->lang->line('validation_name');?>";
            $output['success'] = 0;
        }

        
        if($output['success'] == 1) {
            $expenditure_types_id = $this->input->post('expenditure_types_id', true);
            $data=array(

               'expenditure_types_name' => trim($this->input->post('expenditure_types_name_edit', true)),
               'expenditure_types_description' => trim($this->input->post('expenditure_types_description_edit', true)),
               'user_id' => $this->session->userdata('user_id'),
               );

            if($this->Inventory_model->update_expenditure_type_info($expenditure_types_id, $data)) {
             echo json_encode($output);
         }
         else{

           $output['success']  = 0;
           echo json_encode($output);
       }

   }
   else{

    echo json_encode($output);
}
}

    /**
     * This function save all expense type informations  in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function save_expenditure_type_info()
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $output = array();
            $output['success'] = 1;
            $output['error']= array();

            if($this->input->post('expenditure_types_name') ==null ||  trim($this->input->post('expenditure_types_name'))=="" ) {
                $output['error']['expenditure_types_name']= $this->lang->line('validation_name');;
                $output['success']= 0;

                $output['error']['expenditure_types_name_2']= $this->lang->line('validation_name');;
                $output['success']= 0;

            }

            if($output['success']==0) {
                echo json_encode($output);
                exit();
            }
            else{

                $data= array(

                   'expenditure_types_name' => trim($this->input->post('expenditure_types_name', true)),
                   'expenditure_types_description' => trim($this->input->post('expenditure_types_description', true)),
                   'user_id' => $this->session->userdata('user_id'),
                   );

                $output['data'] = $this->Inventory_model->save_expenditure_type_info($data);
                echo json_encode($output);

            }
        }
        else {
            echo json_encode("No Permisssion");
        }


    }


    /**
     * This function will delete/hide  the expenditure type's information by "expenditure_types_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $expenditure_types_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_expenditure_type($expenditure_types_id)
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            // $brands_id = $this->input->post('brands_id');
            if($this->Inventory_model->delete_expenditure_type_info($expenditure_types_id)) {
                echo "success";    
            }    
            else{
                echo "failed";
            }

        }
        else {
            echo "No Permisssion";
        }

    }
}

/* End of file expenditure_type.php */
/* Location: ./application/controllers/expenditure_type.php */