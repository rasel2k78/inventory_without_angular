<?php
/**
 * Exchange with vendor  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for Exchange items to vendor 
 *
 * @package Controller
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/

class Exchange_with_vendors extends CI_Controller
{
    /**
 * Constructor function load models
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function __construct()
    {
     parent::__construct();
     $this->load->model('Exchange_with_vendor_model');
     $this->load->model('Inventory_model');
     $this->load->model('User_access_check_model');
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('left_side_nev_lang', $cookie);
     $this->lang->load('exchange_with_vendor_form_lang', $cookie);

     $user_id = $this->session->userdata('user_id');
     if ($user_id == null) {

      redirect('Login', 'refresh');
    }
    
  }


    /**
 * loads basic Exchange form
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function index()
    {

     load_header();
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $body_data= array(
       'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
       );

      $this->load->view('exchange/exchange_with_vendor_form', $body_data);
    }
    else
    {
      $this->load->view('access_deny/not_permitted');
    }

    load_footer();    
  }


    /**
 * find voucher informations by voucher
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_voucher_details_info()
    {
          // $this->router->fetch_class(); // class = controller
          // $this->router->fetch_method();
         // $user_id_logged = $this->session->userdata('user_id');
         // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
         // if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
         // {
     $output=array();
     $output['success'] = 1;
     $output['error'] = array();

     if($this->input->post('buy_voucher_no', true)==null) {
      $output['error']['buy_voucher_no'] = "রসিদ নং দিন";
      $output['success'] = 0;
    }

    if($output['success']==0) {
      echo json_encode($output);
      exit();
    }
    else
    {
      $voucher_no = $this->input->post('buy_voucher_no');
      $body_data= array(
       'all_buy_info_by_id' => $this->Exchange_with_vendor_model->get_voucher_info($voucher_no),
       );

      if(!empty($body_data['all_buy_info_by_id'])) {
       echo json_encode($body_data);
     }
     else
     {
      $output['success'] = 0;
      echo json_encode($output);
      exit();
    }

  }
         // }
         // else
         // {
         // 	echo json_encode("No permission");
         // }
}

    /**
 * Search items for exchange
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function search_item_by_name()
    {
     $text = $this->input->post('q');
     echo json_encode($this->Exchange_with_vendor_model->search_item($text));
   }

    /**
 * find current item price from inventory
 *
 * @param  string $items_id
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function find_item_price($items_id)
    {
     $item_with_price_is_exist = $this->Exchange_with_vendor_model->select_item_price_info_row($items_id);
     echo json_encode($item_with_price_is_exist);
   }

    /**
 * check if item exist on inventory
 *
 * @param  string $items_id
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function check_items_on_inventory($items_id)
    {
     $current_items_on_inventory = $this->Exchange_with_vendor_model->items_on_inventory($items_id);
     echo json_encode($current_items_on_inventory);
     exit;
   }

    /**
 * Save exchanged items and add new items with the voucher
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function check_buy_cart_items()
    {
     $voucher_no = $this->input->post('voucher_no');
     $item_spec_set_id = $this->input->post('item_spec_set_id');
     $item_spec_set_id = $this->input->post('item_spec_set_id');
     $check_item_quantity_inventory = $this->Exchange_with_vendor_model->get_item_quantity_inventory($item_spec_set_id);
     echo $check_item_quantity_inventory['quantity'];exit;
   }

    /**
 * Save exchange information
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function save_buy_info() 
    {

     $data = $this->input->post('all_data');
     $error_list = array();
     $ready_data_basic = array();
     $ready_data_basic['voucher_no'] = $data['all_basic_data']['voucher_no'];
     $ready_data_basic['user_id'] = $this->session->userdata('user_id');
     $ready_data_basic['paid'] = $data['all_basic_data']['add_paid'];
     $ready_data_basic['due'] = $data['all_basic_data']['add_due'];
     $ready_data_basic['return_amount'] = $data['all_basic_data']['add_return'];
     $exchanged_items = $data['all_basic_data']['exchanged'];
     $buy_details = $this->Exchange_with_vendor_model->get_buy_details_by_voucher($ready_data_basic['voucher_no']);
     $buy_details_id = $buy_details['buy_details_id'];
     if(($ready_data_basic['due']>0))
     {
      $get_voucher_vendor = $this->Exchange_with_vendor_model->get_buy_vendor($ready_data_basic['voucher_no']);
      if(!$get_voucher_vendor['vendors_id'])
      {
        echo json_encode("No vendor exist");exit;
      }
    }
    $this->db->trans_begin();
    if(!empty($exchanged_items) && !empty($data['item_info']))
    {

      foreach ($data['item_info'] as $key => $value) 
      {
        $value['item_spec_set_id'] ;
        $value['quantity'] ;
        $value['buying_price'] ;
        if($value['quantity'] == null){
          array_push($error_list,'PLease Enter Item Quantity');
        }
        if($value['buying_price'] ==null)
        {
          array_push($error_list,'price not given');
        }
      }

      if(count($error_list)>0)
      {
        echo json_encode($error_list);
        exit();
      }

      $item_data = $data['item_info'];
      $new_items_price = 0;
      $exchanged_items_price = 0;

      foreach ($exchanged_items as $key => $value) 
      {
        $exchanged_items_price += ($value['quantity']*$value['buying_price_final']);
        if($value['item_imei_number'])
        {
          $imei_data = array();
          $imei_data['buy_details_id'] = "";
          $imei_data['imei_barcode'] = $value['item_imei_number'];
          $imei_data['publication_status'] = "deactivated";
          $imei_data['item_spec_set_id'] = $value['item_spec_set_id'];
          $this->Exchange_with_vendor_model->update_imei_barcode($imei_data,$value['item_imei_number'],$value['item_spec_set_id'],$buy_details_id);
        }

        unset($value['buying_price_final']);
        unset($value['item_imei_number']);
        $item_spec_set_id = $value['item_spec_set_id'];
        $find_item_quantity_on_inventory_exchange = $this->Exchange_with_vendor_model->items_on_inventory($item_spec_set_id); 
        $find_item_quantity_on_cart_items = $this->Exchange_with_vendor_model->items_on_cart($buy_details_id,$item_spec_set_id); 

        $update_inventory_data['quantity'] = $find_item_quantity_on_inventory_exchange['quantity'] - $value['quantity'];

        $buy_cart_inventory_update = $this->Exchange_with_vendor_model->update_inventory($update_inventory_data,$item_spec_set_id);
        $update_buy_cart_item_data['quantity'] = $find_item_quantity_on_cart_items['quantity'] - $value['quantity'];
        $update_buy_cart_item_data['sub_total'] = ($find_item_quantity_on_cart_items['quantity'] - $value['quantity'])*$find_item_quantity_on_cart_items['buying_price'];
        $buy_cart_items_update = $this->Exchange_with_vendor_model->update_cart_items($update_buy_cart_item_data,$buy_details_id,$item_spec_set_id); 


        $exchange_hstory_data = array();
        $exchange_hstory_data['voucher_no'] = $ready_data_basic['voucher_no'];
        $exchange_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
        $exchange_hstory_data['type'] = "exchanged_with_vendor";
        $exchange_hstory_data['publication_status'] = "activated";
        $exchange_hstory_data['quantity'] = $value['quantity'];
        $exchange_hstory_data['user_id'] = $this->session->userdata('user_id');
        $add_exchange_history = $this->Exchange_with_vendor_model->save_exchange_history($exchange_hstory_data);
      }

      foreach ($item_data as $key => $value)
      {
        $new_items_price += ($value['quantity']*$value['buying_price'])- $value['new_item_discount'];
        $item_discount = $value['new_item_discount'];
        unset($value['item_name']);
        unset($value['new_item_discount']);
        unset($value['unique_barcode']);
        $insert_data = $value;
        $insert_data['user_id'] = $this->session->userdata('user_id');
        $insert_data['buy_details_id'] = $buy_details_id;
        $find_item_quantity_on_inventory = $this->Exchange_with_vendor_model->items_on_inventory($value['item_spec_set_id']);  
        $insert_data['sub_total'] = ($insert_data['quantity']*$insert_data['buying_price'])-$item_discount;
        $insert_data['discount_amount'] = $item_discount;
        $insert_data['discount_type'] = "amount";
        $this->Exchange_with_vendor_model->add_items_cart($insert_data);

        if($find_item_quantity_on_inventory)
        {
          $update_inventory_data['quantity'] = $find_item_quantity_on_inventory['quantity'] + $value['quantity'];
          $buy_cart_inventory_update = $this->Exchange_with_vendor_model->update_inventory($update_inventory_data,$value['item_spec_set_id']); 
        }
        else
        {
          $insert_inventory_data['quantity'] = $value['quantity'];
          $insert_inventory_data['item_spec_set_id'] = $value['item_spec_set_id'];
          $this->Exchange_with_vendor_model->insert_items_to_inventory($insert_inventory_data);  
        }
        $exchange_hstory_data = array();
        $exchange_hstory_data['voucher_no'] = $ready_data_basic['voucher_no'];
        $exchange_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
        $exchange_hstory_data['type'] = "new_item_with_vendor";
        $exchange_hstory_data['quantity'] = $value['quantity'];
        $exchange_hstory_data['publication_status'] = "activated";
        $exchange_hstory_data['user_id'] = $this->session->userdata('user_id');
        $add_exchange_history = $this->Exchange_with_vendor_model->save_exchange_history($exchange_hstory_data);
      }

      if(array_key_exists("imei_barcode", $data))
      {
        foreach ($data['imei_barcode'] as $arr_imei_bar) 
        {
          foreach ($arr_imei_bar as $val_of_imei)
          {
            $val_of_imei['buy_details_id']= $buy_details_id;
            $val_of_imei['user_id'] = $this->session->userdata('user_id');
            $check_imei_avialability = $this->Inventory_model->check_imei_avilable_info($val_of_imei['imei_barcode']);
            $is_sell = $this->Inventory_model->check_is_sell($val_of_imei['imei_barcode']);
            if(!empty($check_imei_avialability))
            {
             $this->db->trans_rollback();
             echo "duplicate imei inserted";
             return;
           }
           else if (!empty($is_sell))
           {
             $this->db->trans_rollback();
             echo "already sold once and status deactivated";
             return;
           }
           else
           {
            $this->Inventory_model->save_imei_barcode_info($val_of_imei);
          }
        }
      }
    }

    $get_updated_cart_items = $this->Exchange_with_vendor_model->get_buy_cart_items($buy_details_id);
    $new_sub_total = 0;
    foreach ($get_updated_cart_items as $key => $value)
    {
      $new_sub_total+= $value['sub_total'];
    }

    $update_buy_details['grand_total'] = $new_sub_total;
    $update_buy_details['due'] = $ready_data_basic['due'];
    $update_buy_details['discount'] =  ($buy_details['discount']/$buy_details['grand_total']*$new_sub_total);
    $update_buy_details['net_payable'] = $update_buy_details['grand_total'] - $update_buy_details['discount'];
    $update_buy_details['paid'] = $update_buy_details['net_payable']-$ready_data_basic['due'];
    $buy_details_update = $this->Exchange_with_vendor_model->buy_details_update($update_buy_details,$buy_details_id);
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();

    if($ready_data_basic['return_amount']>0)
    {
      $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $ready_data_basic['return_amount'];
    }

    else if($update_buy_details['due']<0 && $update_buy_details['paid']=='')
    {
      $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $ready_data_basic['paid'];
    }
    else
    {
      $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $ready_data_basic['paid'];
    }
    $cashbox_update = $this->Exchange_with_vendor_model->update_final_cashbox_amount($cashbox_amount_update_data);
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    $cash_data['cash_type']= "exchange with vendor";
    $cash_data['user_id'] = $ready_data_basic['user_id'];
    $cash_data['deposit_withdrawal_amount'] = $ready_data_basic['paid'];
    $this->Exchange_with_vendor_model->save_cash_box_info($cash_data);
  }
  if ($this->db->trans_status() === false) {
    $this->db->trans_rollback();
    echo "error transaction";
  }
  else
  {
    $this->db->trans_commit();
    echo "success";
  }
}

}

/* End of file Exchange_with_vendors.php */
/* Location: ./application/controllers/Exchange_with_vendors.php */