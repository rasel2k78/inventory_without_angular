<?php
/**
 * Exchange with customer  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Use this controller for Exchange items to Customer 
 *
 * @package Controller
 * @author Rasel <raselahmed2k7@gmail.com>
 **/


class Exchange_with_customers extends CI_Controller {
/**
 * Constructor function 
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

public function __construct()
{
	parent::__construct();

	$this->load->model('Exchange_with_customer_model');
	$this->load->model('Inventory_model');
	$this->load->model('Transfer_model');
	$this->load->model('User_access_check_model');
	$cookie = $this->input->cookie('language', TRUE);
	$this->lang->load('left_side_nev_lang', $cookie);
	$this->lang->load('exchange_with_customer_form_lang', $cookie);
	userActivityTracking();
	
	$user_id = $this->session->userdata('user_id');
	if ($user_id == NULL) {
		redirect('Login', 'refresh');
	}
	
}

/**
 * loads basic Exchange form
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function index()
{
	load_header();
	$user_id_logged = $this->session->userdata('user_id');
	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
	if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
	{
		$body_data= array(
			'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
			);
		$this->load->view('exchange/exchange_with_customers_form',$body_data);
	}
	else{
		$this->load->view('access_deny/not_permitted');
	}

	load_footer();
}

/**
 * save all exchange history
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function insert_exchange_history()
{
	$cdata['user_id'] = $this->session->userdata('user_id');

	$cdata['sell_voucher_no'] = $this->input->post("sell_voucher_no");
	$cdata['quantity'] = $this->input->post("quantity");
	$cdata['returned_amount'] = $this->input->post("returned_amount");
	print_r($cdata);
}

/**
 * Get inventory of an item
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function check_buy_cart_items()
{
	$voucher_no = $this->input->post('voucher_no');
	$item_spec_set_id = $this->input->post('item_spec_set_id');
	$check_item_quantity_inventory = $this->Exchange_with_customer_model->get_item_quantity_inventory($item_spec_set_id);
	echo $check_item_quantity_inventory['quantity'];exit;
}
/**
 * Check items qauantity from inventory
 * @param string $item_spec_set_id 
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

public function check_items_on_inventory($item_spec_set_id)
{
	$current_items_on_inventory = $this->Exchange_with_customer_model->get_items_inventory($item_spec_set_id);
	echo json_encode($current_items_on_inventory);
	exit;
}

/**
 * get all information with the given voucher number
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_voucher_details_info()
{
	// $user_id_logged = $this->session->userdata('user_id');
	// $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
	// if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
	// {
	
	$voucher_search = array();
	$output=array();
	$output['success'] = 1;
	$voucher_search['voucher_no'] = $this->input->post('sell_voucher_no');
	$voucher_search['item_name'] = $this->input->post('item_name');
	$voucher_search['voucher_date'] = $this->input->post('voucher_date');
	$voucher_search['mobile_number'] = $this->input->post('mobile_number');
	if(!empty($voucher_search))
	{
		$body_data= array(
			'all_sell_info_by_id' => $this->Exchange_with_customer_model->get_voucher_info($voucher_search),
			);
		echo json_encode($body_data);	
	}
	else
	{
		$output['success'] = 0;
		echo json_encode($output);exit;
	}	
	// }
	// else
	// {
	// 	echo json_encode("No permission");
	// }
}
/**
 * Get total voucher information
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_voucher_information()
{
	// $user_id_logged = $this->session->userdata('user_id');
	// $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
	// if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
	// {

		//$sell_voucher_no = array();
	$output=array();
	$output['success'] = 1;
	$output['error'] = array();

	$sell_voucher_no = $this->input->post('sell_voucher_no');
	$body_data= array(
		'all_sell_info_by_id' => $this->Exchange_with_customer_model->get_voucher_info_by_id($sell_voucher_no),
		);
	if(!empty($body_data['all_sell_info_by_id']))
	{
		echo json_encode($body_data);
	}
	else
	{
		$output['success'] = 'no items';
		echo json_encode($output);
		exit();
	}


	// }
	// else
	// {
	// 	echo json_encode("No permission");
	// }
}
/**
 * find items by search for select
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function search_item_by_name()
{
	$text = $this->input->post('q');
	$results = $this->Exchange_with_customer_model->search_items($text);
	echo json_encode($results);
}
/**
 * find items current price
 * @param string $item_spec_set_id 
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function find_item_price($item_spec_set_id)
{
	$item_with_price_is_exist = $this->Exchange_with_customer_model->select_item_price_info_row($item_spec_set_id);
	echo json_encode($item_with_price_is_exist);
}
/**
 * save all exchange and new items information and upadte inventory,cashbox
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function save_sell_info() 
{
	$data = $this->input->post('all_data');
	$error_list = array();
	$ready_data_basic = array();
	$ready_data_basic['voucher_no'] = $data['all_basic_data']['voucher_no'];
	$ready_data_basic['user_id'] = $this->session->userdata('user_id');
	//$ready_data_basic['grand_total'] = $data['all_basic_data']['add_grand_total'];
	//$ready_data_basic['net_payable'] = $data['all_basic_data']['net_pay_after_discount'];
	$ready_data_basic['paid'] = $data['all_basic_data']['add_paid'];
	$ready_data_basic['due'] = $data['all_basic_data']['add_due'];
	$ready_data_basic['return_amount'] = $data['all_basic_data']['add_return'];
	$exchanged_items = $data['all_basic_data']['exchanged'];
	$sell_details = $this->Exchange_with_customer_model->get_sell_details($ready_data_basic['voucher_no']); 
	$sell_details_id = $sell_details['sell_details_id'];
	
	if(($ready_data_basic['due']>0))
	{
		$get_voucher_customer = $this->Exchange_with_customer_model->get_sell_customer($ready_data_basic['voucher_no']);
		if(!$get_voucher_customer['customers_id'])
		{
			echo json_encode("No customer exist");exit;
		}
	}

	$this->db->trans_begin();
	if(!empty($exchanged_items) && !empty($data['item_info']))
	{

		foreach ($data['item_info'] as $key => $value) 
		{
			$value['item_spec_set_id'] ;
			$value['quantity'] ;
			$value['selling_price'] ;
			$return_data = $this->Transfer_model->check_items($value);
			
			if($value['quantity'] == null){
				array_push($error_list,'PLease Enter Item Quantity');
			}
			if($value['quantity'] > $return_data['quantity'])
			{
				array_push($error_list,'Item does not have enough quantity');
			}
			if($value['selling_price'] ==null)
			{
				array_push($error_list,'price not given');
			}
		}
		if(count($error_list)>0)
		{
			echo json_encode($error_list);
			exit();
		}

		$item_data = $data['item_info'];
		$new_items_price = 0;
		$exchanged_items_price = 0;

		foreach ($item_data as $key => $value)
		{
			$new_items_price += ($value['quantity']*$value['selling_price'])- $value['new_item_discount'];
			$item_discount = $value['new_item_discount'];
			$imei_barcode_yes = "";
			if($value['item_imei_number'])
			{
				$imei_data = array();
				$imei_data['sell_details_id'] = $sell_details_id;
				$imei_data['imei_barcode'] = $value['item_imei_number'];
				$imei_data['barcode_status'] = "used";
				$imei_data['item_spec_set_id'] = $value['item_spec_set_id'];
				$this->Exchange_with_customer_model->update_imei_barcode($imei_data,$value['item_imei_number'],$value['item_spec_set_id']);
				$imei_barcode_yes = "yes";
			}

			unset($value['item_name']);
			unset($value['new_item_discount']);
			unset($value['item_imei_number']);

			$insert_data = $value;
			$insert_data['user_id'] = $this->session->userdata('user_id');
			$insert_data['sell_details_id'] = $sell_details_id;
			$find_item_quantity_on_inventory = $this->Exchange_with_customer_model->items_on_inventory($value['item_spec_set_id']);  
			$check_cart_item_is_exist = $this->Exchange_with_customer_model->get_sell_cart_items_voucher($sell_details_id,$value['item_spec_set_id']);
			if($check_cart_item_is_exist)
			{
				if($imei_barcode_yes=="yes")
				{
					
					$insert_data['sub_total'] = ($insert_data['quantity']*$insert_data['selling_price'])-$item_discount;
					$insert_data['discount_amount'] = $item_discount;
					$insert_data['discount_type'] = "";
					$this->Exchange_with_customer_model->add_items_cart($insert_data);
				}
				else
				{
					$insert_data['quantity'] = $check_cart_item_is_exist['quantity'] + $value['quantity'];
					$insert_data['sub_total'] =(($check_cart_item_is_exist['quantity'] + $value['quantity'])*$check_cart_item_is_exist['selling_price'])-$item_discount;
					$insert_data['discount_amount'] = $item_discount;
					$insert_data['discount_type'] = "";
					// $insert_data['discount_type'] = "amount";
					$this->Exchange_with_customer_model->update_cart_items_cart_id($insert_data,$check_cart_item_is_exist['sell_cart_items_id']);
				}
			}
			else
			{
				$insert_data['sub_total'] = ($insert_data['quantity']*$insert_data['selling_price'])-$item_discount;
				$insert_data['discount_amount'] = $item_discount;
				$insert_data['discount_type'] = "";
				$this->Exchange_with_customer_model->add_items_cart($insert_data);
			}

			if($find_item_quantity_on_inventory)
			{
				$update_inventory_data['quantity'] = $find_item_quantity_on_inventory['quantity'] - $value['quantity'];
				$sell_cart_inventory_update = $this->Exchange_with_customer_model->update_inventory($update_inventory_data,$value['item_spec_set_id']); //Update inventory with new items
			}
			else
			{
				$insert_inventory_data['quantity'] = $value['quantity'];
				$insert_inventory_data['item_spec_set_id'] = $value['item_spec_set_id'];
				$this->Exchange_with_customer_model->insert_items_to_inventory($insert_inventory_data);  //update inventory with new items if not exist
			}

			$exchange_hstory_data = array();
			$exchange_hstory_data['voucher_no'] = $ready_data_basic['voucher_no'];
			$exchange_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
			$exchange_hstory_data['type'] = "new_item_with_customer";
			$exchange_hstory_data['quantity'] = $value['quantity'];
			$exchange_hstory_data['publication_status'] = "activated";
			$exchange_hstory_data['user_id'] = $this->session->userdata('user_id');
			$add_exchange_history = $this->Exchange_with_customer_model->save_exchange_history($exchange_hstory_data);
		}

		foreach ($exchanged_items as $key => $value) 
		{
			$exchanged_items_price += ($value['quantity']*$value['selling_price_final']);
			$imei_barcode_yes = "";
			if($value['item_imei_number'])
			{
				$imei_data = array();
				$imei_data['sell_details_id'] = "";
				$imei_data['imei_barcode'] = $value['item_imei_number'];
				$imei_data['barcode_status'] = "not_used";
				$imei_data['item_spec_set_id'] = $value['item_spec_set_id'];
				$this->Exchange_with_customer_model->update_imei_barcode($imei_data,$value['item_imei_number'],$value['item_spec_set_id']);
				$imei_barcode_yes = "yes";
			}
			unset($value['selling_price_final']);
			unset($value['item_imei_number']);

			$item_spec_set_id = $value['item_spec_set_id'];
			$find_item_quantity_on_inventory = $this->Exchange_with_customer_model->items_on_inventory($item_spec_set_id); 
			$find_item_quantity_on_cart_items = $this->Exchange_with_customer_model->items_on_cart($sell_details_id,$item_spec_set_id); 


			$update_inventory_data['quantity'] = $find_item_quantity_on_inventory['quantity'] + $value['quantity'];
			$sell_cart_inventory_update = $this->Exchange_with_customer_model->update_inventory($update_inventory_data,$item_spec_set_id);

			if($imei_barcode_yes=="yes")
			{
				$remove_imei_one = $this->Exchange_with_customer_model->delete_imei_from_cart($find_item_quantity_on_cart_items['sell_cart_items_id']);
			}
			else
			{
				$update_sell_cart_item_data['quantity'] = $find_item_quantity_on_cart_items['quantity'] - $value['quantity'];
				$update_sell_cart_item_data['sub_total'] = ($find_item_quantity_on_cart_items['quantity'] - $value['quantity'])*$find_item_quantity_on_cart_items['selling_price'];
				$sell_cart_items_update = $this->Exchange_with_customer_model->update_cart_items($update_sell_cart_item_data,$sell_details_id,$item_spec_set_id);
			}


			$exchange_hstory_data = array();
			$exchange_hstory_data['voucher_no'] = $ready_data_basic['voucher_no'];
			$exchange_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
			$exchange_hstory_data['type'] = "exchanged_with_customer";
			$exchange_hstory_data['publication_status'] = "activated";
			$exchange_hstory_data['quantity'] = $value['quantity'];
			$exchange_hstory_data['user_id'] = $this->session->userdata('user_id');
			$add_exchange_history = $this->Exchange_with_customer_model->save_exchange_history($exchange_hstory_data);
		}

		$get_updated_cart_items = $this->Exchange_with_customer_model->get_sell_cart_items($sell_details_id);
		$new_sub_total = 0;
		foreach ($get_updated_cart_items as $key => $value)
		{
			$new_sub_total+= $value['sub_total'];
		}

		$update_sell_details['grand_total'] = $new_sub_total;
		$update_sell_details['due'] = $ready_data_basic['due'];
		//$update_sell_details['net_payable'] = $update_sell_details['paid']+$ready_data_basic['due'];
		if($sell_details['discount_percentage']=="yes")
		{
			$update_sell_details['discount'] = ($sell_details['discount']);
			$update_sell_details['net_payable']= $update_sell_details['grand_total'] - (($sell_details['discount']*$update_sell_details['grand_total'])/100);
		}
		else
		{
			$update_sell_details['discount'] =  ($sell_details['discount']/$sell_details['grand_total']*$new_sub_total);
			$update_sell_details['net_payable'] = $update_sell_details['grand_total'] - $update_sell_details['discount'];
		}
		$update_sell_details['paid'] = $update_sell_details['net_payable']-$ready_data_basic['due'];
		$sell_details_update = $this->Exchange_with_customer_model->sell_details_update($update_sell_details,$sell_details_id);
		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();

		if($ready_data_basic['return_amount']>0)
		{
			$cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $ready_data_basic['return_amount'];
		}

		else if($update_sell_details['due']<0 && $update_sell_details['paid']=='')
		{
			$cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $ready_data_basic['paid'];
		}
		else
		{
			$cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $ready_data_basic['paid'];
		}
		$cashbox_update = $this->Exchange_with_customer_model->update_cashbox_after_exc($cashbox_amount_update_data);

		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
		$cash_data['cash_type']= "exchange with customer";
		$cash_data['user_id'] = $ready_data_basic['user_id'];
		$cash_data['deposit_withdrawal_amount'] = $ready_data_basic['paid'];
		$this->Exchange_with_customer_model->save_cash_box_info($cash_data);
	}

	if ($this->db->trans_status() === FALSE)
	{
		$this->db->trans_rollback();
		echo json_encode("error transection");
	}
	else
	{
		$this->db->trans_commit();
		echo json_encode("success");
	}
}	

}

/* End of file Exchange_with_customers.php */
/* Location: ./application/controllers/Exchange_with_customers.php */