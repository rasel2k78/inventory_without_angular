<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GetItems extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('language_helper');
		$cookie = $this->input->cookie('language', true);
		$this->lang->load('left_side_nev_lang', $cookie);
		$this->load->model('Transfer_model');
	}

	public function index()
	{
		
	}

	public function loginCheck()
	{
		load_header();
		$this->load->view('item/login_for_getitems');			
		load_footer();	
	}

	public function getItems()
	{
		$email = trim($this->input->post('email', TRUE));
		$password = trim($this->input->post('password', TRUE));
		$item_type =  trim($this->input->post('item_type', TRUE));

		if($email == "" || $password == "" || $item_type == ""){
			echo '<script> alert("Fill all the fields correctly"); </script>';
			$this->loginCheck();
		}else{

			$this->load->library('curl');
			$url = "https://crm.pos2in.com/ItemListForLocal";

			$auth_data = array();

			$data['email'] = $email;
			$data['password'] = $password;
			$data['item_type'] = $item_type;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);                    
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$output = curl_exec($ch);

			if($output == "invalid"){
				echo '<script> alert("Invalid email or password !!!"); </script>';
				$this->loginCheck();
			}else{
				$data['items'] = json_decode($output, true);
				load_header();
				$this->load->view('item/get_items', $data);			
				load_footer();	
			}
		}
	}

	public function callInsertItem()
	{
		$adding_item_sets = json_decode($this->input->post('itme_json'), TRUE);

		$this->insertItem($adding_item_sets);
	}

	public function insertItem($adding_item_sets)
	{
		// print_r($adding_item_sets);
		// exit();

		$shop_info = $this->Transfer_model->get_my_shop();
		$user_account_folder = $this->Transfer_model->get_folder_name();
		$spec_names = $adding_item_sets['spec_names'];
		$spec_values = $adding_item_sets['spec_values'];
		$categories = $adding_item_sets['categories'];
		$items_name = $adding_item_sets['items_name'];
		$item_spec_set = $adding_item_sets['item_spec_set'];
		$spec_connector_with_values = $adding_item_sets['spec_connector_with_values'];
		
		// $this->db->trans_begin();

		foreach ($spec_names as $key => $value)
		{
			if($value)
			{
				$value['user_id'] = $this->session->userdata('user_id');
				$value['stores_id'] = $shop_info['shop_id'];
				$check_if_exist = $this->Transfer_model->get_spec_name_id($value['spec_name_id']);
				if($check_if_exist)
				{
					$value['publication_status'] = "activated";
					// echo("spec name");					
					// print_r($value);
					$this->Transfer_model->update_on_pull_shops("spec_name_table",$value['spec_name_id'],"spec_name_id",$value);


				}
				else
				{
					$if_exist_spec_name = $this->Transfer_model->get_spec_name_by_name($value['spec_name']);
					if($if_exist_spec_name)
					{
						$tables  = array("spec_name_table","spec_value","spec_set_connector_with_values");
						foreach ($tables as $key => $table) 
						{
							$data['spec_name_id'] = $value['spec_name_id'];
							$update_new_spec_name = $this->Transfer_model->update_spec_names($data,$if_exist_spec_name['spec_name_id'],$table);
						}
					}
					else
					{
						$this->Transfer_model->insert_on_pull_shop("spec_name_table",$value);
					}
				}
			}
		}
		foreach ($spec_values as $key => $value)
		{
			if($value)
			{
				// echo("spec value");					
				// 	print_r($value);

				$value['user_id'] = $this->session->userdata('user_id');
				$value['stores_id'] = $shop_info['shop_id'];
				$check_if_exist = $this->Transfer_model->get_spec_value_id($value['spec_value_id']);
				if($check_if_exist)
				{
					$value['publication_status'] = "activated";
					$this->Transfer_model->update_on_pull_shops("spec_value",$value['spec_value_id'],"spec_value_id",$value);
				}
				else
				{
					$this->Transfer_model->insert_on_pull_shop("spec_value",$value);
				}
			}
		}

		if($categories){
			foreach ($categories as $key => $value)
			{
				if($value)
				{
					// echo("Category");					
					// print_r($value);

					$value['user_id'] = $this->session->userdata('user_id');
					$value['stores_id'] = $shop_info['shop_id'];
					$check_if_exist = $this->Transfer_model->get_category_id($value['categories_id']);
					if($check_if_exist)
					{
						$value['publication_status'] = "activated";
						$this->Transfer_model->update_on_pull_shops("categories",$value['categories_id'],"categories_id",$value);
					}
					else
					{
						$this->Transfer_model->insert_on_pull_shop("categories",$value);
					}
				}
			}
		}
		
		foreach ($items_name as $key => $value)
		{
			if($value)
			{
				// echo("item name");					
				// print_r($value);

				$value['stores_id'] = $shop_info['shop_id'];
				$check_if_exist = $this->Transfer_model->get_items_id($value['items_id']);
				if($check_if_exist)
				{
					$value['publication_status'] = "activated";
					$this->Transfer_model->update_on_pull_shops("items",$value['items_id'],"items_id",$value);
				}
				else
				{
					$this->Transfer_model->insert_on_pull_shop("items",$value);
				}
			}
		}
		foreach ($item_spec_set as $key => $value)
		{
			if($value)
			{
				// echo("item spec set");					
				// 	print_r($value);

				$value['user_id'] = $this->session->userdata('user_id');
				$value['stores_id'] = $shop_info['shop_id'];
				$check_if_exist = $this->Transfer_model->get_item_spec_set_id($value['item_spec_set_id']);
				if($check_if_exist)
				{
					$value['publication_status'] = "activated";
					$this->Transfer_model->update_on_pull_shops("item_spec_set",$value['item_spec_set_id'],"item_spec_set_id",$value);
				}
				else
				{
					$this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
				}
			}
		}
		foreach ($spec_connector_with_values as $key => $value)
		{
			
			if($value)
			{
				// echo("Spec conn with val");					
				// 	print_r($value);

				$value['user_id'] = $this->session->userdata('user_id');
				$value['stores_id'] = $shop_info['shop_id'];
				$check_if_exist = $this->Transfer_model->get_item_spec_connector_id($value['spec_set_connector_id']);
				if($check_if_exist)
				{
					$value['publication_status'] = "activated";
					$this->Transfer_model->update_on_pull_shops("spec_set_connector_with_values",$value['spec_set_connector_id'],"spec_set_connector_id",$value);
				}
				else
				{
					$this->Transfer_model->insert_on_pull_shop("spec_set_connector_with_values",$value);
				}
			}
		}

		// echo $this->db->last_query();
		try{
			
			$this->db->trans_commit();
			echo "success";
		}catch (Exception $e) {
			if($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				log_message('error', sprintf('%s : %s : DB transaction failed. Error no: %s, Error msg:%s, Last query: %s', __CLASS__, __FUNCTION__, $e->getCode(), $e->getMessage(), print_r($this->main_db->last_query(), TRUE)));
				echo "fail";
			}
			
		}
		
	}
}

/* End of file  */
/* Location: ./application/controllers/ */