<?php 
/**
 * Vendor CRUD controller
 * 
 * @author Rasel
 **/
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Vendor class
 *
 * @package default
 * @author  Rasel
 **/

class Vendor extends CI_Controller
{
  public function __construct()
  { 
    parent::__construct();

    $this->load->model('Vendors_model');
    $this->load->library('upload');
    $cookie = $this->input->cookie('language', true);
    $this->lang->load('add_vendor_form_lang', $cookie);
    $this->lang->load('left_side_nev_lang', $cookie);    
    $this->load->model('User_access_check_model');

    $user_id = $this->session->userdata('user_id');
    if ($user_id == null) {

      redirect('Login', 'refresh');
    }
  }

    /**
     * This method is for loading all customer data at initial step
     * 
     * @return View file
     * @author Rasel 
     **/
    public function index()
    {


     load_header();
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $this->load->view('vendor/add_vendor_form');
    }
    else{
      $this->load->view('access_deny/not_permitted');
    }
    load_footer();
  }



    /**
     * This method is for getting all customer info in data-table
     * 
     * @return JSON
     * @author Rasel 
     **/
    public function all_vendor_info_for_datatable()
    {

     $filters = $this->input->get();
     $all_data = $this->Vendors_model->all_vendor_info_for_datatable($filters);
     $all_data_without_limit = $this->Vendors_model->all_vendor_info_for_datatable($filters, true);
     $all_data_final = $this->Vendors_model->all_vendor_info_for_datatable($filters, true);

     $output_data=[];

     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;

     echo json_encode($output_data);

   }

    /**
     * This method is for creating a new customer
     * 
     * @return String
     * @author Rasel 
     **/
    public function createAccount()
    {
      userActivityTracking();

      $udata =array();
      $udata['vendors_name'] = trim($this->input->post("vendors_name"));
      $udata['vendors_email'] = trim($this->input->post("vendors_email"));
      $udata['vendors_present_address'] = trim($this->input->post("vendors_present_address"));
      $udata['vendors_permanent_address'] = trim($this->input->post("vendors_permanent_address"));
      $udata['vendors_phone_1'] = trim($this->input->post("vendors_phone_1"));
      $udata['vendors_phone_2'] = trim($this->input->post("vendors_phone_2"));
      $udata['vendors_national_id'] = trim($this->input->post("vendors_national_id"));
      $udata['user_id'] = $this->session->userdata('user_id');


      if (!empty($_FILES['vendor_image']['name'])) {
        $config['upload_path'] = 'images/vendor_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2000';
        $config['max_width'] = '9999';
        $config['max_height'] = '9999';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('vendor_image')) {
          $error = $this->upload->display_errors();
                     //  exit();
        } else {
          $fdata = $this->upload->data();
          $udata['vendor_image'] = $config['upload_path'].$fdata['file_name'];
        }
      }

      if ($udata['vendors_name'] == '' || $udata['vendors_name'] == null || trim($udata['vendors_name'])== "") {
        echo "Give vendor name";
        exit();
      }
      else if ($udata['vendors_phone_1'] == '' || $udata['vendors_phone_1'] == null || trim($udata['vendors_phone_1'])=="") {
       echo "Give vendor phone1";
       exit();
     }

            // $check_customer_mail = $this->Vendors_model->checkCustomerDuplicacy($udata['customers_email']);
            // if($check_customer_mail){
            // 	echo "Vendor already exist";
            // 	exit(); 
            // }

     $result = $this->Vendors_model->createAccount($udata);
     if($result) {
      echo "success".$result; 
    }
  }


    /**
     * This method is for delete a customer
 *
     * @param  $vendor_id
     * @return String
     * @author Rasel 
     **/
    public function delete_vendor_info($vendor_id)
    {
      userActivityTracking();

      $user_id_logged = $this->session->userdata('user_id');
      $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        if($this->Vendors_model->delete_vendor_info($vendor_id)) {
          echo "Deletion success";    
        }    
        else{
          echo "Deletion failed";
        }
      }
      else
      {
        echo "No permission";
      }

    }

    /**
     * This method is for get a specific customer details by his id
 *
     * @param  $vendor_id
     * @return JSON
     * @author Rasel 
     **/
    public function vendor_info_by_id($vendor_id)
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $body_data= array(
       'vendor_info_by_id' => $this->Vendors_model->vendor_info_by_id($vendor_id)
       );
      echo json_encode($body_data);
    }
    else
    {
      echo json_encode("No permission");
    }
  }



    /**
     * This method is for updating a customer info
 *
     * @param  $customer_id
     * @return String
     * @author Rasel 
     **/
    public function update_vendor_info($customers_id)
    {
      userActivityTracking();
      
      $udata =array();
      $udata['vendors_name'] = trim($this->input->post("vendors_name"));
      $udata['vendors_email'] = $this->input->post("vendors_email");
      $udata['vendors_present_address'] = trim($this->input->post("vendors_present_address"));
      $udata['vendors_permanent_address'] = trim($this->input->post("vendors_permanent_address"));
      $udata['vendors_phone_1'] = trim($this->input->post("vendors_phone_1"));
      $udata['vendors_phone_2'] = trim($this->input->post("vendors_phone_2"));
      $udata['vendors_national_id'] = trim($this->input->post("vendors_national_id"));
      $udata['user_id'] = $this->session->userdata('user_id');


      if (!empty($_FILES['vendor_image']['name'])) {
        $config['upload_path'] = 'images/vendor_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2000';
        $config['max_width'] = '9999';
        $config['max_height'] = '9999';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('vendor_image')) {
          $error = $this->upload->display_errors();
                //echo $error;
                     //  exit();
        } else {
          $fdata = $this->upload->data();
          $udata['vendor_image'] = $config['upload_path'].$fdata['file_name'];
        }
      }


      if ($udata['vendors_name'] == '' || $udata['vendors_name'] == null || trim($udata['vendors_name'])=="") {
        echo "Give vendor name";
        exit();
      }
      else if ($udata['vendors_phone_1'] == '' || $udata['vendors_phone_1'] == null || trim($udata['vendors_phone_1'])=="") {
       echo "Give vendor phone1";
       exit();
     }

     else if($this->Vendors_model->update_vendor_info($customers_id, $udata)) {
      echo "update success";
    }
    else{
      echo "update_fail";    
    }    
  }

}

/* End of file Vendor.php */
/* Location: ./application/controllers/Customers.php */