<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Error_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        load_header();
        $body_data['heading'] = "404 Not found";
        $body_data['message'] = "Sorry you are not permitted here.This should be from owner ";
        $this->load->view('errors/html/error_404', $body_data);
        load_footer();    
    }
}

/* End of file error_controller.php */
/* Location: ./application/controllers/error_controller.php */