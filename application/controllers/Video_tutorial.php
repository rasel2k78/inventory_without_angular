<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Video_tutorial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	 $cookie = $this->input->cookie('language', true);
     $this->lang->load('left_side_nev_lang', $cookie);
     $this->lang->load('video_page_lang', $cookie);
           
	}

	public function index()
	{
		load_header();
        $this->load->view('video_tutorial/video_tutorial_page');
        load_footer();       
	}

}

/* End of file Video_tutorial.php */
/* Location: ./application/controllers/Video_tutorial.php */