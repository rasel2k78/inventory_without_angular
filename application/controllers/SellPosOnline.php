<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SellPosOnline extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Inventory_model');
		$this->load->model('Shop_info_model');
	}

	public function index()
	{
		$sell_details_id = $this->db->select('sell_details_id')->from('sell_details')->order_by('date_created', 'desc')->get()->row_array();

		$sell_details_id = $sell_details_id['sell_details_id'];

		$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_id);
		$customer_info = $this->Inventory_model->getCustomerInfo($sell_details_id);

		$body_data= array(
			'id_voucher' => $sell_details_id,
			'shop_info' => $this->Shop_info_model->getShopInfo(),
			'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
			'customer_info' => $customer_info
			);
		$this->load->view('sell/sell_voucher_thermal', $body_data);
	}

	public function getSellItemData($sell_details_id)
	{
		$this->load->model('Inventory_model');
		echo json_encode($this->Inventory_model->all_sell_info_by_id($sell_details_id));
	}


	public function printFromAllSellList($sell_details_id)
	{
		$sell_details_id = $this->db->select('sell_details_id')->from('sell_details')->where('sell_details_id', $sell_details_id)->get()->row_array();

		$sell_details_id = $sell_details_id['sell_details_id'];

		$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_id);
		$customer_info = $this->Inventory_model->getCustomerInfo($sell_details_id);

		$body_data= array(
			'id_voucher' => $sell_details_id,
			'shop_info' => $this->Shop_info_model->getShopInfo(),
			'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
			'customer_info' => $customer_info
			);
		$this->load->view('sell/sell_voucher_thermal', $body_data);
	}
}

/* End of file  */
/* Location: ./application/controllers/ */