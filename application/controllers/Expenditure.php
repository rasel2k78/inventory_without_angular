<?php

/**
 * Expenditure Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for add new expenditures, edit expenditures , update expenditure's info or delete expenditure's info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/

class Expenditure extends CI_Controller
{

    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/


    public function __construct()
    {
        parent::__construct();

        $this->load->model('Inventory_model');
        $this->load->model('Bank_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('expenditure_type_lang', $cookie);
        $this->lang->load('expenditure_page_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);        
        $this->lang->load('left_side_nev_lang', $cookie);

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {
            redirect('Login', 'refresh');
        }
        // $this->lang->load('expenditure_page_lang','english');
    }


    /**
     * This function is used for loading expenditure form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function index()
    {
        load_header();
        //$this->output->cache(1440);
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $this->load->view('expenditure/add_expenditure_form');
        }
        else {
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();    

    }


    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/


    public function all_expenditure_info_for_datatable()
    {        
        $filters = $this->input->get();
        // print_r($filters);
        
        $all_data = $this->Inventory_model->all_expenditure_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_expenditure_info($filters, true);
        $all_data_final = $this->Inventory_model->all_expenditure_info($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);
        
    }

    /**
     * This function save all expenditure informations to database.
     *
     * @return array[] success or failure
     * @author shoaib <shofik.shoaib@gmail.com>
     **/


    public function save_expenditure_info()
    {

        // $x= $this->input->post();

        // echo '<pre>'; 
        // print_r($x); 
        // echo '</pre>';
        // exit();
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $output = array();
            $output['success'] = 1;
            $output['error']= array();
            if($this->input->post('expenditure_types_id', true) ==null ) {
                $output['error']['expenditure_types_id']= $this->lang->line('validation_type');
                $output['success']= 0;

            }
            if($this->input->post('expenditures_amount', true) ==null || !is_numeric($this->input->post('expenditures_amount', true)) ) {
                $output['error']['expenditures_amount']= $this->lang->line('validation_amount');
                $output['success']= 0;

            }
            if($this->input->post('expenditures_comments', true) ==null ) {
                $output['error']['expenditures_comments']= $this->lang->line('validation_description');
                $output['success']= 0;

            }
            $payment_type= $this->input->post('payment_type', TRUE);;
            $bank_acc_id= $this->input->post('bank_acc_id', TRUE);;
            $chk_number= $this->input->post('cheque_page_num', TRUE);;
            if($payment_type == "cheque" && $bank_acc_id== ""){
                $output['error']['bank_acc_num']= $this->lang->line('validation_bank_acc');
                $output['success']= 0;
            }
            if($payment_type == "cheque" && $chk_number== ""){
                $output['error']['cheque_page_num']= $this->lang->line('validation_chk_num');
                $output['success']= 0;
            }
            $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
            // if($this->input->post('expenditures_amount', true) > $current_cashbox_amount['total_cashbox_amount']) {
            //     $output['error']['expenditures_amount']= $this->lang->line('total_amount_validation');
            //     $output['success']= 0;
            // }

            if($output['success']==0) {
                echo json_encode($output);
                exit();
            }
            else{

                $data= array(
                   'expenditure_types_id' => $this->input->post('expenditure_types_id', true),
                   'payment_type' => $this->input->post('payment_type', TRUE),
                   'expenditures_amount' => $this->input->post('expenditures_amount', true),
                 // 'expenditures_date' => $this->input->post('expenditures_date', TRUE),
                   'expenditures_comments' => $this->input->post('expenditures_comments', true),
                   'user_id' => $this->session->userdata('user_id'),
                   );
                $exp_date = trim($this->input->post('exp_date', true));
                if($exp_date != ""){
                    $data['date_created'] = $exp_date;
                }
                // echo "<pre>";
                // print_r($data);
                // exit();
                $this->db->trans_begin();
                $output['data'] = $this->Inventory_model->save_expenditure_info($data);
                echo json_encode($output);
                $payment_type = $this->input->post('payment_type', TRUE);
                if($payment_type == "cash"){
                    $cash_data['cash_type']= "expense";
                    $cash_data['user_id'] = $data['user_id'];
                    $cash_data['deposit_withdrawal_amount'] = $data['expenditures_amount'] ;
                    $this->Inventory_model->save_cash_box_info($cash_data);
                    $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $cash_data['deposit_withdrawal_amount'];
                    // $this->Inventory_model->update_total_cashbox_info($final_data);
                    $amount['total_cashbox_amount'] = 0 - $data['expenditures_amount'];
                    if($current_cashbox_amount['total_cashbox_amount'] =="" || $current_cashbox_amount['total_cashbox_amount'] == NULL ) {
                       $this->Inventory_model->save_deposit_amount_info($amount);
                   }
                   else{
                       $this->Inventory_model->update_total_cashbox_info($final_data);
                   }
               }
               if($payment_type=="cheque"){
                   $bank_data=array(
                    'deposit_withdrawal_amount' => $data['expenditures_amount'],
                    'cash_type' => 'expense',
                    'user_id' => $this->session->userdata('user_id'),
                    'buy_sell_and_other_id' => $output['data'],
                    'bank_acc_id' => $this->input->post('bank_acc_id', true),
                    );
                   $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);
                   $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_data['bank_acc_id']);
                   $updated_balance['final_amount'] = $curr_bank_amount['final_amount'] - $bank_data['deposit_withdrawal_amount'];
                   $bank_acc_id= $bank_data['bank_acc_id'];
                   $update_bank_amount= $this->Bank_model->update_total_amt_info_of_acc($updated_balance,$bank_acc_id);
                   $cheque_data=array(
                    'buy_sell_or_others_id' => $output['data'],
                    'bank_acc_id'=> $bank_acc_id,
                    'cheque_number' => $this->input->post('cheque_page_num', true),
                    'bank_statement_id' => $bank_statement_id,
                    'user_id' => $this->session->userdata('user_id'),
                    );
                   $this->Bank_model->save_cheque_details_info($cheque_data);
               }
               if ($this->db->trans_status() === false) {
                  $this->db->trans_rollback();
              }
              else
              {
               $this->db->trans_commit();
           }
       }
   }
   else{
    echo json_encode("No Permission");
}
}
    /**
     * This function will delete/hide  the expenditure's information by "expenditures_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $expenditures_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function delete_expenditure($expenditures_id)
    {
        userActivityTracking();
        
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
        {

            $exp_amount= $this->Inventory_model->get_expenditure_amount_by_id($expenditures_id);
            if($exp_amount['payment_type']== null || $exp_amount['payment_type']== 'cash')
            {
                $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
                $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $exp_amount['expenditures_amount'];
                $this->Inventory_model->update_total_cashbox_info($final_data);
            }
            if($exp_amount['payment_type']== 'cheque')
            {
                $bank_acc_id = $exp_amount['bank_acc_id'];
                $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);
                $final_data['final_amount'] = $curr_bank_amount['final_amount']+ $exp_amount['expenditures_amount'];
                $this->Bank_model->update_total_amt_info_of_acc($final_data,$bank_acc_id);
            }
            if($this->Inventory_model->delete_expenditure_info($expenditures_id)) 
            {
                echo "success";    
            }    
            else{
                echo "failed";
            }
        }
        else {
            echo "No Permission";
        }
    }

    /**
     * This function collect all informations of individual expenditure by parameter.
     * 
     * @param string $expenditures_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_expenditure_info($expenditures_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
               'expenditure_info_by_id' => $this->Inventory_model->expenditure_info_by_id($expenditures_id),
               );
            echo json_encode($body_data);
            
        }
        else {
            echo json_encode("No Permission");
        }

    }

    /**
     * This function is used for update expenditure's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function update_expenditure_info()
    {
        userActivityTracking();

        $output = array();
        $output['success'] = 1;
        $output['error']= array();

        if($this->input->post('expenditure_types_id_edit', true) ==null ) {
            $output['error']['expenditure_types_id_edit']= $this->lang->line('validation_type');
            $output['success']= 0;

        }

        if($this->input->post('expenditures_amount_edit', true) ==null ) {
            $output['error']['expenditures_amount_edit']= $this->lang->line('validation_amount');
            $output['success']= 0;

        }

        // if( $this->input->post('expenditures_date_edit', TRUE) ==null )
        // {
        // 	$output['error']['expenditures_date_edit']= "খরচের তারিখ নির্বাচন করুন";
        // 	$output['success']= 0;

        // }

        if($this->input->post('expenditures_comments_edit', true) ==null ) {
            $output['error']['expenditures_comments_edit']= $this->lang->line('validation_description');
            $output['success']= 0;

        }

        if($output['success'] == 1) {

            $expenditures_id = $this->input->post('expenditures_id', true);
            $data=array(
               'expenditure_types_id' => $this->input->post('expenditure_types_id_edit', true),
               'expenditures_amount' => $this->input->post('expenditures_amount_edit', true),
               'expenditures_comments' => $this->input->post('expenditures_comments_edit', true),
               'user_id' => $this->session->userdata('user_id'),
               );
            $exp_date_edit = trim($this->input->post('exp_date_edit', true));
            if($exp_date_edit != ""){
                $data['date_created'] = $exp_date_edit;
            }

            $previous_expenditure_amount = $this->Inventory_model->previous_expenditure_amount_info($expenditures_id);
            $difference_amount =  $data['expenditures_amount'] - $previous_expenditure_amount['expenditures_amount'] ;
            $payment_type= $this->input->post('payment_type_edit', TRUE);
            if($payment_type == "" || $payment_type == "cash"){
                $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
                $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $difference_amount;
                $this->Inventory_model->update_total_cashbox_info($final_data);

                $cash_data['cash_type']= "expense_edited";
                $cash_data['user_id'] = $this->session->userdata('user_id');
                $cash_data['deposit_withdrawal_amount'] = $data['expenditures_amount'] ;
                $this->Inventory_model->save_cash_box_info($cash_data);
            }
            if($payment_type == "cheque"){
                $bank_acc_id = $this->input->post('bank_acc_id_edit', TRUE);
                $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);
                $final_data['final_amount']= $curr_bank_amount['final_amount'] - $difference_amount;
                $this->Bank_model->update_total_amt_info_of_acc($final_data,$bank_acc_id);

                $bank_data=array(
                    'deposit_withdrawal_amount' => $data['expenditures_amount'],
                    'cash_type' => 'expense_edited',
                    'user_id' => $this->session->userdata('user_id'),
                    'buy_sell_and_other_id' => $expenditures_id,
                    'bank_acc_id' => $bank_acc_id,
                    );
                $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);

                $cheque_data['cheque_number'] = $this->input->post('cheque_page_num_edit', TRUE);
                $this->Bank_model->update_cheque_info($expenditures_id,$cheque_data);

            }
            if($this->Inventory_model->update_expenditure_info($expenditures_id, $data)) {
             echo json_encode($output);
         }
         else{

           $output['success']  = 0;
           echo json_encode($output);
       }
   }
   else{

    echo json_encode($output);
}
}

    /**
     * This function is used for boostrap select of expenditures-types's information in database.
     * 
     *@param  string $expenditure_types_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function search_expenditure_type_by_name()
    {
        $text = $this->input->post('q');
        if($text == null || $text=="") {
            echo "input field empty";
        }
        else{
            echo json_encode($this->Inventory_model->search_expenditure($text));
        }
    }

}

/* End of file Expense.php */
/* Location: ./application/controllers/Expense.php */