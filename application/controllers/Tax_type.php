<?php

/**
 * Tax Type Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}


/**
 * Use this controller for add new tax-type, edit tax-type , update tax-type info or delete tax-type info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/

class Tax_type extends CI_Controller
{

    /**
 * This is a constructor function
 * 
 * This load Inventory Model when this controller is called.
 *
 * @return null
 * @author shoaib
    **/

    public function __construct()
    {
     parent::__construct();

     $this->load->model('Inventory_model');
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('left_side_nev_lang', $cookie);
     userActivityTracking();
     
   }

    /**
     * This function is used for loading the tax-type form. 
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function index()
    {
         // $body_data= array(
         // 	//'all_tax_types' => $this->Inventory_model->all_tax_types_info(),
         // 	);
         //$this->output->cache(1440);
     load_header();
     $this->load->view('tax_type/add_tax_type_form');
     load_footer();    
   }

   public function all_tax_type_info_for_datatable()
   {
     $filters = $this->input->get();

     $all_data = $this->Inventory_model->all_tax_type_info($filters);
     $all_data_without_limit = $this->Inventory_model->all_tax_type_info($filters, true);
     $all_data_final = $this->Inventory_model->all_tax_type_info($filters, true);

     $output_data=[];
     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;

     echo json_encode($output_data);
   }

    /**
     * This function collect all informations of individual tay-type by parameter.
     * 
     * @param string $tax_types_id
     *
     * @return array[]
     * @author Rasel <rasel2k78@gmail.com>
     **/

    public function get_tax_type_info($tax_types_id)
    {
      $body_data= array(
       'tax_type_info_by_id' => $this->Inventory_model->tax_type_info_by_id($tax_types_id),
       );
      
      echo json_encode($body_data);
    }


    /**
     * This function is used for update tax-type's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author Rasel <rasel2k78@gmail.com>
    **/

    public function update_tax_type_info()
    {


      $output = array();
      $output['success'] = 1;
      $output['error'] = array();

      if($this->input->post('tax_types_name_edit', true) == null) {
        $output['error']['tax_types_name'] = "ট্যাক্সের ধরনের নাম দিন";
        $output['success'] = 0;
      }
      if($this->input->post('tax_types_percentage_edit', true)== null) {
        $output['error']['tax_types_percentage'] = "ট্যাক্সের পরিমাণ দিন";
        $output['success'] = 0;
      }

      if($output['success'] == 1) {

        $tax_types_id = $this->input->post('tax_types_id', true);
        $data=array(
         'tax_types_name' => $this->input->post('tax_types_name_edit', true),
         'tax_types_percentage' => $this->input->post('tax_types_percentage_edit', true),
         'tax_types_description' => $this->input->post('tax_types_description_edit', true),
         'user_id' => $this->session->userdata('user_id'),
         );

        if($this->Inventory_model->update_tax_types_info($tax_types_id, $data)) {
         echo json_encode($output);
       }
       else{

         $output['success']  = 0;
         echo json_encode($output);
       }
     }
     else{

      echo json_encode($output);
    }

  }

    /**
     * This function save all tax-type informations of tax-type form to database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function save_tax_type_info()
    {

      $output = array();
      $output['success'] = 1;
      $output['error'] = array();

      if($this->input->post('tax_types_name', true) == null) {
        $output['error']['tax_types_name'] = "ট্যাক্সের ধরনের নাম দিন";
        $output['success'] = 0;

      }
      if($this->input->post('tax_types_percentage', true)== null) {
        $output['error']['tax_types_percentage'] = "ট্যাক্সের পরিমাণ দিন";
        $output['success'] = 0;

      }

      if($output['success']== 0) {
        echo json_encode($output);
        exit();

      }
      else{
        $data= array(

         'tax_types_name' => $this->input->post('tax_types_name', true),
         'tax_types_percentage' => $this->input->post('tax_types_percentage', true),
         'tax_types_description' => $this->input->post('tax_types_description', true),
         'user_id' => $this->session->userdata('user_id'),

         );

        $output['data']= $this->Inventory_model->save_tax_type_info($data);
        echo json_encode($output);

      }    
    }

    /**
     * This function will delete/hide  the tax-type's information by "tax-types_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $tax_types_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_tax_type($tax_types_id)
    {

        // $brands_id = $this->input->post('brands_id');

      if($this->Inventory_model->delete_tax_type_info($tax_types_id)) {

        echo "success";    
      }    
      else{

        echo "failed";
      }

    }

  }

  /* End of file Tax_type.php */
/* Location: ./application/controllers/Tax_type.php */