<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Printer_setting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$cookie = $this->input->cookie('language', true);
		$this->lang->load('shop_info_lang', $cookie);
		$this->lang->load('left_side_nev_lang', $cookie);    
		$this->load->model('User_access_check_model');
	}

	public function index()
	{
		load_header();
		$this->load->view('printer_setting');
		load_footer();    
	}

}

/* End of file Printer_setting.php */
/* Location: ./application/controllers/Printer_setting.php */