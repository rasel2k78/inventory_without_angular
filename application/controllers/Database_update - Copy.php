<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
class Database_update extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Database_update_model');
        $this->load->model('Transfer_model');
    }
    public function index()
    {
        $this->db->trans_begin();
        $this->load->dbforge();
        $shop_info = $this->Transfer_model->get_my_shop();
        $shop_id = $shop_info['shop_id'];
        // if ($this->db->table_exists('db_version') )
        // {
        //     $result = $this->Database_update_model->get_db_version();
        //     if(count($result)==0) {
        //         $version = 'db00000000';
        //     }
        //     else
        //     {
        //         $version = $result[count($result)-1]['version_name'];
        //     }
        // }
        // else
        // {
        //     $fields = array(
        //      'version_id' => array(
        //       'type' => 'INT',
        //       'unsigned' => true,
        //       'auto_increment' => true
        //   ),
        //      'version_name' => array(
        //       'type' => 'VARCHAR',
        //       'constraint' => '45',
        //       'unique' => true,
        //   ),
        //      'create_date' => array(
        //       'type' => 'TIMESTAMP'
        //   )
        //  );
        //     $this->dbforge->add_field($fields);
        //     $this->dbforge->add_key('version_id', true);
        //     $this->dbforge->create_table('db_version');
        //     $version = 'db00000000';
        // }

        $db_array = array();

        $dbarray['db201700414'] = "CREATE TABLE IF NOT EXISTS `banks` (
        `banks_id` char(36) NOT NULL,
        `banks_name` varchar(100) NOT NULL,
        `banks_abbe` varchar(45) NOT NULL,
        `banks_description` text,
        `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
        `user_id` char(36) NOT NULL,
        `stores_id` varchar(45) DEFAULT 'change_with_storeid'
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `banks_AFTER_INSERT` AFTER INSERT ON `banks` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('banks', NEW.banks_id, 'insert', NOW(), 'banks_id');CREATE TRIGGER IF NOT EXISTS `banks_AFTER_UPDATE` AFTER UPDATE ON `banks` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('banks', NEW.banks_id, 'update', NOW(), 'banks_id');CREATE TABLE IF NOT EXISTS `bank_accounts` (
        `bank_acc_id` char(36) NOT NULL,
        `bank_acc_num` varchar(100) NOT NULL,
        `final_amount` float NOT NULL DEFAULT '0',
        `bank_acc_branch` varchar(100) NOT NULL,
        `bank_acc_des` text,
        `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `user_id` char(36) NOT NULL,
        `stores_id` varchar(45) DEFAULT 'change_with_storeid',
        `banks_id` char(36) NOT NULL,
        `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
        `selection_status` enum('selected','not_selected') DEFAULT NULL,
        `bank_acc_name` varchar(100) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `bank_accounts_AFTER_INSERT` AFTER INSERT ON `bank_accounts` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('bank_accounts', NEW.bank_acc_id, 'insert', NOW(), 'bank_acc_id');CREATE TRIGGER IF NOT EXISTS `bank_accounts_AFTER_UPDATE` AFTER UPDATE ON `bank_accounts` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('bank_accounts', NEW.bank_acc_id, 'update', NOW(), 'bank_acc_id');CREATE TABLE IF NOT EXISTS `bank_statement` (
        `bank_statement_id` char(36) NOT NULL,
        `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `user_id` char(36) NOT NULL,
        `publication_status` enum('activated','deactivated') DEFAULT 'activated',
        `deposit_withdrawal_amount` float NOT NULL,
        `cash_type` enum('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card') NOT NULL,
        `cash_description` text,
        `stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
        `buy_sell_and_other_id` char(36) DEFAULT NULL,
        `bank_acc_id` char(36) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `bank_statement_AFTER_INSERT` AFTER INSERT ON `bank_statement` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('bank_statement', NEW.bank_statement_id, 'insert', NOW(), 'bank_statement_id');CREATE TRIGGER IF NOT EXISTS `bank_statement_AFTER_UPDATE` AFTER UPDATE ON `bank_statement` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('bank_statement', NEW.bank_statement_id, 'update', NOW(), 'bank_statement_id');CREATE TABLE IF NOT EXISTS `cheque_details` (
        `cheque_details_id` char(36) NOT NULL,
        `buy_sell_or_others_id` char(36) NOT NULL,
        `bank_acc_id` char(36) NOT NULL,
        `cheque_number` varchar(100) NOT NULL,
        `user_id` char(36) NOT NULL,
        `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `stores_id` varchar(45) DEFAULT 'change_with_storeid',
        `publication_status` enum('activated','deactivated') DEFAULT 'activated',
        `bank_statement_id` char(36) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `cheque_details_AFTER_INSERT` AFTER INSERT ON `cheque_details` FOR EACH ROW 
        INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('cheque_details', NEW.cheque_details_id, 'insert', NOW(), 'cheque_details_id');CREATE TRIGGER IF NOT EXISTS `cheque_details_AFTER_UPDATE` AFTER UPDATE ON `cheque_details` FOR EACH ROW 
        INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('cheque_details', NEW.cheque_details_id, 'update', NOW(), 'cheque_details_id');CREATE TABLE IF NOT EXISTS `pending_cheque_details` (
        `pending_cheque_id` char(36) NOT NULL,
        `sell_details_id` char(36) NOT NULL,
        `cheque_number` varchar(100) NOT NULL,
        `user_id` char(36) NOT NULL,
        `stores_id` varchar(45) NOT NULL DEFAULT 'change_with_storeid',
        `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `publication_status` enum('activated','deactivated') DEFAULT 'activated',
        `cheque_status` enum('pending','done') DEFAULT 'pending',
        `cheque_cleared_type` enum('cash','bank_acc_dpst') DEFAULT NULL,
        `bank_acc_id` char(36) DEFAULT NULL,
        `cheque_amount` float NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `pending_cheque_details_AFTER_INSERT` AFTER INSERT ON `pending_cheque_details` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('pending_cheque_details', NEW.pending_cheque_id, 'insert', NOW(), 'pending_cheque_id');CREATE TRIGGER IF NOT EXISTS `pending_cheque_details_AFTER_UPDATE` AFTER UPDATE ON `pending_cheque_details` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('pending_cheque_details', NEW.pending_cheque_id, 'update', NOW(), 'pending_cheque_id');ALTER TABLE `demage_lost` ADD IF NOT EXISTS `demage_lost_type` ENUM('damage','lost') NOT NULL AFTER `publication_status`;ALTER TABLE `demage_lost` ADD IF NOT EXISTS `item_buying_price` FLOAT NOT NULL AFTER `demage_lost_type`;ALTER TABLE `demage_lost` ADD IF NOT EXISTS `sub_total` FLOAT NOT NULL AFTER `demage_lost_type`;ALTER TABLE `shop` ADD IF NOT EXISTS `shop_image` TEXT NOT NULL AFTER `policy_plan`;ALTER TABLE buy_details DROP INDEX IF EXISTS voucher_no;ALTER TABLE `vendors` ADD IF NOT EXISTS `vendors_email` VARCHAR(200) NOT NULL AFTER `vendor_image`;ALTER TABLE `item_spec_set` ADD IF NOT EXISTS `unique_barcode` ENUM('yes','no') NOT NULL AFTER `item_image_thumbnail`;ALTER TABLE `pending_cheque_details` ADD IF NOT EXISTS `cheque_amount` FLOAT NOT NULL AFTER `bank_acc_id`;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `bank_accounts` ADD IF NOT EXISTS `selection_status` ENUM('selected','not_selected') NOT NULL AFTER `final_amount`;ALTER TABLE `bank_accounts` ADD IF NOT EXISTS `bank_acc_name` VARCHAR(100) NOT NULL AFTER `selection_status`;ALTER TABLE `vat` ADD IF NOT EXISTS `selection_status` ENUM('selected','not_selected') NOT NULL AFTER `publication_status`;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;ALTER TABLE `sell_payment_details` CHANGE `payment_type` `payment_type` ENUM('cash','card','cash_card_both','voucher','both_cash','cheque') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `sell_details` ADD IF NOT EXISTS `discount_percentage` ENUM('yes','no') NOT NULL AFTER `vat_amount`;CREATE TRIGGER IF NOT EXISTS `inventory_AFTER_INSERT` AFTER INSERT ON `inventory` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('inventory', NEW.inventory_id, 'insert', NOW(), 'inventory_id');CREATE TRIGGER IF NOT EXISTS `inventory_AFTER_UPDATE` AFTER UPDATE ON `inventory` FOR EACH ROW
        INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('inventory', NEW.inventory_id, 'update', NOW(), 'inventory_id');ALTER TABLE `sell_details` ADD IF NOT EXISTS `discount_percentage` VARCHAR(80) NOT NULL AFTER `vat_amount`;ALTER TABLE `expenditures` ADD IF NOT EXISTS `payment_type` VARCHAR(80) NOT NULL AFTER `stores_id`;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','expense_edited') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL";

        $dbarray['db201700415'] = "ALTER TABLE `sell_details` CHANGE `payment_type` `payment_type` ENUM('cash','card','cash_card_both','cheque') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";


        $dbarray['db201700420'] = "ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card','expense_edited','received') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";

        $dbarray['db201700503'] = "CREATE TABLE IF NOT EXISTS `inventory`.`imei_barcode` (
        `imei_barcode_id` CHAR(36) NOT NULL,
        `user_id` CHAR(36) NOT NULL,
        `date_created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `publication_status` ENUM('activated', 'deactivated') NULL DEFAULT 'activated',
        `barcode_status` ENUM('used', 'not_used') NOT NULL DEFAULT 'not_used',
        `item_spec_set_id` CHAR(36) NOT NULL,
        `imei_barcode` VARCHAR(200) NOT NULL,
        `stores_id` VARCHAR(45) NULL,
        PRIMARY KEY (`imei_barcode_id`),
        UNIQUE INDEX `imei_barcode_UNIQUE` (`imei_barcode` ASC),
        INDEX `fk_imei_barcode_users1_idx` (`user_id` ASC),
        CONSTRAINT `fk_imei_barcode_users1`
        FOREIGN KEY (`user_id`)
        REFERENCES `inventory`.`users` (`user_id`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
        ENGINE = InnoDB;CREATE TRIGGER IF NOT EXISTS `imei_barcode_AFTER_INSERT` AFTER INSERT ON `imei_barcode` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('imei_barcode', NEW.imei_barcode_id, 'insert', NOW(), 'imei_barcode_id');CREATE TRIGGER IF NOT EXISTS `imei_barcode_AFTER_UPDATE` AFTER UPDATE ON `imei_barcode` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('imei_barcode', NEW.imei_barcode_id, 'update', NOW(), 'imei_barcode_id');ALTER TABLE `item_spec_set` ADD IF NOT EXISTS `product_warranty` VARCHAR(45) NOT NULL AFTER `unique_barcode`";

        $dbarray['db201700506'] = "ALTER TABLE `imei_barcode` ADD IF NOT EXISTS `sell_details_id` CHAR(36) NOT NULL AFTER `stores_id`;ALTER TABLE `transfer` CHANGE IF EXISTS `price` `buying_price` FLOAT(11) NOT NULL;ALTER TABLE `transfer` ADD IF NOT EXISTS `selling_price` FLOAT NOT NULL AFTER `buying_price`;ALTER TABLE `transfer_details` CHANGE `status` `status` ENUM('pending','received','completed','adjusted') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `transfer_details` CHANGE `status` `status` ENUM('pending','received','completed','adjusted','finished') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";

        $dbarray['db201700513'] = "ALTER TABLE `sell_cart_items` ADD IF NOT EXISTS `imei_barcode` VARCHAR(45) NOT NULL AFTER `sub_total`;ALTER TABLE `imei_barcode` ADD IF NOT EXISTS `buy_details_id` CHAR(36) NOT NULL AFTER `sell_details_id`;ALTER TABLE `shop` ADD IF NOT EXISTS `active_printer` ENUM('normal','pos','none') NOT NULL DEFAULT 'none' AFTER `shop_image`;ALTER TABLE `sell_cart_items` ADD IF NOT EXISTS `used_imei_barcode` VARCHAR(100) NOT NULL AFTER `sub_total`;ALTER TABLE `sell_details` ADD IF NOT EXISTS `voucher_unique_barcode` VARCHAR(100) NOT NULL AFTER `vat_amount`;ALTER TABLE `item_spec_set` CHANGE `product_warranty` `product_warranty` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL";

        $dbarray['db201700630'] = "ALTER TABLE `sell_details` ADD IF NOT EXISTS `voucher_unique_barcode` VARCHAR(100) NOT NULL";
        
        $dbarray['db201700940'] = "ALTER TABLE `sell_details` ADD IF NOT EXISTS `custom_voucher_no` VARCHAR(80) NOT NULL;DROP TRIGGER IF EXISTS `demage_lost_AFTER_UPDATE`;CREATE DEFINER=`root`@`localhost` TRIGGER `demage_lost_AFTER_UPDATE` AFTER UPDATE ON `demage_lost` FOR EACH ROW  INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('demage_lost', NEW.demage_lost_id, 'update', NOW(), 'demage_lost_id');ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";


        $dbarray['db201700950'] = "CREATE TABLE IF NOT EXISTS `exchange_return_history` (
        `id` int(11) NOT NULL,
        `voucher_no` varchar(200) NOT NULL,
        `type` enum('exchanged_with_vendor','exchanged_with_customer','new_item_with_vendor','new_item_with_customer','returned_with_vendor','returned_with_customer') NOT NULL,
        `item_spec_set_id` varchar(200) NOT NULL,
        `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `user_id` char(36) NOT NULL,
        `publication_status` enum('activated','deactivated') NOT NULL,
        `stores_id` char(36) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;ALTER TABLE `exchange_return_history` ADD UNIQUE KEY IF NOT EXISTS `id_2` (`id`);ALTER TABLE `exchange_return_history` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";


    $dbarray['db201700996'] = "ALTER TABLE imei_barcode DROP INDEX IF EXISTS imei_barcode;INSERT IGNORE INTO `pages` (`pages_id`, `pages_address`, `stores_id`, `pages_group`, `actions`) VALUES ('7192b80f-d33b-11e7-b235-fcaa14ede82b', 'Transfer_warehouse/index', '300', 'warehouse', 'view');INSERT IGNORE INTO `pages` (`pages_id`, `pages_address`, `stores_id`, `pages_group`, `actions`) VALUES('087216a3-2fd4-11e7-8610-1c1b0d31c7c1', 'Bank_create/index', '', 'Bank', 'View'),('087221b5-2fd4-11e7-8610-1c1b0d31c7c1', 'Bank_acc_create/index', '', 'Bank Accounts', 'View'),('23e82ab1-2fd4-11e7-8610-1c1b0d31c7c1', 'Bank_dpst_wdrl/index', '', 'Bank D/W', 'View');ALTER TABLE `transfer_details` ADD if not EXISTS `transfer_type` ENUM('transfer','warehouse') NOT NULL AFTER `transfer_no`;ALTER TABLE `transfer` ADD if not EXISTS `transfer_type` ENUM('transfer','warehouse') NOT NULL;ALTER TABLE `customers` ADD if not EXISTS `customer_custom_id` VARCHAR(50) NOT NULL;ALTER TABLE `exchange_return_history` ADD if not EXISTS `quantity` INT NOT NULL;ALTER TABLE cheque_details DROP INDEX IF EXISTS cheque_number_UNIQUE;ALTER TABLE `shop` ADD IF NOT EXISTS `stores_id` CHAR(36) NOT NULL;UPDATE shop SET stores_id=shop_id;ALTER TABLE `imei_barcode` CHANGE `barcode_status` `barcode_status` ENUM('used','not_used','demaged') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'not_used';ALTER TABLE `demage_lost` ADD IF NOT EXISTS `imei_barcode_id` CHAR(36) NOT NULL;ALTER TABLE item_spec_set DROP COLUMN IF EXISTS `hisab_id`;ALTER TABLE imei_barcode DROP INDEX IF EXISTS imei_barcode_UNIQUE;ALTER TABLE `sell_payment_details` ADD IF NOT EXISTS `card_charge_percent` FLOAT NULL;ALTER TABLE `banks` ADD IF NOT EXISTS `charge_percentage` FLOAT NOT NULL;UPDATE `pages` SET `pages_group` = 'Warehouse' WHERE `pages`.`pages_id` = '7192b80f-d33b-11e7-b235-fcaa14ede82b';UPDATE `pages` SET `pages_address` = 'Reports/index' WHERE `pages`.`pages_id` = 'fca3f6d2-b917-11e5-a956-eca86bfd5e5e'";

    $dbarray['db201700997'] = "ALTER TABLE `account_folder` ADD IF NOT EXISTS `id` INT NOT NULL;ALTER TABLE `final_cashbox_amount` ADD IF NOT EXISTS `id` INT NOT NULL;ALTER TABLE `account_folder` ADD IF NOT EXISTS `stores_id` CHAR(36) NOT NULL;ALTER TABLE `final_cashbox_amount` ADD IF NOT EXISTS `stores_id` CHAR(36) NOT NULL;UPDATE imei_barcode SET stores_id=$shop_id;ALTER TABLE `imei_barcode` CHANGE `stores_id` `stores_id` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT $shop_id;UPDATE exchange_return_history SET stores_id=$shop_id;ALTER TABLE `exchange_return_history` CHANGE `stores_id` `stores_id` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT $shop_id;UPDATE account_folder SET stores_id=$shop_id;ALTER TABLE `account_folder` CHANGE `stores_id` `stores_id` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT $shop_id;UPDATE final_cashbox_amount SET stores_id=$shop_id;ALTER TABLE `final_cashbox_amount` CHANGE `stores_id` `stores_id` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT $shop_id;CREATE TRIGGER IF NOT EXISTS `account_folder_AFTER_INSERT` AFTER INSERT ON `account_folder` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('account_folder', NEW.id, 'insert', NOW(), 'id');CREATE TRIGGER IF NOT EXISTS `final_cashbox_amount_AFTER_INSERT` AFTER INSERT ON `final_cashbox_amount` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('final_cashbox_amount', NEW.id, 'insert', NOW(), 'id');CREATE TRIGGER IF NOT EXISTS `final_cashbox_amount_AFTER_UPDATE` AFTER UPDATE ON `final_cashbox_amount` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('final_cashbox_amount', NEW.id, 'update', NOW(), 'id');CREATE TRIGGER IF NOT EXISTS `exchange_return_history_AFTER_INSERT` AFTER INSERT ON `exchange_return_history` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('exchange_return_history', NEW.id, 'insert', NOW(), 'id');UPDATE account_folder SET id=$shop_id;UPDATE final_cashbox_amount SET id=$shop_id;ALTER TABLE `item_spec_set` CHANGE `unique_barcode` `unique_barcode` ENUM('yes','no') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'no';ALTER TABLE `transfer` ADD IF NOT EXISTS `whole_sale_price` FLOAT NOT NULL AFTER `transfer_type`;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission','transfer_delete') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";

    $dbarray['db201700998'] = "ALTER TABLE `sell_details` ADD IF NOT EXISTS `card_charge_amt` FLOAT NOT NULL DEFAULT '0' AFTER `custom_voucher_no`";
    
    $dbarray['db201700999'] = "CREATE TABLE IF NOT EXISTS `batta_history` (
    `batta_history_id` char(36) NOT NULL,
    `buy_sell_id` char(36) NOT NULL,
    `buy_or_sell` enum('buy','sell') NOT NULL,
    `adjusted_amount` float NOT NULL,
    `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `user_id` char(36) NOT NULL,
    `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
    `stores_id` varchar(45) NOT NULL,
    PRIMARY KEY (`batta_history_id`),
    KEY `fk_batta_history_users1_idx` (`user_id`),
    CONSTRAINT `fk_batta_history_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TRIGGER IF NOT EXISTS `batta_history_AFTER_INSERT` AFTER INSERT ON `batta_history` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('batta_history', NEW.batta_history_id, 'insert', NOW(), 'batta_history_id');CREATE TRIGGER IF NOT EXISTS `batta_history_AFTER_UPDATE` AFTER UPDATE ON `batta_history` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('batta_history', NEW.batta_history_id, 'update', NOW(), 'batta_history_id');";

$dbarray['db2017001001'] = "ALTER TABLE `bank_accounts` CHANGE `final_amount` `final_amount` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `bank_statement` CHANGE `deposit_withdrawal_amount` `deposit_withdrawal_amount` DOUBLE NOT NULL;ALTER TABLE `batta_history` CHANGE `adjusted_amount` `adjusted_amount` DOUBLE NOT NULL;ALTER TABLE `buy_cart_items` CHANGE `buying_price` `buying_price` DOUBLE NOT NULL;ALTER TABLE `buy_cart_items` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `whole_sale_price` `whole_sale_price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `retail_price` `retail_price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `discount_amount` `discount_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_details` CHANGE `grand_total` `grand_total` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `discount` `discount` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `net_payable` `net_payable` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `paid` `paid` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `due` `due` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `landed_cost` `landed_cost` DOUBLE NULL DEFAULT '0';ALTER TABLE `cashbox` CHANGE `deposit_withdrawal_amount` `deposit_withdrawal_amount` DOUBLE NOT NULL;ALTER TABLE `demage_lost` CHANGE `item_buying_price` `item_buying_price` DOUBLE NOT NULL;ALTER TABLE `demage_lost` CHANGE `sub_total` `sub_total` DOUBLE NOT NULL;ALTER TABLE `due_paymentmade` CHANGE `due_paymentmade_amount` `due_paymentmade_amount` DOUBLE NOT NULL;ALTER TABLE `expenditures` CHANGE `expenditures_amount` `expenditures_amount` DOUBLE NOT NULL;ALTER TABLE `final_cashbox_amount` CHANGE `total_cashbox_amount` `total_cashbox_amount` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `loan` CHANGE `loan_amount` `loan_amount` DOUBLE NOT NULL;ALTER TABLE `loan` CHANGE `total_payout_amount` `total_payout_amount` DOUBLE NOT NULL;ALTER TABLE `loan` CHANGE `actual_loan_taken` `actual_loan_taken` DOUBLE NOT NULL;ALTER TABLE `payout_loan` CHANGE `payout_amount` `payout_amount` DOUBLE NOT NULL;ALTER TABLE `pending_cheque_details` CHANGE `cheque_amount` `cheque_amount` DOUBLE NOT NULL;ALTER TABLE `return_from_customer` CHANGE `returned_amount` `returned_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `return_to_vendor` CHANGE `returned_amount` `returned_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `quantity` `quantity` INT NOT NULL;ALTER TABLE `sell_cart_items` CHANGE `selling_price` `selling_price` DOUBLE NOT NULL;ALTER TABLE `sell_cart_items` CHANGE `discount_amount` `discount_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `tax_percentage` `tax_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `grand_total` `grand_total` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `discount` `discount` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `net_payable` `net_payable` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `paid` `paid` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `due` `due` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `received_money` `received_money` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `return_change` `return_change` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `vat_amount` `vat_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `tax_percentage` `tax_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `card_charge_amt` `card_charge_amt` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `sell_payment_details` CHANGE `amount` `amount` DOUBLE NOT NULL;ALTER TABLE `sell_payment_details` CHANGE `card_charge_percent` `card_charge_percent` DOUBLE NULL DEFAULT NULL;ALTER TABLE `tax_types` CHANGE `tax_types_percentage` `tax_types_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `transfer` CHANGE `price` `price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `transfer_details` CHANGE `total_amount` `total_amount` DOUBLE NOT NULL;ALTER TABLE `vat` CHANGE `vat_percentage` `vat_percentage` DOUBLE NOT NULL;";

$dbarray['db2017001002'] = "ALTER TABLE `transfer` ADD IF NOT EXISTS `imei_barcode` VARCHAR(200) NOT NULL;ALTER TABLE `transfer` ADD IF NOT EXISTS `is_unique` ENUM('yes','no') NOT NULL;ALTER TABLE `imei_barcode` CHANGE `barcode_status` `barcode_status` ENUM('used','not_used','demaged','transfered') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'not_used'";

/** Onlt for local database**/


$dbarray['db2017001004'] = "ALTER TABLE `shop` CHANGE `phone` `phone` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";

$dbarray['db2017001005'] = "CREATE TABLE IF NOT EXISTS `master_log` (       
`master_log_id` int(11) NOT NULL AUTO_INCREMENT,
`url` Text NOT NULL,
`get_data` text NOT NULL,
`post_data` text NOT NULL,
`header_data` text NOT NULL,
`user_id` char(36) NOT NULL,
`ip_address` varchar(100) NOT NULL,
`user_agent` varchar(200) NOT NULL,
`date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`master_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";

$dbarray['db2017001006'] = "DELETE FROM `master_log` WHERE DATE_FORMAT(date_created,'%Y-%m-%d') < '2018-04-10'";

         /*
        Query Ends
        */
        $latest_version = ""; 
        $counter = 0;
        foreach ($dbarray as $key=>$value) {

            if($key>$version) {
                if($value=="") {
                    $latest_version = $key;
                }
                else
                {
                    $queries = explode(';', $value);
                    foreach ($queries as $k => $query)
                    {

                        $this->db->query($query);
                    }
                }
            }

            $counter++;
            if($counter==sizeof($dbarray)) {
                $latest_version = $key;
            }
        }

        if($version==$latest_version) {
            $this->db->trans_rollback();
            //return "Already up to date";
            echo "Already up to date";
            exit;
        }

        $version_data = array(
         'version_name' => $latest_version
     );
        
        $this->Database_update_model->insert_db_version($version_data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }
}
/* End of file  */
/* Location: ./application/controllers/ */