<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class ChangeLanguage extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }

    }


    public function index()
    {

    }

    /**
     * changeLangEng method is for change language to English
     * 
     * @return null
     * @author Musabbir 
     **/
    public function changeLangEng()
    {
         $cookie = array(
          'name'   => 'language',
          'value'  => 'english',
          'expire' => 10*365*24*3600,
          );
         $this->input->set_cookie($cookie);
         $this->header_redirect_permanent($this->agent->referrer().'?'.'english');
         //$this->header_redirect_permanent($this->agent->referrer());

    }


    /**
     * changeLangBang method is for change language to Bangla
     *
     * @return null
     * @author Musabbir 
     **/
    public function changeLangBang()
    {
         $cookie = array(
          'name'   => 'language',
          'value'  => 'bangla',
          'expire' => 10*365*24*3600,
          );
         $this->input->set_cookie($cookie);
         $this->header_redirect_permanent($this->agent->referrer().'?'.'bangla');
         //$this->header_redirect_permanent($this->agent->referrer());
    }


    public function screenShot()
    {
         $file = 'invoice.txt';
        // Open the file to get existing content
         $current = file_get_contents($file);
        // Append a new person to the file
         $current .= "
	MAX POS
	House #34 Road #10 Block #D
	Banani, Dhaka-1213
	Date: 08-02-2016

	Items		Qty 	Price	Discount	Total

	Shirt 		2		1250	0%			2500

	T-Shirt		1		500		0%			500
	-------------------------------------------------------

	3000";
        // Write the contents back to the file
         file_put_contents($file, $current);
    
         $this->header_redirect_permanent($this->agent->referrer().'?'.'bangla');
    }


    public function header_redirect_permanent($url)
    {
         header($_SERVER['SERVER_PROTOCOL'] . ' 301 Moved Permanently', true, 301);
         header('Location: ' . $url);
    }

}

/* End of file ChangeLanguage.php */
/* Location: ./application/controllers/ChangeLanguage.php */