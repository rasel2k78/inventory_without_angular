<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Shop_info extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Shop_info_model');
        $this->load->library('upload');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('shop_info_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);    
        $this->load->model('User_access_check_model');
        userActivityTracking();
        
    }

    public function index()
    {


        load_header();

        $body_data= array(
           'shop_info' => $this->Shop_info_model->getShopInfo(),
           );
        $this->load->view('shop_info', $body_data);
        load_footer();    
    }

    public function update_shop()
    {
        if($this->input->post()) {
            $udata=array();
            $data=array();
            $udata['name'] = $this->input->post("name");
            $udata['phone'] = $this->input->post("phone");
            $udata['address'] = $this->input->post("address");
            $udata['website'] = $this->input->post("website");
            $udata['vat_reg_no'] = $this->input->post("vat_reg_no");
            $udata['shop_id'] = $this->input->post("shop_id");
            $udata['policy_plan'] = $this->input->post("policy_plan");


            if (!empty($_FILES['shop_image']['name'])) {
                $config['upload_path'] = 'images/shop_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000';
                $config['max_width'] = '9999';
                $config['max_height'] = '9999';
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $error = '';
                $fdata = array();
                if (!$this->upload->do_upload('shop_image')) {
                    $error = $this->upload->display_errors();
                    echo $error;

                }
                else
                {
                    $fdata = $this->upload->data();
                    $udata['shop_image'] = $fdata['file_name'];
                    $udata['shop_image'] = $config['upload_path'] . $fdata['file_name'];
                }
            }
            $update_shop_info = $this->Shop_info_model->update_shop_info($udata['shop_id'], $udata);
            echo 'success';
            //$item_insert_id = $this->Item_model->save_item_info($udata);
        }
    }


    public function delete_pcs_file()
    {
        chmod("C:\Windows", 0777);
        unlink("C:\Windows/shop_id.txt");
        $FilePath = "C:\Windows";
        $FileName = "shop_id.txt";

        chdir($FilePath); // Comment this out if you are on the same folder
        chown($FileName, 465); //Insert an Invalid UserId to set to Nobody Owner; for instance 465
        $do = unlink($FileName);

        if($do=="1") { 
            echo "The file was deleted successfully."; 
        } else { echo "There was an error trying to delete the file."; 
    } 
}

public function save_printer_type()
{
    $printer_data = $this->input->post();
    if($this->Shop_info_model->update_printer_info($printer_data)){
        echo json_encode(array('success' => 'yes'));
        $this->session->set_userdata('printer_type', $printer_data['active_printer']);
    }else{
        echo json_encode(array('success' => 'no'));
    }
}

}

/* End of file  */
/* Location: ./application/controllers/ */