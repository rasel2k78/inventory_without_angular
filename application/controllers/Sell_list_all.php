 <?php
 /**
 * All sell list Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
 if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
    /**
 * This controller show all sells in a AJAX datatable. We can check details of individual sell and delete each sell
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
    **/
    class Sell_list_all extends CI_Controller
    {
        /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
        public function __construct()
        {
        	parent::__construct();

        	$this->load->model('Inventory_model');
        	$this->load->model('User_access_check_model');
        	$cookie = $this->input->cookie('language', true);
        	$this->lang->load('sell_all_list_lang', $cookie);
        	$this->lang->load('access_message_lang', $cookie);
        	$this->lang->load('left_side_nev_lang', $cookie);
        	$this->lang->load('add_customer_form_lang', $cookie);

        	$user_id = $this->session->userdata('user_id');
        	if ($user_id == null) {

        		redirect('Login', 'refresh');
        	}
        }
        /**
     * This function is used for loading a datatable and showing all sell information in a AJAX datatable.
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
        public function index()
        {
        	load_header();
        	$user_id_logged = $this->session->userdata('user_id');
        	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        	if($this->User_access_check_model->check_access($user_id_logged, $pages_address))
        	{
        		$store_details = $this->Inventory_model->store_details();
        		$body_data= array(
        			'store_details' => $store_details,
        			);
        		$this->load->view('sell/add_sell_list_all_form', $body_data);
        	}
        	else{
        		$this->load->view('access_deny/not_permitted');
        	}
        	load_footer();    
        }

        /**
     * This function is used for AJAX datatable. This function load all data for datatable.
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
        public function all_sell_list_info_for_datatable()
        {
        	$filters = $this->input->get();
        	$all_data = $this->Inventory_model->sell_list_all_info($filters);
        	$all_data_without_limit = $this->Inventory_model->sell_list_all_info($filters, true);
        	$all_data_final = $this->Inventory_model->sell_list_all_info($filters, true);
        	$output_data=[];
        	$output_data["draw"]=$filters['draw'];
        	$output_data["recordsTotal"]=$all_data_without_limit;
        	$output_data["recordsFiltered"]=$all_data_final;
        	$output_data["data"]=$all_data;
        	echo json_encode($output_data);
        }
        /**
     * This function collect all informations of individual sell by parameter.
     * 
     * @param string $sell_details_id
     *
     * @return array[] data of individual sell
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
        public function get_all_sell_info($sell_details_id)
        {
        	$user_id_logged = $this->session->userdata('user_id');
        	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
        	{
        		$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_id);
        		$body_data= array(
        			'all_sell_info_by_id' => $this->Inventory_model->all_sell_info_by_id($sell_details_id),
        			'customer_info' => $this->Inventory_model->get_customer_info_for_voucher($voucher_details['customers_id']),
        			'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
        			);
        		echo json_encode($body_data); 
        	}
        	else
        	{
        		echo json_encode("No Permission");
        	}
        }
        /**
     * This function will delete/hide  the sell's information by "sell_details_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $sell_details_id
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
        public function delete_sell($sell_details_id)
        {
        	userActivityTracking();

        	$user_id_logged = $this->session->userdata('user_id');
        	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
        	{
        		$get_sell_details_info = $this->Inventory_model->sell_details_info_by_id($sell_details_id);

        		$this->db->trans_begin();
        		$this->Inventory_model->change_imei_barcode_status($sell_details_id);
        		if(empty($get_sell_details_info['adv_sell_details_id']))
        		{
        			$payment_details=$this->Inventory_model->sell_payment_detials($sell_details_id);
        			$paid_amount = $this->Inventory_model->sell_paid_amount_by_id($sell_details_id);
        			if($payment_details['payment_type']== "cash"){
        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_amount['paid'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);
        			}

        			if($payment_details['payment_type']== "card"){
        				$card_details_sell= $this->Inventory_model->get_card_payment_details_for_sell($sell_details_id);
        				$bank_acc_id = $card_details_sell['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$card_details_sell['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        			}

        			if($payment_details['payment_type']== "cheque"){
        				$check_status= $this->Inventory_model->curr_cheque_status($sell_details_id);
        				if($check_status['cheque_status']=="pending"){
        					$this->Inventory_model->delete_pending_cheque($sell_details_id);
        				}
        				if($check_status['cheque_status']=="done"){
        					$chk_clear_type=$this->Inventory_model->cheque_cleared_by($sell_details_id);
        					if($chk_clear_type['cheque_cleared_type']=="cash")
        					{
        						$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        						$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_amount['paid'];
        						$this->Inventory_model->update_total_cashbox_info($final_data);
        					}
        					if($chk_clear_type['cheque_cleared_type']=="bank_acc_dpst"){      
        						$bank_acc_id= $chk_clear_type['bank_acc_id'];
        						$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        						$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$paid_amount['paid'];
        						$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        					}
        				}
        			}

        			if($payment_details['payment_type']== "cash_card_both"){
        				$paid_by_cash = $this->Inventory_model->amt_paid_by_cash($sell_details_id);
        				$paid_by_card_details = $this->Inventory_model->amt_paid_by_card_details($sell_details_id);
        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_by_cash['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);
        				$bank_acc_id= $paid_by_card_details['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$paid_by_card_details['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        			}
        		}
        		else
        		{
        			$sell_payment= $this->Inventory_model->get_sell_payment_details($sell_details_id);
        			if( $sell_payment['payment_type']== "cash" ){

        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $sell_payment['amount'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);

        			}
        			if( $sell_payment['payment_type']== "card" ){

        				$card_details_sell= $this->Inventory_model->get_card_payment_details_for_sell($sell_details_id);
        				$bank_acc_id = $card_details_sell['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$card_details_sell['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);

        			}
        			if($sell_payment['payment_type']== "cheque"){
        				$check_status= $this->Inventory_model->curr_cheque_status($sell_details_id);
        				if($check_status['cheque_status']=="pending"){
        					$this->Inventory_model->delete_pending_cheque($sell_details_id);
        				}
        				if($check_status['cheque_status']=="done"){
        					$chk_clear_type=$this->Inventory_model->cheque_cleared_by($sell_details_id);
        					if($chk_clear_type['cheque_cleared_type']=="cash")
        					{
        						$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        						$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $check_status['cheque_amount'];
        						$this->Inventory_model->update_total_cashbox_info($final_data);
        					}
        					if($chk_clear_type['cheque_cleared_type']=="bank_acc_dpst"){      
        						$bank_acc_id= $chk_clear_type['bank_acc_id'];
        						$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        						$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$check_status['cheque_amount'];
        						$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        					}
        				}
        			}
        			if($sell_payment['payment_type']== "cash_card_both"){
        				$paid_by_cash = $this->Inventory_model->amt_paid_by_cash($sell_details_id);
        				$paid_by_card_details = $this->Inventory_model->amt_paid_by_card_details($sell_details_id);
        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_by_cash['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);
        				$bank_acc_id= $paid_by_card_details['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$paid_by_card_details['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        			}

        			$adv_sell_id = $get_sell_details_info['adv_sell_details_id'];
        			$adv_sell_paymetn_details = $this->Inventory_model->get_adv_sell_payment_details($adv_sell_id);
        			if( $adv_sell_paymetn_details['payment_type']== "cash" ){

        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $sell_payment['amount'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);

        			}
        			if( $adv_sell_paymetn_details['payment_type']== "card" ){
        				$adv_sell_id = $get_sell_details_info['adv_sell_details_id'];
        				$card_details_sell= $this->Inventory_model->get_card_payment_details_for_sell($adv_sell_id);
        				$bank_acc_id = $card_details_sell['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$card_details_sell['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        			}
        			if($adv_sell_paymetn_details['payment_type']== "cheque"){
        				$adv_sell_id = $get_sell_details_info['adv_sell_details_id'];
        				$check_status= $this->Inventory_model->curr_cheque_status($adv_sell_id);
        				if($check_status['cheque_status']=="pending"){
        					$this->Inventory_model->delete_pending_cheque($adv_sell_id);
        				}
        				if($check_status['cheque_status']=="done"){
        					$chk_clear_type=$this->Inventory_model->cheque_cleared_by($adv_sell_id);
        					if($chk_clear_type['cheque_cleared_type']=="cash")
        					{
        						$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        						$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $check_status['cheque_amount'];
        						$this->Inventory_model->update_total_cashbox_info($final_data);
        					}
        					if($chk_clear_type['cheque_cleared_type']=="bank_acc_dpst"){      
        						$bank_acc_id= $chk_clear_type['bank_acc_id'];
        						$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        						$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$check_status['cheque_amount'];
        						$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        					}
        				}
        			}

        			if($adv_sell_paymetn_details['payment_type']== "cash_card_both"){
        				$adv_sell_id = $get_sell_details_info['adv_sell_details_id'];
        				$paid_by_cash = $this->Inventory_model->amt_paid_by_cash($adv_sell_id);
        				$paid_by_card_details = $this->Inventory_model->amt_paid_by_card_details($adv_sell_id);
        				$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_by_cash['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_total_cashbox_info($final_data);
        				$bank_acc_id= $paid_by_card_details['bank_acc_id'];
        				$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
        				$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$paid_by_card_details['deposit_withdrawal_amount'];
        				$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
        			}
        		}

        		$data= $this->Inventory_model->item_quantity_by_sell_details_id($sell_details_id);
                $total_items = count($data);
                $update_item_inventory = $this->Inventory_model->update_inventory_after_sell_delete($data);
                if ($update_item_inventory == $total_items) {
                 if($this->Inventory_model->delete_sell_info($sell_details_id)) {
                    $this->db->trans_commit();
                    echo json_encode(array('success' => true));
                }    
                else{
                    $this->db->trans_rollback();
                    echo json_encode(array('success' => false));
                }    
            }    
            else{
             $this->db->trans_rollback();
             echo json_encode(array('update_error' => true));
         }
     }
     else{
      echo json_encode(array('no_permission' => true));
  }
}

}

/* End of file Sell_list_all.php */
    /* Location: ./application/controllers/Sell_list_all.php */