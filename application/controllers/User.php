<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class User extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
        $this->load->model('User_access_check_model');
        $this->load->model('User_access_model');
        $this->load->library('upload');
        $this->load->helper('language_helper');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('add_user_form_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }

    }

    /**
     * This method is for loading all users data at initial step
     * 
     * @return null
     * @author Musabbir 
     **/
    public function index()
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        load_header();
        $this->load->view('user/add_user_form');
        load_footer();    
    }

    else{
        load_header();
        $this->load->view('access_deny/not_permitted');
        load_footer();
    }

}


public function all_users_info_for_datatable()
{

 $filters = $this->input->get();
 $all_data = $this->User_model->all_users_info($filters);
 $all_data_without_limit = $this->User_model->all_users_info($filters, true);
 $all_data_final = $this->User_model->all_users_info($filters, true);

 $output_data=[];

 $output_data["draw"]=$filters['draw'];
 $output_data["recordsTotal"]=$all_data_without_limit;
 $output_data["recordsFiltered"]=$all_data_final;
 $output_data["data"]=$all_data;
 echo json_encode($output_data);
}

    /**
     * This method is for creating a new user
     * 
     * @return null
     * @author Musabbir 
     **/
    public function createAccount()
    {

     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

        $udata =array();
        $udata['username'] = $this->input->post("username");
        $udata['user_role'] = $this->input->post("user_role");
        $udata['password'] = $this->input->post("password");
        $udata['repass'] = $this->input->post("repass");
        $udata['email'] = $this->input->post("email");
        $udata['phone_1'] = $this->input->post("phone");
        $udata['phone_2'] = $this->input->post("phone2");
        $udata['present_address'] = $this->input->post("present_address");
        $udata['permanent_address'] = $this->input->post("permanent_address");

        if (!empty($_FILES['user_image']['name'])) {
            $config['upload_path'] = 'images/user_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2000';
            $config['max_width'] = '9999';
            $config['max_height'] = '9999';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $error = '';
            $fdata = array();
            if (!$this->upload->do_upload('user_image')) {
                $error = $this->upload->display_errors();
                echo $error;
                        //  exit();
            } else {
                $fdata = $this->upload->data();
                $udata['image'] = $config['upload_path'].$fdata['file_name'];
            }
        }


            // exit();
        if ($udata['username'] == '' || $udata['username'] == null || trim($udata['username'])== "") {
            echo "Username not set";
            exit();
        }

        if ($udata['password'] == '' || $udata['password'] == null || trim($udata['password'])=="") {
                //print_r(json_encode(array('success' => false, 'message' => '')));
            echo "Password can not be Empty";
            exit();
        }

        if(strlen($udata['password'])<4) {
            echo "Password should atleast 4 charecters";
            exit();
        }

        if ($udata['password'] !== $udata['repass']) {
                // print_r(json_encode(array('success' => false, 'message' => 'Password didn\'t match')));
            echo "Password did not match";
            exit();
        } else {
            $udata['password'] = $this->input->post('password');
            unset($udata['repass']);
            $udata['password'] = sha1($udata['password']);
        }

        if($udata['email'] == '' || $udata['email'] == null || trim($udata['email'])=="") {
            echo "Email can not be empty";
            exit();
        }


        if ($udata['phone_1'] == '' || $udata['phone_1'] == null || trim($udata['phone_1'])=="") {
            echo "Mobile no. 1 can not be Empty";
            exit();
        }



            //Email check start
        $data = $this->User_model->select_all_user_email();
        $umail = [];

        foreach ($data as $key => $value) {
            array_push($umail, strtolower($value['email']));

        }

        if (in_array(strtolower($udata['email']), $umail)) {
            echo "Email already exists";
            exit();
        }
            //Email check end


            //Username check start
        $data = $this->User_model->select_all_user_username();
        $uname = [];

        foreach ($data as $key => $value) {
            array_push($uname, strtolower($value['username']));

        }

        if (in_array(strtolower($udata['username']), $uname)) {
                // print_r(json_encode(array('success' => false, 'message' => 'Username already exists')));
            echo "Username already exists";
            exit();
        }
            //Username check end
        $result = $this->User_model->createAccount($udata);
        if($result) {
            echo "success"; 
            $this->User_model->setDefaultAccess($result['user_id'], $result['user_role']);
        }

    }
    else{
        load_header();
        $this->load->view('access_deny/not_permitted');
        load_footer();
    }
}


    /**
     * This method is for delete user
 *
     * @param  $user_id
     * @return null
     * @author Musabbir 
     **/
    public function delete_user($user_id)
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

        $get_user_role = $this->User_access_model->get_user_role_by_id($user_id);
        if($get_user_role['user_role'] == "owner"){
           echo "No permission";
       }else{

         if($this->User_model->delete_user_info($user_id)) {
            echo "Deletion success";    
        }    
        else{
            echo "Deletion failed";
        }    
    }
}
else{
    echo "No permission";
}
}

    /**
     * This method is for get a specific user details by his id
 *
     * @param  $user_id
     * @return null
     * @author Musabbir 
     **/
    public function get_user_info($user_id)
    {

     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $body_data= array(
         'user_info_by_id' => $this->User_model->user_info_by_id($user_id)
         );
        echo json_encode($body_data);

    }else{
        echo json_encode("No permission");
    }
}



    /**
     * This method is for updating a user info
     * 
     * @return null
     * @author Musabbir 
     **/
    public function update_user_info()
    {

     $user_id = $this->input->post('user_id');
     $udata =array();
     $udata['username'] = $this->input->post("username");
     $udata['user_role'] = $this->input->post("user_role");
     $udata['email'] = $this->input->post("email");
     $udata['phone_1'] = $this->input->post("phone");
     $udata['phone_2'] = $this->input->post("phone2");
     $udata['present_address'] = $this->input->post("present_address");
     $udata['permanent_address'] = $this->input->post("permanent_address");

     if($this->session->userdata('user_role')=="owner"){
        $udata['user_role'] = "owner";
    }

    if ($udata['username'] == '' || $udata['username'] == null || trim($udata['username'])== "" ) {
        echo "Username not set";
        exit();
    }

    if ($udata['phone_1'] == '' || $udata['phone_1'] == null || trim($udata['phone_1'])== "") {
        echo "Mobile no. 1 can not be Empty";
        exit();
    }

    if($this->User_model->update_user_info($user_id, $udata)) {
        echo "user_info_updated";
    }
    else{
        echo "update_fail";    
    }    
}


    /**
     * This method is for viewing users personal data
     * 
     * @return null
     * @author Musabbir 
     **/
    public function myProfile()
    {
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('my_profile_lang', $cookie);

     load_header();
     $user_id = $this->session->userdata('user_id');
     $body_data['user_info']= $this->User_model->myProfile_info($user_id);        
     $this->load->view('user/myProfile_form', $body_data);
     load_footer();
 }


    /**
     * This method is for updating users personal data
     * 
     * @return null
     * @author Musabbir 
     **/
    public function updateMyProfile()
    {

     $udata =array();

     if($this->input->post("username")=="" || $this->input->post("username") == null) {
        echo "Username can not be empty";
    }else if($this->input->post("phone1")=="") {
        echo "Mobile can not be empty";
    }else if($this->input->post("email")=="") {
        echo "Email can not be empty";
    }
    
    else{
        $udata['username'] = $this->input->post("username");
        $udata['email'] = $this->input->post("email");
        $udata['phone_1'] = $this->input->post("phone1");
        $udata['phone_2'] = $this->input->post("phone2");
        $udata['present_address'] = $this->input->post("present_address");
        $udata['permanent_address'] = $this->input->post("permanent_address");
        $fdata = array();
        if (!empty($_FILES['user_image']['name'])) 
        {
            $config['upload_path'] = 'images/user_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2000';
            $config['max_width'] = '9999';
            $config['max_height'] = '9999';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $error = '';


            if (!$this->upload->do_upload('user_image')) {
                $error = $this->upload->display_errors();
                
            } 
            else
            {
                $fdata = $this->upload->data();
                $udata['image'] = $config['upload_path'].$fdata['file_name'];
                $this->session->unset_userdata('image');
                $this->session->set_userdata('image', $fdata['file_name']);
            }
        }


        $user_id = $this->session->userdata('user_id');

        $checkUpdate = $this->User_model->updateMyProfile($user_id, $udata);

        if($checkUpdate) 
        {

                //echo $this->session->userdata('image');
            echo "success";
        }
        else
        {
                 // echo '<script type="text/javascript">alert("তথ্য হালনাগাদ হয়নি")</script>'; 
            $this-> myProfile();
        }
    }
}


    /**
     * This method is for changing password
     * 
     * @return null
     * @author Musabbir 
     **/

    public function changePassword()
    {


        if($this->input->post("old_pass") == null || $this->input->post("new_pass") == null || $this->input->post("new_pass_again") == null) {
            echo "Password field can't be empty";
        }else if(strlen($this->input->post("new_pass"))<4) {
            echo "Password should atleast 4 charecters";
        }
        else{


            $user_id = $this->session->userdata('user_id');
            $old_pass = sha1($this->input->post("old_pass"));
            $new_pass = $this->input->post("new_pass");
            $new_pass_again = $this->input->post("new_pass_again");

            $get_old_pass = $this->User_model->getOldPass($user_id);

            if($old_pass == $get_old_pass->password) {
                if($new_pass == $new_pass_again) {

                    $updatePass = $this->User_model->updatePass($user_id, sha1($new_pass));
                    if($updatePass == "success") {
                        echo "Password change successfully";
                    }
                    else
                    {
                        echo "Password didn't changed";
                    }

                }
                else if($new_pass != $new_pass_again) {
                    echo "New password didn't match";
                }
            }
            else
            {
                echo "Old paasword didn't match";            
            }
        }
    }

    /**
     * logOut method is for logging out
     * 
     * @return null
     * @author Musabbir 
     **/
    public function logOut()
    {
     $this->session->sess_destroy();
     redirect('/Login');
 }


}

/* End of file User.php */
/* Location: ./application/controllers/User.php */