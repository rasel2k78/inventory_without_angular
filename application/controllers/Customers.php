<?php 
/**
 * Customer CRUD controller
 * 
 * @author Musabbir
 **/
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Customer class
 *
 * @package default
 * @author  Musabbir
 **/

class Customers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Customers_model');
        $this->load->model('User_access_check_model');
        $this->load->library('upload');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('add_customer_form_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }
        
    }

    /**
     * This method is for loading all customer data at initial step
     * 
     * @return View file
     * @author Musabbir 
     **/
    public function index()
    {
       $user_id_logged = $this->session->userdata('user_id');
       $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
       if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        load_header();
        $this->load->view('customer/add_customers_form');
        load_footer();    
    }else{
        load_header();
        $this->load->view('access_deny/not_permitted');
        load_footer();
    }
}


    /**
     * This method is for getting all customer info in data-table
     * 
     * @return JSON
     * @author Musabbir 
     **/
    public function all_customer_info_for_datatable()
    {

       $filters = $this->input->get();
       $all_data = $this->Customers_model->all_customer_info_for_datatable($filters);
       $all_data_without_limit = $this->Customers_model->all_customer_info_for_datatable($filters, true);
       $all_data_final = $this->Customers_model->all_customer_info_for_datatable($filters, true);

       $output_data=[];

       $output_data["draw"]=$filters['draw'];
       $output_data["recordsTotal"]=$all_data_without_limit;
       $output_data["recordsFiltered"]=$all_data_final;
       $output_data["data"]=$all_data;

       echo json_encode($output_data);

   }

    /**
     * This method is for creating a new customer
     * 
     * @return String
     * @author Musabbir 
     **/
    public function createAccount()
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $udata =array();
            $udata['customer_custom_id'] = trim($this->input->post("customer_custom_id"));
            $udata['customers_name'] = trim($this->input->post("customers_name"));
            $udata['customers_email'] = trim($this->input->post("customers_email"));
            $udata['customers_present_address'] = trim($this->input->post("customers_present_address"));
            $udata['customers_permanent_address'] = trim($this->input->post("customers_permanent_address"));
            $udata['customers_phone_1'] = $this->input->post("customers_phone_1");
            $udata['customers_phone_2'] = $this->input->post("customers_phone_2");
            $udata['customers_national_id'] = trim($this->input->post("customers_national_id"));
            $udata['user_id'] = $this->session->userdata('user_id');


            if (!empty($_FILES['user_image']['name'])) {
                $config['upload_path'] = 'images/user_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000';
                $config['max_width'] = '9999';
                $config['max_height'] = '9999';
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $error = '';
                $fdata = array();
                if (!$this->upload->do_upload('user_image')) {
                    $error = $this->upload->display_errors();
                    echo $error;
                        //  exit();
                } else {
                    $fdata = $this->upload->data();
                    $udata['customers_image'] = $config['upload_path'].$fdata['file_name'];
                }
            }


            if ($udata['customers_name'] == '' || $udata['customers_name'] == null || trim($udata['customers_name'])=="") {
                echo "Customer not set";
                exit();
            }

            if ($udata['customers_phone_1'] == '' || $udata['customers_phone_1'] == null || trim($udata['customers_phone_1'])=="") {
                echo "Phone not set";
                exit();
            }

            // $check_customer_mail = $this->Customers_model->checkCustomerDuplicacy($udata['customers_email']);
            // if($check_customer_mail){
            // 	echo "Customer already exist";
            // 	exit(); 
            // }


            $check_customer_id = $this->Customers_model->check_customer_id_duplicacy($udata['customer_custom_id']);
            if($check_customer_id && $udata['customer_custom_id'] != ""){
                echo "Customer id exist";
                exit(); 
            }

            /*Only This Portion Coded By Shoaib*/
            $customer_name = $udata['customers_name'];
            $customer_phone = $udata['customers_phone_1'];
            $is_customer_available =$this->Customers_model->check_customer_availablity($customer_name,$customer_phone);
            if($is_customer_available != ""){
             echo "Customer Already Created.";
             exit(); 
         }
         /* Shoaib Portion Ends*/

         $result = $this->Customers_model->createAccount($udata);
         if($result) {
            echo "success".$result; 
        }

    }else{

        echo "No permission"; 

    }
}


    /**
     * This method is for delete a customer
 *
     * @param  $user_id
     * @return String
     * @author Musabbir 
     **/
    public function delete_user($user_id)
    {
        userActivityTracking();


        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            if($this->Customers_model->delete_user_info($user_id)) {
                echo "Deletion success";    
            }    
            else{
                echo "Deletion failed";
            }
        }else{
            echo "No permission"; 
        }

    }

    /**
     * This method is for get a specific customer details by his id
 *
     * @param  $customer_id
     * @return JSON
     * @author Musabbir 
     **/
    public function customer_info_by_id($customer_id)
    {
       $body_data= array(
          'customer_info_by_id' => $this->Customers_model->customer_info_by_id($customer_id)
          );
       echo json_encode($body_data);
   }



    /**
     * This method is for updating a customer info
 *
     * @param  $customer_id
     * @return String
     * @author Musabbir 
     **/
    public function update_customer_info($customers_id)
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $udata =array();
            $udata['customer_custom_id'] = trim($this->input->post("customer_custom_id"));
            $udata['customers_name'] = $this->input->post("customers_name");
            $udata['customers_present_address'] = $this->input->post("customers_present_address");
            $udata['customers_permanent_address'] = $this->input->post("customers_permanent_address");
            $udata['customers_phone_1'] = $this->input->post("customers_phone_1");
            $udata['customers_phone_2'] = $this->input->post("customers_phone_2");
            $udata['customers_national_id'] = $this->input->post("customers_national_id");
            $udata['user_id'] = $this->session->userdata('user_id');


            if (!empty($_FILES['user_image']['name'])) {
                $config['upload_path'] = 'images/user_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000';
                $config['max_width'] = '9999';
                $config['max_height'] = '9999';
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $error = '';
                $fdata = array();
                if (!$this->upload->do_upload('user_image')) {
                    $error = $this->upload->display_errors();
                    echo $error;
                        //  exit();
                } else {
                    $fdata = $this->upload->data();
                    $udata['customers_image'] = $config['upload_path'].$fdata['file_name'];
                }
            }


            if ($udata['customers_name'] == '' || $udata['customers_name'] == null || trim($udata['customers_name'])=="") {
                echo "Customer not set";
                exit();
            }
            if ($udata['customers_phone_1'] == '' || $udata['customers_phone_1'] == null || trim($udata['customers_phone_1'])=="") {
                echo "Phone not set";
                exit();
            }
            else if($this->Customers_model->update_customer_info($customers_id, $udata)) {
                echo "success";
            }
            else{
                echo "update_fail";    
            }    
        }else{
            echo "No permission"; 
        }
    }

}

/* End of file Customers.php */
/* Location: ./application/controllers/Customers.php */