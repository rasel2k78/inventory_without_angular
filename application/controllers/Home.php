<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('LoginModel');
        $this->load->library('upload');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }
    }

    public function index()
    {
        $this->output->cache(1440);
        echo "Inventory here";
    }

    public function showAccountForm()
    {
        $this->load->view('CreateAccountView');        
    }

    public function CreateAccount()
    {
        $pic;
        $config['upload_path'] = 'images/user_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2000';
        $config['max_width'] = '9999';
        $config['max_height'] = '9999';
        $config['encrypt_name']=true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('user_image')) {
            $error = $this->upload->display_errors();
            echo $error;
                 //  exit();
        } else {
            $fdata = $this->upload->data();
            $udata['image'] = $config['upload_path'] . $fdata['file_name'];

        }

        $udata['username'] = $this->input->post("username");
        $udata['password'] = $this->input->post("password");
        $udata['repass'] = $this->input->post("repass");
        $udata['email'] = $this->input->post("email");
        $udata['phone_1'] = $this->input->post("phone");
        $udata['phone_2'] = $this->input->post("phone2");
        $udata['present_address'] = $this->input->post("present_address");
        $udata['permanent_address'] = $this->input->post("permanent_address");
        
        
        // exit();
        if ($udata['username'] == '' || $udata['username'] == null) {
            print_r(json_encode(array('success' => false, 'message' => 'Username not set')));
            exit();
        }

        if ($udata['password'] == '' || $udata['password'] == null) {
            print_r(json_encode(array('success' => false, 'message' => 'Password can\'t be Empty')));
            exit();
        }

        if ($udata['password'] !== $udata['repass']) {
            print_r(json_encode(array('success' => false, 'message' => 'Password didn\'t match')));
            exit();
        } else {
            $udata['password'] = sha1($this->input->post('password'));
            unset($udata['repass']);
        }

        $data = $this->LoginModel->select_all_user_username();
        $uname = [];
        
        foreach ($data as $key => $value) {
            array_push($uname, strtolower($value['username']));
            
        }

        if (in_array(strtolower($udata['username']), $uname)) {
            print_r(json_encode(array('success' => false, 'message' => 'Username already exists')));
            exit();
        };

        $result = $this->LoginModel->createAccount($udata);

        if (!$result['success'] == true) {
            $return_data = array('success' => $result['success'], 'message' => 'User added successfully!');
            print_r(json_encode($return_data));

        } else {
            $return_data = array('success' => $result['success'], 'message' => 'User adding failed!');
            print_r(json_encode($return_data));
        }
    }

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */