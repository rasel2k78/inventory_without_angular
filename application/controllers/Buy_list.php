<?php 
/**
 * Buy Due List  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */


if (! defined('BASEPATH')) { exit('No direct script access allowed');
}


/**
 * Use this controller for showing all due purchse list with voucher and other informations. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/

class Buy_list extends CI_Controller
{

    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Inventory_model');
        $this->load->model('Bank_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('buy_due_list_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);


        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {
            redirect('Login', 'refresh');
        }
        
    }

    /**
     * This function is used for loading the due puchase table. 
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $this->load->view('buy_list/add_buy_list_form');
        }
        else{
            $this->load->view('access_deny/not_permitted');
        }
        // $this->output->cache(1440);
        load_footer();    


    }
    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function all_due_buy_info_for_datatable()
    {

        $filters = $this->input->get();

        $all_data = $this->Inventory_model->all_buy_due_list_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_buy_due_list_info($filters, true);
        $all_data_final = $this->Inventory_model->all_buy_due_list_info($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);
    }

    /**
     * This function is used for collecting data of individual buy. 
     * 
     * @param  string $buy_details_id
     * @return array[] all buy informations form database.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function get_buy_info($buy_details_id)
    {

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $body_data= array(
               'buy_info_by_id' => $this->Inventory_model->details_buy_info_by_id($buy_details_id),
               );

            // print_r($body_data);
            echo json_encode($body_data);

        }
        else
        {
            echo json_encode("No Permission");
        }

    }

    /**
     * This function save due paymentade informations to database and update buy details,cashbox,final_cashbox_amount table. 
     *
     * @return array[] success or failure.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function save_due_paymentmade()
    {
        userActivityTracking();
        
        $output=array();
        $output['success'] = 1;
        $output['error'] = array();
        if($this->input->post('due_paymentmade_amount', true)==null || !is_numeric($this->input->post('due_paymentmade_amount', true))) {
            $output['error']['due_paymentmade_amount'] = $this->lang->line('validation_msg');
            $output['success'] = 0;
        }
        $payment_type= $this->input->post('payment_type', TRUE);
        $chk_num=$this->input->post('cheque_number', TRUE);;
        $bank_acc_id=$this->input->post('bank_acc_id', TRUE);;
        $buy_details_id =$this->input->post('buy_details_id', true);
        $paymentade_amount = $this->input->post('due_paymentmade_amount', true);
        $current_due = $this->Inventory_model->current_paid_and_due_amount($buy_details_id);
        if($paymentade_amount > $current_due['due']) {
            $output['error']['due_paymentmade_amount'] = $this->lang->line('extra_amount_inserted');
            $output['success'] = 0;
        }
        if($payment_type == "cheque" && $chk_num == null ){
            $output['error']['cheque_page_num'] = $this->lang->line('no_cheque_number');
            $output['success'] = 0;
        }
        if($payment_type == "cheque" && $bank_acc_id == null ){
            $output['error']['bank_acc_num'] = $this->lang->line('no_bank_acc');
            $output['success'] = 0;
        }
        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else
        {
            $data= array(
               'due_paymentmade_type' => 'buy',
               'buy_sell_id' => $this->input->post('buy_details_id', true),
               'buy_sell_voucher_no' =>$this->input->post('voucher_no', true),
               'due_paymentmade_amount' => $this->input->post('due_paymentmade_amount', true),
               'due_paymentmade_comments' => trim($this->input->post('due_paymentmade_comments', true)),
               'user_id' => $this->session->userdata('user_id'),
               );
            $is_adjust= $this->input->post('adjust_due', TRUE);
            $this->db->trans_begin();
            $buy_details_id= $data['buy_sell_id'];
            $current_paid_and_due = $this->Inventory_model->current_paid_and_due_amount($buy_details_id);
            if($is_adjust == "not_adjust"){
                $updated_data = array(
                   'paid' => $current_paid_and_due['paid'] + $data['due_paymentmade_amount'],
                   'due'  => $current_paid_and_due['due'] - $data['due_paymentmade_amount'],
                   );
            }
            if($is_adjust == "adjust"){
                $adjusted_amount = $current_paid_and_due['due'] - $data['due_paymentmade_amount'];
                /* Adjusted amount will added with discount and paid
                   Adjusted amount will deducted from net payable.
                   Due amount will be 0 
                 */
                   $updated_data = array(
                    'discount' => $current_paid_and_due['discount'] + $adjusted_amount,
                    'net_payable' => $current_paid_and_due['net_payable'] - $adjusted_amount,
                    'paid' => $current_paid_and_due['paid'] + $data['due_paymentmade_amount'],
                    'due' => 0,
                    );
                   $batta_history = array(
                    'buy_sell_id' => $buy_details_id,
                    'buy_or_sell' => 'buy',
                    'adjusted_amount' => $adjusted_amount,
                    'user_id' => $this->session->userdata('user_id'),
                    );
                   $this->Inventory_model->save_batta_history($batta_history);                
               }
               $this->Inventory_model->update_buy_after_due_paymentmade($buy_details_id, $updated_data);
               $payment_type = $this->input->post('payment_type', true);

               if($payment_type == "cash"){
                $cash_data['buy_or_sell_details_id'] = $buy_details_id;
                $cash_data['cash_type']= "buy_due_paymentmade";
                $cash_data['user_id'] = $data['user_id'];
                $cash_data['cash_description'] = $data['due_paymentmade_comments'];
                $cash_data['deposit_withdrawal_amount'] = $data['due_paymentmade_amount'] ;
                $this->Inventory_model->save_cash_box_info($cash_data);

                $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
                $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['due_paymentmade_amount'];
                $this->Inventory_model->update_total_cashbox_info($final_data);
            }
            if($payment_type == "cheque"){
             $bank_data=array(
                'deposit_withdrawal_amount' => $data['due_paymentmade_amount'],
                'cash_type' => 'buy_due_paymentmade',
                'user_id' => $this->session->userdata('user_id'),
                'buy_sell_and_other_id' => $buy_details_id,
                'bank_acc_id' => $this->input->post('bank_acc_id', true),
                );
             $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);
             $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_data['bank_acc_id']);
             $updated_balance['final_amount'] = $curr_bank_amount['final_amount'] - $bank_data['deposit_withdrawal_amount'];
             $bank_acc_id= $bank_data['bank_acc_id'];
             $update_bank_amount= $this->Bank_model->update_total_amt_info_of_acc($updated_balance,$bank_acc_id);
             $cheque_data=array(
                'buy_sell_or_others_id' => $buy_details_id,
                'bank_acc_id'=> $bank_acc_id,
                'cheque_number' => $this->input->post('cheque_number', true),
                'bank_statement_id' => $bank_statement_id,
                'user_id' => $this->session->userdata('user_id'),
                );
             $this->Bank_model->save_cheque_details_info($cheque_data);
         }
         $output['data'] = $this->Inventory_model->save_due_paymentmade_info($data);
         echo json_encode($output);
         if ($this->db->trans_status() === false) {
             $this->db->trans_rollback();
         }
         else
         {
           $this->db->trans_commit();

       }
   }
}
/**
 * Collect due payment details of individual vendor
 *@param string $vendors_id
 * @return array[]
* @author shoaib <shofik.shoaib@gmail.com>
 **/
public function get_due_info_by_vendor($vendors_id)
{
   $body_data= array(
       'vendor_due_by_id' => $this->Inventory_model->details_vendor_due_by_id($vendors_id),
       );
            // print_r($body_data);
   echo json_encode($body_data);
}
/**
 * Bunch payment-made of a vendor by owner.
 * @return array[] success or failure.
* @author shoaib <shofik.shoaib@gmail.com>
 **/
public function bunch_payment_by_vendor()
{
    // $x=$this->input->post();
    // echo "<pre>";
    // print_r($x);
    // exit();
    userActivityTracking();

    $output=array();
    $output['success'] = 1;
    $output['error'] = array();
    if($this->input->post('due_paymentmade_amount', true)==null || !is_numeric($this->input->post('due_paymentmade_amount', true))) {
        $output['error']['bunch_due_paymentmade_amount'] = $this->lang->line('validation_msg');
        $output['success'] = 0;
    }
    $buy_details_id =$this->input->post('buy_details_id', true);
    $vendors_id= $this->input->post('vendors_id', true);
    $total_due = $this->Inventory_model->details_vendor_due_by_id($vendors_id);
    $paymentade_amount = $this->input->post('due_paymentmade_amount', true);
    if($paymentade_amount > $total_due['due']) {
        $output['error']['bunch_due_paymentmade_amount'] = $this->lang->line('extra_amount_inserted');
        $output['success'] = 0;
    }
    if($output['success']==0) {
        echo json_encode($output);
        exit();
    }
    else
    {
        $vendors_id= $this->input->post('vendors_id', true);
        $bunch_amount = $this->input->post('due_paymentmade_amount', true);
        $is_adjust = $this->input->post('adjust_bunch_due', true);
        $this->db->trans_begin();
        $data= array(
           'due_paymentmade_amount' => $this->input->post('due_paymentmade_amount', true),
           'due_paymentmade_comments' => trim($this->input->post('due_paymentmade_comments', true)),
           'user_id' => $this->session->userdata('user_id'),
           );
        /* This portion is responsible for batta hisab. At first calculating what amount we need to adjust. According to that amount we increase discount and decrease net_payable and due.*/
        $all_due_vouchers_batta = $this->Inventory_model->all_due_vouchers_by_vendor_id($vendors_id);
        if($is_adjust == "adjust")
        {
            $adjusted_amount = $total_due['due'] - $data['due_paymentmade_amount'];
            foreach ($all_due_vouchers_batta as $due_vouchers_for_batta) {
                $buy_details_id = $due_vouchers_for_batta['buy_details_id'];
                if($adjusted_amount == 0){
                    break;
                }
                if($adjusted_amount >0){
                    if($adjusted_amount <= $due_vouchers_for_batta['due']){
                     $updated_data = array(
                        'discount' => $due_vouchers_for_batta['discount'] + $adjusted_amount,
                        'net_payable' => $due_vouchers_for_batta['net_payable'] - $adjusted_amount,
                        'due' => $due_vouchers_for_batta['due']- $adjusted_amount,
                        );
                     $this->Inventory_model->update_buy_after_due_paymentmade($buy_details_id, $updated_data);
                     $batta_history = array(
                        'buy_sell_id' => $due_vouchers_for_batta['buy_details_id'],
                        'buy_or_sell' => 'buy',
                        'adjusted_amount' => $adjusted_amount,
                        'user_id' => $this->session->userdata('user_id'),
                        );
                     $this->Inventory_model->save_batta_history($batta_history);
                     $adjusted_amount = 0;
                     break;
                 }
                 if($adjusted_amount > $due_vouchers_for_batta['due']){
                     $updated_data = array(
                        'discount' => $due_vouchers_for_batta['discount'] + $due_vouchers_for_batta['due'],
                        'net_payable' => $due_vouchers_for_batta['net_payable'] - $due_vouchers_for_batta['due'],
                        'due' => 0,
                        );
                     $batta_history = array(
                        'buy_sell_id' => $due_vouchers_for_batta['buy_details_id'],
                        'buy_or_sell' => 'buy',
                        'adjusted_amount' => $due_vouchers_for_batta['due'],
                        'user_id' => $this->session->userdata('user_id'),
                        );
                     $this->Inventory_model->update_buy_after_due_paymentmade($buy_details_id, $updated_data);
                     $this->Inventory_model->save_batta_history($batta_history);
                     $adjusted_amount -= $due_vouchers_for_batta['due'];
                 }
             }
         }
     }
     /*Batta hisab calculation ends*/
     /*Bunch payment made calculation starts*/
     $payment_type= $this->input->post('payment_type', TRUE);
     $bank_acc_id = $this->input->post('bank_acc_id', TRUE);;
     $cheque_page_num= $this->input->post('cheque_number', TRUE);;

     $all_due_vouchers = $this->Inventory_model->all_due_vouchers_by_vendor_id($vendors_id);
     foreach ($all_due_vouchers as $v_due_vouchers) {
        $buy_details_id = $v_due_vouchers['buy_details_id'];
        if($bunch_amount >= $v_due_vouchers['due']){
            $bunch_update= array(
                'due' => 0,
                'paid' => $v_due_vouchers['paid']+$v_due_vouchers['due'] ,
                );
            $bunch_amount -=$v_due_vouchers['due'];
            $this->Inventory_model->bunch_due_paymentmade_update($buy_details_id,$bunch_update);
            $data= array(
               'due_paymentmade_type' => 'buy',
               'buy_sell_id' => $buy_details_id,
               'due_paymentmade_amount' => $v_due_vouchers['due'],
               'user_id' => $this->session->userdata('user_id'),
               );
            $this->Inventory_model->save_due_paymentmade_info($data);  
            if($payment_type == "cash")
            {
             $cash_data['buy_or_sell_details_id'] = $buy_details_id;
             $cash_data['cash_type']= "buy_due_paymentmade";
             $cash_data['user_id'] = $data['user_id'];
             $cash_data['deposit_withdrawal_amount'] = $v_due_vouchers['due'];
             $this->Inventory_model->save_cash_box_info($cash_data);
         }
         if ($payment_type == "cheque") {
            $bank_data=array(
                'deposit_withdrawal_amount' => $v_due_vouchers['due'],
                'cash_type' => 'buy_due_paymentmade',
                'user_id' => $this->session->userdata('user_id'),
                'buy_sell_and_other_id' => $buy_details_id,
                'bank_acc_id' => $bank_acc_id,
                );
            $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);
            $cheque_data=array(
                'buy_sell_or_others_id' => $buy_details_id,
                'bank_acc_id'=> $bank_acc_id,
                'cheque_number' => $cheque_page_num,
                'bank_statement_id' => $bank_statement_id,
                'user_id' => $this->session->userdata('user_id'),
                );
            $this->Bank_model->save_cheque_details_info($cheque_data);  
        }
    }
    else{
        $bunch_update =array(
            'due' => $v_due_vouchers['due']-$bunch_amount,
            'paid' => $v_due_vouchers['paid']+$bunch_amount,
            );
                // $payment_amt = $v_due_vouchers['due']-$bunch_amount;
        $this->Inventory_model->bunch_due_paymentmade_update($buy_details_id,$bunch_update);
        $data= array(
          'due_paymentmade_type' => 'buy',
          'buy_sell_id' => $buy_details_id,
          'due_paymentmade_amount' => $bunch_amount,
          'user_id' => $this->session->userdata('user_id'),
          );
        $this->Inventory_model->save_due_paymentmade_info($data);
        if ($payment_type == "cash") {
            $cash_data['buy_or_sell_details_id'] = $buy_details_id;
            $cash_data['cash_type']= "buy_due_paymentmade";
            $cash_data['user_id'] = $data['user_id'];
            $cash_data['deposit_withdrawal_amount'] = $bunch_amount;
            $this->Inventory_model->save_cash_box_info($cash_data);
            break;
        }
        if ($payment_type == "cheque") {
           $bank_data=array(
            'deposit_withdrawal_amount' => $bunch_amount,
            'cash_type' => 'buy_due_paymentmade',
            'user_id' => $this->session->userdata('user_id'),
            'buy_sell_and_other_id' => $buy_details_id,
            'bank_acc_id' => $bank_acc_id,
            );
           $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);
           $cheque_data=array(
            'buy_sell_or_others_id' => $buy_details_id,
            'bank_acc_id'=> $bank_acc_id,
            'cheque_number' => $cheque_page_num,
            'bank_statement_id' => $bank_statement_id,
            'user_id' => $this->session->userdata('user_id'),
            );
           $this->Bank_model->save_cheque_details_info($cheque_data);  
           break;
       }  
   }
}
if ($payment_type == "cash") {
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    $payment_amt = $this->input->post('due_paymentmade_amount', true);
    $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $payment_amt;
    $this->Inventory_model->update_total_cashbox_info($final_data);
}
if ($payment_type == "cheque") {
    $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);
    $updated_balance['final_amount'] = $curr_bank_amount['final_amount'] - $paymentade_amount;
    $update_bank_amount= $this->Bank_model->update_total_amt_info_of_acc($updated_balance,$bank_acc_id); 
}

if ($this->db->trans_status() === false)
{
    $this->db->trans_rollback();
    echo json_encode(array('success' =>'no'));
}
else
{
    $this->db->trans_commit();
    echo json_encode(array('success' => 'yes'));
}
}
}
/*Bunch payment made calculation ends*/
}

/* End of file Buy_list.php */
/* Location: ./application/controllers/Buy_list.php */