<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Barcode extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->benchmark->mark('startPerformance');
    userActivityTracking();
    
    
  }

  public function index()
  {
        //I'm just using rand() function for data example
        // $temp = rand(10000, 99999);

        // $x = $this->input->post('name', TRUE);
        // $temp="Bangladesh";	
        // $this->set_barcode($temp);
    load_header();
    $this->load->view('barcode/barcode_print_page');
    load_footer();
  }
  
  private function set_barcode($code)
  {

         //load library
    $this->load->library('zend');
        //load in folder Zend
    $this->zend->load('Zend/Barcode');
        //generate barcode
    Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
        // print_r($Zend_Barcode);
  }

  public function __destruct()
  {
    $this->benchmark->mark('endPerformance');
    $data["exec_time"] = $this->benchmark->elapsed_time('startPerformance', 'endPerformance');
    $data["url"] = site_url().uri_string();
    $data["id_user"] = $this->session->userdata('user_id');
    $this->db->insert('benchmark_table',$data);
  }


}

/* End of file bercode.php */
/* Location: ./application/controllers/bercode.php */