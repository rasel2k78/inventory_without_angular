<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sell_deleted_invoices extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Inventory_model');
		$this->load->model('User_access_check_model');
		$cookie = $this->input->cookie('language', true);
		$this->lang->load('deleted_sell_lang', $cookie);
		$this->lang->load('access_message_lang', $cookie);
		$this->lang->load('left_side_nev_lang', $cookie);
		userActivityTracking();

		$user_id = $this->session->userdata('user_id');
		if ($user_id == null) {

			redirect('Login', 'refresh');
		}
	}

	public function index()
	{
		load_header();
		$this->load->view('sell/deleted_invoices_view');
		load_footer();    
	}

	public function all_deleted_list_info_for_datatable()
	{
		$filters = $this->input->get();
		$all_data = $this->Inventory_model->deleted_sell_list_all_info($filters);
		$all_data_without_limit = $this->Inventory_model->deleted_sell_list_all_info($filters, true);
		$all_data_final = $this->Inventory_model->deleted_sell_list_all_info($filters, true);
		$output_data=[];
		$output_data["draw"]=$filters['draw'];
		$output_data["recordsTotal"]=$all_data_without_limit;
		$output_data["recordsFiltered"]=$all_data_final;
		$output_data["data"]=$all_data;
		echo json_encode($output_data);
	}
	public function get_deleted_sell_info($sell_details_id)
	{
		$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_id);
		$body_data= array(
			'all_sell_info_by_id' => $this->Inventory_model->deleted_sell_info_by_id($sell_details_id),
			'customer_info' => $this->Inventory_model->get_customer_info_for_voucher($voucher_details['customers_id']),
			'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
			);
		echo json_encode($body_data); 
	}
}

/* End of file Sell_deleted_invoices.php */
/* Location: ./application/controllers/Sell_deleted_invoices.php */