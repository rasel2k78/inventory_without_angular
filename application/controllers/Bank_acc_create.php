<?php
/**
 * Bank Account Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Use this controller for creating/editing/deletion of bank accounts. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Bank_acc_create extends CI_Controller {
	 /**
     * This is a constructor function
     *
     * This load Bank Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
   public function __construct()
   {
    parent::__construct();
    $this->load->model('Bank_model');
    $this->load->model('User_access_check_model');
    $cookie = $this->input->cookie('language', true);
    $this->lang->load('left_side_nev_lang', $cookie);
    $this->lang->load('bank_acc_lang', $cookie);

    userActivityTracking();

    $user_id = $this->session->userdata('user_id');
    if ($user_id == null) {
      redirect('Login', 'refresh');
    }
  }
	/**
	 * Load the view page.
	 *
	 * @return void
	 * @author shoaib <shofik.shoaib@gmail.com> 
	 **/
	public function index()
	{
    load_header();
    $user_id_logged = $this->session->userdata('user_id');
    $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $this->load->view('bank/add_bank_account_form');
    }
    else{
      $this->load->view('access_deny/not_permitted');
    }
    load_footer();
  }
 /**
     * This function save all bank account's informations  in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
 public function save_bank_acc_info()
 {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
  $output = array();
  $output['success'] = 1;
  $output['error']= array();
  if($this->input->post('banks_id') ==null ||  trim($this->input->post('banks_id'))=="" ) {
    $output['error']['banks_id']= $this->lang->line('validation_name');;
    $output['success']= 0;
  }
  if($this->input->post('bank_acc_num') ==null ||  trim($this->input->post('bank_acc_num'))=="" ) {
    $output['error']['bank_acc_num']= $this->lang->line('validation_account');;
    $output['success']= 0;
  }
  if($this->input->post('bank_acc_name') ==null ||  trim($this->input->post('bank_acc_name'))=="" ) {
    $output['error']['bank_acc_name']= $this->lang->line('validation_name');;
    $output['success']= 0;
  }
  if($this->input->post('bank_acc_branch') ==null ||  trim($this->input->post('bank_acc_branch'))=="" ) {
    $output['error']['bank_acc_branch']= $this->lang->line('validation_branch');;
    $output['success']= 0;
  }
  if($output['success']==0) {
    echo json_encode($output);
    exit();
  }
  else{
   $acc_row_count = $this->Bank_model->count_bank_acc();
   if($acc_row_count > 0){
    $data= array(
     'banks_id' => trim($this->input->post('banks_id', true)),
     'bank_acc_num' => trim($this->input->post('bank_acc_num', true)),
     'bank_acc_name' => trim($this->input->post('bank_acc_name', true)),
     'bank_acc_branch' => trim($this->input->post('bank_acc_branch', true)),
     'bank_acc_des' => trim($this->input->post('bank_acc_des', true)),
     'user_id' => $this->session->userdata('user_id'),
     'selection_status' => 'not_selected',
     );
    $output['data'] = $this->Bank_model->save_bank_acc_info($data);
    echo json_encode($output);
  }
  else{
    $data= array(
     'banks_id' => trim($this->input->post('banks_id', true)),
     'bank_acc_num' => trim($this->input->post('bank_acc_num', true)),
     'bank_acc_name' => trim($this->input->post('bank_acc_name', true)),
     'bank_acc_branch' => trim($this->input->post('bank_acc_branch', true)),
     'bank_acc_des' => trim($this->input->post('bank_acc_des', true)),
     'user_id' => $this->session->userdata('user_id'),
     'selection_status' => 'selected',
     );
    $output['data'] = $this->Bank_model->save_bank_acc_info($data);
    echo json_encode($output);
  }

}
        // }
        // else {
        //     echo json_encode("No Permisssion");
        // }
}
      /**
     * This function is used for boostrap select of bank's information in database.
     * 
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

      public function search_bank_by_name()
      {
        $text = $this->input->post('q');
        if($text == null || $text=="") {
          echo "input field empty";
        }
        else{
          echo json_encode($this->Bank_model->search_bank($text));   
        }
      }
     /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
     public function all_bank_acc_info_for_datatable()
     {        
      $filters = $this->input->get();
      $all_data = $this->Bank_model->all_bank_accounts_info($filters);
      $all_data_without_limit = $this->Bank_model->all_bank_accounts_info($filters, true);
      $all_data_final = $this->Bank_model->all_bank_accounts_info($filters, true);
      $output_data=[];
      $output_data["draw"]=$filters['draw'];
      $output_data["recordsTotal"]=$all_data_without_limit;
      $output_data["recordsFiltered"]=$all_data_final;
      $output_data["data"]=$all_data;
      echo json_encode($output_data);
    }
    /**
     * This function collect all informations of individual bank account by parameter.
     * 
     * @param string $bank_acc_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_bank_acc_info($bank_acc_id)
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $body_data= array(
       'bank_acc_info_by_id' => $this->Bank_model->bank_acc_info_by_id($bank_acc_id),
       );
      echo json_encode($body_data);
        // }
        // else {
        //     echo json_encode("No Permisssion");
        // }
    }
    /**
     * This function is used for update bank account's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function update_bank_acc_info()
    {
      $output=array();
      $output['success'] = 1;
      $output['error'] = array();
      if($this->input->post('bank_acc_num_edit', true)==null ||  trim($this->input->post('bank_acc_num_edit'))=="" ) {
        $output['error']['bank_acc_num_edit'] = $this->lang->line('validation_account');
        $output['success'] = 0;
      }
      if($this->input->post('bank_acc_branch_edit', true)==null ||  trim($this->input->post('bank_acc_branch_edit'))=="" ) {
        $output['error']['bank_acc_branch_edit'] = $this->lang->line('validation_branch');
        $output['success'] = 0;
      }
      if($this->input->post('bank_acc_name_edit', true)==null ||  trim($this->input->post('bank_acc_name_edit'))=="" ) {
        $output['error']['bank_acc_name_edit'] = $this->lang->line('validation_name');
        $output['success'] = 0;
      }
      if($output['success'] == 1) {
        $bank_acc_id = $this->input->post('bank_acc_id', true);
        $data=array(
         'bank_acc_num' => trim($this->input->post('bank_acc_num_edit', true)),
         'bank_acc_name' => trim($this->input->post('bank_acc_name_edit', true)),
         'bank_acc_des' => trim($this->input->post('bank_acc_des_edit', true)),
         'bank_acc_branch' => trim($this->input->post('bank_acc_branch_edit', true)),
         'user_id' => $this->session->userdata('user_id'),
         );
        if($this->Bank_model->update_bank_acc_info($bank_acc_id, $data)) {
         echo json_encode($output);
       }
       else{
         $output['success']  = 0;
         echo json_encode($output);
       }
     }
     else{
      echo json_encode($output);
    }
  }
        /**
     * This function will delete/hide the bank account's information by "bak_acc_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $bank_acc_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
        public function delete_bank_acc($bank_acc_id)
        {

        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
          if($this->Bank_model->delete_bank_acc_info($bank_acc_id)) {
            echo "success";    
          }    
          else{
            echo "failed";
          }
        // }
        // else {
        //     echo "No Permisssion";
        // }

        }
/**
 * This function actually used to activa a vat type. Selected vat type will be activated and rest other will be deactivated automatically.
 *
 *@param  string $vat_id
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function activate_acc($bank_acc_id)
{
        //  $user_id_logged = $this->session->userdata('user_id');
        //  $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
  $this->Bank_model->change_status();
  if($this->Bank_model->activate_acc_info($bank_acc_id)) {
    echo "success";
  }
  else {
    echo "failed";
  }
        // }
        // else {
        //     echo "No Permission";
        // }
}
}
/* End of file Bank_acc_create.php */
/* Location: ./application/controllers/Bank_acc_create.php */