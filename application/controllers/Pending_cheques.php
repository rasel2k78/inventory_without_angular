<?php
/**
 * Pending cheques Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Use this controller for showing pending cheques during sell. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Pending_cheques extends CI_Controller {
    /**
     * This is a constructor function
     *
     * This load Bank Model and language files when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
    	parent::__construct();

      $this->load->model('Bank_model');
      $this->load->model('User_access_check_model');
      $cookie = $this->input->cookie('language', true);
      $this->lang->load('pending_cheque_lang', $cookie);
        // $this->lang->load('buy_all_list_lang', $cookie);
      $this->lang->load('access_message_lang', $cookie);
      $this->lang->load('left_side_nev_lang', $cookie);
      userActivityTracking();
      
      $user_id = $this->session->userdata('user_id');
      if ($user_id == null) {
        redirect('Login', 'refresh');
      }
    }
    /**
     * This function is used for loading view page. 
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
    	load_header();
      $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $this->load->view('pending_cheque/pending_cheque_form');
        // }
        // else{
        //     $this->load->view('access_deny/not_permitted');
        // }
      load_footer();  
    }
        /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
        public function all_pending_cheques_info_for_datatable()
        {
        	$filters = $this->input->get();
        	$all_data = $this->Bank_model->all_pending_cheque_info_list($filters);
        	$all_data_without_limit = $this->Bank_model->all_pending_cheque_info_list($filters, true);
        	$all_data_final = $this->Bank_model->all_pending_cheque_info_list($filters, true);
        	$output_data=[];
        	$output_data["draw"]=$filters['draw'];
        	$output_data["recordsTotal"]=$all_data_without_limit;
        	$output_data["recordsFiltered"]=$all_data_final;
        	$output_data["data"]=$all_data;
        	echo json_encode($output_data);
        }
        /**
         * Changes cheque status if it is cleared by cash
         *
         * @return array[]
         * @author shoaib <shofik.shoaib@gmail.com>
         **/
        public function chk_cleared_by_cash($pending_cheque_id)
        {
        	$this->db->trans_begin();
        	$pending_cheques_details = $this->Bank_model->get_cheque_details_by_id($pending_cheque_id);
        	$cash_data=array(
        		'user_id' => $this->session->userdata('user_id'),
        		'deposit_withdrawal_amount'=> $pending_cheques_details['cheque_amount'],
        		'cash_type' => 'sell_chk_clred_by_cash',
        		'buy_or_sell_details_id' =>$pending_cheques_details['sell_details_id'] ,
        		);

        	$this->Bank_model->save_cash_box_info($cash_data);
        	$update_chk_status= array(
        		'cheque_cleared_type' => 'cash',
        		'cheque_status' => 'done',
        		);
        	$this->Bank_model->update_pending_chk_status($pending_cheque_id,$update_chk_status);
        	$current_cashbox_amount = $this->Bank_model->final_cashbox_amount_info();
        	$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $pending_cheques_details['cheque_amount'];
        	$this->Bank_model->update_total_cashbox_info($final_data);
        	if ($this->db->trans_status() === false) {
        		$this->db->trans_rollback();
        		echo "error transection";
        	}
        	else
        	{
        		$this->db->trans_commit();
        		echo json_encode(array('success'=>'yes'));
        	}

        }
        /**
         * Changes cheque status if it is cleared by bank account
         *
         * @return array[]
         * @author shoaib <shofik.shoaib@gmail.com>
         **/
        public function chk_cleared_by_acc()
        {
        	$pending_cheque_id = $this->input->post('pending_cheque_id');
        	$bank_acc_id= $this->input->post('bank_acc_id');
        	if ($bank_acc_id == "") {
        		echo json_encode(array("error"=>"no_acc_selected"));
        		exit();
        	}

        	$this->db->trans_begin();
        	$pending_cheques_details = $this->Bank_model->get_cheque_details_by_id($pending_cheque_id);
        	$bank_data=array(
        		'bank_acc_id'=> $bank_acc_id,
        		'user_id'=> $this->session->userdata('user_id'),
        		'deposit_withdrawal_amount' => $pending_cheques_details['cheque_amount'],
        		'cash_type'=> 'sell',
        		'buy_sell_and_other_id'=> $pending_cheques_details['sell_details_id'],
        		);		
        	$this->Bank_model->save_buy_by_bank_info($bank_data);
        	$update_chk_status= array(
        		'cheque_cleared_type' => 'bank_acc_dpst',
        		'cheque_status' => 'done',
        		'bank_acc_id' => $bank_acc_id,
        		);
        	$this->Bank_model->update_pending_chk_status($pending_cheque_id,$update_chk_status);
        	$current_acc_balance = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);
        	$final_data['final_amount']= $current_acc_balance['final_amount'] + $pending_cheques_details['cheque_amount'];
        	$this->Bank_model->update_total_amt_info_of_acc($final_data,$bank_acc_id);
        	if ($this->db->trans_status() === false) {
        		$this->db->trans_rollback();
        		echo "error transection";
        	}
        	else
        	{
        		$this->db->trans_commit();
        		echo json_encode(array('success'=>'yes'));
        	}
        }

      }

      /* End of file Pending_cheques.php */
/* Location: ./application/controllers/Pending_cheques.php */