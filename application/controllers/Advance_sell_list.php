<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance_sell_list extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Inventory_model');
		$this->load->model('User_access_check_model');
		$this->load->model('Bank_model');

		$cookie = $this->input->cookie('language', true);
		$this->lang->load('sell_all_list_lang', $cookie);
		$this->lang->load('access_message_lang', $cookie);
		$this->lang->load('left_side_nev_lang', $cookie);
		$this->lang->load('sell_page_lang', $cookie);
		$this->lang->load('adv_sell_lang', $cookie);
		$this->lang->load('add_customer_form_lang', $cookie);
		$this->lang->load('add_sales_rep_form_lang', $cookie);
		userActivityTracking();

		$user_id = $this->session->userdata('user_id');
		if ($user_id == null) {

			redirect('Login', 'refresh');
		}
	}

	public function index()
	{
		load_header();
		$this->load->view('advance_sell/advance_sell_list');
		load_footer();
	}

	public function advance_sell_list_info_for_datatable()
	{
		$filters = $this->input->get();
		$all_data = $this->Inventory_model->adv_sell_list_all_info($filters);
		$all_data_without_limit = $this->Inventory_model->adv_sell_list_all_info($filters, true);
		$all_data_final = $this->Inventory_model->adv_sell_list_all_info($filters, true);
		$output_data=[];
		$output_data["draw"]=$filters['draw'];
		$output_data["recordsTotal"]=$all_data_without_limit;
		$output_data["recordsFiltered"]=$all_data_final;
		$output_data["data"]=$all_data;
		echo json_encode($output_data);
	}

	public function get_adv_sell_info($adv_sell_details_id)
	{
		$voucher_details = $this->Inventory_model->adv_sell_details_by_id($adv_sell_details_id);
		$body_data= array(
			'all_sell_info_by_id' => $this->Inventory_model->adv_sell_info_by_id($adv_sell_details_id),
			'customer_info' => $this->Inventory_model->get_customer_info_for_voucher($voucher_details['customers_id']),
			'voucher_number' => $this->Inventory_model->collect_adv_voucher_number($voucher_details['adv_sell_details_id']),
			);
		echo json_encode($body_data); 
	}

	public function complete_adv_sell()
	{
		$data=  $this->input->post();
		$store_details = $this->Inventory_model->store_details();
		$data['store_details'] = $store_details;
		load_header();
		$this->load->view('advance_sell/complete_adv_sell', $data, FALSE);
		load_footer();
	}

	public function update_adv_sell_info()
	{
		$data= $this->input->post();

		$adv_sell_details_id = $data['adv_sell_details_id'];
		$this->db->trans_begin();

		$get_previous_info = $this->Inventory_model->adv_sell_details_by_id($adv_sell_details_id);

		$diff_amt = $data['paid'] - $get_previous_info['paid'];
		if($diff_amt > 0){
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $diff_amt;
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$cash_data['cash_type']= "advance_sell";
			$cash_data['user_id'] = $this->session->userdata('user_id');
			$cash_data['deposit_withdrawal_amount'] = $diff_amt;
			$cash_data['buy_or_sell_details_id']= $adv_sell_details_id;
			$this->Inventory_model->save_cash_box_info($cash_data);
		}
		if($diff_amt < 0){
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $diff_amt;
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$cash_data['cash_type']= "sell_advance_adjust";
			$cash_data['user_id'] = $this->session->userdata('user_id');
			$cash_data['deposit_withdrawal_amount'] = (-1)*$diff_amt;
			$cash_data['buy_or_sell_details_id']= $adv_sell_details_id;
			$this->Inventory_model->save_cash_box_info($cash_data);
		}
		$update_data = array(
			'paid' => $data['paid'],
			'due' => $get_previous_info['net_payable'] - $data['paid'],
			'delivery_date' => $data['delivery_date'],
			);
		$this->Inventory_model->update_adv_sell_info_by_id($adv_sell_details_id,$update_data);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			echo json_encode("error transaction");
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode("success");
		}
	}

	public function delete_adv_sell($adv_sell_details_id)
	{
		

		$get_invoice_details = $this->Inventory_model->adv_sell_details_by_id($adv_sell_details_id);
		
		if($get_invoice_details['payment_type'] == "cash"){
			$this->db->trans_begin();
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $get_invoice_details['paid'];
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$cash_data['cash_type']= "sell_advance_adjust";
			$cash_data['user_id'] = $this->session->userdata('user_id');
			$cash_data['deposit_withdrawal_amount'] = $get_invoice_details['paid'];
			$cash_data['buy_or_sell_details_id']= $adv_sell_details_id;
			$this->Inventory_model->save_cash_box_info($cash_data);
			$this->Inventory_model->delete_adv_invoice($adv_sell_details_id);
			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
				echo json_encode("error transaction");
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode("success");
			}
		}
		if($get_invoice_details['payment_type'] == "card"){
			$this->db->trans_begin();
			$card_details_sell= $this->Inventory_model->get_card_payment_details_for_adv_sell($adv_sell_details_id);
			$bank_acc_id = $card_details_sell['bank_acc_id'];
			$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
			$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$card_details_sell['deposit_withdrawal_amount'];
			$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
			$this->Inventory_model->delete_adv_invoice($adv_sell_details_id);
			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
				echo json_encode("error transaction");
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode("success");
			}
		}

		if($get_invoice_details['payment_type'] == "cheque"){
			$this->db->trans_begin();
			$sell_details_id = $adv_sell_details_id;
			$check_status= $this->Inventory_model->curr_cheque_status($sell_details_id);
			if($check_status['cheque_status']=="pending"){
				$this->Inventory_model->delete_pending_cheque($sell_details_id);
			}
			if($check_status['cheque_status']=="done"){
				$chk_clear_type=$this->Inventory_model->cheque_cleared_by($sell_details_id);
				if($chk_clear_type['cheque_cleared_type']=="cash")
				{
					$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
					$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $check_status['cheque_amount'];
					$this->Inventory_model->update_total_cashbox_info($final_data);
				}
				if($chk_clear_type['cheque_cleared_type']=="bank_acc_dpst"){      
					$bank_acc_id= $chk_clear_type['bank_acc_id'];
					$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
					$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$check_status['cheque_amount'];
					$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
				}
			}
			$this->Inventory_model->delete_adv_invoice($adv_sell_details_id);
			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
				echo json_encode("error transaction");
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode("success");
			}
		}

		if($get_invoice_details['payment_type'] == "cash_card_both"){
			$this->db->trans_begin();
			$sell_details_id = $adv_sell_details_id;
			$paid_by_cash = $this->Inventory_model->amt_paid_by_cash($sell_details_id);
			$paid_by_card_details = $this->Inventory_model->amt_paid_by_card_details($sell_details_id);
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_by_cash['deposit_withdrawal_amount'];
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$bank_acc_id= $paid_by_card_details['bank_acc_id'];
			$curr_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
			$updated_amount['final_amount']= $curr_bank_amt['final_amount']-$paid_by_card_details['deposit_withdrawal_amount'];
			$this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
			$this->Inventory_model->delete_adv_invoice($adv_sell_details_id);
			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
				echo json_encode("error transaction");
			}
			else
			{
				$this->db->trans_commit();
				echo json_encode("success");
			}
		}



	}
}

/* End of file Advance_sell_list.php */
/* Location: ./application/controllers/Advance_sell_list.php */