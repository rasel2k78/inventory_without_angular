<?php
/**
 * Bank Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Use this controller for creating/editing/deletion of bank. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Bank_create extends CI_Controller {
    /**
     * This is a constructor function
     *
     * This load Bank Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
      parent::__construct();

      $this->load->model('Bank_model');
      $this->load->model('User_access_check_model');
      $cookie = $this->input->cookie('language', true);
      $this->lang->load('left_side_nev_lang', $cookie);
      $this->lang->load('bank_create_lang', $cookie);
      userActivityTracking();
      
      $user_id = $this->session->userdata('user_id');
      if ($user_id == null) {
        redirect('Login', 'refresh');
      }
    }
    /**
     * This function is used for loading bank form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
     load_header();
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
       $this->load->view('bank/add_bank_form');
     }
     else{
      $this->load->view('access_deny/not_permitted');
    }
    load_footer();  
  }
    /**
     * This function save all bank informations  in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_bank_info()
    {
     $output = array();
     $output['success'] = 1;
     $output['error']= array();
     if($this->input->post('banks_name') ==null ||  trim($this->input->post('banks_name'))=="" ) {
      $output['error']['banks_name']= $this->lang->line('validation_name');
      $output['success']= 0;
    }
    if($this->input->post('banks_abbe') ==null ||  trim($this->input->post('banks_abbe'))=="" ) {
      $output['error']['banks_abbe']= $this->lang->line('validation_abbe');
      $output['success']= 0;
    }
    if($this->input->post('charge_percentage')== null ||  trim($this->input->post('charge_percentage'))==""){
     $output['error']['bank_charge_percentage']= $this->lang->line('validation_charge_percentage');
     $output['success']= 0;
   }
   if($output['success']==0) {
    echo json_encode($output);
    exit();
  }
  else{
    $data= array(
     'banks_name' => trim($this->input->post('banks_name', true)),
     'banks_abbe' => str_replace(' ', '', strtoupper(trim($this->input->post('banks_abbe', true)))),
     'banks_description' => trim($this->input->post('banks_description', true)),
     'user_id' => $this->session->userdata('user_id'),
     'charge_percentage'=> trim($this->input->post('charge_percentage', TRUE)),
     );
    $output['data'] = $this->Bank_model->save_bank_info($data);
    echo json_encode($output);
  }
}
    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_bank_info_for_datatable()
    {        
      $filters = $this->input->get();
      $all_data = $this->Bank_model->all_bank_info($filters);
      $all_data_without_limit = $this->Bank_model->all_bank_info($filters, true);
      $all_data_final = $this->Bank_model->all_bank_info($filters, true);
      $output_data=[];
      $output_data["draw"]=$filters['draw'];
      $output_data["recordsTotal"]=$all_data_without_limit;
      $output_data["recordsFiltered"]=$all_data_final;
      $output_data["data"]=$all_data;
      echo json_encode($output_data);
    }
    /**
     * This function collect all informations of individual bank by parameter.
     * 
     * @param string $banks_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_bank_info($banks_id)
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $body_data= array(
       'bank_info_by_id' => $this->Bank_model->bank_info_by_id($banks_id),
       );
      echo json_encode($body_data);
        // }
        // else {
        //     echo json_encode("No Permisssion");
        // }
    }
     /**
     * This function is used for update bank's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
     public function update_bank_info()
     {
      $output=array();
      $output['success'] = 1;
      $output['error'] = array();
      if($this->input->post('banks_name_edit', true)==null ||  trim($this->input->post('banks_name_edit'))=="" ) {
        $output['error']['banks_name_edit'] = $this->lang->line('validation_name');
        $output['success'] = 0;
      }
      if($this->input->post('banks_abbe_edit', true)==null ||  trim($this->input->post('banks_abbe_edit'))=="" ) {
        $output['error']['banks_abbe_edit'] = $this->lang->line('validation_abbe');
        $output['success'] = 0;
      }
      if($this->input->post('charge_percentage_edit', true)==null ||  trim($this->input->post('charge_percentage_edit'))=="" ) {
        $output['error']['bank_charge_percentage_edit'] = $this->lang->line('validation_charge_percentage');
        $output['success'] = 0;
      }
      
      if($output['success'] == 1) {
        $banks_id = $this->input->post('banks_id', true);
        $data=array(
         'banks_abbe' => trim($this->input->post('banks_abbe_edit', true)),
         'banks_name' => trim($this->input->post('banks_name_edit', true)),
         'banks_description' => trim($this->input->post('banks_description_edit', true)),
         'user_id' => $this->session->userdata('user_id'),
         'charge_percentage' => trim($this->input->post('charge_percentage_edit', true)),
         );
        if($this->Bank_model->update_banks_info($banks_id, $data)) {
         echo json_encode($output);
       }
       else{
         $output['success']  = 0;
         echo json_encode($output);
       }
     }
     else{
      echo json_encode($output);
    }
  }
    /**
     * This function will delete/hide  the bank's information by "banks_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $banks_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function delete_bank($banks_id)
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      if($this->Bank_model->delete_bank_info($banks_id)) {
        echo "success";    
      }    
      else{
        echo "failed";
      }
        // }
        // else {
        //     echo "No Permisssion";
        // }

    }


  }
  /* End of file Bank_create.php */
/* Location: ./application/controllers/Bank_create.php */