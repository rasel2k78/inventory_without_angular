<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Item_importer extends CI_Controller {
	public  $labelOfCategory=3;
	//public $db=null;
	public function __construct()
	{
		parent::__construct();
		// $this->db=$this->load->database('db',TRUE);
	}
	public function index()
	{

		$fdata = array();
		if (!empty($_FILES['item_file']['name'])) 
		{
			$config['upload_path'] = './';
			$config['allowed_types'] = 'csv';
			$config['max_size'] = '200000';
			$config['max_width'] = '9999';
			$config['max_height'] = '9999';
			$config['encrypt_name'] = false;
			$config['overwrite'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$error = '';


			if (!$this->upload->do_upload('item_file')) {
				// $error = $this->upload->display_errors();
				echo "Something went wrong";
				exit();
			} 
			else
			{
				$fdata = $this->upload->data();
				$udata['image'] = $config['upload_path'].$fdata['file_name'];
			}
		}

		$the_file_name = $fdata['file_name'];


		$csv = array_map('str_getcsv', file($the_file_name));
		$i=0;		
		foreach ($csv as $key => $value) {	
			if($i==0)
			{
				//echo '<pre>';print_r($value) ;exit;
				$i++;				
				$this->insertSapcName($value);				
			}
			else
			{
				$i++;				
				$this->insertSingleRowData($value,$csv[0]);
			}
		}
	}
	public function insertSapcName($value)
	{		
		for ($i=$this->labelOfCategory+1; $i < count($value) ; $i++) { 		 
			$spacName=$this->get_spec_name_id_or_insert($value[$i]);
		}
	}
	public function insert_spec_set_connector_with_values($header,$spacSetID,$data)
	{
		for ($totalSpacType=$this->labelOfCategory+2; $totalSpacType < count($header) ; $totalSpacType++) { 
			if($data[$totalSpacType]!="")
			{
				$spacNameID=$this->get_spec_name_id_or_insert($header[$totalSpacType]);
				$spacValueId=$this->get_spec_value_or_id_insert($data[$totalSpacType],null);
				$this->db->select('UUID() AS `uuid`');
				$dataInsert=array();
				$dataInsert['spec_set_connector_id']=$this->db->get()->row_array()['uuid'];	 		
				$dataInsert['item_spec_set_id'] = $spacSetID;
				$dataInsert['spec_name_id'] = $spacNameID;	
				$dataInsert['spec_value_id'] = 	$spacValueId;	
				$dataInsert['stores_id'] = "327";			
				$this->db->insert('spec_set_connector_with_values', $dataInsert);
			}
		}	
	}
	public function insertSingleRowData($data,$header)
	{
		$lastInsertCategory=null;
		for($categoryInsert=0;$categoryInsert <= $this->labelOfCategory; $categoryInsert++)
		{
			if($data[$categoryInsert] != "" )
			{
				$parentID=null;
				if($categoryInsert!= 0)
				{		
					$parentID = $this->get_category_id_or_insert($data[$categoryInsert-1]);
				}
				$lastInsertCategory=$this->get_category_id_or_insert($data[$categoryInsert],$parentID);
			}
		}
		$ReturnitemID = $this->get_item_id_or_insert($data[$categoryInsert],$lastInsertCategory);
		$combination=$data[$categoryInsert];	
		for ($totalSpacType=$this->labelOfCategory+2; $totalSpacType < count($header) ; $totalSpacType++) { 
			
			if(trim($data[$totalSpacType]) != "")
			{
				$spec_name_id=$this->get_spec_name_id_or_insert($header[$totalSpacType]);
				$this->get_spec_value_or_id_insert($data[$totalSpacType],$spec_name_id);
				if($combination=="")
					$combination.=$data[$totalSpacType];
				else
					$combination.="--".$data[$totalSpacType];					
			} else {
				continue;
			}
		}		
		$spacSetID=$this->item_spec_set_insert_or_select($combination,$ReturnitemID);
		$this->insert_spec_set_connector_with_values($header,$spacSetID,$data);
		
	}
	public function item_spec_set_insert_or_select($combination,$itemID)
	{
		$this->db->select('item_spec_set_id');
		$this->db->from('item_spec_set');
		$this->db->where('UPPER(spec_set)', strtoupper($combination));
		$data = $this->db->get()->result_array();
		if(count($data)>0)
		{
			return $data[0]['item_spec_set_id'];
		}
		else
		{
			$this->db->select('UUID() AS `uuid`');
			$dataInsert=array();
			$dataInsert['item_spec_set_id'] = $this->db->get()->row_array()['uuid'];	 		
			$dataInsert['spec_set'] = $combination;	
			$dataInsert['items_id'] = 	$itemID;	
			$dataInsert['stores_id'] = "327";			
			$this->db->insert('item_spec_set', $dataInsert);
			return $dataInsert['item_spec_set_id'];
		}
	}
	public function get_category_id_or_insert($categories_name,$parent_categories_id=null)
	{
		$this->db->select('categories_id');
		$this->db->from('categories');
		$this->db->where('UPPER(categories_name)', strtoupper($categories_name));
		$data = $this->db->get()->result_array();
		if(count($data)>0)
		{
			return $data[0]['categories_id'];
		}
		else
		{
			$this->db->select('UUID() AS `uuid`');
			$dataInsert=array();
			$dataInsert['categories_id'] = $this->db->get()->row_array()['uuid'];	 		
			$dataInsert['categories_name'] = $categories_name;
			$dataInsert['stores_id'] = "327";
			if($parent_categories_id != null)
			{
				$dataInsert['parent_categories_id'] = $parent_categories_id;
			}
			$this->db->insert('categories', $dataInsert);
			return $dataInsert['categories_id'];
		}
	}
	public function get_spec_name_id_or_insert($spec_name)
	{
		if($spec_name == "item_name"){

		}else{
			$this->db->select('spec_name_id');
			$this->db->from('spec_name_table');
			$this->db->where('UPPER(spec_name)', strtoupper($spec_name));
			$data = $this->db->get()->result_array();
			if(count($data)>0)
			{
				return $data[0]['spec_name_id'];
			}
			else
			{
				$this->db->select('UUID() AS `uuid`');
				$dataInsert=array();
				$dataInsert['spec_name_id'] = $this->db->get()->row_array()['uuid'];	 		
				$dataInsert['spec_name'] = $spec_name;			
				$dataInsert['stores_id'] = "327";			
				$this->db->insert('spec_name_table', $dataInsert);
				return $dataInsert['spec_name_id'];
			}
		}
		
	}
	public function get_item_id_or_insert($item_name,$categories_id)
	{
		// $this->db->select('items_id');
	 // 	$this->db->from('items');
	 // 	$this->db->where('UPPER(items_name)', strtoupper($item_name));
	 // 	$data = $this->db->get()->result_array();
	 // 	if(count($data)>0)
	 // 	{
		// 	return $data[0]['items_id'];
	 // 	}
	 // 	else
	 // 	{
		$this->db->select('UUID() AS `uuid`');
		$dataInsert=array();
		$dataInsert['items_id'] = $this->db->get()->row_array()['uuid'];	 		
		$dataInsert['items_name'] = $item_name;
		$dataInsert['categories_id'] = $categories_id;
		$dataInsert['stores_id'] = "327";			
		$this->db->insert('items', $dataInsert);
		return $dataInsert['items_id'];
	 	// }
	}
	public function get_spec_value_or_id_insert($spec_value,$spec_name_id)
	{		
		$this->db->select('spec_value_id');
		$this->db->from('spec_value');
		$this->db->where('UPPER(spec_value)', strtoupper($spec_value));
		$data = $this->db->get()->result_array();
		if(count($data)>0)
		{
			return $data[0]['spec_value_id'];
		}
		else
		{
			$this->db->select('UUID() AS `uuid`');
			$dataInsert=array();
			$dataInsert['spec_value_id'] = $this->db->get()->row_array()['uuid'];	 		
			$dataInsert['spec_value'] = $spec_value;
			$dataInsert['spec_name_id'] = $spec_name_id;
			$dataInsert['stores_id'] = "327";			
			$this->db->insert('spec_value', $dataInsert);
			return $dataInsert['spec_value_id'];
		}
	}
}
/* End of file test.php */
/* Location: ./application/controllers/test.php */