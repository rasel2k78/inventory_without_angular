<?php
/**
 * Dashboard Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for dashboard view to users. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Dashboard extends CI_Controller
{

    /**
 * This is a constructor function which load inventory model and user_access_check_model and language files
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();
     
     $this->load->model('Inventory_model');
     $this->load->model('User_access_check_model');
     $this->load->model('Item_model');

     $cookie = $this->input->cookie('language', true);
     $this->lang->load('dashboard_lang', $cookie);
     $this->lang->load('left_side_nev_lang', $cookie);
     userActivityTracking();
     
     $user_id = $this->session->userdata('user_id');
     if ($user_id == null) {
      redirect('Login', 'refresh');
    }
  }

    /**
 * Load view page of dashboard
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function index()
    {
     load_header();
     $user_id= $this->session->userdata('user_id');
     $this->load->view('dashboard/dashboard_view_page');
     load_footer();

         // 	echo "<pre>";
         // 	print_r(date('Y-m-d', strtotime('today - 30 days')));
         // 	exit();
   }

    /**
 * Show the item list for datatable based on safely stock calculation
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function shortage_item_info_for_dashboard()
    {

     $filters = $this->input->get();
          //print_r($filters);exit;
          // $this->load->model('Inventory_model');
     $all_data = $this->Item_model->shortage_item_info_for_dashboard($filters);
     $all_data_without_limit = $this->Item_model->shortage_item_info_for_dashboard($filters, true);
     $all_data_final = $this->Item_model->shortage_item_info_for_dashboard($filters, true);

     $output_data=[];

     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;

     echo json_encode($output_data);
   }

    /**
 * Show todays sell list for datatable
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_todays_sell_list_info_for_datatable()
    {
     $filters = $this->input->get();
           // print_r($filters);
          // $this->load->model('Inventory_model');
     $all_data = $this->Inventory_model->todays_sell_list_all_info($filters);
     $all_data_without_limit = $this->Inventory_model->todays_sell_list_all_info($filters, true);
     $all_data_final = $this->Inventory_model->todays_sell_list_all_info($filters, true);

     $output_data=[];

     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;

     echo json_encode($output_data);
   }


    /**
     * This function is used for AJAX datatable. This function load all todays buy data  for dashboard 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function all_todays_buy_list_info_for_datatable()
    {

      $filters = $this->input->get();
      
        // $this->benchmark->mark('start');

      $all_data = $this->Inventory_model->todays_buy_list_all_info($filters);
        // $this->benchmark->mark('start1');
      
      $all_data_without_limit = $this->Inventory_model->todays_buy_list_all_info($filters, true);
        // $this->benchmark->mark('start2');
      $all_data_final = $this->Inventory_model->todays_buy_list_all_info($filters, true);

      $output_data=[];

        // $this->benchmark->elapsed_time('start', 'start1');
        // $this->benchmark->elapsed_time('start1', 'start2');
      $output_data["draw"]=$filters['draw'];
      $output_data["recordsTotal"]=$all_data_without_limit;
      $output_data["recordsFiltered"]=$all_data_final;
      $output_data["data"]=$all_data;

      echo json_encode($output_data);
    }

    /**
 * Show todays staffwise total sell amount
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function todays_total_sell_by_staff()
    {

     $filters = $this->input->get();

     $all_data = $this->Inventory_model->total_sell_amount_by_staff($filters);
     $all_data_without_limit = $this->Inventory_model->total_sell_amount_by_staff($filters, true);
     $all_data_final = $this->Inventory_model->total_sell_amount_by_staff($filters, true);

     $output_data=[];

     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;

     echo json_encode($output_data);
   }

    /**
 * Dispaly latest cashbox amount + todays buy amount + todays sell amount + todays total expenses
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com> 
 **/

    public function curr_cash_amt_todays_buy_and_expense()
    {
     $data= array();
     $data['current_cashbox_amount'] = number_format($this->Inventory_model->final_cashbox_amount_info_for_dashboard(), 2);
     $data['todays_buy'] = number_format($this->Inventory_model->current_day_total_buy(), 2);
     $data['todays_expense']= number_format($this->Inventory_model->current_day_total_expense(), 2);
     $data['total_transfer']= number_format($this->Inventory_model->current_day_total_transfer(), 2);
     $data['total_receive']= number_format($this->Inventory_model->current_day_total_receive(), 2);
     $user_id= $this->session->userdata('user_id');
     if($this->session->userdata('user_role')=="staff") 
     {
      $data['todays_sell'] = number_format($this->Inventory_model->total_sell_by_staff($user_id), 2);
    }
    else
    {
      $data['todays_sell'] = number_format($this->Inventory_model->current_day_total_sell(), 2);    
    }

    // echo '<pre>';print_r($data);exit;
    echo json_encode($data);
  }

  public function set_vat_include_yes()
  {
    $session_data= array(
      'is_vat_included' => 'yes',
      );
    $this->session->set_userdata($session_data);


  }
  public function set_vat_include_no()
  {
    $session_data= array(
      'is_vat_included' => 'no',
      );
    $this->session->set_userdata($session_data);

  }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */