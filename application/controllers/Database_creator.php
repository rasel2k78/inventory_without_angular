<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Database_creator extends CI_Controller {



	public function __construct()

	{

		parent::__construct();

	}



	public function index()

	{

		$body_data= array(



			//'all_categories' => $this->Item_model->all_categories_info(),

		);
		
		//$this->output->cache(1440);

		load_header();

		$this->load->view('category/add_category_form');

		load_footer();	

	}

	public function import_database_test()
	{
		$this->load->library('curl');

		$folder_name = $this->uri->segment(3);
		$shop_id     = $this->uri->segment(4);
		$url = "https://users.pos2in.com/".$folder_name."/Database_creator/backup_files/".$shop_id; 
		$output = file_get_contents($url);
		$jsonSqlLists = json_decode($output);
		foreach ($jsonSqlLists as $key => $value) 
		{
			$file_location = 'https://users.pos2in.com/'.$folder_name.'/shop_'.$shop_id.'/'.$value;
			$status = $this->import_sql_local($file_location);
			
			$unlinkUrl = "https://users.pos2in.com/".$folder_name."/Database_creator/remove_backup_files/".$shop_id.'/'.$value;
			$result = file_get_contents($unlinkUrl); 
		}
		if($result=="success")
		{
			$this->db->query("truncate table sync");
			echo "Imported successfully";
		}
		else
		{
			echo "Data import failed";
		}

	}

	public function import_sql_local($file_location)
	{

		$host = "localhost";
		$uname = "root";
		$pass = "";
		$database = $this->db->database;
		//$database = "pppppppp"; //Change Your Database Name
		$conn = new mysqli($host, $uname, $pass, $database);
		$op_data = '';

		$lines = file($file_location);
		if(!empty($lines))
		{
			foreach ($lines as $line)
			{
		    if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE
		    {
		    	continue;
		    }

		    $op_data .= $line;

		    if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY
		    {
		    	$conn->query($op_data);

		    	$op_data = '';
		    }
		}
		return 'succeded';
	}	
}


public function import_database()

{

	$current_directory = getcwd();

	$host = "localhost";

	$uname = "root";

	$pass = "";

		$database = "Test_new"; //Change Your Database Name

		$conn = new mysqli($host, $uname, $pass, $database);

		$filename = 'full_inventory.sql'; //How to Create SQL File Step : url:http://localhost/phpmyadmin->detabase select->table select->Export(In Upper Toolbar)->Go:DOWNLOAD .SQL FILE

		$op_data = '';

		$lines = file($filename);

		foreach ($lines as $line)

		{

		    if (substr($line, 0, 2) == '--' || $line == '')//This IF Remove Comment Inside SQL FILE

		    {

		    	continue;

		    }

		    $op_data .= $line;

		    if (substr(trim($line), -1, 1) == ';')//Breack Line Upto ';' NEW QUERY

		    {

		    	$conn->query($op_data);

		    	$op_data = '';

		    }

		}
		
		echo "Table Created Inside " . $database . " Database.......";

	}

}



/* End of file Database_creator.php */

/* Location: ./application/controllers/Database_creator.php */