<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class User_access extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->model('User_access_model');
    $this->load->model('User_access_check_model');
    $this->lang->load('user_access_lang', 'bangla');
    $cookie = $this->input->cookie('language', true);
    $this->lang->load('user_access_form_lang', $cookie);

    $this->lang->load('left_side_nev_lang', $cookie);
    userActivityTracking();

    $user_id = $this->session->userdata('user_id');
    if ($user_id == null) {

      redirect('Login', 'refresh');
    }
  }

  
  public function index()
  {

    load_header();
    $data['page_list'] = $this->User_access_model->get_page_name_and_actions();

        // print_r($data);exit();
    $this->load->view('user/user_access_form', $data);
    load_footer();
  }


    /**
     * all_users_info_for_datatable method is for loading all users data at initial step for data table
     * 
     * @return null
     * @author Musabbir 
     **/
    public function all_users_info_for_datatable()
    {
     $filters = $this->input->get();
     $all_data = $this->User_access_model->all_users_info($filters);
     $all_data_without_limit = $this->User_access_model->all_users_info($filters, true);
     $all_data_final = $this->User_access_model->all_users_info($filters, true);

     $output_data=[];

     $output_data["draw"]=$filters['draw'];
     $output_data["recordsTotal"]=$all_data_without_limit;
     $output_data["recordsFiltered"]=$all_data_final;
     $output_data["data"]=$all_data;
     echo json_encode($output_data);
   }


    /**
     * show_all_check_box method is for showing all checkboxex when useraccess page loaded first
     * 
     * @return null
     * @author Rasel 
     **/
    public function show_all_check_box($page)
    {
      $page_info = $this->User_access_model->get_page_details_by_page($page);
      return $page_info;
    }


    /**
     * get_page_access_by_user_id method is for getting page access permission by searched_user_id
     * 
     * @return json / string
     * @author Musabbir 
     **/
    public function get_page_access_by_user_id($searched_user_id)
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

      echo json_encode($this->User_access_model->get_page_access_by_user_id($searched_user_id));
      
    }else{
      echo "No permission";
    }
  }


    /**
     * get_page_name_and_actions method is for page names & actions
     *
     * @author Musabbir 
     **/
    public function get_page_name_and_actions()
    {
     $data['page_list'] = $this->User_access_model->get_page_name_and_actions();
     $this->load->view('user/user_access_form', $data);
   }


    /**
     * set_or_remove_user_access method is for adding or removing page access for an user
     * it based on checked box. If checked then access is given & if unchecked then access is removed.
     * Based on access adding or removing "add_or_remove_on_page_permission_history" function is called for 
     * keeping history.  
     * 
     * @return string
     * @author Musabbir 
     **/
    public function set_or_remove_user_access()
    {

     $checked_or_unchecked = $this->input->post("checked_or_unchecked");
     $user_id = $this->input->post("edited_user_id");
     $page_id = $this->input->post("page_id");
     $user_id_permitted_by = $this->session->userdata('user_id');

     $user_role = $this->session->userdata('user_role');
     $user_id_from_session = $this->session->userdata('user_id');


     if($user_role == "manager" && ($user_id == $user_id_from_session)){
      echo "No permission";
      exit();
    }


    $get_role_by_user_id = $this->User_access_model->get_user_role_by_id($user_id);
    
    if($get_role_by_user_id['user_role'] == "owner" && $user_role !="owner"){
      echo "No permission";
      exit();
    }

         // if($page_id == "1a6c05a3-b918-11e5-a956-eca86bfd5e5e" && $user_role == "owner"){
         // 	echo "can not revoke";
         // }else{

    if($checked_or_unchecked == "checked") {
      $set_access = $this->User_access_model->set_user_access($user_id, $page_id, $user_id_permitted_by);
      if($set_access) {
        echo "Access added";
        $action_taken = "access_added";
        $this->User_access_model->add_or_remove_on_page_permission_history($page_id, $user_id, $user_id_permitted_by, $action_taken);
      }
      else{
        echo "Access add fail";
      }
    }
    else{
      $set_access = $this->User_access_model->remove_user_access($user_id, $page_id);
      if($set_access) {
        echo "Access removed";
        $action_taken = "access_removed";
        $this->User_access_model->add_or_remove_on_page_permission_history($page_id, $user_id, $user_id_permitted_by, $action_taken);
      }
      else{
        echo "Access remove fail";
      }
    }
         // }

  }

}

/* End of file User_access.php */
/* Location: ./application/controllers/User_access.php */