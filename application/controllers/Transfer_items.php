<?php
/**
 * Exchange with Transfer_items  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for transfer items to different shop 
 *
 * @package Controller
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/

class Transfer_items extends CI_Controller
{
    /**
 * Constructor function 
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();

     $this->load->model('Exchange_with_customer_model');
     $this->load->model('Inventory_model');
     $this->load->model('Transfer_model');
     $this->load->model('Item_model');
     $this->load->model('User_access_check_model');
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('left_side_nev_lang', $cookie);
     $this->lang->load('transfer_item_form_lang', $cookie);
     $this->lang->load('transfer_receive_item_lang', $cookie);
     $user_account_folder = $this->Transfer_model->get_folder_name();
     userActivityTracking();
     
     $user_id = $this->session->userdata('user_id');
     if ($user_id == null) {

      redirect('Login', 'refresh');
    }

    $status = $this->check_internet();
    if($status==1) {
      load_header();
      $this->load->view('no_internet');
      load_footer();
    }

  }

    /**
 * loads basic Transfer form
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function index()
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     load_header();
     $status = $this->check_internet();
     if($status==1) {
      $this->load->view('no_internet');
    }
    else
    {
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $shop_info = $this->Transfer_model->get_my_shop();
        $user_account_folder = $this->Transfer_model->get_folder_name();
        $owner_info = $this->Transfer_model->get_my_account()->row_array();
        $find_other_shops ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_synched_shops/".$shop_info['shop_id'];
        $shops_output=file_get_contents($find_other_shops); 
        $shops = json_decode($shops_output);
        $body_data= array(
         'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
         'shop_info' => $shop_info,
         'other_shops' => $shops,
         'owner_info' => $owner_info,
         );
        $this->load->view('Transfer/Transfer_items_form', $body_data);
      }
      else
      {
        $this->load->view('access_deny/not_permitted');
      }
    }
    
    load_footer();
  }
    /**
 * all transfer items information save to database
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function pull_items()
    {
      load_header();
      $status = $this->check_internet();
      if($status==1) 
      {
        $this->load->view('no_internet');
      }
      else
      {
        $shop_info = $this->Transfer_model->get_my_shop();
        $user_account_folder = $this->Transfer_model->get_folder_name();
        $owner_info = $this->Transfer_model->get_my_account()->row_array();
        $find_other_shops ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_synched_shops/".$shop_info['shop_id'];
                //echo $find_other_shops;exit;
        $shops_output=file_get_contents($find_other_shops); 
        $shops = json_decode($shops_output);
                //echo '<pre>';print_r($shops).'</pre>';exit;
        $body_data= array(
         'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
                 // 'store' => $store_information,
         'shop_info' => $shop_info,
         'other_shops' => $shops,
         'owner_info' => $owner_info,
         );
        $this->load->view('Transfer/Transfer_pull_items_from_shops', $body_data);
      }
      load_footer();
    }

/**
 * Item pull from shop form
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function pull_items_warehouse()
{
 load_header();
 $status = $this->check_internet();
 if($status==1) 
 {
  $this->load->view('no_internet');
}
else
{
  $shop_info = $this->Transfer_model->get_my_shop();
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $owner_info = $this->Transfer_model->get_my_account()->row_array();
  $body_data= array(
   'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
   'shop_info' => $shop_info,
   'owner_info' => $owner_info,
   );
  $this->load->view('Transfer/Transfer_pull_items_from_warehouse', $body_data);
}
load_footer();
}

/**
 * Item pull from warehouse shop server and insert to current shop
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function pull_shop_items_from_warehouse()
{

  $shop_info = $this->Transfer_model->get_my_shop();
  $target_shop_id = $this->input->post('stores_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $request_for_items ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_spec_names_warehouse";
  $output = file_get_contents($request_for_items); 
  $result = json_decode($output);
  $users = $result->users;
  $spec_names = $result->spec_names;
  $spec_values = $result->spec_values;
  $categories = $result->categories;
  $items_name = $result->items_name;
  $item_spec_set = $result->item_spec_set;
  $buy_details = $result->buy_details;
  $buy_cart_items = $result->buy_cart_items;
  $spec_connector_with_values = $result->spec_connector_with_values;
  $iif = 0;
  $ielse = 0;
  foreach ($spec_names as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_name_id($value->spec_name_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_name_table",$value->spec_name_id,"spec_name_id",$value);
    }
    else
    {
      $if_exist_spec_name = $this->Transfer_model->get_spec_name_by_name($value->spec_name);
      if($if_exist_spec_name)
      {
        $tables  = array("spec_name_table","spec_value","spec_set_connector_with_values");
        foreach ($tables as $key => $table) 
        {
          $data['spec_name_id'] = $value->spec_name_id;
          $update_new_spec_name = $this->Transfer_model->update_spec_names($data,$if_exist_spec_name['spec_name_id'],$table);
        }
      }
      else
      {
        $this->Transfer_model->insert_on_pull_shop("spec_name_table",$value);
      }
    }  
  }

  foreach ($spec_values as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_value_id($value->spec_value_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_value",$value->spec_value_id,"spec_value_id",$value);
    }
    else
    {
     $this->Transfer_model->insert_on_pull_shop("spec_value",$value);
   }
 }
 foreach ($categories as $key => $value)
 {
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_category_id($value->categories_id);
  if($check_if_exist)
  {
    $value->publication_status = "activated";
    $this->Transfer_model->update_on_pull_shops("categories",$value->categories_id,"categories_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("categories",$value);
  }
}

foreach ($items_name as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_items_id($value->items_id);
  if($check_if_exist)
  {
    $value->publication_status = "activated";
    $this->Transfer_model->update_on_pull_shops("items",$value->items_id,"items_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("items",$value);
  }
}
foreach ($item_spec_set as $key => $value)
{
  if($value)
  {
    $value['user_id'] = $this->session->userdata('user_id');
    $value['stores_id'] = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_item_spec_set_id($value['item_spec_set_id']);
    if($check_if_exist)
    {
     $value['publication_status'] = "activated";
     $check_barcode_else_exist = $this->Item_model->check_barcode_else_exist($value['barcode_number'],$value['item_spec_set_id']);
     if($check_barcode_else_exist)
     {
      unset($value['barcode_number']);
    }
    $this->Transfer_model->update_on_pull_shops("item_spec_set",$value['item_spec_set_id'],"item_spec_set_id",$value);
  }
  else
  {
    $check_barcode_exist = $this->Item_model->check_barcode_if_exist($value['barcode_number']);
    if($check_barcode_exist)
    {
      $value['barcode_number']  = $this->generateRandomString();
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
  }
} 
}

foreach ($buy_details as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_buy_details($value->buy_details_id);
  if($check_if_exist)
  {
   // $value->publication_status = "activated";
   // $this->Transfer_model->update_on_pull_shops("item_spec_set",$value->item_spec_set_id,"item_spec_set_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("buy_details",$value);
  }  
}

foreach ($buy_cart_items as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_buy_cart_items($value->buy_cart_items_id);
  if($check_if_exist)
  {
   // $value->publication_status = "activated";
   // $this->Transfer_model->update_on_pull_shops("item_spec_set",$value->item_spec_set_id,"item_spec_set_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("buy_cart_items",$value);
  }  
}

foreach ($spec_connector_with_values as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_item_spec_connector_id($value->spec_set_connector_id);
  if($check_if_exist)
  {
    if($value->publication_status=="deactivated")
    {
      $value->publication_status = "deactivated";
    }
    else
    {
      $value->publication_status = "activated";
    }
    $this->Transfer_model->update_on_pull_shops("spec_set_connector_with_values",$value->spec_set_connector_id,"spec_set_connector_id",$value);

  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("spec_set_connector_with_values",$value);
  } 
}
echo json_encode('success');exit;
}

/**
 * Item pull from warehouse shop server and insert to current shop
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function pull_shop_items_from_server()
{
  $shop_info = $this->Transfer_model->get_my_shop();
  $target_shop_id = $this->input->post('stores_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $request_for_items ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_spec_names_by_shop/".$target_shop_id;
  $output = file_get_contents($request_for_items); 
  $result = json_decode($output);
  $users = $result->users;
  $spec_names = $result->spec_names;
  $spec_values = $result->spec_values;
  $categories = $result->categories;
  $items_name = $result->items_name;
  $item_spec_set = $result->item_spec_set;
  $spec_connector_with_values = $result->spec_connector_with_values;
  $iif = 0;
  $ielse = 0;
  foreach ($spec_names as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_name_id($value->spec_name_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_name_table",$value->spec_name_id,"spec_name_id",$value);
    }
    else
    {
      $if_exist_spec_name = $this->Transfer_model->get_spec_name_by_name($value->spec_name);
      if($if_exist_spec_name)
      {
        $tables  = array("spec_name_table","spec_value","spec_set_connector_with_values");
        foreach ($tables as $key => $table) 
        {
          $data['spec_name_id'] = $value->spec_name_id;
          $update_new_spec_name = $this->Transfer_model->update_spec_names($data,$if_exist_spec_name['spec_name_id'],$table);
        }
      }
      else
      {
        $this->Transfer_model->insert_on_pull_shop("spec_name_table",$value);
      }
    }  
  }

  foreach ($spec_values as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_value_id($value->spec_value_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_value",$value->spec_value_id,"spec_value_id",$value);
    }
    else
    {
     $this->Transfer_model->insert_on_pull_shop("spec_value",$value);
   }
 }

 foreach ($categories as $key => $value)
 {
  if($value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_category_id($value->categories_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("categories",$value->categories_id,"categories_id",$value);
    }
    else
    {
      $check_exist = $this->Item_model->check_category_exist($value->categories_name);
      if($check_exist)
      {
        $value->publication_status = "activated";
        $this->Transfer_model->update_on_pull_shops("categories",$value->categories_id,"categories_id",$value);
      }
      else
      {

        $this->Transfer_model->insert_on_pull_shop("categories",$value);
      }
    }
  }
}
foreach ($items_name as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_items_id($value->items_id);
  if($check_if_exist)
  {
    $value->publication_status = "activated";
    $this->Transfer_model->update_on_pull_shops("items",$value->items_id,"items_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("items",$value);
  }
}
foreach ($item_spec_set as $key => $value)
{
  if($value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_item_spec_set_id($value->item_spec_set_id);
    if($check_if_exist)
    {
     $value->publication_status = "activated";
     $check_barcode_else_exist = $this->Item_model->check_barcode_else_exist($value->barcode_number,$value->item_spec_set_id);
     if($check_barcode_else_exist)
     {
      unset($value->barcode_number);
    }
    $this->Transfer_model->update_on_pull_shops("item_spec_set",$value->item_spec_set_id,"item_spec_set_id",$value);
  }
  else
  {
    $check_barcode_exist = $this->Item_model->check_barcode_if_exist($value->barcode_number);
    if($check_barcode_exist)
    {
      $value->barcode_number  = $this->generateRandomString();
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
  }
}  
}

foreach ($spec_connector_with_values as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_item_spec_connector_id($value->spec_set_connector_id);
  if($check_if_exist)
  {
    if($value->publication_status=="deactivated")
    {
      $value->publication_status = "deactivated";
    }
    else
    {
      $value->publication_status = "activated";
    }
    $this->Transfer_model->update_on_pull_shops("spec_set_connector_with_values",$value->spec_set_connector_id,"spec_set_connector_id",$value);

  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("spec_set_connector_with_values",$value);
  } 
}
echo json_encode("success");
}

/**
 * Generating default unique barcode by random values
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function generateRandomString()
{
  $length = 6;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  $check_barcode_exist = $this->Item_model->check_barcode_if_exist($randomString);
  if($check_barcode_exist)
  {
    $this->generateRandomString();
  }
  else
  {        
    return $randomString;
  }
}
/**
 * Getting all synced shops from server
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_all_shops()
{

 $shop_info = $this->Transfer_model->get_my_shop();
 $create_shop_backup_url ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_synched_shops/".$shop_info['shop_id'];
 $output=file_get_contents($create_shop_backup_url); 
 echo $output;

}

/**
 * Checking internet availability
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function check_internet()
{
 $connected = @fsockopen('www.google.com', 80);
 if($connected) {
  return 0;
}
else
{
  return 1;    
}
}

/**
 * Save transfer items to database
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function save_transfer_info()
{
 $data = $this->input->post('all_data');
 $error_list = array();
 foreach ($data['item_info'] as $key => $value) 
 {
  
  $return_data = $this->Transfer_model->check_items($value);
  if($value['item_quantity'] == null) {
    array_push($error_list, 'PLease Enter Item Quantity');
  }
  if($value['item_quantity'] > $return_data['quantity']) {
    array_push($error_list, 'Item does not have enough quantity');
  }
  if($value['selling_price'] ==null) {
    array_push($error_list, 'price not given');
  }
  if($value['selling_price'] !=$return_data['retail_price']) {
    array_push($error_list, 'price not perfect');
  }
  if($value['buying_price'] !=$return_data['buying_price']) {
    array_push($error_list, 'price not perfect');
  }
}
if(count($error_list)>0) {
  echo json_encode($error_list);
  exit();
}

$ready_data_basic = array();
$ready_data_basic['transfer_from'] = $data['all_basic_data']['store_form_id'];
$ready_data_basic['transfer_to'] = trim($data['all_basic_data']['store_to_id']);
$ready_data_basic['total_item_quantity'] = $data['all_basic_data']['total_quantity'];
$ready_data_basic['total_amount'] = $data['all_basic_data']['total_amount'];

$ready_data_basic['status'] = "sent";
$ready_data_basic['user_id'] = $this->session->userdata('user_id');
$transfer_details_id = $this->Transfer_model->insert_transfer_data($ready_data_basic);

if($transfer_details_id) 
{
  foreach ($data['item_info'] as $key => $transfer_items) 
  {
    $tdata['transfer_details_id'] = $transfer_details_id;
    $tdata['item_spec_set_id'] = $transfer_items['item_spec_set_id'];
    $tdata['quantity'] = $transfer_items['item_quantity'];
    $tdata['buying_price'] = $transfer_items['buying_price'];
    $tdata['selling_price'] = $transfer_items['selling_price'];
    $tdata['user_id'] = $this->session->userdata('user_id');
    $find_item_quantity_on_inventory = $this->Transfer_model->get_items_inventory($transfer_items['item_spec_set_id']);
    $update_inventory_data['quantity'] = $find_item_quantity_on_inventory['quantity'] - $transfer_items['item_quantity'];
    $this->Transfer_model->update_inventory_transfered_items($update_inventory_data,$transfer_items['item_spec_set_id']);
    $save_transfer_items = $this->Transfer_model->save_transfer_items($tdata);
  }
}
if($save_transfer_items)
{
  $this->session->set_userdata('transfered', 1);
}
echo json_encode("success");
}

public function update_transfer_sent()
{
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $transfer_details_id = $this->input->post('transfer_details_id');
  $total_amount = $this->input->post('total_amount');
  // $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
  // $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $total_amount;
  // $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);
  // $cash_data['cash_type']= "transfer";
  // $cash_data['user_id'] = $this->session->userdata('user_id');
  // $cash_data['deposit_withdrawal_amount'] = $total_amount;
  // $this->Inventory_model->save_cash_box_info($cash_data);
  $data['status'] = 'completed';
  $update_transfer_status = $this->Transfer_model->update_transfer_list_completed($transfer_details_id, $data);
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_sent/".$transfer_details_id;
  $output=file_get_contents($create_shop_backup_url); 
  if($output) {
    echo "success";
  }
}


public function transfered_items()
{
 $transfer_details_id = $this->input->post('transfer_details_id');
 $transfer_items = $this->Transfer_model->get_transfer_items_by_details_id($transfer_details_id);
 echo json_encode($transfer_items);
 exit;
         //echo '<pre>'; print_r($transfer_items);exit;
}

public function confirm_receive_transfer()
{
 $user_account_folder = $this->Transfer_model->get_folder_name();
 $data = $this->input->post('all_data');
 $error_list = array();
 $transfer_from_id = $data['all_basic_data']['transfer_from_id'];
         //echo '<pre>'.print_r($this->input->post());exit;
 $transfer_details_id = $data['all_basic_data']['transfer_details_id'];
 $totalAmount = $data['all_basic_data']['total_amount'];
 $totalQuantity = $data['all_basic_data']['total_quantity'];
 foreach ($data['item_info'] as $key => $value) 
 {
  $find_item_quantity_on_inventory = $this->Transfer_model->get_items_inventory($value['matched_item_spec_id']);
  $udata = array();
  $udata['quantity'] = $value['quantity'];
  $udata['item_spec_set_id'] = $value['matched_item_spec_id'];
  if(!empty($find_item_quantity_on_inventory)) 
  {
    $udata['quantity'] = $find_item_quantity_on_inventory['quantity'] + $value['quantity'];
    $this->Transfer_model->update_inventory_transfered_items($udata, $value['matched_item_spec_id']);
  }
}
$create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_received/".$transfer_details_id;
$output=file_get_contents($create_shop_backup_url); 
$status_update = json_decode($output);
if($status_update) {
  echo 'success';exit;
}    
}

public function transfer_list()
{
 $status = $this->check_internet();
 if($status==1)
 {
  load_header();
  $this->load->view('no_internet');
  load_footer();
}
else
{
  load_header();
  $user_account_folder = $this->Transfer_model->get_folder_name();

  $shop_info = $this->Transfer_model->get_my_shop();
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_transfer_item_details/".$shop_info['shop_id'];
  $output=file_get_contents($create_shop_backup_url); 
  $result = json_decode($output, true);
  $body_data= array(
   'transfer_list' => $result,
   );
  $this->load->view('Transfer/Transfer_list_form', $body_data);
  load_footer();
}
}

/**
 * Show warehouse receive item list
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function receivable_store_items()
{
 $status = $this->check_internet();
 if($status==1) {
  load_header();
  $this->load->view('no_internet');
  load_footer();
}
else
{
  $user_account_folder = $this->Transfer_model->get_folder_name();

  $shop_info = $this->Transfer_model->get_my_shop();
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_if_transfer_receives/".$shop_info['shop_id'];


  $output=file_get_contents($create_shop_backup_url); 
            //echo $output;exit;
  $result = json_decode($output, true);
  $transfer_details_id = $this->uri->segment(3);
  $transfer_details = $this->Transfer_model->get_transfer_details($transfer_details_id)->row_array();
  $body_data= array(
   'transfer_details' => $transfer_details,
   'transfer_in' => $result,
   );
  load_header();
  $this->load->view('Transfer/receive_store_items', $body_data);
  load_footer();
}
}

/**
 * Update cashbox amount
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_cashbox()
{
  $cash_amount = $this->input->post('cash_amount');
  $transfer_details_id = $this->input->post('transfer_details_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
  $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $cash_amount;
  $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);
  if($cashbox_update)
  {
    $change_transfer_last_status = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_adjust_status/".$transfer_details_id;
    $output=file_get_contents($change_transfer_last_status); 
    $result = json_decode($output, true);
    echo $result;exit;
  }
}
/**
 * Update cashbox amount
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_own_cashbox()
{
  $cash_amount = $this->input->post('cash_amount');
  
  $transfer_details_id = $this->input->post('transfer_details_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
  $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $cash_amount;
  $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);
  
  $change_transfer_last_status = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_adjust_status_finish/".$transfer_details_id;
  $output=file_get_contents($change_transfer_last_status); 
  $result = json_decode($output, true);
  echo $result;exit;

}
   /**
 * receive transfer items details information and view
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function receivable_transfer_details()
   {

     $status = $this->check_internet();
     if($status==1) {
      load_header();
      $this->load->view('no_internet');
      load_footer();
    }
    else
    {
      $user_account_folder = $this->Transfer_model->get_folder_name();

      $shop_info = $this->Transfer_model->get_my_shop();
      $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_if_transfer_receives/".$shop_info['shop_id'];


      $output=file_get_contents($create_shop_backup_url); 
            //echo $output;exit;
      $result = json_decode($output, true);
      $transfer_details_id = $this->uri->segment(3);
      $transfer_details = $this->Transfer_model->get_transfer_details($transfer_details_id);
      $body_data= array(
       'transfer_details' => $transfer_details,
       'transfer_in' => $result,
       );
      load_header();
      $this->load->view('Transfer/Transfer_receive_items_form', $body_data);
      load_footer();
    }    
  }


/**
 * Update warehouse transfer status
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_received_transfer_info()
{
 $transfer_details_id = $this->uri->segment(3);
 $transfer_information  = $this->Transfer_model->get_transfer_details($transfer_details_id);
 $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
 $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $transfer_information['total_amount'];
 $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);

 $data['publication_status'] = "deactivated";
 $update = $this->Transfer_model->update_transfer_status($data, $transfer_details_id);
 if($update) {
  redirect('Transfer_items', 'refresh');
}
exit;
}

    /**
* Check items qauantity from inventory
 *
* @param  string $item_spec_set_id 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function check_items_on_inventory($item_spec_set_id)
{
  $current_items_on_inventory = $this->Transfer_model->get_items_inventory($item_spec_set_id);
  echo json_encode($current_items_on_inventory);
  exit;
}
    /**
* find transfer items by transfer details id
 *
* @param  null 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function get_transfer_items_by_transfer_details()
{
 $user_account_folder = $this->Transfer_model->get_folder_name();

 $detailsId = $this->input->post('keyword');
         //echo $detailsId;exit;
         //$transfer_items = $this->Transfer_model->get_transfer_items_by_details_id($detailsId);

 $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_transfer_items/".$detailsId;
 // echo $create_shop_backup_url;exit;
 $output=file_get_contents($create_shop_backup_url); 
         //echo json_encode($transfer_items);
 echo $output;
 exit;
}


    /**
* find stores on select search
 *
* @param  null 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function search_store_by_name()
{
 $text = $this->input->post('q');
 $currentShopId = $this->session->userdata('stores_id');
 $result = $this->Transfer_model->get_other_stores($text, $currentShopId);
 echo json_encode($result->result_array());
}



}

/* End of file Transfer_items.php */
/* Location: ./application/controllers/Transfer_items.php */
?>