<?php 
/**
 * Contact Us  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Use this controller for showing sales and support executive's information . 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Contact_us extends CI_Controller {
	    /**
     * This is a constructor function
     *
     * This load language files when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
	    public function __construct()
	    {
	    	parent::__construct();
	    	$cookie = $this->input->cookie('language', true);
	    	$this->lang->load('left_side_nev_lang', $cookie);
	    	$this->lang->load('contact_us_lang', $cookie);
	    	userActivityTracking();
	    	
	    	$user_id = $this->session->userdata('user_id');
	    	if ($user_id == null) {
	    		redirect('Login', 'refresh');
	    	}
	    }
	        /**
     * This function is used for loading the contact us page. 
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
	        public function index()
	        {
	        	load_header();
	        	$this->load->view('contact_us/contact_us_page');
	        	load_footer();
	        }
	    }
	    /* End of file Contact_us */
/* Location: ./application/controllers/Contact_us */