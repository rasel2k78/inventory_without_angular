<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Item_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('add_category_form_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        $this->load->model('User_access_check_model');
        userActivityTracking();
        
    }

    public function index()
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(

             'all_categories' => $this->Item_model->all_categories_info(),
             );
            //echo '<pre>';print_r($body_data['all_categories']);
            load_header();
            $this->load->view('category/add_category_form', $body_data);
            load_footer();    
        }
        else
        {
            load_header();
            $this->load->view('access_deny/not_permitted');
            load_footer();    
        }
    }



    /**
     * This function save all cateogry informations of category form in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function save_category_info()
    {

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('categories_name', true)==null) {
            $output['error']['categories_name'] =  $this->lang->line('validation_name');
            $output['success'] = 0;
        }
        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else
        {
            $output['success'] = 1;
//echo $this->input->post('categories_name');exit;
            $check_exist = $this->Item_model->check_category_exist($this->input->post('categories_name'));
            if($check_exist) 
            {
                $dataa=array(
                 'publication_status' => 'activated',
                 'parent_categories_id' => $this->input->post('parent_categories_id', true),
                 );

                if($this->Item_model->update_category_info($check_exist['categories_id'], $dataa)) {
                  $output['data'] = $check_exist['categories_id'];
                  echo json_encode($output);
              }
              else
              {
                 $output['success'] = 0;
                 echo json_encode($output);
             }
         }
         else
         {
            $data= array(
             'categories_name' => $this->input->post('categories_name', true),
             'categories_description' => $this->input->post('categories_description', true),
             'parent_categories_id' => $this->input->post('parent_categories_id', true),
             'user_id' => $this->session->userdata('user_id')
             );

          //   if($this->input->post('parent_categories_id')) {
          //     $data['parent_categories_id'] = $this->input->post('parent_categories_id', true);
          // }
            
            $output['data'] = $this->Item_model->save_category_info($data);
            echo json_encode($output);
        }
    }

}

public function all_category_info_for_datatable()
{

    $filters = $this->input->get();
        // print_r($filters);
        // $this->load->model('Inventory_model');
    $all_data = $this->Item_model->all_category_info_for_datatable($filters);
    $all_data_without_limit = $this->Item_model->all_category_info_for_datatable($filters, true);
    $all_data_final = $this->Item_model->all_category_info_for_datatable($filters, true);

//echo '<pre>';print_r($all_data);exit;
    $output_data=[];

    $output_data["draw"]=$filters['draw'];
    $output_data["recordsTotal"]=$all_data_without_limit;
    $output_data["recordsFiltered"]=$all_data_final;
    $output_data["data"]=$all_data;

    echo json_encode($output_data);

}

public function search_category()
{
    $key_data = $this->input->post('keyword');
    
    $search_data = $this->Item_model->search_category_by_key($key_data);
    echo json_encode($search_data);exit;
}

    /**
     * This function collect all informations of individual category by parameter.
     * 
     * @param string $categories_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_category_info($categories_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
             'category_info_by_id' => $this->Item_model->category_info_by_id($categories_id),
             );
            echo json_encode($body_data);
        }
        else
        {
            echo json_encode("No permission");
        }
    }

    /**
     * This function is used for update category's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function update_category_info()
    {

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('categories_name_edit')=='') {

            $output['error']['categories_name_edit'] = $this->lang->line('validation_name');
            $output['success'] = 0;
        }

        if($output['success'] == 1) 
        {
           // echo 'hid';exit;

            $categories_id = $this->input->post('categories_id', true);
            // if($this->input->post('parent_categories_id'))
            // {
            // 	$data['parent_categories_id'] = $this->input->post('parent_categories_id', TRUE);
            // }
            $data=array(
             'categories_name' => $this->input->post('categories_name_edit', true),
             'categories_description' => $this->input->post('categories_description', true),
             'parent_categories_id' => $this->input->post('parent_categories_id', true),
             'user_id' => $this->session->userdata('user_id'),
             );

            // if($this->Item_model->update_category_info($categories_id,$data)){
            // 	echo "success";


            $update = $this->Item_model->update_category_info($categories_id, $data);
            // if($update)
            // {
            $output['success'] = 1;
            echo json_encode($output);
            //}
            // else
            // {
            // 	$output['success'] = 0;
            // 	echo json_encode($output);
            // }
        }

        else
        {
            echo json_encode($output);
        }
    }

    /**
     * This function will delete/hide  the category's information by "categories_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $categories_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_category($categories_id)
    {

        // $brands_id = $this->input->post('brands_id');
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();


        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            // if() {

            print_r($this->Item_model->delete_category_info($categories_id)) ;exit;

            echo "success";    
            // }    
            // else{

            //     echo "failed";
            // }
        }
        else
        {
            echo "No permission";
        }
    }
    /**
     * This function is used for boostrap select of cateories's information in database.
     * 
     *@param  string $categories_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function search_category_by_name()
    {
        $text = $this->input->post('q');
        if($text == null || $text=="") {
            echo "input field empty";
        }
        else{
            echo json_encode($this->Item_model->search_category($text));
        }
    }
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */