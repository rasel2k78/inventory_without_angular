<?php
/**
 * Buy Due List  Controller
 *
 * PHP Version 5.6
 * CodeIgniter 3.0
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for showing all purchse list , delete purchage info . 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/

class Buy_list_all extends CI_Controller
{

    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/


    public function __construct()
    {
        parent::__construct();

        $this->load->model('Inventory_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('buy_all_list_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }
    }

    /**
     * This function is used for loading the puchase list table. User can check details of individual purchage or delete purchage 
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
        {
            $store_details = $this->Inventory_model->store_details();
            $body_data= array(
                'store_details' => $store_details,
                );
            $this->load->view('buy_list/all_buy_list_form', $body_data);
        }
        else{
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();    
    }


    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function all_buy_info_for_datatable()
    {

        $filters = $this->input->get();

        $all_data = $this->Inventory_model->all_buy_info_list($filters);
        $all_data_without_limit = $this->Inventory_model->all_buy_info_list($filters, true);
        $all_data_final = $this->Inventory_model->all_buy_info_list($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);
    }

    /**
     * This function is used for collecting data of individual purchase. 
     *
     * @param  string $buy_details_id
     * @return array[] all individual purchase informations form database.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function get_all_buy_info($buy_details_id)
    {

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $voucher_details = $this->Inventory_model->buy_details_by_id($buy_details_id);
            $body_data= array(
             'all_buy_info_by_id' => $this->Inventory_model->details_buy_info_by_id($buy_details_id),
             'vendor_info' => $this->Inventory_model->get_vendor_info_for_voucher($voucher_details['vendors_id']),
             'voucher_number' => $this->Inventory_model->collect_voucher_number_buy($voucher_details['buy_details_id']),
             );
            // echo "<pre>";
            // print_r($body_data['all_buy_info_by_id']);
            // exit();
            echo json_encode($body_data);
        }
        else{
            echo json_encode("No Permission");
        }
    }

    /**
     * This function will delete/hide  the buy's information by "buy_details_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $buy_details_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_buy($buy_details_id)
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
         $this->db->trans_begin();
         $is_sold_already = $this->Inventory_model->check_imei_sold_already_or_not($buy_details_id);
         if(count($is_sold_already) > 0){
             $this->db->trans_rollback();
             echo "imei_of_this_voucher_already_sold";
             exit();
         }
         $data= $this->Inventory_model->item_quantity_by_buy_details_id($buy_details_id);
         $total_item = count($data);
         $inventory_check_or_update = $this->Inventory_model->update_inventory($data);
         if ( is_array($inventory_check_or_update)) {
           $this->db->trans_rollback();
           echo "delete_not_possible_for_insuff_qty";
           exit();
       }
       if($total_item != $inventory_check_or_update){
         $this->db->trans_rollback();
         echo "all_items_not_deleted";
         exit();
     }
     else{
         $this->Inventory_model->change_imei_barcode_publication_status($buy_details_id);
         $paid_amount = $this->Inventory_model->paid_amount_by_id($buy_details_id);
         $payment_by_cash = $this->Inventory_model->payment_amount_by_cash($buy_details_id);
         $payment_by_cheque = $this->Inventory_model->payment_amount_by_cheque($buy_details_id);
         if($payment_by_cash['amount'] != null){
            $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
            $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $payment_by_cash['amount']+ $paid_amount['landed_cost'];
            $this->Inventory_model->update_total_cashbox_info($final_data);
        }
        if(is_array($payment_by_cheque)){
            foreach ($payment_by_cheque as $v_of_cheque) {
                $bank_acc_id = $v_of_cheque['bank_acc_id'];
                $current_bank_amt = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id);
                $updated_amount['final_amount'] = $current_bank_amt['final_amount'] + $v_of_cheque['amount'];
                $this->Inventory_model->update_bank_acc_info($bank_acc_id, $updated_amount);
            }
        }
        $this->Inventory_model->delete_buy_info($buy_details_id);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            echo "failed";
        }
        else
        {
            $this->db->trans_commit();
            echo "success";   
        }
    }    
}
else
{
    echo "No Permission";
}
}
    /**
     *  
     * Can change purchase date to previous date or upcoming date.
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function change_buy_date_info()
    {
        $buy_details_id = $this->input->post('buy_details_id');
        $data = array(
            'date_created' => $this->input->post('change_purchase_date'),
            );
        if($data['date_created'] ==""){
            echo json_encode(array('success' =>'no'));
        }
        else{
            $this->Inventory_model->change_buy_date($buy_details_id,$data);
            echo json_encode(array('success' =>'yes'));
        }
    }

}

/* End of file Buy_list_all.php */
/* Location: ./application/controllers/Buy_list_all.php */