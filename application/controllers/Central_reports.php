<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Central_reports extends CI_Controller {
	/**
 * Construtor function load the language files and central_reports model
 *
 * @return 		void
 * @author      Shoaib <shofik.shoaib@gmail.com>
 **/
	public function __construct()
	{
		parent::__construct();

		$cookie = $this->input->cookie('language', TRUE);
		$this->load->helper('html');
		$this->lang->load('access_message_lang', $cookie);
		$this->load->model('Inventory_model');
		$this->load->model('Central_reports_model');
		$this->load->model('User_access_check_model');
		$cookie = $this->input->cookie('language', TRUE);
		$this->lang->load('report_lang', $cookie);
		$this->lang->load('left_side_nev_lang', $cookie);
		$user_id = $this->session->userdata('user_id');
		if ($user_id == NULL) {

			redirect('Login', 'refresh');
		}
	}
	/**
 * Load the central_reports view page.
 *
 * @return 		void
 * @author      Shoaib <shofik.shoaib@gmail.com>
 **/
	public function index()
	{
		load_header();
		$this->load->view('Central_reports/central_reports_page');
		// $this->load->view('Central_reports/report_template');
		load_footer();
	}
/**
 * Shows all items list with their for datatable.
 *
 * @return 		void
 * @author     Shoaib <shofik.shoaib@gmail.com>
 **/
public function items_list_with_quantity()
{
	$filters = $this->input->get();
	// print_r($filters);
	$all_data = $this->Central_reports_model->all_items_list_with_qty($filters);
	$all_data_without_limit = $this->Central_reports_model->all_items_list_with_qty($filters, true);
	$all_data_final = $this->Central_reports_model->all_items_list_with_qty($filters, true);
	$output_data=[];
	$output_data["draw"]=$filters['draw'];
	$output_data["recordsTotal"]=$all_data_without_limit;
	$output_data["recordsFiltered"]=$all_data_final;
	$output_data["data"]=$all_data;
	echo json_encode($output_data);
}
/**
 * Shows all expenses list for datatable
 *
 * @return 		void
 * @author     Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_expenses_list()
{
	$filters = $this->input->get();
	$all_data = $this->Central_reports_model->all_expenses_report_info($filters);
	$all_data_without_limit = $this->Central_reports_model->all_expenses_report_info($filters, true);
	$all_data_final = $this->Central_reports_model->all_expenses_report_info($filters, true);
	$output_data=[];
	$output_data["draw"]=$filters['draw'];
	$output_data["recordsTotal"]=$all_data_without_limit;
	$output_data["recordsFiltered"]=$all_data_final;
	$output_data["data"]=$all_data;
	echo json_encode($output_data);
}
/**
 * Shows all loan list for datatable
 *
 * @return	void
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_loan_list()
{
	$filters = $this->input->get();
	$all_data = $this->Central_reports_model->all_loan_report_info($filters);
	$all_data_without_limit = $this->Central_reports_model->all_loan_report_info($filters, true);
	$all_data_final = $this->Central_reports_model->all_loan_report_info($filters, true);
	$output_data=[];
	$output_data["draw"]=$filters['draw'];
	$output_data["recordsTotal"]=$all_data_without_limit;
	$output_data["recordsFiltered"]=$all_data_final;
	$output_data["data"]=$all_data;
	echo json_encode($output_data);
}
/**
 * Shows all buy due list for datatable
 *
 * @return	void
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_buy_due_list()
{
	$filters = $this->input->get();
	$all_data = $this->Central_reports_model->all_buy_due_report_info($filters);
	$all_data_without_limit = $this->Central_reports_model->all_buy_due_report_info($filters, true);
	$all_data_final = $this->Central_reports_model->all_buy_due_report_info($filters, true);
	$output_data=[];
	$output_data["draw"]=$filters['draw'];
	$output_data["recordsTotal"]=$all_data_without_limit;
	$output_data["recordsFiltered"]=$all_data_final;
	$output_data["data"]=$all_data;
	echo json_encode($output_data);
}
/**
 * Shows all sell due list for datatable
 *
 * @return	void
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_sell_due_list()
{
	$filters = $this->input->get();
	$all_data = $this->Central_reports_model->all_sell_due_report_info($filters);
	$all_data_without_limit = $this->Central_reports_model->all_sell_due_report_info($filters, true);
	$all_data_final = $this->Central_reports_model->all_sell_due_report_info($filters, true);
	$output_data=[];
	$output_data["draw"]=$filters['draw'];
	$output_data["recordsTotal"]=$all_data_without_limit;
	$output_data["recordsFiltered"]=$all_data_final;
	$output_data["data"]=$all_data;
	echo json_encode($output_data);
}
    /**
     * This function is used for select of item's information in database.
     * 
     *@param  string $items_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function search_item_by_name_for_report()
    {
    	$text = $this->input->post('q');
    	if($text == null || $text=="") {
    		echo "input field empty";
    	}
    	else{
    		echo json_encode($this->Central_reports_model->search_item($text));
    	}
    }
/**
 * Calculate profit loss for report
 *
 * @return	void
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function print_report_preview()
{
	$report_type= $this->input->post('cus_report_type', TRUE);
	$from_create_date= $this->input->post('from_create_date', TRUE);
	$to_create_date= $this->input->post('to_create_date', TRUE);
	$search_limit = $this->input->post('cus_seach_limit', TRUE);
	$store_id = "change_with_storeid";
	switch ($report_type)
	{
		case "buy_report":
		$output_data['buy_data'] = $this->Central_reports_model->calculate_total_buy($from_create_date,$to_create_date,$store_id);
		$output_data['buy_details'] = $this->Central_reports_model->total_buy_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('Central_reports/buy_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "sell_report":
		$output_data['sell_data'] = $this->Central_reports_model->calculate_total_sell($from_create_date,$to_create_date,$store_id);
		$output_data['sell_details'] = $this->Central_reports_model->total_sell_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('Central_reports/sell_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "vat_report":
		$output_data['vat_data'] = $this->Central_reports_model->calculate_total_vat($from_create_date,$to_create_date,$store_id);
		$output_data['vat_details'] = $this->Central_reports_model->total_vat_amount($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('Central_reports/vat_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "item_inventory":
		$output_data['item_inventory_data'] = $this->Central_reports_model->calculate_total_items_inventory($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		// $output_data['date_from']=$from_create_date;
		// $output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('Central_reports/item_inventory_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "expense_report":
		$output_data['expense_data'] = $this->Central_reports_model->calculate_total_expense($from_create_date,$to_create_date,$store_id);
		$output_data['expense_details'] = $this->Central_reports_model->total_expense_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/expense_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "damage_lost_report":
		$output_data['damage_lost_data'] = $this->Central_reports_model->calculate_total_damage_lost($from_create_date,$to_create_date,$store_id);
		$output_data['damage_lost_details'] = $this->Central_reports_model->total_damage_lost_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/damage_lost_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "loan_report":
		$output_data['loan_data'] = $this->Central_reports_model->calculate_total_loan($from_create_date,$to_create_date,$store_id);
		$output_data['loan_details'] = $this->Central_reports_model->total_loan_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/loan_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "dpst_wtdrl_report":
		$output_data['deposit_wtdrl_data'] = $this->Central_reports_model->calculate_total_dpst_wtdrl($from_create_date,$to_create_date);
		$output_data['deposit_details'] = $this->Central_reports_model->total_dpst_details($from_create_date,$to_create_date);
		$output_data['withdrawal_details']= $this->Central_reports_model->total_wtdrl_details($from_create_date,$to_create_date);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/dept_wtdrl_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "buy_due_report":
		$output_data['buy_due_data'] = $this->Central_reports_model->calculate_total_buy_due($from_create_date,$to_create_date,$store_id);
		$output_data['buy_due_details'] = $this->Central_reports_model->total_buy_due_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/buy_due_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "sell_due_report":
		$output_data['sell_due_data'] = $this->Central_reports_model->calculate_total_sell_due($from_create_date,$to_create_date,$store_id);
		$output_data['sell_due_details'] = $this->Central_reports_model->total_sell_due_details($from_create_date,$to_create_date,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/sell_due_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "all_customers":
		$output_data['all_customer_data'] = $this->Central_reports_model->all_customers_list();
		$output_data['store_info']= $this->Central_reports_model->store_details();
		// $output_data['date_from']=$from_create_date;
		// $output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/all_customers_list_template', $output_data, FALSE);
		load_footer();
		break;
		case "all_vendors":
		$output_data['all_vendor_data'] = $this->Central_reports_model->all_vendors_list();
		$output_data['store_info']= $this->Central_reports_model->store_details();
		// $output_data['date_from']=$from_create_date;
		// $output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/all_vendors_list_template', $output_data, FALSE);
		load_footer();
		break;
		case "top_rev_items":
		$output_data['top_rev_data'] = $this->Central_reports_model->calculate_top_rev_items($from_create_date,$to_create_date,$search_limit,$store_id);
		$output_data['top_rev_details'] = $this->Central_reports_model->top_rev_items_details($from_create_date,$to_create_date,$search_limit,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/top_revenue_items_report_template', $output_data, FALSE);
		load_footer();
		break;
		case "top_categories":
		$output_data['top_rev_data'] = $this->Central_reports_model->calculate_top_rev_category($from_create_date,$to_create_date,$search_limit,$store_id);
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/top_category_template', $output_data, FALSE);
		load_footer();
		break;
		case "profit_loss":
		$vat_include = $this->input->post('vat_amt_selection', TRUE);
		if ($vat_include == 'yes') {
			$output_data['total_vat_amount']= $this->Central_reports_model->total_vat_amount($from_create_date,$to_create_date,$store_id);
		}
		else{
			$output_data['total_vat_amount']=array('vat_amount' =>0);	
		}
		$output_data['total_buy_amount'] = $this->Central_reports_model->total_buy_amount($from_create_date,$to_create_date,$store_id);
		$output_data['total_sell_amount'] = $this->Central_reports_model->total_sell_amt($from_create_date,$to_create_date,$store_id);
		$output_data['total_damage_amount'] = $this->Central_reports_model->total_damage_amt($from_create_date,$to_create_date,$store_id);
		$output_data['total_expense_amount'] = $this->Central_reports_model->total_expense_amt($from_create_date,$to_create_date,$store_id);
		$output_data['total_landed_cost'] = $this->Central_reports_model->total_landed_cost_amt($from_create_date,$to_create_date,$store_id);
		$output_data['total_debit_amount']= $output_data['total_buy_amount']['paid'] + $output_data['total_damage_amount']['damage_lost_amount']+ $output_data['total_landed_cost']['landed_cost']+$output_data['total_expense_amount']['expenditures_amount']+ $output_data['total_vat_amount']['vat_amount'] ;
		$output_data['store_info']= $this->Central_reports_model->store_details();
		$output_data['date_from']=$from_create_date;
		$output_data['date_to']=$to_create_date;
		load_header();
		$this->load->view('central_reports/profit_loss_report', $output_data, FALSE);
		load_footer();
		break;
		default:
		echo "Go back. And try again please";
	}
	// echo ("$report_type, $from_create_date, $to_create_date");
}

}

/* End of file Reports_central.php */
/* Location: ./application/controllers/Reports_central.php */