<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('AttendanceModel');
		$this->load->helper('language_helper');
		$cookie = $this->input->cookie('language', true);
		$this->lang->load('attendance_lang', $cookie);
		$this->lang->load('left_side_nev_lang', $cookie);
	}

	public function index()
	{
		// $user_id_logged = $this->session->userdata('user_id');
		// $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
		// if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
		load_header();
		$this->load->view('user/attendance');
		load_footer();    
		// }
		// else{
		// 	load_header();
		// 	$this->load->view('access_deny/not_permitted');
		// 	load_footer();
		// }
	}

	public function getUserList()
	{
		$term = $this->input->get('term', TRUE);
		$filtered_items = $this->AttendanceModel->getUserList($term);
		echo json_encode($filtered_items);
	}

	public function attendanceEntry(){

		$submitted_data = $this->input->post();
		$user_id_logged = $this->session->userdata('user_id');

		$date = new DateTime($submitted_data['date']);

		$data['id_user'] = trim($submitted_data['user_id']);
		$data['date'] = trim($date->format('Y-m-d'));
		$data['in_time'] = trim( $submitted_data['in_time']);
		$data['out_time'] = trim($submitted_data['out_time']);
		$data['remark'] = trim($submitted_data['remark']);
		$data['id_user_operator'] = $user_id_logged;

		if($data['id_user'] == ""){
			echo 'no_user_selected';
			exit();
		}else if($data['date'] == ""){
			echo 'date_required';
			exit();
		}else if($data['in_time'] == ""){
			echo 'in_time_required';
			exit();
		}else if($data['out_time'] == ""){
			echo 'out_time_required';
			exit();
		}

		if($this->AttendanceModel->checkUserValidity($data['id_user'])){
			if($this->AttendanceModel->todayDuplicacyCheck($data['id_user'], $data['date'])){
				echo "duplicate_entry_for_today"; 
			}else{
				if($this->AttendanceModel->attendanceEntry($data)){
					echo "success"; 
				}else{
					echo "fail"; 
				}
			}	
		}else{
			echo 'invalid_user';
		}

		
	}

	public function all_users_attendance_info()
	{
		$filters = $this->input->get();
		$all_data = $this->AttendanceModel->all_users_attendance_info($filters);
		$all_data_without_limit = $this->AttendanceModel->all_users_attendance_info($filters, true);
		$all_data_final = $this->AttendanceModel->all_users_attendance_info($filters, true);

		$output_data=[];

		$output_data["draw"]=$filters['draw'];
		$output_data["recordsTotal"]=$all_data_without_limit;
		$output_data["recordsFiltered"]=$all_data_final;
		$output_data["data"]=$all_data;
		echo json_encode($output_data);
	}
}

/* End of file  */
/* Location: ./application/controllers/ */