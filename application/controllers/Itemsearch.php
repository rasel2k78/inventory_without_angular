<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itemsearch extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Item_model');
		$cookie = $this->input->cookie('language', true);
		$this->lang->load('left_side_nev_lang', $cookie);
		userActivityTracking();
		
		$user_id = $this->session->userdata('user_id');
		if ($user_id == null) {

			redirect('Login', 'refresh');
		}
		$this->load->model('ItemSearchModel');
	}

	public function index()
	{
		load_header();
		$this->load->view('item/item_search_view');
		load_footer();
	}
 /**
     * To get all category
     * 
     * @return json data
     * @author Musabbir 
     **/
 public function allCatgoryForDataTable()
 {
 	$filters = $this->input->get();
 	$all_data = $this->ItemSearchModel->allCatgoryForDataTable($filters);
 	$all_data_without_limit = $this->ItemSearchModel->allCatgoryForDataTable($filters, true);
 	$all_data_final = $this->ItemSearchModel->allCatgoryForDataTable($filters, true);

 	$output_data=[];

 	$output_data["draw"]=$filters['draw'];
 	$output_data["recordsTotal"]=$all_data_without_limit;
 	$output_data["recordsFiltered"]=$all_data_final;
 	$output_data["data"]=$all_data;
 	echo json_encode($output_data);
 }


	 /**
     * To get item list by category
     * 
     * @return json data
     * @author Musabbir 
     **/
	 public function allItemsByCategory()
	 {
	 	$filters = $this->input->get();
	 	$all_data = $this->ItemSearchModel->allItemsByCategory($filters);
	 	$all_data_without_limit = $this->ItemSearchModel->allItemsByCategory($filters, true);
	 	$all_data_final = $this->ItemSearchModel->allItemsByCategory($filters, true);

	 	$output_data=[];

	 	$output_data["draw"]=$filters['draw'];
	 	$output_data["recordsTotal"]=$all_data_without_limit;
	 	$output_data["recordsFiltered"]=$all_data_final;
	 	$output_data["data"]=$all_data;
	 	echo json_encode($output_data);
	 }

	}

	/* End of file  */
/* Location: ./application/controllers/ */