<?php
/**
 * Exchange with Transfer_items  Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for transfer items to different shop 
 *
 * @package Controller
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/

class Transfer_warehouse extends CI_Controller
{
    /**
 * Constructor function 
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();

     $this->load->model('Bank_model');
     $this->load->model('Exchange_with_customer_model');
     $this->load->model('Inventory_model');
     $this->load->model('Transfer_model');
     $this->load->model('Item_model');
     $this->load->model('User_access_check_model');
     $cookie = $this->input->cookie('language', true);
     $this->lang->load('left_side_nev_lang', $cookie);
     $this->lang->load('transfer_item_form_lang', $cookie);
     $this->lang->load('transfer_receive_item_lang', $cookie);
     $user_account_folder = $this->Transfer_model->get_folder_name();
     userActivityTracking();
     
     $user_id = $this->session->userdata('user_id');
     if ($user_id == null) {

      redirect('Login', 'refresh');
    }

    $status = $this->check_internet();
    if($status==1) {
      load_header();
      $this->load->view('no_internet');
      load_footer();
    }

  }

    /**
 * loads basic Transfer form
 *
 * @return null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function index()
    {
     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     load_header();
     $status = $this->check_internet();
     if($status==1) {
      $this->load->view('no_internet');
    }
    else
    {
      if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
      {
        $shop_info = $this->Transfer_model->get_my_shop();
        $user_account_folder = $this->Transfer_model->get_folder_name();
        $owner_info = $this->Transfer_model->get_my_account()->row_array();
        $find_other_shops = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_synched_shops/".$shop_info['shop_id'];
        $shops_output = file_get_contents($find_other_shops); 
        $shops = json_decode($shops_output);
        $body_data = array(
         'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
         'shop_info' => $shop_info,
         'other_shops' => $shops,
         'owner_info' => $owner_info,
         );
        $this->load->view('warehouse/Transfer_items_form', $body_data);
      }
      else
      {
        $this->load->view('access_deny/not_permitted');
      }
    }
    load_footer();
  }

  /**
   * find all synced shop list from server customer account
   *
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/

  public function get_all_shops()
  {
   $shop_info = $this->Transfer_model->get_my_shop();
   $create_shop_backup_url ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_synched_shops/".$shop_info['shop_id'];
   $output = file_get_contents($create_shop_backup_url); 
   echo $output;
 }

  /**
   * Checking internet before use warehouse
   *
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
  public function check_internet()
  {
   $connected = @fsockopen('www.google.com', 80);
   if($connected) 
   {
    return 0;
  }
  else
  {
    return 1;    
  }
}

/**
   * Save warehouse transfer items to db as it is a temporary sell and make ready for sync.
   *
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function save_transfer_info()
{
  $data = $this->input->post('all_data');
  $store_to_id = $this->input->post('store_to_id');
  $store_to_name = $this->input->post('store_to_name');

  $error_list = array();
  foreach ($data['item_info'] as $key => $value) 
  {
    $return_data = $this->Transfer_model->check_items($value);
    if($value['item_quantity'] == null) {
      array_push($error_list, 'PLease Enter Item Quantity');
    }
    if($value['item_quantity'] > $return_data['quantity']) {
      array_push($error_list, 'Item does not have enough quantity');
    }
    if($value['selling_price'] ==null) {
      array_push($error_list, 'price not given');
    }
    if($value['selling_price'] !=$return_data['retail_price']) {
      array_push($error_list, 'price not perfect');
    }
  }
  if(count($error_list)>0) {
    echo json_encode($error_list);
    exit();
  }
  /** Save transfer data which will be synced and show to transfered shops starts**/
  $ready_data_basic = array();
  $ready_data_basic['transfer_from'] = $data['all_basic_data']['store_form_id'];
  $ready_data_basic['transfer_to'] = trim($data['all_basic_data']['store_to_id']);
  $ready_data_basic['total_item_quantity'] = $data['all_basic_data']['total_quantity'];
  $ready_data_basic['total_amount'] = $data['all_basic_data']['total_amount'];
  $ready_data_basic['transfer_type'] = "warehouse";
  $ready_data_basic['status'] = "sent";
  $ready_data_basic['user_id'] = $this->session->userdata('user_id');
  $transfer_details_id = $this->Transfer_model->insert_warehouse_data($ready_data_basic);
  /** Save transfer data which will be synced and show to transfered shops ends**/

  /** Make transfered shops as a customer, as it is a due temp sell starts**/
  $udata = array();
  $udata['customers_name'] = trim($store_to_name);
  $udata['customers_present_address'] = "warehouse";
  $udata['customers_phone_1'] = $store_to_id;
  $udata['user_id'] = $this->session->userdata('user_id');
  $due_sell_data = array();
  $customer_name = $store_to_name;
  $customer_phone = $store_to_id;
  $insert_customer_id = $this->Transfer_model->check_customer_availablity($customer_name,$customer_phone);
  if($insert_customer_id)
  {
    $due_sell_data['customers_id'] = $this->Transfer_model->update_customer($udata,$insert_customer_id);
  }
  else
  {
    $insert_customer_id = $this->Transfer_model->createAccount($udata);
    $due_sell_data['customers_id'] = $insert_customer_id;
  }

  /** Make transfered shops as a customer, as it is a due temp sell ends**/

  /** Insert transfered data as sell details data with identifier -T starts**/

  $due_sell_data['grand_total'] = $data['all_basic_data']['total_amount'];
  $due_sell_data['net_payable'] = $data['all_basic_data']['total_amount'];
  $due_sell_data['paid'] = 0;

  $due_sell_data['due'] = $data['all_basic_data']['total_amount'];
  $due_sell_data['sub_total'] = $data['all_basic_data']['total_amount'];
  $due_sell_data['received_money'] = 0;
  $due_sell_data['user_id'] = $this->session->userdata('user_id');
  $due_sell_data['custom_voucher_no'] = "T-".uniqid();
  $due_sell_data['transfer_details_id'] = $transfer_details_id;
  $due_sell_data['publication_status'] = "activated";
  $sell_details_id = $this->Transfer_model->insert_due_sell_data($due_sell_data);

  /** Insert transfered data as sell details data with identifier -T ends**/

  /** Insert transfered cart items as sell cart items and transfer items and updating each item inventory starts**/

  if($transfer_details_id) 
  {
    foreach ($data['item_info'] as $key => $transfer_items) 
    {
      $tdata['transfer_details_id'] = $transfer_details_id;
      $tdata['item_spec_set_id'] = $transfer_items['item_spec_set_id'];
      $tdata['quantity'] = $transfer_items['item_quantity'];
      $tdata['buying_price'] = $transfer_items['buying_price'];
      $tdata['imei_barcode'] = $transfer_items['item_imei_number'];
      $tdata['transfer_type'] = "warehouse";
      $tdata['selling_price'] = $transfer_items['selling_price'];
      $tdata['whole_sale_price'] = $transfer_items['whole_sale_price'];
      $tdata['user_id'] = $this->session->userdata('user_id');

      $due_sell_cart_item['quantity'] = $transfer_items['item_quantity'];
      $due_sell_cart_item['selling_price'] = $transfer_items['buying_price'];
      $due_sell_cart_item['sell_details_id'] = $sell_details_id;
      $due_sell_cart_item['sell_type'] = "retail";
      $due_sell_cart_item['user_id'] = $this->session->userdata('user_id');
      $due_sell_cart_item['item_spec_set_id'] = $transfer_items['item_spec_set_id'];
      $due_sell_cart_item['used_imei_barcode'] = $transfer_items['item_imei_number'];
      $due_sell_cart_item['sub_total'] = $transfer_items['item_quantity']*$transfer_items['buying_price'];
      $insert_due_sell_cart_items = $this->Transfer_model->save_due_sell_cart_items($due_sell_cart_item);

      if($transfer_items['item_imei_number'])
      {
        $imei_update_data['barcode_status'] = "transfered";
        $imei_update_data['sell_details_id'] = $transfer_details_id;
        $this->Transfer_model->update_imei_data($imei_update_data,$transfer_items['item_spec_set_id'],$transfer_items['item_imei_number']);
      }

      $find_item_quantity_on_inventory = $this->Transfer_model->get_items_inventory($transfer_items['item_spec_set_id']);
      $update_inventory_data['quantity'] = $find_item_quantity_on_inventory['quantity'] - $transfer_items['item_quantity'];
      $this->Transfer_model->update_inventory_transfered_items($update_inventory_data,$transfer_items['item_spec_set_id']);
      $save_transfer_items = $this->Transfer_model->save_warehouse_details_items($tdata);

    }
  }
  /** Insert transfered cart items as sell cart items and transfer items and updating each item inventory ends**/

  if($save_transfer_items)
  {
    $this->session->set_userdata('transfered', 1);
  }
  echo json_encode("success");
}



/**
   * Change the transfer status to completed.
   *
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function update_transfer_sent()
{
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $transfer_details_id = $this->input->post('transfer_details_id');
  $total_amount = $this->input->post('total_amount');
  $data['status'] = 'completed';
  $update_transfer_status = $this->Transfer_model->update_transfer_list_completed($transfer_details_id, $data);
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_sent/".$transfer_details_id;
  $output = file_get_contents($create_shop_backup_url); 
  if($output) {
    echo "success";
  }
}

/**
   * Delete transfer if any transfer is still pending and wanna back items to inventory
   * Delete transfer details , transfer items , temp sell details ,temp sell cart items
   * Update Inventory
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function transfered_delete()
{
  $transfer_details_id = $this->input->post('transfer_details_id');
  $transfer_items = $this->Transfer_model->get_transfer_items_by_id($transfer_details_id);
  $this->db->trans_begin();
  $cashbox_data = array();
  $cash_data = array();
  $dataa = array();
  $data = array();
  $data['publication_status'] = "deactivated";
  $delete_transfer = $this->Transfer_model->delete_transfer_details($transfer_details_id,$data);
  if($delete_transfer)
  {
    $transfer_sell_details = $this->Transfer_model->get_sell_transfer_details($transfer_details_id);
    if($transfer_sell_details)
    {
      $total_price = 0;
      /*Delete transfer details and items*/
      foreach ($transfer_items as $key => $value) 
      {
        $total_price+= ($value['quantity']*$value['buying_price']);
        $delete_transfer_items = $this->Transfer_model->delete_transfer_items($value['transfers_id'],$data);
        $delete_transfer_details = $this->Transfer_model->delete_transfer_details($transfer_details_id,$data);
        if($value['imei_barcode'])
        {
          $data_imei['barcode_status'] = "not_used";
          $data_imei['sell_details_id'] = "";
          $data_imei['publication_status'] = "activated";
          $update_inventory = $this->Transfer_model->update_imei_items($data_imei,$value['item_spec_set_id'],$value['imei_barcode']);
        }
        /**Adjust cashbox for transfer details**/
      }

      if($transfer_sell_details)
      {
        $sell_details_id = $transfer_sell_details['sell_details_id'];
        $this->Inventory_model->change_imei_barcode_status($sell_details_id);
        $payment_details=$this->Inventory_model->sell_payment_detials($sell_details_id);
        /**Adjust cashbox for temp sell amount and back items to inventory**/
        $paid_amount = $this->Inventory_model->sell_paid_amount_by_id($sell_details_id);
        if($payment_details['payment_type']== "cash"){
          $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
          $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $paid_amount['paid'];
          $this->Inventory_model->update_total_cashbox_info($final_data);
        }
        $data = $this->Inventory_model->item_quantity_by_sell_details_id($sell_details_id);
        $update_item_inventory = $this->Inventory_model->update_inventory_after_sell_delete($data);

        if ($update_item_inventory > 0) 
        {
          if($this->Inventory_model->delete_sell_info($sell_details_id)) 
          {
            $this->db->trans_commit();
            echo json_encode("success");
          }    
          else
          {
            $this->db->trans_rollback();
            echo json_encode("failed");
          }    
        }
      }
    }
    else
    {
      $total_price = 0;
      foreach ($transfer_items as $key => $value) 
      {
        $total_price+= ($value['quantity']*$value['buying_price']);
        $delete_transfer_items = $this->Transfer_model->delete_transfer_items($value['transfers_id'],$data);
        $current_inventory = $this->Transfer_model->get_items_inventory($value['item_spec_set_id']);
        $dataa['quantity'] = $current_inventory['quantity'] + $value['quantity'];
        $update_inventory = $this->Transfer_model->update_inventory_transfered_items($dataa,$value['item_spec_set_id']);
        /**Adjust cashbox for transfer details**/
      }
      if ($this->db->trans_status() === false) {
        $this->db->trans_rollback();
        echo "error transection";
      }
      else
      {
        $this->db->trans_commit();
        echo json_encode("success");
      }
    }
  }
}

/**
   * Show transfer items by transfer details id
   * @return array[]
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function transfered_items()
{
 $transfer_details_id = $this->input->post('transfer_details_id');
 $transfer_items = $this->Transfer_model->get_warehouse_items_by_details_id($transfer_details_id);
 echo json_encode($transfer_items);
 exit;
}

/**
   * Saving warehouse received items to shop
   * @param  string $id
   * @return boolean
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function insert_receive_items($transfer_item_sets)
{
  $shop_info = $this->Transfer_model->get_my_shop();
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $users =$transfer_item_sets['users'];
  $spec_names = $transfer_item_sets['spec_names'];
  $spec_values = $transfer_item_sets['spec_values'];
  $categories = $transfer_item_sets['categories'];
  $items_name = $transfer_item_sets['items_name'];
  $item_spec_set = $transfer_item_sets['item_spec_set'];
  $spec_connector_with_values = $transfer_item_sets['spec_connector_with_values'];
  $iif = 0;
  $ielse = 0;

  /*Spec names insert starts*/
  foreach ($spec_names as $key => $value)
  {
    if($value)
    {
      $value['user_id'] = $this->session->userdata('user_id');
      $value['stores_id'] = $shop_info['shop_id'];
      $check_if_exist = $this->Transfer_model->get_spec_name_id($value['spec_name_id']);
      if($check_if_exist)
      {
        $value['publication_status'] = "activated";
        $this->Transfer_model->update_on_pull_shops("spec_name_table",$value['spec_name_id'],"spec_name_id",$value);
      }
      else
      {
        $if_exist_spec_name = $this->Transfer_model->get_spec_name_by_name($value['spec_name']);
        if($if_exist_spec_name)
        {
          $tables  = array("spec_name_table","spec_value","spec_set_connector_with_values");
          foreach ($tables as $key => $table) 
          {
            $data['spec_name_id'] = $value['spec_name_id'];
            $update_new_spec_name = $this->Transfer_model->update_spec_names($data,$if_exist_spec_name['spec_name_id'],$table);
          }
        }
        else
        {
          $this->Transfer_model->insert_on_pull_shop("spec_name_table",$value);
        }
      }
    }
  }
  /*Spec names insert ends*/

  /*Spec values insert starts*/
  foreach ($spec_values as $key => $value)
  {
    if($value)
    {
     $value['user_id'] = $this->session->userdata('user_id');
     $value['stores_id'] = $shop_info['shop_id'];
     $check_if_exist = $this->Transfer_model->get_spec_value_id($value['spec_value_id']);
     if($check_if_exist)
     {
      $value['publication_status'] = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_value",$value['spec_value_id'],"spec_value_id",$value);
    }
    else
    {
     $this->Transfer_model->insert_on_pull_shop("spec_value",$value);
   }
 }
}
/*Spec values insert ends*/

/*Categories insert starts*/
foreach ($categories as $key => $value)
{
  if($value)
  {
   $value['user_id'] = $this->session->userdata('user_id');
   $value['stores_id'] = $shop_info['shop_id'];
   $check_if_exist = $this->Transfer_model->get_category_id($value['categories_id']);
   if($check_if_exist)
   {
    $value['publication_status'] = "activated";
    $this->Transfer_model->update_on_pull_shops("categories",$value['categories_id'],"categories_id",$value);
  }
  else
  {
    $check_exist = $this->Item_model->check_category_exist($value['categories_name']);
    if($check_exist)
    {
      $value['publication_status'] = "activated";
      $this->Transfer_model->update_on_pull_shops("categories",$value['categories_id'],"categories_id",$value);
    }
    else
    {

      $this->Transfer_model->insert_on_pull_shop("categories",$value);
    }
  }
}
}
/*Categories insert ends*/

/*Item table insert starts*/
foreach ($items_name as $key => $value)
{
  if($value)
  {
    $value['user_id'] = $this->session->userdata('user_id');
    $value['stores_id'] = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_items_id($value['items_id']);
    if($check_if_exist)
    {
      $value['publication_status'] = "activated";
      $this->Transfer_model->update_on_pull_shops("items",$value['items_id'],"items_id",$value);
    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("items",$value);
    }
  }
}
/*Item table insert ends*/

/*Item spec set table insert starts*/
foreach ($item_spec_set as $key => $value)
{
  if($value)
  {
    $value['user_id'] = $this->session->userdata('user_id');
    $value['stores_id'] = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_item_spec_set_id($value['item_spec_set_id']);
    if($check_if_exist)
    {
     $value['publication_status'] = "activated";
     $check_barcode_else_exist = $this->Item_model->check_barcode_else_exist($value['barcode_number'],$value['item_spec_set_id']);
     if($check_barcode_else_exist)
     {
      unset($value['barcode_number']);
    }
    $this->Transfer_model->update_on_pull_shops("item_spec_set",$value['item_spec_set_id'],"item_spec_set_id",$value);
  }
  else
  {
    $check_barcode_exist = $this->Item_model->check_barcode_if_exist($value['barcode_number']);
    if($check_barcode_exist)
    {
      $value['barcode_number']  = $this->generateRandomString();
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
  }
}
}
/*Item spec set table insert ends*/

/*Item spec set connector table insert starts*/
foreach ($spec_connector_with_values as $key => $value)
{
  if($value)
  {
    $value['user_id'] = $this->session->userdata('user_id');
    $value['stores_id'] = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_item_spec_connector_id($value['spec_set_connector_id']);
    if($check_if_exist)
    {
      if($value['publication_status']=="deactivated")
      {
        $value['publication_status'] = "deactivated";
      }
      else
      {
        $value['publication_status'] = "activated";
      }
      $this->Transfer_model->update_on_pull_shops("spec_set_connector_with_values",$value['spec_set_connector_id'],"spec_set_connector_id",$value);

    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("spec_set_connector_with_values",$value);
    }
  }
}
/*Item spec set connector table insert ends*/
return 1;
}


/**
   * Generating default barcode for item insert
   * @return string
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function generateRandomString()
{
  $length = 6;
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  $check_barcode_exist = $this->Item_model->check_barcode_if_exist($randomString);
  if($check_barcode_exist)
  {
    $this->generateRandomString();
  }
  else
  {        
    return $randomString;
  }
}

/**
   * Getting insert all items from target shop to local shop
   * @param  $target_shop_id
   * @return string
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function pull_shop_items_from_server($target_shop_id)
{
  $shop_info = $this->Transfer_model->get_my_shop();
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $request_for_items ="https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_spec_names_by_shop/".$target_shop_id;
  $output = file_get_contents($request_for_items); 
  $result = json_decode($output);
  $users = $result->users;
  $spec_names = $result->spec_names;
  $spec_values = $result->spec_values;
  $categories = $result->categories;
  $items_name = $result->items_name;
  $item_spec_set = $result->item_spec_set;
  $spec_connector_with_values = $result->spec_connector_with_values;
  $iif = 0;
  $ielse = 0;
  foreach ($spec_names as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_name_id($value->spec_name_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_name_table",$value->spec_name_id,"spec_name_id",$value);
    }
    else
    {
      $if_exist_spec_name = $this->Transfer_model->get_spec_name_by_name($value->spec_name);
      if($if_exist_spec_name)
      {
        $tables  = array("spec_name_table","spec_value","spec_set_connector_with_values");
        foreach ($tables as $key => $table) 
        {
          $data['spec_name_id'] = $value->spec_name_id;
          $update_new_spec_name = $this->Transfer_model->update_spec_names($data,$if_exist_spec_name['spec_name_id'],$table);
        }
      }
      else
      {
        $this->Transfer_model->insert_on_pull_shop("spec_name_table",$value);
      }
    }  
  }

  foreach ($spec_values as $key => $value)
  {
    $value->user_id = $this->session->userdata('user_id');
    $value->stores_id = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_spec_value_id($value->spec_value_id);
    if($check_if_exist)
    {
      $value->publication_status = "activated";
      $this->Transfer_model->update_on_pull_shops("spec_value",$value->spec_value_id,"spec_value_id",$value);
    }
    else
    {
     $this->Transfer_model->insert_on_pull_shop("spec_value",$value);
   }
 }

 foreach ($categories as $key => $value)
 {
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_category_id($value->categories_id);
  if($check_if_exist)
  {
    $value->publication_status = "activated";
    $this->Transfer_model->update_on_pull_shops("categories",$value->categories_id,"categories_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("categories",$value);
  }
}
foreach ($items_name as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_items_id($value->items_id);
  if($check_if_exist)
  {
    $value->publication_status = "activated";
    $this->Transfer_model->update_on_pull_shops("items",$value->items_id,"items_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("items",$value);
  }
}
foreach ($item_spec_set as $key => $value)
{
  if($value)
  {
    $value['user_id'] = $this->session->userdata('user_id');
    $value['stores_id'] = $shop_info['shop_id'];
    $check_if_exist = $this->Transfer_model->get_item_spec_set_id($value['item_spec_set_id']);
    if($check_if_exist)
    {
     $value['publication_status'] = "activated";
     $check_barcode_else_exist = $this->Item_model->check_barcode_else_exist($value['barcode_number'],$value['item_spec_set_id']);
     if($check_barcode_else_exist)
     {
      unset($value['barcode_number']);
    }
    $this->Transfer_model->update_on_pull_shops("item_spec_set",$value['item_spec_set_id'],"item_spec_set_id",$value);
  }
  else
  {
    $check_barcode_exist = $this->Item_model->check_barcode_if_exist($value['barcode_number']);
    if($check_barcode_exist)
    {
      $value['barcode_number']  = $this->generateRandomString();
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
    else
    {
      $this->Transfer_model->insert_on_pull_shop("item_spec_set",$value);
    }
  }
}  
}

foreach ($spec_connector_with_values as $key => $value)
{
  $value->user_id = $this->session->userdata('user_id');
  $value->stores_id = $shop_info['shop_id'];
  $check_if_exist = $this->Transfer_model->get_item_spec_connector_id($value->spec_set_connector_id);
  if($check_if_exist)
  {
    if($value->publication_status=="deactivated")
    {
      $value->publication_status = "deactivated";
    }
    else
    {
      $value->publication_status = "activated";
    }
    $this->Transfer_model->update_on_pull_shops("spec_set_connector_with_values",$value->spec_set_connector_id,"spec_set_connector_id",$value);
  }
  else
  {
    $this->Transfer_model->insert_on_pull_shop("spec_set_connector_with_values",$value);
  } 
}
return 1;
}

/**
   * Save warehouse receive list
   * Save receive items to local shop(Item pull)
   * Make temporary buy voucher with receive items
   * Update inventory
   * Update total receive amount
   * @return string
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function save_warehouse_items()
{
  $data = $this->input->post('all_data');
  $target_shop_id = $data['all_basic_data']['transfer_from'];
  $transfer_item_sets = $data['all_basic_data']['transfer_item_sets'];
  $total_previous_receive_amount = $data['all_basic_data']['total_receive_amount'];
  $total_current_receive_amount = $data['all_basic_data']['net_payable'];
  $get_total_receives = $this->Transfer_model->get_total_receives();

  $total_receive_amount_data['amount'] = $total_current_receive_amount;
  $total_receive_amount_data['transfer_from_shop_name'] = $total_previous_receive_amount['name'];
  $total_receive_amount_data['transfer_from_shop_id'] = $total_previous_receive_amount['shop_id'];
  if($total_previous_receive_amount['total_amount']>0)
  {
   $total_receive_amount_data['total_amount'] = $total_current_receive_amount +$total_previous_receive_amount['total_amount'];
 }
 else
 {
  $total_receive_amount_data['total_amount'] = $total_current_receive_amount;
}
$this->Transfer_model->add_total_receive_amount($total_receive_amount_data);

/*Insert received items to local shops*/
$is_pulled = $this->insert_receive_items($transfer_item_sets);

if($is_pulled)
{
  $transfer_details_id = $data['all_basic_data']['transfer_details_id'];
  $transfer_from_id = $data['all_basic_data']['transfer_from_id'];
  $transfer_from_name = $data['all_basic_data']['transfer_from_name'];

  /*Make temp buy with buy details and buy cart items with vendor starts*/
  $ready_data_basic = array();
  $error_list = array();
  $buy_voucher = "T-".uniqid();
  if($data['all_basic_data']['paid'] =="") {
    $due_amount= $data['all_basic_data']['due'];
  }
  else{
    $due_amount= $data['all_basic_data']['due_after_paid'];
  }
  $paid_amount= $data['all_basic_data']['paid'];
  if(count($error_list)>0) {
    echo json_encode($error_list);
    exit();
  }
  foreach ($data['item_info'] as $key => $value) 
  {
    $value['item_spec_set_id'] ;
    $value['quantity'] ;
    $value['buying_price'] ;
    if($value['quantity'] == null || $value['quantity'] < 0 ) {
      array_push($error_list, 'Item Quantity is Invalid');
    }
    if($value['buying_price'] ==null) {
      array_push($error_list, 'Item Buying Price is Empty');
    }
    if($value['item_spec_set_id'] ==null) {
      array_push($error_list, 'Item Not Selected');
    }
  }
  if(count($error_list)>0) {
    echo json_encode($error_list);
    exit();
  }
  $ready_data_basic['voucher_no'] = $buy_voucher;
  $udata =array();
  $udata['vendors_name'] = $transfer_from_name;
  $udata['vendors_present_address'] = "warehouse";
  $udata['vendors_phone_1'] = $transfer_from_id;
  $udata['user_id'] = $this->session->userdata('user_id');
  $vendor_name = $transfer_from_name;
  $vendor_phone = $transfer_from_id;
  $vendor_id = $this->Transfer_model->check_vendor_availablity($vendor_name,$vendor_phone);
  if($vendor_id)
  {
    $this->Transfer_model->update_vendor($udata,$vendor_id);
    $ready_data_basic['vendors_id'] = $vendor_id;
  }
  else
  {
    $ready_data_basic['vendors_id'] = $this->Transfer_model->create_vendor_account($udata);
  }
  $ready_data_basic['landed_cost'] = $data['all_basic_data']['total_cost'];
  $ready_data_basic['user_id'] = $this->session->userdata('user_id');
  $ready_data_basic['discount'] = $data['all_basic_data']['discount'];
  $purchase_date = $data['all_basic_data']['purchase_date'];

  if($purchase_date != null)
  {
    $ready_data_basic['date_created'] = $data['all_basic_data']['purchase_date'];
  }
  $ready_data_basic['grand_total'] = $data['all_basic_data']['net_payable'];
  if($ready_data_basic['discount'] == null || $ready_data_basic['discount'] == 0) 
  {
    $ready_data_basic['net_payable'] = $data['all_basic_data']['net_payable'];
  }
  if($ready_data_basic['discount'] > 0) {
    $ready_data_basic['net_payable'] = $data['all_basic_data']['net_pay_after_discount'];
  }
  $ready_data_basic['paid'] = "";
  $ready_data_basic['due'] = $ready_data_basic['net_payable'];
  /*Make temp buy with buy details and buy cart items with vendor ends*/

  $this->db->trans_begin();
  $buy_details_uuid = $this->Inventory_model->save_buy_basic_info($ready_data_basic);
  $this->Transfer_model->save_buy_item_info($data['item_info'], $buy_details_uuid);
  $this->Inventory_model->save_item_info_to_inventory($data['item_info']);



  /*Serial or imei use as barcode starts*/
  if(array_key_exists("imei_barcode", $data))
  {
    foreach ($data['imei_barcode'] as $val_of_imei) 
    {
      $val_of_imei['buy_details_id']= $buy_details_uuid;
      $val_of_imei['user_id'] = $this->session->userdata('user_id');
      $check_imei_avialability = $this->Inventory_model->check_imei_avilable_info($val_of_imei['imei_barcode']);
      $is_sell = $this->Inventory_model->check_is_sell($val_of_imei['imei_barcode']);
      if(!empty($check_imei_avialability))
      {
       $this->db->trans_rollback();
       echo "Duplicate IMEI or SERIAL Inserted";exit;
     }
     else if (!empty($is_sell))
     {
       $this->db->trans_rollback();
       echo "Particular IMEI or SERIAL Already Sold Once";exit;
     }
     else
     {
      $this->Inventory_model->save_imei_barcode_info($val_of_imei);
    }
  }
}
/*Serial or imei use as barcode ends*/

foreach ($data['item_info'] as $key => $value) 
{
  $item_id= $value['item_spec_set_id'];
  $last_thirty_days_total_sell= $this->Inventory_model->thirty_days_sell_qty($item_id);
  $one_week_avg_sell= floor(($last_thirty_days_total_sell['quantity'])/30);
  $last_thirty_days_individual_sell_qty = $this->Inventory_model->individual_sell_qty($item_id);
  $sum_of_square_of_diff= 0;
  foreach ($last_thirty_days_individual_sell_qty as $key2 => $value2) {
    $sell_quantity = $value2['quantity'];
    $diff_from_avg_sell_qty = ($one_week_avg_sell - $sell_quantity)*($one_week_avg_sell - $sell_quantity);
    $sum_of_square_of_diff+=$diff_from_avg_sell_qty;
  }
  $avg_of_square = floor($sum_of_square_of_diff/30);
  $sqr_root_of_avg = floor(sqrt($avg_of_square));
  $number_of_units_to_stack =  floor($one_week_avg_sell + ($sqr_root_of_avg * 1.65));
  $approx_stock_for_a_week = $number_of_units_to_stack*7;
  $current_inventory_qty = $this->Inventory_model->items_info_from_inventory($item_id);
  if ($current_inventory_qty['quantity'] > ($approx_stock_for_a_week*4) ) {
    $status_data['inventory_status']="Excess";
    $this->Inventory_model->update_inventory_status($item_id, $status_data);
  }
  if ($current_inventory_qty['quantity'] <= $approx_stock_for_a_week) {
    $status_data['inventory_status']="Low";
    $this->Inventory_model->update_inventory_status($item_id, $status_data);
  }
  if ($current_inventory_qty['quantity'] <= ($approx_stock_for_a_week*4) && $current_inventory_qty['quantity'] > $approx_stock_for_a_week ) {
    $status_data['inventory_status']="Normal";
    $this->Inventory_model->update_inventory_status($item_id, $status_data);
  }
}

if ($data['all_basic_data']['payment_type'] == "cheque") {
 $bank_data=array(
  'deposit_withdrawal_amount' => $data['all_basic_data']['paid'],
  'cash_type' => 'buy',
  'user_id' => $this->session->userdata('user_id'),
  'buy_sell_and_other_id' => $buy_details_uuid,
  'bank_acc_id' => $data['all_basic_data']['bank_acc_id'],
  );
 $bank_statement_id= $this->Bank_model->save_buy_by_bank_info($bank_data);
 $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_data['bank_acc_id']);
 $updated_balance['final_amount'] = $curr_bank_amount['final_amount'] - $bank_data['deposit_withdrawal_amount'];
 $bank_acc_id= $bank_data['bank_acc_id'];
 $update_bank_amount= $this->Bank_model->update_total_amt_info_of_acc($updated_balance,$bank_acc_id);
 $cheque_data=array(
  'buy_sell_or_others_id' => $buy_details_uuid,
  'bank_acc_id'=> $bank_acc_id,
  'cheque_number' => $data['all_basic_data']['cheque_page_num'],
  'bank_statement_id' => $bank_statement_id,
  'user_id' => $this->session->userdata('user_id'),
  );
 $this->Bank_model->save_cheque_details_info($cheque_data);
}

if ($this->db->trans_status() === false) {
  $this->db->trans_rollback();
  echo json_encode("error transaction");
}
else
{
 $this->db->trans_commit();
 $user_account_folder = $this->Transfer_model->get_folder_name();
 $change_warehouse_status = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_warehouse_status_from_pending/".$transfer_details_id;
 $output=file_get_contents($change_warehouse_status); 
 $result = json_decode($output);
 if($result)
 {
  echo json_encode("warehouse_success");
}
}
}
else
{
 echo json_encode("item_insert_problem");
}
}


/**
   * Change transfer status
   * Update total receive amount
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function confirm_receive_transfer()
{
 $user_account_folder = $this->Transfer_model->get_folder_name();
 $data = $this->input->post('all_data');
 $error_list = array();
 $transfer_from_id = $data['all_basic_data']['transfer_from_id'];
 $transfer_details_id = $data['all_basic_data']['transfer_details_id'];
 $totalAmount = $data['all_basic_data']['total_amount'];
 $totalQuantity = $data['all_basic_data']['total_quantity'];

 foreach ($data['item_info'] as $key => $value) 
 {
  $find_item_quantity_on_inventory = $this->Transfer_model->get_items_inventory($value['matched_item_spec_id']);
  $udata = array();
  $udata['quantity'] = $value['quantity'];
  $udata['item_spec_set_id'] = $value['matched_item_spec_id'];
  if(!empty($find_item_quantity_on_inventory)) 
  {
    $udata['quantity'] = $find_item_quantity_on_inventory['quantity'] + $value['quantity'];
    $this->Transfer_model->update_inventory_transfered_items($udata, $value['matched_item_spec_id']);
  }
}
$create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_received/".$transfer_details_id;
$output = file_get_contents($create_shop_backup_url); 
$status_update = json_decode($output);
if($status_update) {
  echo 'success';exit;
}    
}

/**
   * Find out shop transfer list to show
   * @return []
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function transfer_list()
{
 $status = $this->check_internet();
 if($status==1)
 {
  load_header();
  $this->load->view('no_internet');
  load_footer();
}
else
{
  load_header();
  $user_account_folder = $this->Transfer_model->get_folder_name();

  $shop_info = $this->Transfer_model->get_my_shop();
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_transfer_item_details_warehouse/".$shop_info['shop_id'];
  $output = file_get_contents($create_shop_backup_url); 
  $result = json_decode($output, true);
  $body_data = array(
   'transfer_list' => $result,
   'transfer_from' => $shop_info,
   );
  $this->load->view('warehouse/Transfer_list_form', $body_data);
  load_footer();
}
}

/**
   * Find out shop transfer receive list to show
   * @return []
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function receivable_store_items()
{
 $status = $this->check_internet();
 if($status==1) {
  load_header();
  $this->load->view('no_internet');
  load_footer();
}
else
{
  $user_account_folder = $this->Transfer_model->get_folder_name();

  $shop_info = $this->Transfer_model->get_my_shop();
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_if_transfer_receives/".$shop_info['shop_id'];
  $output=file_get_contents($create_shop_backup_url);
  $result = json_decode($output, true);
  $transfer_details_id = $this->uri->segment(3);
  $transfer_details = $this->Transfer_model->get_transfer_details($transfer_details_id)->row_array();
  $body_data= array(
   'transfer_details' => $transfer_details,
   'transfer_in' => $result,
   );
  load_header();
  $this->load->view('warehouse/receive_store_items', $body_data);
  load_footer();
}
}


/**
   * Update cashbox amount with respect of transfer amount
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function update_cashbox()
{
  $cash_amount = $this->input->post('cash_amount');
  $transfer_details_id = $this->input->post('transfer_details_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
  $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $cash_amount;
  $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);
  if($cashbox_update)
  {
    $change_transfer_last_status = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_adjust_status/".$transfer_details_id;
    $output=file_get_contents($change_transfer_last_status); 
    $result = json_decode($output, true);
    echo $result;exit;
  }
}

/**
   * Update cashbox amount with respect of transfer amount
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
  **/
public function update_own_cashbox()
{
  $cash_amount = $this->input->post('cash_amount');
  
  $transfer_details_id = $this->input->post('transfer_details_id');
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
  $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $cash_amount;
  $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);
  
  $data['status'] = 'finished';
  $data['transfer_details_id'] = $transfer_details_id;
  $update_transfer_status = $this->Transfer_model->update_transfer_list_completed($transfer_details_id, $data);
  if($update_transfer_status)
  {
    $change_transfer_last_status = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/update_transfer_adjust_status_finish/".$transfer_details_id;
    $output=file_get_contents($change_transfer_last_status); 
    $result = json_decode($output, true);
    echo $result;exit;
  }
}


    /**
   * Get transfer receive item list to show
   *
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
   **/
    public function receivable_transfer_details()
    {

     $status = $this->check_internet();
     if($status==1) {
      load_header();
      $this->load->view('no_internet');
      load_footer();
    }
    else
    {
      $user_account_folder = $this->Transfer_model->get_folder_name();

      $shop_info = $this->Transfer_model->get_my_shop();
      $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_if_transfer_receives_warehouse/".$shop_info['shop_id'];
      $output=file_get_contents($create_shop_backup_url); 
            //echo $output;exit;
      $result = json_decode($output, true);
      $transfer_details_id = $this->uri->segment(3);
      $transfer_details = $this->Transfer_model->get_transfer_details($transfer_details_id);
      $body_data= array(
       'transfer_details' => $transfer_details,
       'transfer_in' => $result,
       'transfer_to' => $shop_info,
       );
      load_header();
      $this->load->view('warehouse/Transfer_receive_items_form', $body_data);
      load_footer();
    }    
  }

/**
   * Update cashbox and transfer status
   * @return null
   * @author Rasel <raselahmed2k7@gmail.com>
   **/
public function update_received_transfer_info()
{
 $transfer_details_id = $this->uri->segment(3);
 $transfer_information  = $this->Transfer_model->get_transfer_details($transfer_details_id);
 $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
 $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $transfer_information['total_amount'];
 $cashbox_update = $this->Transfer_model->update_final_cashbox_amount($cashbox_amount_update_data);

 $data['publication_status'] = "deactivated";
 $update = $this->Transfer_model->update_transfer_status($data, $transfer_details_id);
 if($update) {
  redirect('Transfer_items', 'refresh');
}
exit;
}

    /**
* Check items qauantity from inventory
* @param  string $item_spec_set_id 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function check_items_on_inventory($item_spec_set_id)
{
  $current_items_on_inventory = $this->Transfer_model->get_items_inventory($item_spec_set_id);
  echo json_encode($current_items_on_inventory);
  exit;
}
    /**
* find transfer items by transfer details id
 *
* @param  null 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function get_warehouse_items_by_transfer_details()
{
  $user_account_folder = $this->Transfer_model->get_folder_name();
  $detailsId = $this->input->post('keyword');
  $shop_id = $this->input->post('shop_id');
  $shop_info = $this->Transfer_model->get_my_shop();
  $create_shop_backup_url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/Registration/get_transfer_items_warehouse/".$detailsId."/".$shop_id."/".$shop_info['shop_id'];
  $output = file_get_contents($create_shop_backup_url); 
  echo $output;
  exit;
}


    /**
* find stores on select search
 *
* @param  null 
* @return null
* @author Rasel <raselahmed2k7@gmail.com>
**/
public function search_store_by_name()
{
 $text = $this->input->post('q');
 $currentShopId = $this->session->userdata('stores_id');
 $result = $this->Transfer_model->get_other_stores($text, $currentShopId);
 echo json_encode($result->result_array());
}

}
/* End of file Transfer_items.php */
/* Location: ./application/controllers/Transfer_items.php */
?>