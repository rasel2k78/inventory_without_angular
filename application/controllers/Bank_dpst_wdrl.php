<?php
/**
 * Bank deposit and withdrawal Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Use this controller for depositing and withdrawing from bank accounts. 
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
 **/
class Bank_dpst_wdrl extends CI_Controller {
    /**
     * This is a constructor function
     *
     * This load Bank Model and language files when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Bank_model');
      $this->load->model('User_access_check_model');
      $cookie = $this->input->cookie('language', true);
      $this->lang->load('left_side_nev_lang', $cookie);
      $this->lang->load('bank_create_lang', $cookie);
      $this->lang->load('bank_dw_lang', $cookie);
      $this->lang->load('left_side_nev_lang', $cookie);
      $this->lang->load('access_message_lang', $cookie);
      userActivityTracking();
      
      $user_id = $this->session->userdata('user_id');
      if ($user_id == null) {
        redirect('Login', 'refresh');
      }
    }
      /**
     * This function is used for loading bank deposit and withdrawal form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
      public function index()
      {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
          $this->load->view('bank/add_bank_dpst_wtdl');
        }
        else{
          $this->load->view('access_deny/not_permitted');
        }
        load_footer();
      }
     /**
     * This function is used for boostrap select of bank's information in database.
     * 
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

     public function search_bank_acc_by_name()
     {
      $text = $this->input->post('q');
      if($text == null || $text=="") {
        echo "input field empty";
      }
      else{
       echo json_encode( $this->Bank_model->search_bank_acc($text));   
     }
   }
    /**
     * This function save all deposit-withdrawal informations of bank in database.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_deposit_withdrawal_amount_info()
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $output=array();
      $output['success'] = 1;
      $output['error'] = array();
      if($this->input->post('cash_type', true)==null) {
        $output['error']['cash_type'] = $this->lang->line('validation_amount');
        $output['success'] = 0;
      }
      if($this->input->post('deposit_withdrawal_amount', true)==null || !is_numeric($this->input->post('deposit_withdrawal_amount', true))) {
        $output['error']['deposit_withdrawal_amount'] = $this->lang->line('validation_amount');
        $output['success'] = 0;
      }
      if($this->input->post('bank_acc_id', true)==null ||$this->input->post('bank_acc_id',true)== "") {
        $output['error']['bank_acc_id'] = $this->lang->line('validation_amount');
        $output['success'] = 0;
      }
      $bank_acc_id= $this->input->post('bank_acc_id', TRUE);
      $curr_bank_amt = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);
      if($this->input->post('cash_type', true)== "withdrawal" && $this->input->post('deposit_withdrawal_amount', true) > $curr_bank_amt['final_amount'] ) {
        $output['error']['deposit_withdrawal_amount'] = $this->lang->line('withdrawal_failure');
        $output['success'] = 0;
      }
      if($output['success']==0) {
        echo json_encode($output);
        exit();
      }
      else{
        $data= array(
         'cash_description' =>$this->input->post('cash_description', true),
         'cash_type' => $this->input->post('cash_type', true),
         'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
         'bank_acc_id' => $this->input->post('bank_acc_id', TRUE),
         'user_id' => $this->session->userdata('user_id'),
       );
        //Added by zubayer 2019-04-09
        $date = $this->input->post('date');
        if(!empty($date)){
          $data['date_created'] = date("Y-m-d H:i:s", strtotime($date));
        }
        $this->db->trans_begin();
        if($data['cash_type']== 'deposit') {
          $final_data['final_amount'] = $curr_bank_amt['final_amount'] + $data['deposit_withdrawal_amount'];
        }
        else{
          $final_data['final_amount'] = $curr_bank_amt['final_amount'] - $data['deposit_withdrawal_amount'];
        }
        $this->Bank_model->update_total_amt_info_of_acc($final_data,$bank_acc_id);

        $output['data'] = $this->Bank_model->save_bank_dw_info($data);
        echo json_encode($output);
        if ($this->db->trans_status() === false) {
         $this->db->trans_rollback();
       }
       else
       {
         $this->db->trans_commit();
       }
     }
        // }
        // else{
        //     echo json_encode("No Permission");
        // }

   }
    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_bank_dw_info_for_datatable()
    {        
      $filters = $this->input->get();
      $all_data = $this->Bank_model->all_bank_dw_info($filters);
      $all_data_without_limit = $this->Bank_model->all_bank_dw_info($filters, true);
      $all_data_final = $this->Bank_model->all_bank_dw_info($filters, true);
      $output_data=[];
      $output_data["draw"]=$filters['draw'];
      $output_data["recordsTotal"]=$all_data_without_limit;
      $output_data["recordsFiltered"]=$all_data_final;
      $output_data["data"]=$all_data;
      echo json_encode($output_data);
    }
    /**
     * This function collect all informations of individual deposit/withdrawal type by parameter.
     * 
     * @param string $cashbox_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_statement_info($bank_statement_id)
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $body_data= array(
       'all_bank_statement_by_id' => $this->Bank_model->all_bank_statement_by_id($bank_statement_id),
     );
      echo json_encode($body_data);
        // }
        // else {
        //     echo json_encode("No Permission");
        // }
    }
    /**
     * This function is used for update cashbox's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function update_statement_info()
    {
      $output=array();
      $output['success'] = 1;
      $output['error'] = array();
      if($this->input->post('deposit_withdrawal_amount_edit', true)==null || !is_numeric($this->input->post('deposit_withdrawal_amount_edit', true))) {
        $output['error']['deposit_withdrawal_amount_edit'] = $this->lang->line('validation_amount');
        $output['success'] = 0;
      }
      $cash_type = $this->input->post('cash_type_edit', true);
      $bank_acc_id= $this->input->post('bank_acc_id_edit', true);
      $deposit_withdrawal_amount = $this->input->post('deposit_withdrawal_amount_edit', true);
      $bank_statement_id= $this->input->post('bank_statement_id', true);
      $current_bank_amount = $this->Bank_model->current_bank_amount_info($bank_acc_id);
      if($cash_type == 'withdrawal' && ($deposit_withdrawal_amount >  $current_bank_amount['final_amount']) ) {
        $output['error']['deposit_withdrawal_amount_edit'] = $this->lang->line('more_amount_than_cashbox');
        $output['success'] = 0;    
      }
      if($output['success']==0) {
        echo json_encode($output);
        exit();
      }
      if($output['success'] == 1) {
        $bank_statement_id= $this->input->post('bank_statement_id', true);
        $data=array(
         'cash_type' => $this->input->post('cash_type_edit', true),
         'cash_description' => $this->input->post('cash_description_edit', true),
         'bank_acc_id' => $this->input->post('bank_acc_id_edit', true),
         'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount_edit', true),
         'user_id' => $this->session->userdata('user_id'),
       );
        $this->db->trans_begin();
        $previous_amount_type_by_id = $this->Bank_model->previous_amount_info_by_id($bank_statement_id);
        $difference_amount = $data['deposit_withdrawal_amount'] - $previous_amount_type_by_id['deposit_withdrawal_amount'];
        $current_bank_amount = $this->Bank_model->current_bank_amount_info($bank_acc_id);
        if($data['cash_type']== 'deposit' && $previous_amount_type_by_id['cash_type'] == 'deposit') {
         $final_data['final_amount'] = $current_bank_amount['final_amount'] + $difference_amount;
       }
       if($data['cash_type']== 'withdrawal' && $previous_amount_type_by_id['cash_type'] == 'withdrawal') {
         $final_data['final_amount'] = $current_bank_amount['final_amount'] - $difference_amount;
       }
       if($data['cash_type']== 'withdrawal' && $previous_amount_type_by_id['cash_type'] == 'deposit') {
         $final_data['final_amount'] = $current_bank_amount['final_amount'] - $data['deposit_withdrawal_amount'];
       }
       if($data['cash_type']== 'deposit' && $previous_amount_type_by_id['cash_type'] == 'withdrawal') {
         $final_data['final_amount'] = $current_bank_amount['final_amount'] + $data['deposit_withdrawal_amount'];
       }
       $bank_acc_id = $this->input->post('bank_acc_id_edit', true);
       $this->Bank_model->update_bank_amount_info($final_data,$bank_acc_id);
       $output['data'] = $this->Bank_model->update_statement_info($bank_statement_id, $data);
       echo json_encode($output);
       if ($this->db->trans_status() === false) {
         $this->db->trans_rollback();
       }
       else
       {
         $this->db->trans_commit();
       }
     }
     else{
      echo json_encode($output);
    }
  }
    /**
     * This function will delete/hide  the cashbox's information by "cashbox_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $cashbox_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function delete_statement($statement_id)
    {
        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
      $get_statement_details = $this->Bank_model->get_bank_acc_info($statement_id);
      $bank_acc_id= $get_statement_details['bank_acc_id'];
      $get_curr_balance= $this->Bank_model->get_bank_balance_info($bank_acc_id);

      if($get_statement_details['cash_type'] == 'deposit') {
        $final_data['final_amount'] = $get_curr_balance['final_amount'] - $get_statement_details['deposit_withdrawal_amount'];
        $this->Bank_model->update_acc_balance_info($final_data, $bank_acc_id);
        $data['publication_status'] = 'deactivated';
      }

      if ($get_statement_details['cash_type'] == 'withdrawal') {
        $final_data['final_amount'] = $get_curr_balance['final_amount'] + $get_statement_details['deposit_withdrawal_amount'];
        $this->Bank_model->update_acc_balance_info($final_data, $bank_acc_id);
        $data['publication_status'] = 'deactivated';
      }
      if($this->Bank_model->delete_statement_info($statement_id, $data)) {
        echo "success";    
      }    
      else{
        echo "failed";
      }
        // }
        // else 
        // {
        //     echo "No Permission";
        // }
    }
  }

  /* End of file Bank_dpst_wdrl.php */
/* Location: ./application/controllers/Bank_dpst_wdrl.php */