<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance_buy extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Inventory_model');
    $this->load->model('Advance_buy_model');
    $this->load->model('Bank_model');
    $this->load->model('User_access_check_model');
    $cookie = $this->input->cookie('language', true);
    $this->lang->load('buy_page_lang', $cookie);
    $this->lang->load('add_item_form_lang', $cookie);
    $this->lang->load('add_category_form_lang', $cookie);
    $this->lang->load('spec_name_page_lang', $cookie);
    $this->lang->load('add_vendor_form_lang', $cookie);
    $this->lang->load('left_side_nev_lang', $cookie);


    $user_id = $this->session->userdata('user_id');
    if ($user_id == null) {

      redirect('Login', 'refresh');
    }
  }

  public function index()
  {
    load_header();
    $body_data= array(
     'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
     );
    $this->load->view('Advance_buy/advance_buy_page', $body_data);
    load_footer();
  }

  public function save_adv_buy_info()
  {
    userActivityTracking();

    $data = $this->input->post('all_data');
    // echo "<pre>";
    // print_r($data);
    // exit();
    $ready_data_basic = array();
    $error_list = array();

    if(isset($data['all_basic_data']['adv_amt'])){
      $adv_paid = $data['all_basic_data']['adv_amt'];
      $adv_buy_id=$data['all_basic_data']['adv_buy_id'];
    }
    if(!isset($data['all_basic_data']['adv_amt'])){
      $adv_paid =  0;
      $adv_buy_id= 0;
      // echo "no_adv";
    }
    if(!isset($data['all_basic_data']['adv_amt'])){
      if($data['all_basic_data']['paid'] == $data['all_basic_data']['net_payable']){
        array_push($error_list, 'Make Partial Payment or Do Regular Buy');
      }
    }
    if(!isset($data['all_basic_data']['adv_amt'])){
      if(trim($data['all_basic_data']['receivable_date']) == "") {
        array_push($error_list, 'Enter Product Receivable Date');
      }
    }
    if($data['all_basic_data']['vendor_id'] == "") {
      array_push($error_list, 'Select Vendor');
    }
    $buy_voucher = $data['all_basic_data']['voucher_no'];
    if($buy_voucher == null) {
      array_push($error_list, 'Voucher No Cannot Be Empty');
    }
    // if(isset($data['all_basic_data']['adv_amt'])){

    // }
    if(isset($data['all_basic_data']['adv_amt'])){
      $check_voucher = $this->Inventory_model->check_voucher_is_available($buy_voucher);
      if($check_voucher) {
        array_push($error_list, 'Voucher No Already Exist In Buy');
      }
    }
    if(!isset($data['all_basic_data']['adv_amt'])){
      $check_adv_voucher = $this->Inventory_model->check_adv_voucher_availablity($buy_voucher);
      if($check_adv_voucher) {
        array_push($error_list, 'Voucher No Already Exist In Advance Buy');
      }
    }

    if(!is_numeric($data['all_basic_data']['discount'])) {
      array_push($error_list, 'Discount Amount Is Not Valid');
    }
    if($data['all_basic_data']['paid'] =="") {

      $due_amount= $data['all_basic_data']['due'];
    }
    else{
      $due_amount= $data['all_basic_data']['due_after_paid'];
    }
    $vendor_id= $data['all_basic_data']['vendor_id'];

    if($due_amount > 0 && $vendor_id == null) {
      array_push($error_list, 'Select Vendor for Due Transaction');
    }
    if($due_amount < 0) {
      array_push($error_list, 'Enter actual paid amount');
    }
    if($data['all_basic_data']['discount'] < 0) {
      array_push($error_list, 'Discount Amount Is Not Numeric');
    }
    if($data['all_basic_data']['total_cost'] < 0) {
      array_push($error_list, 'Landed Cost Is Not Numeric');
    }
    if($data['all_basic_data']['paid'] < 0) {
      array_push($error_list, 'Paid Amount Is Not Numeric');
    }
    if($data['all_basic_data']['paid'] ==null || !is_numeric($data['all_basic_data']['paid']) ) {
      array_push($error_list, 'Total Paid Amount is Empty or Not Numeric');
    }
    if(($data['all_basic_data']['discount'] == null ||  $data['all_basic_data']['discount'] == 0) && $data['all_basic_data']['paid'] !=null && $data['all_basic_data']['net_payable'] != ($data['all_basic_data']['paid'] + $data['all_basic_data']['due_after_paid'] + $adv_paid)) {
      array_push($error_list, 'Calculation Mismatch');
    }
    if($data['all_basic_data']['discount'] > 0 && ( $data['all_basic_data']['net_pay_after_discount'] - $data['all_basic_data']['paid']) != $data['all_basic_data']['due_after_paid'] ) {
      array_push($error_list, 'Calculation Mismatch for discount');
    }
    if ($data['all_basic_data']['payment_type'] == "cheque" && $data['all_basic_data']['bank_acc_id'] == "") {
      array_push($error_list, 'Bank Acc Not Select');
    }
    if ($data['all_basic_data']['payment_type'] == "cheque" && $data['all_basic_data']['cheque_page_num'] == "") {
      array_push($error_list, 'Cheque Page Number Empty');
    }
    $bank_account_id= $data['all_basic_data']['bank_acc_id'];
    $paid_amount= $data['all_basic_data']['paid'];
    $curr_bank_amount = $this->Bank_model->get_final_amt_by_acc($bank_account_id);
        // if($data['all_basic_data']['payment_type'] == "cheque" && $paid_amount > $curr_bank_amount['final_amount']){
        //     array_push($error_list, 'Not Enough Balance in Selected Account');
        // }
    if(count($error_list)>0) {
      echo json_encode($error_list);
      exit();
    }
    foreach ($data['item_info'] as $key => $value) {
      $value['item_spec_set_id'] ;
      $value['quantity'] ;
      $value['buying_price'] ;
            // $return_data = $this->Inventory_model->check_sell($value);
      if($value['quantity'] == null || $value['quantity'] < 0 ) {
        array_push($error_list, 'Item Quantity is Invalid');
      }
      if($value['buying_price'] ==null) {
                // array_push($error_list,$value['item_name'].' price not given');
        array_push($error_list, 'Item Buying Price is Empty');
      }
      if($value['item_spec_set_id'] ==null) {
        array_push($error_list, 'Item Not Selected');
      }
    }
    if(count($error_list)>0) {
      echo json_encode($error_list);
      exit();
    }
        // $buy_type['buy_type'] = $data['all_basic_data']['buy_type'];
    $ready_data_basic['voucher_no'] = $data['all_basic_data']['voucher_no'];
    $ready_data_basic['vendors_id'] = $data['all_basic_data']['vendor_id'];
    $ready_data_basic['landed_cost'] = $data['all_basic_data']['total_cost'];
    $ready_data_basic['user_id'] = $this->session->userdata('user_id');
    $ready_data_basic['discount'] = $data['all_basic_data']['discount'];
    if(isset($data['all_basic_data']['adv_amt'])){
      $ready_data_basic['adv_buy_details_id'] = $data['all_basic_data']['adv_buy_id'];
    }

    if(isset($data['all_basic_data']['adv_amt'])){
      $purchase_date = $data['all_basic_data']['purchase_date'];
      if($purchase_date != null){
        $ready_data_basic['date_created'] = $data['all_basic_data']['purchase_date'];
      }
    }
    $ready_data_basic['grand_total'] = $data['all_basic_data']['net_payable'];
    if($ready_data_basic['discount'] == null || $ready_data_basic['discount'] == 0) {

      $ready_data_basic['net_payable'] = $data['all_basic_data']['net_payable'];
    }
    if($ready_data_basic['discount'] > 0) {

      $ready_data_basic['net_payable'] = $data['all_basic_data']['net_pay_after_discount'] + $adv_paid;
    }
    $ready_data_basic['paid'] = $data['all_basic_data']['paid'] + $adv_paid;
    $ready_data_basic['due'] = $data['all_basic_data']['due_after_paid'];
    if(!isset($data['all_basic_data']['adv_amt'])){
      $ready_data_basic['receivable_date'] = $data['all_basic_data']['receivable_date'];
    }

    $this->db->trans_begin();
    if(!isset($data['all_basic_data']['adv_amt'])){
      $buy_details_uuid = $this->Advance_buy_model->save_adv_buy_basic_info($ready_data_basic);
      $this->Advance_buy_model->save_adv_buy_item_info($data['item_info'], $buy_details_uuid);
    }
    else{
      $buy_details_uuid = $this->Inventory_model->save_buy_basic_info($ready_data_basic);
      $this->Inventory_model->save_buy_item_info($data['item_info'], $buy_details_uuid, $purchase_date);
      $this->Inventory_model->save_item_info_to_inventory($data['item_info']);
      /*Serial or imei use as barcode starts*/
      if(array_key_exists("imei_barcode", $data))
      {
        foreach ($data['imei_barcode'] as $arr_imei_bar) 
        {
          foreach ($arr_imei_bar as $val_of_imei)
          {
            $val_of_imei['buy_details_id']= $buy_details_uuid;
            $val_of_imei['user_id'] = $this->session->userdata('user_id');
            $check_imei_avialability = $this->Inventory_model->check_imei_avilable_info($val_of_imei['imei_barcode']);
            $is_sell = $this->Inventory_model->check_is_sell($val_of_imei['imei_barcode']);
            if(!empty($check_imei_avialability))
            {
             $this->db->trans_rollback();
                     // echo "duplicate imei inserted";
             array_push($error_list, 'Duplicate IMEI or SERIAL Inserted ( '. $check_imei_avialability.' )');
             echo json_encode($error_list);

             return;
           }
           else if (!empty($is_sell))
           {
             $this->db->trans_rollback();
                     // echo "already sold once and status deactivated";
             array_push($error_list, 'Particular IMEI or SERIAL Already Sold Once');
             echo json_encode($error_list);
             return;
           }
           else
           {
            $this->Inventory_model->save_imei_barcode_info($val_of_imei);
          }
        }
      }
    }

    /*Serial / imei use as barcode ends*/
    /* Safety Stock Check Starts*/
    foreach ($data['item_info'] as $key => $value) 
    {
      $item_id= $value['item_spec_set_id'];
      $last_thirty_days_total_sell= $this->Inventory_model->thirty_days_sell_qty($item_id);
      $one_week_avg_sell= floor(($last_thirty_days_total_sell['quantity'])/30);
      $last_thirty_days_individual_sell_qty = $this->Inventory_model->individual_sell_qty($item_id);
      $sum_of_square_of_diff= 0;
      foreach ($last_thirty_days_individual_sell_qty as $key2 => $value2) {
        $sell_quantity = $value2['quantity'];
        $diff_from_avg_sell_qty = ($one_week_avg_sell - $sell_quantity)*($one_week_avg_sell - $sell_quantity);
        $sum_of_square_of_diff+=$diff_from_avg_sell_qty;
      }
      $avg_of_square = floor($sum_of_square_of_diff/30);
      $sqr_root_of_avg = floor(sqrt($avg_of_square));
      $number_of_units_to_stack =  floor($one_week_avg_sell + ($sqr_root_of_avg * 1.65));
      $approx_stock_for_a_week = $number_of_units_to_stack*7;
      $current_inventory_qty = $this->Inventory_model->items_info_from_inventory($item_id);
      if ($current_inventory_qty['quantity'] > ($approx_stock_for_a_week*4) ) {
        $status_data['inventory_status']="Excess";
        $this->Inventory_model->update_inventory_status($item_id, $status_data);
      }
      if ($current_inventory_qty['quantity'] <= $approx_stock_for_a_week) {
        $status_data['inventory_status']="Low";
        $this->Inventory_model->update_inventory_status($item_id, $status_data);
      }
      if ($current_inventory_qty['quantity'] <= ($approx_stock_for_a_week*4) && $current_inventory_qty['quantity'] > $approx_stock_for_a_week ) {
        $status_data['inventory_status']="Normal";
        $this->Inventory_model->update_inventory_status($item_id, $status_data);
      }
    }
    /* Safety Stock Ends*/
  }
  if ($data['all_basic_data']['payment_type'] == "cash") {
    if(isset($data['all_basic_data']['adv_amt'])){
      $cash_data['cash_type']= "buy";
      if($purchase_date != ""){
        $cash_data['date_created'] = $data['all_basic_data']['purchase_date'];
      }
    }
    else{
      $cash_data['cash_type']= "advance_buy";
    }

    $cash_data['buy_or_sell_details_id']= $buy_details_uuid;
    $cash_data['user_id'] = $ready_data_basic['user_id'];
    $cash_data['deposit_withdrawal_amount'] = $data['all_basic_data']['paid'] + $ready_data_basic['landed_cost'] ;
    $this->Inventory_model->save_cash_box_info($cash_data);
    $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    if(!isset($data['all_basic_data']['adv_amt'])){
      $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['all_basic_data']['paid'];
    }
    else{
      $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['all_basic_data']['paid'];
      $this->Advance_buy_model->update_adv_buy_status($adv_buy_id);
      // $cash_data['cash_type']= "buy";
      // $cash_data['user_id'] = $ready_data_basic['user_id'];
      // $cash_data['deposit_withdrawal_amount'] = $data['all_basic_data']['paid'] ;
      // $cash_data['buy_or_sell_details_id']= $buy_details_uuid;
      // $this->Inventory_model->save_cash_box_info($cash_data);
    }
    $this->Inventory_model->update_total_cashbox_info($final_data);
  }

  if ($this->db->trans_status() === false) {
    $this->db->trans_rollback();
    echo "error transection";
  }
  else
  {
    $this->db->trans_commit();
    echo json_encode("success");
  }
}

}

/* End of file Advance_buy.php */
/* Location: ./application/controllers/Advance_buy.php */