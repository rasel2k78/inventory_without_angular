<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class PasswordRecovery extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Loginmodel');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }
    }

    public function index()
    {
        
    }

    public function pr($token = '')
    {
        
        if ($token == '') {
            echo "token not valid";
        }
        
        else 
        {
            $data['token'] = $token;

            $result = $this->Loginmodel->get_token_details($token);
            if (count($result)>0) {
                //$this->load->view('header_view');
                $this->load->view('reset_password_view', $data);
                //$this->load->view('footer_view');
            }
            else
            {
                //$this->load->view('reset_password_view',$data);
                echo "Something went wrong";
            }
            //var_dump($result);
        }

    }
}

/* End of file  */
/* Location: ./application/controllers/ */