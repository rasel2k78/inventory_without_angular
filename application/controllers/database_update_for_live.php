<?php 

"CREATE TABLE  `banks` (
`banks_id` char(36) NOT NULL,
`banks_name` varchar(100) NOT NULL,
`banks_abbe` varchar(45) NOT NULL,
`banks_description` text,
`date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
`user_id` char(36) NOT NULL,
`stores_id` varchar(45) DEFAULT 'change_with_storeid'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE  `bank_accounts` (
`bank_acc_id` char(36) NOT NULL,
`bank_acc_num` varchar(100) NOT NULL,
`final_amount` float NOT NULL DEFAULT '0',
`bank_acc_branch` varchar(100) NOT NULL,
`bank_acc_des` text,
`date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`user_id` char(36) NOT NULL,
`stores_id` varchar(45) DEFAULT 'change_with_storeid',
`banks_id` char(36) NOT NULL,
`publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
`selection_status` enum('selected','not_selected') DEFAULT NULL,
`bank_acc_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE  `bank_statement` (
`bank_statement_id` char(36) NOT NULL,
`date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`user_id` char(36) NOT NULL,
`publication_status` enum('activated','deactivated') DEFAULT 'activated',
`deposit_withdrawal_amount` float NOT NULL,
`cash_type` enum('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card') NOT NULL,
`cash_description` text,
`stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
`buy_sell_and_other_id` char(36) DEFAULT NULL,
`bank_acc_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE  `cheque_details` (
`cheque_details_id` char(36) NOT NULL,
`buy_sell_or_others_id` char(36) NOT NULL,
`bank_acc_id` char(36) NOT NULL,
`cheque_number` varchar(100) NOT NULL,
`user_id` char(36) NOT NULL,
`date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`stores_id` varchar(45) DEFAULT 'change_with_storeid',
`publication_status` enum('activated','deactivated') DEFAULT 'activated',
`bank_statement_id` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE  `pending_cheque_details` (
`pending_cheque_id` char(36) NOT NULL,
`sell_details_id` char(36) NOT NULL,
`cheque_number` varchar(100) NOT NULL,
`user_id` char(36) NOT NULL,
`stores_id` varchar(45) NOT NULL DEFAULT 'change_with_storeid',
`date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`publication_status` enum('activated','deactivated') DEFAULT 'activated',
`cheque_status` enum('pending','done') DEFAULT 'pending',
`cheque_cleared_type` enum('cash','bank_acc_dpst') DEFAULT NULL,
`bank_acc_id` char(36) DEFAULT NULL,
`cheque_amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;ALTER TABLE `demage_lost` ADD `demage_lost_type` ENUM('damage','lost') NOT NULL;ALTER TABLE `demage_lost` ADD `item_buying_price` FLOAT NOT NULL;ALTER TABLE `demage_lost` ADD `sub_total` FLOAT NOT NULL;ALTER TABLE `shop` ADD `shop_image` TEXT NOT NULL;ALTER TABLE buy_details DROP INDEX IF EXISTS voucher_no;ALTER TABLE `vendors` ADD `vendors_email` VARCHAR(200) NOT NULL;ALTER TABLE `item_spec_set` ADD `unique_barcode` ENUM('yes','no') NOT NULL;ALTER TABLE `pending_cheque_details` ADD `cheque_amount` FLOAT NOT NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `bank_accounts` ADD `selection_status` ENUM('selected','not_selected') NOT NULL;ALTER TABLE `bank_accounts` ADD `bank_acc_name` VARCHAR(100) NOT NULL;ALTER TABLE `vat` ADD `selection_status` ENUM('selected','not_selected') NOT NULL;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;ALTER TABLE `sell_payment_details` CHANGE `payment_type` `payment_type` ENUM('cash','card','cash_card_both','voucher','both_cash','cheque') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `sell_details` ADD `discount_percentage` ENUM('yes','no') NOT NULL ;ALTER TABLE `expenditures` ADD `payment_type` ENUM('cash','cheque') NOT NULL;ALTER TABLE `sell_details` ADD `discount_percentage` VARCHAR(80) NOT NULL ;ALTER TABLE `expenditures` ADD `  payment_type` VARCHAR(80) NOT NULL;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','expense_edited') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;ALTER TABLE `expenditures` ADD `payment_type` VARCHAR(80) NOT NULL;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','expense_edited') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `bank_statement` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense','buy_due_paymentmade','sell_due_paymentmade','returned_money','transfer','sell_with_card','expense_edited','received') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL"

$dbarray['db201700503'] = "CREATE TABLE `imei_barcode` (
`imei_barcode_id` CHAR(36) NOT NULL,
`user_id` CHAR(36) NOT NULL,
`date_created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
`date_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`publication_status` ENUM('activated', 'deactivated') NULL DEFAULT 'activated',
`barcode_status` ENUM('used', 'not_used') NOT NULL DEFAULT 'not_used',
`item_spec_set_id` CHAR(36) NOT NULL,
`imei_barcode` VARCHAR(200) NOT NULL,
`stores_id` VARCHAR(45) NULL
)ENGINE = InnoDB;ALTER TABLE `item_spec_set` ADD  `product_warranty` VARCHAR(45) NOT NULL;ALTER TABLE `imei_barcode` ADD `sell_details_id` CHAR(36) NOT NULL;ALTER TABLE `sell_cart_items` ADD `imei_barcode` VARCHAR(45) NOT NULL;ALTER TABLE `imei_barcode` ADD `sell_details_id` CHAR(36) NOT NULL;ALTER TABLE `transfer` CHANGE `price` `buying_price` FLOAT(11) NOT NULL;ALTER TABLE `transfer` ADD `selling_price` FLOAT NOT NULL;ALTER TABLE `transfer_details` CHANGE `status` `status` ENUM('pending','received','completed','adjusted') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `transfer_details` CHANGE `status` `status` ENUM('pending','received','completed','adjusted','finished') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `sell_cart_items` ADD `imei_barcode` VARCHAR(45) NOT NULL;ALTER TABLE `imei_barcode` ADD `buy_details_id` CHAR(36) NOT NULL;ALTER TABLE imei_barcode DROP INDEX imei_barcode_UNIQUE;ALTER TABLE `shop` ADD `active_printer` ENUM('normal','pos','none') NOT NULL DEFAULT 'none';ALTER TABLE `sell_cart_items` ADD `used_imei_barcode` VARCHAR(100) NOT NULL";


$dbarray['db201700503'] = "ALTER TABLE `sell_details` ADD  `card_charge_amt` FLOAT NOT NULL DEFAULT '0';ALTER TABLE `sell_payment_details` ADD  `card_charge_percent` FLOAT NULL;ALTER TABLE `sell_details` ADD `voucher_unique_barcode` VARCHAR(100) NOT NULL;ALTER TABLE `item_spec_set` CHANGE `product_warranty` `product_warranty` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;ALTER TABLE `sell_details` ADD `voucher_unique_barcode` VARCHAR(100) NOT NULL;ALTER TABLE `sell_details` ADD `custom_voucher_no` VARCHAR(80) NOT NULL;ALTER TABLE `item_spec_set` ADD `hisab_id` INT NOT NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;CREATE TABLE `exchange_return_history` (
	`id` int(11) NOT NULL,
	`voucher_no` varchar(200) NOT NULL,
	`type` enum('exchanged_with_vendor','exchanged_with_customer','new_item_with_vendor','new_item_with_customer','returned_with_vendor','returned_with_customer') NOT NULL,
	`item_spec_set_id` varchar(200) NOT NULL,
	`date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`user_id` char(36) NOT NULL,
	`publication_status` enum('activated','deactivated') NOT NULL,
	`stores_id` char(36) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;ALTER TABLE `transfer_details` ADD `transfer_type` ENUM('transfer','warehouse') NOT NULL;ALTER TABLE `transfer` ADD `transfer_type` ENUM('transfer','warehouse') NOT NULL;ALTER TABLE `customers` ADD `customer_custom_id` VARCHAR(50) NOT NULL;ALTER TABLE `exchange_return_history` ADD `quantity` INT NOT NULL;ALTER TABLE `shop` ADD `stores_id` CHAR(36) NOT NULL;UPDATE shop SET stores_id=shop_id;ALTER TABLE `imei_barcode` CHANGE `barcode_status` `barcode_status` ENUM('used','not_used','demaged') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'not_used';ALTER TABLE `demage_lost` ADD `imei_barcode_id` CHAR(36) NOT NULL;ALTER TABLE item_spec_set DROP COLUMN `hisab_id`;ALTER TABLE `sell_payment_details` ADD `card_charge_percent` FLOAT NULL;ALTER TABLE `banks` ADD `charge_percentage` FLOAT NOT NULL;ALTER TABLE `account_folder` ADD `id` INT NOT NULL;ALTER TABLE `final_cashbox_amount` ADD `id` INT NOT NULL;ALTER TABLE `account_folder` ADD `stores_id` CHAR(36) NOT NULL;ALTER TABLE `final_cashbox_amount` ADD `stores_id` CHAR(36) NOT NULL;ALTER TABLE `transfer` ADD `whole_sale_price` FLOAT NOT NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission','transfer_delete') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `sell_details` ADD `card_charge_amt` FLOAT NOT NULL DEFAULT '0'";

	$dbarray['db201700503'] = "CREATE TABLE `batta_history` (
    `batta_history_id` CHAR(36) NOT NULL,
    `buy_sell_id` CHAR(36) NOT NULL,
    `buy_or_sell` ENUM('buy','sell') NOT NULL,
    `adjusted_amount` FLOAT NOT NULL,
    `date_created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `date_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `user_id` CHAR(36) NOT NULL,
    `publication_status` ENUM('activated','deactivated') NOT NULL DEFAULT 'activated',
    `stores_id` VARCHAR(45) NOT NULL
) ENGINE=INNODB DEFAULT CHARSET=latin1;ALTER TABLE `bank_accounts` CHANGE `final_amount` `final_amount` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `bank_statement` CHANGE `deposit_withdrawal_amount` `deposit_withdrawal_amount` DOUBLE NOT NULL;ALTER TABLE `batta_history` CHANGE `adjusted_amount` `adjusted_amount` DOUBLE NOT NULL;ALTER TABLE `buy_cart_items` CHANGE `buying_price` `buying_price` DOUBLE NOT NULL;ALTER TABLE `buy_cart_items` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `whole_sale_price` `whole_sale_price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `retail_price` `retail_price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_cart_items` CHANGE `discount_amount` `discount_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `buy_details` CHANGE `grand_total` `grand_total` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `discount` `discount` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `net_payable` `net_payable` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `paid` `paid` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `due` `due` DOUBLE NOT NULL;ALTER TABLE `buy_details` CHANGE `landed_cost` `landed_cost` DOUBLE NULL DEFAULT '0';ALTER TABLE `cashbox` CHANGE `deposit_withdrawal_amount` `deposit_withdrawal_amount` DOUBLE NOT NULL;ALTER TABLE `demage_lost` CHANGE `item_buying_price` `item_buying_price` DOUBLE NOT NULL;ALTER TABLE `demage_lost` CHANGE `sub_total` `sub_total` DOUBLE NOT NULL;ALTER TABLE `due_paymentmade` CHANGE `due_paymentmade_amount` `due_paymentmade_amount` DOUBLE NOT NULL;ALTER TABLE `expenditures` CHANGE `expenditures_amount` `expenditures_amount` DOUBLE NOT NULL;ALTER TABLE `final_cashbox_amount` CHANGE `total_cashbox_amount` `total_cashbox_amount` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `loan` CHANGE `loan_amount` `loan_amount` DOUBLE NOT NULL;ALTER TABLE `loan` CHANGE `total_payout_amount` `total_payout_amount` DOUBLE NOT NULL;ALTER TABLE `loan` CHANGE `actual_loan_taken` `actual_loan_taken` DOUBLE NOT NULL;ALTER TABLE `payout_loan` CHANGE `payout_amount` `payout_amount` DOUBLE NOT NULL;ALTER TABLE `pending_cheque_details` CHANGE `cheque_amount` `cheque_amount` DOUBLE NOT NULL;ALTER TABLE `return_from_customer` CHANGE `returned_amount` `returned_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `return_to_vendor` CHANGE `returned_amount` `returned_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `quantity` `quantity` INT NOT NULL;ALTER TABLE `sell_cart_items` CHANGE `selling_price` `selling_price` DOUBLE NOT NULL;ALTER TABLE `sell_cart_items` CHANGE `discount_amount` `discount_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `tax_percentage` `tax_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_cart_items` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `grand_total` `grand_total` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `discount` `discount` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `net_payable` `net_payable` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `paid` `paid` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `due` `due` DOUBLE NOT NULL;ALTER TABLE `sell_details` CHANGE `sub_total` `sub_total` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `received_money` `received_money` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `return_change` `return_change` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `vat_amount` `vat_amount` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `tax_percentage` `tax_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `sell_details` CHANGE `card_charge_amt` `card_charge_amt` DOUBLE NOT NULL DEFAULT '0';ALTER TABLE `sell_payment_details` CHANGE `amount` `amount` DOUBLE NOT NULL;ALTER TABLE `sell_payment_details` CHANGE `card_charge_percent` `card_charge_percent` DOUBLE NULL DEFAULT NULL;ALTER TABLE `tax_types` CHANGE `tax_types_percentage` `tax_types_percentage` DOUBLE NULL DEFAULT NULL;ALTER TABLE `transfer` CHANGE `price` `price` DOUBLE NULL DEFAULT NULL;ALTER TABLE `transfer_details` CHANGE `total_amount` `total_amount` DOUBLE NOT NULL;ALTER TABLE `vat` CHANGE `vat_percentage` `vat_percentage` DOUBLE NOT NULL;ALTER TABLE `transfer` CHANGE `buying_price` `buying_price` DOUBLE NOT NULL;ALTER TABLE `transfer` CHANGE `selling_price` `selling_price` DOUBLE NOT NULL;ALTER TABLE `transfer_details` CHANGE `total_amount` `total_amount` DOUBLE NOT NULL";

$dbarray['db201700503'] = "ALTER TABLE `transfer` ADD `imei_barcode` VARCHAR(200) NOT NULL;ALTER TABLE `transfer` ADD `is_unique` ENUM('yes','no') NOT NULL;ALTER TABLE `imei_barcode` CHANGE `barcode_status` `barcode_status` ENUM('used','not_used','demaged','transfered') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'not_used';ALTER TABLE `shop` CHANGE `phone` `phone` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL";


$dbarray[''] = "ALTER TABLE `sell_details` ADD `adv_sell_details_id` CHAR(36) NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission','transfer_delete','sell_advance_adjust') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;CREATE TABLE `adv_sell_details` (
  `adv_sell_details_id` char(36) NOT NULL,
  `grand_total` double NOT NULL,
  `discount` double NOT NULL,
  `discount_percentage` varchar(80) NOT NULL,
  `net_payable` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `due_pay_within` date DEFAULT NULL,
  `sub_total` double DEFAULT NULL,
  `received_money` double DEFAULT NULL,
  `return_change` double DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comments` text,
  `customers_id` char(36) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `tax_types_id` char(36) DEFAULT NULL,
  `tax_percentage` double DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
  `stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
  `sell_local_voucher_no` int(11) NOT NULL,
  `payment_type` enum('cash','card','cash_card_both','cheque') NOT NULL,
  `card_payment_voucher_no` varchar(100) DEFAULT NULL,
  `sales_rep_id` char(36) DEFAULT NULL,
  `vat_amount` double DEFAULT NULL,
  `voucher_unique_barcode` varchar(100) NOT NULL,
  `custom_voucher_no` varchar(80) NOT NULL,
  `card_charge_amt` double NOT NULL DEFAULT '0',
  `delivery_status` enum('pending','done','deleted') DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE `adv_sell_cart_items` (
  `adv_sell_cart_items_id` char(36) NOT NULL,
  `quantity` int(11) NOT NULL,
  `selling_price` double NOT NULL,
  `discount_amount` double DEFAULT NULL,
  `discount_type` enum('percentage','amount','free_quantity') DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `adv_sell_details_id` char(36) NOT NULL,
  `tax_types_id` char(36) DEFAULT NULL,
  `tax_percentage` double DEFAULT NULL,
  `sell_comments` text,
  `customers_id` char(36) DEFAULT NULL,
  `sell_type` enum('wholesale','retail') NOT NULL,
  `user_id` char(36) NOT NULL,
  `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
  `stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
  `item_spec_set_id` char(36) NOT NULL,
  `sub_total` double DEFAULT NULL,
  `imei_barcode` varchar(45) NOT NULL,
  `used_imei_barcode` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;ALTER TABLE `sell_details` ADD `transfer_details_id` CHAR(36) NULL";

$dbarray[''] = "CREATE TABLE `adv_buy_details` (
  `adv_buy_details_id` char(36) NOT NULL,
  `grand_total` double NOT NULL,
  `discount` double NOT NULL,
  `net_payable` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `receivable_date` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `voucher_no` varchar(45) NOT NULL,
  `vendors_id` char(36) NOT NULL,
  `landed_cost` double DEFAULT '0',
  `user_id` char(36) NOT NULL,
  `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
  `stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
  `receival_status` enum('pending','done','deleted') NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;CREATE TABLE `adv_buy_cart_items` (
  `adv_buy_cart_items_id` char(36) NOT NULL,
  `quantity` int(11) NOT NULL,
  `buying_price` double NOT NULL,
  `sub_total` double DEFAULT NULL,
  `whole_sale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `discount_amount` double DEFAULT NULL,
  `discount_type` enum('percentage','amount','free_quantity') DEFAULT NULL,
  `expire_date` varchar(80) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `adv_buy_details_id` char(36) NOT NULL,
  `comments` text,
  `user_id` char(36) NOT NULL,
  `publication_status` enum('activated','deactivated') NOT NULL DEFAULT 'activated',
  `stores_id` char(36) NOT NULL DEFAULT 'change_with_storeid',
  `item_spec_set_id` char(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;ALTER TABLE `buy_details` ADD `adv_buy_details_id` CHAR(36) NULL;ALTER TABLE `cashbox` CHANGE `cash_type` `cash_type` ENUM('exchange_with_customer','exchange_with_vendor','deposit','withdrawal','buy','sell','expense_edited','expense','buy_due_paymentmade','sell_due_paymentmade','advance_buy','advance_sell','returned_money','transfer','sell_chk_clred_by_cash','transferred','commission','transfer_delete','sell_advance_adjust','buy_advance_adjust') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;ALTER TABLE `adv_buy_details` ADD `receival_status` ENUM('pending','done','deleted') NOT NULL DEFAULT 'pending'"

?>