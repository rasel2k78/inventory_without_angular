<?php
/**
 * Loan Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for add new loan, edit loan , update loan info or delete loan info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/
class Loan extends CI_Controller
{
    /**
     * This is a constructor function
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Inventory_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('loan_page_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        
        $user_id = $this->session->userdata('user_id');
        if ($user_id == NULL) {

        	redirect('Login', 'refresh');
        }
    }
    /**
     * This function is used for loading loan form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $this->load->view('loan/add_loan_form');
        }
        else {
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();        
    }
    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_loan_info_for_datatable()
    {
        $filters = $this->input->get();
        $all_data = $this->Inventory_model->all_loan_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_loan_info($filters, true);
        $all_data_final = $this->Inventory_model->all_loan_info($filters, true);
        $output_data=[];
        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;
        echo json_encode($output_data);
    }
    /**
     * This function save all loan informations in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_loan_amount_info()
    {
        userActivityTracking();

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();
        if($this->input->post('loan_amount', true)==null || !is_numeric($this->input->post('loan_amount', true))) {
            $output['error']['loan_amount'] = $this->lang->line('validation_amount');
            $output['success'] = 0;
        }
        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else{
            $data= array(
               'loan_description' =>$this->input->post('loan_description', true),
               'loan_amount' => $this->input->post('loan_amount', true),
               'user_id' => $this->session->userdata('user_id'),
               'actual_loan_taken' => $this->input->post('loan_amount', true),
               'total_payout_amount' => 0,
               );
            $this->db->trans_begin();
            $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
            $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $data['loan_amount'];
            // $this->Inventory_model->update_total_cashbox_info($final_data);
            $amount['total_cashbox_amount'] = $this->input->post('loan_amount', true);
            if($current_cashbox_amount['total_cashbox_amount'] =="" || $current_cashbox_amount['total_cashbox_amount'] == NULL ) {
               $this->Inventory_model->save_deposit_amount_info($amount);
           }
           else{
               $this->Inventory_model->update_total_cashbox_info($final_data);
           }
           $output['data'] = $this->Inventory_model->save_loan_info($data);
           echo json_encode($output);
           if ($this->db->trans_status() === false) {
             $this->db->trans_rollback();
         }
         else
         {
           $this->db->trans_commit();
       }
   }
}
    /**
     * This function will delete/hide  the loan's information by "loan_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $loan_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function delete_loan($loan_id)
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $payout_history_check_by_loan_id = $this->Inventory_model->payout_history_info($loan_id);
            if($payout_history_check_by_loan_id == 0) {
                $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
                $previous_loan_by_id = $this->Inventory_model->previous_loan_info_by_id($loan_id);
                $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $previous_loan_by_id['loan_amount'];
                $data['publication_status'] = 'deactivated';
                $this->Inventory_model->update_total_cashbox_info($final_data);
                if($this->Inventory_model->delete_loan_info($loan_id, $data)) {
                    echo "success";    
                }
            }
            else if($payout_history_check_by_loan_id > 0) {
                echo "failed";
            }
        }
        else
        {
            echo "No Permission";
        }
    }
    /**
     * This function collect all informations of individual loan by parameter.
     * 
     * @param string $loan_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_loan_info($loan_id)
    {
        userActivityTracking();

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
               'all_loan_by_id' => $this->Inventory_model->all_loan_info_by_id($loan_id),
               );
            echo json_encode($body_data);
        }
        else{
            echo json_encode("No Permission");
        }
    }
    /**
     * This function is used for update loan's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function update_loan_info()
    {
        userActivityTracking();

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();
        if($this->input->post('loan_amount_edit', true)==null || !is_numeric($this->input->post('loan_amount_edit', true))) {
            $output['error']['loan_amount_edit'] = $this->lang->line('validation_amount');
            $output['success'] = 0;
        }
        $loan_id = $this->input->post('loan_id', true);
        $totally_payout_amount = $this->Inventory_model->all_loan_info_by_id($loan_id);
        if($this->input->post('loan_amount_edit', true) < $totally_payout_amount['total_payout_amount']) {
            $output['error']['loan_amount_edit'] = $this->lang->line('total_payout_validation');
            $output['success'] = 0;
        }
        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else
        {
            $data=array(
               'loan_description' => $this->input->post('loan_description_edit', true),
               'loan_amount' => $this->input->post('loan_amount_edit', true),
               'user_id' => $this->session->userdata('user_id'),
               );
            $this->db->trans_begin();
            $previous_loan_by_id = $this->Inventory_model->previous_loan_info_by_id($loan_id);
            $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
            if($data['loan_amount'] > $previous_loan_by_id['loan_amount']) {
             $difference_amount = $data['loan_amount'] - $previous_loan_by_id['loan_amount'];
             $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $difference_amount;
         }
         if($data['loan_amount'] < $previous_loan_by_id['loan_amount']) {
           $difference_amount = $previous_loan_by_id['loan_amount'] - $data['loan_amount'];
           $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $difference_amount;
       }
       $this->Inventory_model->update_total_cashbox_info($final_data);
       $output['data'] = $this->Inventory_model->update_loan_info($loan_id, $data);
       echo json_encode($output);
       if ($this->db->trans_status() === false) {
           $this->db->trans_rollback();
       }
       else
       {
           $this->db->trans_commit();
       }
   }
}
    /**
     * This function save all loan payout informations  in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_payout_loan()
    {
        userActivityTracking();

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();
        if($this->input->post('payout_amount', true)==null || !is_numeric($this->input->post('payout_amount', true))) {
            $output['error']['payout_amount'] = $this->lang->line('validation_amount');
            $output['success'] = 0;
        }
        $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        $current_loan_id= $this->input->post('loan_id', true);
        $currnt_loan_amount = $this->Inventory_model->all_loan_info_by_id($current_loan_id);
        $due_loan = $currnt_loan_amount['loan_amount'] - $currnt_loan_amount['total_payout_amount'];
        if($this->input->post('payout_amount', true) > $due_loan) {
            $output['error']['payout_amount'] = $this->lang->line('more_payout_than_loan');
            $output['success'] = 0;
        }
        if($this->input->post('payout_amount', true) > $current_cashbox_amount['total_cashbox_amount']) {
            $output['error']['payout_amount'] = $this->lang->line('not_enough_amount');
            $output['success'] = 0;
        }
        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else
        {
            $data= array(
               'loan_id' => $this->input->post('loan_id', true),
               'payout_amount' => $this->input->post('payout_amount', true),
               'payout_comments' => $this->input->post('payout_comments', true),
               'user_id' => $this->session->userdata('user_id'),
               );
            $this->db->trans_begin();
            $loan_id= $data['loan_id'];
            $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['payout_amount'];
            $this->Inventory_model->update_total_cashbox_info($final_data);
            $current_payout_amount =  $this->Inventory_model->all_loan_info_by_id($loan_id);
            $updated_data['total_payout_amount'] = $current_payout_amount['total_payout_amount'] + $data['payout_amount'];
            $updated_data['delete_adjust_status'] = 'adjustable';
            $this->Inventory_model->update_loan_info($loan_id, $updated_data);
            $output['data'] = $this->Inventory_model->save_payout_info($data);
            echo json_encode($output);
            if ($this->db->trans_status() === false) {
             $this->db->trans_rollback();
         }
         else
         {
           $this->db->trans_commit();
       }
   }
}
    /**
     * This function adjust equal payout loan amount and total loan taken.
     *
     * @param  string $loan_id
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function adjust_loan($loan_id)
    {
        userActivityTracking();
        
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $total_payout = $this->Inventory_model->sum_of_payout_loan($loan_id);
            $data['loan_amount'] = $total_payout['SUM(payout_amount)'];
            $total_loan = $this->Inventory_model-> all_loan_info_by_id($loan_id);
            $adjust_diffrence = $total_loan['loan_amount'] -  $data['loan_amount'];
            $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
            $final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $adjust_diffrence;
            $this->Inventory_model->update_total_cashbox_info($final_data);
            if($this->Inventory_model->update_adjust_info($loan_id, $data)) {
                echo "success";
            }
            else {
                echo "failed";
            }
        }
        else {
            echo "No Permission";
        }
    }

}
/* End of file Loan.php */
/* Location: ./application/controllers/Loan.php */