<?php 
/**
 * Sell Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for sell items.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/
class Sell extends CI_Controller
{
    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
    	parent::__construct();

    	check_if_logged_in();
    	$this->load->model('Inventory_model');
    	$this->load->model('Bank_model');
    	$this->load->model('User_access_check_model');
    	$cookie = $this->input->cookie('language', true);
    	$this->lang->load('sell_page_lang', $cookie);
    	$this->lang->load('add_item_form_lang', $cookie);

    	$this->lang->load('access_message_lang', $cookie);
    	$this->lang->load('add_customer_form_lang', $cookie);
    	$this->lang->load('add_sales_rep_form_lang', $cookie);
    	$this->lang->load('left_side_nev_lang', $cookie);
    	$user_id = $this->session->userdata('user_id');
    	if ($user_id == null) {
    		redirect('Login', 'refresh');
    	}
    }
    /**
     * This function is used for loading sell form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
    	load_header();
    	$user_id_logged = $this->session->userdata('user_id');
    	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

    		$store_details = $this->Inventory_model->store_details();
    		$body_data= array(
    			'store_details' => $store_details,
    			);
    		$this->load->view('sell/add_sell_form', $body_data);
    	}
    	else
    	{
    		$this->load->view('access_deny/not_permitted');
    	}

    	load_footer();    
    }

    public function invoice()
    {
    	load_header();
    	$sell_details_id= $this->uri->segment(3);
    	$store_details = $this->Inventory_model->store_details();
    	$voucher_data = $this->Inventory_model->collect_voucher_number($sell_details_id);
    	$customer_info= $this->Inventory_model->get_customer_info_for_voucher($voucher_data['customers_id']) ;

    	$body_data= array(
    		'store_details' => $store_details,
    		'voucher_details' => $this->Inventory_model->voucher_details($sell_details_id),
    		'voucher_data' => $voucher_data,
    		'customer_info' => $customer_info,
    		);
    	$this->load->view('sell/invoice', $body_data);
    	load_footer();   
    }

    /**
     * This function is used for boostrap select of items's information in database.
     * 
     *@param  string $items_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function search_item_by_name()
    {
    	$text = $this->input->post('q');
    	if($text == null || $text=="") {
    		echo "input field empty";
    	}
    	else{
    		$imei_barcode_chk['imei_barcode_yes']= $this->Inventory_model->search_by_imei_barcode($text);
    		if(empty($imei_barcode_chk['imei_barcode_yes'])){
    			$barcode_check['barcode_yes']= $this->Inventory_model->search_by_barcode($text);
    			if(empty($barcode_check['barcode_yes'])){
    				echo json_encode( $this->Inventory_model->search_item($text));
    			}
    			else{
    				echo json_encode($barcode_check);
    			}
    		}
    		else{
    			echo json_encode($imei_barcode_chk);
    		}
    	}
    }
    /**
     * This function is used for boostrap select of customer's information in database.
     * 
     *@param  string $customers_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function search_customer_by_name()
    {
    	$text = $this->input->post('q');
    	if($text == null || $text=="") {
    		echo "input field empty";
    	}
    	else{
    		echo json_encode($this->Inventory_model->search_customer($text));
    	}
    }
    /**
     * This function is used for boostrap select of sales representative's information from database database.
     * 
     *@param  string $sales_rep_name
     * @return array[] Return array if successfully data is available in database or "No result" if nothing found.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function search_sales_rep_by_name()
    {
    	$text = $this->input->post('q');
    	if($text == null || $text=="") {
    		echo "input field empty";
    	}
    	else{
    		echo json_encode( $this->Inventory_model->search_sales_rep($text));
    	}
    }
    /**
     * This function save all sell informations in database.
     *
     * @return array[] success or failure
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_sell_info()
    {
    	userActivityTracking();

    	$data = $this->input->post('all_data');

    	$error_list = array();
    	$item_array= array();

    	if(!is_numeric($data['all_basic_data']['tax_percentage']) && $data['all_basic_data']['tax_percentage'] != null) {
    		array_push($error_list, 'VAT Amount Is Not Valid');
    	}
    	if($data['all_basic_data']['tax_percentage'] < 0) {
    		array_push($error_list, 'VAT Amount Is Not Numeric');
    	}
    	if($data['all_basic_data']['discount'] != null && !is_numeric($data['all_basic_data']['discount'])) {
    		array_push($error_list, 'Discount Amount Is Not Valid');
    	}
    	if($data['all_basic_data']['discount'] < 0) {
    		array_push($error_list, 'Discount Amount Is Not Numeric');
    	}
    	if($data['all_basic_data']['paid'] < 0) {
    		array_push($error_list, 'Paid Amount Is Not Numeric');
    	}
    	if($data['all_basic_data']['paid'] ==null || !is_numeric($data['all_basic_data']['paid']) ) {
    		array_push($error_list, 'Total Paid Amount is Empty or Not Numeric');
    	}
    	$payment_type= $data['all_basic_data']['payment_type'];
    	$card_voucher_no = $data['all_basic_data']['card_payment_voucher_no'] ;

    	if ($data['all_basic_data']['paid'] == '' || $data['all_basic_data']['paid'] == null) {
    		$due_amount = $data['all_basic_data']['net_pay_after_discount'];
    	}
    	else{
    		$due_amount= $data['all_basic_data']['due_after_paid'];
    	}
    	$customer_id= $data['all_basic_data']['customers_id'];
    	if($payment_type == 'cheque'){
    		$cheque_number = $data['all_basic_data']['cheque_number'];
    		if($cheque_number == ""){
    			array_push($error_list, 'Cheque Number Empty');
    		}
    		if($this->Inventory_model->cheque_number_availability($cheque_number)){
    			array_push($error_list, 'Cheque Number Already Exist');
    		}
    	}
    	if($payment_type == 'card' && $card_voucher_no == null ) {
    		array_push($error_list, 'Card Voucher Is Empty');
    	}
    	if($payment_type == 'card' &&  !is_numeric($card_voucher_no)) {
    		array_push($error_list, 'Card Vocuher Is Not Numeric');
    	}
    	if($payment_type == 'cash_card_both' &&  !is_numeric($card_voucher_no)) {
    		array_push($error_list, 'Card Vocuher Is Not Numeric');
    	}
    	$card_amount = $data['all_basic_data']['card_amount'];
    	if($payment_type == 'cash_card_both' &&  $card_voucher_no == null ) {
    		array_push($error_list, 'Card Voucher Is Empty For Both Type Payment');
    	}
    	if($payment_type == 'cash_card_both' &&  ($card_amount == null || !is_numeric($card_amount))) {
    		array_push($error_list, 'Card Amount Is Empty For Both Type Payment');
    	}
    	$paid_amount = $data['all_basic_data']['paid'];
    	if($payment_type == 'cash_card_both' &&  $card_amount == $paid_amount) {
    		array_push($error_list, 'Please Select Card Option');
    	}
    	if($payment_type == 'cash_card_both') {
    		if($paid_amount < $card_amount){
    			array_push($error_list, 'Total Amount Can Not Less Than Card Amount');
    		}
    	}
    	$net_payable = $data['all_basic_data']['net_pay_after_discount'];
    	if($payment_type == 'cash_card_both' &&  $card_amount > $net_payable) {
    		array_push($error_list, 'Card Amount Is More Than Total Amount');
    	}
    	if($due_amount > 0 && $customer_id == null) {
    		array_push($error_list, 'Select Customer for Due Transaction');
    	}
    	$total_net_payable = $data['all_basic_data']['net_pay_after_discount'];
    	$paid = $data['all_basic_data']['paid'];
    	if ($paid == "" || $paid == null) {
    		$due = $data['all_basic_data']['net_pay_after_discount'];
    	}
    	else{
    		$due = $data['all_basic_data']['due_after_paid'];
    	}
    	if($due  == 0 && $paid == 0 && $net_payable >0){
    		array_push($error_list, 'Due And Paid Zero But Net Payable Not Zero');
    	}
    	if($due  == '' && $paid == 0 && $net_payable >0){
    		array_push($error_list, 'Due And Paid Zero But Net Payable Not Zero');
    	}
    	$discount= $data['all_basic_data']['discount'];
    	$is_percentage = $data['all_basic_data']['discount_by_percentage'];
    	$grand_total= $data['all_basic_data']['net_pay_with_vat'];
    	if($is_percentage == "yes" && $discount > 100){
    		array_push($error_list, 'Calculation Mismatch');
    	}
    	$vat_percent = $data['all_basic_data']['tax_percentage'];

    	if($discount == 0 && $vat_percent == 0){
    		if($data['all_basic_data']['net_pay_with_vat'] != $data['all_basic_data']['net_pay_after_discount']){
    			array_push($error_list, 'Grand Total and Net Payable Mismatch');
    		}
    	}

    	if($vat_percent > 0 && $discount == 0) {
    		if($data['all_basic_data']['net_pay_with_vat'] != $data['all_basic_data']['net_pay_after_discount'] ){
    			array_push($error_list, 'Grand Total After Vat and Net Payable Mismatch');
    		}
    	}

    	if($discount > 0) {
    		$is_discount = $data['all_basic_data']['discount_by_percentage'];
    		if($is_discount == "yes")
    		{
    			$dis_amt = round(($data['all_basic_data']['net_pay_with_vat']*$data['all_basic_data']['discount'])/100,2);
    			$net_payable_after_dis = $data['all_basic_data']['net_pay_with_vat'] - $dis_amt;

    			if($data['all_basic_data']['net_pay_after_discount'] != $net_payable_after_dis)
    			{
    				array_push($error_list, 'Net Payable After Discount Mismatch');
    			}
    			if($data['all_basic_data']['return_change'] == 0){
    				if(($paid + $due) != $data['all_basic_data']['net_pay_after_discount'])
    				{
    					array_push($error_list, ' Paid, Due and Net Payable Calculation Mismatch');
    				}
    			}
    		}
    	}

    	if ($paid == "" || $paid == null) {
    		$return_change = 0;
    	}
    	else{
    		$return_change=$data['all_basic_data']['return_change'];
    	}
    	if($due == 0)
    	{
    		if(($paid - $return_change) != $data['all_basic_data']['net_pay_after_discount']){
    			array_push($error_list, 'Paid, Return and Net Payable Mismatch. Please Try Again.');
    		}
    	}

    	if($due_amount > 0 && $total_net_payable !=($paid+$due)) {
    		array_push($error_list, 'Calculation Mismatch');
    	}
    	if($paid != null) {
    		if ($discount == 0 && $is_percentage=="no" && $return_change > 0 && $total_net_payable != ($paid - $return_change)) {
    			array_push($error_list, 'Total Calculation mismatch');
    		}
    		if ($discount == 0 && $is_percentage=="no" && $due > 0 && $total_net_payable != ($paid + $due)) {
    			array_push($error_list, 'Total Calculation mismatch');
    		}            
    	}
    	if ($discount > 0 && $is_percentage=="no" && $due ==0 && $total_net_payable != ($paid - $return_change)) {
    		array_push($error_list, 'Total Calculation mismatch after discount');
    	}
    	if ($discount > 0 && $is_percentage=="no" && $return_change ==0 && $total_net_payable != ($paid + $due)) {
    		array_push($error_list, 'Total Calculation mismatch after discount');
    	}

    	if($discount > 0 && $is_percentage=="no" && ($total_net_payable+$discount) !=$grand_total) {
    		array_push($error_list, 'Total Calculation mismatch in discount field');
    	}
    	if ($is_percentage=="no" && $discount > $grand_total) {
    		array_push($error_list, 'Discount is greater than grand total');
    	}
    	if ($data['all_basic_data']['payment_type'] == "card" && $data['all_basic_data']['bank_acc_id'] == "") {
    		array_push($error_list, 'Bank Acc Not Select');
    	}

    	if(count($error_list)>0) {
    		echo json_encode($error_list);
    		exit();
    	}


    	$item_validation_array = [];

    	foreach ($data['item_info'] as $key => $value) {
    		if(!isset($item_validation_array[$value['item_spec_set_id']])){
    			$item_validation_array[$value['item_spec_set_id']] =  ($value['quantity'] == null?0:$value['quantity']);
    		}
    		else{
    			$item_validation_array[$value['item_spec_set_id']]+= ($value['quantity'] == null?0:$value['quantity']);
    		}
    	}

    	foreach ($item_validation_array as $key => $value) {
    		$return_data = $this->Inventory_model->check_sell(array('item_spec_set_id'=>$key,'quantity'=>$value));

    		if($value == 0) {
    			array_push($error_list, 'PLease Enter Item Quantity');
    		}
    		if($value > $return_data['quantity']) {
    			array_push($error_list,$return_data['spec_set'].' Does not Have Enough Quantity( '.$value.' ). Available Qty: '.$return_data['quantity']);
    		}
    	}

// foreach ($data['item_info'] as $key => $value) {
//   $value['item_spec_set_id'] ;
//   $value['quantity'] ;
//   $value['selling_price'] ;
//   if(isset($item_array[$value['item_spec_set_id']])) {
//    $value['quantity']+=$item_array[$value['item_spec_set_id']];
//    $item_array[$value['item_spec_set_id']]+=$value['quantity'];
//  }
//  else
//  {
//    $item_array[$value['item_spec_set_id']] = $value['quantity'];
//  }
//  $return_data = $this->Inventory_model->check_sell($value);
//  if($value['quantity'] == null) {
//    array_push($error_list, 'PLease Enter Item Quantity');
//  }
//  if($value['quantity'] > $return_data['quantity']) {
//    array_push($error_list,$value['item_name'].' Does not Have Enough Quantity( '.$value['quantity'].' ). Available Qty: '.$return_data['quantity']);
//       // array_push($error_list, 'Item does not have enough quantity');
//  }
//  if($value['selling_price'] ==null) {
//    array_push($error_list, 'Price Not Given');
//  }
// }

    	if(count($error_list)>0) {
    		echo json_encode($error_list);
    		exit();
    	}

    	$ready_data_basic = array();
    	$ready_data_basic['customers_id'] = $data['all_basic_data']['customers_id'];
    	$ready_data_basic['sales_rep_id'] = $data['all_basic_data']['sales_rep_id'];
    	$ready_data_basic['user_id'] = $this->session->userdata('user_id');
    	$ready_data_basic['discount'] = $data['all_basic_data']['discount'];
// $ready_data_basic['delivery_charge'] = $data['all_basic_data']['delivery_charge'];

    	$ready_data_basic['discount_percentage'] = $data['all_basic_data']['discount_by_percentage'];
    	$sell_date= $data['all_basic_data']['sell_date'];
    	if($sell_date != ""){
    		$ready_data_basic['date_created'] = $data['all_basic_data']['sell_date'];
    	}
    	if($data['all_basic_data']['tax_percentage'] > 0) {
    		$vat_amount = $data['all_basic_data']['net_pay_with_vat'] - $data['all_basic_data']['net_payable'];
    		$ready_data_basic['grand_total'] = $data['all_basic_data']['net_pay_with_vat'];
    	}
    	else{
    		$vat_amount = 0;
    		$ready_data_basic['grand_total'] = $data['all_basic_data']['net_pay_with_vat'];
    	}
    	$ready_data_basic['vat_amount'] = $vat_amount;
    	$ready_data_basic['sub_total'] = $data['all_basic_data']['net_payable'];
    	$ready_data_basic['net_payable'] = $data['all_basic_data']['net_pay_after_discount'];
    	$return_change = $data['all_basic_data']['return_change'];
    	if($return_change >=0){
    		$ready_data_basic['paid'] = $data['all_basic_data']['paid']-$return_change;      
    	}
    	else{
    		$ready_data_basic['paid'] = $data['all_basic_data']['paid'];
    	}
    	$ready_data_basic['due'] = $data['all_basic_data']['due_after_paid'];
    	$ready_data_basic['tax_percentage'] = $data['all_basic_data']['tax_percentage'];
//$ready_data_basic['custom_voucher_no'] = $data['all_basic_data']['custom_voucher_no'];
    	$ready_data_basic['received_money'] = $data['all_basic_data']['received_money'];
    	$ready_data_basic['return_change'] = $data['all_basic_data']['return_change'];
    	$ready_data_basic['payment_type'] = $data['all_basic_data']['payment_type'];


    	if($ready_data_basic['payment_type'] == "card"){
    		$card_charge_percent = $data['all_basic_data']['card_charge_percent'];
    		$ready_data_basic['card_charge_amt']  = round(($ready_data_basic['paid']*$card_charge_percent)/100,2);
    	}
    	if($ready_data_basic['payment_type'] == "cash_card_both"){
    		$card_charge_parcent = $data['all_basic_data']['card_charge_percent'];
    		$paid_by_card = $data['all_basic_data']['card_amount'];
    		$ready_data_basic['card_charge_amt'] = round(($paid_by_card * $card_charge_parcent)/100,2);
    	}
    	$ready_data_basic['voucher_unique_barcode']= $this->Inventory_model->get_and_short_unique_id();
    	$this->db->trans_begin();
    	$sell_details_uuid = $this->Inventory_model->save_sell_basic_info($ready_data_basic);
    	$this->Inventory_model->save_sell_item_info($data['item_info'], $sell_details_uuid, $sell_date);
    	$this->Inventory_model->save_sell_item_info_to_inventory($data['item_info']);
    	/*Update imei or serial status*/
    	if(array_key_exists('imei_serial_data', $data)){
    		foreach ($data['imei_serial_data'] as $key => $value) {
    			$imei_code = $value['imei_barcode'];
    			$barcode_data=array(
    				'sell_details_id' => $sell_details_uuid,
    				'barcode_status' => 'used',
    				);
    			$this->Inventory_model->update_barcode_status($imei_code,$barcode_data);
    		}
    	}
    	/* Safety Stock Check Starts */
    	foreach ($data['item_info'] as $key => $value) {
    		$item_id= $value['item_spec_set_id'];
    		$last_thirty_days_total_sell= $this->Inventory_model->thirty_days_sell_qty($item_id);
    		$one_week_avg_sell= floor(($last_thirty_days_total_sell['quantity'])/30);
    		$last_thirty_days_individual_sell_qty = $this->Inventory_model->individual_sell_qty($item_id);
    		$sum_of_square_of_diff= 0;
    		foreach ($last_thirty_days_individual_sell_qty as $key2 => $value2) {
    			$sell_quantity = $value2['quantity'];
    			$diff_from_avg_sell_qty = ($one_week_avg_sell - $sell_quantity)*($one_week_avg_sell - $sell_quantity);
    			$sum_of_square_of_diff+=$diff_from_avg_sell_qty;
    		}
    		$avg_of_square = floor($sum_of_square_of_diff/30);
    		$sqr_root_of_avg = floor(sqrt($avg_of_square));
    		$number_of_units_to_stack =  floor($one_week_avg_sell + ($sqr_root_of_avg * 1.65));
    		$approx_stock_for_a_week = $number_of_units_to_stack*7;
    		$current_inventory_qty = $this->Inventory_model->items_info_from_inventory($item_id);
    		if ($current_inventory_qty['quantity'] > ($approx_stock_for_a_week*4) ) {
    			$status_data['inventory_status']="Excess";
    			$this->Inventory_model->update_inventory_status($item_id, $status_data);
    		}
    		if ($current_inventory_qty['quantity'] <= $approx_stock_for_a_week) {
    			$status_data['inventory_status']="Low";
    			$this->Inventory_model->update_inventory_status($item_id, $status_data);
    		}
    		if ($current_inventory_qty['quantity'] <= ($approx_stock_for_a_week*4) && $current_inventory_qty['quantity'] > $approx_stock_for_a_week ) {
    			$status_data['inventory_status']="Normal";
    			$this->Inventory_model->update_inventory_status($item_id, $status_data);
    		}
    	}
    	/* Safety Stock Check Ends*/
    	if($payment_type == 'cash') {
    		$payment_method_details =array(
    			'sell_details_id' => $sell_details_uuid ,
    			'payment_type' => $data['all_basic_data']['payment_type'],
    			'amount' => $data['all_basic_data']['paid'],
    			'user_id' => $this->session->userdata('user_id'),
    			);    
    		$this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);
    		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    		$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $ready_data_basic['paid'];
    		$this->Inventory_model->update_total_cashbox_info($final_data);
    		$cash_data['cash_type']= "sell";
    		$cash_data['user_id'] = $ready_data_basic['user_id'];
    		$cash_data['deposit_withdrawal_amount'] = $ready_data_basic['paid'] ;
    		$cash_data['buy_or_sell_details_id']= $sell_details_uuid;
    		if($sell_date != ""){
    			$cash_data['date_created'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_cash_box_info($cash_data);
    	}
    	if($payment_type == 'card'){

    		$charge_percent = $data['all_basic_data']['card_charge_percent'];
    		$bank_acc_id= $data['all_basic_data']['bank_acc_id'];
    		$curr_bank_account = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
    		$amt_after_charge =round($ready_data_basic['paid']- (($ready_data_basic['paid']* $charge_percent)/100),2);
    		$bank_data= array(
    			'buy_sell_and_other_id' => $sell_details_uuid ,
    			'cash_type' => 'sell',
    			'deposit_withdrawal_amount' => $amt_after_charge,
    			'bank_acc_id' => $bank_acc_id,
    			'user_id' => $this->session->userdata('user_id'),
    			);
    		if($sell_date != ""){
    			$bank_data['date_created'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_card_payment_info($bank_data);
    		$final_amount['final_amount']=$curr_bank_account['final_amount']+$amt_after_charge;
    		$this->Inventory_model->update_total_amt_info_of_acc($final_amount,$bank_acc_id);
    		$payment_method_details =array(
    			'sell_details_id' => $sell_details_uuid ,
    			'payment_type' => $data['all_basic_data']['payment_type'],
    			'amount' => $data['all_basic_data']['paid'],
    			'card_voucher_or_actual_voucher_number' => $data['all_basic_data']['card_payment_voucher_no'],
    			'user_id' => $this->session->userdata('user_id'),
    			'card_charge_percent' => $charge_percent,
    			);
    		if($sell_date != ""){
    			$payment_method_details['date_creted'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);  
    	}
    	if ($payment_type == 'cheque') {
    		$cheque_data=array(
    			'sell_details_id' => $sell_details_uuid,
    			'cheque_number'  => $data['all_basic_data']['cheque_number'],
    			'user_id' => $this->session->userdata('user_id'), 
    			'cheque_amount' => $ready_data_basic['paid'],
  // 'card_charge_percent' => $charge_percent,
    			);
    		if($sell_date != ""){
    			$cheque_data['date_created'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Bank_model->save_pending_cheque_details_info($cheque_data);
    		$payment_method_details =array(
    			'sell_details_id' => $sell_details_uuid ,
    			'payment_type' => $data['all_basic_data']['payment_type'],
    			'amount' => $ready_data_basic['paid'],
    			'card_voucher_or_actual_voucher_number' => $data['all_basic_data']['cheque_number'],
    			'user_id' => $this->session->userdata('user_id'),
    			);
    		if($sell_date != ""){
    			$payment_method_details['date_creted'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_cash_or_card_payment_info($payment_method_details);  
    	}
    	if($payment_type == 'cash_card_both') {
    		$charge_percent = $data['all_basic_data']['card_charge_percent'];

    		$pay_through_cash = $ready_data_basic['paid'] - $data['all_basic_data']['card_amount'];
    		$cashbox_data=array(
    			'user_id' => $this->session->userdata('user_id'),
    			'deposit_withdrawal_amount' => $pay_through_cash,
    			'cash_type' => 'sell',
    			'buy_or_sell_details_id' => $sell_details_uuid,
    			);
    		if($sell_date != ""){
    			$cashbox_data['date_created'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_cash_box_info($cashbox_data);
    		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    		$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $pay_through_cash;
    		$this->Inventory_model->update_total_cashbox_info($final_data);
    		$bank_acc_id= $data['all_basic_data']['bank_acc_id'];
    		$curr_bank_account = $this->Inventory_model->get_current_bank_amt_info($bank_acc_id); 
    		$paid_by_card = $data['all_basic_data']['card_amount'];
    		$amt_after_charge =round($paid_by_card- (($paid_by_card * $charge_percent)/100),2);
    		$bank_data= array(
    			'buy_sell_and_other_id' => $sell_details_uuid ,
    			'cash_type' => 'sell',
    			'deposit_withdrawal_amount' => $amt_after_charge,
    			'bank_acc_id' => $bank_acc_id,
    			'user_id' => $this->session->userdata('user_id'),
    			);
    		if($sell_date != ""){
    			$bank_data['date_created'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_card_payment_info($bank_data);
    		$final_amount['final_amount']=$curr_bank_account['final_amount']+ $amt_after_charge;
    		$this->Inventory_model->update_total_amt_info_of_acc($final_amount,$bank_acc_id);
    		$payment_method_details =array(
    			'sell_details_id' => $sell_details_uuid ,
    			'payment_type' => $data['all_basic_data']['payment_type'],
    			'amount' => $ready_data_basic['paid'],
    			'card_voucher_or_actual_voucher_number' => $data['all_basic_data']['card_payment_voucher_no'],
    			'user_id' => $this->session->userdata('user_id'),
    			'card_charge_percent' => $charge_percent,
    			);
    		if($sell_date != ""){
    			$payment_method_details['date_creted'] = $data['all_basic_data']['sell_date'];
    		}
    		$this->Inventory_model->save_cash_or_card_payment_info($payment_method_details); 
    	}

    	$voucher_data =array();
    	$customers_id= $data['all_basic_data']['customers_id'];
    	if($customers_id != null){
    		$voucher_data['customer_info']= $this->Inventory_model->get_customer_info_for_voucher($customers_id) ;
    	}
    	$voucher_data['voucher_number']= $this->Inventory_model->collect_voucher_number($sell_details_uuid);
    	if ($this->db->trans_status() === false) {
    		$this->db->trans_rollback();
    		array_push($error_list, 'Transaction Error.Refresh The Page and Try Again.');
    		echo json_encode($error_list);
    	}
    	else
    	{
    		$this->db->trans_commit();
    		echo json_encode(array("success ".$sell_details_uuid,$voucher_data));
    	}
    }

    public function normal_print($sell_details_uuid, $print_type)
    {
    	$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_uuid);
    	$body_data= array(
    		'all_sell_info_by_id' => $this->Inventory_model->all_sell_info_by_id($sell_details_uuid),
    		'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
    		'store_details' => $this->Inventory_model->store_details(),
    		'print_type' => $print_type,
    		);
    	$no_of_items = count($body_data['all_sell_info_by_id']);

//   Without Custom Header & Footer
    	$viewFinalData = array();
    	$already_echo_counter = 0;

    	if($no_of_items <= 10){
    		$viewData = array(
    			"pageType" => "page1",
    			"pageData" => array_slice($body_data['all_sell_info_by_id'], 0, 10)
    			);
    		array_push($viewFinalData, $viewData);
    	}

    	if($no_of_items > 10) {
    		$viewData = array(
    			"pageType" => "page2",
    			"pageData" => array_slice($body_data['all_sell_info_by_id'], 0, 15)
    			);
    		array_push($viewFinalData, $viewData);
    		$already_echo_counter+=15;
    		$no_of_items = $no_of_items - 15;
    		if($no_of_items == 0 || $no_of_items <= 13){
    			$viewData = array(
    				"pageType" => "page4",
    				"pageData" => $no_of_items == 0 ? array() : array_slice($body_data['all_sell_info_by_id'], 15, 13)
    				);
    			array_push($viewFinalData, $viewData);
    		}else{

    			while ($no_of_items > 13) { 

    				$viewData = array(
    					"pageType" => "page3",
    					"pageData" => array_slice($body_data['all_sell_info_by_id'], $already_echo_counter, 20)
    					);
    				array_push($viewFinalData, $viewData);
    				$already_echo_counter+= 20;
    				$no_of_items = $no_of_items - 20;
    				if($no_of_items == 0 || $no_of_items <= 13){
    					$viewData = array(
    						"pageType" => "page4",
    						"pageData" => $no_of_items == 0 ? array() : array_slice($body_data['all_sell_info_by_id'], $already_echo_counter, 13)
    						);
    					array_push($viewFinalData, $viewData);
    				}
    			}
    		}
    	}

    	$body_data['viewFinalData']= $viewFinalData;
// With Header & Footer
    	$this->load->view('sell/new_invoice_with_header_footer', $body_data);

    }

    public function without_header_normal_print($sell_details_uuid, $print_type)
    {

    	$voucher_details = $this->Inventory_model->sell_details_by_id($sell_details_uuid);
    	$body_data= array(
    		'all_sell_info_by_id' => $this->Inventory_model->all_sell_info_by_id($sell_details_uuid),
    		'voucher_number' => $this->Inventory_model->collect_voucher_number($voucher_details['sell_details_id']),
    		'store_details' => $this->Inventory_model->store_details(),
    		'print_type' => $print_type,
    		);

    	$no_of_items = count($body_data['all_sell_info_by_id']);

//   With Custom Header & Footer
    	$viewFinalData = array();
    	$already_echo_counter = 0;

    	if($no_of_items <= 7){
    		$viewData = array(
    			"pageType" => "page1",
    			"pageData" => array_slice($body_data['all_sell_info_by_id'], 0, 7)
    			);
    		array_push($viewFinalData, $viewData);
    	}

    	if($no_of_items > 7) {
    		$viewData = array(
    			"pageType" => "page2",
    			"pageData" => array_slice($body_data['all_sell_info_by_id'], 0, 13)
    			);
    		array_push($viewFinalData, $viewData);
    		$already_echo_counter+=13;
    		$no_of_items = $no_of_items - 13;
    		if($no_of_items == 0 || $no_of_items <= 10){
    			$viewData = array(
    				"pageType" => "page4",
    				"pageData" => $no_of_items == 0 ? array() : array_slice($body_data['all_sell_info_by_id'], 13, 10)
    				);
    			array_push($viewFinalData, $viewData);
    		}else{

    			while ($no_of_items > 10) { 

    				$viewData = array(
    					"pageType" => "page3",
    					"pageData" => array_slice($body_data['all_sell_info_by_id'], $already_echo_counter, 16)
    					);
    				array_push($viewFinalData, $viewData);
    				$already_echo_counter+= 16;
    				$no_of_items = $no_of_items - 16;
    				if($no_of_items == 0 || $no_of_items <= 10){
    					$viewData = array(
    						"pageType" => "page4",
    						"pageData" => $no_of_items == 0 ? array() : array_slice($body_data['all_sell_info_by_id'], $already_echo_counter, 10)
    						);
    					array_push($viewFinalData, $viewData);
    				}
    			}
    		}
    	}

    	$body_data['viewFinalData']= $viewFinalData;
    	$this->load->view('sell/new_invoice_without_header_footer', $body_data);
    }
    /**
     * This function collect retail and wholesale price  of individual item by items_id.
     * 
     * @param string $items_id
     *
     * @return array[] ratail and wholesale price
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_item_price_info($items_id)
    {
    	$final_data = $this->Inventory_model->select_item_price_info($items_id);
    	echo json_encode($final_data);
    }
    /**
     * Collect current vat percentage from vat table
     *
     * @return void
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function get_current_vat_percentage()
    {
    	$final_data = $this->Inventory_model->select_current_vat_percentage_info();
    	if($final_data == null) {
    		$final_data=array(
    			'vat_percentage' => 0,
    			);
    	}
    	echo json_encode($final_data);
    }
    /**
     * This function collect retail and wholesale price  of individual item by items_id.
     *
     * @param  string $items_id
     * @return array[] items current quantity
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function quantity_by_item_from_inventory($data)
    {
    	$final_data = array();
    	$final_data['final_quantiy'] = $this->Inventory_model->quantity_info_by_item_from_inventory($data);
    	$final_data['buying_price'] = $this->Inventory_model->get_item_buying_price($data);
    	$final_data['available_imei']= $this->Inventory_model->get_item_imei_list($data);
    	$final_data['avg_buy_price']= $this->Inventory_model->get_avg_item_buy_price($data);
    	echo json_encode($final_data);
    }
    /**
     * Collect Individual Item's information form Database for edit sell informations.
     * @param  string $id
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function get_item_info_for_sell($id)
    {
    	echo json_encode($this->Inventory_model->select_item_info_for_sell($id));            
    }
/**
 * Test function for viewing print preview
 *
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function print_preview()
{
	load_header();
	$this->load->view('sell/general_invoice');
	load_footer();
}


public function multi_barcode()
{
	load_header();
	$this->load->view('barcode/multi_barcode_page');
	load_footer();
}

public function search_item_by_name_for_barcode()
{
	$text = $this->input->post('q');
	if($text == null || $text=="") {
		echo "input field empty";
	}
	else{
		$barcode_check['barcode_yes']= $this->Inventory_model->search_by_barcode($text);
		if(empty($barcode_check['barcode_yes'])){
			echo json_encode( $this->Inventory_model->search_item_for_barcode_print($text));
		}
		else{
			echo json_encode($barcode_check);
		}
	}
}

public function get_item_barcode($items_id)
{
	echo json_encode($this->Inventory_model->get_item_barcode_info($items_id));
}

public function update_sell_voucher_info()
{
	$form_data = $this->input->post();
	if(empty($form_data['sell_details_id'])){
		echo json_encode("sell_details_id not found");
		exit();
	}
	if(empty($form_data['customers_id'])){
		echo json_encode("customers_id not found");
		exit();
	}
	if(empty($form_data['customers_name'])){
		echo json_encode("customers_name not found");
		exit();
	}
	else{
		$update_data = array(
			'user_id' => $this->session->userdata('user_id'),
			'customers_id' => $form_data['customers_id'],
			);
		$sell_details_id = $form_data['sell_details_id'];
		if($this->Inventory_model->update_sell_voucher_info_by_id($sell_details_id, $update_data)){
			echo json_encode("success");
		}
		else{
			echo json_encode("failed");
		}
	}
}
}
/* End of file Sell.php */
/* Location: ./application/controllers/Sell.php */