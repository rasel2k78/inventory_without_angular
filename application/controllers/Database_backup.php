<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database_backup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{

		$this->load->dbutil();

		$prefs = array(     
			'format'      => '',             
			'filename'    => 'my_db_backup.sql'
			);


		$backup =& $this->dbutil->backup($prefs); 

		$db_name = 'pos2in_backup_on_'. date("Y-m-d-H-i-s") .'';
		$save = 'pathtobkfolder/'.$db_name;

		$this->load->helper('file');
		write_file($save, $backup); 


		$this->load->helper('download');
		force_download($db_name, $backup); 

	}

}

/* End of file  */
/* Location: ./application/controllers/ */