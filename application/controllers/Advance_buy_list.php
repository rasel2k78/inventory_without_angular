<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance_buy_list extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Advance_buy_model');

		$this->load->model('Inventory_model');
		$this->load->model('Bank_model');
		$this->load->model('User_access_check_model');
		$cookie = $this->input->cookie('language', true);
		$this->lang->load('left_side_nev_lang', $cookie);
		$this->lang->load('buy_all_list_lang', $cookie);
		$this->lang->load('buy_page_lang', $cookie);
		$this->lang->load('add_item_form_lang', $cookie);
		$this->lang->load('add_category_form_lang', $cookie);
		$this->lang->load('spec_name_page_lang', $cookie);
		$this->lang->load('add_vendor_form_lang', $cookie);


		$user_id = $this->session->userdata('user_id');
		if ($user_id == null) {

			redirect('Login', 'refresh');
		}
	}

	public function index()
	{
		$store_details = $this->Inventory_model->store_details();
		$body_data= array(
			'store_details' => $store_details,
			);
		load_header();
		$this->load->view('advance_buy/advance_buy_list', $body_data);
		load_footer();
	}

	public function all_adv_buy_info_for_datatable()
	{
		$filters = $this->input->get();

		$all_data = $this->Advance_buy_model->all_adv_buy_info_list($filters);
		$all_data_without_limit = $this->Advance_buy_model->all_adv_buy_info_list($filters, true);
		$all_data_final = $this->Advance_buy_model->all_adv_buy_info_list($filters, true);

		$output_data=[];

		$output_data["draw"]=$filters['draw'];
		$output_data["recordsTotal"]=$all_data_without_limit;
		$output_data["recordsFiltered"]=$all_data_final;
		$output_data["data"]=$all_data;

		echo json_encode($output_data);
	}

	public function get_adv_buy_info($buy_details_id)
	{
		$voucher_details = $this->Advance_buy_model->adv_buy_details_by_id($buy_details_id);
		$body_data= array(
			'all_buy_info_by_id' => $this->Advance_buy_model->details_adv_buy_info_by_id($buy_details_id),
			'vendor_info' => $this->Inventory_model->get_vendor_info_for_voucher($voucher_details['vendors_id']),
			'voucher_number' => $this->Advance_buy_model->collect_adv_voucher_number_buy($voucher_details['adv_buy_details_id']),
			);
		echo json_encode($body_data);
	}

	public function complete_adv_buy()
	{
		$data=  $this->input->post();
		// echo "<pre>";
		// print_r($data);
		// exit();
		$store_details = $this->Inventory_model->store_details();
		$data['store_details'] = $store_details;
		$data['cashbox_amount'] =  $this->Inventory_model->final_cashbox_amount_info();
		load_header();
		$this->load->view('advance_buy/complete_adv_buy', $data, FALSE);
		load_footer();
	}

	public function update_adv_buy_info()
	{
		$data= $this->input->post();
		// echo "<pre>";
		// print_r($data);
		// exit();
		$adv_buy_details_id = $data['adv_buy_details_id'];
		$this->db->trans_begin();

		$get_previous_info = $this->Advance_buy_model->adv_buy_details_by_id($adv_buy_details_id);

		$diff_amt = $data['paid'] - $get_previous_info['paid'];
		if($diff_amt >= 0){
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $diff_amt;
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$cash_data['cash_type']= "advance_buy";
			$cash_data['user_id'] = $this->session->userdata('user_id');
			$cash_data['deposit_withdrawal_amount'] = $diff_amt;
			$cash_data['buy_or_sell_details_id']= $adv_buy_details_id;
			$this->Inventory_model->save_cash_box_info($cash_data);
		}
		if($diff_amt < 0){
			$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - ($diff_amt);
			$this->Inventory_model->update_total_cashbox_info($final_data);
			$cash_data['cash_type']= "buy_advance_adjust";
			$cash_data['user_id'] = $this->session->userdata('user_id');
			$cash_data['deposit_withdrawal_amount'] = (-1)*$diff_amt;
			$cash_data['buy_or_sell_details_id']= $adv_buy_details_id;
			$this->Inventory_model->save_cash_box_info($cash_data);
		}
		$update_data = array(
			'paid' => $data['paid'],
			'due' => $get_previous_info['net_payable'] - $data['paid'],
			'receivable_date' =>$data['receivable_date'],
			);
		$this->Inventory_model->update_adv_buy_info_by_id($adv_buy_details_id,$update_data);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			echo json_encode("error transaction");
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode("success");
		}
	}

	public function delete_adv_buy($adv_buy_details_id)
	{
		$this->db->trans_begin();

		$get_invoice_details = $this->Advance_buy_model->adv_buy_details_by_id($adv_buy_details_id);

		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
		$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $get_invoice_details['paid'];
		$this->Inventory_model->update_total_cashbox_info($final_data);
		$cash_data['cash_type']= "buy_advance_adjust";
		$cash_data['user_id'] = $this->session->userdata('user_id');
		$cash_data['deposit_withdrawal_amount'] = $get_invoice_details['paid'];
		$cash_data['buy_or_sell_details_id']= $adv_buy_details_id;
		$this->Inventory_model->save_cash_box_info($cash_data);
		$this->Inventory_model->delete_buy_adv_invoice($adv_buy_details_id);
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			echo json_encode("error transaction");
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode("success");
		}
	}

}

/* End of file Advance_buy_list.php */
/* Location: ./application/controllers/Advance_buy_list.php */