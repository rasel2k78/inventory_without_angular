<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
class Sync extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('Sync_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('left_side_nev_lang', $cookie);       
    }
    /**
         * This method is for fetch the sync data from Syncmodel and call the function syncTest and if it returns true 
         * then the local sync table will synced for particular rows.
         * 
         * @return null
         * @author Rasel 
         **/
    public function index()
    {
        session_write_close();
        $user_id_logged = $this->session->userdata('user_id');

        $i = 0;
        $ulive ='';
        $getData = $this->Sync_model->getUnsynced();
        $user_account_info = $this->Sync_model->get_user_by_id($user_id_logged);
        $user_account_folder = $this->Sync_model->get_folder_name();
        if(!empty($getData)) {
            foreach ($getData as $key=>$value)
            {       
                $value['data'] = $this->Sync_model->getTableRow($value['table_name'], $value['pkey_column_name'], $value['row_id']);
                if(!empty($value['data'])) {
                    $getData[$key] = $value;
                }
                else
                {

                 $this->db->where('row_id', $value['row_id']);
                 $this->db->delete('sync');
                 unset($getData[$key]);
             }
         }

         $this->load->library('curl');
         $url = "https://users.pos2in.com/".$user_account_folder['folder_name']."/index.php/sync";
         $auth_data = array();
         $owner_info = $this->Sync_model->get_owner();
         $auth_data['email'] = $owner_info['email'];
         $auth_data['stores_id'] = $owner_info['stores_id'];      
         $headers = [
            'Authorization:'.json_encode($auth_data),
            'Content-Type: application/x-www-form-urlencoded'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data['all_data']=json_encode($getData);
        $params = http_build_query($data);                      
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
                       // var_dump( curl_getinfo($ch));
                       // echo curl_strerror(curl_errno($ch)) . '<br/>';
                       // echo curl_error($ch) . '<br/>';
                       //var_dump($output);
                       //exit();
        if($output=="Invalid user") {
            jsonify(
                array(
                    "success" => false,
                    "type" => 'invalid_user',
                    "message" => "invalid user"
                )
            );
            curl_close($ch);
            exit;
        }
        else if(!$output) {
            jsonify(
                array(
                    "success" => false,
                    "type" => 'CURL FAILED',
                    "message" => curl_strerror(curl_errno($ch)) 
                )
            );
            curl_close($ch);
            exit;
        }
        foreach (unserialize($output) as $key => $value) {
            if($value['success']==1) {
                $dataa['status'] = "synced";
                $update_sync_table =$this->Sync_model->update_synch_table($value['id'],$dataa);
            }                       
        }
        jsonify(
            array(
                "success" => true,
                "type" => 'sync_success',
                "message" =>"information synced successfully"
            )
        );
    }
    else
    {
        jsonify(
            array(
                "success" => false,
                "type" => 'no_sync_data_available',
                "message" => "Nothing available to sync!"
            )
        );
    }

}


public function syncCount()
{
    echo json_encode($this->Sync_model->syncCount());
}

public function silent_sync()
{
   echo json_encode(array('success' => true ));
}

}
/* End of file Sync.php */
/* Location: ./application/controllers/Sync.php */