<?php 
/**
 * Damage and Lost Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Demage Lost Controller
 *
 * @return void
      * @author shoaib <shofik.shoaib@gmail.com>
 **/
class Demage_and_lost extends CI_Controller
{
    /**
 * Load language files ,models 
 *
 * @return void
     * @author shoaib <shofik.shoaib@gmail.com> 
 **/
    public function __construct()
    {
       parent::__construct();

       $this->load->model('Inventory_model');
       $cookie = $this->input->cookie('language', true);
       $this->lang->load('left_side_nev_lang', $cookie);
       $this->lang->load('demage_lost_lang', $cookie);
       $this->load->model('User_access_check_model');
       userActivityTracking();
       
       $user_id = $this->session->userdata('user_id');
       if ($user_id == null) {
        redirect('Login', 'refresh');
    }
}
    /**
     * Load demage lost form 
     *
     * @return void
    * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $this->load->view('demage_lost/add_demage_lost_form');
        }
        else{
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();
    }
    /**
     * Save Demage Lost Informations to database 
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function save_demage_lost()
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $output=array();
            $output['success'] = 1;
            $output['error'] = array();
            if($this->input->post('item_spec_set_id', true)==null || $this->input->post('item_spec_set_id', true)=="") {
                $output['error']['item_spec_set_id'] = $this->lang->line('left_valid_item');
                $output['success'] = 0;
            }
            if($this->input->post('demage_lost_quantity', true)==null || !is_numeric($this->input->post('demage_lost_quantity', true)) || $this->input->post('demage_lost_quantity', true) == 0) {
                $output['error']['demage_lost_quantity'] =  $this->lang->line('left_valid_quantity');
                $output['success'] = 0;
            }
            if($this->input->post('item_buying_price', true)==null) {
                $output['error']['item_buying_price'] =  $this->lang->line('left_valid_buying_price');
                $output['success'] = 0;
            }
            $selected_item_id= $this->input->post('item_spec_set_id', true);
            $demage_lost_quantity= $this->input->post('demage_lost_quantity', true);
            $current_inventory_quantity= $this->Inventory_model->items_info_from_inventory($selected_item_id);
            if($demage_lost_quantity >$current_inventory_quantity['quantity'] ) {
                $output['error']['demage_lost_quantity'] =  $this->lang->line('left_valid_more_quantity');
                $output['success'] = 0;
            }
            $is_imei = $this->Inventory_model->is_imei_available_info($selected_item_id);
            if($is_imei['unique_barcode'] == 'yes'){

                
                $demaged_imei = $this->input->post('demaged_imei', TRUE);
                $seleted_imei_info= $this->Inventory_model->get_imei_info($demaged_imei);
                if($seleted_imei_info == ""){
                    $output['error']['demaged_imei'] =  $this->lang->line('wrong_imei');
                    $output['success'] = 0;
                }
                if($seleted_imei_info['barcode_status']== "used" || $seleted_imei_info['publication_status']== "deactivated" || $seleted_imei_info['barcode_status']== "demaged"){
                    $output['error']['demaged_imei'] =  $this->lang->line('imei_used');
                    $output['success'] = 0;
                }
                if($demaged_imei != "" && $demage_lost_quantity > 1){
                    $output['error']['demage_lost_quantity'] =  $this->lang->line('one_qty_for_imei');
                    $output['success'] = 0;
                }
            }

            if($output['success']==0) {
                echo json_encode($output);
                exit();
            }
            else{
                $data= array(
                   'demage_lost_quantity' =>$this->input->post('demage_lost_quantity', true),
                   'demage_lost_type' => $this->input->post('demage_lost_type', true),
                   'demage_lost_description' => $this->input->post('demage_lost_description', true),
                   'item_spec_set_id' => $this->input->post('item_spec_set_id', true),
                   'item_buying_price' => $this->input->post('item_buying_price', true),
                   'sub_total' => ($this->input->post('demage_lost_quantity', true) * $this->input->post('item_buying_price', true)),
                   'user_id' => $this->session->userdata('user_id'),
                   );
                $this->db->trans_begin();

                $demaged_imei = $this->input->post('demaged_imei', TRUE);
                if($demaged_imei != ""){
                    $imei_barcode_id= $this->Inventory_model->get_imei_barcode_id($demaged_imei);
                    $data['imei_barcode_id']= $imei_barcode_id;

                    $barcode_data = array(
                        'barcode_status' => "demaged",
                        );
                    $this->Inventory_model->update_barcode_status($demaged_imei,$barcode_data);
                }

                $item_id = $data['item_spec_set_id'];
                $current_inventory_quantity = $this->Inventory_model->items_info_from_inventory($item_id);
                $updated_data = array(
                   'quantity' => $current_inventory_quantity['quantity'] - $data['demage_lost_quantity'],
                   );
                $this->Inventory_model->update_inventory_quantity($item_id, $updated_data);
                $output['data'] = $this->Inventory_model->save_demage_lost_info($data);
                echo json_encode($output);
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                }
                else
                {
                    $this->db->trans_commit();
                }
            }
        }
        else {
            echo json_encode(array('permission' =>'no'));
        }
    }
    /**
     * Coolect all damage lost informations for ajax datatable
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_demage_lost_info_for_datatable()
    {
        $filters = $this->input->get();
        $all_data = $this->Inventory_model->all_demage_lost_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_demage_lost_info($filters, true);
        $all_data_final = $this->Inventory_model->all_demage_lost_info($filters, true);
        $output_data=[];
        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;
        echo json_encode($output_data);
    }
    /**
     * This function will delete/hide  the sell's information by "sell_details_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $sell_details_id
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function delete_demage_lost($demage_lost_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $items_id_and_qty= $this->Inventory_model->items_info_by_demage_lost_id($demage_lost_id);
            if($items_id_and_qty['imei_barcode_id'] != ""){
                $imei_barcode_id = $items_id_and_qty['imei_barcode_id'];
                $barcode_data = array(
                    'barcode_status' => "not_used",
                    );
                $this->Inventory_model->update_barcode_status_after_delete_damage($imei_barcode_id,$barcode_data);
            }
            $items_id = $items_id_and_qty['item_spec_set_id'];
            $current_inventory_quantity = $this->Inventory_model->items_info_from_inventory($items_id_and_qty['item_spec_set_id']);
            $data['quantity']= $current_inventory_quantity['quantity'] + $items_id_and_qty['demage_lost_quantity'];
            $abc = $this->Inventory_model->update_inventory_after_demage_lost_delete($items_id, $data);
            if ($abc > 0) {
                if($this->Inventory_model->delete_demage_lost_info($demage_lost_id)) {
                    echo "success";    
                }    
                else{
                    echo "failed";
                }    
            }
        }
        else {
            echo "No Permisssion";
        }
    }
    /**
 * Coolect individual demage lost informations form database
 *
 * @param  string $demage_lost_id
 * @return array[]
      * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function get_demage_lost_info($demage_lost_id)
    {
       $user_id_logged = $this->session->userdata('user_id');
       $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
       if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $body_data= array(
           'all_demage_lost_by_id' => $this->Inventory_model->all_demage_lost_info_by_id($demage_lost_id),
           );
        echo json_encode($body_data);
    }
    else {
        echo json_encode(array('permission' =>'no'));
    }
}
    /**
 * Update Demage Lost informations after edit
 *
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function update_demage_lost_info()
    {
        // echo "<pre>";
        // print_r($this->input->post());
        // exit();
       $output = array();
       $output['success'] = 1;
       $output['error']= array();
       if($this->input->post('item_spec_set_id_edit', true) ==null ) {
        $output['error']['item_spec_set_id_edit']= $this->lang->line('left_valid_item');
        $output['success']= 0;

    }
    $selected_item_id= $this->input->post('item_spec_set_id_edit', true);
    $demage_lost_quantity= $this->input->post('demage_lost_quantity_edit', true);
    $current_inventory_quantity= $this->Inventory_model->items_info_from_inventory($selected_item_id);
    if($demage_lost_quantity >$current_inventory_quantity['quantity'] ) {
        $output['error']['demage_lost_quantity_edit'] =  $this->lang->line('left_valid_more_quantity');
        $output['success'] = 0;
    }
    if($this->input->post('demage_lost_quantity_edit', true) ==null || !is_numeric($this->input->post('demage_lost_quantity_edit', true)) ) {
        $output['error']['demage_lost_quantity_edit']= $this->lang->line('left_valid_quantity');
        $output['success']= 0;

    }
    if($this->input->post('item_buying_price_edit', true)==null) {
        $output['error']['item_buying_price_edit'] =  $this->lang->line('left_valid_buying_price');
        $output['success'] = 0;
    }
    if($output['success'] == 1) {
        $demage_lost_id = $this->input->post('demage_lost_id', true);
        $data=array(
           'item_spec_set_id' => $this->input->post('item_spec_set_id_edit', true),
           'demage_lost_quantity' => $this->input->post('demage_lost_quantity_edit', true),
           'demage_lost_description' => $this->input->post('demage_lost_description_edit', true),
           'item_buying_price' => $this->input->post('item_buying_price_edit', true),
           'demage_lost_type' => $this->input->post('demage_lost_type_edit', true),
           'sub_total' => ($this->input->post('demage_lost_quantity_edit', true) * $this->input->post('item_buying_price_edit', true)),
           'user_id' => $this->session->userdata('user_id'),
           );
        $item_id = $data['item_spec_set_id'];
        $current_inventory_quantity = $this->Inventory_model->items_info_from_inventory($item_id);
        $previous_demage_lost_quanity= $this ->Inventory_model ->previous_demage_lost_quanity_info($demage_lost_id);
        $difference_demage_lost_amount = $data['demage_lost_quantity'] - $previous_demage_lost_quanity['demage_lost_quantity'];
        if($difference_demage_lost_amount <= 0) {
         $updated_data = array(
            'quantity' => $current_inventory_quantity['quantity'] - $difference_demage_lost_amount,
            );
     }
     if($difference_demage_lost_amount > 0) {
        $updated_data = array(
           'quantity' => $current_inventory_quantity['quantity'] - $difference_demage_lost_amount,
           );
    }
    $this->Inventory_model->update_inventory_quantity($item_id, $updated_data);
    if($this->Inventory_model->update_demage_lost_info($demage_lost_id, $data)) {
        echo json_encode($output);
    }
    else{
        $output['success']  = 0;
        echo json_encode($output);
    }
}
else{
    echo json_encode($output);
}
}


}

/* End of file Demage_and_lost.php */
/* Location: ./application/controllers/Demage_and_lost.php */