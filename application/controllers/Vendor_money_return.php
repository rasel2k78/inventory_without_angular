<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Vendor_money_return extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Exchange_with_vendor_model');
        $this->load->model('Exchange_with_customer_model');
        $this->load->model('Inventory_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('left_side_nev_lang', $cookie);
        $this->lang->load('vendor_money_return_lang', $cookie);
        userActivityTracking();

        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }
        
    }

    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
               'cashbox_amount' => $this->Inventory_model->final_cashbox_amount_info(),
               );
            $this->load->view('Return_money/Vendor_money_return', $body_data);
        }
        else
        {
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();
    }


    public function get_voucher_details_info()
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
        // {

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('buy_voucher_no', true)==null) {
            $output['error']['buy_voucher_no'] = "Voucher number দিন";
            $output['success'] = 0;
        }

        if($output['success']==0) {
            echo json_encode($output);
            exit();
        }
        else
        {
            $voucher_no = $this->input->post('buy_voucher_no');

            $body_data= array(
               'all_sell_info_by_id' => $this->Exchange_with_vendor_model->get_voucher_info($voucher_no),
               );
            echo json_encode($body_data);
        }
        // }
        // else
        // {
        // 	echo json_encode("No permission");
        // }
    }
    
    public function check_item_quantity()
    {
        $item_spec_set_id = $this->input->post('item_spec_set_id');
        $check_item_quantity_inventory = $this->Exchange_with_vendor_model->get_item_quantity_inventory($item_spec_set_id);
        echo $check_item_quantity_inventory['quantity'];exit;
    }


    public function save_sell_info() 
    {
        $ready_data_basic['user_id'] = $this->session->userdata('user_id');
        $voucher_id = $this->input->post('voucher_id');
        $returned_items = $this->input->post('returned_items');
        if(!empty($returned_items))
        {
            $buy_details = $this->Exchange_with_vendor_model->get_buy_details_return($voucher_id);
            if(!$buy_details)
            {
                echo 'no return';exit;
            }
            $buy_details_id = $buy_details['buy_details_id'];
            $returned_price = 0;
            foreach ($returned_items as $key => $value) 
            {
                $new_buying_price = $value['buying_price_final'];
                if($value['item_imei_number'])
                {
                    $imei_data = array();
                    $imei_data['buy_details_id'] = "";
                    $imei_data['imei_barcode'] = $value['item_imei_number'];
                    $imei_data['publication_status'] = "deactivated";
                    $imei_data['item_spec_set_id'] = $value['item_spec_set_id'];
                    $this->Exchange_with_vendor_model->update_imei_barcode($imei_data,$value['item_imei_number'],$value['item_spec_set_id'],$buy_details_id);
                }

                unset($value['item_name']);
                unset($value['buying_price_final']);
                unset($value['item_imei_number']);

                $insert_data = $value;
                $insert_data['user_id'] = $this->session->userdata('user_id');
                $insert_data['buy_details_id'] = $buy_details_id;

                $find_item_quantity_on_inventory = $this->Exchange_with_vendor_model->get_item_quantity_inventory($value['item_spec_set_id']);  
                $check_cart_item_is_exist = $this->Exchange_with_vendor_model->get_item_quantity_cart($buy_details_id, $value['item_spec_set_id']);

                if($check_cart_item_is_exist) 
                {
                    $discount_unit_price = $check_cart_item_is_exist['discount_amount']/$check_cart_item_is_exist['quantity'];
                    $insert_data['quantity'] = $check_cart_item_is_exist['quantity'] - $value['quantity'];
                    // $insert_data['sub_total'] = ($check_cart_item_is_exist['quantity'] - $value['quantity'])*$check_cart_item_is_exist['buying_price'];
                    if($check_cart_item_is_exist['discount_type'] == "percentage")
                    {
                        $insert_data['discount_amount'] = $check_cart_item_is_exist['discount_amount'];
                        $discount_price = (($check_cart_item_is_exist['quantity'] - $value['quantity'])*$check_cart_item_is_exist['buying_price'])*($check_cart_item_is_exist['discount_amount']/100);
                        $insert_data['sub_total'] = (($check_cart_item_is_exist['quantity'] - $value['quantity'])*$check_cart_item_is_exist['buying_price']) - $discount_price;
                    }
                    else
                    {
                        $insert_data['discount_amount'] = $insert_data['quantity']*$discount_unit_price;
                        $insert_data['sub_total'] = (($check_cart_item_is_exist['quantity'] - $value['quantity'])*$check_cart_item_is_exist['buying_price']) - $insert_data['discount_amount'];
                    }
                    $this->Exchange_with_vendor_model->update_buy_cart_item_by_id($insert_data, $check_cart_item_is_exist['buy_cart_items_id']);
                }

                $item_spec_set_id = $value['item_spec_set_id'];
                $current_items_on_inventory = $this->Exchange_with_vendor_model->get_item_quantity_inventory($item_spec_set_id);

                $update_inventory_data['quantity'] = $current_items_on_inventory['quantity'] - $value['quantity'];
                $sell_cart_inventory_update = $this->Exchange_with_vendor_model->update_inventory($update_inventory_data,$value['item_spec_set_id']);
                $returned_price += $value['quantity']*$new_buying_price;

                $return_hstory_data = array();
                $return_hstory_data['voucher_no'] = $voucher_id;
                $return_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
                $return_hstory_data['type'] = "returned_with_vendor";
                $return_hstory_data['publication_status'] = "activated";
                $return_hstory_data['quantity'] = $value['quantity'];
                $return_hstory_data['user_id'] = $this->session->userdata('user_id');
                $add_return_history = $this->Exchange_with_vendor_model->save_exchange_history($return_hstory_data);  
            }

            $all_buy_list = $this->Exchange_with_vendor_model->find_items_on_cart_items($buy_details_id);
            $current_grand_total = 0;
            foreach ($all_buy_list as $key => $value) 
            {
               $current_grand_total = $current_grand_total  + $value['sub_total'];
           }
           $this->db->trans_begin();
           $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
           $buy_details_new = array();

           if($returned_price>$buy_details['due'])
           {
            $buy_details_new['due'] = 0;
            $buy_details_new['grand_total'] = $current_grand_total;
            $buy_details_new['discount'] =  ($buy_details['discount']/$buy_details['grand_total']*$current_grand_total);

            $buy_details_new['net_payable'] = $buy_details['net_payable']-$returned_price;
            $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + ($returned_price-$buy_details['due']);

            if($buy_details['due']==0)
            {
                $buy_details_new['paid'] = $buy_details['paid']-$returned_price;
            }
            else
            {
                $buy_details_new['paid'] = $buy_details['paid']-($returned_price-$buy_details['due']);
            } 
        }
        else if($buy_details['due']>=$returned_price)
        {
            $buy_details_new['due'] = $buy_details['due']-$returned_price;
            $buy_details_new['grand_total'] = $current_grand_total;
            $buy_details_new['net_payable'] = $buy_details['net_payable']-$returned_price;
            $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'];
        }

            // if($buy_details['paid']==0)
            // {
            //     $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'];
            // }
            // else
            // {
            //     $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $returned_price;
            // }

        $cashbox_update = $this->Exchange_with_vendor_model->update_final_cashbox_amount($cashbox_amount_update_data);
        $buy_details_update = $this->Exchange_with_vendor_model->update_buy_details($buy_details_new, $buy_details_id); 

        $find_items_on_cart_items = $this->Exchange_with_vendor_model->find_items_on_cart_items($buy_details_id);
        $cash_data['cash_type']= "returned_money";
        $cash_data['user_id'] = $this->session->userdata('user_id');;
        $cash_data['deposit_withdrawal_amount'] = $buy_details['paid'];
        $this->Exchange_with_customer_model->save_cash_box_info($cash_data);
    }
    else
    {

        echo 'No items returned';exit;

        $buy_details = $this->Exchange_with_vendor_model->get_buy_details_return($voucher_id);

        if(!$buy_details) 
        {
            echo 'no return';exit;
        }
        $buy_details_id = $buy_details['buy_details_id'];
        $find_items_on_cart_items = $this->Exchange_with_vendor_model->find_items_on_cart_items($buy_details_id);
        $this->db->trans_begin();



        foreach ($find_items_on_cart_items as $key => $value) 
        {
            $item_spec_set_id = $value['item_spec_set_id'];
            $current_items_on_inventory = $this->Exchange_with_vendor_model->get_item_quantity_inventory($item_spec_set_id);

            if($value['quantity']>$current_items_on_inventory['quantity'])
            {
                echo "insufficient amount on stock";exit;
            }
            $update_inventory_data['quantity'] = $current_items_on_inventory['quantity'] - $value['quantity'];
            $sell_cart_inventory_update = $this->Exchange_with_vendor_model->update_inventory($update_inventory_data,$value['item_spec_set_id']);

            $update_sell_cart_item_data['quantity'] = 0;
            $update_sell_cart_item_data['publication_status'] = "deactivated";

            $buy_cart_items_update = $this->Exchange_with_vendor_model->buy_cart_items_update($update_sell_cart_item_data,$buy_details_id,$value['item_spec_set_id']);

            $return_hstory_data = array();
            $return_hstory_data['voucher_no'] = $voucher_id;
            $return_hstory_data['item_spec_set_id'] = $value['item_spec_set_id'];
            $return_hstory_data['type'] = "returned_with_vendor";
            $return_hstory_data['publication_status'] = "activated";
            $return_hstory_data['quantity'] = $value['quantity'];
            $return_hstory_data['user_id'] = $this->session->userdata('user_id');
            $add_return_history = $this->Exchange_with_vendor_model->save_exchange_history($return_hstory_data);

        }



        $current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
        if($buy_details['paid']==0)
        {
            $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'];
        }
        else
        {
            $cashbox_amount_update_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $buy_details['paid'];
        }
        
        $cashbox_update = $this->Exchange_with_vendor_model->update_final_cashbox_amount($cashbox_amount_update_data);

        $bdata['publication_status'] = 'activated';
        $bdata['paid'] = 0;
        $bdata['grand_total'] = 0;
        $bdata['net_payable'] = 0;
        $bdata['discount'] = 0;
        $bdata['due'] = 0;
        $bdata['publication_status'] = "deactivated";
        $buy_details_update = $this->Exchange_with_vendor_model->update_buy_details($bdata,$buy_details_id);
        $cash_data['cash_type']= "returned_money";
        $cash_data['user_id'] = $this->session->userdata('user_id');;
        $cash_data['deposit_withdrawal_amount'] = $buy_details['paid'];
        $this->Exchange_with_customer_model->save_cash_box_info($cash_data);

        
            // if($buy_details['due']>0)
            // {
            //     echo 'Please Due First.Money return is not possible now';exit;
            // }
    }



    if ($this->db->trans_status() === false) {
        $this->db->trans_rollback();
        echo "error transection";
    }
    else
    {
        $this->db->trans_commit();
        echo "success";
    }
}

}

/* End of file Return_money.php */
/* Location: ./application/controllers/Return_money.php */
