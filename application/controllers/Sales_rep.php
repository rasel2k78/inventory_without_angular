<?php

/**
 * Sales representative Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for add sales rep , edit sales rep , update sales rep info or delete sales rep info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/
class Sales_rep extends CI_Controller
{
    /**
     * This is a constructor function
     *
     * This load Inventory Model and language pages when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Inventory_model');
        $this->load->model('User_access_check_model');
        $this->load->library('upload');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('add_sales_rep_form_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();
        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {
            redirect('Login', 'refresh');
        }
    }
    /**
     * This function is used for loading sales rep view page
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $this->load->view('sales_rep/add_sales_representative_form');
        }
        else{
            $this->load->view('access_deny/not_permitted');
        }
        load_footer();
    }
    /**
     * Save sales rep information to database.
     *
     * @return void
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function save_sales_rep()
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $data = array(
             'sales_rep_name' => trim($this->input->post('sales_rep_name', true)),
             'sales_rep_email' => trim($this->input->post('sales_rep_email', true)),
             'sales_rep_phone_1' => trim($this->input->post('sales_rep_phone_1', true)),
             'sales_rep_phone_2' => trim($this->input->post('sales_rep_phone_2', true)),
             'sales_rep_present_address' => trim($this->input->post('sales_rep_present_address', true)),
             'sales_rep_permanent_address' => trim($this->input->post('sales_rep_permanent_address', true)),
             'sales_rep_national_id' => trim($this->input->post('sales_rep_national_id', true)),
             'user_id' => $this->session->userdata('user_id'),
             );
            if (!empty($_FILES['sales_rep_image']['name'])) {
               $config['upload_path'] = 'images/sales_rep_images/';
               $config['allowed_types'] = 'gif|jpg|png|jpeg';
               $config['max_size'] = '2000';
               $config['max_width'] = '9999';
               $config['max_height'] = '9999';
               $config['encrypt_name'] = true;
               $this->load->library('upload', $config);
               $this->upload->initialize($config);
               $error = '';
               $fdata = array();
               if (!$this->upload->do_upload('sales_rep_image')) {
                $error = $this->upload->display_errors();
                echo $error;
            } else {
                $fdata = $this->upload->data();
                $data['sales_rep_image'] = $config['upload_path'].$fdata['file_name'];
            }
        }
        if($data['sales_rep_name']==null || trim($data['sales_rep_name'])=="") {
         echo "no_name";
         exit();
     }
     if($this->Inventory_model->save_sales_rep_info($data)) {
         echo "success";
     }
     else
     {
         echo "failed";
     }
 }
 else
 {
    echo "No permission";
}
}
    /**
     * Collect all data for AJAX datatable
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_sales_rep_info_for_datatable()
    {
        $filters = $this->input->get();
        $all_data = $this->Inventory_model->all_sales_rep_info($filters);
        $all_data_without_limit = $this->Inventory_model->all_sales_rep_info($filters, true);
        $all_data_final = $this->Inventory_model->all_sales_rep_info($filters, true);
        $output_data=[];
        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;
        echo json_encode($output_data);
    }
    /**
     * Delete sales rep info form AJAX Datatable. It actually change the publication status from "activated" to "deactivated".
     * 
     *@param  string $sales_rep_id
     * @return void
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function delete_sales_rep($sales_rep_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            if($this->Inventory_model->delete_sales_rep_info($sales_rep_id)) {
                echo "success" ;    
            }    
            else{
                echo "failed" ;
            }
        }
        else
        {
            echo "No Permission";
        }
    }
    /**
     * Collect individual data from database by sales rep id
     * 
     *@param  string $sales_rep_id
     * @return array[] 
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function get_sales_rep_info($sales_rep_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
             'sales_rep_info_by_id' => $this->Inventory_model->sales_rep_info_by_id($sales_rep_id),
             );
            echo json_encode($body_data);
        }
        else
        {
            echo json_encode(array('permission' =>'no'));
        }
    }
    /**
     * Update sales representatives data in database
     *
     * @return void
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function update_sales_rep() 
    {
        $sales_rep_id = $this->input->post('sales_rep_id', true);
        $data = array(
         'sales_rep_name' => trim($this->input->post('sales_rep_name_edit', true)),
         'sales_rep_email' => trim($this->input->post('sales_rep_email_edit', true)),
         'sales_rep_phone_1' => trim($this->input->post('sales_rep_phone_1_edit', true)),
         'sales_rep_phone_2' => trim($this->input->post('sales_rep_phone_2_edit', true)),
         'sales_rep_present_address' => trim($this->input->post('sales_rep_present_address_edit', true)),
         'sales_rep_permanent_address' => trim($this->input->post('sales_rep_permanent_address_edit', true)),
         'sales_rep_national_id' => $this->input->post('sales_rep_national_id_edit', true),
         'user_id' => $this->session->userdata('user_id'),
         );
        if (!empty($_FILES['sales_rep_image_edit']['name'])) {
            $config['upload_path'] = 'images/sales_rep_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2000';
            $config['max_width'] = '9999';
            $config['max_height'] = '9999';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $error = '';
            $fdata = array();
            if (!$this->upload->do_upload('sales_rep_image_edit')) {
                $error = $this->upload->display_errors();
                echo $error;
            } else {
                $fdata = $this->upload->data();
                $data['sales_rep_image_edit'] = $config['upload_path'].$fdata['file_name'];
            }
        }
        if($data['sales_rep_name']==null || trim($data['sales_rep_name'])=="") {
            echo "no_name";
            exit();
        }
        if($this->Inventory_model->update_sales_rep_info($sales_rep_id, $data)) {
            echo "success";
        }
        else
        {
            echo "failed";
        }
    }
}
/* End of file Sales_rep.php */
/* Location: ./application/controllers/Sales_rep.php */