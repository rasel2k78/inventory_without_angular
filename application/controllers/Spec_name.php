<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Spec_name extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Spec_name_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('spec_name_page_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();
        
        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }

    }

    public function index()
    {

        load_header();
        $this->load->view('spec_name/add_spec_name_form');
        load_footer();
    }

    /**
     * Save Specification Names To Databse
     *
     * @return void
     * @author jk
     **/
    public function save_spec_name_info()
    {
        $user_id_logged = $this->session->userdata('user_id');

        
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $output = array();
            $output['success'] = 1;
            $output['error']= array();

            if($this->input->post('spec_name', true) ==null || trim($this->input->post('spec_name', true))=="" ) {
                $output['error']['spec_name']= $this->lang->line('validation_name');;
                $output['success']= 0;
            }

            $spec_name = trim($this->input->post('spec_name', true));
            if($this->Spec_name_model->check_spec_name($spec_name)) {
                $output['error']['spec_name']= $this->lang->line('validation_check_spec_name');;
                $output['success']= 0;
            }

            if($output['success']==0) {
                echo json_encode($output);
                exit();
            }
            else{

                $data= array(

                 'spec_name' =>strtoupper(trim($this->input->post('spec_name', true))),
                 'spec_description' => $this->input->post('spec_description', true),
                 'spec_default_status' => $this->input->post('spec_default_status', true),
                 'user_id' => $this->session->userdata('user_id'),
                 );

                $output['data'] = $this->Spec_name_model->save_spec_name_info($data);
                echo json_encode($output);
            }
        }
        else {
            echo json_encode("No Permisssion");
        }

    }

    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function all_spec_name_info_for_datatable()
    {        
        $filters = $this->input->get();
        // print_r($filters);
        // exit();
        // $this->load->model('Inventory_model');
        $all_data = $this->Spec_name_model->all_spec_name_info($filters);
        $all_data_without_limit = $this->Spec_name_model->all_spec_name_info($filters, true);
        $all_data_final = $this->Spec_name_model->all_spec_name_info($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);

    }

    /**
     * This function collect all informations of individual specification by parameter.
     * 
     * @param string $spec_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_spec_name_info($spec_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
             'spec_name_info_by_id' => $this->Spec_name_model->spec_name_info_by_id($spec_id),
             );

            echo json_encode($body_data);

        }
        else {
            echo json_encode("No Permisssion");
        }

    }

    

    /**
     * This function is used for update specification's information in database.
     * 
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function update_spec_name_info()
    {

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('spec_name_edit', true)==null || trim($this->input->post('spec_name_edit', true))=="" ) {
            $output['error']['spec_name_edit'] =  $this->lang->line('validation_name');
            $output['success'] = 0;
        }


        if($output['success'] == 1) {
            $spec_name_id = $this->input->post('spec_name_id', true);
            $data=array(

             'spec_name' =>strtoupper(trim($this->input->post('spec_name_edit', true))),
             'spec_description' => $this->input->post('spec_description_edit', true),
             'spec_default_status' => $this->input->post('spec_default_status_edit', true),
             'user_id' => $this->session->userdata('user_id'),
             );

            // echo "<pre>";
            // print_r($data);
            // exit();

            if($this->Spec_name_model->update_spec_name_info($spec_name_id, $data)) {
               echo json_encode($output);
           }
           else{

             $output['success']  = 0;
             echo json_encode($output);
         }

     }
     else{

        echo json_encode($output);
    }
}




    /**
     * This function will delete/hide  the expenditure type's information by "expenditure_types_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $expenditure_types_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_spec_name($spec_name_id)
    {

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            if($this->Spec_name_model->delete_spec_name_info($spec_name_id)) {
                echo "success";    
            }    

            else
            {
                echo "failed";
            }

        }
        else {
            echo "No Permisssion";
        }

    }

}

/* End of file Spec_name.php */
/* Location: ./application/controllers/Spec_name.php */