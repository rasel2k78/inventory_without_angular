<?php 
/**
 * Nagadanboi Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
/**
 * Use this controller for save,edit and delete deposit/withdrawal money.  
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/
class Nagadanboi extends CI_Controller
{
    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/
    public function __construct()
    {
    	parent::__construct();

    	$this->load->model('Inventory_model');
    	$this->load->model('Bank_model');
    	$this->load->model('User_access_check_model');
    	$cookie = $this->input->cookie('language', true);
    	$this->lang->load('cashbox_page_lang', $cookie);
    	$this->lang->load('left_side_nev_lang', $cookie);
    	$this->lang->load('access_message_lang', $cookie);

    	$user_id = $this->session->userdata('user_id');
    	if ($user_id == null) {

    		redirect('Login', 'refresh');
    	}
    }
    /**
     * This function is used for loading deposit/withdrawal form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function index()
    {
    	load_header();
    	$user_id_logged = $this->session->userdata('user_id');
    	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
    		$body_data = array(
    			'final_cashbox_info' => $this->Inventory_model->final_cashbox_amount_info(),
              );
    		$this->load->view('nagadanboi/add_nagadanboi_form', $body_data);
    	}
    	else{
    		$this->load->view('access_deny/not_permitted');
    	}
    	load_footer();    
    }
    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function all_cashbox_info_for_datatable()
    {        
    	$filters = $this->input->get();
    	$all_data = $this->Inventory_model->all_cashbox_info($filters);
    	$all_data_without_limit = $this->Inventory_model->all_cashbox_info($filters, true);
    	$all_data_final = $this->Inventory_model->all_cashbox_info($filters, true);
    	$output_data=[];
    	$output_data["draw"]=$filters['draw'];
    	$output_data["recordsTotal"]=$all_data_without_limit;
    	$output_data["recordsFiltered"]=$all_data_final;
    	$output_data["data"]=$all_data;
    	echo json_encode($output_data);
    }
    /**
     * This function save all deposit-withdrawal informations in database.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function save_deposit_withdrawal_amount_info()
    {
    	userActivityTracking();

    	$user_id_logged = $this->session->userdata('user_id');
    	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

    		$output=array();
    		$output['success'] = 1;
    		$output['error'] = array();
    		if($this->input->post('cash_type', true)==null) {
    			$output['error']['cash_type'] = $this->lang->line('validation_amount');
    			$output['success'] = 0;
    		}
    		if($this->input->post('deposit_withdrawal_amount', true)==null || !is_numeric($this->input->post('deposit_withdrawal_amount', true))) {
    			$output['error']['deposit_withdrawal_amount'] = $this->lang->line('validation_amount');
    			$output['success'] = 0;
    		}
    		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    		if($this->input->post('cash_type', true)== "withdrawal" && $current_cashbox_amount['total_cashbox_amount'] <= 0 ) {
    			$output['error']['deposit_withdrawal_amount'] = $this->lang->line('withdrawal_failure');
    			$output['success'] = 0;
    		}
    		if($this->input->post('cash_type', true)== "withdrawal" && $this->input->post('deposit_withdrawal_amount', true) > $current_cashbox_amount['total_cashbox_amount'] ) {
    			$output['error']['deposit_withdrawal_amount'] = $this->lang->line('withdrawal_failure');
    			$output['success'] = 0;
    		}
    		if($output['success']==0) {
    			echo json_encode($output);
    			exit();
    		}
    		else{
    			$this->db->trans_begin();
    			$cash_type= $this->input->post('cash_type', true);

    			if($cash_type == "transferred"){

    				$cash_data= array(
    					'cash_description' =>$this->input->post('cash_description', true),
    					'cash_type' => $this->input->post('cash_type', true),
    					'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
    					'user_id' => $this->session->userdata('user_id'),
    					'buy_or_sell_details_id' => $this->input->post('bank_acc_id', TRUE),
                        );

    				$cashbox_id= $this->Inventory_model->save_cash_box_info($cash_data);
    				$final_data_for_cashbox['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $cash_data['deposit_withdrawal_amount'];
    				$this->Inventory_model->update_total_cashbox_info($final_data_for_cashbox);
    				$data= array(
    					'cash_description' =>$this->input->post('cash_description', true),
    					'cash_type' => "received",
    					'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
    					'bank_acc_id' => $this->input->post('bank_acc_id', TRUE),
    					'user_id' => $this->session->userdata('user_id'),
    					'buy_sell_and_other_id'=> $cashbox_id,
                        );

    				$bank_acc_id= $this->input->post('bank_acc_id', TRUE);
    				$curr_bank_amt = $this->Bank_model->get_final_amt_by_acc($bank_acc_id);

    				$final_data['final_amount'] = $curr_bank_amt['final_amount'] + $data['deposit_withdrawal_amount'];
    				$this->Bank_model->update_total_amt_info_of_acc($final_data,$bank_acc_id);
    				$output['data'] = $this->Bank_model->save_bank_dw_info($data);
    				echo json_encode($output);
    				if ($this->db->trans_status() === false) {
    					$this->db->trans_rollback();
    				}
    				else
    				{
    					$this->db->trans_commit();
    				}
    			}
    			elseif ($cash_type == "advance_to_vendor") {
    				$cash_data= array(
    					'cash_description' =>$this->input->post('cash_description', true),
    					'cash_type' => $this->input->post('cash_type', true),
    					'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
    					'user_id' => $this->session->userdata('user_id'),
    					'buy_or_sell_details_id' => $this->input->post('vendors_id', TRUE),
                        );
    				$cashbox_id= $this->Inventory_model->save_cash_box_info($cash_data);
    				$final_data_for_cashbox['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $cash_data['deposit_withdrawal_amount'];
    				$this->Inventory_model->update_total_cashbox_info($final_data_for_cashbox);

    				$data= array(
    					'cash_description' =>$this->input->post('cash_description', true),
    					'cash_type' => "received",
    					'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
    					'bank_acc_id' => $this->input->post('bank_acc_id', TRUE),
    					'user_id' => $this->session->userdata('user_id'),
    					'buy_sell_and_other_id'=> $cashbox_id,
                        );
    				$vendor_id= $this->input->post('vendors_id', TRUE);
    				$curr_amt = $this->Inventory_model->get_current_amount_by_vendor($vendor_id);

    				$final_data_vendor['current_balance'] = $curr_amt['current_balance'] + $cash_data['deposit_withdrawal_amount'];

    				$this->Inventory_model->update_total_amt_info_of_vendor($final_data_vendor,$vendor_id);

          // $output['data'] = $this->Bank_model->save_bank_dw_info($data);
    				echo json_encode($output);
    				if ($this->db->trans_status() === false) {
    					$this->db->trans_rollback();
    				}
    				else
    				{
    					$this->db->trans_commit();
    				}
    			}
    			else{
    				$data= array(
    					'cash_description' =>$this->input->post('cash_description', true),
    					'cash_type' => $this->input->post('cash_type', true),
    					'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount', true),
    					'user_id' => $this->session->userdata('user_id'),
                        );

         		//Added by zubayer 2019-04-09
    				$date = $this->input->post('date');
    				if(!empty($date)){
    					$data['date_created'] = date("Y-m-d H:i:s", strtotime($date));
    				}

    				$amount['total_cashbox_amount'] = $data['deposit_withdrawal_amount'];
    				if($data['cash_type']== 'deposit' || $data['cash_type']== 'commission') {
    					$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $data['deposit_withdrawal_amount'];
    				} else{
    					$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['deposit_withdrawal_amount'];
    				}
    				if($current_cashbox_amount['total_cashbox_amount'] ==null) {
    					$this->Inventory_model->save_deposit_amount_info($amount);
    				}
    				else{
    					$this->Inventory_model->update_total_cashbox_info($final_data);
    				}
    				// echo "<pre>";
    				// print_r($data);exit;
    				$output['data'] = $this->Inventory_model->save_cash_box_info($data);
    				echo json_encode($output);
    				if ($this->db->trans_status() === false) {
    					$this->db->trans_rollback();
    				}
    				else
    				{
    					$this->db->trans_commit();
    				}
    			}
    		}
    	}
    	else{
    		echo json_encode("No Permission");
    	}
    }
    /**
     * This function collect all informations of individual deposit/withdrawal type by parameter.
     * 
     * @param string $cashbox_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function get_cashbox_info($cashbox_id)
    {
    	userActivityTracking();

    	$user_id_logged = $this->session->userdata('user_id');
    	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
    		$body_data= array(
    			'all_cashbox_by_id' => $this->Inventory_model->all_cashbox_info_by_id($cashbox_id),
              );
    		echo json_encode($body_data);
    	}
    	else {
    		echo json_encode("No Permission");
    	}
    }
    /**
     * This function is used for update cashbox's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
     **/
    public function update_cashbox_info()
    {
    	userActivityTracking();

    	$output=array();
    	$output['success'] = 1;
    	$output['error'] = array();
    	$final_data=array(); 
    	if($this->input->post('deposit_withdrawal_amount_edit', true)==null || !is_numeric($this->input->post('deposit_withdrawal_amount_edit', true))) {
    		$output['error']['deposit_withdrawal_amount_edit'] = $this->lang->line('validation_amount');
    		$output['success'] = 0;
    	}
    	$cash_type = $this->input->post('cash_type_edit', true);
    	$deposit_withdrawal_amount = $this->input->post('deposit_withdrawal_amount_edit', true);
    	$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    	if($cash_type == 'withdrawal' && ($deposit_withdrawal_amount >  $current_cashbox_amount['total_cashbox_amount']) ) {
    		$output['error']['deposit_withdrawal_amount_edit'] = $this->lang->line('more_amount_than_cashbox');
    		$output['success'] = 0;    
    	}
    	if($output['success']==0) {
    		echo json_encode($output);
    		exit();
    	}
    	if($output['success'] == 1) {
    		$cashbox_id = $this->input->post('cashbox_id', true);
    		$data=array(
    			'cash_type' => $this->input->post('cash_type_edit', true),
    			'cash_description' => $this->input->post('cash_description_edit', true),
    			'deposit_withdrawal_amount' => $this->input->post('deposit_withdrawal_amount_edit', true),
    			'user_id' => $this->session->userdata('user_id'),
    			'buy_or_sell_details_id' => $this->input->post('bank_acc_id_edit', true),
              );
    		$this->db->trans_begin();
    		$previous_amount_type_by_id = $this->Inventory_model->previous_amount_info_by_id($cashbox_id);
    		$difference_amount = $data['deposit_withdrawal_amount'] - $previous_amount_type_by_id['deposit_withdrawal_amount'];
    		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    		if($data['cash_type']== 'transferred'){
    			$bank_acc_id = $this->input->post('bank_acc_id_edit');
    			$curr_acc_blnc = $this->Bank_model->get_bank_balance_info($bank_acc_id);
    			$final_data_bank= array(
    				'final_amount' => $curr_acc_blnc['final_amount'] + $difference_amount,
                 );
    			$this->Bank_model->update_acc_balance_info($final_data_bank, $bank_acc_id);
    			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $difference_amount;
    			$bank_data=array(
    				'deposit_withdrawal_amount'=> $previous_amount_type_by_id['deposit_withdrawal_amount'] + $difference_amount,
                 );
    			$this->Bank_model->update_bank_statement($cashbox_id,$bank_data);
    		}
    		else{
    			if(($data['cash_type']== 'deposit' || $data['cash_type']== 'commission') && ($previous_amount_type_by_id['cash_type'] == 'deposit' || $previous_amount_type_by_id['cash_type'] == 'commission')) {
    				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $difference_amount;
    			}
    			if($data['cash_type']== 'withdrawal' && $previous_amount_type_by_id['cash_type'] == 'withdrawal') {
    				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $difference_amount;
    			}
    			if($data['cash_type']== 'withdrawal' && ($previous_amount_type_by_id['cash_type'] == 'deposit' || $previous_amount_type_by_id['cash_type'] == 'commission')) {
    				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $data['deposit_withdrawal_amount'];
    			}
    			if(($data['cash_type']== 'deposit' || $data['cash_type']== 'commission') && $previous_amount_type_by_id['cash_type'] == 'withdrawal') {
    				$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $data['deposit_withdrawal_amount'];
    			}
    		}
    		$this->Inventory_model->update_total_cashbox_info($final_data);
    		$output['data']= $this->Inventory_model->update_cashbox_info($cashbox_id, $data);
    		echo json_encode($output);
    		if ($this->db->trans_status() === false) {
    			$this->db->trans_rollback();
    		}
    		else
    		{
    			$this->db->trans_commit();

    		}
    	}
    	else{
    		echo json_encode($output);
    	}
    }
    /**
     * This function will delete/hide  the cashbox's information by "cashbox_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $cashbox_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/
    public function delete_cashbox($cashbox_id)
    {
    	userActivityTracking();

    	$user_id_logged = $this->session->userdata('user_id');
    	$pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    	if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
    		$current_cashbox_amount = $this->Inventory_model->final_cashbox_amount_info();
    		$previous_amount_type_by_id = $this->Inventory_model->previous_amount_info_by_id($cashbox_id);
    		if($previous_amount_type_by_id['cash_type'] == 'transferred'){
    			$bank_acc_id = $previous_amount_type_by_id['buy_or_sell_details_id'];
    			$curr_acc_blnc = $this->Bank_model->get_bank_balance_info($bank_acc_id);
    			$final_data_bank['final_amount']= $curr_acc_blnc['final_amount'] - $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Bank_model->update_acc_balance_info($final_data_bank, $bank_acc_id);
    			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Inventory_model->update_total_cashbox_info($final_data);
    			$bank_data=array(
    				'publication_status' =>"deactivated",
                 );
    			$this->Bank_model->update_bank_statement($cashbox_id,$bank_data);
    			$data['publication_status'] = 'deactivated';
    		}
    		if($previous_amount_type_by_id['cash_type'] == 'deposit' || $previous_amount_type_by_id['cash_type'] == 'commission') {
    			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] - $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Inventory_model->update_total_cashbox_info($final_data);
    			$data['publication_status'] = 'deactivated';
    		}
    		if ($previous_amount_type_by_id['cash_type'] == 'withdrawal') {
    			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Inventory_model->update_total_cashbox_info($final_data);
    			$data['publication_status'] = 'deactivated';
    		}
    		if ($previous_amount_type_by_id['cash_type'] == 'advance_to_vendor') {
    			$vendor_id= $previous_amount_type_by_id['buy_or_sell_details_id'];
    			$curr_amt = $this->Inventory_model->get_current_amount_by_vendor($vendor_id);
    			$final_data_vendor['current_balance'] = $curr_amt['current_balance'] - $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Inventory_model->update_total_amt_info_of_vendor($final_data_vendor,$vendor_id);
    			$final_data['total_cashbox_amount'] = $current_cashbox_amount['total_cashbox_amount'] + $previous_amount_type_by_id['deposit_withdrawal_amount'];
    			$this->Inventory_model->update_total_cashbox_info($final_data);
    			$data['publication_status'] = 'deactivated';
    		}
    		if($this->Inventory_model->delete_cashbox_info($cashbox_id, $data)) {
    			echo "success";    
    		}    
    		else{
    			echo "failed";
    		}
    	}
    	else 
    	{
    		echo "No Permission";
    	}
    }
/**
 * Collect current cashbox amount.
 *
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function current_cashbox_amount()
{
	$amount = $this->Inventory_model->final_cashbox_amount_info();
	echo json_encode($amount);
}

}
/* End of file nagadanboi.php */
/* Location: ./application/controllers/nagadanboi.php */