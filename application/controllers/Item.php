<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Item extends CI_Controller
{
 private $shop_id;
 public function __construct()
 {
    parent::__construct();
    $this->load->model('Item_model');
    $this->load->model('Inventory_model');
    $cookie = $this->input->cookie('language', true);
    $this->lang->load('buy_page_lang', $cookie);
    $this->lang->load('add_vendor_form_lang', $cookie);
    $this->lang->load('left_side_nev_lang', $cookie);
    $this->lang->load('add_item_form_lang', $cookie); 
    $this->lang->load('spec_name_page_lang', $cookie);
    $this->lang->load('add_category_form_lang', $cookie);
    $this->load->model('User_access_check_model');
    userActivityTracking();

    $user_id = $this->session->userdata('user_id');
    if ($user_id == null) {

        redirect('Login', 'refresh');
    }

}

public function index()
{
    $shop_info =$this->Item_model->get_my_shop();
    $this->shop_id = $shop_info['shop_id'];

    $user_id_logged = $this->session->userdata('user_id');
    $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
    if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
        $body_data= array(
           'all_specs_autoselected' => $this->Item_model->all_specs_info(),
       );
        load_header();
        $this->load->view('item/add_item_form', $body_data);
        load_footer();
    }

    else{
        load_header();
        $this->load->view('access_deny/not_permitted');
        load_footer();
    }
}

public function quick_item_buy()
{
 load_header();
 $current_amount= $this->Inventory_model->final_cashbox_amount_info();
 if($current_amount == "" || $current_amount == NULL || empty($current_amount)){
    $current_amount['total_cashbox_amount'] = 0;
}
$body_data= array(
   'all_specs_autoselected' => $this->Item_model->all_specs_info(),
   'cashbox_amount' => $current_amount,
);
$this->load->view('item/quick_item_form',$body_data);
load_footer();
}
public function error_test()
{
    $sql = "Select * From mytable";
    $query = $this->db->query($sql);
    echo $this->db->__error_message();
    echo $this->db->__error_number();


            $db_debug = $this->db->db_debug; //save setting

        // $this->db->db_debug = FALSE; //disable debugging for queries

        // $result = $this->db->query($sql); //run query

        // //check for errors, etc

        // $this->db->db_debug = $db_debug;
        }



        public function all_item_info_for_datatable()
        {

            $filters = $this->input->get();
        //print_r($filters);exit;
        // $this->load->model('Inventory_model');
            $all_data = $this->Item_model->all_item_info_for_datatable($filters);
            $all_data_without_limit = $this->Item_model->all_item_info_for_datatable($filters, true);
            $all_data_final = $this->Item_model->all_item_info_for_datatable($filters, true);

            $output_data=[];

            $output_data["draw"] = $filters['draw'];
            $output_data["recordsTotal"] = $all_data_without_limit;
            $output_data["recordsFiltered"] = $all_data_final;
            $output_data["data"] = $all_data;

            echo json_encode($output_data);
        }


        public function search()
        {
            $key_data = $this->input->post('keyword');
            $search_data = $this->Item_model->search_by_key($key_data);

    //echo '<pre>';print_r($search_data);exit;

            echo json_encode($search_data);exit;
        }

    /**
     * This function save all  informations of item form to database.
     *
     * @return null
     * @author shoaib <raselahmed2k7@gmail.com>
     **/
    
    public function save_item_info()
    {
//echo '<pre>';print_r($this->input->post());exit;

        // $user_id_logged = $this->session->userdata('user_id');
        // $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        // if($this->User_access_check_model->check_access($user_id_logged,$pages_address))
        // {
        $udata=array();
        $data=array();

        $udata['items_name'] = $this->input->post("items_name");
        if($this->input->post("categories_id")==null) {
            $udata['categories_id'] = null;
        }
        else
        {
            $udata['categories_id'] = $this->input->post("categories_id");
        }

        $udata['items_description'] = $this->input->post("items_description");
        $udata['user_id'] = $this->session->userdata('user_id');
        $spec_value_combinations = $this->input->post("specs_values");
        if(empty($spec_value_combinations)) {
            echo "Choose item spec combination";
            exit();
        }

        if ($udata['items_name'] == '' || $udata['items_name'] == null) {
            echo "Give a item name";
            exit();
        }
        $spec_set_info = array();
        $spec_set_connector= array();
        $spec_set_combination_values= array();
        $status = 0;
        $this->db->trans_begin();

        if($this->input->post("barcode_number"))
        {

         $check_barcode_exist = $this->Item_model->check_barcode_if_exist($this->input->post("barcode_number"));
         if($check_barcode_exist)
         {
            echo "barcode_exist";exit;
        }
        else
        {
            $spec_set_info['barcode_number'] = $this->input->post("barcode_number");
        }
    }
    else
    {
     $uuid_short_barcode = $this->generateRandomString();
     $spec_set_info['barcode_number'] = $uuid_short_barcode;
 }

 $item_insert_id = $this->Item_model->save_item_info($udata);
 if(!$item_insert_id) 
 {
    echo 'Item exist already';exit;
}

foreach ($spec_value_combinations as $key => $value) 
{
    $spec_values = '';
    foreach ($value as $spec_key => $combinations) 
    {
        $spec_values = $spec_values.'-'.$combinations['value'];    
    }
    $index = $key+1;

    /****************************Dynamic image upload for dynamic item specification Starts******************************************/
        // if (!empty($_FILES['combination_images_'.$index]['name'])) 
        // {
        //     $config['upload_path'] = 'images/item_images/';
        //     $config['allowed_types'] = 'gif|jpg|png|jpeg';
        //     $config['max_size'] = '2000';
        //     $config['max_width'] = '9999';
        //     $config['max_height'] = '9999';
        //     $config['encrypt_name'] = true;

        //     $this->load->library('upload', $config);
        //     $this->upload->initialize($config);
        //     $error = '';
        //     $fdata = array();
        //     if (!$this->upload->do_upload('combination_images_'.$index)) {
        //         $error = $this->upload->display_errors();
        //         echo $error;
        //     } else {
        //         $fdata = $this->upload->data();
        //         $spec_set_info['item_spec_set_image'] = $config['upload_path'] . $fdata['file_name'];
        //     }
        // }
    /****************************Dynamic image upload for dynamic item specification Ends******************************************/


    /****************************Static image upload for dynamic item specification Starts******************************************/

    if (!empty($_FILES['edit_item_image']['name'])) 
    {
        $config['upload_path'] = 'images/item_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        // $config['max_size'] = '2000';
        // $config['max_width'] = '9999';
        // $config['max_height'] = '9999';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('edit_item_image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } else {
            $fdata = $this->upload->data();
            $spec_set_info['item_spec_set_image'] = $config['upload_path'] . $fdata['file_name'];
        }
    }

    /****************************Static image upload for dynamic item specification Ends******************************************/

    if($this->input->post("is_unique")=="yes")
    {
        $spec_set_info['unique_barcode'] = 'yes';
    }
    else
    {
        $spec_set_info['unique_barcode'] = 'no';
    }
    $spec_set_info['items_id'] = $item_insert_id;
    $spec_set_info['spec_set'] =$udata['items_name'].'-'.$spec_values;
    $spec_set_info['user_id'] = $this->session->userdata('user_id');
    $spec_set_info['product_warranty'] = $this->input->post('product_warranty');
    $insert_spec_set_id = $this->Item_model->save_spec_set($spec_set_info);

    foreach ($value as $key => $combinations) 
    {
        $data['spec_name_id'] = $combinations['id'];
        $data['spec_value'] = $combinations['value'];
        $data['user_id'] = $this->session->userdata('user_id');
        $check_value_exist_id = $this->Item_model->check_if_value_exist($combinations['value']);
                //echo $check_value_exist_id['spec_value_id'];exit;
        if($check_value_exist_id) {
            $specValueId = $check_value_exist_id['spec_value_id'];
        }
        else
        {
            $specValueId = $this->Item_model->insert_spec_value($data);
        }

        $spec_set_connector['item_spec_set_id'] = $insert_spec_set_id;
        $spec_set_connector['spec_name_id'] = $combinations['id'];
        $spec_set_connector['spec_value_id'] = $specValueId;
        $spec_set_connector['user_id'] = $this->session->userdata('user_id');
        $insert_spec_set_connector = $this->Item_model->save_spec_set_connector($spec_set_connector);
        $status = 1;
    }
}



if ($this->db->trans_status() === false) {
    $this->db->trans_rollback();
    echo "error transection";
}
else
{
    $this->db->trans_commit();
    if($status==1) {
        echo 'success';
    }
    else
    {
        echo 'failed';
    }
}

        // }
        // else
        // {
        // 	echo "No permission";
        // }
}

    /**
     * This function is used for update item's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <raselahmed2k7@gmail.com>
     **/

    public function update_item_info()
    {
       $udata=array();
       $data=array();
       //echo '<pre>';print_r($this->input->post());exit;

       $udata['items_name'] = $this->input->post("items_name");
       if($this->input->post("categories_id")==null) 
       {
        $udata['categories_id'] = null;
    }
    else
    {
        $udata['categories_id'] = $this->input->post("categories_id");
    }

    $udata['items_description'] = $this->input->post("items_description");
    $udata['user_id'] = $this->session->userdata('user_id');
    $item_spec_set_id = $this->input->post('item_spec_set_id');


    $spec_set_info = array();
    $spec_set_connector= array();
    $spec_set_combination_values= array();
    $status = 0;
    $this->db->trans_begin();
    if($this->input->post("barcode_number"))
    {
       $spec_set_info['barcode_number'] = $this->input->post("barcode_number");
       $check_barcode_exist = $this->Item_model->check_barcode_if_exist_another($this->input->post("barcode_number"),$item_spec_set_id);
       if($check_barcode_exist)
       {
        echo "barcode_exist";exit;
    }
}
else
{
  $uuid_short_barcode = $this->generateRandomString();
  $spec_set_info['barcode_number'] = $uuid_short_barcode;
}

$spec_value_combinations = $this->input->post("specs_values");
$spec_combination_count_check = count($this->input->post("specs_values"));
if($spec_combination_count_check>1) 
{
    echo "Please choose atmost one combination";exit();
}
if(empty($spec_value_combinations)) {
    echo "Choose item spec combination";
    exit();
}

if ($udata['items_name'] == '' || $udata['items_name'] == null) {
    echo "Give a item name";
    exit();
}

$items_id = $this->input->post('items_id');
$this->Item_model->update_item_info($items_id, $udata);

foreach ($spec_value_combinations as $key => $value) 
{
    $spec_values = '';
    foreach ($value as $spec_key => $combinations) 
    {
        $spec_values = $spec_values.'-'.$combinations['value'];    
    }


    if (!empty($_FILES['edit_item_image']['name'])) 
    {
        $config['upload_path'] = 'images/item_images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        // $config['max_size'] = '2000';
        // $config['max_width'] = '9999';
        // $config['max_height'] = '9999';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $error = '';
        $fdata = array();
        if (!$this->upload->do_upload('edit_item_image')) {
            $error = $this->upload->display_errors();
            echo $error;
        } else {
            $fdata = $this->upload->data();
            $spec_set_info['item_spec_set_image'] = $config['upload_path'] . $fdata['file_name'];
        }
    }

    $spec_set_info['items_id'] = $items_id;
    $spec_set_info['product_warranty'] = $this->input->post('product_warranty');
    $spec_set_info['spec_set'] = $udata['items_name'].'-'.$spec_values;
    if($this->input->post("is_unique")=="yes")
    {
        $spec_set_info['unique_barcode'] = "yes";
    }
    else
    {
        $spec_set_info['unique_barcode'] = "no";
    }
    $spec_set_info['user_id'] = $this->session->userdata('user_id');

    $this->Item_model->update_spec_set($item_spec_set_id, $spec_set_info);
    $insert_spec_set_id = $item_spec_set_id;
    $spec_name_ids = array();
    foreach ($value as $key2 => $combinations) 
    {
        array_push($spec_name_ids, $combinations['id']);
        $data['spec_name_id'] = $combinations['id'];
        $data['spec_value'] = $combinations['value'];
        $data['user_id'] = $this->session->userdata('user_id');
        $check_value_exist_id = $this->Item_model->check_if_value_exist($combinations['value']);
        if($check_value_exist_id) 
        {
            $specValueId = $check_value_exist_id['spec_value_id'];
        }
        else
        {
            $specValueId = $this->Item_model->insert_spec_value($data);
        }

        $spec_set_connector['item_spec_set_id'] = $insert_spec_set_id;
        $spec_set_connector['spec_name_id'] = $combinations['id'];
        $spec_set_connector['spec_value_id'] = $specValueId;
        $spec_set_connector['publication_status'] = "activated";
        $spec_set_connector['user_id'] = $this->session->userdata('user_id');


        $check_combination = $this->Item_model->check_spec_value_exist($spec_set_connector['item_spec_set_id'],$spec_set_connector['spec_name_id']);
        if($check_combination)
        {
            $update_combination_value = $this->Item_model->update_combination_spec_value($check_combination['spec_set_connector_id'],$spec_set_connector);
        }
        else
        {
         $insert_spec_set_connector = $this->Item_model->save_spec_set_connector($spec_set_connector);
     }
     $status = 1;
 }

 $dataa['publication_status'] = 'deactivated';
 $delete_extra_spect_combination_values = $this->Item_model->deactivate_extra_spect_combination_values($spec_set_connector['item_spec_set_id'],$spec_name_ids,$dataa);
}


if ($this->db->trans_status() === false) {
    $this->db->trans_rollback();
    echo "error transection";
}
else
{
    $this->db->trans_commit();
    if($status==1) 
    {
        echo 'Update Success';
    }
    else
    {
        echo 'failed';
    }
}




}



    /**
     * This function will delete/hide  the item's information by "items_id" from view but not from database. 
     * It will actually change the table field "publication_status" from "activated" to "deactivated".
     *
     * @return null
     * @param  string $items_id
     * @author shoaib <raselahmed2k7@gmail.com>
     **/
    

    public function delete_item($spec_set_id)
    {

        // $brands_id = $this->input->post('brands_id');
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) 
        {

            $check_inventory_exist = $this->Item_model->check_if_inventory_exist($spec_set_id);
            if($check_inventory_exist['quantity']>0)
            {
                echo 'inventory_exist';
            }
            else
            {
              if($this->Item_model->delete_item_info($spec_set_id)) 
              {
                echo "success";    
            }    
            else
            {
                echo "failed";
            }
        }
    }
    else
    {
        echo "No permission";

    }

}

    /**
     * This function collect all informations of individual item by parameter.
     * 
     * @param string $items_id
     *
     * @return array[]
     * @author shoaib <raselahmed2k7@gmail.com>
     **/

    public function get_item_info($spec_set_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
               'item_info_by_id' => $this->Item_model->item_info_by_id($spec_set_id),
               'spec_sets' => $this->Item_model->spec_set_by_id($spec_set_id),
               'all_specs_autoselected' => $this->Item_model->all_specs_info(),
           );
            echo json_encode($body_data);
        }
        else
        {
            echo json_encode("No permission");
        }
    }

    public function search_spec_by_name()
    {
        $text = $this->input->get('name');
        $spec_results = $this->Item_model->get_specs_by_search($text);
        echo json_encode($spec_results);
    }

    public function get_item_barcode_info($items_id)
    {

        $barcode = $this->Item_model->barcode_info_by_id($items_id);
        $barcode_number = $barcode['barcode_number'];
        // list($barcodeId, $extension) = explode('-'.$this->shop_id, $barcode['barcode_number']);
        $body_data= array(
         'barcode_number' => $barcode_number,
     );

        echo json_encode($body_data);
    }

    /* Code Single barcode print Starts*/
// public function print_barcode($barcode)
//     {

//         $item_spec_set = $this->Item_model->get_item_by_barcode($barcode);
//         $item_price = $this->Item_model->get_item_price($item_spec_set['item_spec_set_id']);
//         // $company_name = $this->Item_model->get_company_name();
//         if(!empty($item_price))
//         {
//             $data = array(
//                'barcode' => $barcode,
//                'print_quantity' => 1,
//                'items_name' => substr($item_spec_set['spec_set'],0,250),
//                'price' => 'Price : '.'Tk '.$item_price['retail_price'],
//              // 'company_name' => $company_name['name'],
//                );
//         }
//         else
//         {
//             $data = array(
//                'barcode' => $barcode,
//                'print_quantity' => 1,
//              // 'items_name' => $item_spec_set['spec_set'],
//                'items_name' => substr($item_spec_set['spec_set'],0,250),
//                'price' => 'Price : '.'Tk',
//              // 'company_name' => $company_name['name'],
//                );
//         }

//         $this->load->view('barcode/barcode_print_page_1_label_barcode', $data);
//     }
    /*Code for Single barcode print ends*/

    public function print_barcode($barcode,$print_quantity,$page_size, $company_name_add)
    {
        $item_spec_set = $this->Item_model->get_item_by_barcode($barcode);
        $item_price = $this->Item_model->get_item_price($item_spec_set['item_spec_set_id']);
        $company_name = $this->Item_model->get_company_name();
        if(!empty($item_price))
        {
            if($company_name_add == "Yes"){
                $data = array(
                   'barcode' => $barcode,
                   'print_quantity' => $print_quantity,
                   'items_name' => str_repeat('&nbsp;', 10)."<b>".$company_name['name']."</b>"."<br>".substr($item_spec_set['spec_set'],0,250),
                   'price' => 'Price : '.'Tk '.$item_price['retail_price'],
                   'company_name' => $company_name['name'],
               );
            }
            else{
                $data = array(
                   'barcode' => $barcode,
                   'print_quantity' => $print_quantity,
                   'items_name' => substr($item_spec_set['spec_set'],0,250),
                   'price' => 'Price : '.'Tk '.$item_price['retail_price'],
                   'company_name' => $company_name['name'],
               );
            }

        }
        else
        {
            if($company_name_add == "Yes"){
                $data = array(
                   'barcode' => $barcode,
                   'print_quantity' => $print_quantity,
                   'print_quantity' => $print_quantity,
                   'items_name' => str_repeat('&nbsp;', 10)."<b>".$company_name['name']."</b>"."<br>".substr($item_spec_set['spec_set'],0,250),
                   'price' => 'Price : '.'Tk',
                   'company_name' => $company_name['name'],
               );
            }
            else{
                $data = array(
                   'barcode' => $barcode,
                   'print_quantity' => $print_quantity,
                   'print_quantity' => $print_quantity,
                   'items_name' => substr($item_spec_set['spec_set'],0,250),
                   'price' => 'Price : '.'Tk',
                   'company_name' => $company_name['name'],
               );
            }

        }
        $this->load->view('barcode/barcode_print_page'.$page_size, $data);
    }

    public function multi_barcode_print_barcode()
    {
        $getdata = $this->input->post();
        $getdata["itemData"] = json_decode($getdata["itemData"],true);
        // echo "<pre>";
        // print_r($getdata);
        // exit();
        $data = array();
        $company_name = $this->Item_model->get_company_name();
        $company_name_add= $getdata['add_company'];
        $page_size = $getdata['page_type'];

        if($company_name_add == "Yes"){
            foreach ($getdata['itemData'] as $value) {
                for ($i=0; $i < $value['actual_quantity']; $i++) { 
                    $new_data = array(
                     'barcode' => $value['barcode_number'],
                     'items_name' => str_repeat('&nbsp;', 10)."<b>".$company_name['name']."</b>"."<br>".substr($value['item_name'],0,250),
                     'price' => 'Price : '.'Tk '.$value['selling_price'],
                     'company_name' => $company_name['name'],
                 );   
                    array_push($data,  $new_data);
                }
            }
            $sentData['sdata']=$data;
        }
        else{
            foreach ($getdata['itemData'] as $value) {
                for ($i=0; $i < $value['actual_quantity']; $i++) { 
                    $new_data = array(
                     'barcode' => $value['barcode_number'],
                     'items_name' => substr($value['item_name'],0,250),
                     'price' => 'Price : '.'Tk '.$value['selling_price'],
                     'company_name' => $company_name['name'],
                 );   
                    array_push($data,  $new_data);
                }
            }
            $sentData['sdata']=$data;
        }

        $this->load->view('barcode/multi_barcode_print_page'.$page_size, $sentData);
    }

    public function generateRandomString()
    {
        $shop_info = $this->Item_model->get_my_shop();
        $length = 6;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }
      
      $check_barcode_exist = $this->Item_model->check_barcode_if_exist($randomString);
      if($check_barcode_exist)
      {
        $this->generateRandomString();
    }
    else
    {  
        //$randomString = $randomString;  
        return $randomString;
    }
}

public function save_item_update_price_info(){
    $formData = $this->input->post();
    $item_spec_set_id = $formData['item_id'];

    $getLastBuyCartItemsId = $this->Item_model->getBuyCatItemsInfo($item_spec_set_id);


    $updateData = array(
        'buy_cart_items_id' => $getLastBuyCartItemsId,
        'whole_sale_price' => $formData['whole_sale_price'],
        'retail_price' => $formData['retail_price'],
    );

    $updatePrice = $this->Item_model->updateBuyCatItemsInfo($getLastBuyCartItemsId,$updateData);
    if($updatePrice > 0){
        echo json_encode("success");
    }
    else{
       echo json_encode("not_updated");   
   }
}

public function get_item_price_info($item_id){
    echo json_encode($this->Item_model->getItemPriceInfoById($item_id));
}

}

/* End of file Item.php */
/* Location: ./application/controllers/Item.php */