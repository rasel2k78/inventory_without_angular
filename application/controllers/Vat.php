<?php 

/**
 * VAT Controller
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    shoaib <shofik.shoaib@gmail.com>
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this controller for add new vat, edit vat , update vat info or delete vat info.
 *
 * @package Controller
 * @author  shoaib <shofik.shoaib@gmail.com>
**/

class vat extends CI_Controller
{

    /**
     * This is a constructor function
     *
     * This load Inventory Model when this controller is called.
     * 
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com> 
     **/


    public function __construct()
    {
        parent::__construct();

        $this->load->model('Vat_model');
        $this->load->model('User_access_check_model');
        $cookie = $this->input->cookie('language', true);
        $this->lang->load('vat_lang', $cookie);
        $this->lang->load('access_message_lang', $cookie);
        $this->lang->load('left_side_nev_lang', $cookie);
        userActivityTracking();
        
        $user_id = $this->session->userdata('user_id');
        if ($user_id == null) {

            redirect('Login', 'refresh');
        }

    }

    /**
     * This function is used for loading vat form.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
     **/

    public function index()
    {

        load_header();
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        


        $this->load->view('vat/add_vat_form');

        
        load_footer();    
    }

    /**
     * This function is used for AJAX datatable. This function load all data for datatable 
     *
     * @return array[] return data from database
     * @author shoaib <shofik.shoaib@gmail.com>
     **/


    public function all_vat_info_for_datatable()
    {        
        $filters = $this->input->get();


        $all_data = $this->Vat_model->all_vat_info($filters);
        $all_data_without_limit = $this->Vat_model->all_vat_info($filters, true);
        $all_data_final = $this->Vat_model->all_vat_info($filters, true);

        $output_data=[];

        $output_data["draw"]=$filters['draw'];
        $output_data["recordsTotal"]=$all_data_without_limit;
        $output_data["recordsFiltered"]=$all_data_final;
        $output_data["data"]=$all_data;

        echo json_encode($output_data);

    }


    /**
     * This function collect all informations of individual vat info by parameter.
     * 
     * @param string $vat_id
     *
     * @return array[]
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function get_vat_info($vat_id)
    {
        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {
            $body_data= array(
             'vat_info_by_id' => $this->Vat_model->vat_info_by_id($vat_id),
             );

            echo json_encode($body_data);

        }
        else {
            echo json_encode("No Permisssion");
        }

    }

    /**
     * This function is used for update vat's information in database.
     *
     * @return mix[] Return array if successfully data updated or "Error" if update failure.
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function update_vat_info()
    {

        $output=array();
        $output['success'] = 1;
        $output['error'] = array();

        if($this->input->post('vat_name_edit', true)==null ||  trim($this->input->post('vat_name_edit'))=="" ) {
            $output['error']['vat_name_edit'] = $this->lang->line('validation_name');
            $output['success'] = 0;
        }

        if($this->input->post('vat_percentage_edit') ==null ||  trim($this->input->post('vat_percentage_edit'))=="" || !is_numeric($this->input->post('vat_percentage_edit', true)) ) {
            $output['error']['vat_percentage_edit']= $this->lang->line('valid_percentage');
            $output['success']= 0;

        }
        
        if($output['success'] == 1) {
            $vat_id = $this->input->post('vat_id', true);
            $data=array(

             'vat_name' => trim($this->input->post('vat_name_edit', true)),
             'vat_percentage' => trim($this->input->post('vat_percentage_edit', true)),
             'vat_details' => trim($this->input->post('vat_details_edit', true)),
             'user_id' => $this->session->userdata('user_id'),
             );

            if($this->Vat_model->update_vat_info($vat_id, $data)) {
               echo json_encode($output);
           }
           else{

             $output['success']  = 0;
             echo json_encode($output);
         }

     }
     else{

        echo json_encode($output);
    }
}

    /**
     * This function save all vat informations  in database.
     *
     * @return null
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function save_vat_info()
    {

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            $output = array();
            $output['success'] = 1;
            $output['error']= array();

            if($this->input->post('vat_name') ==null ||  trim($this->input->post('vat_name'))=="" ) {
                $output['error']['vat_name']= $this->lang->line('validation_name');;
                $output['success']= 0;

            }
            if($this->input->post('vat_percentage') ==null ||  trim($this->input->post('vat_percentage'))=="" || !is_numeric($this->input->post('vat_percentage', true)) ) {
                $output['error']['vat_percentage']= $this->lang->line('valid_percentage');;
                $output['success']= 0;

            }

            if($output['success']==0) {
                echo json_encode($output);
                exit();
            }
            else{

                $vat_row_count = $this->Vat_model->count_rows();

                if($vat_row_count > 0) {
                    $data= array(

                     'vat_name' => trim($this->input->post('vat_name', true)),
                     'vat_details' => trim($this->input->post('vat_details', true)),
                     'vat_percentage' => trim($this->input->post('vat_percentage', true)),
                     'selection_status' => 'not_selected',
                     'user_id' => $this->session->userdata('user_id'),
                     );

                    $output['data'] = $this->Vat_model->save_vat_info($data);
                    echo json_encode($output);
                }

                else{

                    $data= array(

                     'vat_name' => trim($this->input->post('vat_name', true)),
                     'vat_details' => trim($this->input->post('vat_details', true)),
                     'vat_percentage' => trim($this->input->post('vat_percentage', true)),
                     'selection_status' => 'selected',
                     'user_id' => $this->session->userdata('user_id'),
                     );

                    $output['data'] = $this->Vat_model->save_vat_info($data);
                    echo json_encode($output);
                }
            }
        }
        else {
            echo json_encode("No Permisssion");
        }

    }


    /**
     * This function will delete/hide  the vat's information by "vat_id" from view but not from database. 
     * It will actually change the "publication_status" from "activated" to "deactivated"
     *
     * @return void
     * @param  string $vat_id
     * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function delete_vat($vat_id)
    {

        $user_id_logged = $this->session->userdata('user_id');
        $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
        if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

            if($this->Vat_model->delete_vat_info($vat_id)) {
                echo "success";    
            }    
            else{
                echo "failed";
            }

        }
        else {
            echo "No Permisssion";
        }

    }


    /**
 * This function actually used to activa a vat type. Selected vat type will be activated and rest other will be deactivated automatically.
 *
 *@param  string $vat_id
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function activate_vat($vat_id)
    {

     $user_id_logged = $this->session->userdata('user_id');
     $pages_address = $this->router->fetch_class()."/".$this->router->fetch_method();
     if($this->User_access_check_model->check_access($user_id_logged, $pages_address)) {

        $this->Vat_model->change_status_of_vat();

        if($this->Vat_model->activate_vat_info($vat_id)) {
            echo "success";
        }

        else {
            echo "failed";
        }

    }
    else {
        echo "No Permission";
    }
}


}

/* End of file expenditure_type.php */
/* Location: ./application/controllers/expenditure_type.php */