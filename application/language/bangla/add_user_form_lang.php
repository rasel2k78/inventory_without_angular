<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['header_text_left'] = 'নতুন ব্যবহারকারী';
$lang['username'] = 'ব্যবহারকারীর নাম';
$lang['user_role'] = 'ব্যবহারকারীর ভূমিকা';
$lang['staff'] = 'স্টাফ';
$lang['manager'] = 'ম্যানেজার';
$lang['password'] = 'পাসওয়ার্ড';
$lang['repass'] = 'পুনরায় পাসওয়ার্ড';
$lang['email'] = 'ইমেইল';
$lang['phone'] = 'মোবাইল ১';
$lang['phone2'] = 'মোবাইল ২';
$lang['present_address'] = 'বর্তমান ঠিকানা';
$lang['permanent_address'] = 'স্থায়ী ঠিকানা';
$lang['user_image'] = 'ব্যবহারকারীর ছবি';
$lang['user_image_select'] = 'ছবি নির্বাচন করুন';
$lang['change'] = 'পরিবর্তন';
$lang['delete'] = 'ডিলিট';
$lang['save'] = 'সেভ';
$lang['cancel'] = 'বাতিল';

$lang['delete_success'] = 'সফলভাবে তথ্য ডিলিট হয়েছে';
$lang['insert_failure'] = 'দুঃখিত !! তথ্য সেভ হয়নি';
$lang['user_insert_success'] = 'সফলভাবে তথ্য সেভ হয়েছে';
$lang['update_success'] = 'সফলভাবে তথ্য আপডেট হয়েছে';
$lang['no_username'] = 'ব্যবহারকারীর নাম দেওয়া হয়নি';
$lang['no_mobile'] = 'মোবাইল নামাবার দেওয়া হয়নি';
$lang['no_password'] = 'পাসওয়ার্ড দেওয়া হয়নি';
$lang['no_match'] = 'পাসওয়ার্ডের অসামঞ্জস্য';
$lang['email_exist'] = 'এই ইমেইল আগে ব্যবহার হয়েছে';
$lang['username_exist'] = 'এই ব্যবহারকারীর নাম আগে ব্যবহার হয়েছে';
$lang['short_pass'] = 'পাসওয়ার্ড কম্পক্ষে চার অক্ষরের হতে হবে';
$lang['no_permission_u_create'] = 'আপনার অনুমতি নেই';


$lang['user_deleted'] = 'সফলভাবে তথ্য ডিলিট হয়েছে';
$lang['user_not_deleted'] = 'দুঃখিত !! তথ্য ডিলিট হয়নি';
$lang['no_permission_right'] = 'আপনার অনুমতি নেই';
$lang['all_user_list'] = 'সকল ব্যবহারকারী';
$lang['name_th'] = 'নাম';
$lang['mobile_th'] = 'মোবাইল';
$lang['edit_delete_th'] = 'পরিবর্তন / ডিলিট';
$lang['manage_u_acc'] = 'ব্যবহারকারীর প্রবেশাধিকার';
$lang['sure_to_delete'] = 'ব্যবহারকারীর তথ্য ডিলিট';
$lang['confirm_delete_user'] = 'আপনি কি ডিলিট করতে চান';
$lang['no'] = 'না';
$lang['yes'] = 'হ্যাঁ';

$lang['change_user_info'] = 'ব্যবহারকারীর তথ্য পরিবর্তন';

$lang['change_user_image'] = 'ব্যবহারকারীর ছবি (অপরিবর্তন যোগ্য)';

$lang['infor_update'] = 'তথ্য হালনাগাদ';



$lang['username_tt'] = 'ব্যবহারকারীর নাম প্রদান করুন';
$lang['user_role_tt'] = 'ব্যবহারকারীর ভূমিকা নির্বাচন করুন';
$lang['staff_tt'] = 'স্টাফ';
$lang['manager_tt'] = 'ম্যানেজার';
$lang['password_tt'] = 'পাসওয়ার্ড প্রদান করুন';
$lang['repass_tt'] = 'পুনরায় পাসওয়ার্ড প্রদান করুন';
$lang['email_tt'] = 'ইমেইল প্রদান করুন';
$lang['phone_tt'] = 'মোবাইল নাম্বার ১ প্রদান করুন';
$lang['phone2_tt'] = 'মোবাইল নাম্বার ২ প্রদান করুন';
$lang['present_address_tt'] = 'বর্তমান ঠিকানা প্রদান করুন';
$lang['permanent_address_tt'] = 'স্থায়ী ঠিকানা প্রদান করুন';

$lang['no_email'] = 'ইমেইল দেওয়া হয়নি';
