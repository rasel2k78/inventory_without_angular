<?php 

/**
 *English Language file for Nagadanboi controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "নতুন ভ্যাট যোগ করুন";
$lang['right_portlet_title'] = "ভ্যাটের তালিকা";
$lang['left_expense_type_name'] = "নাম";
$lang['percentage']="ভ্যাটের পরিমাণ";
$lang['vat_percentage']="ভ্যাটের পরিমাণ দিন";
$lang['left_expense_type_name_placeholder'] = "নাম দিন";
$lang['left_expense_description'] = "বিবরণ";
$lang['left_expense_description_placeholder'] = "বিবরণ দিন";
$lang['left_save'] = "সেভ করুন";
$lang['left_cancle'] = "বাতিল করুন";
$lang['right_expense_type'] = "ধরন";
$lang['right_description'] = "বিবরণ";
$lang['right_date'] = "তারিখ";
$lang['right_edit_delete'] = "পরিবর্তন / ডিলিট / সক্রিয়";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";

$lang['alert_activate']="সক্রিয়তার নিশ্চিতকরণঃ";
$lang['alert_activate_details']="আপনি কি এই ভ্যাটের পরিমাণ সক্রিয় করতে চান ?";


$lang['alert_active_success']="ভ্যাট সক্রিয় হয়েছে";
$lang['alert_active_failed']="দুঃখিত!! ভ্যাট সক্রিয় হয়নি";

$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";

$lang['alert_access_failure']="";
$lang['alert_delete_success']="সফলভাবে ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !! ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! সেভ হয়নি";


$lang['edit_header']="ভ্যাটের তথ্য পরিবর্তন করুন";
$lang['validation_name']="ভ্যাটের নাম দিন";

$lang['valid_percentage']="ভ্যাটের পরিমাণ দিন";


$lang['model_edit']="পরিবর্তন";
$lang['model_delete']="ডিলিট";

$lang['left_expense_type_name_new']="নতুন ভ্যাট যোগ করুন";
$lang['cancel']="বাতিল করুন";

$lang['right_percentage']="পরিমাণ";
