<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title_receive'] = 'পণ্য হস্তান্তর তথ্য';


$lang['transfer_no'] = 'পণ্য হস্তান্তর নাম্বার';
$lang['item'] = 'পণ্য';
$lang['total_quantity'] = 'মোট পরিমাণ';
$lang['total_price'] = 'মোট মূল্য';
$lang['description'] = 'বিবরণ';
$lang['selling_price'] = 'বিক্রয় মূল্য';

$lang['transfer_receive_list'] = 'পণ্য গ্রহণের তালিকা';
$lang['transfer_from'] = 'স্থানান্তর থেকে ';
$lang['price'] = 'মূল্য';
$lang['status'] = 'অবস্থা';                 

$lang['details'] = 'বিস্তারিত';
$lang['transfer_items'] = 'স্থানান্তর পণ্যের তালিকা';
$lang['item_on_inventory'] = 'মজুদে পণ্যের';
$lang['search_item'] = 'পণ্য  নির্বাচন করুন';
$lang['match_item'] = 'পণ্য ম্যাচ করুন';
$lang['adjust_item'] = 'পণ্যের সামঞ্জস্য';
$lang['pic_please'] = 'প্রথমে আপনার মজুদ থেকে একটি পণ্য খুঁজুন এই পণ্যের মত';
$lang['pic_item'] = 'প্রতি সারির জন্যে আপনার মজুদ থেকে পণ্য খুঁজে পাশের বাটন এ ক্লিক করুন এবং সব শেষে এখানে ক্লিক করুন';

$lang['item_invalid_price'] = 'পণ্যের মূল্য ঠিক নেই';
$lang['give_item_quntity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['give_item_price'] = 'পণ্যের মূল্য দিন';
$lang['give_appropriate_amount'] = 'পণ্যের সঠিক মূল্য দিন';
$lang['match_item_tt'] = 'আপনার কাছে যে পণ্যগুলো আসছে ওগুলোর সাথে মিল রেখে পণ্য বের করে পাশের বাটন এ ক্লিক করুন';
$lang['item_on_tt'] = 'আপনার মজুদের পণ্য যা আপনার কাছে আসার পণ্যের সাথে মিল আছে';
$lang['adjust_click_tt'] = 'এই পণ্যের সাথে মিল রেখে আপনার পণ্য উপরে থেকে নির্বাচন করে এই বাটন এ ক্লিক করুন';


$lang['finish'] = 'সফলভাবে পণ্য গৃহীত শেষ  হয়েছে ';
$lang['update'] = 'হালনাগাদ';
$lang['sorry'] = 'দুঃখিত';
$lang['completed'] = 'সম্পন্ন ?';
$lang['item_details'] = 'পণ্যের বিবরণ';
$lang['confirm_delete'] = 'হস্তান্তর গ্রহণ করতে চান ?';

$lang['want_to_update'] = 'হস্তান্তর গ্রহণ করতে চান ? | সমপরিমাণ টাকা আপনার অ্যাকাউন্ট থেকে কাটা যাবে !!';
$lang['no'] = 'না';
$lang['yes'] = 'হ্যাঁ';

$lang['receive'] = 'গ্রহন করুন';
$lang['cancle'] = 'বাতিল করুন';
/* End of file transfer_receive_item_lang.php */
/* Location: ./application/language/bangla/transfer_receive_item_lang.php */