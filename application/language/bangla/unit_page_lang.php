<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'নতুন ইউনিট যোগ করুন!';
$lang['right_portlet_title'] = 'সকল ইউনিটের তালিকা';
$lang['unit_name'] = 'ইউনিটের নাম';
$lang['unit_name_placeholder'] = 'ইউনিটের নাম';
$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['Buy advance'] = 'বাতিল করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['unit_name_required'] = 'ইউনিটের নাম দিন';
$lang['want_to_delete'] = 'আপনি কি ইউনিটের এই তথ্য ডিলিট করতে চান ?';
$lang['delete_success'] = 'সফলভাবে  ক্যাটাগরির  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  ক্যাটাগরির  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  ক্যাটাগরির  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  ক্যাটাগরির  তথ্য আপডেট হয়েছে';
$lang['edit_unit'] = 'ইউনিটের তথ্য পরিবর্তন করুন';
$lang['no_unit'] = 'ইউনিটের নাম দিন';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */