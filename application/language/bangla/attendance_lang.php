<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['box_header_left']="Attendance entry";
$lang['box_header_right']="Attendance history";
$lang['employee_name']="Employee name";
$lang['date']="Date";
$lang['in_time']="In time";
$lang['out_time']="Out time";
$lang['remark']="Remark";

$lang['name_th']="Name";
$lang['date_th']="Date";
$lang['in_time_th']="In time";
$lang['out_time_th']="Out time";
$lang['action_th']="Edit";


