<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'পণ্য হস্তান্তর করুন';
$lang['shop_in'] = 'দোকান হতে';
$lang['shop_to'] = 'দোকানে';
$lang['select_item'] = 'পণ্য নির্বাচন করুন';
$lang['select_shop'] = 'দোকান নির্বাচন করুন';
$lang['item_quantity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['selling_price_unit'] = 'বিক্রয় মূল্য /ইউনিট (খুচরা)';
$lang['item_transfer_info'] = 'পণ্য হস্তান্তর তথ্য';
$lang['add_item'] = 'যোগ করুন';
$lang['item_f'] = 'পণ্য';
$lang['sell_price_single'] = 'খুসরা মূল্য ';
$lang['selling_price_f'] = 'বিক্রয় মূল্য';
$lang['total_price'] = 'সর্বমোট মূল্য';
$lang['transfer_item_tt'] = 'হস্তান্তরের জন্যে পণ্য নির্বাচন করুন';
$lang['your_shop_tt'] = 'আপনার দোকান';
$lang['shop_to_tt'] = 'যে দোকানে হস্তান্তর করবেন';
$lang['transfer_list'] = 'পণ্য পাঠানোর তালিকা';
$lang['transfer_to'] = 'স্থানান্তর থেকে';
$lang['status'] = 'অবস্থা';
$lang['finish_transfer'] = 'সফলভাবে পণ্য হস্তান্তর হয়েছে ';
$lang['details'] = 'বিস্তারিত';
$lang['update'] = 'হালনাগাদ';
$lang['sorry'] = 'দুঃখিত';
$lang['completed'] = 'সম্পন্ন ?';
$lang['item_details'] = 'পণ্যের বিবরণ';
$lang['transfer_items'] = 'স্থানান্তর পণ্যের তালিকা';
$lang['price'] = 'মূল্য';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['want_to_update'] = 'হস্তান্তর গৃহীত হয়েছে | আপনি মজুদ আপডেট করতে চান ?';
$lang['no'] = 'না';
$lang['yes'] = 'হ্যাঁ';
$lang['sorry'] = 'দুঃখিত';
$lang['check_internet'] = 'ইন্টারনেট সংযোগ পরীক্ষা করুন !!';
$lang['not_enough_item'] = 'পণ্য মজুদ নেই';

$lang['item_invalid_price'] = 'পণ্যের মূল্য ঠিক নেই';
$lang['give_item_quntity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['give_item_price'] = 'পণ্যের মূল্য দিন';
$lang['give_appropriate_amount'] = 'পণ্যের সঠিক মূল্য দিন';

$lang['pull_form'] = 'পণ্য সংগ্রহ করুন';
$lang['get_items'] = 'পণ্য টান দিন';
$lang['received_successfully'] = 'সব পণ্য আপনার দোকানে গৃহীত হয়েছে';

$lang['sub_total_f'] = 'সাব-টোটাল';
$lang['total_quantity_f'] = 'মোট পরিমাণ';


$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/return_money_form_lang.php */