<?php
/**
 *Bangla Language file for Contact Us page.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['help_support']="হেল্প এবং সাপোর্ট ";
$lang['help_details']="যে কোনো প্রয়োজনে যোগাযোগ করুনঃ ";
$lang['person_one']="শামসুর রাহমান - ০১৯৬৬ ৬৬২৮৭০";
$lang['person_two']="রাকিব খান - ০১৭০১ ২২৬৫৪৯";