<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['add_new_customer'] = 'নতুন কাস্টমার যোগ করুন';
$lang['all_customer'] = 'সকল কাস্টমারের তালিকা';

$lang['customer_name'] = 'কাস্টমারের নাম';
$lang['customer_id'] = 'কাস্টমারের আই ডি';
$lang['customer_email'] = 'কাস্টমারের ইমেইল';

$lang['customer_present_address'] = 'কাস্টমারের বর্তমান ঠিকানা';
$lang['customer_permanent_address'] = 'কাস্টমারের স্থায়ী ঠিকানা';
$lang['customer_phone'] = 'কাস্টমারের মোবাইল নাম্বার ';
$lang['customer_phone_2'] = 'কাস্টমারের মোবাইল নাম্বার ২ ';
$lang['customer_nid'] = 'জাতীয় পরিচয়পত্র নম্বর';
$lang['customer_image'] = 'কাস্টমারের ছবি';
$lang['net_alert'] = 'Sample';
$lang['select_image'] = 'ছবি নির্বাচন করুন';
$lang['change'] = 'পরিবর্তন';
$lang['delete'] = 'ডিলিট';
$lang['save'] = 'সেভ করুন';
$lang['cancel'] = 'বাতিল করুন';
$lang['name'] = 'নাম';
$lang['mobile'] = 'মোবাইল';
$lang['change_delete'] = 'পরিবর্তন / ডিলিট';
$lang['yes'] = 'হ্যাঁ';
$lang['no'] = 'না';
$lang['id_exist_alert'] = 'কাস্টমারের আইডি ইতিমধ্যে বিদ্যমান';


$lang['no_permission_left'] = 'আপনার অনুমতি নেই';
$lang['delete_success'] = 'সফলভাবে তথ্য ডিলিট হয়েছে';
$lang['insert_failure'] = 'দুঃখিত !! তথ্য সেভ হয়নি';
$lang['user_insert_success'] = 'সফলভাবে তথ্য সেভ হয়েছে';
$lang['update_success'] = 'সফলভাবে তথ্য সেভ হয়েছে';
$lang['no_username'] = 'কাস্টমারের নাম দেওয়া হয়নি';

$lang['do_you_want_delete'] = 'আপনি কি এই কাস্টমার ডিলিট করতে চান?';

$lang['deletion_success'] = 'সফলভাবে তথ্য ডিলিট হয়েছে';
$lang['no_deletion'] = 'দুঃখিত !! তথ্য ডিলিট হয়নি';

$lang['change_customer_info'] = 'কাস্টমারের তথ্য পরিবর্তন';
$lang['update_info'] = 'তথ্য হালনাগাদ';
$lang['image_of_customer'] = 'কাস্টমারের ছবি';

$lang['customer_exist'] = 'এই ইমেইল দিয়ে একজন কাস্টমার বর্তমানে আছেন';
$lang['customer_insert_success'] = 'সফলভাবে তথ্য সেভ হয়েছে';


$lang['no_phone']="মোবাইল নাম্বার দিন";
$lang['no_username_customer'] = 'কাস্টমারের নাম দেওয়া হয়নি';
$lang['customer_already_exists'] = 'কাস্টমার আগে থেকেই আছে';
