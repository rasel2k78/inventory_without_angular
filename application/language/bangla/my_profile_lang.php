<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['change_information'] = 'তথ্য পরিবর্তন';
$lang['name'] = 'নাম';
$lang['email'] = 'ইমেইল';
$lang['mobile1'] = 'মোবাইল ১';
$lang['mobile2'] = 'মোবাইল ২';
$lang['present_address'] = 'বর্তমান ঠিকানা';
$lang['permanent_address'] = 'স্থায়ী ঠিকানা';
$lang['image'] = 'ছবি';
$lang['select_image'] = 'ছবি নির্বাচন করুন';
$lang['change_image'] = 'ছবি পরিবর্তন';
$lang['cancel'] = 'বাতিল করুন';
$lang['update_information'] = 'তথ্য হালনাগাদ';
$lang['change_password'] = 'পাসওয়ার্ড পরিবর্তন';
$lang['old_password'] = 'পুরোনো পাসওয়ার্ড';
$lang['new_password'] = 'নতুন পাসওয়ার্ড';
$lang['new_password_again'] = 'পুনরায় নতুন পাসওয়ার্ড';
$lang[''] = '';
$lang[''] = '';


$lang['pass_update_failure'] = 'দুঃখিত !!  পাসওয়ার্ড আপডেট হয়নি';
$lang['pass_update_success'] = 'সফলভাবে পাসওয়ার্ড আপডেট হয়েছে';
$lang['new_pass_not_matched'] = 'দুঃখিত !!  নতুন পাসওয়ার্ড দুটি ভিন্ন হয়েছে';
$lang['old_pass_not_matched'] = 'দুঃখিত !!  পুরোনো পাসওয়ার্ড ভুল হয়েছে';
$lang['empty_pass'] = 'পাসওয়ার্ড এর বক্স খালি থাকতে পারবে না';
$lang['short_pass'] = 'পাসওয়ার্ড কম্পক্ষে চার অক্ষরের হতে হবে';


$lang['update_failure'] = 'দুঃখিত !!  তথ্য আপডেট হয়নি';
$lang['update_success'] = 'সফলভাবে তথ্য আপডেট হয়েছে';
$lang['no_username'] = 'নাম দেওয়া হয়নি';
$lang['no_mobile'] = 'মোবাইল নামাবার দেওয়া হয়নি';
$lang['no_email'] = 'ইমেইল দেওয়া হয়নি';




























