<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_sales_rep'] = 'নতুন বিক্রয় প্রতিনিধি যোগ করুন';
$lang['all_sales_rep'] = 'সকল বিক্রয় প্রতিনিধি তালিকা';
$lang['sales_rep_name'] = 'নাম';

$lang['sales_rep_email'] = 'ইমেইল';

$lang['sales_rep_present_address'] = 'বর্তমান ঠিকানা';
$lang['sales_rep_permanent_address'] = 'স্থায়ী ঠিকানা ';
$lang['sales_rep_phone'] = 'মোবাইল';
$lang['sales_rep_phone_2'] = 'মোবাইল ২';
$lang['sales_rep_nid'] = 'জাতীয় পরিচয় পত্র ';
$lang['sales_rep_image'] = 'ছবি';
$lang['net_alert'] = 'Sample';

$lang['select_image'] = 'ছবি নির্বাচন করুন ';
$lang['change'] = 'পরিবর্তন';
$lang['delete'] = 'ডিলিট';

$lang['save'] = 'সেভ করুন';
$lang['cancel'] = 'বাতিল ';

$lang['name'] = 'নাম';
$lang['mobile'] = 'মোবাইল ';
$lang['date'] = 'তারিখ';


$lang['change_delete'] = 'পরিবর্তন / ডিলিট ';

$lang['yes'] = 'হ্যা';
$lang['no'] = 'না ';


$lang['delete_success'] = 'সফলভাবে ডিলিট হয়েছে';
$lang['no_permission_left'] = 'আপনার অনুমতি নেই ';
$lang['insert_failure'] = 'দুঃখিত! আবার চেষ্টা করুন ';
$lang['user_insert_success'] = 'সফলভাবে বিক্রয় প্রতিনিধি যোগ হয়েছে ';
$lang['update_success'] = 'সফলভাবে তথ্য হালনাগাদ হয়েছে ';
$lang['no_username'] = 'বিক্রয় প্রতিনিধির নাম দিন ';
$lang['do_you_want_delete'] = 'আপনি কি এই তথ্য ডিলিট করতে চান ?';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';

$lang['deletion_success'] = 'সফলভাবে ডিলিট হয়েছে ';
$lang['no_deletion'] = 'দুঃখিত! ডিলিট হয়নি';


$lang['change_sales_rep_info'] = 'Change Representative\'s information';
$lang['update_info'] = 'Update info';
$lang['image_of_sales_rep'] = 'Image';

$lang['sales_rep_exist'] = 'Email already exist';


$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";

$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";

$lang['alert_delete_success']="সফলভাবে ডিলিট হয়েছে ";
$lang['alert_delete_failure']="দুঃখিত! ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে  আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !!  আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !!  সেভ হয়নি";

$lang['edit_header']="বিক্রয় প্রতিনিধির তথ্য পরিবর্তন করুন";
$lang['no_name']='বিক্রয় প্রতিনিধির নাম দিন';

$lang['alert_access_failure']="দুঃখিত!! আপনার অনুমতি নেই।";