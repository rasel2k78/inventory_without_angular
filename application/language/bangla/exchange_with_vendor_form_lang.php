<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'পরিবর্তিত পণ্য ক্রয়ের তথ্য';
$lang['voucher_no'] = 'রসিদ নং';
$lang['item'] = 'পণ্য';
$lang['quantity'] = 'পরিমাণ';
$lang['total_quantity'] = 'মোট পরিমাণ';
$lang['buying_price'] = 'ক্রয় মূল্য';
$lang['sub_total'] = 'সাব-টোটাল';
$lang['Delete_items'] = 'ডিলিট করুন';
$lang['total'] = 'সর্বমোট:';
$lang['new_item_buy_info'] = 'নতুন পণ্য ক্রয়ের তথ্য:';
$lang['total_new_amount'] = 'নতুন পণ্য ক্রয়ের মূল্য';
$lang['receivable'] = 'প্রাপ্য';
$lang['payable'] = 'প্রদেয়';
$lang['Due'] = 'বাকি';
$lang['total_payable'] = 'মোট প্রদেয়';
$lang['sorry'] = 'দুঃখিত';

$lang['total_paid'] = 'মোট পরিশোধ :';
$lang['exchange_with_vendor_info'] = 'সরবরাহকারীর সাথে পণ্য পরিবর্তনের তথ্য';
$lang['find'] = 'খুজুন';
$lang['item_buy_details'] = 'পণ্য ক্রয়ের বিবরণ';
$lang['item_name'] = 'পণ্যের নাম';
$lang['buying_price_unit'] = 'ক্রয় মূল্য /ইউনিট';
$lang['exchange_quantity_tt'] = 'পরিবর্তনের সংখ্যা প্রদান করুন';
$lang['give_buying_price'] = 'পণ্যের ক্রয়মূল্য প্রদান করুন';
$lang['get_voucher_tt'] = 'রসিদের তথ্য বাহির করতে রসিদ নাম্বার প্রদান করুন';
$lang['select_item_tt'] = 'পরিবর্তন করতে নতুন পণ্য নির্বাচন করুন';
$lang['item_list_tt'] = 'পরিবর্তিত পণ্যের তালিকা';
$lang['total_price_tt'] = 'পরিবরতিত পণ্যের মোট মূল্য';
$lang['item_new_tt'] = 'পরিবরতনের পর নতুন পণ্যসমূহ';
$lang['item_new_total_price_tt'] = 'নতুন পণ্যসমূহের সর্বমোট মূল্য';
$lang['item_new_paid_tt'] = 'পণ্য পরিবরতনের পর পরিশোধিত পরিমাণ';
$lang['amount_exchange_tt'] = 'নতুন পণ্য পরিবরতনের পর মোট পরিমাণ';
$lang['amount_after_paid_tt'] = 'পণ্য পরিবরতনের পর অবশিষ্ট পরিশোধের পর মোট পরিমাণ';
$lang['new_quantity'] = 'ফেরতক্রিত পণ্যের পরিমাণ';

$lang['total_bill'] = 'মোট বিল';
$lang['select_item'] = 'পণ্য নির্বাচন করুন';
$lang['give_item_quantity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['sell_price_unit'] = 'বিক্রয় মূল্য /ইউনিট (খুচরা)';
$lang['single_price'] = 'খুসরা মূল্য';
$lang['whole_price'] = 'পাইকারি মূল্য';
$lang['sell_price_whole'] = 'বিক্রয় মূল্য /ইউনিট (পাইকারি)';
$lang['deadline'] = 'মেয়াদ উত্তীর্ণের তারিখ';
$lang['add_new_item'] = 'নতুন পণ্য যোগ করুন';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['not_exchanged'] = 'কোন পণ্য পরিবরতন করেননি';
$lang['exchange_success'] = 'সফলভাবে পণ্য পরিবর্তন  হয়েছে';
$lang['no_new_item'] = 'কোন নতুন পণ্য যোগ করেননি';
$lang['no_voucher'] = 'রসিদ খুঁজে পায়নি';
$lang['no_items'] = 'রসিদে পণ্য নেই';
$lang['exchanged_quantity_exceded'] = 'পরিবর্তিত পরিমাণ অতিরিক্ত হয়ে গেছে';

$lang['give_voucher_number'] = 'পরিবর্তনের জন্য এবং রশিদের বিস্তারিত জানতে রশিদ নাম্বার প্রদান করুন';

$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['want_to_delete'] = 'আপনি কি লট সাইজের এই তথ্য ডিলিট করতে চান ?';
$lang['delete_success'] = 'সফলভাবে  লট সাইজের  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  লট সাইজের  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য আপডেট হয়েছে';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */