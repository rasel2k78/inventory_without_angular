<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'নতুন সাইজ যোগ করুন!';
$lang['right_portlet_title'] = 'সকল সাইজের তালিকা';
$lang['Size_name'] = 'সাইজের নাম';
$lang['edit_size_name'] = 'সাইজের তথ্য পরিবর্তন করুন';
$lang['Size_name_placeholder'] = 'সাইজের নাম';
$lang['delete_success'] = 'সফলভাবে  সাইজের  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  সাইজের  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  সাইজের  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  সাইজের  তথ্য আপডেট হয়েছে';
$lang['Edit'] = 'পরিবর্তন';
$lang['Delete'] = 'ডিলিট';
$lang['want_to_delete'] = 'আপনি কি সাইজের এই তথ্য ডিলিট করতে চান ?';
$lang['Delete_confirm'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';

$lang['validation_name'] = "সাইজের নাম দিন";

/* End of file size_page_lang.php */
/* Location: ./application/language/english/size_page_lang.php */