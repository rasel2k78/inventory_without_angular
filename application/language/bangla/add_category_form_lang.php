<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] = 'নতুন ক্যাটাগরি যোগ করুন';
$lang['right_portlet_title'] = 'সকল ক্যাটাগরির তালিকা';
$lang['delete_success'] = 'সফলভাবে  ক্যাটাগরির  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  ক্যাটাগরির  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  ক্যাটাগরির  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  ক্যাটাগরির  তথ্য আপডেট হয়েছে';
$lang['edit_category'] = 'ক্যাটাগরির তথ্য পরিবর্তন করুন';
$lang['category_name'] = 'ক্যাটাগরির  নাম';
$lang['category_description'] = 'ক্যাটাগরির বিবরণ ';
$lang['main_category_name'] = 'মূল ক্যাটাগরি (যদি থাকে)';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['select_category'] = 'ক্যাটাগরি নির্বাচন করুন';
$lang['edit_category'] = 'ক্যাটাগরির তথ্য পরিবর্তন করুন';
$lang['want_to_delete'] = 'আপনি কি  এই তথ্য ডিলিট করতে চান ?';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যা';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['validation_name'] = "ক্যাটাগরির নাম দিন";
/* End of file add_category_form_lang.php */
/* Location: ./application/language/english/add_category_form_lang.php */