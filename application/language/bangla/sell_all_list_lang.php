<?php 
/**
 *Bangla Language file for Sell_list_all controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="পণ্য বিক্রয়ের তালিকা";
$lang['right_portlet_title'] ="পণ্য বিক্রয়ের বিবরণ";
$lang['left_customer']="ক্রেতা";
$lang['left_voucher_no'] ="ক্রয়ের রসিদ নং";
$lang['left_date'] ="ক্রয়ের তারিখ";
$lang['left_details_delete'] ="বিস্তারিত / ডিলিট";
$lang['right_voucher_no'] ="রসিদ নং";
$lang['right_net_payable'] ="প্রদেয়";
$lang['right_paid'] ="মোট পরিশোধ";
$lang['right_due'] ="মোট বাকি";
$lang['right_item_name'] ="পণ্যের নাম";
$lang['right_quantity'] ="পরিমাণ";
$lang['right_price'] ="বিক্রয় মূল্য /ইউনিট";
$lang['right_sub_total'] ="সাব-টোটাল";
$lang['alert_delete_success'] ="সফলভাবে বিক্রয়ের তথ্য ডিলিট হয়েছে";
$lang['alert_delete_failed'] ="দুঃখিত!! বিক্রয়ের তথ্য ডিলিট হয়নি ";
$lang['right_grand_total'] ="মোট বিল";
$lang['right_discount'] ="মূল্যছাড়";
$lang['customer_phone'] ="ক্রেতার ফোন নাম্বার";
$lang['alert_delete_confirmation'] ="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details'] ="আপনি কি  এই তথ্য ডিলিট করতে চান ?";
$lang['alert_delete_yes'] ="হ্যা";
$lang['alert_delete_no'] ="না ";
$lang['no_customer']="খালি";
$lang['voucher_print']="প্রিন্ট";
$lang['right_dis_type']="ছাড়ের ধরন ";
$lang['right_dis_amount']="ছাড়ের পরিমাণ";
$lang['imei_or_serial']="সিরিয়াল";