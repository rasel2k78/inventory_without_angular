<?php 
/**
 *Bangla Language file for Inventory Model.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['model_edit']="পরিবর্তন";
$lang['model_delete']="ডিলিট";
$lang['model_paymentmade']="বাকি পরিশোধ";
$lang['model_details']="বিস্তারিত";
$lang['model_payout']="পরিশোধ";
$lang['model_adjust']="সমন্বয়";
$lang['model_active']="সক্রিয়";
$lang['model_deactive']="নিষ্ক্রিয়";
$lang['model_empty']="খালি";
$lang['cleared_by_cash']="ক্যাশ ";
$lang['cleared_by_acc']="ব্যাংক অ্যাকাউন্ট ";