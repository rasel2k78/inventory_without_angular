<?php 
/**
 *Bangla Language file for Loan controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_header']= "পণ্য নষ্ট / হারিয়ে যাবার তথ্য লিপিবদ্দ করুন";
$lang['left_type']="ধরন" ;
$lang['demage']="নষ্ট হওয়া";
$lang['lost']="হারিয়ে যাওয়া ";
$lang['left_item']= "পণ্য";
$lang['left_item_select']= "পণ্য  নির্বাচন করুন";
$lang['left_quantity']= "পরিমাণ";
$lang['left_description']= "বিবরণ";
$lang['left_save']= "সেভ করুন";
$lang['left_cancle']= "বাতিল করুন";
$lang['left_valid_quantity']= "পণ্যের সঠিক পরিমাণ দিন";
$lang['right_header']= "নষ্ট অথবা হারানো পণের তালিকা ";
$lang['right_type']= "ধরন";
$lang['right_item_name']= "পণ্যের নাম";
$lang['right_quantity']="পরিমাণ";
$lang['right_description']= "বিবরণ";
$lang['right_date']="তারিখ";
$lang['right_edit_delete']= "পরিবর্তন / ডিলিট";
$lang['left_valid_item']="পণ্য নির্বাচন করুন ";
$lang['left_edit']="নষ্ট / ডিলিটের তথ্য পরিবর্তন করুন ";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_adjust_confirmation']="সামঞ্জস্য নিশ্চিতকরণঃ";
$lang['alert_adjust_details']="আপনি কি এই তথ্য সামঞ্জস্য করতে চান ? ";
$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";
$lang['alert_delete_success']="সফলভাবে  ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !! ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে  আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !!  আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে  সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !!  সেভ হয়নি";
$lang['default_error']="দুঃখিত !! আবার চেষ্টা করুন";
$lang['left_valid_more_quantity']= "মজুদক্রিত পরিমাণের চেয়ে বেশি পরিমাণ প্রদান করা হয়েছে";
$lang['alert_access_failure']="দুঃখিত!! আপনার অনুমতি নেই।";
$lang['item_select_tt']="পণ্যের নাম লিখুন । পণ্যের তালিকা থেকে পণ্য নির্বাচন করুন ";
$lang['left_buying_price']="ক্রয় মূল্য";
$lang['left_valid_buying_price']="পণ্যের ক্রয় মূল্য প্রদান করুন";
$lang['imei_used']="এই আই.এম.ই.আই বিক্রিত অথবা ডিলিট করা হয়েছে ";
$lang['one_qty_for_imei']="পরিমান এক দিন ";
$lang['wrong_imei']="ভুল আই.এম.ই.আই প্রদান করা হয়েছে";