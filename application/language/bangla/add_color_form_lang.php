<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] = 'নতুন রং যোগ করুন!';
$lang['color'] = 'প্রোডাক্টের রং যোগ করুন!';
$lang['right_portlet_title'] = 'সকল রং ের তালিকা';
$lang['color_name'] = 'রং এর নাম';
$lang['color_name_placeholder'] = 'রংের নাম';
$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['Buy advance'] = 'বাতিল করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['want_to_delete'] = 'আপনি কি রংের এই তথ্য ডিলিট করতে চান ?';
$lang['delete_success'] = 'সফলভাবে  প্রোডাক্টের রংের  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  প্রোডাক্টের রংের  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  প্রোডাক্টের রংের  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  প্রোডাক্টের রংের  তথ্য আপডেট হয়েছে';
$lang['edit_color'] = 'রংের তথ্য পরিবর্তন করুন';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file color_page_lang.php */
/* Location: ./application/language/english/color_page_lang.php */