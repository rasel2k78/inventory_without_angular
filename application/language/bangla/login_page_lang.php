<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['sign_in_title'] = 'সাইন ইন';
$lang['email_place_holder'] = 'ইমেইল / মোবাইল';
$lang['password_place_holder'] = 'দোকানের পাসওয়ার্ড';
$lang['login_button'] = 'লগইন';
$lang['remember_check'] = 'পাসওয়ার্ড মনে রাখুন';
$lang['forgot_password_link'] = 'পাসওয়ার্ড ভুলে গেছেন?';
$lang['forgot_password_title'] = 'পাসওয়ার্ড  পুনরুদ্ধার';
$lang['recover_email_button'] = 'ইমেইল দিয়ে  পুনরুদ্ধার';
$lang['recover_phone_button'] = 'মোবাইল দিয়ে  পুনরুদ্ধার';
$lang['back_button'] = 'পিছনে যান';
$lang['submit_button'] = 'সাবমিট';

$lang['forget_email_placeholder'] = '		ইমেইল (ইন্টারনেট বাধ্যতামূলক)';
$lang['forget_phone_placeholder'] = '	মোবাইল নাম্বার (ইন্টারনেট বাধ্যতামূলক)';

$lang['empty_alert'] = 'ইনপুট এর ঘর খালি থাকতে পারবেনা';
$lang['invalid_alert'] = 'ভুল তথ্য দেওয়া হয়েছে';
$lang['net_alert'] = 'প্রথমবার লগইনের জন্য ইন্টারনেট বাধ্যতামূলক';
$lang['mac_alert'] = 'আপনি ভিন্ন কম্পিউটার থেকে চেষ্টা করছেন';


$lang['no_net']="ইন্টারনেট সংযোগ প্রয়োজন";
$lang['empty_email']="ইমেইল এড্রেস দিন";
$lang['email_not_found']="এই ইমেইলের রেকর্ড নেই";

$lang['check_email']="নতুন পাসওয়ার্ডের জন্য ইমেইল চেক করুন";






