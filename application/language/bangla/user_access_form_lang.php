<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// Alert portion
$lang['user_not_selected'] = 'আগে একজন ব্যবহারকারী নির্বাচন করুন';
$lang['access_added'] = 'সফলভাবে অনুমতি দেওয়া হয়েছে';

$lang['access_adding_fail'] = 'দুঃখিত !!অনুমতি দেওয়া যায়নি';
$lang['access_remove_success'] = 'সফলভাবে অনুমতি বাতিল করা হয়েছে';

$lang['access_remove_fail'] = 'দুঃখিত !!অনুমতি বাতিল করা যায়নি';
$lang['no_permission'] = 'আপনার অনুমতি নেই';

$lang['owner_remove_fail'] = 'মালিক হিসেবে আপনি আপনার এই অনুমতি বাতিল করতে পারবেননা';
$lang['Bank'] = "ব্যাংক";
$lang['Bank Accounts'] = "ব্যাংক একাউন্ট";
$lang['Bank D/W'] = "ব্যাংক আমানত / প্রত্যাহার";
$lang['Pending cheque'] = "প্রক্রিয়াধীন চেক";

$lang['all_user'] = 'সকল ব্যবহারকারী';
$lang['name'] = 'নাম';
$lang['role'] = 'ভূমিকা';
$lang['edit'] = 'পরিবর্তন';
$lang['page_access'] = 'ব্যবহারকারীর প্রবেশাধিকার';

$lang['language'] = 'ভাষা';
// $lang['Page name'] = 'পাতার নাম';
$lang['page_name'] = 'নাম';


$lang['Brand'] = 'ব্র্যান্ড';
$lang['Buy'] = 'ক্রয়';
$lang['Buy advance'] = 'অগ্রীম ক্রয়';
$lang['Buy due'] = 'বাকীতে ক্রয়';
$lang['Buy list'] = 'ক্রয় তালিকা';
$lang['Cashbox'] = 'ক্যাশ বক্স';
$lang['Category'] = 'ক্যাটাগরি';
$lang['Customer'] = 'কাস্টমার';
$lang['Exchange with customer'] = 'কাস্টমারের সাথে পরিবর্তন';
$lang['Exchange with vendor'] = 'ভেন্ডরের সাথে পরিবর্তন';
$lang['Expenditure'] = 'ব্যায়';
$lang['Expenditure type'] = 'ব্যায়ের ধরন';
$lang['Item'] = 'পন্য';
$lang['Loan'] = 'ধার';
$lang['Lot size'] = 'লট সাইজ';
$lang['Report'] = 'রিপোর্ট';
$lang['Sell'] = 'বিক্রয়';
$lang['Sell advance'] = 'অগ্রীম বিক্রয়';
$lang['Sell due'] = 'বাকীতে বিক্রয়';
$lang['Sell list'] = 'বিক্রয়ের তালিকা';
$lang['Size'] = 'সাইজ';
$lang['Sync'] = 'সিঙ্ক';
$lang['Tax'] = 'ট্যাক্স';
$lang['Transfer'] = 'ট্রান্সফার';
$lang['Unit'] = 'ইউনিট';
$lang['User'] = 'ব্যবহারকারী';
$lang['User Access'] = 'ব্যবহারকারীর প্রবেশাধিকার';
$lang['Vendor'] = 'ভেন্ডর';
$lang['Return money'] = 'টাকা ফেরত';
$lang['Sales representative'] = 'বিক্রয় প্রতিনিধি';
$lang['Damage and lost'] = 'নষ্ট / হারিয়ে যাবার তথ্য';
$lang['Vat'] = 'ভ্যাট';
$lang['Warehouse'] = 'গুদাম';