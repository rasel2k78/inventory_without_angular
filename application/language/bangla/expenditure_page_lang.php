<?php 
/**
 *Bangla Language file for Expenditure controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="নতুন খরচের তথ্য যোগ করুন";
$lang['right_portlet_title']="সকল খরচের তালিকা";
$lang['left_exp_type']="খরচের ধরন নির্বাচন করুন";
$lang['left_exp_type_placeholder']="খরচের ধরন নির্বাচন করুন";
$lang['left_amount']="খরচের পরিমাণ (টাকায়)";
$lang['left_amount_placeholder']="খরচের পরিমাণ (টাকায়)";
$lang['left_date']="খরচের তারিখ";
$lang['left_date_placeholder']="বছর-মাস-দিন";
$lang['left_expense_description']="খরচের বিবরণ";
$lang['left_expense_description_placeholder']="খরচের বিবরণ";
$lang['left_save']="সেভ করুন";
$lang['left_cancle']="বাতিল করুন";
$lang['right_exp_type']="ধরন";
$lang['right_amount']="পরিমাণ";
$lang['right_description']="বিবরণ";
$lang['right_date']="তারিখ";
$lang['right_edit_delete']="পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";
$lang['alert_delete_success']="সফলভাবে খরচের তথ্য ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!খরচের তথ্য ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে খরচের তথ্য আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! খরচের তথ্য আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে খরচের তথ্য সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! খরচের তথ্য সেভ হয়নি";
$lang['edit_header']="খরচের তথ্য পরিবর্তন করুন";
$lang['validation_type']="খরচের ধরন নির্বাচন করুন";
$lang['validation_amount']="খরচের পরিমাণ দিন (টাকায়)";
$lang['validation_date']="খরচের তারিখ নির্বাচন করুন";
$lang['validation_description']="খরচের বিবরণ দিন";
$lang['total_amount_validation']="দুঃখিত! ক্যাশবক্সে পর্যাপ্ত পরিমাণ টাকা নেই";
$lang['alert_insert_failed_modal']="দুঃখিত!! খরচের ধরন যোগ হয়নি";
$lang['alert_insert_success_modal']="সফলভাবে খরচের তথ্য যোগ হয়েছে";
$lang['type_select_tt']="খরচের ধরনের নাম লিখুন । তালিকা থেকে খরচের ধরনের নাম নির্বাচন করুন";
$lang['left_exp_type_title']="নতুন খরচের ধরন যোগ করুন";
$lang['more_than_actual_amount']="প্রদেয় এর চেয়ে বেশি টাকা প্রদান করা হয়েছে।";
$lang['right_cash_type']="বিল প্রদানের ধরন";
$lang['right_type_cash']="ক্যাশ";
$lang['right_type_cheque']="চেক";
$lang['cheque_page_empty']="চেক নাম্বার দিন ";
$lang['bank_acc_not_select']="ব্যাংক অ্যাকাউন্ট নির্বাচন করুন ";
$lang['select_acc']="ব্যাংক অ্যাকাউন্ট ";
$lang['enter_chk_num']="চেক নাম্বার ";
$lang['not_enough_balance']="অ্যাকাউন্টে পর্যাপ্ত পরিমাণ টাকা নেই";
$lang['validation_bank_acc']="ব্যাংক অ্যাকাউন্ট নির্বাচন করুন";
$lang['validation_chk_num']="চেক নাম্বার দিন";
$lang['pre_date']= "বিগত তারিখ ";