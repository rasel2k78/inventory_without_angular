<?php 
/**
 *Bangla Language file for Nagadanboi controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="জমা/ উত্তোলন ফরম";
$lang['right_portlet_title']="জমা/ উত্তোলনের তালিকা";
$lang['left_type']="ধরন";
$lang['left_type_deposit']="জমা করুন";
$lang['left_type_withdrawal']="উত্তোলন করুন";
$lang['left_amount']="টাকার পরিমাণ";
$lang['left_amount_placeholder']="টাকার পরিমাণ";
$lang['left_description']="জমা/উত্তোলনের বিবরণ";
$lang['left_description_placeholder']="জমা/উত্তোলনের বিবরণ";
$lang['left_save']="সেভ করুন";
$lang['left_cancle']="বাতিল করুন";
$lang['right_type']="ধরন";
$lang['right_amount']="টাকার পরিমাণ";
$lang['right_description']="বিবরণ";
$lang['right_date']="তারিখ";
$lang['right_edit_delete']="পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";
$lang['alert_delete_success']="সফলভাবে অ্যাকাউন্টের তথ্য ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !! অ্যাকাউন্টের তথ্য ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে অ্যাকাউন্টের তথ্য আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! অ্যাকাউন্টের তথ্য আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে অ্যাকাউন্টের তথ্য সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! অ্যাকাউন্টের তথ্য সেভ হয়নি";
$lang['validation_amount']="টাকার সঠিক পরিমাণ দিন";
$lang['edit_cashbox']="জমা/ উত্তোলনের তথ্য পরিবর্তন করুন";
$lang['withdrawal_failure']="দুঃখিত! অ্যাকাউন্ট পর্যাপ্ত পরিমাণ টাকা নেই";
$lang['more_amount_than_cashbox']= "দুঃখিত! অ্যাকাউন্ট পর্যাপ্ত পরিমাণ টাকা নেই";
$lang['right_ac']="অ্যাকাউন্ট নাম্বার";
$lang['select_acc']="অ্যাকাউন্ট নাম্বার ";
$lang['select_acc_pl']="অ্যাকাউন্ট নাম্বার নির্বাচন করুন ";
$lang['bank_dw']="অ্যাকাউন্ট নাম্বার";