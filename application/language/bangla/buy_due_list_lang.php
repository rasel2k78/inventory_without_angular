<?php 
/**
 *Bangla Language file for Buy_list controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="পণ্য ক্রয়ে বাকির তালিকা";
$lang['right_portlet_title'] ="পণ্য ক্রয়ের বাকি টাকা পরিশোধ করুন";
$lang['left_voucher_no'] ="ক্রয়ের রসিদ নং";
$lang['left_purchase_date'] ="ক্রয়ের তারিখ";
$lang['left_paymentmade'] ="বাকি পরিশোধ";
$lang['right_voucher_no'] ="রসিদ নং";
$lang['right_net_payable'] ="প্রদেয়";
$lang['right_paid'] ="মোট পরিশোধ";
$lang['right_due'] ="মোট বাকি";
$lang['right_paymentmade_amount'] ="বাকি পরিশোধ";
$lang['right_paymentmade_amount_placeholder'] ="বাকি পরিশোধ";
$lang['right_description'] ="বিবরণ";
$lang['right_description_placeholder'] ="বিবরণ";
$lang['right_save_button'] ="সেভ করুন";
$lang['right_cancle_button'] ="বাতিল করুন";
$lang['right_item_name'] ="পণ্যের নাম";
$lang['right_quantity'] ="পরিমাণ";
$lang['right_purchase_price'] ="ক্রয় মূল্য /ইউনিট";
$lang['right_sub_total'] ="সাব-টোটাল";
$lang['alert_paymentmade_success'] ="সফলভাবে বাকি টাকা পরিশোধ হল";
$lang['alert_paymentmade_failed'] ="দুঃখিত! বাকি টাকা পরিশোধ হয়নি";
$lang['validation_msg']="টাকার পরিমাণ দিন";
$lang['alert_more_paid']="দুঃখিত! বাকি টাকার চেয়ে বেশি টাকা পরিশোধ করা হয়েছে";
$lang['extra_amount_inserted']= "দুঃখিত! বাকি টাকার চেয়ে বেশি টাকা পরিশোধ করা হয়েছে";
$lang['right_dis_type']="ছাড়ের ধরন ";
$lang['right_dis_amount']="ছাড়ের পরিমাণ";
$lang['buy_due_paymentmade_tt']="বাকি টাকা পরিশোধের পরিমাণ দিন। মোট বাকি টাকার চেয়ে বেশি টাকা প্রদান করবেন না।";
$lang['alert_message_ed']="দুঃখিত! আপনার অনুমতি নেই।";
$lang['left_vendor']="সরবরাহকারী";
$lang['search_vendor']="সরবরাহকারী খুজুন";
$lang['select_vendor']="সরবরাহকারী নির্বাচন করুন";
$lang['search']="খুজুন";
$lang['select_vendor_first']="প্রথমে সরবরাহকারী নির্বাচন করুন";
$lang['bunch_payment_title']="বাকি টাকা পরিশোধের ফরম ";
$lang['vendor_name']="সরবরাহকারী";
$lang['right_cash_type']="বিল প্রদানের ধরন";
$lang['right_type_cash']="ক্যাশ";
$lang['right_type_cheque']="চেক";
$lang['cheque_page_empty']="চেক নাম্বার দিন ";
$lang['bank_acc_not_select']="ব্যাংক অ্যাকাউন্ট নির্বাচন করুন ";
$lang['select_acc']="ব্যাংক অ্যাকাউন্ট ";
$lang['enter_chk_num']="চেক নাম্বার ";
$lang['not_enough_balance']="অ্যাকাউন্টে পর্যাপ্ত পরিমাণ টাকা নেই";
$lang['no_cheque_number']="চেক নাম্বার দিন";
$lang['no_bank_acc']="সঠিক ব্যাংক অ্যাকাউন্ট নির্বাচন করুন";
$lang['adjust_due_lang']="বাট্টা হিসাব";