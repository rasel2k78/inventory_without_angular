<?php 

/**
 *Bangla Language file for Expenditure_type controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "নতুন খরচের ধরন যোগ করুন";
$lang['right_portlet_title'] = "সকল খরচের ধরনের তালিকা";
$lang['left_expense_type_name'] = "খরচের ধরনের নাম";
$lang['left_expense_type_name_placeholder'] = "খরচের ধরনের নাম";
$lang['left_expense_description'] = "খরচের ধরনের বিবরণ";
$lang['left_expense_description_placeholder'] = "খরচের ধরনের বিবরণ";
$lang['left_save'] = "সেভ করুন";
$lang['left_cancle'] = "বাতিল করুন";
$lang['right_expense_type'] = "ধরন";
$lang['right_description'] = "বিবরণ";
$lang['right_date'] = "তারিখ";
$lang['right_edit_delete'] = "পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";

$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";

$lang['alert_delete_success']="সফলভাবে খরচের ধরন ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!খরচের ধরন ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে খরচের ধরন আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! খরচের ধরন আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে খরচের ধরন সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! খরচের ধরন সেভ হয়নি";


$lang['edit_header']="খরচের ধরনের তথ্য পরিবর্তন করুন";
$lang['validation_name']="খরচের ধরনের নাম দিন";

$lang['model_edit']="পরিবর্তন";
$lang['model_delete']="ডিলিট";

$lang['left_expense_type_name_new']="নতুন খরচের ধরন যোগ করুন";
$lang['cancel']="বাতিল করুন";