<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['current_cashbox_amount']="ক্যাশবক্সে বর্তমান টাকার পরিমাণ";
$lang['today_total_sell']="আজকের মোট বিক্রয়";
$lang['today_total_buy']="আজকের মোট ক্রয়";
$lang['today_total_expense']="আজকের মোট খরচ";
$lang['all_shortage_list']="স্বল্প পরিমাণ পণ্যের তালিকা";
$lang['item_name']="পণ্যের নাম";
$lang['quantity']="পরিমাণ ";
$lang['today_sell_list']="আজকের বিক্রয়ের তালিকা";
$lang['customer_name']="কাস্টমারের নাম";
$lang['sell_voucher_no']="রসিদ নং";
$lang['today_buy_list']="আজকের ক্রয়ের তালিকা ";
$lang['buy_voucher_no']="রসিদ নং";
$lang['grand_total']="মোট";
$lang['discount']="মূল্যছাড়";
$lang['paid']="পরিশোধ";
$lang['staff_wise_daily_sell']="কর্মচারীদের দৈনিক বিক্রয়";
$lang['staff_name']="কর্মচারীর নাম";
$lang['todays_sell']="আজকের মোট বিক্রয়ের পরিমাণ";
$lang['due']="বাকি";
$lang['change']="ফেরতকৃত টাকা";