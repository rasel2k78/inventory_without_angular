<?php 
/**
 *Bangla Language file for Left Nevigation Bar.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['item']="পণ্য";
$lang['dashboard']="ড্যাশবোর্ড";
$lang['cashbox']="ক্যাশবাক্স";
$lang['deposit_withdrawal']="জমা / উত্তোলন";
$lang['loan']="ঋণ গ্রহণ / পরিশোধ";
$lang['add_item']="পণ্য যোগ করুন";
$lang['add_brand']="ব্রান্ড যোগ করুন";
$lang['add_category']="ক্যাটাগরি যোগ করুন";
$lang['add_unit']="ইউনিট যোগ করুন";
$lang['add_vendor']="সরবরাহকারী যোগ করুন";
$lang['add_size']="সাইজ যোগ করুন";
$lang['add_lot_size']="লট সাইজ যোগ করুন";
$lang['buy']=" ক্রয়";
$lang['buy_items']=" ক্রয় করুন ";
$lang['buy_due_list']="ক্রয়ে বাকির তালিকা";
$lang['buy_all_list']="সকল ক্রয়ের তালিকা ";
$lang['advance_buy']="অগ্রিম মূল্য পরিশোধ";
$lang['sell']=" বিক্রয়";
$lang['sell_items']=" বিক্রয় করুন";
$lang['sell_due_list']="বিক্রয়ে বাকির তালিকা";
$lang['sell_all_list']="সকল বিক্রয়ের তালিকা";
$lang['add_tax_type']="ট্যাক্সের ধরন যোগ করুন";
$lang['add_customer']="কাস্টমার যোগ করুন";
$lang['demage_and_lost']="নষ্ট / হারিয়ে যাওয়া ";
$lang['expenditure']="আনুসাঙ্গিক খরচ";
$lang['add_exp_type']="খরচের ধরন যোগ করুন";
$lang['add_exp']="নতুন খরচ যোগ করুন";
$lang['exchange_return']="ফেরত / পরিবর্তন";
$lang['exchange_with_vendor']="সরবরাহকারীর সাথে পরিবর্তন";
$lang['exchange_with_customer']="কাস্টমার হতে পরিবর্তন ";
$lang['money_return_customer']="কাস্টমারের টাকা ফেরত";
$lang['money_return_vendor']="সরবরাহকারীর টাকা ফেরত";
$lang['user']="ব্যবহারকারী";
$lang['add_user']="ব্যবহারকারী যোগ করুন";
$lang['attendance']="উপস্থিতি";
$lang['user_access_panel']="ব্যবহারকারীর প্রবেশাধিকার";
$lang['shop_info']="দোকান এবং প্রিন্টার সেটআপ";
$lang['transfer']="পণ্য হস্তান্তর ";
$lang['sync']="সামঞ্জস্য";
$lang['update']="হালনাগাদ";
$lang['report']="রিপোর্ট";
$lang['add_sales_rep']="বিক্রয় প্রতিনিধি";
$lang['transfer']="পণ্য হস্তান্তর"; 
$lang['transfer_list']="পণ্য হস্তান্তরের তালিকা";
$lang['transfer_receive_list']="বিক্রয় আসার প্রতিনিধি";
$lang['Specification']="সবিস্তার বিবরণী";
$lang['Return money to vendor']="সরবরাহকারীের টাকা প্রত্যাবর্তন";
$lang['Return money to customer']="কাস্টমারের টাকা প্রত্যাবর্তন";
$lang['vat']="ভ্যাট যোগ করুন";
$lang['add_spec_left_nav']="বিবরণী যোগ করুন";
$lang['money_sign']="৳";
$lang['no_permission']="দুঃখিত!! আপনার অনুমতি নেই।";
$lang['settings_report']="সেটিংস";
$lang['vat']= "ভ্যাট";
$lang['video_tut']="ভিডিও টিউটোরিয়াল";
$lang['bank']="ব্যাংক ";
$lang['add_new_bank']="নতুন ব্যাংক যোগ করুন ";
$lang['add_bank_accounts']="নতুন অ্যাকাউন্ট যোগ করুন ";
$lang['bank_depo_with']="জমা / উত্তোলন ";
$lang['pending_cheques']="প্রক্রিয়াধীন চেক";
$lang['printer_setting']="প্রিন্টার সেটিং";
$lang['contact_us']="হেল্প এবং সাপোর্ট ";
$lang['database_backup']="ডেটাবেস ব্যাকআপ";
$lang['warehouse_receive_list']="পণ্য গ্রহণের তালিকা";
$lang['Warehouse'] = 'গুদাম';