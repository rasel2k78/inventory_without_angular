<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'নতুন ব্রান্ড যোগ করুন!';
$lang['right_portlet_title'] = 'সকল ব্রান্ড তালিকা';
$lang['edit_brand'] = 'ব্রান্ডের তথ্য পরিবর্তন করুন';
$lang['brand_name'] = 'ব্রান্ডের নাম';
$lang['brand_description'] = 'ব্রান্ডের বিবরণ';
$lang['brand_select_picture'] = 'ছবি নির্বাচন করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
//$lang['brand_name'] = 'আপনি কি জমা / উত্তোলনের এই তথ্য ডিলিট করতে চান';
$lang['select_brand'] = 'ব্রান্ড নির্বাচন করুন';
$lang['change_brand_info'] = 'ব্রান্ডের তথ্য পরিবর্তন করুন';
$lang['brand_logo'] = 'ব্রান্ডের ছবি';

$lang['want_to_delete'] = 'আপনি কি ব্রান্ডের এই তথ্য ডিলিট করতে চান ?';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['brand_name_required'] = 'ব্রান্ডের নাম দিন';
$lang['delete_success'] = 'সফলভাবে ব্রান্ডের তথ্য ডিলিট হয়েছে';
$lang['insert_failure'] = 'দুঃখিত !! ব্রান্ডের তথ্য সেভ হয়নি';
$lang['insert_success'] = 'সফলভাবে ব্রান্ডের তথ্য সেভ হয়েছে';
$lang['update_success'] = 'সফলভাবে ব্রান্ডের তথ্য আপডেট হয়েছে';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';


/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */