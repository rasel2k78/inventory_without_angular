<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'পরিবর্তিত পণ্য ক্রয়ের তথ্য';
$lang['voucher_no'] = 'রসিদ নং';
$lang['item'] = 'পণ্য';
$lang['quantity'] = 'পরিমাণ';
$lang['total_quantity'] = 'মোট পরিমাণ';
$lang['buying_price'] = 'ক্রয় মূল্য';
$lang['sub_total'] = 'সাব-টোটাল';
$lang['Delete_items'] = 'ডিলিট করুন';
$lang['total'] = 'সর্বমোট:';
$lang['new_item_buy_info'] = 'নতুন পণ্য ক্রয়ের তথ্য:';
$lang['select_item'] = 'পণ্য  নির্বাচন করুন';
$lang['total_new_amount'] = 'নতুন পণ্য ক্রয়ের মূল্য';
$lang['receivable'] = 'প্রাপ্য';
$lang['payable'] = 'প্রদেয়';
$lang['Due'] = 'বাকি';
$lang['use_voucher'] = 'এই রসিদ আপনার?';
$lang['phone_number'] = 'ফোন নাম্বার';
$lang['item'] = 'পণ্য';
$lang['date'] = 'তারিখ';
$lang['total_price'] = 'মোট মূল্য';
$lang['paid'] = 'পরিশোধ';
$lang['details'] = 'বিস্তারিত';
$lang['sorry'] = 'দুঃখিত';
$lang['no_items'] = 'রসিদে পণ্য নেই';
$lang['give_numeric'] = 'সংখ্যায় ইনপুট দিন';


$lang['item_invalid_price'] = 'পণ্যের মূল্য ঠিক নেই';
$lang['give_item_quntity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['give_item_price'] = 'পণ্যের মূল্য দিন';
$lang['give_appropriate_amount'] = 'পণ্যের সঠিক মূল্য দিন';
$lang['no_voucher'] = 'রসিদ নেই';
$lang['exchanged_quantity_exceded'] = 'পরিবর্তিত পরিমাণ অতিরিক্ত হয়ে গেছে';
$lang['add_exchanged_item'] = 'পণ্য পরিবর্তনের জন্যে নতুন পণ্য যোগ করুন';


$lang['total_paid'] = 'মোট পরিশোধ :';
$lang['exchange_with_customer_info'] = 'কাস্টমারের সাথে পণ্য পরিবর্তনের তথ্য';
$lang['find'] = 'খুজুন';
$lang['item_buy_details'] = 'পণ্য ক্রয়ের বিবরণ';
$lang['item_sell'] = 'নতুন পণ্য বিক্রয় করুন ';
$lang['item_name'] = 'পণ্যের নাম';
$lang['buying_price_unit'] = 'ক্রয় মূল্য /ইউনিট';
$lang['new_quantity'] = 'ফেরতক্রিত পণ্যের পরিমাণ';
$lang['total_bill'] = 'মোট বিল';
$lang['select_item'] = 'পণ্য নির্বাচন করুন';
$lang['give_item_quantity'] = 'পণ্যের পরিমাণ প্রদান করুন';
$lang['sell_price'] = 'বিক্রয় মূল্য';
$lang['sell_price_unit'] = 'বিক্রয় মূল্য /ইউনিট (খুচরা)';
$lang['single_price'] = 'খুসরা মূল্য';
$lang['whole_price'] = 'পাইকারি মূল্য';
$lang['sell_price_whole'] = 'বিক্রয় মূল্য /ইউনিট (পাইকারি)';
$lang['deadline'] = 'মেয়াদ উত্তীর্ণের তারিখ';
$lang['add_new_item'] = 'নতুন পণ্য যোগ করুন';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['customer_phone'] = 'কুস্তমের ফোন নাম্বার';
$lang['not_exchanged'] = 'কোন পণ্য পরিবরতন করেননি';
$lang['exchange_success'] = 'সফলভাবে পণ্য পরিবর্তন  হয়েছে';
$lang['find_voucher_tt'] = 'সঠিক রসিদ নাম্বার দিন অথবা তারিখ,মোবাইল,পণ্যের নামে দিয়ে আপনার রসিদ বের করুন';
$lang['details_button_tt'] = 'নিচের বাটন এ ক্লিক করে দেখুন আপনার রসিদ কি না এবং নিশ্চিত করে নিচের বাটন এ ক্লিক করুন';
$lang['new_item_tt'] = 'বিনিময়ের জন্যে নতুন পণ্য নির্বাচন করুন';
$lang['item_list_tt'] = 'পরিবর্তিত পণ্যের তালিকা';
$lang['item_new_total_price_tt'] = 'নতুন পণ্যসমূহের সর্বমোট মূল্য';
$lang['item_new_tt'] = 'পরিবরতনের পর নতুন পণ্যসমূহ';
$lang['item_new_paid_tt'] = 'পণ্য পরিবরতনের পর পরিশোধিত পরিমাণ';
$lang['amount_exchange_tt'] = 'নতুন পণ্য পরিবরতনের পর মোট পরিমাণ';
$lang['amount_after_paid_tt'] = 'পণ্য পরিবরতনের পর অবশিষ্ট পরিশোধের পর মোট পরিমাণ';

$lang['give_voucher_no_tt'] = "রশিদ নাম্বার প্রদান করুন";

$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['want_to_delete'] = 'আপনি কি লট সাইজের এই তথ্য ডিলিট করতে চান ?';
$lang['delete_success'] = 'সফলভাবে  লট সাইজের  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  লট সাইজের  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য আপডেট হয়েছে';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/exchange_with_customer_form_lang.php */