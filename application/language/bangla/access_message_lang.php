<?php 

/**
 *Bangla Language file for Nagadanboi controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['alert_access_failure'] = "দুঃখিত!! আপনার অনুমতি নেই";