<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'কাস্টমারের সাথে টাকা ফেরতের তথ্য';
$lang['find'] = 'খুজুন';
$lang['voucher_no'] = 'ক্রয় রসিদ নং';
$lang['buy_details'] = 'পণ্য ক্রয়ের বিবরণ';
$lang['item_name'] = 'পণ্যের নাম';
$lang['quantity'] = 'পরিমাণ';
$lang['buying_price'] = 'ক্রয় মূল্য /ইউনিট';
$lang['sub_total'] = 'সাব-টোটাল';
$lang['total_bill'] = 'মোট বিল';
$lang['total_paid'] = 'মোট পরিশোধ';
$lang['total_receivable'] = 'মোট বাকি';
$lang['return_money'] = 'টাকা ফেরত';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';
$lang['return_success'] = 'সফলভাবে টাকা ফেরত সম্পূর্ণ হয়েছে';
$lang['no_voucher'] = 'রসিদে নেই';
$lang['no_return_amount'] = 'রসিদে টাকা নেই';
$lang['give_voucher'] = 'রসিদ নাম্বার দিন';
$lang['selling_price'] = 'বিক্রয় মূল্য';


$lang['due_first'] = 'টাকা ফেরত সম্ভহব না !! আগে বাকি শোধ করুন';
$lang['returned_items'] = 'ফেরতক্রিত প্রোডাক্টের তালিকা';
$lang['buying_price'] = 'ক্রয়মূল্য';
$lang['total'] = 'মোট';
$lang['return_quantity'] = 'ফেরতক্রিত প্রোডাক্টের পরিমান';
$lang['total_due'] = 'মোট বাকি';
$lang['return_quantity_exceded'] = 'দুঃখিতখিত পরিবর্তিত প্রোডাক্টের সীমা অতিক্রম করছে';



$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/return_money_form_lang.php */