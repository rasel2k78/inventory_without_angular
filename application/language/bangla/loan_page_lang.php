<?php 

/**
 *Bangla Language file for Loan controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

defined('BASEPATH') OR exit('No direct script access allowed');


$lang['left_portlet_title']="ঋণ গ্রহণ / পরিশোধের ফর্ম পূরণ করুন";
$lang['right_portlet_title']="ঋণ কৃত টাকার তালিকা";

$lang['left_amount']="ঋণ কৃত টাকার পরিমাণ";
$lang['left_amount_placeholder']="ঋণ কৃত টাকার পরিমাণ";
$lang['left_payout']="ঋণ পরিশোধ";

$lang['left_loan_description']="ঋণ গ্রহণের বিবরণ";
$lang['left_loan_description_placeholder']="ঋণ গ্রহণের বিবরণ";
$lang['left_save']="সেভ করুন";
$lang['left_cancle']="বাতিল করুন";
$lang['right_type']="ধরন";
$lang['right_amount']="পরিমাণ";
$lang['right_description']="বিবরণ";
$lang['right_date']="তারিখ";
$lang['right_edit_delete']="পরিবর্তন / পরিশোধ / ডিলিট";


$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_adjust_confirmation']="সামঞ্জস্য নিশ্চিতকরণঃ";
$lang['alert_adjust_details']="আপনি কি এই তথ্য সামঞ্জস্য করতে চান ? ";

$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";

$lang['alert_delete_success']="সফলভাবে ঋণ ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!ঋণ ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে ঋণ আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! ঋণ আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে ঋণ সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! ঋণ সেভ হয়নি";
$lang['alert_adjust_success']="সফলভাবে ঋণ সামঞ্জস্য হয়েছে";
$lang['alert_adjust_failed']="দুঃখিত !! ঋণ সামঞ্জস্য হয়নি";
$lang['default_error']="দুঃখিত !! আবার চেষ্টা করুন";
$lang['delete_error']="দুঃখিত!! ডিলিট করা সম্ভব হবে না... অ্যাডজাস্ট করুন  ";
$lang['payout_success']="সফলভাবে ঋণ পরিশোধ সম্পন্ন হয়েছে";

$lang['edit_header']="ঋণ  পরিবর্তন করুন";

$lang['validation_amount']="টাকার সঠিক পরিমাণ দিন";
$lang['total_loan_paid']="পরিশোধ";
$lang['alert_more_paid']="দুঃখিত!! ঋণের থেকে বেশি টাকা পরিশোধ করা হয়েছে ";

$lang['alert_access_failure']="দুঃখিত! আপনার অনুমতি নেই";
$lang['payout_header']="ঋণ পরিশোধের ফরম";
$lang['not_enough_amount']="দুঃখিত! পর্যাপ্ত পরিমাণ টাকা ক্যাশবক্সে নেই";
$lang['more_payout_than_loan']="দুঃখিত! ঋণের চেয়ে বেশি টাকা পরিশোধ করা হচ্ছে";
$lang['total_payout_validation']="দুঃখিত! প্রদত্ত টাকার চেয়ে বেশি টাকা পরিশোধ কওরা হয়েছে";

$lang['loan_paid']="পরিশোধ";
$lang['remaining_loan']= "অবশিষ্ট";