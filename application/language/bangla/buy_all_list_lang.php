<?php 
/**
 * Buy_list_all pages language change here.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="পণ্য ক্রয়ের তালিকা";
$lang['right_portlet_title'] ="পণ্য ক্রয়ের বিবরণ";
$lang['left_voucher_no'] ="ক্রয়ের রসিদ নং";
$lang['left_purchase_date'] ="ক্রয়ের তারিখ";
$lang['left_details_delete'] ="বিস্তারিত / ডিলিট";
$lang['right_voucher_no'] ="রসিদ নং";
$lang['right_net_payable'] ="প্রদেয়";
$lang['right_paid'] ="মোট পরিশোধ";
$lang['right_due'] ="মোট বাকি";
$lang['right_item_name'] ="পণ্যের নাম";
$lang['right_quantity'] ="পরিমাণ";
$lang['right_purchase_price'] ="ক্রয় মূল্য /ইউনিট";
$lang['right_sub_total'] ="সাব-টোটাল";
$lang['alert_paymentmade_success'] ="সফলভাবে বাকি টাকা পরিশোধ হল";
$lang['alert_paymentmade_failed'] ="দুঃখিত! বাকি টাকা পরিশোধ হয়নি";
$lang['right_grand_total'] ="মোট বিল";
$lang['right_discount'] ="মূল্যছাড়";
$lang['alert_delete_confirmation'] ="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details'] ="আপনি কি  এই তথ্য ডিলিট করতে চান ?";
$lang['alert_delete_yes'] ="হ্যা";
$lang['alert_delete_no'] ="না ";
$lang['alert_delete_success'] ="সফলভাবে বাকি টাকা পরিশোধ হল";
$lang['alert_delete_failure'] ="দুঃখিত! ডিলিট হয়নি";
$lang['right_landed_cost']="পণ্য ক্রয়ে মোট খরচ";
$lang['voucher_print']="প্রিন্ট";
$lang['right_dis_type']="ছাড়ের ধরন ";
$lang['right_dis_amount']="ছাড়ের পরিমাণ";
$lang['left_details_vendor']="সরবরাহকারী";
$lang['delete_failure_for_insuff_qty']="পণ্যের পরিমাণ কম থাকার কারণে এই রসিদ ডিলিট করা সম্ভব না ";
$lang['adv_buy_delete']="অগ্রিম ক্রয় সফলভাবে ডিলিট হয়েছে ";
$lang['adv_buy_list']="অগ্রিম ক্রয়ের তালিকা ";
$lang['change_date']="তারিখ পরিবর্তন";
$lang['edit_adv_buy']="অগ্রিম ক্রয় পরিবর্তন";
$lang['adv_paid']="অগ্রিম প্রদানকৃত টাকা";
$lang['receivable_date']="গ্রহণের তারিখ";
$lang['cancel']="বাতিল ";
$lang['update']="আপডেট";
$lang['buy_details']="ক্রয়ের বিবরণ ";
$lang['complete_order']="কমপ্লিট অর্ডার ";