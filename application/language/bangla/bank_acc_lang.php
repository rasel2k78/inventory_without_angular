<?php 
/**
 *Bangla Language file for Expenditure controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="নতুন ব্যাংক অ্যাকাউন্ট যোগ করুন";
$lang['right_portlet_title']="সকল অ্যাকাউন্টের তালিকা";
$lang['left_exp_type']="ব্যাংক নির্বাচন করুন";
$lang['left_exp_type_placeholder']="ব্যাংক নির্বাচন করুন";
$lang['left_acc']="অ্যাকাউন্ট নাম্বার";
$lang['left_acc_placeholder']="অ্যাকাউন্ট নাম্বার";
$lang['left_branch']="ব্যাংকের শাখা";
$lang['left_branch_placeholder']="ব্যাংকের শাখা";
$lang['left_expense_description']="বিবরণ";
$lang['left_expense_description_placeholder']="বিবরণ";
$lang['left_save']="সেভ করুন";
$lang['left_cancle']="বাতিল করুন";
$lang['right_exp_type']="ধরন";
$lang['right_ac']="অ্যাকাউন্ট নাম্বার ";
$lang['right_branch']="শাখা ";
$lang['right_date']="তারিখ";
$lang['right_edit_delete']="পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";
$lang['alert_delete_success']="সফলভাবে তথ্য ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!তথ্য ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে তথ্য আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! তথ্য আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে তথ্য সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! তথ্য সেভ হয়নি";
$lang['edit_header']="তথ্য পরিবর্তন করুন";
$lang['validation_type']="ব্যাংক নির্বাচন করুন";
$lang['validation_account']="ব্যাংক অ্যাকাউন্টের নাম্বার দিন";
$lang['validation_branch']="ব্যাংকের শাখার নাম দিন";
$lang['validation_description']="বিবরণ দিন";
$lang['alert_insert_failed_modal']="দুঃখিত!! তথ্য যোগ হয়নি";
$lang['alert_insert_success_modal']="সফলভাবে তথ্য যোগ হয়েছে";
$lang['type_select_tt']="ব্যাংকের নাম লিখুন । তালিকা থেকে ব্যাংকের নাম নির্বাচন করুন";
$lang['left_exp_type_title']="নতুন ব্যাংক যোগ করুন";
$lang['right_balance']="পরিমাণ";
$lang['alert_active_success']="অ্যাকাউন্ট সক্রিয় হয়েছে";
$lang['alert_active_failed']="দুঃখিত!! ভ্যাট সক্রিয় হয়নি";
$lang['alert_activate']="সক্রিয়তার নিশ্চিতকরণঃ";
$lang['alert_activate_details']="আপনি কি এই অ্যাকাউন্ট সক্রিয় করতে চান ?";
$lang['left_acc_name_placeholder']="অ্যাকাউন্টের নাম দিন";
$lang['left_acc_name']="অ্যাকাউন্টের নাম";
$lang['validation_name']="অ্যাকাউন্টের নাম দিন";