<?php 
/**
 *Bangla Language file for Sell_list_due controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] =" পণ্য বিক্রয়ে বাকির তালিকা ";
$lang['right_portlet_title'] ="পণ্য বিক্রয়ের বাকি টাকা পরিশোধ ";
$lang['left_customer']="ক্রেতা";
$lang['left_voucher_no'] ="রসিদ নং";
$lang['left_purchase_date'] ="তারিখ";
$lang['left_paymentmade'] ="বাকি পরিশোধ";
$lang['right_voucher_no'] ="রসিদ নং";
$lang['right_net_payable'] ="মোট বিল";
$lang['right_paid'] ="মোট পরিশোধ";
$lang['right_due'] ="মোট বাকি";
$lang['right_paymentmade_amount'] ="বাকি পরিশোধ";
$lang['right_paymentmade_amount_placeholder'] ="বাকি পরিশোধ";
$lang['right_description'] ="বিবরণ";
$lang['right_description_placeholder'] ="বিবরণ";
$lang['right_save_button'] ="সেভ করুন";
$lang['right_cancle_button'] ="বাতিল করুন";
$lang['right_item_name'] ="পণ্যের নাম";
$lang['right_quantity'] ="পরিমাণ";
$lang['right_selling_price'] =" মূল্য /ইউনিট";
$lang['right_sub_total'] ="সাব-টোটাল";
$lang['alert_paymentmade_success'] ="সফলভাবে বাকি টাকা পরিশোধ হল";
$lang['alert_paymentmade_failed'] ="দুঃখিত! বাকি টাকা পরিশোধ হয়নি";
$lang['validation_amount']="টাকার পরিমাণ দিন";
$lang['extra_sell_due_amount']= "দুঃখিত! বাকি টাকার চেয়ে বেশি টাকা পরিশোধ করা হয়েছে";
$lang['right_type_both']="ক্যাশ এবং কার্ড";
$lang['right_card_amount']="কার্ডে পরিশোধকৃত টাকার পরিমাণ";
$lang['right_cash_type']= "বিল প্রদানের ধরন";
$lang['right_type_cash']="ক্যাশ";
$lang['right_type_card']="কার্ড";
$lang['right_card_voucher_no']="কার্ডের বিল রসিদ নং";
$lang['card_voucher_err_msg']= "দুঃখিত! কার্ড পেমেন্টের সঠিক রসিদ নং দিন";
$lang['card_amount_larger']="দুঃখিত! টাকার পরিমাণ মোট পরিশোধকৃত টাকার চেয়ে বেশি";
$lang['no_customer']="খালি";
$lang['right_dis_type']="ছাড়ের ধরন ";
$lang['right_dis_amount']="ছাড়ের পরিমাণ";
$lang['sell_due_paymentmade_tt']="বাকি টাকা পরিশোধের পরিমাণ দিন। মোট বাকি টাকার চেয়ে বেশি টাকা প্রদান করবেন না।";
$lang['chk_num_empty']="চেক নাম্বার দিন";
$lang['chk_num_already_exist']="এই চেক নাম্বার একবার ব্যাবহার করা হয়েছে";
$lang['chk_number']="চেক নাম্বার ";
$lang['cheque']="চেক";
$lang['cheque_number_empty']="চেক নাম্বার দিন";
$lang['cheque_number_already_exist']="এই চেক নাম্বার একবার ব্যাবহার করা হয়েছে";
$lang['paymentmade_less_than_card_amt']="কার্ডে পরিশোধকৃত টাকার চেয়ে মোট পরিশোধে কম টাকা প্রদান করা হয়েছে ";
$lang['select_customer']="কাস্টমার নির্বাচন করুন ";
$lang['right_customer_placeholder']="কাস্টমার নির্বাচন করুন ";
$lang['right_cancle_button']="বাতিল";
$lang['search']="খুজুন";
$lang['search_customer']="কাস্টমার খুজুন";
$lang['validation_msg']="টাকার সঠিক পরিমাণ দিন ";
$lang['extra_amount_inserted']="বাকি টাকার চেয়ে বেশি টাকা পরিশোধ করা হয়েছে ";
$lang['cutomer_name']="কাস্টমার";
$lang['select_acc']="ব্যাংক অ্যাকাউন্ট";
$lang['charge'] = "চার্জ";