<?php 
/**
 *Bangla Language file for Bank controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] = "নতুন ব্যাংক যোগ করুন";
$lang['right_portlet_title'] = "সকল ব্যাংকের তালিকা";
$lang['left_bank_name'] = "ব্যাংকের নাম";
$lang['left_bank_name_placeholder'] = "ব্যাংকের নাম";
$lang['left_bank_description'] = "ব্যাংকের বিবরণ";
$lang['left_bank_description_placeholder'] = "ব্যাংকের বিবরণ";
$lang['left_save'] = "সেভ করুন";
$lang['left_cancle'] = "বাতিল করুন";
$lang['right_bank'] = "ধরন";
$lang['right_description'] = "চার্জ (%)";
$lang['right_date'] = "তারিখ";
$lang['right_edit_delete'] = "পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";
$lang['alert_delete_success']="সফলভাবে ব্যাংকের তথ্য ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!ব্যাংকের তথ্য ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে ব্যাংকের তথ্য আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! ব্যাংকের তথ্য আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে ব্যাংকের তথ্য সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! ব্যাংকের তথ্য সেভ হয়নি";
$lang['edit_header']="ব্যাংকের তথ্য পরিবর্তন করুন";
$lang['validation_name']="ব্যাংকের নাম দিন";
$lang['model_edit']="পরিবর্তন";
$lang['model_delete']="ডিলিট";
$lang['left_bank_name_new']="নতুন ব্যাংক যোগ করুন";
$lang['cancel']="বাতিল করুন";
$lang['validation_abbe']="ব্যাংকের সংক্ষিপ্ত নাম দিন ";
$lang['left_bank_abbe']="ব্যাংকের সংক্ষিপ্ত নাম";
$lang['left_update']="আপডেট";
$lang['comm_err']="দুঃখিত ! আবার চেষ্টা করুন ।";
$lang['charge_percentage']= "চার্জ (%)";
$lang['validation_charge_percentage']="চার্জের পরিমান দিন। কমপক্ষে ০ হতে হবে।";