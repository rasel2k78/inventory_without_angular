<?php 

/**
 *Bangla Language file for Expenditure_type controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "নতুন বিবরণী যোগ করুন";
$lang['right_portlet_title'] = "সকল বিবরণীর তালিকা";
$lang['left_name'] = "বিবরণীর নাম";
$lang['left_name_placeholder'] = "বিবরণীর নাম";
$lang['left_description'] = "বিবরণীর বিবরণ";
$lang['left_description_placeholder'] = "বিবরণীর বিবরণ";
$lang['left_save'] = "সেভ করুন";
$lang['left_cancle'] = "বাতিল করুন";
$lang['right_type'] = "ধরন";
$lang['right_description'] = "বিবরণ";
$lang['right_date'] = "তারিখ";
$lang['right_edit_delete'] = "পরিবর্তন / ডিলিট";
$lang['alert_delete_confirmation']="ডিলিট নিশ্চিতকরণঃ";
$lang['alert_delete_details']="আপনি কি এই তথ্য ডিলিট করতে চান ? ";
$lang['alert_edit_confirmation']="পরিবর্তন নিশ্চিতকরণঃ";
$lang['alert_edit_details']="আপনি কি এই তথ্য পরিবর্তন করতে চান ? ";
$lang['spec_name_list']="বৈশিষ্ট্যের তালিকা";

$lang['alert_delete_yes']="হ্যা";
$lang['alert_delete_no']="না";
$lang['alert_edit_yes']="হ্যা";
$lang['alert_edit_no']="না";

$lang['alert_delete_success']="সফলভাবে বিবরণী ডিলিট হয়েছে";
$lang['alert_delete_failure']="দুঃখিত !!বিবরণী ডিলিট হয়নি";
$lang['alert_edit_success']="সফলভাবে বিবরণী আপডেট হয়েছে";
$lang['alert_edit_failed']="দুঃখিত !! বিবরণী আপডেট হয়নি";
$lang['alert_insert_success']="সফলভাবে বিবরণী সেভ হয়েছে";
$lang['alert_insert_failed']="দুঃখিত !! বিবরণী সেভ হয়নি";
$lang['not_permitted'] = 'আপনার অনুমতি নেই';


$lang['edit_header']="বিবরণীর তথ্য পরিবর্তন করুন";
$lang['validation_name']="বিবরণীর নাম দিন";

$lang['model_edit']="পরিবর্তন";
$lang['model_delete']="ডিলিট";

$lang['left_name_new']="নতুন বিবরণী যোগ করুন";


$lang['right_spec_default_type']= "আইটেম ফর্মে এই ঘর ডিফল্ট থাকবে";
$lang['right_default_status_selected']="হ্যা";
$lang['right_default_status_not_selected']="না";


$lang['spec_insert_failure']="দুঃখিত !! বিবরণী সেভ হয়নি";
$lang['spec_insert_success']="সফলভাবে বিবরণী সেভ হয়েছে";


$lang['validation_check_spec_name']="এই বিবরণী একবার সেভ করা হয়েছে। অন্য নামের বিবরণী যোগ করুন।";
