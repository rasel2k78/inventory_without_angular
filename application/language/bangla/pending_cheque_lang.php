<?php 
/**
 *Bangla Language file for pending cheque controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="সকল প্রক্রিয়াধীন চেক";
$lang['sell_voucher']="রসিদ নং";
$lang['cheque_number']="চেক নং";
$lang['date']="তারিখ";
$lang['cash_or_acc']="ক্যাশ/ ব্যাংক গ্রহন";
$lang['chk_clr_by_acc']="ব্যাংক অ্যাকাউন্টে চেক গ্রহন";
$lang['select_acc']="ব্যাংক অ্যাকাউন্ট";
$lang['save']="সেভ";
$lang['cancel']="বাতিল ";
$lang['alert_confirmation'] ="চেক গ্রহন নিশ্চিতকরনঃ";
$lang['alert_details'] ="আপনি কি এই চেক ক্যাশ করতে চান ?";
$lang['alert_yes'] ="হ্যা";
$lang['alert_no'] =" না";
$lang['alert_success'] ="সফলভাবে সম্পন্ন হয়েছে";
$lang['alert_failure'] ="দুঃখিত! আবার চেষ্টা করুন";
$lang['no_acc_selected']="ব্যাংক অ্যাকাউন্ট নির্বাচন করুন ";
$lang['error_msg']="দুঃখিত! দয়া করে আবার চেষ্টা করুন";