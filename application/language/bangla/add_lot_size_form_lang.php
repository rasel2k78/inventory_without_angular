<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'নতুন লট সাইজের যোগ করুন!';
$lang['right_portlet_title'] = 'সকল লট সাইজের তালিকা';
$lang['lot_size_name'] = 'লট সাইজের পরিমাণ';
$lang['not_permitted'] = 'আপনার অনুমতি নেই';


$lang['save'] = 'সেভ করুন';
$lang['cancle'] = 'বাতিল করুন';
$lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
$lang['want_to_delete'] = 'আপনি কি লট সাইজের এই তথ্য ডিলিট করতে চান ?';
$lang['delete_success'] = 'সফলভাবে  লট সাইজের  তথ্য ডিলিট হয়েছে';
$lang['insert_failed'] = 'দুঃখিত !!  লট সাইজের  তথ্য সেভ হয়নি';
$lang['insert_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য সেভ হয়েছে';
$lang['update_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য আপডেট হয়েছে';
$lang['No'] = 'না';
$lang['Yes'] = 'হ্যাঁ';
$lang['delete'] = 'ডিলিট';
$lang['edit'] = 'পরিবর্তন';

$lang['validation_name']="লট সাইজের নাম দিন";

/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */