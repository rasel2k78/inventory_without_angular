<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['current_cashbox_amount']="Current Cash-box Amount";
$lang['today_total_sell']="Today's Total Sell";
$lang['today_total_buy']="Today's Total Buy";
$lang['today_total_expense']="Today's Total Expense";
$lang['all_shortage_list']="Running Low";
$lang['item_name']="Item Name";
$lang['quantity']="Quantity";
$lang['today_sell_list']="Today's Sell List";
$lang['customer_name']="Customer Name";
$lang['sell_voucher_no']="Voucher No";
$lang['today_buy_list']="Today's Buy List";
$lang['buy_voucher_no']="Voucher No";
$lang['grand_total']="Grand Total";
$lang['discount']="Discount";
$lang['paid']="Paid";
$lang['staff_wise_daily_sell']="Staff-wise Daily Sell";
$lang['staff_name']="Staff Name";
$lang['todays_sell']="Today's Total Sell";
$lang['due']="Due";
$lang['change']="Change";