<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['sign_in_title'] = 'Sign in';
$lang['email_place_holder'] = 'Email / Phone';
$lang['password_place_holder'] = 'Shop password';
$lang['login_button'] = 'LOGIN';
$lang['remember_check'] = 'Remember';
$lang['forgot_password_link'] = 'Forgot password?';
$lang['forgot_password_title'] = 'Forgot password';
$lang['recover_email_button'] = 'Recover by email';
$lang['recover_phone_button'] = 'Recover by phone';
$lang['back_button'] = 'Back';
$lang['submit_button'] = 'Submit';

$lang['forget_email_placeholder'] = 'Email (Internet required)';
$lang['forget_phone_placeholder'] = 'Phone Number (Internet required)';

$lang['empty_alert'] = 'Input fields can\'t be empty';
$lang['invalid_alert'] = 'Invalid information';
$lang['net_alert'] = 'Internet connection required for first time login';
$lang['mac_alert'] = 'Sorry unknown computer detected';


$lang['no_net']="Internet Connection Required";
$lang['empty_email']="Please Enter a Valid Email Address";
$lang['email_not_found']="Email Not Found in Record";

$lang['check_email']="Please Check Your Email For New Password";




