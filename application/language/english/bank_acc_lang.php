<?php 
/**
 *English Language file for Expenditure controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="Add New Bank Account";
$lang['right_portlet_title']="All Accounts List";
$lang['left_exp_type']="Select Bank";
$lang['left_exp_type_placeholder']="Select Bank";
$lang['left_acc']="A/C Number";
$lang['left_acc_placeholder']="A/C Number";
$lang['left_branch']="Branch";
$lang['left_branch_placeholder']="Branch";
$lang['left_expense_description']="Description";
$lang['left_expense_description_placeholder']="Description";
$lang['left_save']="Save";
$lang['left_cancle']="Cancel";
$lang['right_exp_type']="Bank Name";
$lang['right_ac']="A/C No.";
$lang['right_branch']="Branch";
$lang['right_date']="Date";
$lang['right_edit_delete']="Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ? ";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_delete_yes']="Yes";
$lang['alert_edit_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_no']="No";
$lang['alert_delete_success']="Bank Account Deleted Successfully";
$lang['alert_delete_failure']="Deletion Failed! Try Again ";
$lang['alert_edit_success']="Bank Account Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Bank Account Created Successfully";
$lang['alert_insert_failed']="Sorry! Try Again";
$lang['edit_header']="Edit Bank Account";
$lang['validation_type']="Select Bank From The List";
$lang['validation_account']="Enter A/C Number";
$lang['validation_branch']="Enter Branch Name";
$lang['validation_description']="Enter Description";
$lang['alert_insert_failed_modal']="Sorry!! Bank Not Saved";
$lang['alert_insert_success_modal']="Bank Saved Successfully";
$lang['type_select_tt']="Type keyword to select your desire bank from the bank list.";
$lang['left_exp_type_title']="Add New Bank";
$lang['right_balance']="Balance";
$lang['alert_active_success']="Account Activated Successfully";
$lang['alert_active_failed']="Activation Failed! Try Again";
$lang['alert_activate']="Activate Confirmation:";
$lang['alert_activate_details']="Do You Want To Activate This Account ?";
$lang['left_acc_name_placeholder']="Enter Account Name";
$lang['left_acc_name']="Account Name";
$lang['validation_name']="Enter Account Name";