<?php 

/**
 *English Language file for Expenditure_type controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "Add New Specification Title";
$lang['right_portlet_title'] = "All Specification Titles";
$lang['left_name'] = "Name";
$lang['left_name_placeholder'] = "Name";
$lang['left_description'] = "Description";
$lang['left_description_placeholder'] = "Description";
$lang['left_save'] = "Save";
$lang['left_cancle'] = "Cancel";
$lang['right_type'] = "Title";
$lang['right_description'] = "Description";
$lang['right_date'] = "Date";
$lang['right_edit_delete'] = "Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['not_permitted'] = 'No Permission';

$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";
$lang['spec_name_list']="Spec Name List's";

$lang['alert_delete_success']="Specification Title Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="Specification Title Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Specification Title Added Successfully";
$lang['alert_insert_failed']="Failed! Try Again";

$lang['edit_header']="Edit Specification Title";
$lang['validation_name']="Enter Specification Title Name";

$lang['model_edit']="Edit";
$lang['model_delete']="Delete";

$lang['left_name_new']="Add New Specification Title";

$lang['right_spec_default_type']= "Default Field In Item Form";
$lang['right_default_status_selected']="Yes";
$lang['right_default_status_not_selected']="No";


$lang['spec_insert_failure']="Sorry!! Specification Title Not Created";
$lang['spec_insert_success']="Specification Title Added Successfully";


$lang['validation_check_spec_name']="This Specification Title Have Already Created Once.";