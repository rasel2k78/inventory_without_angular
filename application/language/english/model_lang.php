<?php 
/**
 *English Language file for Inventory Model.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['model_edit']="Edit";
$lang['model_delete']="Delete";
$lang['model_paymentmade']="Payment made";
$lang['model_details']="Details";
$lang['model_payout']="Payout";
$lang['model_adjust']="Adjust";
$lang['model_active']="Activated";
$lang['model_deactive']="Deactivated";
$lang['model_empty']="Empty";
$lang['cleared_by_cash']="Cash";
$lang['cleared_by_acc']="Bank Account";