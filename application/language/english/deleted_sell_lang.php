<?php 
/**
 *English Language file for Sell_list_all controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="Deleted Sales List";
$lang['right_portlet_title'] ="Voucher Details";
$lang['left_voucher_no'] ="Voucher No";
$lang['left_customer']="Customer";
$lang['left_date'] ="Date";
$lang['left_details_delete'] ="Details / Delete";
$lang['right_voucher_no'] ="Voucher No";
$lang['right_net_payable'] ="Net Payable";
$lang['right_paid'] ="Paid";
$lang['right_due'] ="Due";
$lang['right_item_name'] ="Item Name";
$lang['right_quantity'] ="Quantity";
$lang['right_price'] ="Price /Unit";
$lang['right_sub_total'] ="Sub-total";
$lang['alert_delete_success'] ="Successfully Deleted";
$lang['alert_delete_failed'] ="Delete Failed! Please Try Again";
$lang['right_grand_total'] ="Grand Total";
$lang['right_discount'] ="Discount";
$lang['alert_delete_confirmation'] ="Delete Confirmation:";
$lang['alert_delete_details'] ="Do You Want To Delete This Information ?";
$lang['alert_delete_yes'] ="Yes";
$lang['alert_delete_no'] ="No";
$lang['customer_phone'] ="Customer Phone";
$lang['no_customer']="Empty";
$lang['voucher_print']="Print";
$lang['right_dis_type']="Dis. Type";
$lang['right_dis_amount']="Dis. Amt";
$lang['imei_or_serial']="IMEI/Serial";