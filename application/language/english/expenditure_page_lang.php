<?php 
/**
 *English Language file for Expenditure controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="Add New Expense";
$lang['right_portlet_title']="All Expense Lists";
$lang['left_exp_type']="Select Expense Type";
$lang['left_exp_type_placeholder']="Select Expense Type";
$lang['left_amount']="Amount";
$lang['left_amount_placeholder']="Amount";
$lang['left_date']="Date";
$lang['left_date_placeholder']="yyyy-mm-dd";
$lang['left_expense_description']="Description";
$lang['left_expense_description_placeholder']="Description";
$lang['left_save']="Save";
$lang['left_cancle']="Cancel";
$lang['right_exp_type']="Type";
$lang['right_amount']="Amount";
$lang['right_description']="Description";
$lang['right_date']="Date";
$lang['right_edit_delete']="Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ? ";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_delete_yes']="Yes";
$lang['alert_edit_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_no']="No";
$lang['alert_delete_success']="Expense Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again ";
$lang['alert_edit_success']="Expense Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Expense Saved Successfully";
$lang['alert_insert_failed']="Save Failed! Try Again";
$lang['edit_header']="Edit Expense";
$lang['validation_type']="Select Expense Type From The List";
$lang['validation_amount']="Enter Valid Amount";
$lang['validation_date']="Enter Date";
$lang['validation_description']="Enter Description";
$lang['total_amount_validation']="Not Enough Amount In Cash-box";
$lang['alert_insert_failed_modal']="Sorry!! Expenditure Type Not Created";
$lang['alert_insert_success_modal']="Expenditure Type Saved Successfully";
$lang['type_select_tt']="Type keyword to select your desire expenditure type from the type list.";
$lang['left_exp_type_title']="Add New Expenditure Type";
$lang['more_than_actual_amount']="Paid Amount Is Greater Than Net Payable Amount";
$lang['right_cash_type']="Select Payment Type";
$lang['right_type_cash']="Cash";
$lang['right_type_cheque']="Cheque";
$lang['cheque_page_empty']="Please Enter Cheque Number";
$lang['bank_acc_not_select']="Select A Bank Account";
$lang['select_acc']="Select A/C";
$lang['enter_chk_num']="Enter Cheque Number";
$lang['not_enough_balance']="Not Enough Balance In The Account";
$lang['validation_bank_acc']="Select Bank Account";
$lang['validation_chk_num']="Enter Cheque Number";
$lang['pre_date']= "Previous Date";