<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Add New Unit';
$lang['right_portlet_title'] = 'All Units List';
$lang['unit_name'] = 'Unit\'s Name';
$lang['unit_name_placeholder'] = 'Name of the unit';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['not_permitted'] = 'No Permission';
$lang['want_to_delete'] = 'Do You Want to Delete ?';
$lang['confirm_delete'] = 'Confirm Delete?';
$lang['unit_name_required'] = 'Unit\'s Name Cann\'t be Empty';
$lang['delete_success'] = 'Unit Deleted Successfully';
$lang['insert_failed'] = 'Sorry !!  Not Saved.Try Again';
$lang['insert_succeded'] = 'Unit Saved Successfully';
$lang['update_succeded'] = 'Unit Updated Successfully';
$lang['no_unit'] = 'Unit\'s Name Cann\'t be Empty';
$lang['edit_unit'] = 'Edit Unit Information';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */