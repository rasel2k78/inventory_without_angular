<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title_receive'] = 'Transfer Item Information';


$lang['transfer_no'] = 'Received Transfer Details No';
$lang['item'] = 'Item';
$lang['total_quantity'] = 'Total Quantity';
$lang['total_price'] = 'Total Price';
$lang['description'] = 'Description';
$lang['selling_price'] = 'Selling Price';


$lang['transfer_receive_list'] = 'Transfer Receive Lists';
$lang['transfer_from'] = 'Transfer From';
$lang['price'] = 'Price';
$lang['status'] = 'Status';                 

$lang['details'] = 'Details';
$lang['transfer_items'] = 'Transfer Item Lists';
$lang['item_on_inventory'] = 'Item on Inventory';
$lang['search_item'] = 'Search Item for match';
$lang['match_item'] = 'Match Item';
$lang['adjust_item'] = 'Adjust Item';
$lang['pic_item'] = 'Please pick item by search and click on adjust item on individual items for every row first';
$lang['pic_please'] = 'Please select an item from your inventory first';
$lang['match_item_tt'] = 'Pick similar item by search and click on right side button to replace transfer items with your items for each row';
$lang['item_on_tt'] = 'Item on your inventory that match to the transfer items';
$lang['adjust_click_tt'] = 'Click on this button by searching similar item of your inventory for each row';

$lang['item_invalid_price'] = 'Item Price is not correct';
$lang['give_item_quntity'] = 'Give item quantity';
$lang['give_item_price'] = 'Please give item price';
$lang['give_appropriate_amount'] = 'Give appropriate item amount from inventory';
$lang['finish'] = 'Items received successfully';
$lang['update'] = 'Update';
$lang['sorry'] = 'Sorry';
$lang['completed'] = 'Completed ?';
$lang['item_details'] = 'Item Details';
$lang['confirm_delete'] = 'Confirm Receive Transfer: ?';

$lang['want_to_update'] = 'Want to receive transfer? Same money will be deducted from your cashbook!!';
$lang['no'] = 'No';
$lang['yes'] = 'Yes';


$lang['receive'] = 'Receive';
$lang['cancle'] = 'Cancel';
/* End of file transfer_receive_item_lang.php */
/* Location: ./application/language/english/transfer_receive_item_lang.php */