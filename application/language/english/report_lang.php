<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['current_cashbox_amount']="Current Cash-box Amount";
$lang['today_total_sell']="Sales History";
$lang['today_total_buy']="Buy History";
$lang['today_total_expense']="Today's Total Expense";
$lang['all_shortage_list']="All Shortage Items List ";
$lang['item_name']="Item Name";
$lang['quantity']="Quantity";
$lang['today_sell_list']="Sell History";
$lang['customer_name']="Customer Name";
$lang['sell_voucher_no']="Voucher No";
$lang['today_buy_list']="Buy history";
$lang['buy_voucher_no']="Voucher No";
$lang['grand_total']="Grand Total";
$lang['discount']="Discount";
$lang['paid']="Paid";
$lang['staff_wise_daily_sell']="Staff-wise Daily Sell";
$lang['staff_name']="Staff Name";
$lang['todays_sell']="Sell history";
$lang['top_sell_items_lang']= "Top Items";
$lang['top_sales_rep_lang']= "Top Sales Staff";
$lang['top_customer_lang']="Top Customers";
$lang['expenses_lang']="Expenses";
$lang['demage_lost_lang']="Damage List";
$lang['due']="Due";
$lang['date']="Date";
$lang['payment_type']="Payment Type";
$lang['items_name']="Item's Name";
$lang['no_of_item_sold']="No. of Items Sold";
$lang['sales_rep_name']="Representative's Name";
$lang['no_of_sale']="Total Number of Sale";
$lang['customers_name']="Name";
$lang['amount_of_buy']="Total Amount of Purchase";
$lang['expenses_name_lang']="Name";
$lang['expenses_amount_lang']="Amount";
$lang['expenses_details_lang']="Details";
$lang['details']="Details";
$lang['vendor']="Vendor";
$lang['customer']="Customer";
$lang['vendor_lang'] ="Top Vendors";
$lang['no_of_buy']="Number of Purchase";
$lang['vendor_name']="Vendor's Name";
$lang['receive']="Received";
$lang['loan_lang']="Loan List";
$lang['buy_due_lang']="Buy Due List";
$lang['sell_due_lang']="Sell Due List";
$lang['loan_list_name']="Loan List";
$lang['loan_type_lang']="Type";
$lang['loan_amount_lang']="Loan Amount";
$lang['payment_made_lang']="Payment made Amount";
$lang['loan_date_lang']="Date";
$lang['buy_due_list_lang']="Buy Due List";
$lang['vendor_name_lang']="Vendor Name";
$lang['voucher_no_lang']="Voucher No";
$lang['grand_total_lang']="Grand Total";
$lang['paid_lang']="Paid";
$lang['due_lang']="Due";
$lang['date_lang']="Date";
$lang['sell_due_list_lang']="Sell Due List";
$lang['custom_report_lang']="Custom Report";
$lang['custom_report_header_lang']="Generate Custom Report";
$lang['report_type']="Select Report Type";
$lang['buy_report']="Buy Report";
$lang['sell_report']="Sell Report";
$lang['expense_report']="Expense Report";
$lang['damage_lost_report']="Damage/Lost Report";
$lang['loan_report']="Loan Report";
$lang['buy_due_report']="Buy Due Report";
$lang['sell_due_report']="Sell Due Report";
$lang['top_revenue_items']="Top Revenue Item";
$lang['profit_revenue_report']="Profit/Revenue Report";
$lang['date_range']="Date Range";
$lang['to']="To";
$lang['limit']="Limit";
$lang['limit_ten']="10";
$lang['limit_fifty']="50";
$lang['limit_hun']="100";
$lang['limit_two_hun']="200";
$lang['limit_all']="All";
$lang['report_with_vat']="Report With VAT";
$lang['vat_no']="No";
$lang['vat_yes']="Yes";
$lang['generate_report']="Custom Report";
$lang['custom_report']="Custom Report";
$lang['date_range_search']="Date Range";
$lang['item_report']="Item Inventory Report";
$lang['vat_report']="VAT Report";
$lang['dpst_wtdrl_report']="Deposit & Withdrawal Report ";
$lang['top_cat']="Top Categories Report";
$lang['all_cus']="All Customer";
$lang['all_vendor']="All Vendor";
$lang['total_amt_inventory_items']="Total Amount of Available Items";
$lang['vendor_payment_history_lang']="Vendor Payment History";
$lang['vendor_select']="Select Vendor";
$lang['individual']="Individual";
$lang['all']="All";
$lang['item_select']="Item List";
$lang['search_item']="Search Item";
$lang['cus_payment_history']="Customer Payment History";
$lang['select_customer']="Select Customer";
$lang['imei_report']="Available IMEI/Serial Report";
$lang['bank_report']="Bank Balance Report";
$lang['card_report']="Card Payment Report";