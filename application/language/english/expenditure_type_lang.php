<?php 

/**
 *English Language file for Expenditure_type controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "Add New Expense Type";
$lang['right_portlet_title'] = "All Expense Types";
$lang['left_expense_type_name'] = "Type Name";
$lang['left_expense_type_name_placeholder'] = "Type Name";
$lang['left_expense_description'] = "Type Description";
$lang['left_expense_description_placeholder'] = "Type Description";
$lang['left_save'] = "Save";
$lang['left_cancle'] = "Cancel";
$lang['right_expense_type'] = "Type";
$lang['right_description'] = "Description";
$lang['right_date'] = "Date";
$lang['right_edit_delete'] = "Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";

$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";

$lang['alert_delete_success']="Expense Type Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="Expense Type Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Expense Type Created Successfully";
$lang['alert_insert_failed']="Failed! Try Again";

$lang['edit_header']="Edit Expense Type";
$lang['validation_name']="Enter Expenditure Type Name";

$lang['model_edit']="Edit";
$lang['model_delete']="Delete";

$lang['left_expense_type_name_new']="Add New Expenditure Type";

$lang['cancel']="Cancel";