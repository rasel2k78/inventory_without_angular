<?php 

/**
 *English Language file for Loan controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title']="Add New Loan";
$lang['right_portlet_title']="Loan List";

$lang['left_amount']="Amount";
$lang['left_amount_placeholder']="Amount";
$lang['left_payout']="Payout";
$lang['left_loan_description']="Description";
$lang['left_loan_description_placeholder']="Description";
$lang['left_save']="Save";
$lang['left_cancle']="Cancel";
$lang['right_type']="Type";
$lang['right_amount']="Amount";
$lang['right_description']="Description";
$lang['right_date']="Date";
$lang['right_edit_delete']="Edit / Adjust / Delete";

$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_adjust_confirmation']="Adjust Confirmation:";
$lang['alert_adjust_details']="Do You Want To Adjust This ?";

$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";

$lang['alert_delete_success']="Loan Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="Loan Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Loan Created Successfully";
$lang['alert_insert_failed']="Failed! Try Again";
$lang['alert_adjust_success']="Loan Adjusted Successfully";
$lang['alert_adjust_failed']="Failed! Try Again";
$lang['default_error']="Sorry! Try Again";
$lang['delete_error']="Sorry !! Delete Not Possible...Please Adjust ";
$lang['payout_success']="Loan Payout Successfully Completed";

$lang['edit_header']="Edit Loan";

$lang['validation_amount']="Enter Valid Amount";
$lang['total_loan_paid']="Paid";
$lang['alert_more_paid']="Sorry. Payout Amount Is More Than Loan Amount";


$lang['alert_access_failure']="Sorry! You Have No Permission";
$lang['payout_header']="Loan Payout Form";
$lang['not_enough_amount']="Sorry! Not Sufficient Amount in Cash-box";
$lang['more_payout_than_loan']="Payout Amount Is Greater Than Loan Amount";
$lang['total_payout_validation']="Sorry! More Loan Amount Paid Than Given Amount";

$lang['loan_paid']="Payout";
$lang['remaining_loan'] = "Remaining";