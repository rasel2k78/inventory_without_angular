<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Add New Size';
$lang['right_portlet_title'] = 'All Sizes List';
$lang['Size_name'] = 'Size Name';
$lang['Size_name_placeholder'] = 'Name of the Size';
$lang['delete_success'] = 'Size Deleted Successfully';
$lang['insert_failed'] = 'Sorry !!  Size did not Saved';
$lang['insert_succeded'] = 'Size Saved Successfully';
$lang['update_succeded'] = 'Size Updated Successfully';
$lang['edit_size_name'] = 'Edit Size';
$lang['not_permitted'] = 'No Permission';
$lang['Edit'] = 'Edit';
$lang['Delete'] = 'Delete';
$lang['want_to_delete'] = 'Do You Want to Delete ?';
$lang['Delete_confirm'] = 'Confirm Delete:';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';

$lang['validation_name'] = "Size Name Can't be Empty";


/* End of file size_page_lang.php */
/* Location: ./application/language/english/size_page_lang.php */