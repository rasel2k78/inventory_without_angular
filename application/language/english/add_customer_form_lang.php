<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_customer'] = 'Add New Customer';
$lang['all_customer'] = 'All Customers';
$lang['customer_name'] = 'Full Name';

$lang['customer_email'] = 'Email';
$lang['customer_id'] = 'Customer ID';
$lang['customer_present_address'] = 'Present Address';
$lang['customer_permanent_address'] = 'Permanent Address';
$lang['customer_phone'] = 'Phone 1';
$lang['customer_phone_2'] = 'Phone 2';
$lang['customer_nid'] = 'Customer NID';
$lang['customer_image'] = 'Image';
$lang['net_alert'] = 'Sample';
$lang['id_exist_alert'] = 'Customer ID already exist';

$lang['select_image'] = 'Select Image';
$lang['change'] = 'Change';
$lang['delete'] = 'Delete';

$lang['save'] = 'Save';
$lang['cancel'] = 'Cancel';

$lang['name'] = 'Name';
$lang['mobile'] = 'Mobile';

$lang['change_delete'] = 'Change / Delete';

$lang['yes'] = 'Yes';
$lang['no'] = 'No';


$lang['delete_success'] = 'Successfully Deleted';
$lang['no_permission_left'] = 'No Permission';
$lang['insert_failure'] = 'Sorry! Not Created. Try Again';
$lang['user_insert_success'] = 'Information Saved Successfully';
$lang['update_success'] = 'Information Saved Successfully';
$lang['no_username'] = 'Customer name can not be empty';
$lang['do_you_want_delete'] = 'Do you want to delete the customer ?';
$lang['confirm_delete'] = 'Confirm delete customer';

$lang['deletion_success'] = 'Successfully deleted';
$lang['no_deletion'] = 'Deletion failed';


$lang['change_customer_info'] = 'Change customer information';
$lang['update_info'] = 'Update info';
$lang['image_of_customer'] = 'Customer image';

$lang['customer_exist'] = 'Customer with same mail already exist';


$lang['customer_insert_success'] = 'Customer Created Successfully';


$lang['no_phone']="Phone can't be empty";

$lang['no_username_customer'] = 'Customer name can not be empty';
$lang['customer_already_exists'] = 'Customer Already Exists';