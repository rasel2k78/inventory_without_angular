<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Add New Brand';
$lang['right_portlet_title'] = 'All Brand\'s List';
$lang['brand_name'] = 'Brand\'s Name';
$lang['edit_brand'] = 'Edit Brand Information';
$lang['brand_description'] = 'Brand Description';
$lang['brand_select_picture'] = 'Select Brand\'s Image';
$lang['confirm_delete'] = 'Confirm Delete:';
//$lang['brand_name'] = 'আপনি কি জমা / উত্তোলনের এই তথ্য ডিলিট করতে চান';
$lang['select_brand'] = 'Select Brand';
$lang['change_brand_info'] = 'Change Brand Information';
$lang['brand_logo'] = 'Brand\'s Logo';
$lang['want_to_delete'] = 'Do You Want to Delete?';
$lang['not_permitted'] = 'No Permission';
$lang['brand_name_required'] = 'Brand\'s Name Is Empty';
$lang['delete_success'] = 'Brand Deleted Successfully';
$lang['insert_failure'] = 'Sorry !! Brand Not Saved';
$lang['insert_success'] = 'Brand Save Successfully';
$lang['update_success'] = 'Brand Updated Successfully';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';


/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */