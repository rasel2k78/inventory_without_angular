<?php 
/**
 *English Language file for Left Nevigation Bar.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['item']="Item";
$lang['dashboard']="Dashboard";
$lang['cashbox']="Cashbook";
$lang['deposit_withdrawal']="Deposit / Withdrawal";
$lang['loan']="Loan";
$lang['add_item']="Add Item";
$lang['add_brand']="Add Brand";
$lang['add_category']="Add Category";
$lang['add_unit']="Add Unit";
$lang['add_vendor']="Add Vendor";
$lang['add_size']="Add Size";
$lang['add_lot_size']="Add Lot-size";
$lang['buy']="Buy";
$lang['buy_items']="Buy";
$lang['buy_due_list']="Due List";
$lang['buy_all_list']="All Purchases List";
$lang['advance_buy']="Advance Payment";
$lang['sell']="Sell";
$lang['sell_items']="Sell";
$lang['sell_due_list']="Due List";
$lang['sell_all_list']="All Sales List";
$lang['add_tax_type']="Add Tax Type";
$lang['add_customer']="Add Customer";
$lang['demage_and_lost']="Damage / Lost";
$lang['expenditure']="Expenditure";
$lang['add_exp_type']="Add Expenditure Type";
$lang['add_exp']="Add Expenditure";
$lang['exchange_return']="Exchange / Return";
$lang['exchange_with_vendor']="With Vendor";
$lang['exchange_with_customer']="With Customer";
$lang['money_return_customer']="Customer Money Return";
$lang['money_return_vendor']="Vendor Money Return";
$lang['user']="User";
$lang['add_user']="Add User";
$lang['attendance']="Attendance";
$lang['user_access_panel']="User Access Panel";
$lang['shop_info']="Shop & Printer Setup";
$lang['transfer']="Send Items";
$lang['sync']="Sync";
$lang['update']="Update";
$lang['report']="Reports";
$lang['add_sales_rep']="Sales Representative";
$lang['Specification']="Specification";
$lang['Return money to vendor']="Return money to vendor";
$lang['Return money to customer']="Return money to customer";
$lang['vat']="Add VAT";
$lang['add_spec_left_nav']="Add Specification Title";
$lang['money_sign']="৳";
$lang['no_permission']="Sorry!! No Permission. Please Contact With Owner or Manager";
$lang['transfer']="Transfer Items"; 
$lang['transfer_list']="Transfer List";
$lang['transfer_receive_list']="Transfer Receive list";
$lang['settings_report']="Settings";
$lang['vat']= "VAT";
$lang['video_tut']="Video Tutorial";
$lang['bank']="Bank";
$lang['add_new_bank']="Add New Bank";
$lang['add_bank_accounts']="Add Bank Accounts";
$lang['bank_depo_with']="Deposit / Withdrawal";
$lang['pending_cheques']="Pending Cheques";
$lang['printer_setting']="Printer Setting";
$lang['contact_us']="Contact Us";
$lang['database_backup']="Database Backup";
$lang['warehouse_receive_list']="Warehouse Receive Lists";
$lang['Warehouse'] = 'Warehouse';