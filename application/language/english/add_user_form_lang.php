<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['header_text_left'] = 'Add New User';
$lang['username'] = 'User Name';
$lang['user_role'] = 'User Role';
$lang['staff'] = 'Staff';
$lang['manager'] = 'Manager';
$lang['password'] = 'Password';
$lang['repass'] = 'Confirm Password';
$lang['email'] = 'Email';
$lang['phone'] = 'Phone 1';
$lang['phone2'] = 'Phone 2';
$lang['present_address'] = 'Present Address';
$lang['permanent_address'] = 'Permanent Address';
$lang['user_image'] = 'Image';
$lang['user_image_select'] = 'Select User Image';
$lang['change'] = 'Change';
$lang['delete'] = 'Delete';
$lang['save'] = 'Save';
$lang['cancel'] = 'Cancel';

$lang['delete_success'] = 'Successfully Deleted';
$lang['insert_failure'] = 'Sorry! User Not Created. Try Again';
$lang['user_insert_success'] = 'User Created Successfully';
$lang['update_success'] = 'User Information Updated Successfully';
$lang['no_username'] = 'User name Can\'t be Empty';
$lang['no_mobile'] = 'Phone 1 Can\'t be Empty';
$lang['no_password'] = 'Password Field Empty';
$lang['no_match'] = 'Password n\'t Match or Empty';
$lang['email_exist'] = 'Email Already Exist';
$lang['username_exist'] = 'User name Already Exist';
$lang['short_pass'] = 'Password Length Minimum 4 Characters';
$lang['no_permission_u_create'] = 'You are not Permitted';

$lang['user_deleted'] = 'User Deleted Successfully';
$lang['user_not_deleted'] = 'Sorry! Deletion Failed. Try Again';
$lang['no_permission_right'] = 'You are not permitted';
$lang['all_user_list'] = 'All Users List';
$lang['name_th'] = 'Name';
$lang['mobile_th'] = 'Mobile';
$lang['edit_delete_th'] = 'Edit / Delete';
$lang['manage_u_acc'] = 'Manage User Access';
$lang['sure_to_delete'] = 'User Delete Confirmation';
$lang['confirm_delete_user'] = 'Do You Want to Delete This?';
$lang['no'] = 'No';
$lang['yes'] = 'Yes';

$lang['change_user_info'] = 'Edit User Information';
$lang['change_user_image'] = 'User Image (Not changeable from here)';

$lang['infor_update'] = 'Information Update';

$lang['username_tt'] = 'Type a user name';
$lang['user_role_tt'] = 'Select a user role';
$lang['password_tt'] = 'Type a password here';
$lang['repass_tt'] = 'Re-type the password here';
$lang['email_tt'] = 'Type an email here';
$lang['phone_tt'] = 'Type a phone number here';
$lang['phone2_tt'] = 'Type a secondary phone here';
$lang['present_address_tt'] = 'Type a present address here';
$lang['permanent_address_tt'] = 'Type a permanent address here';

$lang['no_email'] = 'Email can not be empty';
