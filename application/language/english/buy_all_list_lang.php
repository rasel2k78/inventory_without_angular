<?php 
/**
 * English Language file for Buy_list_all controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="All Purchase List";
$lang['right_portlet_title'] ="Payment made Form";
$lang['left_voucher_no'] ="Voucher No";
$lang['left_purchase_date'] ="Date";
$lang['left_details_delete'] ="Details / Delete";
$lang['right_voucher_no'] ="Voucher No";
$lang['right_net_payable'] ="Net Payable";
$lang['right_paid'] ="Paid";
$lang['right_due'] ="Due";
$lang['right_item_name'] ="Item Name";
$lang['right_quantity'] ="Quantity";
$lang['right_purchase_price'] ="Purchase Price/Unit";
$lang['right_sub_total'] ="Sub-total";
$lang['alert_paymentmade_success'] ="Successfully Updated";
$lang['alert_paymentmade_failed'] ="Update Failed! Please Try Again";
$lang['right_grand_total'] ="Grand Total";
$lang['right_discount'] ="Discount";
$lang['alert_delete_confirmation'] ="Delete Confirmation:";
$lang['alert_delete_details'] ="Do You Want To Delete This Information ?";
$lang['alert_delete_yes'] ="Yes";
$lang['alert_delete_no'] ="No";
$lang['alert_delete_success'] ="Successfully Deleted";
$lang['alert_delete_failure'] ="Sorry! Delete Failed.Try Again";
$lang['right_landed_cost']="Landed Cost";
$lang['voucher_print']="Print";
$lang['right_dis_type']="Dis. Type";
$lang['right_dis_amount']="Dis. Amt";
$lang['left_details_vendor']="Vendor";
$lang['delete_failure_for_insuff_qty']="Can not Delete This Voucher Due To Insufficient Quantity of Item";
$lang['adv_buy_delete']="Advance Buy Deleted Successfully";
$lang['adv_buy_list']="All Advance Purchase List";
$lang['change_date']="Change Date";
$lang['edit_adv_buy']="Edit Advance Buy";
$lang['adv_paid']="Total Advance Paid Amount";
$lang['receivable_date']="Next Receivable Date";
$lang['cancel']="Cancel";
$lang['update']="Update";
$lang['buy_details']="Purchase Details";
$lang['complete_order']="Complete Order";