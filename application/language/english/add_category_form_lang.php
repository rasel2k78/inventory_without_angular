<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] = 'Add New Category';
$lang['right_portlet_title'] = 'All Categories List';
$lang['delete_success'] = 'Category Deleted Successfully';
$lang['insert_failed'] = 'Already exist category';
$lang['insert_succeded'] = 'Category Saved Successfully';
$lang['update_succeded'] = 'Category Updated Successfully';
$lang['edit_category'] = 'Edit Category Information';
$lang['category_name'] = 'Category Name';
$lang['category_description'] = 'Category Description ';
$lang['main_category_name'] = 'Main Category(If exist)';
$lang['not_permitted'] = 'No Permission';
$lang['confirm_delete'] = 'Confirm Delete';
$lang['select_category'] = 'Select Category';
$lang['edit_category'] = 'Edit Category Information';
$lang['want_to_delete'] = 'Do You Want To Delete This?';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['validation_name'] = "Category Name Can't be Empty";
/* End of file add_category_form_lang.php */
/* Location: ./application/language/english/add_category_form_lang.php */