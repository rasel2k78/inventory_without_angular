<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Exchanged Items List';
$lang['voucher_no'] = 'Voucher Number';
$lang['item'] = 'Item';
$lang['quantity'] = 'Quantity';
$lang['total_quantity'] = 'Quantity';
$lang['buying_price'] = 'Price';
$lang['sub_total'] = 'Sub Total';
$lang['Delete_items'] = 'Delete';
$lang['total'] = 'Total:';
$lang['new_item_buy_info'] = 'Buy New Item';
$lang['not_permitted'] = 'No Permission';
$lang['total_paid'] = 'Total Paid:';
$lang['exchange_with_vendor_info'] = 'Exchange Item  With Vendor';
$lang['find'] = 'Search';
$lang['item_buy_details'] = 'Voucher Details';
$lang['item_name'] = 'Item Name';
$lang['buying_price_unit'] = 'Buying Price/Unit';
$lang['new_quantity'] = 'Exchanged Quantity';
$lang['total_bill'] = 'Total Bill';
$lang['select_item'] = 'Select Item';
$lang['give_item_quantity'] = 'Give Item Quantity';
$lang['sell_price_unit'] = 'Selling Price /Unit (Retail)';
$lang['single_price'] = 'Retail Price';
$lang['whole_price'] = 'Wholesale Price';
$lang['sell_price_whole'] = 'Selling Price /Unit (Wholesale)';
$lang['deadline'] = 'Expire date';
$lang['add_new_item'] = 'Add New Item';
$lang['total_new_amount'] = 'Total Amount';
$lang['receivable'] = 'Receivable';
$lang['payable'] = 'Payable';
$lang['total_payable'] = 'Total payable';
$lang['sorry'] = 'Sorry';
$lang['give_buying_price'] = 'Give a buying price';
$lang['select_item_tt'] = 'Select new item to exchange';
$lang['exchange_quantity_tt'] = 'Give Item quantity to exchange';
$lang['get_voucher_tt'] = 'Give Voucher Number to get voucher information for exchange purpose';
$lang['item_list_tt'] = 'Exchanged items list';
$lang['total_price_tt'] = 'Exchanged items total price';
$lang['item_new_tt'] = 'New items after exchange';
$lang['item_new_total_price_tt'] = 'Exchanged new items total amount';
$lang['item_new_paid_tt'] = 'Total paid of exchanged items';
$lang['amount_exchange_tt'] = 'Amount after exchange with new items';
$lang['amount_after_paid_tt'] = 'Amount after paid';


$lang['Due'] = 'Due';
$lang['exchanged_quantity_exceded'] = 'Exchange quantity exceeded previous quantity';
$lang['not_exchanged'] = 'Sorry! No Item Exchanged';
$lang['exchange_success'] = 'Item Exchanged Successfully';
$lang['no_new_item'] = 'Sorry! No New Item Added';
$lang['no_voucher'] = 'Sorry! Field Empty or Requested Voucher Not Found';
$lang['no_items'] = 'Sorry! No Item on This Voucher';

$lang['give_voucher_number'] = 'Give Voucher Number to get voucher information for exchange purpose';


$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
// $lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
// $lang['want_to_delete'] = 'আপনি কি লট সাইজের এই তথ্য ডিলিট করতে চান ?';
// $lang['delete_success'] = 'সফলভাবে  লট সাইজের  তথ্য ডিলিট হয়েছে';
// $lang['insert_failed'] = 'দুঃখিত !!  লট সাইজের  তথ্য সেভ হয়নি';
// $lang['insert_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য সেভ হয়েছে';
// $lang['update_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য আপডেট হয়েছে';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */