<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_sales_rep'] = 'Add New Sales Representative';
$lang['all_sales_rep'] = 'All Sales Representatives';
$lang['sales_rep_name'] = 'Name';

$lang['sales_rep_email'] = 'Email';

$lang['sales_rep_present_address'] = 'Present address';
$lang['sales_rep_permanent_address'] = 'Permanent address';
$lang['sales_rep_phone'] = 'Phone';
$lang['sales_rep_phone_2'] = 'Phone 2';
$lang['sales_rep_nid'] = 'National ID';
$lang['sales_rep_image'] = 'Image';
$lang['net_alert'] = 'Sample';

$lang['select_image'] = 'Select image';
$lang['change'] = 'Change';
$lang['delete'] = 'Delete';

$lang['save'] = 'Save';
$lang['cancel'] = 'Cancel';

$lang['name'] = 'Name';
$lang['mobile'] = 'Mobile';
$lang['date'] = 'Date';


$lang['change_delete'] = 'Change / Delete';

$lang['yes'] = 'Yes';
$lang['no'] = 'No';


$lang['delete_success'] = 'Successfully deleted';
$lang['no_permission_left'] = 'You have no permission';
$lang['insert_failure'] = 'Sorry! Sales Representative Not Created.';
$lang['user_insert_success'] = 'Sales Representative Created Successfully';
$lang['update_success'] = 'Update success';
$lang['no_username'] = 'Representative\'s Name Can\'t be Empty';
$lang['do_you_want_delete'] = 'Do You Want to Delete ?';
$lang['confirm_delete'] = 'Confirm Delete';

$lang['deletion_success'] = 'Successfully deleted';
$lang['no_deletion'] = 'Deletion failed';


$lang['change_sales_rep_info'] = 'Change Representative\'s information';
$lang['update_info'] = 'Update info';
$lang['image_of_sales_rep'] = 'Image';

$lang['sales_rep_exist'] = 'Email already exist';



$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit ?";

$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";


$lang['alert_delete_success']="Sales Representative Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="Sales Representative Updated Successfully";
$lang['alert_edit_failed']="Update Failed! Try Again";
$lang['alert_insert_success']="Sales Representative Created Successfully";
$lang['alert_insert_failed']="Failed! Try Again";

$lang['edit_header']="Edit Sales Representative's Information";
$lang['no_name']='Name Can\'t be Empty';

$lang['alert_access_failure']="No Permission. Contact With Owner or Manager";

