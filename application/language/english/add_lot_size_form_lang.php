<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Add New Lot Size';
$lang['right_portlet_title'] = 'All Lot Size\'s List';
$lang['lot_size_name'] = 'Lot Size Amount';
$lang['not_permitted'] = 'No Permission';


$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['confirm_delete'] = 'Confirm Delete:';
$lang['want_to_delete'] = 'Do You Want to Delete ?';
$lang['delete_success'] = 'Lot Size Deleted Successfully';
$lang['insert_failed'] = 'Sorry !!  Lot Size Not Saved';
$lang['insert_succeded'] = 'Lot Size Inserted Successfully';
$lang['update_succeded'] = 'Lot Size Updated Successfully';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';

$lang['validation_name']="Lot Size Can't be Empty";
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */