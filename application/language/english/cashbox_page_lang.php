<?php 
/**
 *English Language file for Nagadanboi controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title']="Deposit / Withdrawal Form";
$lang['right_portlet_title']="Deposit / Withdrawal List";
$lang['left_type']="Select Type";
$lang['left_type_deposit']="Deposit";
$lang['left_type_withdrawal']="Withdrawal";
$lang['left_amount']="Amount";
$lang['left_date']="Date";
$lang['left_amount_placeholder']="Amount";
$lang['left_description']="Description";
$lang['left_description_placeholder']="Description";
$lang['left_save']="Save";
$lang['left_cancle']="Cancel";
$lang['right_type']="Type";
$lang['right_amount']="Amount";
$lang['right_description']="Description";
$lang['right_date']="Date";
$lang['right_edit_delete']="Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";
$lang['alert_delete_success']="Delete Completed Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again ";
$lang['alert_edit_success']="Edit Complete Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Successfully Deposited";
$lang['alert_insert_failed']="Deposition Failed! Try Again";
$lang['validation_amount']="Enter Valid Amount";
$lang['edit_cashbox']="Edit Deposit / Withdrawal Form";
$lang['withdrawal_failure']="Not Enough Money In Cash-box";
$lang['more_amount_than_cashbox']= "Not Enough Money In Cash-box";
$lang['select_acc']="Select Account";
$lang['transfer_to_bank']="Transfer to Bank ";
$lang['left_commission']="Commission";
$lang['right_select_vendor']="Select Vendor";