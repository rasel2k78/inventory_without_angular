<?php 
/**
 *English Language file for Loan controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_header']= "Damage / Lost Form";
$lang['left_type']="Type" ;
$lang['demage']="Damage" ;
$lang['lost']="Lost" ;
$lang['left_item']="Select Item" ;
$lang['left_item_select']="Select Item From The List" ;
$lang['left_quantity']= "Quantity";
$lang['left_description']="Description" ;
$lang['left_save']= "Save";
$lang['left_cancle']= "Cancel";
$lang['left_valid_quantity']= "Enter Valid Quantity";
$lang['right_header']= "Damage / Lost List";
$lang['right_type']= "Type";
$lang['right_item_name']= "Item";
$lang['right_quantity']= "Quantity";
$lang['right_description']="Description" ;
$lang['right_date']= "Date";
$lang['right_edit_delete']="Edit / Delete" ;
$lang['left_valid_item']="Select Item From The List";
$lang['left_edit']="Edit Damage / Lost Form";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_adjust_confirmation']="Adjust Confirmation:";
$lang['alert_adjust_details']="Do You Want To Adjust This ?";
$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";
$lang['alert_delete_success']=" Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']=" Updated Successfully";
$lang['alert_edit_failed']="Update Failed! Try Again";
$lang['alert_insert_success']="Saved Successfully";
$lang['alert_insert_failed']="Failed! Try Again";
$lang['default_error']="Sorry! Please Try Again";
$lang['left_valid_more_quantity']= "More Quantity Provided Than current Quantity";
$lang['alert_access_failure']="Sorry!! No Permission. Contact With Owner or Manager";
$lang['item_select_tt']="Type keyword to select your desire item from item list";
$lang['left_buying_price']="Buying Price";
$lang['left_valid_buying_price']="Enter Buying Price of The Item";
$lang['imei_used']="IMEI/SERIAL Already Used or Deleted.";
$lang['one_qty_for_imei']="Quantity Must Be 1 For IMEI/SERIAL";
$lang['wrong_imei']="Wrong IMEI Inserted";