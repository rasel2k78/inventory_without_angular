<?php 

/**
 *English Language file for Sell_list_due controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="All Sales Due List";
$lang['right_portlet_title'] ="Payment Made Form";
$lang['left_customer']= "Customer";
$lang['left_voucher_no'] ="Voucher No";
$lang['left_purchase_date'] ="Date";
$lang['left_paymentmade'] ="Payment Made";
$lang['right_voucher_no'] ="Voucher No";
$lang['right_net_payable'] ="Grand Total";
$lang['right_paid'] ="Paid";
$lang['right_due'] ="Due";
$lang['right_paymentmade_amount'] ="Payment Made Amount";
$lang['right_paymentmade_amount_placeholder'] ="Payment Made";
$lang['right_description'] ="Description";
$lang['right_description_placeholder'] ="Description";
$lang['right_save_button'] ="Save";
$lang['right_cancle_button'] ="Cancel";
$lang['right_item_name'] ="Item";
$lang['right_quantity'] ="Quantity";
$lang['right_selling_price'] ="Price/Unit";
$lang['right_sub_total'] ="Sub-total";
$lang['alert_paymentmade_success'] ="Successfully Payment Made Information updated";
$lang['alert_paymentmade_failed'] ="Update Failed! Please Try Again";
$lang['validation_amount']="Enter Valid Amount";
$lang['extra_sell_due_amount']= "Sorry! Payment Made Amount Is Larger Than Due Amount";
$lang['right_type_both']="Cash and Card";
$lang['right_card_amount']="Payment Through Card";
$lang['right_cash_type']= "Payment Type";
$lang['right_type_cash']="Cash";
$lang['right_type_card']="Card";
$lang['right_card_voucher_no']="Card Payment Voucher";
$lang['card_voucher_err_msg']= "Card Payment Voucher Is Empty or Not a Valid Number";
$lang['card_amount_larger']="Card Amount Is Larger Than Total Payment Made Amount";
$lang['no_customer']="Empty";
$lang['voucher_print']="Print";
$lang['right_dis_type']="Dis. Type";
$lang['right_dis_amount']="Dis. Amt";
$lang['sell_due_paymentmade_tt']="Enter payment made amount to adjust due. Larger payment made amount than total due is not allowed.";
$lang['chk_num_empty']="Cheque Number Empty";
$lang['chk_num_already_exist']="Cheque Number Already Exist";
$lang['chk_number']="Cheque Number";
$lang['cheque']="Cheque";
$lang['cheque_number_empty']="Cheque Number Empty";
$lang['cheque_number_already_exist']="This Cheque Already Exist";
$lang['paymentmade_less_than_card_amt']="Payment made Amount Is Less Than Card Payment Amount";
$lang['select_customer']="Select Customer";
$lang['right_customer_placeholder']="Select Customer";
$lang['right_cancle_button']="Cancel";
$lang['search']="Search";
$lang['search_customer']="Search Customer Form";
$lang['validation_msg']="Please Enter Valid Amount";
$lang['extra_amount_inserted']="Payment Made Amount Is Greater Than Due Amount";
$lang['cutomer_name']="Customer";
$lang['select_acc']="Select A/C";
$lang['charge'] = "Charge";