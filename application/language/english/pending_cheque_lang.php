<?php 
/**
 *English Language file for pending cheque controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="All Pending Cheque";
$lang['sell_voucher']="Sell Voucher";
$lang['cheque_number']="Cheque Number";
$lang['date']="Date";
$lang['cash_or_acc']="Cash/Bank Receive";
$lang['chk_clr_by_acc']="Cheque Clear By Bank";
$lang['select_acc']="Select Account";
$lang['save']="Save";
$lang['cancel']="Cancel";
$lang['alert_confirmation'] ="Cheque Clear Confirmation:";
$lang['alert_details'] ="Do You Want To Cash This Cheque ?";
$lang['alert_yes'] ="Yes";
$lang['alert_no'] ="No";
$lang['alert_success'] ="Transaction Successfully Completed";
$lang['alert_failure'] ="Sorry! Transaction Failed.Try Again";
$lang['no_acc_selected']="Please Select A Bank Account";
$lang['error_msg']="An error has occurred. PLease try again";