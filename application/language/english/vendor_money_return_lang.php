<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Money Return With Vendor';
$lang['find'] = 'Find';
$lang['voucher_no'] = 'Voucher Number';
$lang['buy_details'] = 'Purchase Details';
$lang['item_name'] = 'Item';
$lang['quantity'] = 'Quantity';
$lang['buying_price'] = 'Purchase price(Unit)';
$lang['sub_total'] = 'Sub-total';
$lang['total_bill'] = 'Total Bill';
$lang['total_paid'] = 'Total Paid';
$lang['total_receivable'] = 'Total Receivable';
$lang['return_money'] = 'Return Money';
$lang['not_permitted'] = 'Not Permitted';
$lang['return_success'] = 'Successfully Money Returned to Vendor';
$lang['no_return_amount'] = 'No Amount of Money to Return';
$lang['no_voucher'] = 'Sorry ! No Voucher Exist';
$lang['give_voucher'] = 'Please give voucher number';
$lang['due_first'] = 'Please pay due first.Money return is not possible now';
$lang['returned_items'] = 'Returned Items List';
$lang['buying_price'] = 'Buying Price';
$lang['total'] = 'Total';
$lang['return_quantity'] = 'Return Quantity';
$lang['total_due'] = 'Total Due';
$lang['item_not_inventory'] = 'Sorry!!Item quantity is not in inventory stock.';




$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/return_money_form_lang.php */