<?php 
/**
 *English Language file for Bank controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] = "Add New Bank";
$lang['right_portlet_title'] = "All Banks List";
$lang['left_bank_name'] = "Bank Name";
$lang['left_bank_name_placeholder'] = "Bank Name";
$lang['left_bank_description'] = "Bank Description";
$lang['left_bank_description_placeholder'] = "Bank Description";
$lang['left_save'] = "Save";
$lang['left_cancle'] = "Cancel";
$lang['right_bank'] = "Bank Name";
$lang['right_description'] = "Charge Percentage";
$lang['right_date'] = "Date";
$lang['right_edit_delete'] = "Edit / Delete";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";
$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";
$lang['alert_delete_success']="Bank Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="Bank Edited Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="Bank Created Successfully";
$lang['alert_insert_failed']="Failed! Try Again";
$lang['edit_header']="Edit Bank Information";
$lang['validation_name']="Enter Bank Name";
$lang['model_edit']="Edit";
$lang['model_delete']="Delete";
$lang['left_bank_name_new']="Add New Bank";
$lang['cancel']="Cancel";
$lang['validation_abbe']="Enter Bank Short Form";
$lang['left_bank_abbe']="Bank Short Name";
$lang['left_update']="Update";
$lang['comm_err']="An error has occurred. Please try again";
$lang['charge_percentage']= "Charge (%)";
$lang['validation_charge_percentage']="Enter Percentage for Card Payment. Minimum 0";