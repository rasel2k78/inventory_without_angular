<?php 
/**
 *English Language file for Buy_list controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['left_portlet_title'] ="All Purchase Due List";
$lang['right_portlet_title'] ="Payment made Form";
$lang['left_voucher_no'] ="Voucher No";
$lang['left_purchase_date'] ="Date";
$lang['left_paymentmade'] ="Payment made";
$lang['right_voucher_no'] ="Voucher No";
$lang['right_net_payable'] ="Net Payable";
$lang['right_paid'] ="Paid";
$lang['right_due'] ="Due";
$lang['right_paymentmade_amount'] ="Payment made Amount";
$lang['right_paymentmade_amount_placeholder'] ="Payment made";
$lang['right_description'] ="Description";
$lang['right_description_placeholder'] ="Description";
$lang['right_save_button'] ="Save";
$lang['right_cancle_button'] ="Cancel";
$lang['right_item_name'] ="Item Name";
$lang['right_quantity'] ="Quantity";
$lang['right_purchase_price'] ="Purchase Price/Unit";
$lang['right_sub_total'] ="Sub-total";
$lang['alert_paymentmade_success'] ="Successfully Payment made Information Updated";
$lang['alert_paymentmade_failed'] ="Update Failed! Please Try Again";
$lang['validation_msg']="Enter Valid Amount";
$lang['alert_more_paid']="Sorry! Large Amount Provided Than Due Amount";
$lang['extra_amount_inserted']= "Sorry! Large Amount Provided Than Due Amount";
$lang['voucher_print']="Print";
$lang['right_dis_type']="Dis. Type";
$lang['right_dis_amount']="Dis. Amt";
$lang['buy_due_paymentmade_tt']="Enter payment made amount to adjust due. Larger 'payment made amount' than 'total due' is not allowed.";
$lang['alert_message_ed']="Sorry! You do not Have Permission";
$lang['left_vendor']="Vendor";
$lang['search_vendor']="Search Vendor";
$lang['select_vendor']="Select Vendor";
$lang['search']="Search";
$lang['select_vendor_first']="Please Select A Vendor First";
$lang['bunch_payment_title']="Due Payment Made Form";
$lang['vendor_name']="Vendor";
$lang['right_cash_type']="Select Payment Type";
$lang['right_type_cash']="Cash";
$lang['right_type_cheque']="Cheque";
$lang['cheque_page_empty']="Please Enter Cheque Number";
$lang['bank_acc_not_select']="Select A Bank Account";
$lang['select_acc']="Select A/C";
$lang['enter_chk_num']="Enter Cheque Number";
$lang['not_enough_balance']="Not Enough Balance In The Account";
$lang['no_cheque_number']="Enter Cheque Number";
$lang['no_bank_acc']="Select A Valid Bank Account";
$lang['adjust_due_lang']="Adjust Remaining Due";