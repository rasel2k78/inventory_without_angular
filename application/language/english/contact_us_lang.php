<?php 
/**
 *English Language file for Contact Us page.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['help_support']="Help & Support";
$lang['help_details']="For instant support, please contact with the following persons.";
$lang['person_one']="Shamsur Rahman - 01966 662870";
$lang['person_two']="Rakib Khan - 01701 226549";
