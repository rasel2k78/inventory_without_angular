<?php 

/**
 *English Language file for Nagadanboi controller.
 *
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = "Add New VAT";
$lang['right_portlet_title'] = "VAT List";
$lang['left_expense_type_name'] = "Name";
$lang['percentage']="Percentage";
$lang['left_expense_type_name_placeholder'] = "Enter Name";
$lang['vat_percentage']="Enter Percentage";
$lang['left_expense_description'] = "Description";
$lang['left_expense_description_placeholder'] = "Enter Description";
$lang['left_save'] = "Save";
$lang['left_cancle'] = "Cancel";
$lang['right_expense_type'] = "Name";
$lang['right_description'] = "Percentage";
$lang['right_date'] = "Date";
$lang['right_edit_delete'] = "Edit / Delete / Activate";
$lang['alert_delete_confirmation']="Delete Confirmation:";
$lang['alert_delete_details']="Do You Want To Delete This ?";
$lang['alert_edit_confirmation']="Edit Confirmation:";
$lang['alert_edit_details']="Do You Want To Edit This ?";

$lang['alert_activate']="Activate Confirmation:";
$lang['alert_activate_details']="Do You Want To Activate This ?";



$lang['alert_delete_yes']="Yes";
$lang['alert_delete_no']="No";
$lang['alert_edit_yes']="Yes";
$lang['alert_edit_no']="No";

$lang['alert_access_failure']="Sorry !! No Permission";
$lang['alert_delete_success']="VAT Deleted Successfully";
$lang['alert_delete_failure']="Delete Failed! Try Again";
$lang['alert_edit_success']="VAT Updated Successfully";
$lang['alert_edit_failed']="Edit Failed! Try Again";
$lang['alert_insert_success']="VAT Created Successfully";
$lang['alert_insert_failed']="Failed! Try Again";

$lang['alert_active_success']="VAT Activated Successfully";
$lang['alert_active_failed']="Activation Failed! Try Again";


$lang['edit_header']="Edit VAT";
$lang['validation_name']="Enter VAT Name";

$lang['valid_percentage']="Enter Valid VAT Percentage";

$lang['model_edit']="Edit";
$lang['model_delete']="Delete";

$lang['left_expense_type_name_new']="Add New Expenditure Type";

$lang['cancel']="Cancel";

$lang['right_percentage']="Percentage";