<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// Alert portion
$lang['user_not_selected'] = 'Select a User First';
$lang['access_added'] = 'Permission Given Successfully';

$lang['access_adding_fail'] = 'Sorry! Permission Given Failed';
$lang['access_remove_success'] = 'Permission Removed Successfully';

$lang['access_remove_fail'] = 'Sorry! Permission Given Failed';
$lang['no_permission'] = 'You Have no Permission';
$lang['owner_remove_fail'] = 'As a owner you can not revoke this access for you';

$lang['all_user'] = 'All Users';
$lang['name'] = 'Name';
$lang['role'] = 'Role';
$lang['edit'] = 'Edit';

$lang['Bank'] = "Bank";
$lang['Bank Accounts'] = "Bank Accounts";
$lang['Bank D/W'] = "Bank D/W";
$lang['Pending cheque'] = "Pending cheque";
$lang['page_access'] = 'Page Access For Users';

$lang['page_name'] = 'Page Name';

$lang['Brand'] = 'Brand';
$lang['Buy'] = 'Buy';
$lang['Buy advance'] = 'Buy Advance';
$lang['Buy due'] = 'Buy Due List';
$lang['Buy list'] = 'Buy All List';
$lang['Cashbox'] = 'Cash-box';
$lang['Category'] = 'Category';
$lang['Customer'] = 'Customer';
$lang['Exchange with customer'] = 'Exchange With Customer';
$lang['Exchange with vendor'] = 'Exchange With Vendor';
$lang['Expenditure'] = 'Expenditure';
$lang['Expenditure type'] = 'Expenditure Type';
$lang['Item'] = 'Item';
$lang['Loan'] = 'Loan';
$lang['Lot size'] = 'Lot Size';
$lang['Report'] = 'Report';
$lang['Sell'] = 'Sell';
$lang['Sell advance'] = 'Sell Advance';
$lang['Sell due'] = 'Sell Due List';
$lang['Sell list'] = 'Sell All List';
$lang['Size'] = 'Size';
$lang['Sync'] = 'Sync';
$lang['Tax'] = 'Tax';
$lang['Transfer'] = 'Transfer ITems';
$lang['Unit'] = 'Unit';
$lang['User'] = 'User';
$lang['User Access'] = 'User Access';
$lang['Vendor'] = 'Vendor';
$lang['Return money'] = 'Return Money';
$lang['Sales representative'] = 'Sales representative';
$lang['Damage and lost'] = 'Damage and lost';
$lang['Vat'] = 'Vat';
$lang['Warehouse'] = 'Warehouse';


