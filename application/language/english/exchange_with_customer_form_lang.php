<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Exchange Item With Customer';
$lang['voucher_no'] = 'Voucher Number';
$lang['item'] = 'Item';
$lang['quantity'] = 'Quantity';
$lang['total_quantity'] = 'Quantity';
$lang['buying_price'] = 'Price';
$lang['sub_total'] = 'Sub Total';
$lang['Delete_items'] = 'Delete';
$lang['total'] = 'Total:';
$lang['new_item_buy_info'] = 'Sell New Item';
$lang['select_item'] = 'Select Item';
$lang['total_paid'] = 'Paid:';
$lang['exchange_with_customer_info'] = 'Exchange Item With Customer';
$lang['find'] = 'Search';
$lang['item_buy_details'] = 'Sell Voucher Details';
$lang['item_sell'] = 'Sell Register';
$lang['item_name'] = 'Item Name';
$lang['buying_price_unit'] = 'Price /Unit';
$lang['new_quantity'] = 'Exchanged Quantity';
$lang['total_bill'] = 'Total bill';
$lang['select_item'] = 'Select item';
$lang['give_item_quantity'] = 'Give item quantity';
$lang['sell_price'] = 'Selling Price';
$lang['sell_price_unit'] = 'Selling price /Unit';
$lang['single_price'] = 'Selling Price';
$lang['whole_price'] = 'Wholesale Price';
$lang['sell_price_whole'] = 'Selling Price /Unit (Wholesale)';
$lang['deadline'] = 'Expire date';
$lang['add_new_item'] = 'Add New Item';
$lang['not_permitted'] = 'No Permission';
$lang['customer_phone'] = 'Customer Phone Number';
$lang['phone_number'] = 'Phone Number';
$lang['total_new_amount'] = 'Total Price';
$lang['receivable'] = 'Receivable';
$lang['payable'] = 'Return';
$lang['Due'] = 'Due';
$lang['use_voucher'] = 'Use This Voucher';
$lang['not_exchanged'] = 'Sorry! No Item Exchanged';
$lang['exchange_success'] = 'Item Exchanged Successfully';
$lang['date'] = 'Date';
$lang['item'] = 'Item';
$lang['total_price'] = 'Total Price';
$lang['paid'] = 'Paid';
$lang['details'] = 'Details';
$lang['sorry'] = 'Sorry';
$lang['no_voucher'] = 'No Voucher available';
$lang['no_items'] = 'No Items on this voucher';
$lang['give_numeric'] = 'Please Give Numeric Input';
$lang['exchanged_quantity_exceded'] = 'Exchanged Quantity exceeded';
$lang['details_button_tt'] = 'Confirm your voucher by clicking following button';
$lang['find_voucher_tt'] = 'Give appropriate voucher number to find out exact your voucher or you can find out voucher by date or item name or phone number by following';
$lang['new_item_tt'] = 'Add new item by search for exchange';
$lang['item_list_tt'] = 'Exchanged items list';
$lang['item_new_total_price_tt'] = 'Exchanged new items total amount';
$lang['item_new_tt'] = 'New items after exchange';
$lang['item_new_paid_tt'] = 'Total paid of exchanged items';
$lang['amount_exchange_tt'] = 'Amount after exchange with new items';
$lang['amount_after_paid_tt'] = 'Amount after paid';
$lang['add_exchanged_item'] = 'Please add exchanging items';


$lang['item_invalid_price'] = 'Item Price is not correct';
$lang['give_item_quntity'] = 'Give item quantity';
$lang['give_item_price'] = 'Please give item price';
$lang['give_appropriate_amount'] = 'Give appropriate item amount of new Items';

$lang['give_voucher_no_tt'] = "Give appropriate voucher number to find out exact your voucher";


$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
// $lang['confirm_delete'] = 'ডিলিট নিশ্চিতকরণঃ';
// $lang['want_to_delete'] = 'আপনি কি লট সাইজের এই তথ্য ডিলিট করতে চান ?';
// $lang['delete_success'] = 'সফলভাবে  লট সাইজের  তথ্য ডিলিট হয়েছে';
// $lang['insert_failed'] = 'দুঃখিত !!  লট সাইজের  তথ্য সেভ হয়নি';
// $lang['insert_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য সেভ হয়েছে';
// $lang['update_succeded'] = 'সফলভাবে  লট সাইজের  তথ্য আপডেট হয়েছে';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/unit_page_lang.php */