<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Add New color';
$lang['color'] = 'Add product color';
$lang['right_portlet_title'] = 'All color List';
$lang['color_name'] = 'Color\'s Name';
$lang['color_name_placeholder'] = 'Name of the color';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['not_permitted'] = 'No permission';
$lang['want_to_delete'] = 'want to delete color?';
$lang['confirm_delete'] = 'confirm delete?';
$lang['delete_success'] = 'color deleted successfully';
$lang['insert_failed'] = 'Sorry !!  color did not saved';
$lang['insert_succeded'] = 'color inserted successfully';
$lang['update_succeded'] = 'color updated successfully';
$lang['edit_color'] = 'Edit color Information';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
/* End of file color_page_lang.php */
/* Location: ./application/language/english/color_page_lang.php */