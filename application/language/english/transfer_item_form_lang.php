<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['left_portlet_title'] = 'Items Transfer Form';
$lang['shop_in'] = 'Current Shop';
$lang['select_item'] = 'Select Item';
$lang['select_shop'] = 'Select Store';
$lang['item_quantity'] = 'Give Item Quantity';
$lang['selling_price_unit'] = 'Selling Price/unit(single)';
$lang['item_transfer_info'] = 'Item Transfer Information';
$lang['add_item'] = 'Add Item';
$lang['item_f'] = 'Item';
$lang['sell_price_single'] = 'Single Price';
$lang['selling_price_f'] = 'Selling Price';
$lang['total_price'] = 'Total Price';
$lang['transfer_item_tt'] = 'Choose item which you want to transfer';
$lang['your_shop_tt'] = 'Your Shop';
$lang['shop_to_tt'] = 'Shop to transfer';

$lang['transfer_list'] = 'Transfer List\'s';
$lang['transfer_to'] = 'Transfer To';
$lang['status'] = 'Status';
$lang['details'] = 'Details';
$lang['finish_transfer'] = 'Items transfer finished successfully';
$lang['update'] = 'Update';
$lang['sorry'] = 'Sorry';
$lang['completed'] = 'Completed ?';
$lang['item_details'] = 'Item Details';
$lang['transfer_items'] = 'Transfer Item List\'s';
$lang['price'] = 'Price';
$lang['confirm_delete'] = 'Confirm Delete: ?';
$lang['check_internet'] = 'Please check internet connection!!';
$lang['not_enough_item'] = 'Not enough item on inventory';

$lang['item_invalid_price'] = 'Item Price is not correct';
$lang['give_item_quntity'] = 'Give item quantity';
$lang['give_item_price'] = 'Please give item price';
$lang['give_appropriate_amount'] = '>Give appropriate item amount from inventory';

$lang['want_to_update'] = 'Transfer is received. Do you want to update inventory ?';
$lang['pull_form'] = 'Item\'s pulling form';
$lang['get_items'] = 'Get Item\'s';
$lang['received_successfully'] = 'All items received form the shop';
$lang['no'] = 'No';
$lang['yes'] = 'Yes';


$lang['sub_total_f'] = 'Sub-total';
$lang['total_quantity_f'] = 'Quantity';

$lang['save'] = 'Save';
$lang['cancle'] = 'Cancel';
$lang['delete'] = 'Delete';
$lang['edit'] = 'Edit';
/* End of file unit_page_lang.php */
/* Location: ./application/language/english/return_money_form_lang.php */