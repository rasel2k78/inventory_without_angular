<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['no_username'] = 'NameASD';
$lang['change_information'] = 'Change information';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['mobile1'] = 'Mobile 1';
$lang['mobile2'] = 'Mobile 2';
$lang['present_address'] = 'Present address';
$lang['permanent_address'] = 'Permanent address';
$lang['image'] = 'Image';
$lang['select_image'] = 'Select image';
$lang['change_image'] = 'Change image';
$lang['update_information'] = 'Update information';
$lang['cancel'] = 'Cancel';
$lang['change_password'] = 'Change password';
$lang['old_password'] = 'Old password';
$lang['new_password'] = 'New password';
$lang['new_password_again'] = 'New password again';

$lang['pass_update_failure'] = 'Password update failed';
$lang['pass_update_success'] = 'Password updated successfully';
$lang['new_pass_not_matched'] = 'New password and re-password did not matched';
$lang['old_pass_not_matched'] = 'Old password did not matched';
$lang['empty_pass'] = 'Password field can not be empty';
$lang['short_pass'] = 'Password should at least 4 characters';


$lang['update_failure'] = 'Update failed';
$lang['update_success'] = 'Information updated successfully';
$lang['no_username'] = 'Name can not be empty ';
$lang['no_mobile'] = 'Mobile number can not be empty';
$lang['no_email'] = 'Email can not be empty';










