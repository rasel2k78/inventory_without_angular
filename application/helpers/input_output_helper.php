<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Function Name
 *
 * Function description
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('jsonify')) {
    function jsonify($data)
    {
        echo json_encode($data);
        exit();
    }
}

/* End of file input_output_helper.php */
/* Location: ./application/helpers/input_output_helper.php */