<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(!defined('CURRENT_ASSET')){
	define('CURRENT_ASSET', base_url());
};
/**
 * load_header
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('load_header')) {
	function load_header($data = array())
	{
		$CI =& get_instance();
        // $CI->load->model('Inventory_model');
        // $data= array(
        // 	'final_cashbox_info' => $CI->Inventory_model->final_cashbox_amount_info(),
        // 	);

		if (defined('IS_NODE_APP')&&IS_NODE_APP) {
			$CI->load->view('includes/header_node');
		} else {
			$CI->load->view('includes/header');
		}
	}
}

/**
 * load_footer
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('load_footer')) {
	function load_footer($data = array())
	{
		$CI =& get_instance();
		$CI->load->view('includes/footer', $data);
	}
}

if ( ! function_exists('userActivityTracking'))
{

	function userActivityTracking(){
		$ci = &get_instance();
        //load necessary libraries
		$ci->load->library('user_agent');
		$headersData = $ci->input->request_headers();

        //Array prepared for user activity log
		$inputData = array();
		$inputData['url'] = uri_string();
		if(!empty($ci->input->get())){
			$inputData['get_data'] = json_encode($ci->input->get());
		}
		if(!empty($ci->input->post())){
			$inputData['post_data'] =json_encode( $ci->input->post());
		}
		$inputData['header_data'] = json_encode($headersData);
		$inputData['user_id'] = $ci->session->userdata('user_id');
		$inputData['ip_address'] = $ci->input->ip_address();
		$inputData['user_agent'] = $ci->agent->agent_string();
           //Add it to the database
      // $result = $ci->db->insert('master_log', $inputData);
	}
}


/* End of file theme_header.php */
/* Location: ./application/helpers/theme_header.php */