<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('start_benchmarking')) {
    function start_benchmarking()
    {
        $CI =& get_instance();
        $execution_time = $CI->benchmark->mark('start');
    }
}

if (! function_exists('end_benchmarking')) {
  //   function end_benchmarking($param = '')
  //   {
  //       $CI =& get_instance();
  //       $CI->benchmark->mark('end');
  //       $execution_time = $CI->benchmark->elapsed_time('start', 'end');
  //       $url = $CI->uri->segment_array();

  //       $params = json_encode($url[3].$url[4]);


  //       $CI->load->database('inventory'); 

  //       $CI->db->select('UUID()', false);    
  //       $uuid =$CI->db->get()->row_array();
  //       $id =  $uuid['UUID()'];

  //       $sql = "INSERT INTO benchmark_table (benchmark_table_id, controller, method, params, exec_time)
		// VALUES ('$id','$url[1]', '$url[2]', '$params','$execution_time')"; 
  //       $query = $CI->db->query($sql);
  //       if($query) {
  //           echo "Success";
  //       }else{
  //           echo "Failed";
  //       }
  //   }
}

/* End of file benchmark_helper.php */
/* Location: ./application/helpers/benchmark_helper.php */