<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
     * getLang helper method is for getting which language is selected
     * 
     * @return null
     * @author Musabbir (musabbir.mamun@gmail.com)
     **/

if (! function_exists('getLang')) {
    function getLang()
    {
        $CI =& get_instance();
        $CI->load->model('Loginmodel');
        return $CI->Loginmodel->getLang();
    }
}
