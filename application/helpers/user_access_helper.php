<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * check_if_logged_in
 *
 * check if logged in
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('check_if_logged_in')) {
    function check_if_logged_in()
    {
        $CI =& get_instance();
        if ($CI->session->userdata('logged_in') === null) {
            $CI->output->set_status_header('401');
        }
    }
}

/**
 * checking user permissions level helper...made by Rasel
 * load_header
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('is_owner'))  /*Check user is owner*/
{
    function is_owner($data = array())
    {
        $CI =& get_instance();
        $user = $CI->session->userdata('email');
        $logged_in = $CI->session->userdata('logged_in');
        if(isset($user) && isset($logged_in)) {
            if($CI->session->userdata('user_type') != 'owner') {
                return false;
            }
            else
            {
                return true;
            }
        }    
    }
}



/**
 * load_footer
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('is_staff'))        /*Check user is staff*/
{
    function is_staff($data = array())
    {
        $CI =& get_instance();
        $user = $CI->session->userdata('email');
        $logged_in = $CI->session->userdata('logged_in');
        if(isset($user) && isset($logged_in)) {
            if($CI->session->userdata('user_type') != 'staff') {
                return false;
            }
            else
            {
                return true;
            }
        }    
    }
}

if (! function_exists('is_manager'))                 /*Check user is manager*/
{
    function is_manager($data = array())
    {
        $CI =& get_instance();
        $user = $CI->session->userdata('email');
        $logged_in = $CI->session->userdata('logged_in');
        if(isset($user) && isset($logged_in)) {
            if($CI->session->userdata('user_type') != 'manager') {
                return false;
            }
            else
            {
                return true;
            }
        }    
    }
}

/**
 * Function Name
 *
 * Function description
 *
 * @access public
 * @param  type    name
 * @return type    
 */

if (! function_exists('check_access')) {
    function check_access($user_type = array(), $redirect = true)
    {
        $CI =& get_instance();
        if ($CI->session->userdata('logged_in') === true) {
            if (in_array($CI->session->userdata('user_type'), $user_type)) {
                return true;
            }
            else
            {
                if (!!$redirect) { 
                    redirect('/error_controller', 'refresh');
                }
                else
                {
                    if($CI->session->userdata('user_type')=="manager") {
                        return "sorry you are not permitted here.This shdsdfsgould be from owner";exit;
                    }
                }
            }
        }
    }
}

if (! function_exists('url_active')) {
    function url_active($url)
    {
        $array_of_urls = [];
        foreach ($url as $key => $value) {
            array_push($array_of_urls, '/inventory/index.php/'.$value);
        }

        // $actual_link = "http://".$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];

        $actual_link = $_SERVER['REQUEST_URI'];
        if (in_array($actual_link, $array_of_urls)) {
            return 'start active';    
        } else {
            return '';    
        }
        
    }
}
