<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Second_db extends CI_Model
{

    private $another;
    function __construct()
    {
        parent::__construct();
        $this->another = $this->load->database('second', true);
    }

    public function getSomething()
    {
        $this->another->select('*');
        $q = $this->another->get('test');
        if($q->num_rows()>0) {
            foreach($q->result() as $row)
            {
                $data[] = $row;
            }
        }
        else
        {
            return false;
        }
    }

}

/* End of file Second_db.php */
/* Location: ./application/models/Second_db.php */