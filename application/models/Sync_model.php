<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Sync_model extends CI_Model
{

    public $variable;
    public  $db2;

    public function __construct()
    {
        parent::__construct();
        //$this->load->database("inventory");
        //$this->load->database("maxrailw_inventory");
        
    }

    public function syncCount()
    {
        $data = array();
        $this->db->select('COUNT(*) AS unsynced');
        $this->db->from('sync');
        $this->db->where('status', 'unsynced');
        $data['unsynced'] =  $this->db->get()->row()->unsynced;

        $this->db->select('COUNT(*) AS synced');
        $this->db->from('sync');
        $this->db->where('status', 'synced');
        $data['synced'] =  $this->db->get()->row()->synced;

        return $data;
    }

    public function update_synch_table($id,$dataa)
    {
        $this->db->where('id', $id);
        return $this->db->update('sync', $dataa); 
    }
    
    public function getUnsynced()
    {
        $this->db->select('id,table_name,row_id,type,pkey_column_name');
        $this->db->from('sync');
        $this->db->where('status', 'unsynced');
        $this->db->limit(50);
        return $this->db->get()->result_array();
    }

    public function get_folder_name()
    {
        $query = $this->db->get('account_folder');
        return $query->row_array();
    }

    public function get_owner()
    {
        // $query = $this->db->get_where('users', array("user_role" => "owner",""));
        // return $query->row_array();
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_role', 'owner');
        $this->db->where('publication_status', 'activated');
        $this->db->limit(1);
        return $this->db->get()->row_array();
        
    }
    public function getTableRow($table_name,$columnId,$columnIdValue)
    {
        $query = $this->db->get_where($table_name, array("$columnId" => "$columnIdValue"));
        return $query->row_array();
    }

    public function get_user_by_id($id)
    {
        $query = $this->db->get_where('users', array("user_id" => "$id"));
        return $query->row_array();
    }


    // public function updateLiveRow($data,$table_name,$columnId,$columnIdValue)
    // {
    // 	$ip = gethostbyname('www.google.com');
    // 	if($ip != 'www.google.com')
    // 	{
    //                  //connected!echo
    // 		$db2 = $this->load->database('new', TRUE);
    // 		return $db2->where("$columnId", $columnIdValue)->update("$table_name",$data);
    // 	} 
    // 	else 
    // 	{
    //                  //not connected
    // 		return false;
    // 	}
    // }
    // public function insertLiveRow($data,$table_name)
    // {
    // 	$ip = gethostbyname('www.google.com');
    // 	if($ip != 'www.google.com')
    // 	{
    // 		$db2 = $this->load->database('new', TRUE);
    // 		return $db2->insert("$table_name",$data);
    // 	} 
    // 	else 
    // 	{
    // 		return false;
    // 	}

    // }
}

/* End of file Sync_Model.php */
/* Location: ./application/models/Sync_Model.php */