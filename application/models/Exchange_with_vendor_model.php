<?php 
/**
 * Exchange_with_vendor_model CRUD model
 * 
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Exchange_with_vendor_model class
 *
 * @package default
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/

class Exchange_with_vendor_model extends CI_Model
{



  public $variable;

    /**
 * This is a construtor functions.
 *
 * @return void
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();

   }

    /**
 * Collect individual voucher's information from database by id
 *
 * @return array[]
 * @param  string $buy_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_buy_details_by_voucher($voucher_no)
    {
     $this->db->select('*');
     $this->db->from('buy_details');
     $this->db->where('voucher_no', $voucher_no);
     $this->db->limit(1);
     $q = $this->db->get();
     return $q->row_array();
   }
 /**
 * Get current inventory of an item
 *
 * @return array[]
 * @param  string $items_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
 public function items_on_inventory($items_id)
 {
  return $this->db->get_where('inventory', array('item_spec_set_id'=>$items_id))->row_array();
}
  /**
 * Update buy voucher details after exchange
 *
 * @return array[]
 * @param  string $buy_details_id
 * @param  array[] $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function update_buy_details($data,$buy_details_id)
  {
   return $this->db->update('buy_details', $data, array('buy_details_id' =>$buy_details_id));
 }
 /**
 * Save exchange summary as history
 *
 * @return boolean
 * @param  array[] $exchange_hstory_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
 public function save_exchange_history($exchange_hstory_data)
 {
  $this->db->insert('exchange_return_history', $exchange_hstory_data);
}

/**
 * Get buy voucher vendor details
 *
 * @return array[]
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_vendor($voucher_no)
{
  return $this->db->get_where('buy_details', array('voucher_no' => $voucher_no))->row_array();
}
/**
 * Get buy voucher vendor details
 *
 * @return boolean
 * @param  array[] $update_buy_cart_item_data
 * @param  string $buy_details_id
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_cart_items($update_buy_cart_item_data,$buy_details_id,$item_spec_set_id)
{
 return $this->db->update('buy_cart_items', $update_buy_cart_item_data, array('buy_details_id' =>$buy_details_id,'item_spec_set_id'=>$item_spec_set_id));
}
/**
 * Update cashbox amount
 *
 * @return boolean
 * @param  array[] $cashbox_amount_update_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_final_cashbox_amount($cashbox_amount_update_data)
{
 return $this->db->update('final_cashbox_amount', $cashbox_amount_update_data);
}
/**
 * Get buy cart item by item id
 *
 * @return array[]
 * @param  string $buy_details_id
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function items_on_cart($buy_details_id,$item_spec_set_id)
{
 return $this->db->get_where('buy_cart_items', array('buy_details_id' => $buy_details_id,'item_spec_set_id'=>$item_spec_set_id))->row_array();
}
/**
 * Get current inventory of an item
 *
 * @return array[]
 * @param  string $items_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_item_quantity_inventory($items_id)
{
 return $this->db->get_where('inventory', array('item_spec_set_id' => $items_id))->row_array();
}
/**
 * Get current inventory of an item from buy cart item table
 *
 * @return array[]
 * @param  string $buy_details_id
 * @param  string $items_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_item_quantity_cart($buy_details_id,$items_id)
{
 return $this->db->get_where('buy_cart_items', array('buy_details_id' => $buy_details_id,'item_spec_set_id'=>$items_id))->row_array();
}
/**
 * Get current items of buy voucher 
 *
 * @return array[]
 * @param  string $buy_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function find_items_on_cart_items($buy_details_id)
{
  return $this->db->get_where('buy_cart_items', array('buy_details_id' => $buy_details_id,'publication_status'=>'activated'))->result_array();
}
/**
 * Update inventory of an item
 *
 * @return boolean
 * @param  string $items_id
 * @param  array[] $update_inventory_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_inventory($update_inventory_data,$items_id)
{
 return $this->db->update('inventory', $update_inventory_data, array('item_spec_set_id'=>$items_id));
}
/**
 * Update inventory of an item on buy cart item table
 *
 * @return boolean
 * @param  string $items_id
 * @param  string $buy_details_id
 * @param  array[] $update_buy_cart_item_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function buy_cart_items_update($update_buy_cart_item_data,$buy_details_id,$items_id)
{
 return $this->db->update('buy_cart_items', $update_buy_cart_item_data, array('buy_details_id' =>$buy_details_id,'item_spec_set_id'=>$items_id));
}
/**
 * Update buy cart item after exchange
 *
 * @return boolean
 * @param  string $cart_id
 * @param  array[] $update_buy_cart_item_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_buy_cart_item_by_id($update_buy_cart_item_data,$cart_id)
{
 return $this->db->update('buy_cart_items', $update_buy_cart_item_data, array('buy_cart_items_id' =>$cart_id));
}



    /**
 * Add items to inventory
 *
 * @return void
 * @author rasel 
 **/


    public function insert_items_to_inventory($data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['inventory_id'] = $uuid['UUID()'];
     $this->db->insert('inventory', $data);
   }

    /**
 * Save data of nagadanboi form  in database 
 *
 * @return void
 * @link   Nogodanboi/save_cash_deposit_info
 * @param  array[] $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function save_cash_box_info($data)
    {

     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();

     $data['cashbox_id']=$uuid['UUID()'];
     $this->db->insert('cashbox', $data);

   }
 /**
 * Insert new items to buy cart table
 *
 * @return boolean
 * @param  array[] $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
 public function add_items_cart($data)
 {
   $this->db->select('UUID()');    
   $uuid =$this->db->get()->row_array();
   $data['buy_cart_items_id'] = $uuid['UUID()'];
   $this->db->insert('buy_cart_items', $data);
 }

/**
 * Get inventory of an item
 *
 * @return array[]
 * @param  array[] $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function check_sell($data)
{
 $this->db->select('quantity', false);
 $this->db->from('inventory');
 $this->db->where('item_spec_set_id', $data['item_spec_set_id']);
 return $this->db->get()->row_array();
}
/**
 * Get inventory of an item
 *
 * @return array[]
 * @param  string $buy_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_cart_items($buy_details_id)
{
 return $this->db->get_where('buy_cart_items', array('buy_details_id' => $buy_details_id,'publication_status'=>'activated'))->result_array();
}
/**
 * Update buy details after exchange
 *
 * @return array[]
 * @param  string $buy_details_id
 * @param  array[] $update_buy_details
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function buy_details_update($update_buy_details,$buy_details_id)
{
 return  $this->db->update('buy_details', $update_buy_details, array('buy_details_id' =>$buy_details_id));
}
/**
 * Get all items of a buy voucher
 *
 * @return array[]
 * @param  string $buy_details_id
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_cart_items_voucher($buy_details_id,$item_spec_set_id)
{
 $this->db->select('*');
 $this->db->from('buy_cart_items');
 $this->db->where('buy_details_id', $buy_details_id);
 $this->db->where('item_spec_set_id', $item_spec_set_id);
 $this->db->limit(1);
 $q = $this->db->get();
 return $q->row_array();
}
/**
 * Get buy voucher by voucher id
 *
 * @return array[]
 * @param  string $voucher_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_details_return($voucher_id)
{
  return $this->db->get_where('buy_details', array('voucher_no' => $voucher_id,'publication_status'=>'activated'))->row_array();
}
  /**
 * Get buy voucher by voucher no
 *
 * @return array[]
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function get_voucher_info($voucher_no)
  {

   $this->db->select('DISTINCT imei_barcode.imei_barcode,buy_details.buy_details_id,buy_details.grand_total,buy_details.voucher_no,buy_details.paid,buy_details.due,buy_details.discount,buy_details.net_payable,buy_details.discount,buy_cart_items.quantity,buy_cart_items.sub_total,buy_cart_items.discount_amount,buy_cart_items.discount_type,buy_cart_items.buying_price,buy_cart_items.item_spec_set_id,item_spec_set.spec_set,item_spec_set.unique_barcode,imei_barcode.barcode_status,imei_barcode.buy_details_id',false);
   $this->db->from('buy_details');
   $this->db->join('buy_cart_items', 'buy_cart_items.buy_details_id = buy_details.buy_details_id', 'left');
   $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');

   $this->db->join('imei_barcode', 'imei_barcode.buy_details_id = buy_details.buy_details_id AND imei_barcode.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
   
   $this->db->where('buy_details.publication_status', 'activated');
   $this->db->where('buy_details.voucher_no', $voucher_no);
   $this->db->where('buy_cart_items.quantity !=', 0);
   $this->db->where('item_spec_set.publication_status', 'activated');
   return $this->db->get()->result_array();
 }

    /**
 * Collect individual items's information from database by id
 *
 * @return array[]
 * @param  string $item
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function select_item_price_info_row($item)
    {
     $this->db->select('*', false);
     $this->db->from('buy_cart_items');
     $this->db->where('item_spec_set_id', $item);
     $this->db->order_by('date_created', 'desc');
     $this->db->limit(1);
     $q = $this->db->get();
     return $q->row_array();
   }
/**
 * Update exchanged imei data
 *
 * @return boolean
 * @param  string $imei_barcode
 * @param  array[] $imei_data
 * @param  string $item_spec_set_id
 * @param  string $buy_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_imei_barcode($imei_data,$imei_barcode,$item_spec_set_id,$buy_details_id)
{
  return $this->db->update('imei_barcode', $imei_data, array('item_spec_set_id'=>$item_spec_set_id,'imei_barcode'=>$imei_barcode,'buy_details_id'=>$buy_details_id));
}
  /**
 * Get buy voucher information
 *
 * @return boolean
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function get_voucher_info_transfer($voucher_no)
  {
   $this->db->select('buy_details.buy_details_id,buy_details.date_created,buy_details.voucher_no,buy_details.grand_total,buy_details.voucher_no,buy_details.paid,buy_details.due,buy_details.discount,buy_details.net_payable,buy_cart_items.quantity,buy_cart_items.discount_amount,buy_cart_items.buying_price,buy_cart_items.sub_total,buy_cart_items.discount_type,buy_cart_items.item_spec_set_id,item_spec_set.item_spec_set_id,item_spec_set.spec_set');
   $this->db->from('buy_details');
   $this->db->join('buy_cart_items', 'buy_cart_items.buy_details_id = buy_details.buy_details_id', 'left');
   $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
   $this->db->where('buy_details.publication_status', 'activated');
   $this->db->where('buy_details.voucher_no', $voucher_no);
   return $this->db->get()->result_array();
 }
 /**
 * Get item information by search value
 *
 * @return array[]
 * @param  string $text
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
 public function search_item($text)
 {
   $this->db->select('items_id,items_name,item_image');
   $this->db->from('items');
   $this->db->like('items_name', $text, 'both');
   $this->db->or_like('unique_id', $text, 'both');
   $this->db->where('publication_status', 'activated');
   $this->db->limit(10);
   return $this->db->get()->result_array();
 }
}

/* End of file  */
/* Location: ./application/models/ */