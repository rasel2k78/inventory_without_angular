<?php
/**
 * Transfer_model CRUD model
 * 
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Transfer_model class
 *
 * @package default
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/

class Transfer_model extends CI_Model
{
  public $variable;

    /**
 * This is a construtor functions.
 *
 * @return void
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();
   }

    /**
 * save transfered items summery to database
 *
 * @return array[]
 * @param  string $dataa
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function insert_transfer_data($dataa)
    {
     $data = array();
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['transfer_details_id'] = $uuid['UUID()'];
     $data['transfer_from'] = $dataa['transfer_from'];
     $data['transfer_to'] = $dataa['transfer_to'];
     $data['total_item_quantity'] = $dataa['total_item_quantity'];
     $data['total_amount'] = $dataa['total_amount'];
     $data['user_id'] = $dataa['user_id'];
     if($this->db->insert('transfer_details', $data)) {
      return $uuid['UUID()'];
    } 
  }

 /**
 * Insert buy cart items for temp buy with transfer warehouse.
 *
 * @return boolean
 * @param  array[] $data
 * @param  string $buy_details_uuid
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function save_buy_item_info($data,$buy_details_uuid)
  {
    foreach ($data as $key => $value) {
      $this->db->select('UUID()', false);    
      $uuid =$this->db->get()->row_array();
      unset($data[$key]['item_name']);
      unset($data[$key]['actual_quantity']);
      unset($data[$key]['unique_barcode']);
      unset($data[$key]['cart_temp_id']);
      $data[$key]['buy_details_id'] = $buy_details_uuid;
      $data[$key]['user_id'] = $this->session->userdata('user_id');    
      $data[$key]['buy_cart_items_id']=$uuid['UUID()'];

    }
    return $this->db->insert_batch('buy_cart_items', $data);
  }


/**
 * Check if customer exist.
 *
 * @return string
 * @param  string $customer_name
 * @param  string $customer_phone
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function check_customer_availablity($customer_name,$customer_phone)
  {
    $this->db->select('customers_id', FALSE);
    $this->db->from('customers');
    $this->db->where('publication_status', 'activated');
    $this->db->where('customers_name', $customer_name);
    $this->db->where('customers_phone_1', $customer_phone);
    $return_data =  $this->db->get()->row_array();
    return $return_data['customers_id'];
  }

  /**
 * Check if vendor exist.
 *
 * @return string
 * @param  string $vendor_name
 * @param  string $vendor_phone
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function check_vendor_availablity($vendor_name,$vendor_phone)
  {
    $this->db->select('vendors_id', FALSE);
    $this->db->from('vendors');
    $this->db->where('publication_status', 'activated');
    $this->db->where('vendors_name', $vendor_name);
    $this->db->where('vendors_phone_1', $vendor_phone);
    $return_data =  $this->db->get()->row_array();
    return $return_data['vendors_id'];
  }

/**
 * Save transfer summary to transfer details table
 *
 * @return string
 * @param  array[] $dataa
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function insert_warehouse_data($dataa)
  {
   $data = array();
   $this->db->select('UUID()', false);    
   $uuid =$this->db->get()->row_array();
   $data['transfer_details_id'] = $uuid['UUID()'];
   $data['transfer_from'] = $dataa['transfer_from'];
   $data['transfer_to'] = $dataa['transfer_to'];
   $data['total_item_quantity'] = $dataa['total_item_quantity'];
   $data['total_amount'] = $dataa['total_amount'];
   $data['user_id'] = $dataa['user_id'];
   $data['transfer_type'] = $dataa['transfer_type'];
   if($this->db->insert('transfer_details', $data)) {
    return $uuid['UUID()'];
  } 
}

/**
 * Insert sell details data for transfer summary or sell summary
 *
 * @return string
 * @param  array[] $dataa
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function insert_due_sell_data($dataa)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $dataa['sell_details_id'] = $uuid['UUID()'];
 if($this->db->insert('sell_details', $dataa)) {
  return $uuid['UUID()'];
} 
}

/**
 * Insert sell cart items data for transfer summary or sell summary
 *
 * @return string
 * @param  array[] $dataa
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function save_due_sell_cart_items($dataa)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $dataa['sell_cart_items_id'] = $uuid['UUID()'];
 //echo "<pre>";print_r($dataa);exit;
 if($this->db->insert('sell_cart_items', $dataa)) {
  return $uuid['UUID()'];
} 
}

/**
 * Fetch spec name by name
 *
 * @return array[]
 * @param  string $spec_name
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_spec_name_by_name($spec_name)
{
  return $this->db->get_where('spec_name_table', array('spec_name'=>$spec_name))->row_array();
}

/**
 * Getting total warehouse receive amount
 * @return array[]
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_total_receives()
{
  return $this->db->get("transfer_receives")->row_array();
}

/**
 * Insert total receive amount
 * @return boolean 
 * @param  double $total_transfer_receive_amount
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function add_total_receive_amount($total_transfer_receive_amount)
{
 return $this->db->insert("transfer_receives",$total_transfer_receive_amount);
}
/**
 * update warehouse receive amount
 * @return boolean
 * @param double $total_transfer_receive_amount
 * @param string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_receive_amount($total_transfer_receive_amount,$id)
{
 $this->db->where("id", $id);
 return $this->db->update("transfer_receives", $total_transfer_receive_amount);
}

/**
 * Insert receive item data to individual table 
 * @return boolean
 * @param string $table
 * @param array[] $value
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function insert_on_pull_shop($table,$value)
{
 return $this->db->insert("$table",$value);
}

/**
 * Update receive item data to individual table 
 * @return boolean
 * @param string $table
 * @param string $id
 * @param string $column
 * @param array[] $value
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_on_pull_shops($table,$id,$column,$value)
{
 $this->db->where("$column", $id);
 return $this->db->update("$table", $value);
}

/**
 * Insert customer data
 * @return array[]
 * @param array[] $udata
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function createAccount($udata)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();

 $udata['customers_id']=$uuid['UUID()'];
 $this->db->insert('customers', $udata);
 return $uuid['UUID()'];
}

/**
 * Insert vendor data
 * @return string
 * @param array[] $udata
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function create_vendor_account($udata)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();

 $udata['vendors_id']= $uuid['UUID()'];
 $this->db->insert('vendors', $udata);
 return $uuid['UUID()'];
}
/**
 * Update customer information
 * @return array[]
 * @param array[] $data
 * @param string $customers_id
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_customer($data,$customers_id)
{
  $this->db->where("customers_id", $customers_id);
  return $this->db->update("customers", $data);
}
/**
 * Update vendor information
 * @return array[]
 * @param array[] $data
 * @param string $vendor_id
 * @link   Transfer_warehouse/save_warehouse_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_vendor($data,$vendor_id)
{
  $this->db->where("vendors_id", $vendor_id);
  return $this->db->update("vendors", $data);
}

/**
 * Update spec_name table
 * @return array[]
 * @param array[] $data
 * @param string $spec_name_id
 * @param string $table
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_spec_names($data,$spec_name_id,$table)
{
 return $this->db->update("$table", $data, array('spec_name_id'=>$spec_name_id));
}

/**
 * Check item_spec_set table item by id
 * @return array[]
 * @param array[] $data
 * @link   Transfer_warehouse/save_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function check_items($data)
{
  $this->db->select('buy_cart_items.retail_price,buy_cart_items.buying_price,inventory.quantity');
  $this->db->from('inventory');
  $this->db->join('buy_cart_items', 'buy_cart_items.item_spec_set_id = inventory.item_spec_set_id', 'left');
  $this->db->where('inventory.item_spec_set_id', $data['item_spec_set_id']);
  $this->db->order_by('buy_cart_items.date_updated', 'desc');
  return $this->db->get()->row_array();
}

/**
 * Fetch shop information
 * @return array[]
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_my_shop()
{
 return $this->db->get('shop')->row_array();
}

/**
 * Fetch user information
 * @return array[]
 * @link   Transfer_warehouse/index
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_my_account()
{
 return $this->db->get_where('users', array('user_role'=>"owner"));
}

/**
 * Get spec name from spec_nambe table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_spec_name_id($id)
{
  return $this->db->get_where('spec_name_table', array('spec_name_id'=>$id))->row_array();
}


/**
 * Get buy details from buy_details table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_details($id)
{
  return $this->db->get_where('buy_details', array('buy_details_id'=>$id))->row_array();
}

/**
 * Get buy cart items from buy_cart_items table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_buy_cart_items()
{
  return $this->db->get_where('buy_cart_items', array('buy_cart_items_id'=>$id))->row_array();
}

/**
 * Get buy cart items from buy_cart_items table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_spec_value_id($id)
{
  return $this->db->get_where('spec_value', array('spec_value_id'=>$id))->row_array();;
}

/**
 * Get buy cart items from buy_cart_items table by id
 * @return array[]
 * @param string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_imei_value_id($id)
{
  return $this->db->get_where('imei_barcode', array('imei_barcode_id'=>$id))->row_array();;
}

/**
 * Get Category from categories table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_category_id($id)
{
  return $this->db->get_where('categories', array('categories_id'=>$id))->row_array();;
}

/**
 * Get item from items table by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_items_id($id)
{
  return $this->db->get_where('items', array('items_id'=>$id))->row_array();;
}
/**
 * Get item spec set from items item_spec_set by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_item_spec_set_id($id)
{
  return $this->db->get_where('item_spec_set', array('item_spec_set_id'=>$id))->row_array();
}

/**
 * Get item spec set connector from spec_set_connector_with_values by id
 * @return array[]
 * @param string $id
 * @link   Transfer_warehouse/insert_receive_items
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_item_spec_connector_id($id)
{
  return $this->db->get_where('spec_set_connector_with_values', array('spec_set_connector_id'=>$id))->row_array();;
}

/**
 * Get Transfer summary by id
 * @return array[]
 * @param string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_transfer_summery_by_id($id)
{
 return $this->db->get_where('transfer_details', array('transfer_details_id'=>$id));
}

/**
 * Get Transfer items by id
 * @return array[]
 * @param string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_transfer_items_id($id)
{
 return $this->db->get_where('transfer', array('transfer_details_id'=>$id));
}

/**
 * Get Transfer items by id
 * @return array[]
 * @param array $data
 * @param string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_inventory_transfered_items($data,$item_spec_set_id)
{
 return $this->db->update('inventory', $data, array('item_spec_set_id'=>$item_spec_set_id));
}

/**
 * Get update imei items for delete transfer 
 * @return boolean
 * @param array $data_imei
 * @param string $item_spec_set_id
 * @param string $value_imei
 * @link   Transfer_warehouse/transfered_delete
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_imei_items($data_imei,$item_spec_set_id,$value_imei)
{
  return $this->db->update('imei_barcode', $data_imei, array('item_spec_set_id'=>$item_spec_set_id,'imei_barcode'=>$value_imei));
}

/**
 * Get update imei items for delete transfer 
 * @return boolean
 * @param array $data_imei
 * @param string $item_spec_set_id
 * @param string $value_imei
 * @link   Transfer_warehouse/transfered_delete
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_imei_data($imei_update_data,$item_spec_set_id,$imei_barcode)
{
  return $this->db->update('imei_barcode', $imei_update_data, array('item_spec_set_id'=>$item_spec_set_id,'imei_barcode'=>$imei_barcode));
}
/**
 * Update transfer status 
 * @return boolean
 * @param array $data
 * @param string $id
 * @link   Transfer_warehouse/update_own_cashbox
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_transfer_list_completed($id,$data)
{
 return $this->db->update('transfer_details', $data, array('transfer_details_id'=>$id));
}
/**
 * Get final cashbox amount
 * @return boolean
 * @link   Transfer_warehouse/update_received_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function final_cashbox_amount_info()
{

 $this->db->select('total_cashbox_amount');
 $this->db->from('final_cashbox_amount');
 $qry_get = $this->db->get();
 return $qry_get->row_array();
}
/**
 * Save cash box data
 * @return boolean
 * @param array $data
 * @link   Transfer_warehouse/update_received_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function save_cash_box_info($data)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $data['cashbox_id']=$uuid['UUID()'];
 $this->db->insert('cashbox', $data);
 return $uuid['UUID()'];

}
/**
 * Update cash box data
 * @return boolean
 * @param array $cashbox_amount_update_data
 * @link   Transfer_warehouse/update_received_transfer_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_final_cashbox_amount($cashbox_amount_update_data)
{
 return $this->db->update('final_cashbox_amount', $cashbox_amount_update_data);
}
/**
 * Get all transfer receive list
 * @return array[]
 * @param array $cashbox_amount_update_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_transfer_received_list()
{
 return $this->db->get_where('transfer_details', array('status'=>1,'publication_status'=>'activated'));
}
/**
 * Update transfer status
 * @return boolean
 * @param array $data
 * @param string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_transfer_status($data,$id)
{
 return $this->db->update('transfer_details', $data, array('transfer_details_id'=>$id));
}
/**
 * Search shop names
 * @return array[]
 * @param string $text
 * @param string $currentShop
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_other_stores($text,$currentShop)
{
 $this->db->select('stores_id,name,location');
 $this->db->from('stores');
 $this->db->like('name', $text, 'both');
 $this->db->where('publication_status', 'activated');
 $this->db->where('stores_id !=', $currentShop);
 $this->db->limit(10);
 return $this->db->get();
}
/**
 * Get account folder information shop names
 * @return array[]
 * @param string $text
 * @param string $currentShop
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_folder_name()
{
 $query = $this->db->get('account_folder');
 return $query->row_array();
}


    /**
 * Save transfered items to database
 *
 * @return array[]
 * @param  string $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function save_transfer_items($dataa)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['transfers_id'] = $uuid['UUID()'];
     $data['transfer_details_id'] = $dataa['transfer_details_id'];
     $data['item_spec_set_id'] = $dataa['item_spec_set_id'];
     $data['quantity'] = $dataa['quantity'];
     $data['buying_price'] = $dataa['buying_price'];
     $data['selling_price'] = $dataa['selling_price'];
     $data['user_id'] = $dataa['user_id'];

     return $this->db->insert('transfer', $data);
   }

/**
 * Insert warehouse transfer items
 * @return boolean
 * @param array[] $dataa
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function save_warehouse_details_items($dataa)
   {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['transfers_id'] = $uuid['UUID()'];
     $data['transfer_details_id'] = $dataa['transfer_details_id'];
     $data['item_spec_set_id'] = $dataa['item_spec_set_id'];
     $data['quantity'] = $dataa['quantity'];
     $data['buying_price'] = $dataa['buying_price'];
     $data['imei_barcode'] = $dataa['imei_barcode'];
     $data['selling_price'] = $dataa['selling_price'];
     $data['whole_sale_price'] = $dataa['whole_sale_price'];
     $data['user_id'] = $dataa['user_id'];
     $data['transfer_type'] = $dataa['transfer_type'];
     return $this->db->insert('transfer', $data);
   }

    /**
 * Collect transfer summery information if any transfer receives to current store from database by transfer details id
 *
 * @return array[]
 * @param  string $Id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_recievable_transfer_items($Id)
    {
     return $this->db->get_where('transfer_details', array('transfer_to'=>$Id,'transfer_from !=' => $Id,'publication_status'=>'activated','status'=>0));
   }
    /**
 * collect transfer summery by transfer details id
 *
 * @return array[]
 * @param  string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_transfer_details($id)
    {
     return $this->db->get_where('transfer_details', array('transfer_details_id'=>$id,'publication_status'=>'activated','status'=>0))->row_array();
   }

 /**
 * Get sell details which is temp transfer
 *
 * @return array[]
 * @param  string $transfer_detailsId
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function get_sell_transfer_details($transfer_detailsId)
   {
     return $this->db->get_where('sell_details', array('transfer_details_id'=>$transfer_detailsId,'publication_status'=>'activated'))->row_array();
   }
    /**
 * collect transfer summery with all items by transfer details id
 *
 * @return array[]
 * @param  string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_transfer_items_by_details_id($id)
    {
     $this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,transfer.transfers_id,transfer.quantity,transfer.buying_price,transfer.selling_price');
     $this->db->from('item_spec_set');
     $this->db->join('transfer', 'item_spec_set.item_spec_set_id = transfer.item_spec_set_id', 'left');
     $this->db->where('item_spec_set.publication_status', 'activated');
     $this->db->where('transfer.transfer_details_id', $id);
     return $this->db->get()->result_array();
   }

/**
 * Get transfer items by transfer details id
 *
 * @return array[]
 * @param  string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function get_warehouse_items_by_details_id($id)
   {
     $this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,transfer.transfers_id,transfer.quantity,transfer.buying_price,transfer.selling_price,transfer.imei_barcode');
     $this->db->from('item_spec_set');
     $this->db->join('transfer', 'item_spec_set.item_spec_set_id = transfer.item_spec_set_id', 'left');
     $this->db->where('item_spec_set.publication_status', 'activated');
     $this->db->where('transfer.transfer_details_id', $id);
     $this->db->where('transfer.transfer_type', "warehouse");
     return $this->db->get()->result_array();
   }
/**
 * Get transfer items by transfer details id
 *
 * @return array[]
 * @param  string $transfer_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function get_transfer_items_by_id($transfer_details_id)
   {
    $this->db->select('transfers_id,item_spec_set_id,imei_barcode,buying_price,quantity,publication_status,transfer_details_id');
    $this->db->from('transfer');
    $this->db->where('publication_status', 'activated');
    $this->db->where('transfer_details_id', $transfer_details_id);
    $this->db->where('transfer_type', "warehouse");
    return $this->db->get()->result_array();
  }

/**
 * Deactivate transfer items id
 *
 * @return array[]
 * @param  array[] $data
 * @param  string $transfer_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function delete_transfer_items($transfers_id,$data)
  {
    $this->db->where('transfers_id', $transfers_id);
    return $this->db->update('transfer', $data);
  }
/**
 * Deactivate transfer transfer_details_id 
 *
 * @return array[]
 * @param  string $transfer_details_id
 * @param  array[] $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function delete_transfer_details($transfer_details_id,$data)
  {
    $this->db->where('transfer_details_id', $transfer_details_id);
    return $this->db->update('transfer_details', $data);
  }

    /**
 * Collect shop information from database by id
 *
 * @return array[]
 * @param  string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_shop_by_id($id)
    {
     return $this->db->get_where('stores', array('stores_id'=>$id));
   }
    /**
 * get item information by id
 *
 * @return array[]
 * @param  string $id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function get_item_name($id)
    {
     return $this->db->get_where('item_spec_set', array('item_spec_set_id'=>$id));
   }
   /**
 * get item inventory by item id
 *
 * @return array[]
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function get_items_inventory($item_spec_set_id)
   {
    return $this->db->get_where('inventory', array('item_spec_set_id' =>$item_spec_set_id))->row_array();
  }

}

/* End of file Transfer_model.php */
/* Location: ./application/models/Transfer_model.php */
?>