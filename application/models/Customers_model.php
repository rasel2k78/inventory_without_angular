<?php 

/**
 * Customer CRUD model
 * 
 * @author Musabbir
 **/

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}


/**
 * Customers_model class
 *
 * @package default
 * @author  Musabbir
 **/

class Customers_model extends CI_Model
{

  public $variable;

  public function __construct()
  {
    parent::__construct();
    $this->load->database("inventory");
  }

    /**
     * This method is for getting all customer info inside data-table
 *
     * @param  $filter, $total_count
     * @return null
     * @author Musabbir 
     **/
    public function all_customer_info_for_datatable($filters,$total_count=false)
    {

     $this->db->select(
       'customer_custom_id as "0",
       customers_name as "1",
       customers_phone_1 as "2"
       '
       );
          //$this->db->select('concat(\'<button class="btn btn-sm green edit_customer" id="edit_\',user_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_customer"   data-toggle="modal" href="#responsive_modal" id="delete_\',user_id,\'">Delete</button>\') as "2"', FALSE);
     $this->db->select('concat(\'<button class="btn btn-xs green edit_user" id="edit_\',customers_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_user"   data-toggle="modal" href="#responsive_modal_delete" id="delete_\',customers_id,\'">Delete</button>\') as "3"', false);
     $this->db->from('customers');
     $this->db->where('publication_status', 'activated');

     if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
      $where ="(";
      $where.= "customer_custom_id like '%".$filters['search']['value']."%'or ";
      $where.= "customers_name like '%".$filters['search']['value']."%'or ";
      $where.= "customers_phone_1 like '%".$filters['search']['value']."%'";
      $where.= ")";

      $this->db->where($where, null, false);
    }
    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('customer_custom_id', 'asc');
    }
    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('customer_custom_id', 'desc');
    }

    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('customers_name', 'asc');
    }
    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('customers_name', 'desc');
    }

    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('customers_phone_1', 'asc');
    }
    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('customers_phone_1', 'desc');
    }

    if ($total_count) {
      return $this->db->get()->num_rows();            
    } else {
      $this->db->limit($filters['length'], $filters['start']);
      $q = $this->db->get();
     //  echo $this->db->last_query();
      $a = $q->result_array();
      return $a;
    }

  }

    /**
     * This method is for create a customer
 *
     * @param  $udata
     * @return array
     * @author Musabbir 
     **/
    public function createAccount($udata)
    {

     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();

     $udata['customers_id']=$uuid['UUID()'];
     $this->db->insert('customers', $udata);
     return $uuid['UUID()'];
   }


    /**
     * This method is for check customer duplicacy by email
 *
     * @param  $email
     * @return row
     * @author Musabbir 
     **/
    public function checkCustomerDuplicacy($email)
    {
     $this->db->select('customers_email');
     $this->db->from('customers');
     $this->db->where('customers_email', $email);
     return $this->db->get()->row();
   }


    /**
     * This method is for getting a customer info individually by his id
 *
     * @param  $customers_id
     * @return row array
     * @author Musabbir 
     **/
    public function customer_info_by_id($customers_id)
    {

     $this->db->select('customers_image,customers_name,customer_custom_id,customers_email,customers_present_address,customers_permanent_address,customers_phone_1,customers_phone_2,customers_national_id');
     $this->db->from('customers');
     $this->db->where('publication_status', 'activated');
     $this->db->where('customers_id', $customers_id);
     return $this->db->get()->row_array();
   }


    /**
     * This method is for updating a customer info individually
 *
     * @param  $customers_id, $data
     * @return true/false
     * @author Musabbir 
     **/
    public function update_customer_info($customers_id,$data)
    {
     return    $this->db->where('customers_id', $customers_id)->update('customers', $data);
   }


    /**
     * This method is for delete a customer individually. 
     * Basically we don't delete a customer permenantly but change his publication_status deactivated
 *
     * @param  $user_id
     * @return true/false
     * @author Musabbir 
     **/
    public function delete_user_info($user_id)
    {
     return    $this->db->set('publication_status', 'deactivated')->where('customers_id', $user_id)->update('customers');
   }

   public function check_customer_id_duplicacy($id_customer)
   {
    $this->db->select('customer_custom_id')->from('customers')->where('customer_custom_id', $id_customer);
    return $this->db->get()->row_array();
  }
    /**
     * Check customer already available or not
     *
     * @param  $customer_name
     * @param  $customer_phone
     * @return array[]
     * @author Shofik Shoaib 
     **/
    public function check_customer_availablity($customer_name,$customer_phone)
    {
      $this->db->select('customers_id', FALSE);
      $this->db->from('customers');
      $this->db->where('publication_status', 'activated');
      $this->db->where('customers_name', $customer_name);
      $this->db->where('customers_phone_1', $customer_phone);
      $return_data =  $this->db->get()->row_array();
      return $return_data['customers_id'];
    }

  }

  /* End of file Customers_model.php */
/* Location: ./application/models/Customers_model.php */