<?php 

/**
 * Customer CRUD model
 * 
 * @author Musabbir
 **/

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}


/**
 * Customers_model class
 *
 * @package default
 * @author  Musabbir
 **/

class Vendors_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();
        $this->load->database("inventory");
    }

    /**
     * This method is for getting all customer info inside data-table
 *
     * @param  $filter, $total_count
     * @return null
     * @author Musabbir 
     **/
    public function all_vendor_info_for_datatable($filters,$total_count=false)
    {

         $this->db->select(
             '
		vendors_name as "0",
		vendors_phone_1 as "1"
		'
         );
          //$this->db->select('concat(\'<button class="btn btn-sm green edit_customer" id="edit_\',user_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_customer"   data-toggle="modal" href="#responsive_modal" id="delete_\',user_id,\'">Delete</button>\') as "2"', FALSE);
         $this->db->select('concat(\'<button class="btn btn-xs green edit_user" id="edit_\',vendors_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_user"   data-toggle="modal" href="#responsive_modal" id="delete_\',vendors_id,\'">Delete</button>\') as "2"', false);
         $this->db->from('vendors');
         $this->db->where('publication_status', 'activated');

        if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
            $where ="(";
            $where.= "vendors_name like '%".$filters['search']['value']."%'or ";
            $where.= "vendors_phone_1 like '%".$filters['search']['value']."%'";
            $where.= ")";
            $this->db->where($where, null, false);
        }

        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('vendors_name', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('vendors_name', 'desc');
        }

        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('vendors_phone_1', 'asc');
        }
        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('vendors_phone_1', 'desc');
        }

        if ($total_count) {
            return $this->db->get()->num_rows();            
        } else {
            $this->db->limit($filters['length'], $filters['start']);
            $q = $this->db->get();
            $this->db->last_query();
            $a = $q->result_array();
            return $a;
        }

    }

    /**
     * This method is for create a customer
 *
     * @param  $udata
     * @return array
     * @author Musabbir 
     **/
    public function createAccount($udata)
    {

         $this->db->select('UUID()', false);    
         $uuid =$this->db->get()->row_array();

         $udata['vendors_id']=$uuid['UUID()'];
         $this->db->insert('vendors', $udata);
         return $uuid['UUID()'];
    }


    /**
     * This method is for check customer duplicacy by email
 *
     * @param  $email
     * @return row
     * @author Musabbir 
     **/
    public function checkCustomerDuplicacy($email)
    {
         $this->db->select('customers_email');
         $this->db->from('customers');
         $this->db->where('customers_email', $email);
         return $this->db->get()->row();
    }


    /**
     * This method is for getting a customer info individually by his id
 *
     * @param  $customers_id
     * @return row array
     * @author Musabbir 
     **/
    public function vendor_info_by_id($vendors_id)
    {

         $this->db->select('vendor_image,vendors_email,vendors_name,vendors_present_address,vendors_permanent_address,vendors_phone_1,vendors_phone_2,vendors_national_id');
         $this->db->from('vendors');
         $this->db->where('publication_status', 'activated');
         $this->db->where('vendors_id', $vendors_id);
         return $this->db->get()->row_array();
    }


    /**
     * This method is for updating a customer info individually
 *
     * @param  $customers_id, $data
     * @return true/false
     * @author Musabbir 
     **/
    public function update_vendor_info($customers_id,$data)
    {
         return    $this->db->where('vendors_id', $customers_id)->update('vendors', $data);
    }


    /**
     * This method is for delete a customer individually. 
     * Basically we don't delete a customer permenantly but change his publication_status deactivated
 *
     * @param  $user_id
     * @return true/false
     * @author Musabbir 
     **/
    public function delete_vendor_info($user_id)
    {
         return    $this->db->set('publication_status', 'deactivated')->where('vendors_id', $user_id)->update('vendors');
    }

}

/* End of file Customers_model.php */
/* Location: ./application/models/Customers_model.php */