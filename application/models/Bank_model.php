<?php
/**
 * Bank Model bank controller.
 *
 * PHP Version 5.6
 * CodeIgniter 3.0
 *
 * @copyright copyright@2016
 * @license   MAX Group BD
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Bank Model includes Bank controller.
 *
 * @subpackage Model
 * @author     shoaib <shofik.shoaib@gmail.com>
**/

class Bank_model extends CI_Model {

  public $is_vat_included;
  /**
 * This is a constructor functions.
 *
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
  public function __construct()
  {
   parent::__construct();
   
   $cookie = $this->input->cookie('language', true);
   $this->lang->load('model_lang', $cookie);
   $this->is_vat_included = $this->session->userdata('is_vat_included');
   
 }
    /**
 * Save data of bank form in database 
 *
 * @return null
 * @link   Bank_create/save_bank_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_bank_info($data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['banks_id']=$uuid['UUID()'];
     $this->db->insert('banks', $data);
     return $uuid['UUID()'];
   }
 /**
 * Collect all bank's information from Database
 * 
 *@param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Bank_create/all_bank_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
 public function all_bank_info($filters,$total_count=false)
 {
   $this->db->select(
    '
    banks_name as "0",
    charge_percentage as "1",
    DATE_FORMAT(date_created, "%d-%b-%Y") as "2"
    '
    );
   $lang_edit = $this->lang->line("model_edit");
   $lang_delete = $this->lang->line("model_delete");
   $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_bank\" id=\"edit_', CAST(banks_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_bank\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(banks_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);
   $this->db->from('banks');
   $this->db->where('publication_status', 'activated');
   if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    $where ="(";
    $where.= "banks_name like '%".$filters['search']['value']."%' or ";
    $where.= "banks_abbe like '%".$filters['search']['value']."%'";

    $where.= ")";
    $this->db->where($where, null, false);
  }
  if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    $this->db->like('banks_name', $filters['columns']['0']['search']['value'], 'both');
  }
  if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    $this->db->like('charge_percentage', $filters['columns']['1']['search']['value'], 'both');
  }
  if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    $range_date = explode('_', $filters['columns']['2']['search']['value']);
    if ($range_date[1]!=0 && $range_date[0]==0) {
      $this->db->where('DATE(date_created) >', DATE("1992-01-01"));
      $this->db->where('DATE(date_created) <', DATE($range_date[1]));
    }
    if ($range_date[1]==0 && $range_date[0]!=0) {
      $this->db->where('DATE(date_created) >', DATE($range_date[0]));
      $this->db->where('DATE(date_created) <', "DATE(NOW())");
    }
    if ($range_date[1]>0 && $range_date[0]>0) {
      $this->db->where('DATE(date_created) >=', DATE($range_date[0]));
      $this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    }
  }
  if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    $this->db->order_by('banks_name', 'asc');
  }
  if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    $this->db->order_by('banks_name', 'desc');
  }
  if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    $this->db->order_by('charge_percentage', 'asc');
  }
  if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    $this->db->order_by('charge_percentage', 'desc');
  }
  if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    $this->db->order_by('date_created', 'asc');
  }
  if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    $this->db->order_by('date_created', 'desc');
  }
  if ($total_count) {
    return $this->db->get()->num_rows();            
  } else {
    $this->db->limit($filters['length'], $filters['start']);
    $qry_get = $this->db->get();
    $output_arr =$qry_get->result_array();
    return $output_arr;
  }
}
    /**
 * Collect individual expense type's information from database by id
 *
 * @return array[]
 * @link   Expenditure_type/get_expenditure_type_info
 * @param  string $expenditure_types_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function bank_info_by_id($banks_id)
    {
     $this->db->select('banks_name,banks_abbe,banks_description,charge_percentage');
     $this->db->from('banks');
     $this->db->where('publication_status', 'activated');
     $this->db->where('banks_id', $banks_id);
     return $this->db->get()->row_array();
   }

    /**
 * Update individual bank's information of database by id
 *
 * @return boolean
 * @link   Bank_create/update_bank_info
 * @param  string  $banks_id Parameter-1
 * @param  array[] $data     Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function update_banks_info($banks_id, $data)
    {
     return ($this->db->where('banks_id', $banks_id)->update('banks', $data)); 
   }
 /**
 * Delete/Hide bank's information from database. 
 * It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Bank_create/delete_bank
 * @param  string $banks_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
 public function delete_bank_info($banks_id)
 {
  return $this->db->set('publication_status', 'deactivated')->where('banks_id', $banks_id)->update('banks');
         // return $this->db->affected_rows();
}
    /**
 * Save data of bank account form in database 
 *
 * @return null
 * @link   Bank_acc_create/save_bank_acc_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_bank_acc_info($data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['bank_acc_id']=$uuid['UUID()'];
     $this->db->insert('bank_accounts', $data);
     return $uuid['UUID()'];
   }
   /**
 * Collect all bank accounts information from database and display in AJAX Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Bank_acc_create/all_bank_accounts_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/
   public function all_bank_accounts_info($filters,$total_count=false)
   {
     $this->db->select(
       '
       banks.banks_name as "0",
       bank_accounts.bank_acc_num as "1",
       bank_accounts.final_amount as "2",
       bank_accounts.bank_acc_branch as "3",
       DATE_FORMAT(bank_accounts.date_created, "%d-%b-%Y") as "4"
       ', false
       );
     $lang_edit = $this->lang->line("model_edit");
     $lang_delete = $this->lang->line("model_delete");
     $lang_active= $this->lang->line("model_active");
     $lang_deactive= $this->lang->line("model_deactive");
     $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_acc\" id=\"edit_', CAST(bank_accounts.bank_acc_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_acc\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(bank_accounts.bank_acc_id AS CHAR) ,'\">$lang_delete</button>') as '5'", false);
     $this->db->from('bank_accounts');
     $this->db->join('banks', 'banks.banks_id = bank_accounts.banks_id', 'left');
     $this->db->where('bank_accounts.publication_status', 'activated');
     if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
      $where ="(";
      $where.= "banks.banks_name like '%".$filters['search']['value']."%' or ";
      $where.= "bank_accounts.bank_acc_num like '%".$filters['search']['value']."%' or ";
      $where.= "bank_accounts.bank_acc_branch like '%".$filters['search']['value']."%'";
      $where.= ")";

      $this->db->where($where, null, false);
    }

    if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
      $this->db->like('banks.banks_name', $filters['columns']['0']['search']['value'], 'both');
    }
    if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
      $this->db->like('bank_accounts.bank_acc_num', $filters['columns']['1']['search']['value'], 'both');
    }
    if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
      $this->db->like('bank_accounts.final_amount', $filters['columns']['2']['search']['value'], 'both');
    }
    if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
      $this->db->like('bank_accounts.bank_acc_branch', $filters['columns']['3']['search']['value'], 'both');
    }
    if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
      $range_date = explode('_', $filters['columns']['4']['search']['value']);
      if ($range_date[1]!=0 && $range_date[0]==0) {
        $this->db->where('DATE(bank_accounts.date_created) >', DATE("1992-01-01"));
        $this->db->where('DATE(bank_accounts.date_created) <', DATE($range_date[1]));
      }
      if ($range_date[1]==0 && $range_date[0]!=0) {
        $this->db->where('DATE(bank_accounts.date_created) >', DATE($range_date[0]));
        $this->db->where('DATE(bank_accounts.date_created) <', "DATE(NOW())");
      }
      if ($range_date[1]>0 && $range_date[0]>0) {
        $this->db->where('DATE(bank_accounts.date_created) >', DATE($range_date[0]));
        $this->db->where('DATE(bank_accounts.date_created) <', DATE($range_date[1]));
      }
    }
    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('banks.banks_name', 'asc');
    }
    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('banks.banks_name', 'desc');
    }
    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('bank_accounts.bank_acc_num', 'asc');
    }
    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('bank_accounts.bank_acc_num', 'desc');
    }
    if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('bank_accounts.bank_acc_branch', 'asc');
    }
    if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('bank_accounts.bank_acc_branch', 'desc');
    }
    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('bank_accounts.final_amount', 'asc');
    }
    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('bank_accounts.final_amount', 'desc');
    }
    if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('bank_accounts.date_created', 'asc');
    }
    if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('bank_accounts.date_created', 'desc');
    }
    if ($total_count) {
      return $this->db->get()->num_rows();            
    } else {
      $this->db->limit($filters['length'], $filters['start']);
      $qry_get = $this->db->get();
      $output_arr =$qry_get->result_array();
      return $output_arr;
    }
  }
 /**
 * Collect individual bank account's information from database by id
 *
 * @return array[]
 * @link   Bank_acc_create/get_bank_acc_info
 * @param  string $bank_acc_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
 public function bank_acc_info_by_id($bank_acc_id)
 {
   $this->db->select('banks.banks_id,banks.banks_name,bank_accounts.bank_acc_name,bank_accounts.bank_acc_num,bank_accounts.bank_acc_branch,bank_accounts.bank_acc_des');
   $this->db->from('bank_accounts');
   $this->db->join('banks','banks.banks_id = bank_accounts.banks_id', 'left');
   $this->db->where('bank_accounts.publication_status', 'activated');
   $this->db->where('bank_accounts.bank_acc_id', $bank_acc_id);
   return $this->db->get()->row_array();
 }
  /**
 * Update individual bank account's information of database by id
 *
 * @return boolean
 * @link   Bank_acc_create/update_bank_acc_info
 * @param  string  $bank_acc_id Parameter-1
 * @param  array[] $data     Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
  public function update_bank_acc_info($bank_acc_id, $data)
  {
   return ($this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $data)); 
 }
/**
 * Delete/Hide bank account's information from database. 
 * It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Bank_acc_create/delete_bank_acc
 * @param  string $bank_acc_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function delete_bank_acc_info($bank_acc_id)
{
  return $this->db->set('publication_status', 'deactivated')->where('bank_acc_id', $bank_acc_id)->update('bank_accounts');
}
    /**
 * Save data of bank form in database 
 *
 * @return null
 * @link   Bank_dpst_wdrl/save_deposit_withdrawal_amount_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_bank_dw_info($data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['bank_statement_id']=$uuid['UUID()'];
     $this->db->insert('bank_statement', $data);
     return $uuid['UUID()'];
   }

   public function get_final_amt_by_acc($bank_acc_id)
   {
    $this->db->select('final_amount', FALSE);
    $this->db->from('bank_accounts');
    $this->db->where('bank_acc_id', $bank_acc_id);
    return $this->db->get()->row_array();
  }
  public function update_total_amt_info_of_acc($final_data,$bank_acc_id)
  {
    return $this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $final_data);
  }
        /**
 * Collect all deposit - withdrawal  information of cash box from database 
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Nagadanboi/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
        public function all_bank_dw_info($filters,$total_count=false)
        {
         $lang_edit = $this->lang->line("model_edit");
         $lang_delete = $this->lang->line("model_delete");
         $this->db->select('cash_type as "0"', false);
         $this->db->select(
           '
           bank_accounts.bank_acc_num as "1",
           format(bank_statement.deposit_withdrawal_amount,2) as "2",
           DATE_FORMAT(bank_statement.date_created, "%d-%b-%Y") as "4",
           ', false
           );
         $arr_values = ['deposit', 'withdrawal','received'];
         $this->db->select('concat(SUBSTRING(bank_statement.cash_description, 1,10),\'<a class="info_cashbox" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',bank_statement.cash_description,\'" style="font-family: courier" >......</a>\') as "3"', false);
         $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_statement\" id=\"edit_', CAST(bank_statement.bank_statement_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_statement\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(bank_statement.bank_statement_id AS CHAR) ,'\">$lang_delete</button>') as '5'", false);
         $this->db->from('bank_statement');
         $this->db->join('bank_accounts', 'bank_accounts.bank_acc_id = bank_statement.bank_acc_id', 'left');
         $this->db->where('bank_statement.publication_status', 'activated');
         $this->db->where_in('bank_statement.cash_type', $arr_values);
         $this->db->limit(1000);
         if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
          $where ="(";
          $where.= "bank_statement.cash_type like '%".$filters['search']['value']."%' or ";
          $where.= "bank_accounts.bank_acc_num like '%".$filters['search']['value']."%' ";
          $where.= ")";
          $this->db->where($where, null, false);
        }
        if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
          $this->db->like('cash_type', $filters['columns']['0']['search']['value'], 'both');
        }
        if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
          $this->db->like('bank_accounts.bank_acc_num', $filters['columns']['1']['search']['value'], 'both');
        }
        if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
          $this->db->like('bank_statement.deposit_withdrawal_amount', $filters['columns']['2']['search']['value'], 'both');
        }
        if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
          $range_date = explode('_', $filters['columns']['4']['search']['value']);
          if ($range_date[1]!=0 && $range_date[0]==0) {
            $this->db->where('DATE(bank_statement.date_created) >', DATE("1992-01-01"));
            $this->db->where('DATE(bank_statement.date_created) <', DATE($range_date[1]));
          }
          if ($range_date[1]==0 && $range_date[0]!=0) {
            $this->db->where('DATE(bank_statement.date_created) >', DATE($range_date[0]));
            $this->db->where('DATE(bank_statement.date_created) <', "DATE(NOW())");
          }
          if ($range_date[1]>0 && $range_date[0]>0) {
            $this->db->where('DATE(bank_statement.date_created) >=', DATE($range_date[0]));
            $this->db->where('DATE(bank_statement.date_created) <=', DATE($range_date[1]));
          }
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          $this->db->order_by('bank_statement.cash_type', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          $this->db->order_by('bank_statement.cash_type', 'desc');
        }
        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
          $this->db->order_by('bank_accounts.bank_acc_num', 'asc');        
        }
        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
          $this->db->order_by('bank_accounts.bank_acc_num', 'desc');                
        }
        if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
          $this->db->order_by('bank_statement.deposit_withdrawal_amount', 'asc');        
        }
        if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
          $this->db->order_by('bank_statement.deposit_withdrawal_amount', 'desc');                
        }
        if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
          $this->db->order_by('bank_statement.date_created', 'asc');        
        }
        if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
          $this->db->order_by('bank_statement.date_created', 'desc');                
        }
        if ($total_count) {
          return $this->db->get()->num_rows();            
        } else {
          $this->db->limit($filters['length'], $filters['start']);
          $qry_get = $this->db->get();
          $output_arr =$qry_get->result_array();
          return $output_arr;
        }
      }
/**
 * collect bank deposit withdrawal informations form database.
 * 
 * @link   nagadanboi/get_cashbox_info
 * @param  string $cashbox_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/
public function all_bank_statement_by_id($bank_statement_id)
{
 $this->db->select('bank_statement.cash_type,bank_statement.cash_description,bank_statement.deposit_withdrawal_amount,bank_accounts.bank_acc_num,bank_accounts.bank_acc_id');
 $this->db->from('bank_statement');
 $this->db->join('bank_accounts', 'bank_accounts.bank_acc_id = bank_statement.bank_acc_id', 'left');
 $this->db->where('bank_statement.bank_statement_id', $bank_statement_id);
 return $this->db->get()->row_array();
}

public function current_bank_amount_info($bank_acc_id)
{
  $this->db->select('final_amount', FALSE);
  $this->db->from('bank_accounts');
  $this->db->where('bank_acc_id', $bank_acc_id);
  return $this->db->get()->row_array();
}

public function previous_amount_info_by_id($bank_statement_id)
{
  $this->db->select('cash_type,deposit_withdrawal_amount', FALSE);
  $this->db->from('bank_statement');
  $this->db->where('bank_statement_id', $bank_statement_id);
  return $this->db->get()->row_array();
}

public function update_bank_amount_info($final_data,$bank_acc_id)
{
  return $this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $final_data);
}

public function update_statement_info($bank_statement_id, $data)
{
  return $this->db->where('bank_statement_id', $bank_statement_id)->update('bank_statement', $data);
}

public function get_bank_acc_info($statement_id)
{
  $this->db->select('bank_acc_id, deposit_withdrawal_amount, cash_type', FALSE);
  $this->db->from('bank_statement');
  $this->db->where('bank_statement_id', $statement_id);
  return $this->db->get()->row_array();
}

public function get_bank_balance_info($bank_acc_id)
{
  $this->db->select('final_amount', FALSE);
  $this->db->from('bank_accounts');
  $this->db->where('bank_acc_id', $bank_acc_id );
  return $this->db->get()->row_array();
}
        /**
 * Delete/Hide statement's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Bank_dpst_wdrl/delete_statement
 * @param  string $statement_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
        public function delete_statement_info($statement_id, $data)
        {
         return $this->db->where('bank_statement_id', $statement_id)->update('bank_statement', $data);
       }

        /**
 * Upadte cashbox amount after loan paid.
 * 
 * @param  array[] $final_data
 * @link   Loan/update_loan_info
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

        public function update_acc_balance_info($final_data, $bank_acc_id)
        {
          $this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $final_data);
        }
/**
 * Save data of nagadanboi form  in database 
 *
 * @return void
 * @link   Nogodanboi/save_cash_deposit_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function save_buy_by_bank_info($bank_data)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $bank_data['bank_statement_id']=$uuid['UUID()'];
 $this->db->insert('bank_statement', $bank_data);
 return $uuid['UUID()'];
}

public function save_cheque_details_info($cheque_data)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $cheque_data['cheque_details_id']=$uuid['UUID()'];
 $this->db->insert('cheque_details', $cheque_data);
 return $uuid['UUID()'];
}

public function save_pending_cheque_details_info($cheque_data)
{
 $this->db->select('UUID()', false);    
 $uuid =$this->db->get()->row_array();
 $cheque_data['pending_cheque_id']=$uuid['UUID()'];
 $this->db->insert('pending_cheque_details', $cheque_data);
 return $uuid['UUID()'];
}

public function all_pending_cheque_info_list($filters,$total_count=false)
{
 $this->db->select(
  '
  sell_details.sell_local_voucher_no as "0",
  pending_cheque_details.cheque_number as "1",
  DATE_FORMAT(pending_cheque_details.date_created, "%d-%b-%Y") as "2"
  '
  );
 $lang_cash = $this->lang->line("cleared_by_cash");
 $lang_acc = $this->lang->line("cleared_by_acc");
 $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green cleared_by_cash\" data-toggle=\"modal\" href=\"#responsive_modal_cash\" id=\"cash_', CAST(pending_cheque_details.pending_cheque_id AS CHAR) ,'\">$lang_cash</button>','<button type=\"button\" class=\"btn btn-xs yellow cleared_by_acc\" data-toggle=\"modal\" id=\"acc_', CAST(pending_cheque_details.pending_cheque_id AS CHAR) ,'\">$lang_acc</button>') as '3'", false);
 $this->db->from('pending_cheque_details');
 $this->db->join('sell_details', 'sell_details.sell_details_id = pending_cheque_details.sell_details_id', 'left');
 $this->db->where('pending_cheque_details.publication_status', 'activated');
 if ($this->is_vat_included == "yes") {
  $this->db->where('sell_details.vat_amount >', 0);
}
$this->db->where('pending_cheque_details.cheque_status', 'pending');
if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
  $where ="(";
  $where.= "sell_details.sell_local_voucher_no like '%".$filters['search']['value']."%' or ";
  $where.= "pending_cheque_details.cheque_number like '%".$filters['search']['value']."%'";
  $where.= ")";
  $this->db->where($where, null, false);
}
if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
  $this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['0']['search']['value'], 'both');
}
if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
  $this->db->like('pending_cheque_details.cheque_number', $filters['columns']['1']['search']['value'], 'both');
}
if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
  $range_date = explode('_', $filters['columns']['2']['search']['value']);
  if ($range_date[1]!=0 && $range_date[0]==0) {
    $this->db->where('DATE(pending_cheque_details.date_created) >', DATE("1992-01-01"));
    $this->db->where('DATE(pending_cheque_details.date_created) <', DATE($range_date[1]));
  }
  if ($range_date[1]==0 && $range_date[0]!=0) {
    $this->db->where('DATE(pending_cheque_details.date_created) >', DATE($range_date[0]));
    $this->db->where('DATE(pending_cheque_details.date_created) <', "DATE(NOW())");
  }
  if ($range_date[1]>0 && $range_date[0]>0) {
    $this->db->where('DATE(pending_cheque_details.date_created) >=', DATE($range_date[0]));
    $this->db->where('DATE(pending_cheque_details.date_created) <=', DATE($range_date[1]));
  }
}
if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
  $this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
}
if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
  $this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
}
if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
  $this->db->order_by('pending_cheque_details.cheque_number', 'asc');
}
if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
  $this->db->order_by('pending_cheque_details.cheque_number', 'desc');
}
if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
  $this->db->order_by('pending_cheque_details.date_created', 'asc');
}
if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
  $this->db->order_by('pending_cheque_details.date_created', 'desc');
}
if ($total_count) {
  return $this->db->get()->num_rows();            
} else {
  $this->db->limit($filters['length'], $filters['start']);
  $qry_get = $this->db->get();
  $output_arr =$qry_get->result_array();
  return $output_arr;
}
}

public function get_cheque_details_by_id($pending_cheque_id)
{
  $this->db->select('sell_details_id,cheque_amount');  
  $this->db->from('pending_cheque_details');
  $this->db->where('pending_cheque_id', $pending_cheque_id);
  return $this->db->get()->row_array();
}
    /**
 * Save data of nagadanboi form  in database 
 *
 * @return void
 * @link   Nogodanboi/save_cash_deposit_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_cash_box_info($cash_data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $cash_data['cashbox_id']=$uuid['UUID()'];
     $this->db->insert('cashbox', $cash_data);
     
   }

   public function update_pending_chk_status($pending_cheque_id,$update_chk_status)
   {
     return $this->db->where('pending_cheque_id', $pending_cheque_id)->update('pending_cheque_details', $update_chk_status);
   }

    /**
 * Collect current cashbox amount form final_cashbox_amount table
 *
 * @link   Sell/save_sell_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function final_cashbox_amount_info()
    {
     $this->db->select('total_cashbox_amount');
     $this->db->from('final_cashbox_amount');
     $qry_get = $this->db->get();
     return $qry_get->row_array();
   }
    /**
 * Upadte cashbox amount after loan paid.
 * 
 * @param  array[] $final_data
 * @link   Loan/update_loan_info
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/
    public function update_total_cashbox_info($final_data)
    {
      return $this->db->update('final_cashbox_amount', $final_data);
    }
    /**
 *Activate bank accounts by id. It activate the bank account by id and deactivate previous one.
 *
 *@param  string $bank_acc_id
 *@link   Bank_acc_create/activate_acc
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function activate_acc_info($bank_acc_id)
    {
      return    $this->db->set('selection_status', 'selected')->where('bank_acc_id', $bank_acc_id)->update('bank_accounts');
    }

    public function change_status()
    {
     return $this->db->set('selection_status', 'not_selected')->update('bank_accounts');
   }

   public function update_cheque_info($expenditures_id,$cheque_data)
   {
     $this->db->where('buy_sell_or_others_id', $expenditures_id)->update('cheque_details', $cheque_data);
   }

   public function update_bank_statement($cashbox_id,$bank_data)
   {
     $this->db->where('buy_sell_and_other_id', $cashbox_id)->update('bank_statement', $bank_data);
   }

   public function search_bank($text)
   {
     $this->db->select('banks_name,banks_id');
     $this->db->from('banks');
     $this->db->like('banks_name', $text, 'both');
     $this->db->where('publication_status', 'activated');
     $this->db->limit(5);
     return $this->db->get()->result_array();
   }

   public function search_bank_acc($text)
   {
    $this->db->select('bank_accounts.bank_acc_num,bank_accounts.bank_acc_id,banks.charge_percentage');
    $this->db->from('bank_accounts');
    $this->db->join('banks', 'banks.banks_id = bank_accounts.banks_id', 'left');
    $this->db->like('bank_accounts.bank_acc_num', $text, 'both');
    $this->db->where('bank_accounts.publication_status', 'activated');
    $this->db->limit(5);
    return $this->db->get()->result_array();
  }

  public function count_bank_acc()
  {
    return $this->db->count_all('bank_accounts');
  }
}
/* End of file Bank_model.php */
/* Location: ./application/models/Bank_model.php */