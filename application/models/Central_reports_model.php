<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Central_reports_model extends CI_Model {
/**
 * This is a construtor functions.
 *
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function __construct()
{
	parent::__construct();
	$cookie = $this->input->cookie('language', TRUE);
	$this->lang->load('model_lang', $cookie);
}
/**
 * Display all items list with their qty.
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/items_list_with_quantity
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_items_list_with_qty($filters,$total_count=false)
{
	$this->db->select('
		item_spec_set.spec_set as "0",
		inventory.quantity as "1",

		');
	$this->db->from('inventory');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
	/* For top right search*/
	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
	{
		$where ="(";
		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
		$where.= ")";
		$this->db->where($where, NULL, FALSE);
	}
	/* For greater then and less than search for "top sell item"*/
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$range = explode('_', $filters['columns']['1']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('inventory.quantity <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('inventory.quantity >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('inventory.quantity <', $range[1]);
			$this->db->where('inventory.quantity >', $range[0]);
		}
	}
	/* For customer column based bottom search*/
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
		$this->db->like('item_spec_set.spec_set',$filters['columns']['0']['search']['value'], 'both');
	}
	/* For asc and desc orders*/
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('item_spec_set.spec_set', 'desc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('item_spec_set.spec_set', 'asc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('1', 'desc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('1', 'asc');
	}
	if ($total_count) {
		return $this->db->get()->num_rows();			
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$q = $this->db->get();
		$a = $q->result_array();
		return $a;
	}
}
/**
 * Display all expenditure report.
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/all_expenses_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_expenses_report_info($filters,$total_count=false)
{
	$this->db->select('
		expenditure_types.expenditure_types_name as "0",
		format(expenditures.expenditures_amount,2) as "1",
		DATE_FORMAT(expenditures.date_created, "%d-%b-%Y %h:%i %p") as "2",
		expenditures.expenditures_comments as "3",
		',false);
	$this->db->from('expenditures');
	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
	$this->db->where('expenditures.publication_status','activated');
	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
	{
		$where ="(";
		$where.= "expenditure_types.expenditure_types_name like '%".$filters['search']['value']."%' or ";
		$where.= "expenditures.expenditures_comments like '%".$filters['search']['value']."%' or ";
		$where.= "expenditures.expenditures_amount like '%".$filters['search']['value']."%'";
		$where.= ")";
		$this->db->where($where, NULL, FALSE);
	}
	/* For greater then and less than search for "amount of expense"*/
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$range = explode('_', $filters['columns']['1']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('expenditures.expenditures_amount <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('expenditures.expenditures_amount >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('expenditures.expenditures_amount <', $range[1]);
			$this->db->where('expenditures.expenditures_amount >', $range[0]);
		}
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
		$this->db->like('expenditure_types.expenditure_types_name',$filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null){
		$this->db->like('expenditures.expenditures_comments',$filters['columns']['3']['search']['value'], 'both');
	}
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
		$range_date = explode('_', $filters['columns']['2']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(expenditures.date_created) >', DATE("1992-01-01"));
			$this->db->where('DATE(expenditures.date_created) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(expenditures.date_created) >', DATE($range_date[0]));
			$this->db->where('DATE(expenditures.date_created) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(expenditures.date_created) >=', DATE($range_date[0]));
			$this->db->where('DATE(expenditures.date_created) <=', DATE($range_date[1]));
		}
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('expenditure_types.expenditure_types_name', 'asc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('expenditure_types.expenditure_types_name', 'desc');
	}

	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('expenditures.expenditures_amount', 'asc');
	}

	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('expenditures.expenditures_amount', 'desc');
	}

	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('expenditures.date_created', 'asc');
	}

	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('expenditures.date_created', 'desc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('expenditures.expenditures_comments', 'asc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('expenditures.expenditures_comments', 'desc');
	}
	if ($total_count) {
		return $this->db->get()->num_rows();			
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$q = $this->db->get();
		// echo $this->db->last_query();
		// exit();
		$a = $q->result_array();
		return $a;
	}
}
/**
 * Display all loan report.
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/all_loan_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_loan_report_info($filters,$total_count=false)
{
	$this->db->select('loan_type as "0"', FALSE);
	$this->db->select('
		format(loan_amount,2) as "1",
		format(total_payout_amount,2) as "2",
		DATE_FORMAT(date_created, "%d-%b-%Y %h:%i %p") as "3"
		',false);
	$this->db->from('loan');
	$this->db->where('publication_status','activated');
	// $this->db->order_by("date_created", "desc"); 
	// $this->db->limit(1000);
	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
	{
		$where ="(";
		$where.= "loan_amount like '%".$filters['search']['value']."%' ";
		$where.= ")";

		$this->db->where($where, NULL, FALSE);
	}
	/* For greater then and less than search for "amount of loan taken"*/
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$range = explode('_', $filters['columns']['1']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('loan_amount <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('loan_amount >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('loan_amount <', $range[1]);
			$this->db->where('loan_amount >', $range[0]);
		}
	}
	/* For greater then and less than search for "amount of loan payment made"*/
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
		$range = explode('_', $filters['columns']['2']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('loan_amount <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('loan_amount >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('loan_amount <', $range[1]);
			$this->db->where('loan_amount >', $range[0]);
		}
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
		$this->db->like('loan_type',$filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$this->db->like('loan_amount',$filters['columns']['1']['search']['value'], 'both');
	}
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
		$this->db->like('total_payout_amount',$filters['columns']['2']['search']['value'], 'both');
	}
	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null){
		$range_date = explode('_', $filters['columns']['3']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
			$this->db->where('DATE(date_created) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
		}
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('loan_amount', 'asc');		
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('loan_amount', 'desc');				
	}

	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('total_payout_amount', 'asc');		
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('total_payout_amount', 'desc');				
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('loan_description', 'asc');		
	}
	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('loan_description', 'desc');				
	}

	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('date_created', 'asc');		
	}
	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('date_created', 'desc');				
	}
	if ($total_count) {
		return $this->db->get()->num_rows();			
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$q = $this->db->get();
	// $this->db->last_query();
		$a = $q->result_array();
		return $a;
	}
}
/**
 * Display all buy due report.
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/all_buy_due_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_buy_due_report_info($filters,$total_count=false)
{
	$no_vendor = $this->lang->line("model_empty");
	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "0"', FALSE);
	$this->db->select('
		buy_details.voucher_no as "1",
		format(buy_details.net_payable,2) as "2",
		format(buy_details.paid,2) as "3",
		format(buy_details.due,2) as "4",
		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y %h:%i %p") as "5"
		',false);
	$this->db->from('buy_details');
	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
	$this->db->where('buy_details.publication_status','activated');
	$this->db->where('buy_details.due >', 0);
	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
	{
		$where ="(";
		$where.= "buy_details.voucher_no like '%".$filters['search']['value']."%' ";
		$where.= ")";

		$this->db->where($where, NULL, FALSE);
	}
	/* For greater then and less than search*/
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
		$range = explode('_', $filters['columns']['2']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('buy_details.net_payable <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('buy_details.net_payable >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('buy_details.net_payable <', $range[1]);
			$this->db->where('buy_details.net_payable >', $range[0]);
		}
	}
	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null){
		$range = explode('_', $filters['columns']['3']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('buy_details.paid <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('buy_details.paid >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('buy_details.paid <', $range[1]);
			$this->db->where('buy_details.paid >', $range[0]);
		}
	}
	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null){
		$range = explode('_', $filters['columns']['4']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('buy_details.due <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('buy_details.due >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('buy_details.due <', $range[1]);
			$this->db->where('buy_details.due >', $range[0]);
		}
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
		$this->db->like('vendors.vendors_name',$filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$this->db->like('buy_details.voucher_no',$filters['columns']['1']['search']['value'], 'both');
	}
	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null){
		$range_date = explode('_', $filters['columns']['5']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(buy_details.date_created) >', DATE("1992-01-01"));
			$this->db->where('DATE(buy_details.date_created) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(buy_details.date_created) >', DATE($range_date[0]));
			$this->db->where('DATE(buy_details.date_created) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(buy_details.date_created) >=', DATE($range_date[0]));
			$this->db->where('DATE(buy_details.date_created) <=', DATE($range_date[1]));
		}
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('vendors.vendors_name', 'asc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('vendors.vendors_name', 'desc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('buy_details.voucher_no', 'asc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('buy_details.voucher_no', 'desc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('buy_details.net_payable', 'asc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('buy_details.net_payable', 'desc');
	}
	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('buy_details.paid', 'asc');
	}
	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('buy_details.paid', 'desc');
	}
	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('buy_details.due', 'asc');
	}
	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('buy_details.due', 'desc');
	}
	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('buy_details.date_created', 'asc');
	}
	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('buy_details.date_created', 'desc');
	}
	if ($total_count) {
		return $this->db->get()->num_rows();			
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$q = $this->db->get();
		$a = $q->result_array();
		return $a;
	}
}
/**
 * Display all sell due report.
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/all_sell_due_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function all_sell_due_report_info($filters,$total_count=false)
{
	$no_customer = $this->lang->line("no_customer");
	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', FALSE);
	$this->db->select('
		sell_details.sell_local_voucher_no as "1",
		format(sell_details.net_payable,2) as "2",
		format(sell_details.paid,2) as "3",
		format(sell_details.due,2) as "4",
		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y %h:%i %p") as "5"
		',false);
	$this->db->from('sell_details');
	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
	$this->db->where('sell_details.publication_status','activated');
	$this->db->where('sell_details.due >', 0);
	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
	{
		$where ="(";
		$where.= "sell_details.sell_local_voucher_no like '%".$filters['search']['value']."%' or ";
		$where.= "customers.customers_name like '%".$filters['search']['value']."%'";
		$where.= ")";
		$this->db->where($where, NULL, FALSE);
	}
	/* For greater then and less than search*/
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
		$range = explode('_', $filters['columns']['2']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('sell_details.net_payable <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('sell_details.net_payable >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('sell_details.net_payable <', $range[1]);
			$this->db->where('sell_details.net_payable >', $range[0]);
		}
	}
	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null){
		$range = explode('_', $filters['columns']['3']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('sell_details.paid <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('sell_details.paid >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('sell_details.paid <', $range[1]);
			$this->db->where('sell_details.paid >', $range[0]);
		}
	}
	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null){
		$range = explode('_', $filters['columns']['4']['search']['value']);
		if ($range[1]>0 && $range[0]==0) {
			$this->db->where('sell_details.due <', $range[1]);
		}
		if ($range[1]==0 && $range[0]>0) {
			$this->db->where('sell_details.due >', $range[0]);
		}
		if ($range[1]>0 && $range[0]>0) {
			$this->db->where('sell_details.due <', $range[1]);
			$this->db->where('sell_details.due >', $range[0]);
		}
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
		$this->db->like('customers.customers_name',$filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
		$this->db->like('sell_details.sell_local_voucher_no',$filters['columns']['1']['search']['value'], 'both');
	}
	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null){
		$range_date = explode('_', $filters['columns']['5']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(sell_details.date_created) >', DATE("1992-01-01"));
			$this->db->where('DATE(sell_details.date_created) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(sell_details.date_created) >', DATE($range_date[0]));
			$this->db->where('DATE(sell_details.date_created) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(sell_details.date_created) >=', DATE($range_date[0]));
			$this->db->where('DATE(sell_details.date_created) <=', DATE($range_date[1]));
		}
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('customers.customers_name', 'asc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('customers.customers_name', 'desc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('sell_details.net_payable', 'asc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('sell_details.net_payable', 'desc');
	}
	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('sell_details.paid', 'asc');
	}
	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('sell_details.paid', 'desc');
	}	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('sell_details.due', 'asc');
	}
	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('sell_details.due', 'desc');
	}
	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc'){
		$this->db->order_by('sell_details.date_created', 'asc');
	}
	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc'){
		$this->db->order_by('sell_details.date_created', 'desc');
	}
	if ($total_count) {
		return $this->db->get()->num_rows();			
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$q = $this->db->get();
		$a = $q->result_array();
		return $a;
	}
}
/**
 * Generate total buy list report
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_buy($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('voucher_no,grand_total,discount,paid,due,DATE_FORMAT(date_created, "%d-%b-%Y") as "date_updated"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(paid),2) as "paid",format(sum(grand_total),2) as "grand_total",format(sum(discount),2) as "discount",format(sum(due),2) as "due"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate all sell list report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_sell($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sell_local_voucher_no,grand_total,discount,paid,due,DATE_FORMAT(date_created, "%d-%b-%Y") as "date_updated"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(paid),2) as "paid",format(sum(grand_total),2) as "grand_total",format(sum(discount),2) as "discount",format(sum(due),2) as "due"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect store info for report header.
 *
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function store_details()
{
	$this->db->select('name,address,website,phone', FALSE);
	$this->db->from('shop');
	return $this->db->get()->row_array();
}
/**
 * Generate expense report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_expense($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('
		expenditure_types.expenditure_types_name as "name",
		format(expenditures.expenditures_amount,2) as "amount",
		DATE_FORMAT(expenditures.date_created, "%d-%b-%Y") as "date",
		expenditures.expenditures_comments as "details"
		',false);
	$this->db->from('expenditures');
	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
	$this->db->where('date_format(expenditures.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(expenditures.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('expenditures.stores_id', $store_id);
	$this->db->where('expenditures.publication_status','activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total amount of expense for report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_expense_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(expenditures_amount),2) as "total_expense"', FALSE);
	$this->db->from('expenditures');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate all damage lost report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_damage_lost($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('
		item_spec_set.spec_set as "item",
		demage_lost.demage_lost_quantity as "qty",
		DATE_FORMAT(demage_lost.date_created, "%d-%b-%Y") as "date",
		demage_lost.demage_lost_description as "comments",
		');
	$this->db->from('demage_lost');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = demage_lost.item_spec_set_id', 'left');
	$this->db->where('date_format(demage_lost.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(demage_lost.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('demage_lost.stores_id', $store_id);
	$this->db->where('demage_lost.publication_status','activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_damage_lost_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(demage_lost_quantity),2) as "total_qty"', FALSE);
	$this->db->from('demage_lost');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate loan report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_loan($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('loan_type as "type"', FALSE);
	$this->db->select('
		format(loan_amount,2) as "amount",
		format(total_payout_amount,2) as "paymentmade",
		DATE_FORMAT(date_created, "%d-%b-%Y") as "date",
		loan_description as "comments",
		',false);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status','activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of loan taken and payment made
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_loan_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(loan_amount),2) as "total_amount", format(sum(total_payout_amount),2) as "total_paymentmade"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate buy due list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_buy_due($from_create_date,$to_create_date,$store_id)
{
	$no_vendor = "Empty";
	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "vendor"', FALSE);
	$this->db->select('
		buy_details.voucher_no as "voucher",
		format(buy_details.net_payable,2) as "net_payable",
		format(buy_details.paid,2) as "paid",
		format(buy_details.due,2) as "due",
		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "date"
		',false);
	$this->db->from('buy_details');
	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
	$this->db->where('date_format(buy_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(buy_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('buy_details.stores_id', $store_id);
	$this->db->where('buy_details.publication_status','activated');
	$this->db->where('buy_details.due >', 0);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of net payable , paid and due
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_due_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(net_payable),2) as "total_net_payable", format(sum(paid),2) as "total_paid",  format(sum(due),2) as "total_due", format(count(voucher_no),2) as "voucher_no"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('due >', 0);
	return $this->db->get()->row_array();
}
/**
 * Generate sell due list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_sell_due($from_create_date,$to_create_date,$store_id)
{
	$no_vendor = "Empty";
	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_vendor.'" ) as "customer"', FALSE);
	$this->db->select('
		customers.customers_phone_1 as "phone",
		sell_details.sell_local_voucher_no as "voucher",
		format(sell_details.net_payable,2) as "net_payable",
		format(sell_details.paid,2) as "paid",
		format(sell_details.due,2) as "due",
		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "date"
		',false);
	$this->db->from('sell_details');
	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_details.stores_id', $store_id);
	$this->db->where('sell_details.publication_status','activated');
	$this->db->where('sell_details.due >', 0);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of net payable , paid and due
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_due_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(net_payable),2) as "total_net_payable", format(sum(paid),2) as "total_paid",  format(sum(due),2) as "total_due", format(count(sell_local_voucher_no),2) as "voucher"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('due >', 0);
	return $this->db->get()->row_array();
}
/**
 * Generate top revenue items list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_top_rev_items($from_create_date,$to_create_date,$search_limit,$store_id)
{
	$this->db->select('item_spec_set.spec_set as "item_name",format(sum(sell_cart_items.sub_total),2) as "total_amount",sum(sell_cart_items.quantity) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_cart_items.stores_id', $store_id);
	$this->db->where('sell_cart_items.publication_status','activated');
	$this->db->group_by('sell_cart_items.item_spec_set_id');
	$this->db->order_by('sum(sell_cart_items.sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of revenue amount and quantity
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function top_rev_items_details($from_create_date,$to_create_date,$search_limit,$store_id)
{
	$this->db->select('format(sum(sub_total),2) as "total_amount", format(sum(quantity),2) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status','activated');
	$this->db->order_by('sum(sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->row_array();
}
/**
 * Generate purchase list report in a date range.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_top_profit_items_for_buy($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('item_spec_set.spec_set,sum(buy_cart_items.quantity),format(sum(buy_cart_items.buying_price),2),', FALSE);
	$this->db->from('buy_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->join('sell_cart_items', 'sell_cart_items.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->where('buy_cart_items.date_created >=', $from_create_date);
	$this->db->where('buy_cart_items.date_created <=', $to_create_date);
	$this->db->where('buy_cart_items.stores_id', $store_id);
	$this->db->where('buy_cart_items.publication_status', 'activated');
	return $this->db->get()->result_array();
}
/**
 * Collect total buy amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_amount($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(paid) as "paid"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total sell amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(paid) as "paid"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total loan amount taken in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_loan_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(loan_amount) as "loan_amount"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total loan payment-made amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_loan_paid_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(total_payout_amount) as "total_payout_amount"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total deposit amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_deposit_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "deposit_withdrawal_amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('cash_type', 'deposit');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total withdrawal amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_withdrawal_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "deposit_withdrawal_amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('cash_type', 'withdrawal');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total damage lost amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_damage_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(sub_total) as "damage_lost_amount"', FALSE);
	$this->db->from('demage_lost');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total expense amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_expense_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(expenditures_amount) as "expenditures_amount"', FALSE);
	$this->db->from('expenditures');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total vat amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_vat_amount($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(vat_amount),2) as "vat_amount"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total landed cost in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_landed_cost_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(landed_cost) as "landed_cost"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
public function calculate_top_rev_category($from_create_date,$to_create_date,$search_limit,$store_id)
{
	$this->db->select('categories.categories_name as "category",format(sum(sell_cart_items.sub_total),2) as "total_amount",sum(sell_cart_items.quantity) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'right');
	$this->db->join('items', 'items.items_id = item_spec_set.items_id', 'right');
	$this->db->join('categories', 'categories.categories_id = items.categories_id', 'right');
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_cart_items.stores_id', $store_id);
	$this->db->where('sell_cart_items.publication_status','activated');
	$this->db->group_by('categories.categories_id');
	$this->db->order_by('sum(sell_cart_items.sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->result_array();
}

public function calculate_total_items_inventory($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('inventory.quantity,item_spec_set.spec_set ', FALSE);
	$this->db->from('inventory');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');	
	// $this->db->where('buy_cart_items.date_created >=', $from_create_date);
	// $this->db->where('buy_cart_items.date_created <=', $to_create_date);
	// $this->db->where('buy_cart_items.stores_id', $store_id);
	// $this->db->where('buy_cart_items.publication_status', 'activated');
	$this->db->where('inventory.quantity >', 0);
	return $this->db->get()->result_array();
}

public function calculate_total_vat($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(vat_amount,2) as "vat", sell_local_voucher_no', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_details.stores_id', $store_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('vat_amount >', 0);
	$this->db->group_by('sell_local_voucher_no');
	$this->db->order_by('vat_amount', 'desc');
	return $this->db->get()->result_array();

}

public function all_customers_list()
{
	$this->db->select('customers_name,customers_phone_1,customers_present_address,customers_email', FALSE);
	$this->db->from('customers');
	return $this->db->get()->result_array();
}

public function all_vendors_list()
{
	$this->db->select('vendors_name,vendors_present_address,vendors_phone_1,vendors_email', FALSE);
	$this->db->from('vendors');
	return $this->db->get()->result_array();
}

public function calculate_total_dpst_wtdrl($from_create_date,$to_create_date)
{
	$this->db->select('cashbox.deposit_withdrawal_amount,cashbox.cash_description,users.name,cashbox.cash_type,DATE_FORMAT(cashbox.date_created, "%d-%b-%Y") as "date"', FALSE);
	$this->db->from('cashbox');
	$this->db->join('users', 'users.user_id = cashbox.user_id', 'left');
	$this->db->where('date_format(cashbox.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(cashbox.date_created, "%Y-%m-%d") <=', $to_create_date);
	$type = array('deposit', 'withdrawal');
	$this->db->where_in('cashbox.cash_type', $type);	
	$this->db->where('cashbox.publication_status','activated');
	$this->db->order_by('cashbox.date_created', 'asc');
	return $this->db->get()->result_array();
}

public function total_dpst_details($from_create_date,$to_create_date)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('cash_type', 'deposit');
	// $this->db->group_by('cash_type');
	return $this->db->get()->row_array();
}

public function total_wtdrl_details($from_create_date,$to_create_date)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('cash_type', 'withdrawal');
	return $this->db->get()->row_array();
}

public function search_item($text)
{
	$this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,item_spec_set.item_spec_set_image');
	$this->db->from('item_spec_set');
	$this->db->where('publication_status', 'activated');
	$this->db->like('spec_set', $text, 'both');
	$this->db->or_like('barcode_number', $text, 'both');
	$this->db->limit(5);
	return $this->db->get()->result_array()
}

}
/* End of file Central_reports.php */
/* Location: ./application/models/Central_reports.php */