<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AttendanceModel extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getUserList($term)
	{
		$queryStirng =  "SELECT user_id AS id, username AS  value FROM users WHERE username LIKE '%{$term}%'";
		$query = $this->db->query($queryStirng);
		return $query->result_array();
	}

	public function attendanceEntry($data)
	{
		return $this->db->insert('attendance', $data);
	}

	public function todayDuplicacyCheck($id_user, $date)
	{
		$this->db->select('*');
		$this->db->from('attendance');
		$this->db->where('id_user', $id_user);
		$this->db->where('date', $date);
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	public function checkUserValidity($id_user)
	{
		$this->db->select('username, user_id');
		$this->db->from('users');
		$this->db->where('user_id', $id_user);	
		$this->db->limit(1);
		return $this->db->get()->row_array();
	}

	public function all_users_attendance_info($filters,$total_count=false)
	{

		$this->db->select(
			'users.username as "0",
			DATE_FORMAT(attendance.date, "%d-%b-%y") as "1",
			attendance.in_time as "2",
			attendance.out_time as "3"'
			);
		$this->db->select('concat(\'<button class="btn btn-sm green edit_attendance" id="edit_\',attendance.id_attendance,\'">Edit</button>\',\'<button style="display:none;" class="btn btn-sm red delete_user"   data-toggle="modal" href="#responsive_modal" id="delete_\',attendance.id_attendance,\'">Delete</button>\') as "4"', false);

		$this->db->from('attendance');
		$this->db->join('users', 'users.user_id = attendance.id_user', 'left');

		if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
			$where ="(";
			$where.= "users.username like '%".$filters['search']['value']."%'or ";
			$where.= "attendance.date like '%".$filters['search']['value']."%'";
			$where.= ")";
			$this->db->where($where, null, false);
		}

		if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
			$this->db->order_by('users.username', 'asc');
		}
		if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
			$this->db->order_by('users.username', 'desc');
		}

		if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
			$this->db->order_by('attendance.date', 'asc');
		}
		if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
			$this->db->order_by('attendance.date', 'desc');
		}

		if ($total_count) {
			return $this->db->get()->num_rows();            
		} else {
			$this->db->limit($filters['length'], $filters['start']);
			$q = $this->db->get();
            // echo $this->db->last_query();
			$a = $q->result_array();
			return $a;
		}

	}

}

/* End of file  */
/* Location: ./application/models/ */