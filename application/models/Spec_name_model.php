<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Spec_name_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();
        
    }

    /**
 * Save data of specification name form  in database 
 *
 * @return string
 * @link   Spec_name/save_spec_name_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_spec_name_info($data)
    {

         $this->db->select('UUID()', false);    
         $uuid =$this->db->get()->row_array();
         $data['spec_name_id']=$uuid['UUID()'];
         $this->db->insert('spec_name_table', $data);
         return $uuid['UUID()'];
    }



    /**
 * Collect all expenditure type's information from Database
 * 
 *@param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Expenditure_type/all_expenditure_type_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_spec_name_info($filters,$total_count=false)
    {
         $this->db->select(
             '
		spec_name as "0",
		DATE_FORMAT(date_created, "%d-%b-%Y %h:%i %p") as "2"

		'
         );

         $lang_edit = $this->lang->line("model_edit");
         $lang_delete = $this->lang->line("model_delete");
    
         /*another way to display multiple language*/

         // $query_string = "concat('<button type=\"button\" class=\"btn btn-sm green edit_spec_name\" id=\"edit_', CAST(spec_name_id AS CHAR) ,'\">$lang</button>') as '3'";
         // $this->db->select($query_string, FALSE);


         $this->db->select('concat(SUBSTRING(spec_description, 1,10),\'<a class="info_spec_description" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',spec_description,\'" style="font-family: courier" >...</a>\') as "1"', false);

         $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_spec_name\" id=\"edit_', CAST(spec_name_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_spec_name\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(spec_name_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);

         $this->db->from('spec_name_table');
         $this->db->where('publication_status', 'activated');

        if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
            $where ="(";
            $where.= "spec_name like '%".$filters['search']['value']."%' or ";
            $where.= "spec_description like '%".$filters['search']['value']."%'";

            $where.= ")";
            $this->db->where($where, null, false);
        }

        if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
            $this->db->like('spec_name', $filters['columns']['0']['search']['value'], 'both');
        }
        if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
            $this->db->like('spec_description', $filters['columns']['1']['search']['value'], 'both');
        }
        if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
            $range_date = explode('_', $filters['columns']['2']['search']['value']);
            if ($range_date[1]!=0 && $range_date[0]==0) {
                $this->db->where('DATE(date_created) >', DATE("1992-01-01"));
                $this->db->where('DATE(date_created) <', DATE($range_date[1]));
            }
            if ($range_date[1]==0 && $range_date[0]!=0) {
                $this->db->where('DATE(date_created) >', DATE($range_date[0]));
                $this->db->where('DATE(date_created) <', "DATE(NOW())");
            }
            if ($range_date[1]>0 && $range_date[0]>0) {
                $this->db->where('DATE(date_created) >=', DATE($range_date[0]));
                $this->db->where('DATE(date_created) <=', DATE($range_date[1]));
            }
        }

        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('spec_name', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('spec_name', 'desc');
        }

        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('spec_description', 'asc');
        }
        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('spec_description', 'desc');
        }

        if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('date_created', 'asc');
        }
        if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('date_created', 'desc');
        }

        if ($total_count) {
            return $this->db->get()->num_rows();            
        } else {
            $this->db->limit($filters['length'], $filters['start']);
            $q = $this->db->get();
            $a = $q->result_array();
            return $a;
        }

    }

    /**
 * Collect Specification's all information form database by spec_name_id
 *
 * @param  string $spec_id
 * @link   Spec_name/get_spec_name_info
 * @return void
 * @author jk
 **/
    public function spec_name_info_by_id($spec_id)
    {
         $this->db->select('spec_name,spec_description,spec_default_status', false);
         $this->db->from('spec_name_table');
         $this->db->where('spec_name_id', $spec_id);
         return $this->db->get()->row_array();
    }



    /**
 * Update individual specification's information of database by id
 *
 * @return boolean
 * @link   Spec_name/update_spec_name_info
 * @param  string  $spec_name_id Parameter-1
 * @param  array[] $data         Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function update_spec_name_info($spec_name_id,$data)
    {

         return $this->db->where('spec_name_id', $spec_name_id)->update('spec_name_table', $data);
         // return $this->db->affected_rows();
    }

    /**
 * Delete/Hide specification's information from database. 
 * It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Spec_name/delete_spec_name
 * @param  string $spec_name_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_spec_name_info($spec_name_id)
    {

         return    $this->db->set('publication_status', 'deactivated')->where('spec_name_id', $spec_name_id)->update('spec_name_table');
         // return $this->db->affected_rows();
    }

    /**
 * Check that if the spec name already created or not.
 *
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function check_spec_name($spec_name)
    {
         $this->db->select('spec_name', false);
         $this->db->from('spec_name_table');
         $this->db->where('spec_name', $spec_name);
         $this->db->where('publication_status', 'activated');

         return $this->db->get()->row_array();
    }


}

/* End of file Spec_name_model.php */
/* Location: ./application/models/Spec_name_model.php */