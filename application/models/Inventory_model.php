<?php 

/**
 * Inventory Model includes Buy,Sell,Tax, Expenditure_types, Expenditures, Customer,Loan,Demage and Lost,Nagadanboi controller.
 *
 * PHP Version 5.6
 * CodeIgniter 3.0
 *
 * @copyright copyright@2016
 * @license   MAX Group BD
 */


if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Inventory Model includes Buy,Sell,Tax, Expenditure_types, Expenditures, Customer controller.
 *
 * @subpackage Model
 * @author     shoaib <shofik.shoaib@gmail.com>
**/


class Inventory_model extends CI_Model
{

	public $variable;
	public $is_vat_included;

    /**
 * This is a construtor functions.
 *
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function __construct()
    {
    	parent::__construct();
    	$cookie = $this->input->cookie('language', true);
    	$this->lang->load('model_lang', $cookie);
    	$this->is_vat_included = $this->session->userdata('is_vat_included');

    }



    public function store_details()
    {
    	$this->db->select('*', FALSE);
    	$this->db->from('shop');
    	return $this->db->get()->row_array();
    }

    /**
 * Collect individual tax_type's information from database by id
 *
 * @return array[]
 * @link   Tax_type/get_tax_type_info
 * @param  string $tax_types_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function tax_type_info_by_id($tax_types_id)
    {

    	$this->db->select('tax_types_name,tax_types_percentage,tax_types_description');
    	$this->db->from('tax_types');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('tax_types_id', $tax_types_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Add items to buy_cart_items table after purchase items form vendors
 *
 * @param  array[] data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/




    /**
 * Update individual tax_type's information of database by id
 *
 * @return boolean
 * @link   Tax_type/update_tax_type_info
 * @param  string  $tax_types_id Parameter-1
 * @param  array[] $data         Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_tax_types_info($tax_types_id,$data)
    {

    	$this->db->where('tax_types_id', $tax_types_id)->update('tax_types', $data);
    	return $this->db->affected_rows();
    }

    /**
 * Save data of tax_type form in database 
 *
 * @return null
 * @link   Tax_type/save_tax_type_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_tax_type_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['tax_types_id']=$uuid['UUID()'];
    	$this->db->insert('tax_types', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Collect all tax_types information from database 
 *
 * @return array[]
 * @link   Tax_type/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_tax_types_info()
    {

    	$this->db->select('tax_types_id,tax_types_name,tax_types_percentage');
    	$this->db->from('tax_types');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }


    public function all_tax_type_info($filters,$total_count=false)
    {
    	$this->db->select(
    		'
    		tax_types_name as "0"
    		'
    		);

    	$this->db->select('concat(\'<button class="btn btn-sm green edit_tax_type" id="edit_\',tax_types_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_tax_type" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',tax_types_id,\'">Delete</button>\') as "1"', false);
    	$this->db->from('tax_types');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "tax_types_name like '%".$filters['search']['value']."%'";
    		$where.= ")";
            $this->db->where($where, null, false);
        }

        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
        	$this->db->order_by('tax_types_name', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
        	$this->db->order_by('tax_types_name', 'desc');
        }

        if ($total_count) {
        	return $this->db->get()->num_rows();            
        } else {
        	$this->db->limit($filters['length'], $filters['start']);
        	$qry_get = $this->db->get();
        	$output_arr =$qry_get->result_array();
        	return $output_arr;
        }
    }

    /**
 * Delete/Hide tax_type's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Tax_type/delete_tax_type
 * @param  string $tax_types_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_tax_type_info($tax_types_id)
    {

    	return    $this->db->set('publication_status', 'deactivated')->where('tax_types_id', $tax_types_id)->update('tax_types');
    }

    /**
 * Collect all customers information from database 
 *
 * @return array[]
 * @link   Customer/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_customers_info()
    {
    	$this->db->select('customers_id,customers_name,customers_phone_1,customers_present_address');
    	$this->db->from('customers');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }


    /**
 * Save data of nagadanboi form  in database 
 *
 * @return void
 * @link   Nogodanboi/save_cash_deposit_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_cash_box_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['cashbox_id']=$uuid['UUID()'];
    	$this->db->insert('cashbox', $data);
    	return $uuid['UUID()'];

    }

    /**
 * Collect individual expense type's information from database by id
 *
 * @return array[]
 * @link   Expenditure_type/get_expenditure_type_info
 * @param  string $expenditure_types_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function expenditure_type_info_by_id($expenditure_types_id)
    {

    	$this->db->select('expenditure_types_name,expenditure_types_description');
    	$this->db->from('expenditure_types');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('expenditure_types_id', $expenditure_types_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Update individual expense-type's information of database by id
 *
 * @return boolean
 * @link   Expenditure_type/update_expenditure_type_info
 * @param  string  $expenditure_types_id Parameter-1
 * @param  array[] $data                 Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function update_expenditure_type_info($expenditure_types_id,$data)
    {

    	$this->db->where('expenditure_types_id', $expenditure_types_id)->update('expenditure_types', $data);
    	return $this->db->affected_rows();
    }

    /**
 * Save data of expense_type form  in database 
 *
 * @return string
 * @link   Expenditure_type/save_expenditure_type_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_expenditure_type_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();
    	$data['expenditure_types_id']=$uuid['UUID()'];
    	$this->db->insert('expenditure_types', $data);
    	return $uuid['UUID()'];
    }


    /**
 * Collect all expenditure type's information from database 
 *
 * @return array[]
 * @link   Expenditure_type/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_expenditure_types_info()
    {

    	$this->db->select('expenditure_types_id,expenditure_types_name,expenditure_types_description');
    	$this->db->from('expenditure_types');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }

    /**
 * Delete/Hide expenditure_type's information from database. 
 * It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Expenditure_type/delete_expenditure_type
 * @param  string $expenditure_types_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_expenditure_type_info($expenditure_types_id)
    {

    	$this->db->set('publication_status', 'deactivated')->where('expenditure_types_id', $expenditure_types_id)->update('expenditure_types');
    	return $this->db->affected_rows();
    }


    /**
 * Collect all expenditure's information from database 
 *
 * @return array[]
 * @link   Expenditure/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_expenditures_info()
    {

    	$this->db->select('expenditure_types.expenditure_types_id,expenditure_types.expenditure_types_name,expenditures.expenditures_id,expenditures.expenditure_types_id,expenditures.publication_status,expenditures.expenditures_amount,expenditures.expenditures_comments');
         // $this->db->select('expenditures_id,expenditure_types_id,expenditures_amount');
    	$this->db->from('expenditures');
    	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
    	$this->db->where('expenditures.publication_status', 'activated');
    	return $this->db->get()->result_array();
    }


    /**
 * Save data of expense form  in database 
 *
 * @return string
 * @link   Expenditure/save_expenditure_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_expenditure_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['expenditures_id']=$uuid['UUID()'];
    	$this->db->insert('expenditures', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Delete/Hide expenditure's information from database. 
 * It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Expenditure/delete_expenditure
 * @param  string $expenditures_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_expenditure_info($expenditures_id)
    {

    	$this->db->set('publication_status', 'deactivated')->where('expenditures_id', $expenditures_id)->update('expenditures');
    	return $this->db->affected_rows();
    }


    /**
 * Collect individual expenses's information from database by id
 *
 * @return array[]
 * @link   Expenditure/get_expenditure_info
 * @param  string $expenditures_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function expenditure_info_by_id($expenditures_id)
    {

    	$this->db->select('expenditure_types.expenditure_types_id,expenditure_types.expenditure_types_name,expenditures.expenditures_id,expenditures.expenditure_types_id,expenditures.publication_status,expenditures.expenditures_amount,expenditures.expenditures_comments,expenditures.expenditures_date,expenditures.payment_type,bank_statement.bank_acc_id,cheque_details.cheque_number,bank_accounts.bank_acc_num, DATE_FORMAT(expenditures.date_created, "%Y-%m-%d") as "date"');
    	$this->db->from('expenditures');
    	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
    	$this->db->join('bank_statement', 'bank_statement.buy_sell_and_other_id = expenditures.expenditures_id', 'left');
    	$this->db->join('bank_accounts', 'bank_accounts.bank_acc_id = bank_statement.bank_acc_id', 'left');
    	$this->db->join('cheque_details', 'cheque_details.buy_sell_or_others_id = expenditures.expenditures_id', 'left');
    	$this->db->where('expenditures.publication_status', 'activated');
    	$this->db->where('expenditures.expenditures_id', $expenditures_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Update individual expenditure's information of database by id
 *
 * @return boolean
 * @link   expenditure/update_expenditure_info
 * @param  string  $expenditures_id Parameter-1
 * @param  array[] $data            Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_expenditure_info($expenditures_id,$data)
    {

    	return $this->db->where('expenditures_id', $expenditures_id)->update('expenditures', $data);
         // return $this->db->affected_rows();
    }

    /**
 * Save basic nad common buy informations  in buy details table like grand total,discount,voucher no ertc.
 *
 * @return array[]
 * @param  array[] $ready_data_basic data of input fields.
 * @link   Buy/save_buy_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_buy_basic_info($ready_data_basic)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$ready_data_basic['buy_details_id']=$uuid['UUID()'];
    	$this->db->insert('buy_details', $ready_data_basic);
    	return $uuid['UUID()'];
    }

    /**
 * undocumented function
 *
 * @return void
 * @author rasel
 **/


    public function update_buy_basic_info($ready_data_basic,$id)
    {

    	$this->db->where('buy_details_id', $id);
    	$this->db->update('buy_details', $ready_data_basic);
    	return $id;
    }

    /**
 * Save individual items information of buy list
 *
 * @param  array[] $data             parameter-1
 * @param  string  $buy_details_uuid parameter-2
 * @link   Buy/save_buy_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/


    public function save_buy_item_info($data,$buy_details_uuid, $purchase_date)
    {
    	foreach ($data as $key => $value) {
    		$this->db->select('UUID()', false);    
    		$uuid =$this->db->get()->row_array();
    		unset($data[$key]['item_name']);
    		unset($data[$key]['actual_quantity']);
    		unset($data[$key]['unique_barcode']);
    		unset($data[$key]['cart_temp_id']);
    		if($purchase_date != null){
    			$data[$key]['date_created'] = $purchase_date;
    		}
    		$data[$key]['buy_details_id'] = $buy_details_uuid;
    		$data[$key]['user_id'] = $this->session->userdata('user_id');    
    		$data[$key]['buy_cart_items_id']=$uuid['UUID()'];

    	}
    	return $this->db->insert_batch('buy_cart_items', $data);
    }

    /**
 * Save individual items information of advance buy list in buy_cart_items table
 *
 * @param  array[] $data             parameter-1
 * @param  string  $buy_details_uuid parameter-2
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/



    public function save_advance_item_info_buy_cart($data,$buy_details_uuid)
    {

    	foreach ($data as $key => $value) {
    		$this->db->select('UUID()', false);    
    		$uuid =$this->db->get()->row_array();
    		unset($data[$key]['item_name']);
    		$data[$key]['quantity'] = 0;
    		$data[$key]['buy_details_id'] = $buy_details_uuid;
    		$data[$key]['user_id'] = $this->session->userdata('user_id');    
    		$data[$key]['buy_cart_items_id']=$uuid['UUID()'];

    	}
    	return $this->db->insert_batch('buy_cart_items', $data);

    }



    /**
 * Update item's quantity of inventory table after purchase
 *
 * @link   Buy/save_buy_info
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function save_item_info_to_inventory($data)
    {

    	foreach ($data as $key => $value) {
    		$this->db->select('UUID()', false);    
    		$uuid =$this->db->get()->row_array();
            $sub_total = $value['sub_total'];
            unset($value['item_name']);
            unset($value['buying_price']);
            unset($value['buying_price_with_landed_cost']);
            unset($value['retail_price']);
            unset($value['whole_sale_price']);
            unset($value['comments']);
            unset($value['discount_type']);
            unset($value['discount_amount']);
            unset($value['sub_total']);
            unset($value['unique_barcode']);
            unset($value['cart_temp_id']);

            unset($value['actual_quantity']);
            $item_id= $value['item_spec_set_id'];

            $current_quantity= $this->items_info_from_inventory($item_id);

            $value['inventory_id']=$uuid['UUID()'];

            if($current_quantity['quantity'] == null) {

                $value['avg_buy_price'] = round(($sub_total/$value['quantity']),2);
                $this->db->insert('inventory', $value);    

            }

            if ($current_quantity['quantity']>=0) {

                $total_cost = ($current_quantity['quantity'] * $current_quantity['avg_buy_price']) + $sub_total;
                $total_qty = $current_quantity['quantity'] + $value['quantity'];
                $value['avg_buy_price'] = round(($total_cost/$total_qty),2);

                $value['quantity']= $value['quantity']+$current_quantity['quantity'];
                $iid = $value['item_spec_set_id'];
                unset($value['item_spec_set_id']);
                unset($value['inventory_id']);
                $this->db->where('item_spec_set_id', $iid)->update('inventory', $value);
            }

        }
    }




    /**
 * Collect item's quantity form Inventory Table
 *
 * @link   Inventory_model/save_item_info_to_inventory
 * @link   Demage_and_lost/save_demage_lost
 * @param  string $item_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function items_info_from_inventory($item_id)
    {
    	$this->db->select('quantity,avg_buy_price');
    	$this->db->from('inventory');
    	$this->db->where('item_spec_set_id', $item_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Save basic and common sell informations in sell_details table like grand_total,discount etc
 *
 * @param  array[] $ready_data_basic
 * @link   Sell/save_sell_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_sell_basic_info($ready_data_basic)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$ready_data_basic['sell_details_id']=$uuid['UUID()'];
    	$this->db->insert('sell_details', $ready_data_basic);
         // return $this->db->insert_id();
    	return $uuid['UUID()'];
    }

    /**
 * undocumented function
 *
 * @param  array[] $data              parameter-1
 * @param  string  $sell_details_uuid parameter-2
 * @link   Sell/save_sell_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_sell_item_info($data,$sell_details_uuid , $sell_date)
    {

        foreach ($data as $key => $value) {
            $item_id = $value['item_spec_set_id'];
            $avg_buy_price = $this->items_info_from_inventory($item_id);
            $this->db->select('UUID()', false);    
            $uuid =$this->db->get()->row_array();
            unset($data[$key]['item_name']);
            unset($data[$key]['product_warranty']);
            unset($data[$key]['actual_quantity']);
            unset($data[$key]['imei_used']);
            unset($data[$key]['temp_id_info']);
            if($sell_date != ""){
             $data[$key]['date_created'] = $sell_date;
         }
         $data[$key]['total_buy_price'] = $avg_buy_price['avg_buy_price'] * $value['quantity'];
         $data[$key]['avg_buy_price'] = $avg_buy_price['avg_buy_price'];
         $data[$key]['sell_details_id'] = $sell_details_uuid;
         $data[$key]['user_id'] = $this->session->userdata('user_id');    
         $data[$key]['sell_cart_items_id']=$uuid['UUID()'];

     }
     return $this->db->insert_batch('sell_cart_items', $data);

 }

    /**
 * Update Item's quantity after sell a item
 *
 * @param  array[] $data
 * @link   Sell/save_sell_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/


    public function save_sell_item_info_to_inventory($data)
    {
    	foreach ($data as $key => $value) {
    		unset($value['item_name']);
    		unset($value['selling_price']);
    		unset($value['sell_comments']);
    		unset($value['discount_type']);
    		unset($value['actual_quantity']);
    		unset($value['sell_type']);
    		unset($value['discount_amount']);
    		unset($value['sub_total']);
    		unset($value['product_warranty']);
    		unset($value['imei_used']);
    		unset($value['temp_id_info']);
    		unset($value['used_imei_barcode']);
    		$item_id= $value['item_spec_set_id'];
    		$current_quantity = $this->items_info_from_inventory($item_id);            
            $value['quantity']= $current_quantity['quantity']-$value['quantity'];
            $quantity = $value['quantity'];
            $this->db->where('item_spec_set_id', $value['item_spec_set_id'])->update('inventory', $value);
        }
    }
    // /**
    //  * Collect Item's sell quantity of last 30 days
    //  *
    //  * @param array[] $data
    //  * @link Sell/save_sell_info
    //  * @return void
    //  * @author shoaib <shofik.shoaib@gmail.com> 
    //  **/
    // public function  total_sell_qty_of_last_month($data)
    // {

    // 	foreach ($data as $key => $value) {
    // 		unset($value['item_name']);
    // 		unset($value['selling_price']);
    // 		unset($value['sell_comments']);
    // 		unset($value['discount_type']);
    // 		unset($value['actual_quantity']);
    // 		unset($value['sell_type']);
    // 		unset($value['discount_amount']);
    // 		unset($value['sub_total']);

    // 		$item_id= $value['item_spec_set_id'];

    // 		return $last_thirty_days_sell_qty = $this->thirty_days_sell_qty($item_id);

    // 	}
    // }

    /**
 * Collect item's sell quantity for last 30 days
 *
 * @link   Inventory_model/save_item_info_to_inventory
 * @link   Demage_and_lost/save_demage_lost
 * @param  string $item_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function thirty_days_sell_qty($item_id)
    {
         // $current_date= date("Y-m-d");
    	$starting_date = date('Y-m-d', strtotime('today - 30 days'));  

    	$this->db->select('SUM(quantity) AS quantity');

    	$this->db->from('sell_cart_items');
    	$this->db->where('item_spec_set_id', $item_id);
    	$this->db->where('date_created >=', $starting_date);

    	return $this->db->get()->row_array();
    }

    /**
 *  Select retail or wholesale price by item's id.
 *
 * @param  string $item item's id
 * @link   Sell/get_item_price_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function select_item_price_info($data)
    {
    	$this->db->select('buy_cart_items.retail_price,buy_cart_items.whole_sale_price,inventory.quantity,item_spec_set.product_warranty');
    	$this->db->from('buy_cart_items');
    	$this->db->join('inventory', 'inventory.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
    	$this->db->where('buy_cart_items.item_spec_set_id', $data);
    	$this->db->where('buy_cart_items.publication_status', 'activated');
    	$this->db->order_by('buy_cart_items.date_updated', 'desc');
    	$this->db->limit(1);
    	$qry_get = $this->db->get();
    	return $qry_get->result_array();
    }



    /**
 * Collect current cashbox amount form final_cashbox_amount table
 *
 * @link   Sell/save_sell_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function final_cashbox_amount_info()
    {

    	$this->db->select('total_cashbox_amount', FALSE);
    	$this->db->from('final_cashbox_amount');
         // $this->db->where('items_id', $item);
    	$qry_get = $this->db->get();
    	return $qry_get->row_array();
    }

    /**
 * Collect all expenditure type's information from Database
 * 
 *@param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Expenditure_type/all_expenditure_type_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_expenditure_type_info($filters,$total_count=false)
    {
    	$this->db->select(
    		'
    		expenditure_types_name as "0",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "2"

    		'
    		);

    	$lang_edit = $this->lang->line("model_edit");
    	$lang_delete = $this->lang->line("model_delete");

    	/*another way to display multiple language*/

         // $query_string = "concat('<button type=\"button\" class=\"btn btn-sm green edit_expenditure_type\" id=\"edit_', CAST(expenditure_types_id AS CHAR) ,'\">$lang</button>') as '3'";
         // $this->db->select($query_string, FALSE);


    	$this->db->select('concat(SUBSTRING(expenditure_types_description, 1,10),\'<a class="info_expenditure_type_description" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',expenditure_types_description,\'" style="font-family: courier" >...</a>\') as "1"', false);

    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_expenditure_type\" id=\"edit_', CAST(expenditure_types_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_expenditure_type\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(expenditure_types_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);

    	$this->db->from('expenditure_types');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "expenditure_types_name like '%".$filters['search']['value']."%' or ";
    		$where.= "expenditure_types_description like '%".$filters['search']['value']."%'";

    		$where.= ")";
    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('expenditure_types_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('expenditure_types_description', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditure_types_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditure_types_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditure_types_description', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditure_types_description', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('date_created', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}


    }
    /**
 * Collect all deposit - withdrawal  information of cash box from database 
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Nagadanboi/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_cashbox_info($filters,$total_count=false)
    {
         // echo "<pre>";
         // print_r($filters);
         // exit();
    	$lang_edit = $this->lang->line("model_edit");
    	$lang_delete = $this->lang->line("model_delete");

         // $this->db->select('if(cash_type="deposit", "জমা", "উত্তোলন") as "0"', FALSE);
    	$this->db->select('cash_type as "0"', false);

    	$this->db->select(
    		'
    		format(deposit_withdrawal_amount,2) as "1",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "3"
    		', false
    		);

    	$arr_values = ['deposit', 'withdrawal','transferred','commission','advance_to_vendor'];

    	$this->db->select('concat(SUBSTRING(cash_description, 1,10),\'<a class="info_cashbox" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',cash_description,\'" style="font-family: courier" >......</a>\') as "2"', false);

    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs red delete_cashbox\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(cashbox_id AS CHAR) ,'\">$lang_delete</button>') as '4'", false);

    	$this->db->from('cashbox');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where_in('cash_type', $arr_values);


         // $this->db->order_by("date_created", "desc"); 

    	$this->db->limit(1000);

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "cash_type like '%".$filters['search']['value']."%' or ";
    		$where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%' ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('cash_type', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('deposit_withdrawal_amount', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('cash_description', $filters['columns']['2']['search']['value'], 'both');
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('cash_type', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('cash_type', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('deposit_withdrawal_amount', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('deposit_withdrawal_amount', 'desc');                
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('cash_description', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('cash_description', 'desc');                
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('date_created', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('date_created', 'desc');                
    	}


    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();

    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }

    /**
 * Collect all expenditure information from database and display in AJAX Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Expenditure/all_expenditure_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_expenditure_info($filters,$total_count=false)
    {

    	$this->db->select(
    		'
    		expenditure_types.expenditure_types_name as "0",
    		format(expenditures.expenditures_amount,2) as "1",
    		DATE_FORMAT(expenditures.date_created, "%d-%b-%Y") as "3"
    		', false
    		);
    	$this->db->select('concat(SUBSTRING(expenditures.expenditures_comments, 1,10),\'<a class="info_expenditure" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',expenditures.expenditures_comments,\'" style="font-family: courier" >...</a>\') as "2"', false);

    	$lang_edit = $this->lang->line("model_edit");
    	$lang_delete = $this->lang->line("model_delete");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs red delete_expenditure\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(expenditures.expenditures_id AS CHAR) ,'\">$lang_delete</button>') as '4'", false);
// '<button type=\"button\" class=\"btn btn-xs green edit_expenditure\" id=\"edit_', CAST(expenditures.expenditures_id AS CHAR) ,'\">$lang_edit</button>',

         // $this->db->select('concat(\'<button class="btn btn-xs green edit_expenditure"  id="edit_\',expenditures.expenditures_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_expenditure" data-toggle="modal" href="#responsive_modal_delete"  id="delete_\',expenditures.expenditures_id,\'">Delete</button>\') as "4"', FALSE);

    	$this->db->from('expenditures');
    	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
    	$this->db->where('expenditures.publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "expenditure_types.expenditure_types_name like '%".$filters['search']['value']."%' or ";
    		$where.= "expenditures.expenditures_amount like '%".$filters['search']['value']."%' or ";
    		$where.= "expenditures.expenditures_comments like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('expenditure_types.expenditure_types_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('expenditures.expenditures_amount', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('expenditures.expenditures_comments', $filters['columns']['2']['search']['value'], 'both');
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(expenditures.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(expenditures.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(expenditures.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(expenditures.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(expenditures.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(expenditures.date_created) <', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditure_types.expenditure_types_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditure_types.expenditure_types_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.expenditures_amount', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.expenditures_amount', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.expenditures_comments', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.expenditures_comments', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.date_created', 'desc');
    	}



    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }


    public function all_size_info($filters,$total_count=false)
    {

    	$this->db->select(
    		'
    		sizes_name as "0"
    		'
    		);
    	$this->db->select('concat(\'<button class="btn btn-sm green edit_size" id="edit_\',sizes_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_size" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',sizes_id,\'">Delete</button>\') as "1"', false);

    	$this->db->from('sizes');

         //$this->db->join('expenditures', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sizes_name like '%".$filters['search']['value']."%' ";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sizes_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sizes_name', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }


    /**
 * Collect all due buy information from database for AJAX Datatable 
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Buy_list/all_due_buy_info_for_datatable
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_buy_due_list_info($filters,$total_count=false)
    {

    	$no_vendor = $this->lang->line("model_empty");
    	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "0"', false);

    	$this->db->select(
    		'
    		buy_details.voucher_no as "1",
    		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "2"
    		'
    		);
    	$lang_paymentmade = $this->lang->line("model_paymentmade");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green pay_due_amount\" id=\"update_', CAST(buy_details.buy_details_id AS CHAR) ,'\">$lang_paymentmade</button>') as '3'", false);
         // $this->db->select('concat(\'<button class="btn btn-xs green pay_due_amount" id="update_\',buy_details_id,\'">Paymentmade</button>\') as "2"', FALSE);

    	$this->db->from('buy_details');
    	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
    	$this->db->where('buy_details.publication_status', 'activated');
    	$this->db->not_like('buy_details.voucher_no', 'T-', 'after');
    	$this->db->where('buy_details.due >', 0);

         // $this->db->order_by("date_created", "desc"); 

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "buy_details.voucher_no like '%".$filters['search']['value']."%' ";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('vendors.vendors_name', $filters['columns']['0']['search']['value'], 'both');
    	}

    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('buy_details.voucher_no', $filters['columns']['1']['search']['value'], 'both');
    	}

    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(buy_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(buy_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('vendors.vendors_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('vendors.vendors_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.voucher_no', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.date_created', 'desc');
    	}


    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }

    /**
 * Collect individual product's quantity from database 
 *
 * @return array[]
 * @link   Sell/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function product_quantity_info()
    {

    	$this->db->select('buy_details_id,grand_total,net_payable,due,paid,voucher_no');
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();

    }

    /**
 * Collect the last voucher no for auto generating the next one.
 *
 * @link   Sell/index
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function last_sell_voucher_no_info()
    {

    	return  $this->db->count_all('sell_details');

         // $this->db->select('sell_local_voucher_no', FALSE);
         // $this->db->from('sell_details');
         // // $this->db->where('items_id', $item);
         // $this->db->order_by('date_created', 'desc');

         // // $this->db->limit(1);
         // $qry_get = $this->db->get();
         // return $qry_get->row_array();
    }

    /**
 * Collect buy informations from 
 *
 * @param  string $buy_details_id
 * @link   Buy_list/get_buy_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function buy_info_by_id($buy_details_id)
    {

    	$this->db->select('net_payable,voucher_no,paid,due,buy_details_id');
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('buy_details_id', $buy_details_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Save due paymentmade information and update final_cashbox_amount,buy_details etc.
 *
 * @link   Buy_list/save_due_paymentmade
 * @param  array[] $data
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/


    public function save_due_paymentmade_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['due_paymentmade_id']=$uuid['UUID()'];
    	$this->db->insert('due_paymentmade', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Collect current paid and due of individual buy
 * 
 * @param  string $buy_details_id
 * @link   Buy_list/save_due_paymentmade
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function current_paid_and_due_amount($buy_details_id)
    {
    	$this->db->select('paid,due,discount,net_payable');
    	$this->db->from('buy_details');
    	$this->db->where('buy_details_id', $buy_details_id);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    /**
 * Update buy paid due information after due paymentmade
 *
 * @link   Buy_list/save_due_paymentmade
 * @param  string  $buy_details_id
 * @param  array[] $updated_data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_buy_after_due_paymentmade($buy_details_id,$updated_data)
    {

    	return $this->db->where('buy_details_id', $buy_details_id)->update('buy_details', $updated_data);
    // echo $this->db->last_query();
    // return $this->db->affected_rows();
    }

    /**
 * Collect all sell due list form database for AJAX Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Sell_list_due/all_sell_list_due_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_sell_list_due_info($filters,$total_count=false)
    {

    	$no_customer = $this->lang->line("no_customer");

    	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', false);
    	$this->db->select(
    		'

    		sell_details.sell_local_voucher_no as "1",
    		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "2"
    		'
    		);
    	$lang_paymentmade = $this->lang->line("model_paymentmade");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-sm green pay_due_amount\" id=\"update_', CAST(sell_details.sell_details_id AS CHAR) ,'\">$lang_paymentmade</button>') as '3'", false);

         // $this->db->select('concat(\'<button class="btn btn-sm green pay_due_amount" id="update_\',sell_details.sell_details_id,\'">Details</button>\') as "3"', FALSE);

    	$this->db->from('sell_details');
    	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
    	$this->db->not_like('sell_details.custom_voucher_no', 'T-', 'after');
    	$this->db->where('sell_details.due >', 0);
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('sell_details.vat_amount >', 0);
    	}

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sell_details.sell_local_voucher_no like '%".$filters['search']['value']."%' or ";
    		$where.= "customers.customers_name like '%".$filters['search']['value']."%'";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['1']['search']['value'], 'both');
    	}


    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(sell_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(sell_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers.customers_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers.customers_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.date_created', 'desc');
    	}



    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }

    /**
 * Collect all sell informations from database.
 * 
 *@param  string $sell_details_id
 * @link   sell_list_due/get_sell_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function sell_info_by_id($sell_details_id)
    {

    	$this->db->select('sell_details.grand_total,sell_details.sell_local_voucher_no,sell_details.paid,sell_details.due,sell_details.discount,sell_details.net_payable,sell_cart_items.quantity,sell_cart_items.selling_price,sell_cart_items.sub_total,sell_cart_items.discount_amount,sell_cart_items.discount_type,item_spec_set.spec_set');

    	$this->db->from('sell_details');
    	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
    	$this->db->where('sell_details.sell_details_id', $sell_details_id);
    	return $this->db->get()->result_array();

    }

    /**
 * Collect paid and due amount of individual sell
 * 
 *@param  string $sell_details_id
 * @link   sell_list_due/save_due_paymentmade
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function sell_paid_and_due_amount($sell_details_id)
    {

    	$this->db->select('paid,due,discount,discount_percentage,net_payable,grand_total,card_charge_amt');
    	$this->db->from('sell_details');
    	$this->db->where('sell_details_id', $sell_details_id);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();

    }

    public function sell_details_by_id($sell_details_id)
    {
    	$this->db->select('*');
    	$this->db->from('sell_details');
    	$this->db->where('sell_details_id', $sell_details_id);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    /**
 * Update the sell_details table after due_paymentmade by customer.
 * 
 * @link   sell_list_due/save_due_paymentmade
 *@param  string  $sell_details_id
 * @param  array[] $updated_data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_sell_after_due_paymentmade($sell_details_id,$updated_data)
    {

    	$this->db->where('sell_details_id', $sell_details_id)->update('sell_details', $updated_data);
    	return $this->db->affected_rows();
    }

    /**
 * Collect all sell data for AJAZ Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   sell_list_all/all_sell_list_info_for_datatable
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function sell_list_all_info($filters,$total_count=false)
    {

    	$no_customer = $this->lang->line("no_customer");

    	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', false);
    	$this->db->select(
    		'
    		customers.customers_phone_1 as "1",
    		sell_details.sell_local_voucher_no as "2",
    		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "3"
    		'
    		);

    	$lang_details = $this->lang->line("model_details");
    	$lang_delete = $this->lang->line("model_delete");
    	$lang_edit = "Edit";
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green sell_voucher_details\" id=\"details_', CAST(sell_details.sell_details_id AS CHAR) ,'\">$lang_details</button>','<button type=\"button\" class=\"btn btn-xs red delete_sell_info\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(sell_details.sell_details_id AS CHAR) ,'\">$lang_delete</button>','<button type=\"button\" class=\"btn btn-xs blue sell_voucher_edit\" id=\"edit_', CAST(sell_details.sell_details_id AS CHAR) ,'\">$lang_edit</button>') as '4'", false);


         // $this->db->select('concat(\'<button class="btn btn-xs green sell_voucher_details" id="edit_\',sell_details.sell_details_id,\'">Details</button>\',\'<button class="btn btn-xs red delete_sell_info" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',sell_details.sell_details_id,\'">Delete</button>\') as "3"', FALSE);

    	$this->db->from('sell_details');
    	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
    	$this->db->join('imei_barcode', 'imei_barcode.sell_details_id = sell_details.sell_details_id', 'left');
    	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
    	$this->db->not_like('sell_details.custom_voucher_no', 'T-', 'after');
    	$this->db->where('sell_details.publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('sell_details.vat_amount >', 0);
    	}
    	$this->db->group_by('sell_details.sell_local_voucher_no');
         // $this->db->order_by("sell_details.date_created", "desc"); 

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
    		$where.= "imei_barcode.imei_barcode like '%".$filters['search']['value']."%' or ";
    		$where.= "item_spec_set.barcode_number like '%".$filters['search']['value']."%' or ";
    		$where.= "item_spec_set.item_spec_set_id like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('customers.customers_phone_1', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['2']['search']['value'], 'both');
    	}

    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(sell_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(sell_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers.customers_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers.customers_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers.customers_phone_1', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers.customers_phone_1', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.date_created', 'asc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.date_created', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }

    /**
 * Collect all sell informations by id
 *
 * @param  string $sell_details_id
 * @link   sell_list_all/get_all_sell_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_sell_info_by_id($sell_details_id)
    {
        $this->db->select('DISTINCT imei_barcode.imei_barcode,
            sell_details.grand_total,
            sell_details.sell_local_voucher_no,
            sell_details.paid,
            sell_details.due,
            sell_details.discount,
            sell_details.net_payable,
            sell_cart_items.quantity,
            sell_cart_items.sub_total,
            sell_cart_items.discount_amount,
            sell_cart_items.discount_type,
            sell_cart_items.selling_price,
            item_spec_set.spec_set,
            sell_cart_items.used_imei_barcode,
            sell_details.discount_percentage,
            ', FALSE);
        $this->db->from('sell_details');
        $this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
        $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
        $this->db->join('imei_barcode', 'imei_barcode.sell_details_id = sell_details.sell_details_id AND imei_barcode.item_spec_set_id = item_spec_set.item_spec_set_id ', 'left');
     // $this->db->group_by('imei_barcode.imei_barcode_id');
        $this->db->where('sell_details.publication_status', 'activated');
        $this->db->where('sell_details.sell_details_id', $sell_details_id);
        return $this->db->get()->result_array();
     // echo $this->db->last_query();
    }
    
    public function get_and_short_unique_id()
    {
    	$this->db->select('UUID_short()', false);    
    	$uuid =$this->db->get()->row_array();
    	return $uuid['UUID_short()'];
    }
    /**
 * Delete/Hide sell's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Sell_list_all/delete_sell
 * @param  string $sell_details_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_sell_info($sell_details_id)
    {
    	$this->db->set('publication_status', 'deactivated')->where('sell_details_id', $sell_details_id)->update('sell_details');
    	return    $this->db->set('publication_status', 'deactivated')->where('sell_details_id', $sell_details_id)->update('sell_cart_items');
    }
    /**
 * Collect all buy information from database 
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Buy_list_all/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_buy_info_list($filters,$total_count=false)
    {
    	$no_vendor = $this->lang->line("model_empty");
    	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "0"', false);
    	$this->db->select(
    		'
    		buy_details.voucher_no as "1",
    		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "2"
    		'
    		,false);
    	$lang_edit = $this->lang->line("model_edit");
    	$lang_details = $this->lang->line("model_details");
    	$lang_delete = $this->lang->line("model_delete");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green buy_voucher_details\" id=\"update_', CAST(buy_details.buy_details_id AS CHAR) ,'\">$lang_details</button>','<button type=\"button\" class=\"btn btn-xs red delete_buy_info\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(buy_details.buy_details_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);
    	/*for edit button in future if necessary  ,'<button type=\"button\" class=\"btn btn-xs blue edit_buy_info\"  id=\"edit_', CAST(buy_details.buy_details_id AS CHAR) ,'\">$lang_edit</button>'*/
         // $this->db->select('concat(\'<button class="btn btn-xs green buy_voucher_details" id="update_\',buy_details_id,\'">Details</button>\',\'<button class="btn btn-xs red delete_buy_info" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',buy_details_id,\'">Delete</button>\') as "2"', FALSE);

    	$this->db->from('buy_details');
    	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
    	$this->db->join('imei_barcode', 'imei_barcode.buy_details_id = buy_details.buy_details_id', 'left');
    	$this->db->join('buy_cart_items', 'buy_cart_items.buy_details_id = buy_details.buy_details_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
    	$this->db->where('buy_details.publication_status', 'activated');
    	$this->db->not_like('buy_details.voucher_no', 'T-', 'after');
    	$this->db->group_by('buy_details.voucher_no');

         // $this->db->order_by("date_created", "desc"); 

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
    		$where.= "imei_barcode.imei_barcode like '%".$filters['search']['value']."%' or ";
    		$where.= "item_spec_set.barcode_number like '%".$filters['search']['value']."%' or ";
    		$where.= "item_spec_set.item_spec_set_id like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('vendors.vendors_name', $filters['columns']['0']['search']['value'], 'both');
    	}

    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('buy_details.voucher_no', $filters['columns']['1']['search']['value'], 'both');
    	}

    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(buy_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(buy_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('vendors.vendors_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('vendors.vendors_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.voucher_no', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.date_created', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }

    public function all_advance_buy_info_list($filters,$total_count=false)
    {

    	$this->db->select(
    		'
    		voucher_no as "0",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "1"
    		'
    		);
    	$this->db->select('concat(\'<button class="btn btn-xs green advance_buy_details" id="update_\',advance_buy_details_id,\'">Details</button>\',\'<button class="btn btn-xs red delete_advance_buy_info" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',advance_buy_details_id,\'">Delete</button>\') as "2"', false);

    	$this->db->from('advance_buy_details');
    	$this->db->where('publication_status', 'activated');

    	$this->db->order_by("date_created", "desc"); 

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "voucher_no like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('voucher_no', $filters['columns']['0']['search']['value'], 'both');
    	}

    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('voucher_no', 'desc');
    	}


    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }

    /**
 * Collect all buy informations form buy_details table.
 *
 * @param  string $buy_details_id
 * @link   Buy_list_all/get_all_buy_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_buy_info_by_id($buy_details_id)
    {

    	$this->db->select('grand_total,voucher_no,paid,due,discount,net_payable');
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('buy_details_id', $buy_details_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect individual items information form buy_cart_items.
 *
 * @param  string $buy_details_id
 * @link   Buy_list_all/get_all_buy_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function details_buy_info_by_id($buy_details_id)
    {

    	$this->db->select('buy_details.landed_cost,buy_details.grand_total,buy_details.voucher_no,buy_details.paid,buy_details.due,buy_details.discount,buy_details.net_payable,buy_cart_items.quantity,buy_cart_items.sub_total,buy_cart_items.discount_type,buy_cart_items.discount_amount,buy_cart_items.buying_price,item_spec_set.spec_set,buy_details.date_created', false);
    	$this->db->select('(select IFNULL( group_concat(imei_barcode.imei_barcode),"N/A") 
    		from imei_barcode
    		where imei_barcode.buy_details_id = buy_cart_items.buy_details_id AND imei_barcode.item_spec_set_id= buy_cart_items.item_spec_set_id ) as imei_barcode', FALSE);
    	$this->db->from('buy_details');
    	$this->db->join('buy_cart_items', 'buy_cart_items.buy_details_id = buy_details.buy_details_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
    	$this->db->where('buy_details.publication_status', 'activated');
    	$this->db->where('buy_details.buy_details_id', $buy_details_id);
    	return $this->db->get()->result_array();
    }


    public function details_advance_buy_info_by_id($advance_buy_details_id)
    {
    	$this->db->select('advance_buy_details.advance_buy_details_id,advance_buy_details.grand_total,advance_buy_details.voucher_no,advance_buy_details.paid,advance_buy_details.due,advance_buy_details.discount,advance_buy_details.net_payable,advance_buy_cart_items.quantity,advance_buy_cart_items.buying_price,items.items_id,items.items_name');

    	$this->db->from('advance_buy_details');
    	$this->db->join('advance_buy_cart_items', 'advance_buy_cart_items.advance_buy_details_id = advance_buy_details.advance_buy_details_id', 'left');
    	$this->db->join('items', 'items.items_id = advance_buy_cart_items.items_id', 'left');
    	$this->db->where('advance_buy_details.publication_status', 'activated');
    	$this->db->where('advance_buy_details.advance_buy_details_id', $advance_buy_details_id);
    	return $this->db->get()->result_array();
    }

    /**
 * Delete/Hide buy's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Buy_list_all/delete_buy
 * @param  string $sell_details_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_buy_info($buy_details_id)
    {

    	$this->db->set('publication_status', 'deactivated')->where('buy_details_id', $buy_details_id)->update('buy_details');
    	return    $this->db->set('publication_status', 'deactivated')->where('buy_details_id', $buy_details_id)->update('buy_cart_items');
    }

    /**
 * collect cashbox informations form database.
 * 
 * @link   nagadanboi/get_cashbox_info
 * @param  string $cashbox_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_cashbox_info_by_id($cashbox_id)
    {

    	$this->db->select('cashbox.cash_type,cashbox.cash_description,cashbox.deposit_withdrawal_amount,bank_accounts.bank_acc_id,bank_accounts.bank_acc_num');
    	$this->db->from('cashbox');
    	$this->db->join('bank_accounts', 'bank_accounts.bank_acc_id = cashbox.buy_or_sell_details_id', 'left');
    	$this->db->where('cashbox.publication_status', 'activated');
    	$this->db->where('cashbox.cashbox_id', $cashbox_id);
    	return $this->db->get()->row_array();
    }


    /**
 * Update cashbox informations after changes.
 *
 * @param  string  $cashbox_id
 * @param  array[] $data
 * @link   nagadanboi/update_cashbox_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_cashbox_info($cashbox_id,$data)
    {

    	$this->db->where('cashbox_id', $cashbox_id)->update('cashbox', $data);
    	return $this->db->affected_rows();
    }

    /**
 * Collect previous amount info by id.
 * 
 * @link   nagadanboi/update_cashbox_info
 * @param  string $cashbox_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function previous_amount_info_by_id($cashbox_id)
    {

    	$this->db->select('deposit_withdrawal_amount,cash_type,buy_or_sell_details_id');
    	$this->db->from('cashbox');
    	$this->db->where('cashbox_id', $cashbox_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Delete/Hide cashbox's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Nagadanboi/delete_cashbox
 * @param  string $cashbox_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_cashbox_info($cashbox_id,$data)
    {

    	return $this->db->where('cashbox_id', $cashbox_id)->update('cashbox', $data);

    }

    /**
 * Collect all loan informations from database for AJAX datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Loan/all_loan_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_loan_info($filters,$total_count=false)
    {

         // $this->db->select('if(loan_type="loan", "ঋণ", "উত্তোলন") as "0"', FALSE);
    	$this->db->select('loan_type as "0"', false);

    	$this->db->select(
    		'

    		format(loan_amount,2) as "1",
    		format(total_payout_amount,2) as "2",
    		format((loan_amount - total_payout_amount),2) as "3",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "5"
    		', false
    		);

    	$this->db->select('concat(SUBSTRING(loan_description, 1,10),\'<a class="info_loan" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',loan_description,\'" style="font-family: courier" >...</a>\') as "4"', false);
    	$lang_edit = $this->lang->line("model_edit");
    	$lang_adjust = $this->lang->line("model_adjust");
    	$lang_payout = $this->lang->line("model_payout");
    	$lang_delete = $this->lang->line("model_delete");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_loan\" id=\"edit_', CAST(loan_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs blue payout_loan\"  id=\"payout_', CAST(loan_id AS CHAR) ,'\">$lang_payout</button>',if(delete_adjust_status=\"deletable\",concat('<button type=\"button\" class=\"btn btn-xs red delete_loan\" data-toggle=\"modal\" href=\"#responsive_modal_delete\" id=\"delete_', CAST(loan_id AS CHAR) ,'\">$lang_delete</button>'), concat('<button type=\"button\" class=\"btn btn-xs yellow adjust_loan\" data-toggle=\"modal\" href=\"#responsive_modal_adjust\" id=\"adjust_', CAST(loan_id AS CHAR) ,'\">$lang_adjust</button>' ))) as '6'", false);

    	$this->db->from('loan');
    	$this->db->where('publication_status', 'activated');

         // $this->db->order_by("date_created", "desc"); 

    	$this->db->limit(1000);

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "loan_amount like '%".$filters['search']['value']."%' ";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    // if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    //   $this->db->like('loan_amount', $filters['columns']['1']['search']['value'], 'both');
    // }
    // if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    //   $this->db->like('total_payout_amount', $filters['columns']['2']['search']['value'], 'both');
    // }
    // if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    //   $this->db->like('(loan_amount - total_payout_amount)', $filters['columns']['3']['search']['value'], 'both');
    // }
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$this->db->like('loan_description', $filters['columns']['4']['search']['value'], 'both');
    	}

    	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['5']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('loan_amount', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('loan_amount', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('total_payout_amount', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('total_payout_amount', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('(loan_amount - total_payout_amount)', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('(loan_amount - total_payout_amount)', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('loan_description', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('loan_description', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('date_created', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('date_created', 'desc');                
    	}


    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
            // $this->db->last_query();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }


    /**
 * Delete loan informations. It actully change the publication status form "activaed" to "deactivated".
 *
 * @link   Loan/delete_loan
 * @param  string  $loan_id
 * @param  array[] $data
 * @return boolean
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_loan_info($loan_id,$data)
    {

    	return $this->db->where('loan_id', $loan_id)->update('loan', $data);
    }

    /**
 * Collect loan infomations for database
 *
 * @return array[]
 * @param  string $cashbox_id
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function loan_info_by_id($cashbox_id)
    {

    	$this->db->select('net_payable,voucher_no,paid,due,buy_details_id');
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('buy_details_id', $buy_details_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Upadte cashbox amount after loan paid.
 * 
 * @param  array[] $final_data
 * @link   Loan/update_loan_info
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function update_total_cashbox_info($final_data)
    {

    	$this->db->update('final_cashbox_amount', $final_data);
    }

    /**
 * Save data of loan form  in database 
 *
 * @return string
 * @link   Loan/save_loan_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_loan_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['loan_id']=$uuid['UUID()'];
    	$this->db->insert('loan', $data);

    }

    /**
 * Collect loan informations from dataabse.
 *
 * @param  string $loan_id
 * @link   Loan/save_payout_loan 
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_loan_info_by_id($loan_id)
    {

    	$this->db->select('loan_description,loan_amount,total_payout_amount');
    	$this->db->from('loan');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('loan_id', $loan_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect previous loan information by id
 *
 * @param  string $loan_id
 * @link   Loan/delete_loan
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function previous_loan_info_by_id($loan_id)
    {

    	$this->db->select('loan_amount');
    	$this->db->from('loan');
    	$this->db->where('loan_id', $loan_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Update loan information after loan payout.
 *
 * @link   Loan/update_loan_info
 * @param  string  $loan_id
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function update_loan_info($loan_id,$data)
    {

    	$this->db->where('loan_id', $loan_id)->update('loan', $data);
    	return $this->db->affected_rows();
    }

    /**
 * Save loan payout informations to database.
 *
 * @param  array[] $data
 * @link   loan/save_payout_loan
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/


    public function save_payout_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['payout_loan_id']=$uuid['UUID()'];
    	$this->db->insert('payout_loan', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Check payout history of individual loan for confirming their status "deletable" or "adjustable"
 *
 * @link   loan/delete
 * @param  string $loan_id
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function payout_history_info($loan_id)
    {

    	$this->db->where('loan_id', $loan_id);
    	$this->db->from('payout_loan');
    	return $this->db->count_all_results();
    }

    /**
 * Collect the sum of total payout of individual loan by id
 *
 * @link   Loan/adjust_loan
 * @param  string $loan_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function sum_of_payout_loan($loan_id)
    {

    	$this->db->select('SUM(payout_amount)', false);
    	$this->db->from('payout_loan');
    	$this->db->where('loan_id', $loan_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Update net loan taken based on adjusted amount.
 *
 * @link   Loan/adjust_loan
 * @param  string  $loan_id
 * @param  array[] $data
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function update_adjust_info($loan_id,$data)
    {

    	return $this->db->where('loan_id', $loan_id)->update('loan', $data);
    }

    /**
 * Collect the paid of individual buy from databse.
 *
 * @param  string $buy_details_id
 * @link   Buy_list_all/delete_buy
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function paid_amount_by_id($buy_details_id)
    {

    	$this->db->select('paid,landed_cost');
    	$this->db->from('buy_details');
    	$this->db->where('buy_details_id', $buy_details_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect individual purchased item's quantity by id
 *
 * @param  string $buy_details_id
 * @link   Buy_list_all/delete_buy
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function item_quantity_by_buy_details_id($buy_details_id)
    {

    	$this->db->select('item_spec_set_id,quantity');
    	$this->db->from('buy_cart_items');
    	$this->db->where('buy_details_id', $buy_details_id);
    	return $this->db->get()->result_array();
    }

    /**
 * Update inventory of item after delete a buy.
 *
 * @param  array[] $data
 * @link   Buy_list_all/delete_buy
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_inventory($data)
    {    

        $error_list=array();
        $total_updated_item =  0;
        foreach ($data as  $value) {

            $items_id = $value['item_spec_set_id'];
            $this->db->select('quantity');    
            $this->db->from('inventory');
            $this->db->where('item_spec_set_id', $items_id);
            $qry_get = $this->db->get()->row_array();
            $current_item_quantity = $qry_get['quantity'];
            $updated_qty = $current_item_quantity - $value['quantity'];
            if($updated_qty < 0){
               array_push($error_list, 'not enough qty in inventory');
           }
           else{
            $this->db->set('quantity', $updated_qty)->where('item_spec_set_id', $items_id)->update('inventory');
            if($this->db->affected_rows() > 0){
                $total_updated_item += 1;
            }
        }
    }
    if(count($error_list)>0) {
      return $error_list;
  }
  else{
   return $total_updated_item;
}
}

    /**
 * Save advance _buy_basic_info in database.
 *
 * @return array[]
 * @param  array[] $ready_data_basic
 * @link   Buy/save_buy_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_advance_buy_basic_info($ready_data_basic)
    {
    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$ready_data_basic['advance_buy_details_id']=$uuid['UUID()'];
    	$this->db->insert('advance_buy_details', $ready_data_basic);
    	return $uuid['UUID()'];
    }

    /**
 * Save advance_buy_basic_info in advance_buy_casrt_items
 *
 * @return array[]
 * @param  array[] $data
 * @param  string  $advance_buy_details_uuid
 * @link   Buy/save_buy_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_advance_buy_item_info($data,$advance_buy_details_uuid)
    {

    	foreach ($data as $key => $value) {
    		$this->db->select('UUID()', false);    
    		$uuid =$this->db->get()->row_array();
    		unset($data[$key]['item_name']);
    		$data[$key]['advance_buy_details_id'] = $advance_buy_details_uuid;
    		$data[$key]['user_id'] = $this->session->userdata('user_id');    
    		$data[$key]['advance_buy_cart_items_id']=$uuid['UUID()'];

    	}
    	return $this->db->insert_batch('advance_buy_cart_items', $data);
    }

    /**
 * Save advance_buy_basic_info in advance_buy_casrt_items
 *
 * @return array[]
 * @param  string $voucher_no
 * @link   Advance_buy/update_receive_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/


    public function buy_details_info_by_voucher($voucher_no)
    {
    	$this->db->select('paid,due');
    	$this->db->from('buy_details');
    	$this->db->where('voucher_no', $voucher_no);
    	return $this->db->get()->row_array();
    }

    /**
 * Save updated buy_details information in buy_details table
 *
 * @return array[]
 * @param  string  $voucher_no
 * @param  array[] $updated_buy_details_data
 * @link   Advance_buy/update_receive_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_buy_details_info_by_voucher($voucher_no,$updated_buy_details_data)
    {

    	$this->db->where('voucher_no', $voucher_no)->update('buy_details', $updated_buy_details_data);
    	return $this->db->affected_rows();

    }

    /**
 * Save updated buy_details information in advance_buy_details table
 *
 * @return array[]
 * @param  string  $voucher_no
 * @param  array[] $updated_buy_details_data
 * @link   Advance_buy/update_receive_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_advance_buy_details_info_by_voucher($voucher_no,$updated_buy_details_data)
    {
    	$this->db->where('voucher_no', $voucher_no)->update('advance_buy_details', $updated_buy_details_data);
    	return $this->db->affected_rows();
    }

    /**
 * Update buy_cart_items_table after receive quantity
 *
 * @link   Advance_buy/update_receive_info
 * @return boolean
 * @param  array[] $basic_receive_info
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_quantity_after_rec_buy_cart($basic_receive_info)
    {
    	foreach ($basic_receive_info as $key => $value) {

    		unset($value['advance_buy_details_id']);

    		$voucher_no= $value['voucher_no'];
    		$buy_details_id= $this->buy_details_id_by_voucher($voucher_no);

    		$id = array(
    			'items_id' => $value['items_id'],
    			'buy_details_id' => $buy_details_id,
    			);

    		$current_quantity= $this->current_quantity($id);
    		$value['quantity']= $current_quantity + $value['received_quantity'];
            // unset($value['items_id']);
    		unset($value['voucher_no']);
    		unset($value['received_quantity']);

    		$this->db->where('items_id', $value['items_id'])->where('buy_details_id', $id['buy_details_id'])->update('buy_cart_items', $value);


    	}
    }

    /**
 * Collect buy_details_id by voucher no.
 *
 * @param  string $voucher_no
 * @link   Inventory_model/update_quantity_after_receive
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function buy_details_id_by_voucher($voucher_no)
    {
    	$this->db->select('buy_details_id');
    	$this->db->from('buy_details');
    	$this->db->where('voucher_no', $voucher_no);
    	$x = $this->db->get()->row_array();
    	return $x['buy_details_id'];
    }
    /**
 * Collect current quantity of item for buy_cart_items table
 *
 * @link   Inventory_model/update_quantity_after_receive
 * @return array[]
 * @param  string $id
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function current_quantity($id)
    {

    	$this->db->select('quantity');
    	$this->db->from('buy_cart_items');
    	$this->db->where('items_id', $id['items_id']);
    	$this->db->where('buy_details_id', $id['buy_details_id']);
    	$y = $this->db->get()->row_array();
    	return $y['quantity'];

    }

    /**
 * Update buy_cart_items table after receive items
 *
 * @param  array[] $basic_receive_info
 * @link   Advance_buy/update_receive_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_quantity_after_rec_advance_buy_cart($basic_receive_info)
    {
    	foreach ($basic_receive_info as $key => $value) {

    		unset($value['voucher_no']);
    		$id = array(
    			'items_id' => $value['items_id'],
    			'advance_buy_details_id' => $value['advance_buy_details_id'],
    			);

    		$current_remaining_quantity= $this->current_remaining_quantity($id);

    		$value['remaining_quantity']= $current_remaining_quantity - $value['received_quantity'];

    		unset($value['received_quantity']);

    		$this->db->where('items_id', $value['items_id'])->where('advance_buy_details_id', $id['advance_buy_details_id'])->update('advance_buy_cart_items', $value);

    	}
    }

    /**
 * Calculate the current remaining quantity to receive
 *
 * @link   Inventory_model/update_quantity_after_rec_advance_buy_cart
 * @return array[]
 * @param  string $id
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function current_remaining_quantity($id)
    {

    	$this->db->select('remaining_quantity');
    	$this->db->from('advance_buy_cart_items');
    	$this->db->where('items_id', $id['items_id']);
    	$this->db->where('advance_buy_details_id', $id['advance_buy_details_id']);
    	$y = $this->db->get()->row_array();
    	return $y['remaining_quantity'];
    }

    /**
 * 
 *
 * @link Demage_and_lost/save_demage_lost
 * @param array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_demage_lost_info($data)
    {
    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['demage_lost_id']=$uuid['UUID()'];
    	$this->db->insert('demage_lost', $data);
    }

    /**
 * Update Inventory Item's quantity after demage or lost a item
 *
 * @param  string  $item_id
 * @param  array[] $updated_data
 * @link   Demage_and_lost/save_demage_lost
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function update_inventory_quantity($item_id,$updated_data)
    {

    	$this->db->where('item_spec_set_id', $item_id)->update('inventory', $updated_data);
    	return $this->db->affected_rows();
    }


    /**
 * Collect all demage and lost information for AJAX Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Demage_and_lost/all_demage_lost_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/

    public function all_demage_lost_info($filters,$total_count=false)
    {

         // $this->db->select('if(demage_lost.demage_lost_type="demage", "নষ্ট", "হারানো") as "0"', FALSE);
    	$this->db->select('demage_lost.demage_lost_type as "0"', false);
    	$this->db->select(
    		'
    		demage_lost.demage_lost_quantity as "2",
    		item_spec_set.spec_set as "1",
    		DATE_FORMAT(demage_lost.date_created, "%d-%b-%Y") as "4"
    		'
    		);

    	$this->db->select('concat(SUBSTRING(demage_lost.demage_lost_description, 1,10),\'<a class="info_demage_box" tabindex="0" data-trigger="focus" title="Details:" data-container="body" data-toggle="popover"  data-placement="left" data-content="\',demage_lost.demage_lost_description,\'" style="font-family: courier" >...</a>\') as "3"', false);

         // $this->db->select('concat(\'<button class="btn btn-xs green edit_demage_lost" id="edit_\',demage_lost.demage_lost_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_demage_lost " data-toggle="modal" href="#responsive_modal_delete" id="delete_\',demage_lost.demage_lost_id,\'">Delete</button>\') as "5"', FALSE);

    	$lang_edit = $this->lang->line("model_edit");
    	$lang_delete = $this->lang->line("model_delete");
    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_demage_lost\" id=\"edit_', CAST(demage_lost.demage_lost_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_demage_lost\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(demage_lost.demage_lost_id AS CHAR) ,'\">$lang_delete</button>') as '5'", false);

    	$this->db->from('demage_lost');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = demage_lost.item_spec_set_id', 'left');
    	$this->db->where('demage_lost.publication_status', 'activated');

         // $this->db->order_by("demage_lost.date_created", "desc"); 

    	$this->db->limit(1000);

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "demage_lost.demage_lost_type like '%".$filters['search']['value']."%' or ";
    		$where.= "demage_lost.demage_lost_quantity like '%".$filters['search']['value']."%' or ";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('demage_lost.demage_lost_type', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('item_spec_set.spec_set', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('demage_lost.demage_lost_quantity', $filters['columns']['2']['search']['value'], 'both');
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$this->db->like('demage_lost.demage_lost_description', $filters['columns']['3']['search']['value'], 'both');
    	}
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['4']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(demage_lost.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(demage_lost.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(demage_lost.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(demage_lost.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(demage_lost.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(demage_lost.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.demage_lost_type', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.demage_lost_type', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('item_spec_set.spec_set', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('item_spec_set.spec_set', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.demage_lost_quantity', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.demage_lost_quantity', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.demage_lost_description', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.demage_lost_description', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.date_created', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.date_created', 'desc');                
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
            // $this->db->last_query();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}

    }


    /**
 * Collect total paid amount of sell
 *
 * @param  string $sell_details_id
 * @link   Sell_list_all/Delete_sell
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function sell_paid_amount_by_id($sell_details_id)    
    {
    	$this->db->select('paid');
    	$this->db->from('sell_details');
    	$this->db->where('sell_details_id', $sell_details_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect total quantity of individual sell item
 *
 * @param  string $sell_details_id
 * @link   Sell_list_all/Delete_sell
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function item_quantity_by_sell_details_id($sell_details_id)    
    {
    	$this->db->select('item_spec_set_id,quantity');
    	$this->db->from('sell_cart_items');
    	$this->db->where('sell_details_id', $sell_details_id);
    	return $this->db->get()->result_array();
    }

    /**
 * Update inventory of item quantity after delete sell informations.
 *
 * @param  array[] $data
 * @link   Sell_list_all/Delete_sell
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_inventory_after_sell_delete($data)
    {   
        $no_of_rows_affected  = 0;
        foreach ($data as $key => $value) {
          $item_ids = $value['item_spec_set_id'];
          $this->db->select('inventory.item_spec_set_id, inventory.quantity');    
          $this->db->from('inventory');
          $this->db->where_in('inventory.item_spec_set_id', $item_ids);
          $qry_get = $this->db->get();
          $current_item_quantity = $qry_get->result_array();

          $updated_data = array();

          if ($value['item_spec_set_id'] == $current_item_quantity[0]['item_spec_set_id']) {
             $updated_data['item_spec_set_id'] = $value['item_spec_set_id'];
             $updated_data['quantity'] = $current_item_quantity[0]['quantity']+$value['quantity'];  
             $this->db->where('item_spec_set_id', $current_item_quantity[0]['item_spec_set_id'])->update('inventory', $updated_data);
             if($this->db->affected_rows() > 0){
                $no_of_rows_affected++;
            }
        }
    }
    return $no_of_rows_affected;
}


    /**
 * Collect individual demage or lost item's quantity.
 *
 * @param  string $demage_lost_id
 * @link   Demage_and_lost/delete_demage_lost
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function items_info_by_demage_lost_id($demage_lost_id)
    {
    	$this->db->select('item_spec_set_id,demage_lost_quantity,imei_barcode_id');
    	$this->db->from('demage_lost');
    	$this->db->where('demage_lost_id', $demage_lost_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Update inventory of item quantity after delete sell informations.
 *
 * @link   Demage_and_lost/delete_demage_lost
 * @param  string  $items_id
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_inventory_after_demage_lost_delete($items_id,$data)
    {

    	$this->db->where('item_spec_set_id', $items_id)->update('inventory', $data);
    	return $this->db->affected_rows();
    }

    /**
 * This actually change "publication_status" from "activated" to "deactvated".
 *
 * @param  string $demage_lost_id
 * @link   Demage_and_lost/delete_damage_lost
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_demage_lost_info($demage_lost_id)
    {
    	return    $this->db->set('publication_status', 'deactivated')->where('demage_lost_id', $demage_lost_id)->update('demage_lost');
    }


    /**
 * Collect expenditure amount form database.
 *
 * @link   Expenditure/delete_expenditure
 * @param  string $expenditures_id
 * @return void
 * @author <shofik.shoaib@gmail.com>
 **/
    public function get_expenditure_amount_by_id($expenditures_id)
    {

    	$this->db->select('expenditures.expenditures_amount as "expenditures_amount",expenditures.payment_type as "payment_type",bank_statement.bank_acc_id as "bank_acc_id"');
    	$this->db->from('expenditures');
    	$this->db->join('bank_statement', 'bank_statement.buy_sell_and_other_id = expenditures.expenditures_id', 'left');
    	$this->db->where('expenditures.expenditures_id', $expenditures_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect the final quantiy of individual item form inventory table.
 * 
 * @param  array[] $data
 * @link   Sell/quantity_by_item_from_inventory 
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function quantity_info_by_item_from_inventory($data)
    {
    	$this->db->select('quantity', false);
    	$this->db->from('inventory');
    	$this->db->where('item_spec_set_id', $data);
    	$output_arr= $this->db->get()->row_array();
    	return $output_arr['quantity'];
    }

    /**
 * Callect all demage lost informations by  individual id.
 *
 * @param  string $demage_lost_id
 * @link   Demage_and_lost/get_demage_lost_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_demage_lost_info_by_id($demage_lost_id)
    {
    	$this->db->select('demage_lost.item_buying_price,demage_lost.item_spec_set_id,demage_lost.demage_lost_quantity,demage_lost.demage_lost_type,demage_lost.demage_lost_description,item_spec_set.spec_set,imei_barcode.imei_barcode');
    	$this->db->from('demage_lost');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = demage_lost.item_spec_set_id', 'left');
    	$this->db->join('imei_barcode', 'imei_barcode.imei_barcode_id = demage_lost.imei_barcode_id', 'left');
    	$this->db->where('demage_lost.publication_status', 'activated');
    	$this->db->where('demage_lost.demage_lost_id', $demage_lost_id);
    	return $this->db->get()->row_array();

    }


    /**
 * Collect previous expenditure amount by expenditure_id
 *
 * @link   Expenditure/update_expenditure_info
 * @param  string $expenditures_id
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function previous_expenditure_amount_info($expenditures_id)
    {
    	$this->db->select('expenditures_amount', false);
    	$this->db->from('expenditures');
    	$this->db->where('expenditures_id', $expenditures_id);
    	return $this->db->get()->row_array();
    }


    /**
 * Save Sales Representative Informations to Database
 *
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_sales_rep_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['sales_rep_id']=$uuid['UUID()'];
    	$this->db->insert('sales_representative', $data);
    	return $uuid['UUID()'];


         // $this->db->insert_string('sales_representative', $data);
    }

    /**
 * Collect all sales representatives informations form database
 *
 *   @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   Sales_rep/all_sales_rep_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_sales_rep_info($filters,$total_count=false)
    {

    	$this->db->select(
    		'
    		sales_rep_name as "0",
    		sales_rep_phone_1 as "1",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "2"

    		'
    		);

    	$lang_edit = $this->lang->line("model_edit");
    	$lang_delete = $this->lang->line("model_delete");

    	/*another way to display multiple language*/

         // $query_string = "concat('<button type=\"button\" class=\"btn btn-sm green edit_expenditure_type\" id=\"edit_', CAST(expenditure_types_id AS CHAR) ,'\">$lang</button>') as '3'";
         // $this->db->select($query_string, FALSE);

    	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_sales_rep\" id=\"edit_', CAST(sales_rep_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_sales_rep\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(sales_rep_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);

    	$this->db->from('sales_representative');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sales_rep_name like '%".$filters['search']['value']."%' or ";
    		$where.= "sales_rep_phone_1 like '%".$filters['search']['value']."%'";

    		$where.= ")";
    		$this->db->where($where, null, false);
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('sales_rep_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('sales_rep_phone_1', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sales_rep_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sales_rep_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sales_rep_phone_1', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sales_rep_phone_1', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('date_created', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }

    /**
 * Delete sales rep info. It actually change the "publication_status" from "activated" to "deactivated".
 *
 * @param  string $sales_rep_id
 * @return void
 * @author 
 **/
    public function delete_sales_rep_info($sales_rep_id)
    {
    	return $this->db->set('publication_status', 'deactivated')->where('sales_rep_id', $sales_rep_id)->update('sales_representative');
    }

    /**
 * Collect all informations of individual sales representatives from database
 * 
 * @param  string $sales_rep_id
 *@link   Sales_rep/get_sales_rep_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function sales_rep_info_by_id($sales_rep_id)
    {

    	$this->db->select('sales_rep_name,sales_rep_email,sales_rep_phone_1,sales_rep_phone_2,sales_rep_present_address,sales_rep_permanent_address,sales_rep_national_id,sales_rep_image');
    	$this->db->from('sales_representative');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('sales_rep_id', $sales_rep_id);
    	return $this->db->get()->row_array();

    }


    /**
 * Update sales representatives information to database
 *
 * @link   Sales_rep/update_sales_rep
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function update_sales_rep_info($sales_rep_id,$data)
    {

    	return $this->db->where('sales_rep_id', $sales_rep_id)->update('sales_representative', $data);
    }

    /**
 * Update demage_lost table after edit
 * 
 * @param  string $demage_lost_id
 * @link   Demage_and_lost/update_demage_lost_info
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function previous_demage_lost_quanity_info($demage_lost_id)
    {

    	$this->db->select('demage_lost_quantity', false);
    	$this->db->from('demage_lost');
    	$this->db->where('demage_lost_id', $demage_lost_id);
    	return $this->db->get()->row_array();
    }

    /**
 * Update demage lost informations after update
 *
 * @link   Demasge_and_lost/update_demage_lost_info
 * @param  string  $demage_lost_id
 * @param  array[] $data
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_demage_lost_info($demage_lost_id,$data)
    {
    	return $this->db->where('demage_lost_id', $demage_lost_id)->update('demage_lost', $data);
    }

    /**
 * save deposit amount while database is blank
 *
 * Nagadanboi/save_deposit_withdrawal_amount_info
 *
 * @param  array[] $amount
 * @return void
 * @author shoaib <shofik.shaoib@gmail.com>
 **/
    public function save_deposit_amount_info($amount)
    {
    	$this->db->insert('final_cashbox_amount', $amount);
    }

    // function get_field()
    // {
    // 	$result = $this->db->list_fields('vendors');
    // 	return $result;
    // }



    /**
 * Check is provided voucher no already exist in database
 *
 * @param  string $buy_voucher
 *@link   Buy/save_buy_info
 * @return array[]
 * @author 
 **/
    public function check_voucher_is_available($buy_voucher)
    {
    	$this->db->select('voucher_no', false);
    	$this->db->from('buy_details');
    	$this->db->where('voucher_no', $buy_voucher);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    public function check_adv_voucher_availablity($buy_voucher)
    {
    	$this->db->select('voucher_no', false);
    	$this->db->from('adv_buy_details');
    	$this->db->where('voucher_no', $buy_voucher);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    /**
 * Collect Individual Item's information form Databse for Item edit of buy and sell
 *
 * @param  string $id
 * @link   buy/get_item_info 
 * @return array[]
 * @author  
 **/

    public function select_item_info($id)
    {
    	$this->db->select('item_spec_set_id,spec_set');
    	$this->db->from('item_spec_set');
    	$this->db->where('item_spec_set_id', $id);
    	return $this->db->get()->row_array();
    }

    /**
 * Collect Item Informations and quantity for sell edit.
 *
 *@param  string $id
  * @link   buy/get_item_info _for_sell
 * @return array[]
 * @author Shofik Shoaib <shofik.shoaib@gmail.com> 
 **/
    public function select_item_info_for_sell($id)
    {

    	$this->db->select('items.item_image,items.items_name,inventory.quantity', false);
    	$this->db->from('items');
    	$this->db->join('inventory', 'inventory.items_id = items.items_id', 'left');
    	$this->db->where('items.items_id', $id);
    	return $this->db->get()->row_array();
    }


    /**
 * Save Payment Method's Information in Database
 *
 * @link   Sell/save_sell_info
 *@param  array[] $payment_method_details
 * @return void
 * @author Shofik Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_cash_or_card_payment_info($payment_method_details)
    {
    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();
    	$payment_method_details['sell_payment_details_id'] = $uuid['UUID()'];
    	$this->db->insert('sell_payment_details', $payment_method_details);
    }

    /**
 * Save "Both Cash and Card" Payment Information in Database
 *
 *@link   Sell/save_sell_info
 *@param  arry[] $payment_method_details
 * @return void
 * @author Shofik Shoaib <shofik.shoaib@gmail.com>
 **/
    public function save_both_cash_card_payment_info($payment_method_details)
    {
    	foreach ($payment_method_details as $key => $value) {
    		$this->db->select('UUID()', false);    
    		$uuid =$this->db->get()->row_array();
    		$payment_method_details[$key]['sell_payment_details_id']=$uuid['UUID()'];
    	}
    	return $this->db->insert_batch('sell_payment_details', $payment_method_details);

    }

    /**
 * Check that the selling item quantity is available to inventory or not
 *
 *@link   Sell/save_sell_info
 *@param  array[] $data
 * @return void
 * @author Shofik Shoaib <shofik.shoaib@gmail.com>
 **/

   //  public function check_sell($data)
   //  {
   //   $this->db->select('quantity', false);
   //   $this->db->from('inventory');
   //   $this->db->where('item_spec_set_id', $data['item_spec_set_id']);
   //   return $this->db->get()->row_array();
   // }
    public function check_sell($data)
    {
    	$this->db->select('inventory.quantity,item_spec_set.spec_set', false);
    	$this->db->from('inventory');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
    	$this->db->where('inventory.item_spec_set_id', $data['item_spec_set_id']);
    	return $this->db->get()->row_array();
    }

    /**
 * Claculate todays total sell amount
 *
 *@link   Dashboard/curr_cash_amt_todays_buy_and_expense
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function current_day_total_sell()
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
    	$this->db->select('SUM(net_payable) AS paid', false);
    	$this->db->from('sell_details');
    	$this->db->where('publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('vat_amount >', 0);
    	}
    	$this->db->group_start();
    	$this->db->like('date_created', $x);
    	$this->db->not_like('custom_voucher_no', 'T-', 'after');
    	$this->db->group_end();
    	$qry_get= $this->db->get()->row_array();
    	return $qry_get['paid'];
    }

    /**
 * Claculate todays total buy amount
 *
 *@link   Dashboard/curr_cash_amt_todays_buy_and_expense
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function current_day_total_buy()
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
    	$this->db->select('SUM(net_payable) AS paid', false);
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	$this->db->group_start();
    	$this->db->like('date_created', $x);
    	$this->db->not_like('voucher_no', 'T-', 'after');
    	$this->db->group_end();
    	$qry_get=  $this->db->get()->row_array();
    	return $qry_get['paid'];
    }

    /**
 * Calculate total sell due amount
 *
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function total_due_amount()
    {
         // $x= date("Y-m-d");  
    	$this->db->select('SUM(due) AS due', false);
    	$this->db->from('sell_details');

         // $this->db->like('date_created', $x);
    	return $this->db->get()->row_array();
    }

    /**
 * After buy and sell update item info in inventory
 *
 *@param  string  $item_id
 *@param  array[] $status_data 
 *@link   Sell/save_sell_info
 *@link   Buy/save_buy_info
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_inventory_status($item_id,$status_data)
    {

    	return    $this->db->where('item_spec_set_id', $item_id)->update('inventory', $status_data);

    }


    /**
 * Collect today's all sell data for AJAZ Datatable in Dashboard
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @link   dashboard/all_todays_sell_list_info_for_datatable
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/

    public function todays_sell_list_all_info($filters,$total_count=false)
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
         // $starting_date = date('Y-m-d', strtotime('today - 1 days')); 
    	$this->db->select(
    		'
    		sell_local_voucher_no as "0",
    		format(grand_total,2) as "1",
    		format(discount,2) as "2",
    		format(paid,2) as "3",
    		format(due,2) as "4",
    		format(return_change,2) as "5",
    		', false
    		);
    	$this->db->from('sell_details');

    	$this->db->where('publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('vat_amount >', 0);
    	}

    	$this->db->group_start();
    	$this->db->like('date_created', $x);
    	$this->db->not_like('custom_voucher_no', 'T-', 'after');
    	$this->db->group_end();

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sell_local_voucher_no like '%".$filters['search']['value']."%'";

             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}


    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_local_voucher_no', 'asc');
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_local_voucher_no', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('grand_total', 'asc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('grand_total', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('discount', 'asc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('discount', 'desc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}

    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}

    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}

    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}

    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Collect all buy information from database 
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Buy_list_all/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function todays_buy_list_all_info($filters,$total_count=false)
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
    	$this->db->select(
    		'
    		voucher_no as "0",
    		format(grand_total,2) as "1",
    		format(discount,2) as "2",
    		format(paid,2) as "3"
    		', false
    		);
         // $this->db->select('format(grand_total) as 1', FALSE);
    	$this->db->from('buy_details');
    	$this->db->where('publication_status', 'activated');
    	$this->db->group_start();
    	$this->db->not_like('voucher_no', 'T-', 'after');
    	$this->db->like('date_created', $x);
    	$this->db->group_end();
         // $this->db->limit(1);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "voucher_no like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);

    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('grand_total', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('grand_total', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('discount', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('discount', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Collect last 30 days individual sale quantiy from databae 
 *
 *@link   Sell/save_sell_info
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function individual_sell_qty($item_id)
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$starting_date = date('Y-m-d', strtotime('today - 30 days'));  
    	$this->db->select('quantity', false);
    	$this->db->from('sell_cart_items');
    	$this->db->where('item_spec_set_id', $item_id);
    	$this->db->where('date_created >=', $starting_date);
    	return $this->db->get()->result_array();
    }
    /**
 * Display todays total sell amount by staff for datatable.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Dashboard/todays_total_sell_by_staff
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function total_sell_amount_by_staff($filters,$total_count=false)
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
    	$this->db->select(
    		'
    		users.username as "0",
    		format(sum(sell_details.paid),2) as "1",
    		', false
    		);
    	$this->db->from('sell_details');
    	$this->db->join('users', 'users.user_id = sell_details.user_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
    	$this->db->where('users.user_role', 'staff');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('sell_details.vat_amount >', 0);
    	}
    	$this->db->group_start();
    	$this->db->group_by('sell_details.user_id');
    	$this->db->like('sell_details.date_created', $x);
    	$this->db->not_like('sell_details.custom_voucher_no', 'T-', 'after');
    	$this->db->group_end();
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "users.username like '%".$filters['search']['value']."%' or ";
    		$where.= "sell_details.paid like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);

    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('users.username', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('users.username', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.paid', 'desc');
    	}
         // if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc'){
         // 	$this->db->order_by('sell_details.date_created', 'asc');
         // }
         // if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc'){
         // 	$this->db->order_by('sell_details.date_created', 'desc');
         // }
         // $sql ="SELECT u.username AS "0",SUM(`paid`) as "1" FROM `sell_details` s RIGHT JOIN `users` u ON s.`user_id`= u.`user_id` WHERE u.`user_role`='staff' AND DATE_FORMAT(s.`date_created`,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') GROUP BY s.`user_id`";
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display last todays individual staff sell amount  report.
 *
 * @param  string $user_id
 * @return array[]
 * @link   Dashboard/curr_cash_amt_todays_buy_and_expense
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function total_sell_by_staff($user_id)
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x= date("Y-m-d");  
    	$this->db->select('sum(net_payable) as paid');
    	$this->db->from('sell_details');
    	$this->db->where('user_id', $user_id);
    	$this->db->where('publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('vat_amount >', 0);
    	}
    	$this->db->like('date_created', $x);
    	$qry_get =  $this->db->get()->row_array();
    	return $qry_get['paid'];
    }
    /**
 * Display todays total expense.
 *
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function current_day_total_expense()
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x = date("Y-m-d");  
    	$this->db->select('sum(expenditures_amount) as expenditures_amount');
    	$this->db->from('expenditures');
    	$this->db->where('publication_status', 'activated');
    	$this->db->like('date_created', $x);
    	$qry_get = $this->db->get()->row_array();
    	return $qry_get['expenditures_amount'];
    }

    public function current_day_total_transfer()
    {
    	date_default_timezone_set("Asia/Dhaka");
    	$x = date("Y-m-d");  
    	$this->db->select('sum(total_amount) as transfer_amount');
    	$this->db->from('transfer_details');
    	$this->db->where('publication_status', 'activated');
      // $this->db->like('date_created', $x);
    	$qry_get = $this->db->get()->row_array();
    	return $qry_get['transfer_amount'];
    }

    public function current_day_total_receive()
    {
    	$this->db->select('total_amount');
    	$this->db->from('transfer_receives');
    	$this->db->order_by('id', 'desc');
    	$this->db->limit('1');
    	$qry_get = $this->db->get()->row_array();
    	return $qry_get['total_amount'];
    }

    /**
 * Display current cashbox amount.
 *
 * @link   Dashboard/curr_cash_amt_todays_buy_and_expense
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function final_cashbox_amount_info_for_dashboard()
    {
    	$this->db->select('total_cashbox_amount');
    	$this->db->from('final_cashbox_amount');
    	$qry_get = $this->db->get()->row_array();
    	return $qry_get['total_cashbox_amount'];
    }
    /**
 * Collect current vat percentage from vat table
 *
 * @link   sell/get_current_vat_percentage
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function select_current_vat_percentage_info()
    {
    	$this->db->select('vat_percentage');
    	$this->db->from('vat');
    	$this->db->where('selection_status', 'selected');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }
    /**
 * Display last 30 days all sell report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/monthly_sell_info_for_datatable
 * @author Musabbir <musabbir.mamun@gmail.com>
 **/
    public function monthly_sell_info_for_datatable($filters,$total_count=false)
    {
         // $starting_date = date('Y-m-d', strtotime('today - 30 days')); 
    	$this->db->select(
    		'
    		sell_details.sell_local_voucher_no as "0",
    		format(sell_details.grand_total,2) as "1",
    		format(sell_details.discount,2) as "2",
    		format(sell_details.paid,2) as "3",
    		format(sell_details.due,2) as "4",
    		sell_details.payment_type as "5",
    		customers.customers_name as "6",
    		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "7",
    		', false
    		);
    	$this->db->select('concat(\'<button class="btn btn-sm green sold_items" id="edit_\',sell_details.sell_local_voucher_no,\'">Details</button>\') as "8"', false);
    	$this->db->from('sell_details');
    	$this->db->join('customers', 'sell_details.customers_id = customers.customers_id', 'left');
    	$this->db->not_like('sell_details.custom_voucher_no', 'T-', 'after');
    	$this->db->where('sell_details.publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('sell_details.vat_amount >', 0);
    	}
         // $this->db->where('sell_details.date_created >', $starting_date);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sell_local_voucher_no like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}
    	if($filters['columns']['7']['search']['value']!="" && $filters['columns']['7']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['7']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(sell_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(sell_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <=', DATE($range_date[1]));
    		}
    	}
    	/* For greater then and less than search for "sell list"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('sell_details.grand_total <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('sell_details.grand_total >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('sell_details.grand_total <', $range[1]);
    			$this->db->where('sell_details.grand_total >', $range[0]);
    		}
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['0']['search']['value'], 'both');
    	}
         // if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
         // 	$this->db->like('sell_details.grand_total',$filters['columns']['1']['search']['value'], 'both');
         // }
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('sell_details.discount', $filters['columns']['2']['search']['value'], 'both');
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$this->db->like('sell_details.paid', $filters['columns']['3']['search']['value'], 'both');
    	}
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$this->db->like('sell_details.due', $filters['columns']['4']['search']['value'], 'both');
    	}
    	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null) {
    		$this->db->like('sell_details.payment_type', $filters['columns']['5']['search']['value'], 'both');
    	}
    	if($filters['columns']['6']['search']['value']!="" && $filters['columns']['6']['search']['value']!=null) {
    		$this->db->like('customers.customers_name', $filters['columns']['6']['search']['value'], 'both');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_local_voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_local_voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('grand_total', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('grand_total', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('discount', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('discount', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('due', 'asc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('due', 'desc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('payment_type', 'asc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('payment_type', 'desc');
    	}
    	if($filters['order']['0']['column'] == '6' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '6' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '7' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '7' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.date_created', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display last 30 days all buy report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/monthly_buy_info_for_datatable
 * @author Musabbir <musabbir.mamun@gmail.com>
 **/
    public function monthly_buy_info_for_datatable($filters,$total_count=false)
    {
         //$x= date("Y");  
         // $starting_date = date('Y-m-d', strtotime('today - 30 days')); 
    	$this->db->select(
    		'
    		buy_details.voucher_no as "0",
    		format(buy_details.grand_total,2) as "1",
    		format(buy_details.discount,2) as "2",
    		format(buy_details.paid,2) as "3",
    		format(buy_details.due,2) as "4",
    		vendors.vendors_name as "5",
    		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "6",
    		', false
    		);
    	$this->db->select('concat(\'<button class="btn btn-sm green buy_items" id="edit_\',buy_details.voucher_no,\'">Details</button>\') as "7"', false);

    	$this->db->from('buy_details');
    	$this->db->join('vendors', 'buy_details.vendors_id = vendors.vendors_id', 'left');
    	$this->db->not_like('buy_details.voucher_no', 'T-', 'after');
    	$this->db->where('buy_details.publication_status', 'activated');
         // $this->db->where('buy_details.date_created >', $starting_date);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "voucher_no like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}
    	if($filters['columns']['6']['search']['value']!="" && $filters['columns']['6']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['6']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(buy_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(buy_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <=', DATE($range_date[1]));
    		}
    	}
    	/* For greater then and less than search for "sell list"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('buy_details.grand_total <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('buy_details.grand_total >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('buy_details.grand_total <', $range[1]);
    			$this->db->where('buy_details.grand_total >', $range[0]);
    		}
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('buy_details.voucher_no', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$this->db->like('buy_details.discount', $filters['columns']['2']['search']['value'], 'both');
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$this->db->like('buy_details.paid', $filters['columns']['3']['search']['value'], 'both');
    	}
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$this->db->like('buy_details.due', $filters['columns']['4']['search']['value'], 'both');
    	}
    	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null) {
    		$this->db->like('vendors.vendors_name', $filters['columns']['5']['search']['value'], 'both');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('grand_total', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('grand_total', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('discount', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('discount', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('paid', 'desc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('due', 'asc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('due', 'desc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('vendors_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('vendors_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '6' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '6' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.date_created', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }

    /**
 * Display last 30 days top sales items report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_top_sell_items_report
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_top_sell_items_report_info($filters,$total_count=false)
    {
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select(
    		'
    		item_spec_set.spec_set as "0",
    		sum(sell_cart_items.quantity) as "1",

    		'
    		);
    	$this->db->from('sell_cart_items');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
    	$this->db->where('sell_cart_items.publication_status', 'activated');
         // $this->db->where('sell_cart_items.date_created >', $starting_date);
    	$this->db->group_by('sell_cart_items.item_spec_set_id');
    	/* For top right search*/
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);

    	}
    	/* For greater then and less than search for "top sell item"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('sum(`sell_cart_items`.`quantity`) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('sum(`sell_cart_items`.`quantity`) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('sum(`sell_cart_items`.`quantity`) <', $range[1]);
    			$this->db->having('sum(`sell_cart_items`.`quantity`) >', $range[0]);
    		}
    	}
    	/* For customer column based bottom search*/
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('item_spec_set.spec_set', $filters['columns']['0']['search']['value'], 'both');
    	}
    	/* For asc and desc orders*/
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('item_spec_set.spec_set', 'desc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('item_spec_set.spec_set', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sum(sell_cart_items.quantity)', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sum(sell_cart_items.quantity)', 'asc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display last 30 days top sales representative report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/items_list_with_quantity
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_top_sales_rep_report_info($filters,$total_count=false)
    {
    	$empty = $this->lang->line("model_empty");
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select('IFNULL(CAST(sales_representative.sales_rep_name AS CHAR) , "'.$empty.'" ) as "0"', false);
    	$this->db->select('count(sell_details.sell_details_id) as "1",');
    	$this->db->from('sell_details');
    	$this->db->join('sales_representative', 'sales_representative.sales_rep_id = sell_details.sales_rep_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
         // $this->db->where('sell_details.date_created >', $starting_date);
    	$this->db->group_by('sell_details.sales_rep_id');
    	/* For top right search*/
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sales_representative.sales_rep_name like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);

    	}
    	/* For greater then and less than search for "total sell by sales rep"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('count(sell_details.sell_details_id) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('count(sell_details.sell_details_id) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('count(sell_details.sell_details_id) <', $range[1]);
    			$this->db->having('count(sell_details.sell_details_id) >', $range[0]);
    		}
    	}
    	/* For customer column based bottom search*/
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('sales_representative.sales_rep_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	/* For asc and desc orders*/
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sales_representative.sales_rep_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sales_representative.sales_rep_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('count(sell_details.sell_details_id)', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('count(sell_details.sell_details_id)', 'asc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display last 30 days top customers report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_top_customers_report
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_customers_report_info($filters,$total_count=false)
    {
    	$empty = $this->lang->line("model_empty");
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$empty.'" ) as "0"', false);
         // $this->db->select('CAST(sum(sell_details.paid) AS DECIMAL (18,2)) as "1",',false);
    	$this->db->select('format(sum(sell_details.paid),2) as "1",', false);
    	$this->db->select(
    		'
    		count(sell_details.customers_id) as "2",
    		'
    		);

    	$this->db->from('sell_details');
    	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
         // $this->db->where('sell_details.date_created >', $starting_date);
    	$this->db->group_by('sell_details.customers_id');
    	/* For top right search*/
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "customers.customers_name like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);

    	}
    	/* For greater then and less than search for "total amount of perchase"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('sum(`sell_details`.`paid`) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('sum(`sell_details`.`paid`) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('sum(`sell_details`.`paid`) <', $range[1]);
    			$this->db->having('sum(`sell_details`.`paid`) >', $range[0]);
    		}
    	}
    	/* For greater then and less than search for "number of perchase"*/
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('count(sell_details.customers_id) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('count(sell_details.customers_id) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('count(sell_details.customers_id) <', $range[1]);
    			$this->db->having('count(sell_details.customers_id) >', $range[0]);
    		}
    	}
    	/* For customise column based bottom search*/
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	/* For asc and desc orders*/
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers.customers_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers.customers_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sum(sell_details.paid)', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sum(sell_details.paid)', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('count(sell_details.customers_id)', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('count(sell_details.customers_id)', 'asc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
            // echo $this->db->last_query();
            // exit();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display last 30 days expenditure report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_expenses_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_expenses_report_info($filters,$total_count=false)
    {
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select(
    		'
    		expenditure_types.expenditure_types_name as "0",
    		format(expenditures.expenditures_amount,2) as "1",
    		DATE_FORMAT(expenditures.date_created, "%d-%b-%Y") as "2",
    		expenditures.expenditures_comments as "3"
    		', false
    		);
    	$this->db->from('expenditures');
    	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
    	$this->db->where('expenditures.publication_status', 'activated');
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "expenditure_types.expenditure_types_name like '%".$filters['search']['value']."%' or ";
    		$where.= "expenditures.expenditures_comments like '%".$filters['search']['value']."%' or ";
    		$where.= "expenditures.expenditures_amount like '%".$filters['search']['value']."%'";

    		$where.= ")";

    		$this->db->where($where, null, false);
    	}

    	/* For greater then and less than search for "amount of expense"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('expenditures.expenditures_amount <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('expenditures.expenditures_amount >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('expenditures.expenditures_amount <', $range[1]);
    			$this->db->where('expenditures.expenditures_amount >', $range[0]);
    		}
    	}

    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('expenditure_types.expenditure_types_name', $filters['columns']['0']['search']['value'], 'both');
    	}

         // if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
         // 	$this->db->like('expenditures.expenditures_amount',$filters['columns']['1']['search']['value'], 'both');
         // }

    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$this->db->like('expenditures.expenditures_comments', $filters['columns']['3']['search']['value'], 'both');
    	}



    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(expenditures.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(expenditures.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(expenditures.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(expenditures.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(expenditures.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(expenditures.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditure_types.expenditure_types_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditure_types.expenditure_types_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.expenditures_amount', 'asc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.expenditures_amount', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.date_created', 'asc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.date_created', 'desc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('expenditures.expenditures_comments', 'asc');
    	}

    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('expenditures.expenditures_comments', 'desc');
    	}

    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }

    /**
 * Display last 30 days demage and lost report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_demage_lost_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_demage_lost_report_info($filters,$total_count=false)
    {
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select(
    		'
    		item_spec_set.spec_set as "0",
    		demage_lost.demage_lost_quantity as "1",
    		DATE_FORMAT(demage_lost.date_created, "%d-%b-%Y") as "2",
    		'
    		);
    	$this->db->from('demage_lost');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = demage_lost.item_spec_set_id', 'left');
    	$this->db->where('demage_lost.publication_status', 'activated');
         // $this->db->where('demage_lost.date_created >', $starting_date);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}
    	/* For greater then and less than search for "quantiy of demage and lost"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('demage_lost.demage_lost_quantity <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('demage_lost.demage_lost_quantity >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('demage_lost.demage_lost_quantity <', $range[1]);
    			$this->db->where('demage_lost.demage_lost_quantity >', $range[0]);
    		}
    	}

    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->or_where('demage_lost.demage_lost_quantity', $filters['columns']['1']['search']['value'], 'both');
    	}

         // if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null){
         // 	$this->db->like('item_spec_set.spec_set',$filters['columns']['0']['search']['value'], 'both');
         // }


    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(demage_lost.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(demage_lost.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(demage_lost.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(demage_lost.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(demage_lost.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(demage_lost.date_created) <=', DATE($range_date[1]));
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('item_spec_set.spec_set', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('item_spec_set.spec_set', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.demage_lost_quantity', 'asc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.demage_lost_quantity', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('demage_lost.date_created', 'asc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('demage_lost.date_created', 'desc');
    	}


    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
            // echo	$this->db->last_query();
            // exit();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }





    /**
 * Display last 30 days all vendor report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/monthly_vendor_info
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function monthly_vendor_info_for_datatable($filters,$total_count=false)
    {
         // echo "<pre>";
         // print_r($filters);
         // exit();
    	$empty = $this->lang->line("model_empty");
         // $starting_date = date('Y-m-d', strtotime('today - 30 days'));
    	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$empty.'" ) as "0"', false);

    	$this->db->select(
    		'
    		count(buy_details.vendors_id) as "2",
    		'
    		);
         // $this->db->select('CAST(sum(buy_details.paid) AS DECIMAL (18,2)) as "1",',false);
    	$this->db->select('format(sum(buy_details.paid),2) as "1",', false);
    	$this->db->from('buy_details');
    	$this->db->join('vendors', 'buy_details.vendors_id = vendors.vendors_id', 'left');
    	$this->db->where('buy_details.publication_status', 'activated');
         // $this->db->where('buy_details.date_created >', $starting_date);
    	$this->db->group_by('buy_details.vendors_id');


    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "vendors.vendors_name like '%".$filters['search']['value']."%'";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}


    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('vendors.vendors_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	/* For greater then and less than search for "total amount of perchase"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('sum(`buy_details`.`paid`) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('sum(`buy_details`.`paid`) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('sum(`buy_details`.`paid`) <', $range[1]);
    			$this->db->having('sum(`buy_details`.`paid`) >', $range[0]);
    		}
    	}
    	/* For greater then and less than search for "number of perchase"*/
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->having('count(buy_details.vendors_id) <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->having('count(buy_details.vendors_id) >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->having('count(buy_details.vendors_id) <', $range[1]);
    			$this->db->having('count(buy_details.vendors_id) >', $range[0]);
    		}
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('vendors.vendors_name', 'asc');
    	}

    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('vendors.vendors_name', 'desc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sum(buy_details.paid)', 'asc');
    	}

    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sum(buy_details.paid)', 'desc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('count(buy_details.vendors_id)', 'asc');
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('count(buy_details.vendors_id)', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display all loan report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Central_reports/all_loan_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_loan_report_info($filters,$total_count=false)
    {
    	$this->db->select('loan_type as "0"', false);
    	$this->db->select(
    		'
    		format(loan_amount,2) as "1",
    		format(total_payout_amount,2) as "2",
    		DATE_FORMAT(date_created, "%d-%b-%Y") as "3"
    		', false
    		);
    	$this->db->from('loan');
    	$this->db->where('publication_status', 'activated');
         // $this->db->order_by("date_created", "desc"); 
         // $this->db->limit(1000);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "loan_amount like '%".$filters['search']['value']."%' ";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}
    	/* For greater then and less than search for "amount of loan taken"*/
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['1']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('loan_amount <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('loan_amount >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('loan_amount <', $range[1]);
    			$this->db->where('loan_amount >', $range[0]);
    		}
    	}
    	/* For greater then and less than search for "amount of loan payment made"*/
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('total_payout_amount <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('total_payout_amount >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('total_payout_amount <', $range[1]);
    			$this->db->where('total_payout_amount >', $range[0]);
    		}
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('loan_type', $filters['columns']['0']['search']['value'], 'both');
    	}
         // if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null){
         // 	$this->db->like('loan_amount',$filters['columns']['1']['search']['value'], 'both');
         // }
         // if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null){
         // 	$this->db->like('total_payout_amount',$filters['columns']['2']['search']['value'], 'both');
         // }
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(date_created) <=', DATE($range_date[1]));
    		}
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('loan_amount', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('loan_amount', 'desc');                
    	}

    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('total_payout_amount', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('total_payout_amount', 'desc');                
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('date_created', 'asc');        
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('date_created', 'desc');                
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
            // echo $this->db->last_query();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display all buy due report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_buy_due_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_buy_due_report_info($filters,$total_count=false)
    {
    	$no_vendor = $this->lang->line("model_empty");
    	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "0"', false);
    	$this->db->select(
    		'
    		buy_details.voucher_no as "1",
    		format(buy_details.net_payable,2) as "2",
    		format(buy_details.paid,2) as "3",
    		format(buy_details.due,2) as "4",
    		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "5"
    		', false
    		);
    	$this->db->from('buy_details');
    	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
    	$this->db->not_like('buy_details.voucher_no', 'T-', 'after');
    	$this->db->where('buy_details.publication_status', 'activated');
    	$this->db->where('buy_details.due >', 0);
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "buy_details.voucher_no like '%".$filters['search']['value']."%' ";
    		$where.= ")";

    		$this->db->where($where, null, false);
    	}
    	/* For greater then and less than search*/
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('buy_details.net_payable <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('buy_details.net_payable >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('buy_details.net_payable <', $range[1]);
    			$this->db->where('buy_details.net_payable >', $range[0]);
    		}
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('buy_details.paid <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('buy_details.paid >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('buy_details.paid <', $range[1]);
    			$this->db->where('buy_details.paid >', $range[0]);
    		}
    	}
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['4']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('buy_details.due <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('buy_details.due >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('buy_details.due <', $range[1]);
    			$this->db->where('buy_details.due >', $range[0]);
    		}
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('vendors.vendors_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('buy_details.voucher_no', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['5']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(buy_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(buy_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(buy_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(buy_details.date_created) <=', DATE($range_date[1]));
    		}
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('vendors.vendors_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('vendors.vendors_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.net_payable', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.net_payable', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.paid', 'desc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.due', 'asc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.due', 'desc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('buy_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('buy_details.date_created', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Display all sell due report.
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 * @return array[]
 * @link   Reports/all_sell_due_list
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function all_sell_due_report_info($filters,$total_count=false)
    {
    	$no_customer = $this->lang->line("no_customer");
    	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', false);
    	$this->db->select(
    		'
    		sell_details.sell_local_voucher_no as "1",
    		format(sell_details.net_payable,2) as "2",
    		format(sell_details.paid,2) as "3",
    		format(sell_details.due,2) as "4",
    		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "5"
    		', false
    		);
    	$this->db->from('sell_details');
    	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
    	$this->db->not_like('sell_details.custom_voucher_no', 'T-', 'after');
    	$this->db->where('sell_details.publication_status', 'activated');
    	$this->db->where('sell_details.due >', 0);
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('sell_details.vat_amount >', 0);
    	}
    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "sell_details.sell_local_voucher_no like '%".$filters['search']['value']."%' or ";
    		$where.= "customers.customers_name like '%".$filters['search']['value']."%'";
    		$where.= ")";
    		$this->db->where($where, null, false);
    	}
    	/* For greater then and less than search*/
    	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['2']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('sell_details.net_payable <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('sell_details.net_payable >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('sell_details.net_payable <', $range[1]);
    			$this->db->where('sell_details.net_payable >', $range[0]);
    		}
    	}
    	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['3']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('sell_details.paid <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('sell_details.paid >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('sell_details.paid <', $range[1]);
    			$this->db->where('sell_details.paid >', $range[0]);
    		}
    	}
    	if($filters['columns']['4']['search']['value']!="" && $filters['columns']['4']['search']['value']!=null) {
    		$range = explode('_', $filters['columns']['4']['search']['value']);
    		if ($range[1]>0 && $range[0]==0) {
    			$this->db->where('sell_details.due <', $range[1]);
    		}
    		if ($range[1]==0 && $range[0]>0) {
    			$this->db->where('sell_details.due >', $range[0]);
    		}
    		if ($range[1]>0 && $range[0]>0) {
    			$this->db->where('sell_details.due <', $range[1]);
    			$this->db->where('sell_details.due >', $range[0]);
    		}
    	}
    	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
    		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
    	}
    	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
    		$this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['1']['search']['value'], 'both');
    	}
    	if($filters['columns']['5']['search']['value']!="" && $filters['columns']['5']['search']['value']!=null) {
    		$range_date = explode('_', $filters['columns']['5']['search']['value']);
    		if ($range_date[1]!=0 && $range_date[0]==0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE("1992-01-01"));
    			$this->db->where('DATE(sell_details.date_created) <', DATE($range_date[1]));
    		}
    		if ($range_date[1]==0 && $range_date[0]!=0) {
    			$this->db->where('DATE(sell_details.date_created) >', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <', "DATE(NOW())");
    		}
    		if ($range_date[1]>0 && $range_date[0]>0) {
    			$this->db->where('DATE(sell_details.date_created) >=', DATE($range_date[0]));
    			$this->db->where('DATE(sell_details.date_created) <=', DATE($range_date[1]));
    		}
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
    	}
    	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('customers.customers_name', 'asc');
    	}
    	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('customers.customers_name', 'desc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.net_payable', 'asc');
    	}
    	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.net_payable', 'desc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.paid', 'asc');
    	}
    	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.paid', 'desc');
    	}    if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.due', 'asc');
    	}
    	if($filters['order']['0']['column'] == '4' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.due', 'desc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='asc') {
    		$this->db->order_by('sell_details.date_created', 'asc');
    	}
    	if($filters['order']['0']['column'] == '5' && $filters['order']['0']['dir']=='desc') {
    		$this->db->order_by('sell_details.date_created', 'desc');
    	}
    	if ($total_count) {
    		return $this->db->get()->num_rows();            
    	} else {
    		$this->db->limit($filters['length'], $filters['start']);
    		$qry_get = $this->db->get();
    		$output_arr =$qry_get->result_array();
    		return $output_arr;
    	}
    }
    /**
 * Get items buying price for damage lost form
 *
 * @param  string $id
 * @return array[]
 * @link   Buy/get_item_buying_price
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function select_item_buying_info($id)
    {
    	$this->db->select('buying_price', false);
    	$this->db->from('buy_cart_items');
    	$this->db->where('item_spec_set_id', $id);
    	$this->db->order_by('date_created', 'desc');
    	return$this->db->get()->row_array();
    }
    /**
 * Select bank account's info which is currently activated for card payment receive.
 *
 * @return array[]
 * @link   Sell/save_sell_info
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function curr_bank_for_card_receive()
    {
    	$this->db->select('bank_acc_id,final_amount');
    	$this->db->from('bank_accounts');
    	$this->db->where('selection_status', 'selected');
    	return  $this->db->get()->row_array();
    }
/**
 * Save data of bank form in database 
 *
 * @return null
 * @link   Bank_dpst_wdrl/save_deposit_withdrawal_amount_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function save_card_payment_info($data)
{
	$this->db->select('UUID()', false);    
	$uuid =$this->db->get()->row_array();
	$data['bank_statement_id']=$uuid['UUID()'];
	$this->db->insert('bank_statement', $data);
	return $uuid['UUID()'];
}
    /**
 * Update total amount of an account after card payment
 *
 * @return null
 * @link   Sell/save_sell_info
 * @param  array[] $final_amount
 * @param  string $bank_acc_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
    public function update_total_amt_info_of_acc($final_amount,$bank_acc_id)
    {
    	return $this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $final_amount);
    }
    public function cheque_number_availability($cheque_number)
    {
    	$this->db->select('cheque_number');
    	$this->db->from('pending_cheque_details');
    	$this->db->where('cheque_number', $cheque_number);
    	return $this->db->get()->row_array();
    }

    public function details_vendor_due_by_id($vendors_id)
    {
    	$this->db->select('sum(due) as due,format(sum(paid),2) as paid,format(sum(net_payable),2) as net_payable', FALSE);
    	$this->db->from('buy_details');
    	$this->db->where('vendors_id', $vendors_id);
    	$this->db->where('publication_status', 'activated');
    	$this->db->group_by('vendors_id');
    	return $this->db->get()->row_array();
    }

    public function all_due_vouchers_by_vendor_id($vendors_id)
    {
    	$this->db->select('due,paid,buy_details_id,discount,net_payable', FALSE);
    	$this->db->from('buy_details');
    	$this->db->where('vendors_id', $vendors_id);
    	$this->db->where('due >', 0);
    	$this->db->where('publication_status', 'activated');
    	$this->db->order_by('date_created', 'asc');
    	return $this->db->get()->result_array();
    }

    public function bunch_due_paymentmade_update($buy_details_id,$bunch_update)
    {
    	return $this->db->where('buy_details_id', $buy_details_id)->update('buy_details', $bunch_update);
    }

    public function details_customer_due_by_id($customers_id)
    {
    	$this->db->select('format(sum(due),2) as due,format(sum(paid),2) as paid,format(sum(net_payable),2) as net_payable', FALSE);
    	$this->db->from('sell_details');
    	$this->db->where('customers_id', $customers_id);
    	$this->db->where('publication_status', 'activated');
    	if ($this->is_vat_included == "yes") {
    		$this->db->where('vat_amount >', 0);
    	}
    	$this->db->group_by('customers_id');
    	return $this->db->get()->row_array();
    }

    public function all_due_vouchers_by_customer_id($customers_id)
    {
    	$this->db->select('due,paid,sell_details_id', FALSE);
    	$this->db->from('sell_details');
    	$this->db->where('customers_id', $customers_id);
    	$this->db->where('due >', 0);
    	$this->db->where('publication_status', 'activated');
    	$this->db->order_by('date_created', 'asc');
    	return $this->db->get()->result_array();
    }

    public function get_customer_info_for_voucher($customers_id)
    {
    	$this->db->select('*', FALSE);
    	$this->db->from('customers');
    	$this->db->where('customers_id', $customers_id);
    	return  $this->db->get()->row_array();
    }

    public function collect_voucher_number($sell_details_uuid)
    {
        $this->db->select('
            sell_details.sell_local_voucher_no,
            sell_details.vat_amount,
            sell_details.net_payable,
            sell_details.sub_total,
            sell_details.tax_percentage,
            sell_details.discount,
            sell_details.paid,
            sell_details.due,
            sell_details.customers_id,
            sell_details.discount_percentage,
            sell_details.grand_total,
            sell_details.voucher_unique_barcode,
            sell_details.date_created, 
            customers.customers_name,
            customers.customers_phone_1,
            customers.customers_present_address,
            sales_representative.sales_rep_name
            ', FALSE);
        $this->db->from('sell_details');
        $this->db->join('sales_representative', 'sales_representative.sales_rep_id = sell_details.sales_rep_id', 'left');
        $this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
        $this->db->where('sell_details.sell_details_id', $sell_details_uuid);
        return $this->db->get()->row_array();
    }

    public function voucher_details($sell_details_id)
    {
    	$this->db->select('sell_cart_items.quantity,sell_cart_items.sell_comments,sell_cart_items.sub_total,sell_cart_items.discount_amount,sell_cart_items.discount_type,sell_cart_items.selling_price,item_spec_set.spec_set');

    	$this->db->from('sell_details');
    	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
    	$this->db->where('sell_details.publication_status', 'activated');
    	$this->db->where('sell_details.sell_details_id', $sell_details_id);
    	return $this->db->get()->result_array();
    }

    public function payment_amount_by_cash($buy_details_id)
    {
    	$this->db->select('SUM(deposit_withdrawal_amount) AS amount', FALSE);
    	$this->db->from('cashbox');
    	$this->db->where('buy_or_sell_details_id', $buy_details_id);
    	return $this->db->get()->row_array();
    }

    public function payment_amount_by_cheque($buy_details_id)
    {
    	$this->db->select('SUM(deposit_withdrawal_amount) AS amount,bank_acc_id', FALSE);
    	$this->db->from('bank_statement');
    	$this->db->where('buy_sell_and_other_id', $buy_details_id);
    	$this->db->group_by('bank_acc_id');
    	return $this->db->get()->result_array();
    }

    public function get_current_bank_amt_info($bank_acc_id)
    {
    	$this->db->select('final_amount', FALSE);
    	$this->db->from('bank_accounts');
    	$this->db->where('bank_acc_id', $bank_acc_id);
    	return $this->db->get()->row_array();
    }
/**
 * Update individual bank account's information of database by id after deletion of a buy voucher.
 *
 * @return boolean
 * @link   Buy_list_all/delete_buy
 * @param  string  $bank_acc_id Parameter-1
 * @param  array[] $data     Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function update_bank_acc_info($bank_acc_id, $updated_amount)
{
	return ($this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $updated_amount)); 
}
/**
 * Collect bank account id used in expense for deletion of expense
 *
 * @return array[]
 * @link   Expenditure/delete_expenditure
 * @param  string  $expenditures_id Parameter-1
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function get_bank_acc_info($expenditures_id)
{
	$this->db->select('fields', FALSE);
	$this->db->from('table');
	$this->db->join('table', 'table1.field = table2.field', 'left');
	$this->db->where('field', 'value', FALSE);
}
/**
 * Collect payment type by sell id.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function sell_payment_detials($sell_details_id)
{
	$this->db->select('payment_type');
	$this->db->from('sell_payment_details');
	$this->db->where('sell_details_id', $sell_details_id);
	return $this->db->get()->row_array();
}

/**
 * Collect sell amount and bank account id.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function get_card_payment_details_for_sell($sell_details_id)
{
	$this->db->select('deposit_withdrawal_amount,bank_acc_id');
	$this->db->from('bank_statement');
	$this->db->where('buy_sell_and_other_id',$sell_details_id);
	$this->db->where('cash_type', 'sell');
	return $this->db->get()->row_array();
}

public function get_card_payment_details_for_adv_sell($adv_sell_details_id)
{
	$this->db->select('deposit_withdrawal_amount,bank_acc_id');
	$this->db->from('bank_statement');
	$this->db->where('buy_sell_and_other_id',$adv_sell_details_id);
	$this->db->where('cash_type', 'advance_sell');
	return $this->db->get()->row_array();
}
/**
 * Change publication status activated to deactivated.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function change_status_of_card_payment($sell_details_id)
{
	$this->db->set('publication_status', 'deactivated')->where('buy_sell_and_other_id', $sell_details_id)->update('bank_statement');
}
/**
 *Just check that the cheque already has cleared or pending.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function curr_cheque_status($sell_details_id)
{
	$this->db->select('cheque_status, cheque_amount');
	$this->db->from('pending_cheque_details');
	$this->db->where('sell_details_id', $sell_details_id);
	return $this->db->get()->row_array();
}
/**
 * Change publication status activated to deactivated.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function delete_pending_cheque($sell_details_id)
{
	$this->db->set('publication_status', 'deactivated')->where('sell_details_id', $sell_details_id)->update('pending_cheque_details');
}
/**
 * Collect bank account id for cleared cheque
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function acc_id_for_complete_cheque($sell_details_id)
{
	$this->db->select('bank_acc_id');
	$this->db->from('pending_cheque_details');
	$this->db->where('sell_details_id', $sell_details_id);
	$acc_id =  $this->db->get()->row_array();
	return $acc_id['bank_acc_id'];
}
/**
 * Collect cheque cleared type whether it is cleared by cash or bank account
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function cheque_cleared_by($sell_details_id)
{
	$this->db->select('cheque_cleared_type,bank_acc_id');
	$this->db->from('pending_cheque_details');
	$this->db->where('sell_details_id', $sell_details_id);
	return $this->db->get()->row_array();
}
/**
 * Collect amount paid by cash for cash and card both payment during sell.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function amt_paid_by_cash($sell_details_id)
{
	$this->db->select('deposit_withdrawal_amount');
	$this->db->from('cashbox');
	$this->db->where('buy_or_sell_details_id', $sell_details_id);
	return $this->db->get()->row_array();
}
/**
 * Collect amount paid by card and bank account id for cash and card both payment during sell.
 *@link Sell_list_all/delete_sell
 *@param string $sell_details_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function amt_paid_by_card_details($sell_details_id)
{
	$this->db->select('deposit_withdrawal_amount,bank_acc_id');
	$this->db->from('bank_statement');
	$this->db->where('buy_sell_and_other_id', $sell_details_id);
	return $this->db->get()->row_array();
}
/**
 * Can change current buy date to previous or upcoming date.
 *@link Buy_list_all/change_buy_date_info
 *@param string $buy_detials_id
 *@param array[] $data
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

public function change_buy_date($buy_details_id,$data)
{
	return $this->db->where('buy_details_id', $buy_details_id)->update('buy_details', $data);
}
/**
 * Save imei barcode info in table.
 *@param array[] $val_of_imei
 * @link Buy/save_buy_info
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function save_imei_barcode_info($val_of_imei)
{
	unset($val_of_imei['imei_temp_id']);
	$this->db->select('UUID()', false);    
	$uuid =$this->db->get()->row_array();
	$val_of_imei['imei_barcode_id']=$uuid['UUID()'];
	$this->db->insert('imei_barcode', $val_of_imei);
}
/**
 * Change barcode status to used after sell of that product.
 *@param string $imei_code
 *@param array[] $barcode_data
 * @link Sell/save_sell_info
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function update_barcode_status($imei_code,$barcode_data)
{
	$this->db->where('imei_barcode', $imei_code)->where('barcode_status', 'not_used')->where('publication_status', 'activated')->update('imei_barcode', $barcode_data);

}
/**
 * Search item by imei or unique serial number in sell page.
 *@param string $text
 * @link Sell/search_item_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_by_imei_barcode($text)
{
	$this->db->select('inventory.item_spec_set_id,item_spec_set.spec_set,inventory.quantity,item_spec_set.item_spec_set_image,imei_barcode.imei_barcode,item_spec_set.product_warranty,item_spec_set.unique_barcode');
	$this->db->from('imei_barcode');
	$this->db->join('inventory', 'inventory.item_spec_set_id = imei_barcode.item_spec_set_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
	$this->db->where('inventory.quantity >',  0);
	$this->db->where('item_spec_set.publication_status', 'activated');
	$this->db->where('imei_barcode.barcode_status', 'not_used');
	$this->db->where('imei_barcode.publication_status', 'activated');
	$this->db->where('imei_barcode.imei_barcode', $text);
	return $this->db->get()->row_array();
}
/**
 * Search item by product barcode in sell page.
 *@param string $text
 * @link Sell/search_item_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_by_barcode($text)
{
	$this->db->select('inventory.item_spec_set_id,item_spec_set.spec_set,inventory.quantity,item_spec_set.item_spec_set_image,item_spec_set.barcode_number,item_spec_set.product_warranty,item_spec_set.unique_barcode');
	$this->db->from('inventory');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
	$this->db->where('inventory.quantity >',  0);
	$this->db->where('item_spec_set.publication_status', 'activated');
	$this->db->where('item_spec_set.barcode_number', $text);
	return $this->db->get()->row_array();
}
/**
 * Search item by name in sell page.
 *@param string $text
 * @link Sell/search_item_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_item($text)
{
	$this->db->select('inventory.item_spec_set_id,item_spec_set.spec_set,inventory.quantity,item_spec_set.item_spec_set_image,item_spec_set.barcode_number,item_spec_set.product_warranty,item_spec_set.unique_barcode');
	$this->db->from('inventory');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
	$this->db->where('inventory.quantity>',  0);
	$this->db->where('item_spec_set.publication_status', 'activated');
	$this->db->like('item_spec_set.spec_set', $text, 'both');
	$this->db->or_like('item_spec_set.barcode_number', $text, 'both');
	$this->db->limit(10);
	return $this->db->get()->result_array();
}
/**
 * Search customer in sell page by name or phone number.
 *@param string $text
 * @link Sell/search_customer_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_customer($text)
{
	$this->db->select('customers_id,customers_name,customers_image,customers_phone_1');
	$this->db->from('customers');
	$this->db->where('publication_status', 'activated');
	$this->db->like('customers_name', $text, 'both');
	$this->db->or_like('customers_phone_1', $text, 'both');
	$this->db->limit(5);
	return $this->db->get()->result_array();
}
/**
 * Search sales representative in sell page by name or phone number.
 *@param string $text
 * @link Sell/search_sales_rep_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_sales_rep($text)
{
	$this->db->select('sales_rep_id,sales_rep_name,sales_rep_image');
	$this->db->from('sales_representative');
	$this->db->where('publication_status', 'activated');
	$this->db->like('sales_rep_name', $text, 'both');
	$this->db->or_like('sales_rep_phone_1', $text, 'both');
	$this->db->limit(5);
	return $this->db->get()->result_array();
}
/**
 * Search vendor in buy page by name or phone number.
 *@param string $text
 * @link Buy/search_vendor_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_vendor($text)
{
	$this->db->select('vendors_id,vendors_name,vendor_image,vendors_phone_1');
	$this->db->from('vendors');
	$this->db->where('publication_status', 'activated');
	$this->db->like('vendors_name', $text, 'both');
	$this->db->or_like('vendors_phone_1', $text, 'both');
	$this->db->limit(5);
	return $this->db->get()->result_array();
}
/**
 * Search item by product barcode in buy page.
 *@param string $text
 * @link Buy/search_item_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_by_barcode_in_buy($text)
{
	$this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,item_spec_set.item_spec_set_image,item_spec_set.unique_barcode');
	$this->db->from('item_spec_set');
	$this->db->where('publication_status', 'activated');
	$this->db->where('barcode_number', $text);
            // $this->db->limit(1);
	return $this->db->get()->row_array();
}
/**
 * Search item by name in buy page.
 *@param string $text
 * @link Buy/search_item_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_item_in_buy($text)
{
	$this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,item_spec_set.item_spec_set_image,item_spec_set.unique_barcode');
	$this->db->from('item_spec_set');
	$this->db->where('publication_status', 'activated');
	$this->db->like('spec_set', $text,'both');
	$this->db->limit(10);
	return $this->db->get()->result_array();
 // $this->db->get();
 // echo $this->db->last_query();
}
/**
 * Search category by name in category page.
 *@param string $text
 * @link Category/search_category_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_category($text)
{
	$this->db->select('categories_name,categories_id');
	$this->db->from('categories');
	$this->db->like('categories_name', $text, 'both');
	$this->db->where('publication_status', 'activated');
	$this->db->limit(5);
	return $this->db->get()->result_array();
}
/**
 * Search expenditure by name.
 *@param string $text
 * @link Expenditure/search_expenditure_type_by_name
 * @return array[]
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function search_expenditure($text)
{
	$this->db->select('expenditure_types_name,expenditure_types_id');
	$this->db->from('expenditure_types');
	$this->db->like('expenditure_types_name', $text, 'both');
	$this->db->where('publication_status', 'activated');
	$this->db->limit(10);
	return $this->db->get()->result_array();
}
/**
 * Change imei barcode status to not used after delete a sell.
 *@param string $sell_details_id
 * @link Sell_list_all/delete_sell
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function change_imei_barcode_status($sell_details_id)
{
	$this->db->set('barcode_status', 'not_used')->where('sell_details_id', $sell_details_id)->update('imei_barcode');
}
/**
 * Change imei barcode status to deactivated after delete a buy.
 *@param string $buy_details_id
 * @link Buy_list_all/delete_buy
 * @author  Shoaib <shofik.shoaib@gmail.com>
 **/
public function change_imei_barcode_publication_status($buy_details_id)
{
	$this->db->set('publication_status', 'deactivated')->where('buy_details_id', $buy_details_id)->update('imei_barcode');
}

public function customer_payment_history($customers_id)
{
	$this->db->select('sum(due) as due,sum(paid) as paid,sum(net_payable) as net_payable', FALSE);
	$this->db->from('sell_details');
	$this->db->where('customers_id', $customers_id);
	$this->db->group_by('customers_id');
	return $this->db->get()->row_array();
}
/**
 * undocumented function
 *
 *@param string $vendors_id
 *@link 
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function get_vendor_info_for_voucher($vendors_id)
{
	$this->db->select('*', FALSE);
	$this->db->from('vendors');
	$this->db->where('vendors_id', $vendors_id);
	return  $this->db->get()->row_array();
}
/**
 * Collect all the information of particular buy by its id.
 *
 *@param string $buy_details_uuid
 *@link Buy_list_aff/get_all_buy_info
 *@return array[]
 *@author Shoaib <shofik.shoaib@gmail.com>
 **/
public function buy_details_by_id($buy_details_id)
{
	$this->db->select('*');
	$this->db->from('buy_details');
	$this->db->where('buy_details_id', $buy_details_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect voucher number from database by buy_details_uuid
 *
 *@param string $buy_details_uuid
 *@link Buy_list_aff/get_all_buy_info
 *@return array[]
 *@author Shoaib <shofik.shoaib@gmail.com>
 **/
public function collect_voucher_number_buy($buy_details_uuid)
{
	$this->db->select('voucher_no,net_payable,grand_total,discount,paid,due,vendors_id', FALSE);
	$this->db->from('buy_details');
	$this->db->where('buy_details_id', $buy_details_uuid);
	return $this->db->get()->row_array();
}
/**
 * Save batta hisab to database.
 * @param array[] $batta_history
 * @link Buy_list/save_due_paymentmade
 * @return void
 * @author shoaib
 **/
public function save_batta_history($batta_history)
{
	$this->db->select('UUID()', false);    
	$uuid =$this->db->get()->row_array();
	$batta_history['batta_history_id']=$uuid['UUID()'];
	$this->db->insert('batta_history', $batta_history);
	return $uuid['UUID()'];
}
/**
 * Check imei is exist and not deleted.
 * @param string $val_of_imei 
 * @link Buy/save_buy_info
 * @return void
 * @author shoaib
 **/

public function check_imei_avilable_info($val_of_imei)
{
	$this->db->select('imei_barcode');
	$this->db->from('imei_barcode');
	$condition_array = array('imei_barcode' => $val_of_imei, 'publication_status' => 'activated', 'barcode_status'=>'not_used');
	$this->db->where($condition_array);
	$iemi_num =  $this->db->get()->row_array();
	return $iemi_num['imei_barcode'];
}

/**
 * If imei used for selling items and status is activated then the item can be use again.
 * @param string $val_of_iemi
 * @link buy/save_buy_info
 * @return void
 * @author shoaib
 **/

public function check_is_sell($val_of_imei)
{
	$this->db->select('imei_barcode');
	$this->db->from('imei_barcode');
	$condition_array = array('imei_barcode' => $val_of_imei, 'barcode_status' => 'used', 'publication_status' => 'deactivated');
	$this->db->where($condition_array);
	$iemi_num =  $this->db->get()->row_array();
	return $iemi_num['imei_barcode'];
}



public function getBuyItemsByVoucher($voucher_no)
{
	$this->db->select('buy_details_id');
	$this->db->from('buy_details');
	$this->db->where('voucher_no', $voucher_no);
	$v_no = $this->db->get()->row_array();
	$this->db->select('buy_cart_items.quantity,buy_cart_items.sub_total,buy_cart_items.discount_type,buy_cart_items.discount_amount,buy_cart_items.buying_price,item_spec_set.spec_set');
	$this->db->from('buy_details');
	$this->db->join('buy_cart_items', 'buy_cart_items.buy_details_id = buy_details.buy_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->where('buy_details.publication_status', 'activated');
	$this->db->where('buy_details.buy_details_id', $v_no['buy_details_id']);
	$this->db->group_by('item_spec_set.spec_set');
	return $this->db->get()->result_array();
}
public function getSoldItemsByVoucher($voucher_no)
{
	$this->db->select('sell_details_id');
	$this->db->from('sell_details');
	$this->db->where('sell_local_voucher_no', $voucher_no);
	$v_no = $this->db->get()->row_array();
	$this->db->select('sell_cart_items.quantity,sell_cart_items.sub_total,sell_cart_items.discount_type,sell_cart_items.discount_amount,sell_cart_items.selling_price as buying_price,item_spec_set.spec_set');
	$this->db->from('sell_details');
	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
	$this->db->where('sell_details.publication_status', 'activated');
	$this->db->where('sell_details.sell_details_id', $v_no['sell_details_id']);
	$this->db->group_by('item_spec_set.spec_set');
	return $this->db->get()->result_array();
}

public function get_item_buying_price($data)
{
	$this->db->select('buying_price', FALSE);
	$this->db->from('buy_cart_items');
	$this->db->where('item_spec_set_id', $data);
	$this->db->order_by('date_updated', 'desc');
	$this->db->limit(1);
	$buying_price= $this->db->get()->row_array();
	return $buying_price['buying_price'];
}
public function get_item_imei_list($data)
{
	$this->db->select('imei_barcode', FALSE);
	$this->db->from('imei_barcode');
	$condition_array = array('item_spec_set_id' => $data, 'barcode_status' => 'not_used', 'publication_status' => 'activated');
	$this->db->where($condition_array);
	return $this->db->get()->result_array();

}

public function is_unique_barcode_for_item($id)
{
	$this->db->select('unique_barcode', FALSE);
	$this->db->from('item_spec_set');
	$this->db->where('item_spec_set_id', $id);
	return $this->db->get()->row_array();
}

public function get_imei_info($demaged_imei)
{
	$this->db->select('barcode_status,publication_status', FALSE);
	$this->db->from('imei_barcode');
	$this->db->where('imei_barcode', $demaged_imei);
	return $this->db->get()->row_array();
}

public function get_imei_barcode_id($demaged_imei)
{
	$this->db->select('imei_barcode_id', FALSE);
	$this->db->from('imei_barcode');
	$this->db->where('imei_barcode', $demaged_imei);
	$result = $this->db->get()->row_array();
	return $result['imei_barcode_id'];
}

public function update_barcode_status_after_delete_damage($imei_barcode_id,$barcode_data)
{
	$this->db->where('imei_barcode_id', $imei_barcode_id)->update('imei_barcode', $barcode_data);

}

public function is_imei_available_info($selected_item_id)
{
	$this->db->select('unique_barcode', FALSE);
	$this->db->from('item_spec_set');
	$this->db->where('item_spec_set_id', $selected_item_id);
	return $this->db->get()->row_array();
}

public function check_imei_sold_already_or_not($buy_details_id)
{
	$this->db->select('imei_barcode_id');
	$this->db->from('imei_barcode');
	$this->db->where('publication_status', 'activated');
	$this->db->where('barcode_status', 'used');
	$this->db->where('buy_details_id', $buy_details_id);
	return $this->db->get()->result_array();
}

public function get_avg_item_buy_price($data)
{
    // $this->db->select('format(sum(sub_total)/ sum(quantity),2) as "avg_price"', false);
    // $this->db->from('buy_cart_items');
    /*New Avg Buying Price Integrated*/
    $this->db->select('avg_buy_price');
    $this->db->from('inventory');
    $this->db->where('item_spec_set_id', $data);
    // $this->db->where('publication_status', 'activated');

    $output_arr= $this->db->get()->row_array();
    return $output_arr['avg_buy_price'];
}


public function deleted_sell_list_all_info($filters,$total_count=false)
{

	$no_customer = $this->lang->line("no_customer");

	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', false);
	$this->db->select(
		'
		customers.customers_phone_1 as "1",
		sell_details.sell_local_voucher_no as "2",
		DATE_FORMAT(sell_details.date_updated, "%d-%b-%Y") as "3"
		'
		);

	$lang_details = $this->lang->line("model_details");
	$lang_delete = $this->lang->line("model_delete");
	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green sell_voucher_details\" id=\"edit_', CAST(sell_details.sell_details_id AS CHAR) ,'\">$lang_details</button>') as '4'", false);


         // $this->db->select('concat(\'<button class="btn btn-xs green sell_voucher_details" id="edit_\',sell_details.sell_details_id,\'">Details</button>\',\'<button class="btn btn-xs red delete_sell_info" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',sell_details.sell_details_id,\'">Delete</button>\') as "3"', FALSE);

	$this->db->from('sell_details');
	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
	$this->db->join('imei_barcode', 'imei_barcode.sell_details_id = sell_details.sell_details_id', 'left');
	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
	$this->db->where('sell_details.publication_status', 'deactivated');
	if ($this->is_vat_included == "yes") {
		$this->db->where('sell_details.vat_amount >', 0);
	}
	$this->db->group_by('sell_details.sell_local_voucher_no');
         // $this->db->order_by("sell_details.date_created", "desc"); 

	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
		$where ="(";
		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
		$where.= "imei_barcode.imei_barcode like '%".$filters['search']['value']."%'";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
		$where.= ")";

		$this->db->where($where, null, false);
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
		$this->db->like('customers.customers_phone_1', $filters['columns']['1']['search']['value'], 'both');
	}
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
		$this->db->like('sell_details.sell_local_voucher_no', $filters['columns']['2']['search']['value'], 'both');
	}

	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
		$range_date = explode('_', $filters['columns']['3']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(sell_details.date_updated) >', DATE("1992-01-01"));
			$this->db->where('DATE(sell_details.date_updated) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(sell_details.date_updated) >', DATE($range_date[0]));
			$this->db->where('DATE(sell_details.date_updated) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(sell_details.date_updated) >=', DATE($range_date[0]));
			$this->db->where('DATE(sell_details.date_updated) <=', DATE($range_date[1]));
		}
	}

	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('customers.customers_name', 'asc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('customers.customers_name', 'desc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('customers.customers_phone_1', 'asc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('customers.customers_phone_1', 'desc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('sell_details.sell_local_voucher_no', 'asc');
	}

	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('sell_details.sell_local_voucher_no', 'desc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('sell_details.date_updated', 'asc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('sell_details.date_updated', 'desc');
	}

	if ($total_count) {
		return $this->db->get()->num_rows();            
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$qry_get = $this->db->get();
		$output_arr =$qry_get->result_array();
		return $output_arr;
	}

}

public function deleted_sell_info_by_id($sell_details_id)
{
	$this->db->select('DISTINCT imei_barcode.imei_barcode,sell_details.grand_total,sell_details.sell_local_voucher_no,sell_details.paid,sell_details.due,sell_details.discount,sell_details.net_payable,sell_cart_items.quantity,sell_cart_items.sub_total,sell_cart_items.discount_amount,sell_cart_items.discount_type,sell_cart_items.selling_price,item_spec_set.spec_set,sell_cart_items.used_imei_barcode', FALSE);
	$this->db->from('sell_details');
	$this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
	$this->db->join('imei_barcode', 'imei_barcode.sell_details_id = sell_details.sell_details_id AND imei_barcode.item_spec_set_id = item_spec_set.item_spec_set_id ', 'left');
     // $this->db->group_by('imei_barcode.imei_barcode_id');
	$this->db->where('sell_details.publication_status', 'deactivated');
	$this->db->where('sell_details.sell_details_id', $sell_details_id);
	return $this->db->get()->result_array();
     // echo $this->db->last_query();
}

public function adv_sell_list_all_info($filters,$total_count=false)
{

	$no_customer = $this->lang->line("no_customer");

	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_customer.'" ) as "0"', false);
	$this->db->select(
		'
		customers.customers_phone_1 as "1",
		adv_sell_details.sell_local_voucher_no as "2",
		DATE_FORMAT(adv_sell_details.delivery_date, "%d-%b-%Y") as "3"
		'
		);

	$lang_details = $this->lang->line("model_details");
	$lang_delete = $this->lang->line("model_delete");
	$lang_edit = "Edit";

	$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green adv_sell_voucher_details\" id=\"edit_', CAST(adv_sell_details.adv_sell_details_id AS CHAR) ,'\">$lang_details</button>','<button type=\"button\" class=\"btn btn-xs red delete_adv_sell\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(adv_sell_details.adv_sell_details_id AS CHAR) ,'\">$lang_delete</button>') as '4'", false);

	/*Edit Button*/
 // '<button type=\"button\" class=\"btn btn-xs blue adv_sell_voucher_edit\" id=\"edit_', CAST(adv_sell_details.adv_sell_details_id AS CHAR) ,'\">$lang_edit</button>',

	$this->db->from('adv_sell_details');
	$this->db->join('customers', 'customers.customers_id = adv_sell_details.customers_id', 'left');
	$this->db->join('imei_barcode', 'imei_barcode.sell_details_id = adv_sell_details.adv_sell_details_id', 'left');
	$this->db->join('adv_sell_cart_items', 'adv_sell_cart_items.adv_sell_details_id = adv_sell_details.adv_sell_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = adv_sell_cart_items.item_spec_set_id', 'left');
	$this->db->where('adv_sell_details.publication_status', 'activated');
	$this->db->where('adv_sell_details.delivery_status', 'pending');

	if ($this->is_vat_included == "yes") {
		$this->db->where('adv_sell_details.vat_amount >', 0);
	}
	$this->db->group_by('adv_sell_details.sell_local_voucher_no');

	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
		$where ="(";
		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
		$where.= "imei_barcode.imei_barcode like '%".$filters['search']['value']."%'";
		$where.= ")";
		$this->db->where($where, null, false);
	}
	if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
		$this->db->like('customers.customers_name', $filters['columns']['0']['search']['value'], 'both');
	}
	if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
		$this->db->like('customers.customers_phone_1', $filters['columns']['1']['search']['value'], 'both');
	}
	if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
		$this->db->like('adv_sell_details.sell_local_voucher_no', $filters['columns']['2']['search']['value'], 'both');
	}

	if($filters['columns']['3']['search']['value']!="" && $filters['columns']['3']['search']['value']!=null) {
		$range_date = explode('_', $filters['columns']['3']['search']['value']);
		if ($range_date[1]!=0 && $range_date[0]==0) {
			$this->db->where('DATE(adv_sell_details.delivery_date) >', DATE("1992-01-01"));
			$this->db->where('DATE(adv_sell_details.delivery_date) <', DATE($range_date[1]));
		}
		if ($range_date[1]==0 && $range_date[0]!=0) {
			$this->db->where('DATE(adv_sell_details.delivery_date) >', DATE($range_date[0]));
			$this->db->where('DATE(adv_sell_details.delivery_date) <', "DATE(NOW())");
		}
		if ($range_date[1]>0 && $range_date[0]>0) {
			$this->db->where('DATE(adv_sell_details.delivery_date) >=', DATE($range_date[0]));
			$this->db->where('DATE(adv_sell_details.delivery_date) <=', DATE($range_date[1]));
		}
	}

	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('customers.customers_name', 'asc');
	}
	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('customers.customers_name', 'desc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('customers.customers_phone_1', 'asc');
	}
	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('customers.customers_phone_1', 'desc');
	}
	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('adv_sell_details.sell_local_voucher_no', 'asc');
	}

	if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('adv_sell_details.sell_local_voucher_no', 'desc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='asc') {
		$this->db->order_by('adv_sell_details.delivery_date', 'asc');
	}

	if($filters['order']['0']['column'] == '3' && $filters['order']['0']['dir']=='desc') {
		$this->db->order_by('adv_sell_details.delivery_date', 'desc');
	}

	if ($total_count) {
		return $this->db->get()->num_rows();            
	} else {
		$this->db->limit($filters['length'], $filters['start']);
		$qry_get = $this->db->get();
  // echo $this->db->last_query();
		$output_arr =$qry_get->result_array();
		return $output_arr;
	}

}

public function adv_sell_details_by_id($adv_sell_details_id)
{
	$this->db->select('*');
	$this->db->from('adv_sell_details');
	$this->db->where('adv_sell_details_id', $adv_sell_details_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}

public function sell_details_info_by_id($sell_details_id)
{
	$this->db->select('*');
	$this->db->from('sell_details');
	$this->db->where('sell_details_id', $sell_details_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}

public function adv_sell_info_by_id($adv_sell_details_id)
{
	$this->db->select('DISTINCT imei_barcode.imei_barcode,adv_sell_details.grand_total,adv_sell_details.sell_local_voucher_no,adv_sell_details.paid,adv_sell_details.due,adv_sell_details.discount,adv_sell_details.net_payable,adv_sell_cart_items.quantity,adv_sell_cart_items.sub_total,adv_sell_cart_items.discount_amount,adv_sell_cart_items.discount_type,adv_sell_cart_items.selling_price,item_spec_set.spec_set,adv_sell_cart_items.used_imei_barcode,adv_sell_details.delivery_date,adv_sell_details.custom_voucher_no', FALSE);
	$this->db->from('adv_sell_details');
	$this->db->join('adv_sell_cart_items', 'adv_sell_cart_items.adv_sell_details_id = adv_sell_details.adv_sell_details_id', 'left');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = adv_sell_cart_items.item_spec_set_id', 'left');
	$this->db->join('imei_barcode', 'imei_barcode.sell_details_id = adv_sell_details.adv_sell_details_id AND imei_barcode.item_spec_set_id = item_spec_set.item_spec_set_id ', 'left');
     // $this->db->group_by('imei_barcode.imei_barcode_id');
	$this->db->where('adv_sell_details.publication_status', 'activated');
	$this->db->where('adv_sell_details.adv_sell_details_id', $adv_sell_details_id);
	return $this->db->get()->result_array();
  // echo $this->db->last_query();
}

public function collect_adv_voucher_number($adv_sell_details_uuid)
{
	$this->db->select('adv_sell_details_id,sell_local_voucher_no,vat_amount,net_payable,sub_total,tax_percentage,discount,paid,due,customers_id,discount_percentage,grand_total,voucher_unique_barcode', FALSE);
	$this->db->from('adv_sell_details');
	$this->db->where('adv_sell_details_id', $adv_sell_details_uuid);
	return $this->db->get()->row_array();
}

public function update_adv_sell_info_by_id($adv_sell_details_id,$update_data)
{
	return  $this->db->where('adv_sell_details_id', $adv_sell_details_id)->update('adv_sell_details', $update_data);
}

public function update_adv_buy_info_by_id($adv_buy_details_id,$update_data)
{
	return  $this->db->where('adv_buy_details_id', $adv_buy_details_id)->update('adv_buy_details', $update_data);
}

public function delete_adv_invoice($adv_sell_details_id)
{
	$data =array(
		'publication_status' => 'deactivated',
		'delivery_status' => 'deleted'
		);
	return  $this->db->where('adv_sell_details_id', $adv_sell_details_id)->update('adv_sell_details', $data);
}

public function delete_buy_adv_invoice($adv_buy_details_id)
{
	$data =array(
		'publication_status' => 'deactivated',
		'receival_status' => 'deleted'
		);
	return $this->db->where('adv_buy_details_id', $adv_buy_details_id)->update('adv_buy_details', $data);

}

public function get_current_amount_by_vendor($vendor_id)
{
	$this->db->select('current_balance', FALSE);
	$this->db->from('vendors');
	$this->db->where('vendors_id', $vendor_id);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}

public function update_total_amt_info_of_vendor($final_data_vendor,$vendor_id)
{
	return $this->db->where('vendors_id', $vendor_id)->update('vendors', $final_data_vendor);
}

public function search_item_for_barcode_print($text)
{
	$this->db->select('*');
	$this->db->from('item_spec_set');
	$this->db->where('publication_status', 'activated');
	$this->db->like('spec_set', $text, 'both');
	$this->db->or_like('barcode_number', $text, 'both');
	$this->db->limit(10);
	return $this->db->get()->result_array();
}

public function get_item_barcode_info($items_id)
{
	$this->db->select('item_spec_set.barcode_number,
		IFNULL(buy_cart_items.retail_price,0) as price
		', FALSE);
	$this->db->from('item_spec_set');
	$this->db->join('buy_cart_items', 'buy_cart_items.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
	$this->db->where('item_spec_set.item_spec_set_id', $items_id);
	$this->db->where('item_spec_set.publication_status', 'activated');
	$this->db->limit(1);
	$this->db->order_by('buy_cart_items.date_created', 'desc');
	return $this->db->get()->row_array();

}

public function update_sell_voucher_info_by_id($sell_details_id, $update_data)
{
	return  $this->db->where('sell_details_id', $sell_details_id)->update('sell_details', $update_data);
}

public function get_sell_payment_details($sell_details_id)
{
	$this->db->select('payment_type, amount, ', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->where('sell_details_id', $sell_details_id);
	return $this->db->get()->row_array();
}

public function get_adv_sell_payment_details($adv_sell_id)
{
	$this->db->select('payment_type, amount,', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->where('sell_details_id', $adv_sell_id);
	// $this->db->where('field', 'value', FALSE);
	return $this->db->get()->row_array();
}

public function check_item_imei($item_spec_set_id)
{
    $this->db->select('unique_barcode');
    $this->db->from('item_spec_set');
    $this->db->where('item_spec_set_id', $item_spec_set_id);
    return $this->db->get()->row_array();
    
}

public function getCustomerInfo($sell_details_id)
{
    $this->db->select('customers.customers_name, customers.customers_phone_1, customers.customers_present_address');
    $this->db->from('sell_details');
    $this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
    $this->db->where('sell_details.sell_details_id', $sell_details_id);
    return $this->db->get()->row_array();
}

}
/* End of file Inventory_model.php */
/* Location: ./application/models/Inventory_model.php */