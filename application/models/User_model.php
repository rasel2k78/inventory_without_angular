<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class User_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();
        $this->load->database("inventory");
        
    }

    /**
     * This method is for getting all user info inside data-table
     * 
     * @return null
     * @author Musabbir 
     **/
    public function all_users_info($filters,$total_count=false)
    {

     $this->db->select(
         '
         username as "0",
         phone_1 as "1"
         '
         );
     $this->db->select('concat(\'<button class="btn btn-sm green edit_user" id="edit_\',user_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_user"   data-toggle="modal" href="#responsive_modal" id="delete_\',user_id,\'">Delete</button>\') as "2"', false);

     $this->db->from('users');
     $this->db->where('status', 'activated');

     if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
        $where ="(";
        $where.= "username like '%".$filters['search']['value']."%'or ";
        $where.= "phone_1 like '%".$filters['search']['value']."%'";
        $where.= ")";
            /*
            */
            $this->db->where($where, null, false);
        }

        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('username', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('username', 'desc');
        }

        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('phone_1', 'asc');
        }
        if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('phone_1', 'desc');
        }

        if ($total_count) {
            return $this->db->get()->num_rows();            
        } else {
            $this->db->limit($filters['length'], $filters['start']);
            $q = $this->db->get();
            $this->db->last_query();
            $a = $q->result_array();
            return $a;
        }

    }

    public function select_all_user_email()
    {
     $this->db->select('users.email');
     $this->db->from('users');
     $q = $this->db->get();
     return $q->result_array();
 }


    /**
     * This method is for select all username from database
     * to check user duplicacy
     * 
     * @return array
     * @author Musabbir 
     **/
    public function select_all_user_username()
    {
     $this->db->select('users.username');
     $this->db->from('users');
     $q = $this->db->get();
     return $q->result_array();
 }


    /**
     * This method is for create a new user
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function createAccount($udata)
    {
     $this->db->select('UUID()');
     $data = $this->db->get()->row_array();
     $udata['user_id'] = $data['UUID()'];

     if($this->db->insert('users', $udata)) {
        return $udata;
    }
    else
    {
        return false;
    }
}


    /**
     * setDefaultAccess method is for set some default access based on user role
     * the method is called silently when a new user creation is success.
     * 
     * @return array
     * @author Musabbir 
     **/
    public function setDefaultAccess($added_user_id,$user_role)
    {
        if($user_role == "manager") {
            $pages_array = array(    
                '0aebd7a2-b854-11e5-acd6-eca86bfd5e5e',
                '0aebe6cc-b854-11e5-acd6-eca86bfd5e5e',
                '0fcb00e9-3b69-11e6-97da-fcaa14e27a26',
                '0fcb0961-3b69-11e6-97da-fcaa14e27a26',
                '1405e8c2-b919-11e5-a956-eca86bfd5e5e',
                '1405f347-b919-11e5-a956-eca86bfd5e5e',
                '1a6c05a3-b918-11e5-a956-eca86bfd5e5e',
                '217d09fe-3b69-11e6-97da-fcaa14e27a26',
                '224d749e-b919-11e5-a956-eca86bfd5e5e',
                '2ae89e9b-47fb-11e6-8bca-eca86bfd5e5e',
                '2eb02d83-b918-11e5-a956-eca86bfd5e5e',
                '2eb037d9-b918-11e5-a956-eca86bfd5e5e',
                '2fa1defa-b919-11e5-a956-eca86bfd5e5e',
                '2fa1e8b8-b919-11e5-a956-eca86bfd5e5e',
                '3639c24e-b915-11e5-a956-eca86bfd5e5e',
                '3639cc1f-b915-11e5-a956-eca86bfd5e5e',
                '3c2fa846-b919-11e5-a956-eca86bfd5e5e',
                '3ce19d14-b918-11e5-a956-eca86bfd5e5e',
                '3ce1a7a3-b918-11e5-a956-eca86bfd5e5e',
                '45811804-b915-11e5-a956-eca86bfd5e5e',
                '4581221c-b915-11e5-a956-eca86bfd5e5e',
                '5ec46626-b915-11e5-a956-eca86bfd5e5e',
                '5ec4704b-b915-11e5-a956-eca86bfd5e5e',
                '655fa8e4-b914-11e5-a956-eca86bfd5e5e',
                '655fb2c5-b914-11e5-a956-eca86bfd5e5e',
                '6b2d56a7-3b88-11e6-b7d1-fcaa14ede82b',
                '6f0836cd-b919-11e5-a956-eca86bfd5e5e',
                '6f93de54-3b84-11e6-b7d1-fcaa14ede82b',
                '7898c1b6-b914-11e5-a956-eca86bfd5e5e',
                '7898cc77-b914-11e5-a956-eca86bfd5e5e',
                '7d11f248-47fe-11e6-8bca-eca86bfd5e5e',
                '7d11fd62-47fe-11e6-8bca-eca86bfd5e5e',
                '86a31d34-b914-11e5-a956-eca86bfd5e5e',
                '86a327cb-b914-11e5-a956-eca86bfd5e5e',
                '88305c46-b918-11e5-a956-eca86bfd5e5e',
                '93cd6437-3b84-11e6-b7d1-fcaa14ede82b',
                '95f4cc9f-3aac-11e6-9e9d-eca86bfd5e5e',
                '9f592a42-3b67-11e6-97da-fcaa14e27a26',
                '9f593210-3b67-11e6-97da-fcaa14e27a26',
                'a28ae09e-b918-11e5-a956-eca86bfd5e5e',
                'a28aea7f-b918-11e5-a956-eca86bfd5e5e',
                'a50a38a0-3b84-11e6-b7d1-fcaa14ede82b',
                'aa816b31-b914-11e5-a956-eca86bfd5e5e',
                'aa81750e-b914-11e5-a956-eca86bfd5e5e',
                'afb34a71-b919-11e5-a956-eca86bfd5e5e',
                'b5adc846-b914-11e5-a956-eca86bfd5e5e',
                'b5add3a8-b914-11e5-a956-eca86bfd5e5e',
                'bae53b58-b919-11e5-a956-eca86bfd5e5e',
                'bae5460f-b919-11e5-a956-eca86bfd5e5e',
                'bd7ff43c-b917-11e5-a956-eca86bfd5e5e',
                'c0ab75f6-d61f-11e5-a98f-eca86bfd5e5e',
                'cc4cfcdd-b918-11e5-a956-eca86bfd5e5e',
                'cc4d071e-b918-11e5-a956-eca86bfd5e5e',
                'cc97c70b-b917-11e5-a956-eca86bfd5e5e',
                'd7d8f8fe-b918-11e5-a956-eca86bfd5e5e',
                'dbfd8142-3b67-11e6-97da-fcaa14e27a26',
                'e5b55f5d-b914-11e5-a956-eca86bfd5e5e',
                'e5b56a23-b914-11e5-a956-eca86bfd5e5e',
                'ea0c38a5-3aad-11e6-9e9d-eca86bfd5e5e',
                'ee571ed9-b917-11e5-a956-eca86bfd5e5e',
                'ee5728d6-b917-11e5-a956-eca86bfd5e5e',
                'f13672c6-b914-11e5-a956-eca86bfd5e5e',
                'f1367d6d-b914-11e5-a956-eca86bfd5e5e',
                'f43d29e1-47fe-11e6-8bca-eca86bfd5e5e',
                'fca3f6d2-b917-11e5-a956-eca86bfd5e5e',
                'fca4008d-b917-11e5-a956-eca86bfd5e5e',
                'fdc2e52a-b914-11e5-a956-eca86bfd5e5e',
                '9f8ab634-9aa2-11e6-bb20-00ff12b862e5',
                '9f8abff3-9aa2-11e6-bb20-00ff12b862e5'
                );
        }else if($user_role == "staff") {
            $pages_array = array(
             '1405f347-b919-11e5-a956-eca86bfd5e5e',
             'fca4008d-b917-11e5-a956-eca86bfd5e5e'
             );
        }else if($user_role == "Owner") {
            $pages_array = array(    
                '0aebd7a2-b854-11e5-acd6-eca86bfd5e5e',
                '0aebe6cc-b854-11e5-acd6-eca86bfd5e5e',
                '0fcb00e9-3b69-11e6-97da-fcaa14e27a26',
                '0fcb0961-3b69-11e6-97da-fcaa14e27a26',
                '1405e8c2-b919-11e5-a956-eca86bfd5e5e',
                '1405f347-b919-11e5-a956-eca86bfd5e5e',
                '1a6c05a3-b918-11e5-a956-eca86bfd5e5e',
                '217d09fe-3b69-11e6-97da-fcaa14e27a26',
                '224d749e-b919-11e5-a956-eca86bfd5e5e',
                '2ae89e9b-47fb-11e6-8bca-eca86bfd5e5e',
                '2eb02d83-b918-11e5-a956-eca86bfd5e5e',
                '2eb037d9-b918-11e5-a956-eca86bfd5e5e',
                '2fa1defa-b919-11e5-a956-eca86bfd5e5e',
                '2fa1e8b8-b919-11e5-a956-eca86bfd5e5e',
                '3639c24e-b915-11e5-a956-eca86bfd5e5e',
                '3639cc1f-b915-11e5-a956-eca86bfd5e5e',
                '3c2fa846-b919-11e5-a956-eca86bfd5e5e',
                '3ce19d14-b918-11e5-a956-eca86bfd5e5e',
                '3ce1a7a3-b918-11e5-a956-eca86bfd5e5e',
                '45811804-b915-11e5-a956-eca86bfd5e5e',
                '4581221c-b915-11e5-a956-eca86bfd5e5e',
                '5ec46626-b915-11e5-a956-eca86bfd5e5e',
                '5ec4704b-b915-11e5-a956-eca86bfd5e5e',
                '655fa8e4-b914-11e5-a956-eca86bfd5e5e',
                '655fb2c5-b914-11e5-a956-eca86bfd5e5e',
                '6b2d56a7-3b88-11e6-b7d1-fcaa14ede82b',
                '6f0836cd-b919-11e5-a956-eca86bfd5e5e',
                '6f93de54-3b84-11e6-b7d1-fcaa14ede82b',
                '7898c1b6-b914-11e5-a956-eca86bfd5e5e',
                '7898cc77-b914-11e5-a956-eca86bfd5e5e',
                '7d11f248-47fe-11e6-8bca-eca86bfd5e5e',
                '7d11fd62-47fe-11e6-8bca-eca86bfd5e5e',
                '86a31d34-b914-11e5-a956-eca86bfd5e5e',
                '86a327cb-b914-11e5-a956-eca86bfd5e5e',
                '88305c46-b918-11e5-a956-eca86bfd5e5e',
                '93cd6437-3b84-11e6-b7d1-fcaa14ede82b',
                '95f4cc9f-3aac-11e6-9e9d-eca86bfd5e5e',
                '9f592a42-3b67-11e6-97da-fcaa14e27a26',
                '9f593210-3b67-11e6-97da-fcaa14e27a26',
                'a28ae09e-b918-11e5-a956-eca86bfd5e5e',
                'a28aea7f-b918-11e5-a956-eca86bfd5e5e',
                'a50a38a0-3b84-11e6-b7d1-fcaa14ede82b',
                'aa816b31-b914-11e5-a956-eca86bfd5e5e',
                'aa81750e-b914-11e5-a956-eca86bfd5e5e',
                'afb34a71-b919-11e5-a956-eca86bfd5e5e',
                'b5adc846-b914-11e5-a956-eca86bfd5e5e',
                'b5add3a8-b914-11e5-a956-eca86bfd5e5e',
                'bae53b58-b919-11e5-a956-eca86bfd5e5e',
                'bae5460f-b919-11e5-a956-eca86bfd5e5e',
                'bd7ff43c-b917-11e5-a956-eca86bfd5e5e',
                'c0ab75f6-d61f-11e5-a98f-eca86bfd5e5e',
                'cc4cfcdd-b918-11e5-a956-eca86bfd5e5e',
                'cc4d071e-b918-11e5-a956-eca86bfd5e5e',
                'cc97c70b-b917-11e5-a956-eca86bfd5e5e',
                'd7d8f8fe-b918-11e5-a956-eca86bfd5e5e',
                'dbfd8142-3b67-11e6-97da-fcaa14e27a26',
                'e5b55f5d-b914-11e5-a956-eca86bfd5e5e',
                'e5b56a23-b914-11e5-a956-eca86bfd5e5e',
                'ea0c38a5-3aad-11e6-9e9d-eca86bfd5e5e',
                'ee571ed9-b917-11e5-a956-eca86bfd5e5e',
                'ee5728d6-b917-11e5-a956-eca86bfd5e5e',
                'f13672c6-b914-11e5-a956-eca86bfd5e5e',
                'f1367d6d-b914-11e5-a956-eca86bfd5e5e',
                'f43d29e1-47fe-11e6-8bca-eca86bfd5e5e',
                'fca3f6d2-b917-11e5-a956-eca86bfd5e5e',
                'fca4008d-b917-11e5-a956-eca86bfd5e5e',
                'fdc2e52a-b914-11e5-a956-eca86bfd5e5e',
                '9f8ab634-9aa2-11e6-bb20-00ff12b862e5',
                '9f8abff3-9aa2-11e6-bb20-00ff12b862e5',
                '087216a3-2fd4-11e7-8610-1c1b0d31c7c1',
                '087221b5-2fd4-11e7-8610-1c1b0d31c7c1',
                '23e82ab1-2fd4-11e7-8610-1c1b0d31c7c1',
                '49367607-2fea-11e7-8610-1c1b0d31c7c1'
                );
}

foreach ($pages_array as $key => $value) {
    $this->db->select('UUID()');
    $data = $this->db->get()->row_array();
    $pdata['pages_permission_id'] = $data['UUID()'];
    $pdata['user_id'] = $added_user_id;
    $pdata['pages_id'] = $value;
    $pdata['user_id_permitted_by'] = $this->session->userdata('user_id');

    $this->db->insert('pages_permission', $pdata);
}
         // return $this->db->insert_batch('pages_permission', $pdata);
}


    /**
     * user_info_by_id method is for getting user info individually
     * 
     * @return array
     * @author Musabbir 
     **/
    public function user_info_by_id($user_id)
    {

     $this->db->select('image,user_id,username,user_role,phone_1,phone_2,present_address,permanent_address,email');
     $this->db->from('users');
     $this->db->where('status', 'activated');
     $this->db->where('user_id', $user_id);
     return $this->db->get()->row_array();
 }


    /**
     * update_user_info method is for updating a user info individually
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function update_user_info($user_id,$data)
    {
     return    $this->db->where('user_id', $user_id)->update('users', $data);
 }


    /**
     * delete_user_info method is for delete a user individually. 
     * Basically we don't delete a user permenantly but change his status deactivated
     * 
     * @return null
     * @author Musabbir 
     **/
    public function delete_user_info($user_id)
    {
     return    $this->db->set('status', 'deactivated')->where('user_id', $user_id)->update('users');
 }


    /**
     * myProfile_info method is for show users own information on my profile
     * 
     * @return null
     * @author Musabbir 
     **/
    public function myProfile_info($user_id)
    {
     $this->db->select('image,user_id,username,phone_1,phone_2,present_address,permanent_address,email');
     $this->db->from('users');
     $this->db->where('status', 'activated');
     $this->db->where('user_id', $user_id);
     $result=  $this->db->get()->row();
     return  $result;
 }


    /**
     * updateMyProfile method is for update a user information by his own 
     * 
     * @return null
     * @author Musabbir 
     **/
    public function updateMyProfile($user_id,$data)
    {
     $this->db->where('user_id', $user_id);

     if($this->db->update('users', $data)) {
        return true;
    }else{
        return false;
    }
}


    /**
     * updatePass method is for update a user passwoed by his own 
     * 
     * @return null
     * @author Musabbir 
     **/
    public function updatePass($user_id, $new_pass)
    {
     $data = array(
      'password' => $new_pass
      );

     $this->db->where('user_id', $user_id);

     if($this->db->update('users', $data)) {
        return "success";
    }else{
        return "fail";
    }
}


    /**
     * getOldPass method is for get a users old passwoed for check on his password change option 
     * 
     * @return null
     * @author Musabbir 
     **/
    public function getOldPass($user_id)
    {
     $this->db->select('password');
     $this->db->from('users');
     $this->db->where('user_id', $user_id);

     $result=  $this->db->get()->row();
     return  $result;
 }

}

/* End of file User_Model.php */
/* Location: ./application/models/User_Model.php */