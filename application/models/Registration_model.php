<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Registration_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();

    }

    public function price_list_info()
    {

        $this->db->select('*');
        $this->db->from('price_list');
        return $this->db->get()->result_array();
    }

}

/* End of file Registration_model */
/* Location: ./application/models/Registration_model */