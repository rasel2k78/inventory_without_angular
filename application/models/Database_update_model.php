<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Database_update_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	public function get_db_version()
	{
		return $this->db->select('*')->from('db_version')->get()->result_array();
	}
	public function insert_db_version($version_data)
	{
		$this->db->insert('db_version', $version_data);
	}
}

/* End of file Database_update_model.php */
/* Location: ./application/models/Database_update_model.php */
?>