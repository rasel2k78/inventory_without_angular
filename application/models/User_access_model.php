<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class User_access_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();
        $this->load->database("inventory");
    }

 /**
     * @author Musabbir 
     **/
 public function get_user_role_by_id ($user_id)
 {
    $this->db->select('user_role');
    $this->db->from('users');
    $this->db->where('user_id', $user_id);
    return $this->db->get()->row_array();
}

    /**
     * all_users_info method is for getting all user info inside data-table on User access page
     * 
     * @return null
     * @author Musabbir 
     **/
    public function all_users_info($filters,$total_count=false)
    {

        $this->db->select(
            '
            username as "0",
            user_role as "1",
            '
            );
        $this->db->select(
            'concat(
            \'<button style="display: none"  name="edit_\',username,\'"></button>\',
            \'<button class="btn btn-sm green edit_user" id="edit_\',user_id,\'">Edit Access</button>\') as "2"', false
            );

        $this->db->from('users');
        $this->db->where('status', 'activated');

        if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
            $where ="(";
            $where.= "username like '%".$filters['search']['value']."%'or ";
            $where.= "phone_1 like '%".$filters['search']['value']."%'";
            $where.= ")";
            /*
            */
            $this->db->where($where, null, false);
        }

        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
            $this->db->order_by('username', 'asc');
        }
        if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
            $this->db->order_by('username', 'desc');
        }

        if ($total_count) {
            return $this->db->get()->num_rows();            
        } else {
            $this->db->limit($filters['length'], $filters['start']);
            $q = $this->db->get();
            $this->db->last_query();
            $a = $q->result_array();
            return $a;
        }

    }



    /**
     * get_page_access_by_user_id method is for getting page access permission by user_id
     * 
     * @return permitted page array for a particular user
     * @author Musabbir 
     **/
    public function get_page_access_by_user_id($user_id)
    {
        $this->db->select('users.user_id, users.username, pages_permission.`pages_id`, pages.pages_group');
        $this->db->from('pages_permission');
        $this->db->join('users', 'users.user_id = pages_permission.user_id', 'left');
        $this->db->join('pages', 'pages.pages_id = pages_permission.pages_id', 'left');
        $this->db->where('pages_permission.user_id', $user_id);
        $data = $this->db->get();
        return $data->result_array();
    }


    /**
     * get_page_details_by_page method is for showing all checkboxes by page id 
     * 
     * @return page details array
     * @author Rasel 
     **/
    public function get_page_details_by_page($page)
    {
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->where('pages_group', $page);
        $data = $this->db->get();
        return $data->result_array();
    }


    /**
     * get_page_name_and_actions method is for getting pages_group
     * 
     * @return array of pages address
     * @author Rasel 
     **/
    public function get_page_name_and_actions()
    {
       $this->db->select('pages_group,count(pages_group)');
       $this->db->from('pages');
       $this->db->group_by('pages_group');
       $this->db->order_by('pages_group', 'asc');
       $data = $this->db->get();
       return $data->result_array();
   }



    /**
     * set_user_access method is for setting page access for an user
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function set_user_access($edit_user_id,$page_id,$user_id_permitted_by)
    {
       $this->db->select('UUID()');
       $data_pp_id = $this->db->get()->row_array();
       $pages_permission_id = $data_pp_id['UUID()'];

       $data = array(
          'pages_permission_id' => $pages_permission_id, 
          'user_id' => $edit_user_id, 
          'pages_id' => $page_id,
          'user_id_permitted_by' => $user_id_permitted_by);
       return $this->db->insert('pages_permission', $data);
   }


    /**
     * add_or_remove_on_page_permission_history method is for keep history when a user get access or removed for access
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function add_or_remove_on_page_permission_history($page_id,$edit_user_id,$user_id_permitted_by,$action_taken)
    {
       $this->db->select('UUID()');
       $data_pp_id = $this->db->get()->row_array();
       $pages_permission_history_id = $data_pp_id['UUID()'];

       $data = array(
          'pages_permission_history_id' => $pages_permission_history_id, 
          'user_id' => $edit_user_id, 
          'pages_id' => $page_id,
          'user_id_permitted_by' => $user_id_permitted_by,
          'action' => $action_taken);
       return $this->db->insert('pages_permission_history', $data);
   }


    /**
     * remove_user_access method is for removing page access for an user
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function remove_user_access($edit_user_id,$page_id)
    {
       $this->db->where('user_id', $edit_user_id);
       $this->db->where('pages_id', $page_id);
       return $this->db->delete('pages_permission'); 
   }
}

/* End of file User_access_model.php */
/* Location: ./application/models/User_access_model.php */