<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance_sell_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_and_short_unique_id()
	{
		$this->db->select('UUID_short()', false);    
		$uuid =$this->db->get()->row_array();
		return $uuid['UUID_short()'];
	}

	public function save_adv_sell_basic_info($ready_data_basic)
	{

		$this->db->select('UUID()', false);    
		$uuid =$this->db->get()->row_array();

		$ready_data_basic['adv_sell_details_id']=$uuid['UUID()'];
		$this->db->insert('adv_sell_details', $ready_data_basic);
         // return $this->db->insert_id();
		return $uuid['UUID()'];
	}

	public function save_adv_sell_item_info($data,$sell_details_uuid)
	{

		foreach ($data as $key => $value) {
			$this->db->select('UUID()', false);    
			$uuid =$this->db->get()->row_array();
			unset($data[$key]['item_name']);
			unset($data[$key]['product_warranty']);
			unset($data[$key]['actual_quantity']);
			unset($data[$key]['imei_used']);
			unset($data[$key]['temp_id_info']);
			$data[$key]['adv_sell_details_id'] = $sell_details_uuid;
			$data[$key]['user_id'] = $this->session->userdata('user_id');    
			$data[$key]['adv_sell_cart_items_id']=$uuid['UUID()'];

		}
		return $this->db->insert_batch('adv_sell_cart_items', $data);

	}

	public function save_cash_or_card_payment_info($payment_method_details)
	{
		$this->db->select('UUID()');    
		$uuid =$this->db->get()->row_array();
		$payment_method_details['sell_payment_details_id'] = $uuid['UUID()'];
		$this->db->insert('sell_payment_details', $payment_method_details);
	}

	public function final_cashbox_amount_info()
	{

		$this->db->select('total_cashbox_amount');
		$this->db->from('final_cashbox_amount');
         // $this->db->where('items_id', $item);
		$qry_get = $this->db->get();
		return $qry_get->row_array();
	}

	public function update_total_cashbox_info($final_data)
	{

		$this->db->update('final_cashbox_amount', $final_data);
	}

	public function save_cash_box_info($data)
	{

		$this->db->select('UUID()', false);    
		$uuid =$this->db->get()->row_array();

		$data['cashbox_id']=$uuid['UUID()'];
		$this->db->insert('cashbox', $data);
		return $uuid['UUID()'];

	}

	public function get_current_bank_amt_info($bank_acc_id)
	{
		$this->db->select('final_amount', FALSE);
		$this->db->from('bank_accounts');
		$this->db->where('bank_acc_id', $bank_acc_id);
		return $this->db->get()->row_array();
	}

	public function save_card_payment_info($data)
	{
		$this->db->select('UUID()', false);    
		$uuid =$this->db->get()->row_array();
		$data['bank_statement_id']=$uuid['UUID()'];
		$this->db->insert('bank_statement', $data);
		return $uuid['UUID()'];
	}

	public function update_total_amt_info_of_acc($final_amount,$bank_acc_id)
	{
		return $this->db->where('bank_acc_id', $bank_acc_id)->update('bank_accounts', $final_amount);
	}


	public function get_customer_info_for_voucher($customers_id)
	{
		$this->db->select('*', FALSE);
		$this->db->from('customers');
		$this->db->where('customers_id', $customers_id);
		return  $this->db->get()->row_array();
	}

	public function collect_voucher_number($sell_details_uuid)
	{
		$this->db->select('sell_local_voucher_no,vat_amount,net_payable,sub_total,tax_percentage,discount,paid,due,customers_id,discount_percentage,grand_total,voucher_unique_barcode', FALSE);
		$this->db->from('adv_sell_details');
		$this->db->where('adv_sell_details_id', $sell_details_uuid);
		return $this->db->get()->row_array();
	}

	public function update_adv_sell_status($adv_sell_id)
	{
		$this->db->set('delivery_status', 'done')->where('adv_sell_details_id', $adv_sell_id)->update('adv_sell_details');
		// return $this->db->affected_rows();
	}

	public function search_item($text)
	{
		$this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,item_spec_set.item_spec_set_image,item_spec_set.unique_barcode');
		$this->db->from('item_spec_set');
		$this->db->where('publication_status', 'activated');
		$this->db->like('spec_set', $text, 'both');
		$this->db->limit(10);
		return $this->db->get()->result_array();
	}
}

/* End of file Advance_sell_model.php */
/* Location: ./application/models/Advance_sell_model.php */