<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ItemSearchModel extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

 /**
     * To get all category
     * 
     * @return array
     * @author Musabbir 
     **/
 public function allCatgoryForDataTable($filters,$total_count=false){
 	$this->db->select('
 		categories_name as "0",
 		categories_description as "1",
 		');
 	$this->db->select('concat(\'<button class="btn btn-xs green get_item_by_cat" data-toggle="modal" data-target="#editCategoryModal" id="edit_\',categories_id,\'">Show items</button>\') as "2"', FALSE);


 	$this->db->from('categories');

 	if($filters['search']['value'] !="" && $filters['search']['value'] != null)
 	{

 		$where ="(";
 		$where.= "categories_name like '%".$filters['search']['value']."%'or ";
 		$where.= "categories_description like '%".$filters['search']['value']."%'";
 		$where.= ")";

 		$this->db->where($where, NULL, FALSE);

 	}

 	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
 		$this->db->order_by('categories_name', 'asc');
 	}
 	if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
 		$this->db->order_by('categories_name', 'desc');
 	}


 	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc'){
 		$this->db->order_by('categories_description', 'asc');
 	}
 	if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc'){
 		$this->db->order_by('categories_description', 'desc');
 	}

  $this->db->where("publication_status",'activated');

  if ($total_count) {
   return $this->db->get()->num_rows();			
} else {
   $this->db->limit($filters['length'], $filters['start']);
   $q = $this->db->get();

   $a = $q->result_array();
   return $a;
}

}

 /**
     * To get item list by category
     * 
     * @return array
     * @author Musabbir 
     **/
 public function allItemsByCategory($filters,$total_count=false){

 	$id_of_cat = $filters['id_cat'];

  $this->db->select('inventory.quantity as "1",item_spec_set.spec_set as "0"');

  $this->db->select('(SELECT buying_price FROM `buy_cart_items` WHERE item_spec_set_id = item_spec_set.item_spec_set_id AND publication_status = "activated" ORDER BY date_created DESC LIMIT 1) AS "2" ', FALSE);

  $this->db->select('(SELECT whole_sale_price FROM `buy_cart_items` WHERE item_spec_set_id = item_spec_set.item_spec_set_id AND publication_status = "activated" ORDER BY date_created DESC LIMIT 1) AS "4" ', FALSE);

  $this->db->select('(SELECT retail_price FROM `buy_cart_items` WHERE item_spec_set_id = item_spec_set.item_spec_set_id AND publication_status = "activated" ORDER BY date_created DESC LIMIT 1) AS "3"', FALSE);

  $this->db->from('inventory');
  $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'right');
  $this->db->join('buy_cart_items', 'buy_cart_items.item_spec_set_id = inventory.item_spec_set_id', 'left');
  $this->db->join('items', 'items.items_id = item_spec_set.items_id', 'inner');
  $this->db->group_by('item_spec_set.item_spec_set_id');
  $this->db->where('items.categories_id', $id_of_cat);
  $this->db->where('item_spec_set.publication_status', "activated");

  if($filters['search']['value'] !="" && $filters['search']['value'] != null)
  {
   $where ="(";
   $where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
   $where.= ")";
   $this->db->where($where, NULL, FALSE);
}

if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc'){
   $this->db->order_by('item_spec_set.spec_set', 'asc');
}

if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc'){
   $this->db->order_by('item_spec_set.spec_set', 'desc');
}

if ($total_count) {
   return $this->db->get()->num_rows();			
} else {
   $this->db->limit($filters['length'], $filters['start']);
   $q = $this->db->get();
// echo $this->db->last_query();
// exit();
   $a = $q->result_array();
   return $a;
}

}

}

/* End of file  */
/* Location: ./application/models/ */
