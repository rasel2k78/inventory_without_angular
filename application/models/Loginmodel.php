<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Loginmodel extends CI_Model
{

  public $variable;

  public function __construct()
  {
    parent::__construct();
    $this->load->database("inventory");

  }


  public function getLang()
  {
    $this->db->select('name');
    $this->db->from('language');
    $this->db->where('language_id', 1);
    $query = $this->db->get();
    $result = $query->row();
    return $result->name;  
  }


  public function changeLang($value)
  {
    $data['name'] = $value;
    return $this->db->update('language', $data);
  }

    /**
     * firstTimeLogin method is creating user for the very first time
     * 
     * @return string
     * @author Musabbir 
     **/
    public function firstTimeLogin($owner_name,$owner_email,$local_pin)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();

     $login_data = array(
      'user_id' => $uuid['UUID()'],
      'username' => $owner_name,
      'email' => $owner_email,
      'password' => $local_pin
      );

     return $this->db->insert('users', $login_data);

   }


   public function getOwnerInfo($email)
   {

     $this->db->select('user_id, username, email, user_role');
     $this->db->from('users');
     $this->db->where('email', $email);
     $ownerInfo = $this->db->get()->row_array();
     return $ownerInfo;
   }






   public function setAllAccessForOwner($ownerId)
   {
     $pages_array = array(
      '0aebd7a2-b854-11e5-acd6-eca86bfd5e5e',
      '0aebe6cc-b854-11e5-acd6-eca86bfd5e5e',
      '0fcb00e9-3b69-11e6-97da-fcaa14e27a26',
      '0fcb0961-3b69-11e6-97da-fcaa14e27a26',
      '1405e8c2-b919-11e5-a956-eca86bfd5e5e',
      '1405f347-b919-11e5-a956-eca86bfd5e5e',
      '1a6c05a3-b918-11e5-a956-eca86bfd5e5e',
      '217d09fe-3b69-11e6-97da-fcaa14e27a26',
      '224d749e-b919-11e5-a956-eca86bfd5e5e',
      '2ae89e9b-47fb-11e6-8bca-eca86bfd5e5e',
      '2eb02d83-b918-11e5-a956-eca86bfd5e5e',
      '2eb037d9-b918-11e5-a956-eca86bfd5e5e',
      '2fa1defa-b919-11e5-a956-eca86bfd5e5e',
      '2fa1e8b8-b919-11e5-a956-eca86bfd5e5e',
      '3639c24e-b915-11e5-a956-eca86bfd5e5e',
      '3639cc1f-b915-11e5-a956-eca86bfd5e5e',
      '3c2fa846-b919-11e5-a956-eca86bfd5e5e',
      '3ce19d14-b918-11e5-a956-eca86bfd5e5e',
      '3ce1a7a3-b918-11e5-a956-eca86bfd5e5e',
      '45811804-b915-11e5-a956-eca86bfd5e5e',
      '4581221c-b915-11e5-a956-eca86bfd5e5e',
      '5ec46626-b915-11e5-a956-eca86bfd5e5e',
      '5ec4704b-b915-11e5-a956-eca86bfd5e5e',
      '655fa8e4-b914-11e5-a956-eca86bfd5e5e',
      '655fb2c5-b914-11e5-a956-eca86bfd5e5e',
      '6b2d56a7-3b88-11e6-b7d1-fcaa14ede82b',
      '6f0836cd-b919-11e5-a956-eca86bfd5e5e',
      '6f93de54-3b84-11e6-b7d1-fcaa14ede82b',
      '7898c1b6-b914-11e5-a956-eca86bfd5e5e',
      '7898cc77-b914-11e5-a956-eca86bfd5e5e',
      '7d11f248-47fe-11e6-8bca-eca86bfd5e5e',
      '7d11fd62-47fe-11e6-8bca-eca86bfd5e5e',
      '86a31d34-b914-11e5-a956-eca86bfd5e5e',
      '86a327cb-b914-11e5-a956-eca86bfd5e5e',
      '88305c46-b918-11e5-a956-eca86bfd5e5e',
      '93cd6437-3b84-11e6-b7d1-fcaa14ede82b',
      '95f4cc9f-3aac-11e6-9e9d-eca86bfd5e5e',
      '9f592a42-3b67-11e6-97da-fcaa14e27a26',
      '9f593210-3b67-11e6-97da-fcaa14e27a26',
      'a28ae09e-b918-11e5-a956-eca86bfd5e5e',
      'a28aea7f-b918-11e5-a956-eca86bfd5e5e',
      'a50a38a0-3b84-11e6-b7d1-fcaa14ede82b',
      'aa816b31-b914-11e5-a956-eca86bfd5e5e',
      'aa81750e-b914-11e5-a956-eca86bfd5e5e',
      'afb34a71-b919-11e5-a956-eca86bfd5e5e',
      'b5adc846-b914-11e5-a956-eca86bfd5e5e',
      'b5add3a8-b914-11e5-a956-eca86bfd5e5e',
      'bae53b58-b919-11e5-a956-eca86bfd5e5e',
      'bae5460f-b919-11e5-a956-eca86bfd5e5e',
      'bd7ff43c-b917-11e5-a956-eca86bfd5e5e',
      'c0ab75f6-d61f-11e5-a98f-eca86bfd5e5e',
      'cc4cfcdd-b918-11e5-a956-eca86bfd5e5e',
      'cc4d071e-b918-11e5-a956-eca86bfd5e5e',
      'cc97c70b-b917-11e5-a956-eca86bfd5e5e',
      'd7d8f8fe-b918-11e5-a956-eca86bfd5e5e',
      'dbfd8142-3b67-11e6-97da-fcaa14e27a26',
      'e5b55f5d-b914-11e5-a956-eca86bfd5e5e',
      'e5b56a23-b914-11e5-a956-eca86bfd5e5e',
      'ea0c38a5-3aad-11e6-9e9d-eca86bfd5e5e',
      'ee571ed9-b917-11e5-a956-eca86bfd5e5e',
      'ee5728d6-b917-11e5-a956-eca86bfd5e5e',
      'f13672c6-b914-11e5-a956-eca86bfd5e5e',
      'f1367d6d-b914-11e5-a956-eca86bfd5e5e',
      'f43d29e1-47fe-11e6-8bca-eca86bfd5e5e',
      'fca3f6d2-b917-11e5-a956-eca86bfd5e5e',
      'fca4008d-b917-11e5-a956-eca86bfd5e5e',
      'fdc2e52a-b914-11e5-a956-eca86bfd5e5e',
      '9f8ab634-9aa2-11e6-bb20-00ff12b862e5',
      '9f8abff3-9aa2-11e6-bb20-00ff12b862e5',
      '087216a3-2fd4-11e7-8610-1c1b0d31c7c1',
      '087221b5-2fd4-11e7-8610-1c1b0d31c7c1',
      '23e82ab1-2fd4-11e7-8610-1c1b0d31c7c1',
      '49367607-2fea-11e7-8610-1c1b0d31c7c1',
      '7192b80f-d33b-11e7-b235-fcaa14ede82b'
      );


     foreach ($pages_array as $key => $value) {
      $this->db->select('UUID()');
      $data = $this->db->get()->row_array();
      $pdata['pages_permission_id'] = $data['UUID()'];
      $pdata['user_id'] = $ownerId;
      $pdata['pages_id'] = $value;
      $pdata['user_id_permitted_by'] = "default";

      $this->db->insert('pages_permission', $pdata);
    }
  }


    /**
     * login method is for allow/deny a user to use the software
     * user can login using email or phone_1 
     * 
     * @return string
     * @author Musabbir 
     **/
    public function login($email, $password)
    {
      $this->db->select('*');
      $this->db->from('users');
      $this->db->where('password', $password);
      $this->db->where('status', "activated");
      $this->db->group_start();
      $this->db->where('email', $email);
      $this->db->or_where('phone_1', $email);
      $this->db->group_end();

      return $this->db->get()->row_array();
    }


    public function checkEmptyUser()
    {
     $this->db->select('user_id');
     $this->db->from('users');
     $q = $this->db->get();
     return $q->row_array();
   }



    /**
     * select_all_user_username method is for select all username to check when a new user is created
     * 
     * @return string
     * @author Musabbir 
     **/
    public function select_all_user_username()
    {
     $this->db->select('users.username');
     $this->db->from('users');
     $q = $this->db->get();
     return $q->result_array();
   }

    /**
     * createAccount method is for adding a new user in the software
     * 
     * @return string
     * @author Musabbir 
     **/
    public function createAccount($udata)
    {

     $this->db->set('user_id', 'UUID()', false);
     return $this->db->insert('users', $udata);
   }


    /**
     * checkMailForForgetPass method is for checking a mail that it exist or not
     * 
     * @return string
     * @author Musabbir 
     **/
    public function checkMailForForgetPass($email)
    {
     $this->db->where('email', $email);
     $query = $this->db->get('users');
     if ($query->num_rows() > 0) {
      return true;
    }
    else{
      return false;
    }
  }


    /**
     * checkPhoneForForgetPass method is for checking a phone no. that it exist or not
     * 
     * @return string
     * @author Musabbir 
     **/
    public function checkPhoneForForgetPass($phone)
    {
     $this->db->where('phone_1', $phone);
     $query = $this->db->get('users');
     if ($query->num_rows() > 0) {
      return true;
    }
    else{
      return false;
    }
  }



    /**
     * createNewPass method is for creating a new random password if he request on forget password and request via email
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function createNewPass($email,$new_pass)
    {
     $p_data_new = array(
      'password' => $new_pass
      );
     return    $this->db->where('email', $email)->update('users', $p_data_new);
   }



    /**
     * createNewPassForPhone method is for creating a new random password if he request on forget password and request via phone
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function createNewPassForPhone($phone,$new_pass)
    {
     $p_data_new = array(
      'password' => $new_pass
      );
     return    $this->db->where('phone_1', $phone)->update('users', $p_data_new);
   }


    /**
     * getUserPass method is for get the password of an user to send it to his mail via email
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function getUserPass($email)
    {
     $this->db->select('users.password');
     $this->db->from('users');
     $this->db->where('email', $email);
     $q = $this->db->get();
     return $q->row_array('password');
   }


    /**
     * getUserPass method is for get the password of an user to send it to his mail via phone
     * 
     * @return true/false
     * @author Musabbir 
     **/
    public function getUserPassByPhone($phone)
    {
     $this->db->select('users.password');
     $this->db->from('users');
     $this->db->where('phone_1', $phone);
     $q = $this->db->get();
     return $q->row_array('password');
   }

   public function insert_token($token_data)
   {
     return $this->db->insert('temporary_tokens', $token_data);
   }

   public function get_token_details($token)
   {
     return $this->db->select('*')->from('temporary_tokens')->where('token', $token)->get()->result_array();
   }

   public function get_id_from_token($data)
   {
     $this->db->select('*');
     $this->db->from('temporary_tokens');
     $this->db->where('temporary_tokens.token', $data);
     $query = $this->db->get();
     return $query->result_array();
   }

   public function reset_password($p_data)
   {

     $this->change_token_status($p_data);

     $p_data_new = array(
      'user_id' => $p_data['user_id'],
      'password' => $p_data['password'],
      'date_updated' => Date('Y-m-d H:i:s')
      );

     $this->db->where('user_id', $p_data_new['user_id']);
     $this->db->update('users', $p_data_new);

          return $this->db->affected_rows();   //This line is importatnt for the confirmation of data update.
        }

        public function change_token_status($p_data)
        {
          $status = array('status' => 1);
          $this->db->where('token', $p_data['token']);
          $this->db->update('temporary_tokens', $status);
        }


        public function insertStoreData($shop_id,$shop_name,$shop_address,$vat_reg_no)
        {

          $shop_data = array(
           'shop_id' => $shop_id,
           'name' => $shop_name,
           'address' => $shop_address,
           'vat_reg_no' => $vat_reg_no,
           'type' => 'store'
           );
          return $this->db->insert('shop', $shop_data);
        }

        public function edit_store_id_format($table,$store_id)
        {
         $this->db->query(" ALTER TABLE $table CHANGE `stores_id` `stores_id` CHAR(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '$store_id' ");
       }

       public function edit_store_id($table,$store_id)
       {
         $this->db->query("UPDATE $table SET stores_id ='$store_id'");
       }

       public function set_folder_name($folder_name)
       {
         $this->db->set('folder_name', $folder_name); 
         $this->db->insert('account_folder');
       }

       public function get_table_name()
       {

        return  $this->db->query("select table_name from information_schema.tables where TABLE_SCHEMA='inventory'")->result_array();
      }

      public function get_printer_info()
      {
        $this->db->select('active_printer');
        $this->db->from('shop');
        return $this->db->get()->row_array();
      }

    }

    /* End of file LoginModel.php */
/* Location: ./application/models/LoginModel.php */