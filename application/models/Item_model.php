<?php 

/**
 * Item Model includes Brand,Unit,Lot-size, Size,Vendor,Category,Lot-size,Item controller.
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 */

if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Use this Model for brand controller, size controller, lot-size controller, unit controller, vendor controller, category controller.
 *
 * @subpackage Model
 * @author     shoaib <shofik.shoaib@gmail.com>
**/


class Item_model extends CI_Model
{

	public $variable;

    /**
 * This is a constructor function
 * 
 * @return null
 * @author shoaib <shofik.shoaib@gmail.com>
    **/

    public function __construct()
    {
    	parent::__construct();

    }



    /**
 * find brands name,id from database
 *
 * @return array[]
 * @param  null
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function search_by_key($key)
    {
    	$sql = "Select DISTINCT(items_name) from items where items_name LIKE '%$key%' AND publication_status='activated' limit 8";
    	return $this->db->query($sql)->result_array();
    }

    public function check_if_inventory_exist($item_spec_set_id)
    {
    	$this->db->select('quantity,item_spec_set_id');
    	$this->db->from('inventory');
    	$this->db->where('item_spec_set_id', $item_spec_set_id);
    	return $this->db->get()->row_array();
    }
    public function search_category($text)
    {
    	$this->db->select('categories_name,categories_id');
    	$this->db->from('categories');
    	$this->db->like('categories_name', $text, 'both');
    	$this->db->where('publication_status', 'activated');
    	$this->db->limit(5);
    	return $this->db->get()->result_array();
    }

    public function search_category_by_key($key)
    {
   // $sql = "Select categories_name,categories_id from categories where categories_name LIKE '%$key%'";
   // return $this->db->query($sql)->result_array();
    	$this->db->select('categories_name,categories_id');
    	$this->db->from('categories');
    	$this->db->like('categories_name', $key, 'both');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }

    public function get_my_shop()
    {
    	return $this->db->get('shop')->row_array();
    }

    public function check_barcode_else_exist($barcode,$item_spec_set_id)
    {
    	$this->db->select('*');
    	$this->db->from('item_spec_set');
    	$this->db->where('barcode_number', $barcode);
    	$this->db->where('item_spec_set_id!=', $item_spec_set_id);
  //$this->db->where('publication_status', "activated");
    	return $this->db->get()->row_array();
    }

    public function check_barcode_if_exist($barcode)
    {
    	$this->db->select('*');
    	$this->db->from('item_spec_set');
    	$this->db->where('barcode_number', $barcode);
    	$this->db->where('publication_status', "activated");
    	return $this->db->get()->row_array();
    }
    public function check_barcode_if_exist_another($barcode,$item_spec_set_id)
    {
    	$this->db->select('*');
    	$this->db->from('item_spec_set');
    	$this->db->where('barcode_number', $barcode);
    	$this->db->where('item_spec_set_id !=', $item_spec_set_id);
    	return $this->db->get()->row_array();
    }

    public function check_category_by_id($category_id)
    {
    	$this->db->select('*');
    	$this->db->from('categories');
    	$this->db->where('categories_id', $category_id);
    	$this->db->where('parent_categories_id', $category_id);
    	return $this->db->get()->result_array();
    }


    public function check_category_exist($category_name)
    {
    	$this->db->select('categories_name,categories_id');
    	$this->db->from('categories');
    	$this->db->where('categories_name', $category_name);
  // $this->db->where('publication_status', 'deactivated');
  // $this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    public function check_if_category($category_name)
    {

    	$this->db->select('categories_name,categories_id');
    	$this->db->from('categories');
    	$this->db->where('categories_name', $category_name);
    	return $this->db->get()->row_array();
 // $sql = "Select categories_name,categories_id from categories where categories_name = '$category_name'";
 // return $this->db->query($sql)->row_array();
    }

    public function get_specs_by_search($text)
    {
    	$this->db->select('spec_name,spec_name_id');
    	$this->db->from('spec_name_table');
    	$this->db->where('publication_status', 'activated');
    	$this->db->like('spec_name', $text, 'both');
    	$this->db->order_by('spec_name', 'asc');
    	$this->db->limit(10);
    	return $this->db->get()->result_array();
    }

    public function get_spec_name_by_id($spec_name_id)
    {
    	$this->db->select('spec_name');
    	$this->db->from('spec_name_table');
    	$this->db->where('spec_name_id', $spec_name_id);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    public function check_if_value_exist($name)
    {
    	$this->db->select('spec_value,spec_value_id');
    	$this->db->from('spec_value');
    	$this->db->where('spec_value', $name);
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->row_array();
    }

    public function insert_spec_value($data)
    {
    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();
    	$data['spec_value_id']=$uuid['UUID()'];
    	$data['publication_status']='Activated';
    	$this->db->insert('spec_value', $data);
    	return $data['spec_value_id'];
    }

    public function all_specs_info()
    {
    	$this->db->select('*');
    	$this->db->from('spec_name_table');
    	$this->db->where('publication_status', 'activated');
    	$this->db->order_by('date_created', 'ASC');
    	return $this->db->get()->result_array();
    }

    /**
 * get all Category information from database with datatable Edit, Delete button
 *
 * @return array[]
 * @param  filters, $total_count
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function all_category_info_for_datatable($filters,$total_count=false)
    {
    	$this->db->select(
    		'
    		categories_name as "0"
    		'
       );

    	$this->db->select('concat(\'<button class="btn btn-xs green edit_category" id="edit_\',categories_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_category" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',categories_id,\'">Delete</button>\') as "1"', false);
    	$this->db->from('categories');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "categories_name like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('categories_name', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('categories_name', 'desc');
          }

          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }
    /**
 * get all item information with datatable 
 *
 * @return array[]
 * @param  filters, $total_count
 * @author Rasel <raselahmed2k7@gmail.com>
 **/


    public function all_item_info_for_datatable($filters,$total_count=false)
    {
    	$this->db->select('concat(\'<a class="view_item"  id="Item_\',item_spec_set.item_spec_set_id,\'">\',item_spec_set.spec_set,\'</a>\') as "0"', false);

    	$this->db->select('inventory.quantity as "1"');

    	$this->db->select('(SELECT retail_price FROM `buy_cart_items` WHERE item_spec_set_id = item_spec_set.item_spec_set_id AND publication_status = "activated" ORDER BY date_created DESC LIMIT 1) AS "2"', FALSE);

     // $this->db->select(
     //   '
     //   inventory.quantity as "1",
     //   '
     //   );

     // $this->db->select(
     //   '
     //   buy_cart_items.retail_price as "2",
     //   '
     //   );
          // $this->db->select('conca(<a class="btn btn-xs green">item_spec_set.spec_set as "0")', FALSE);
    	$this->db->select('concat(\'<button class="btn btn-xs green edit_item" id="edit_\',item_spec_set.item_spec_set_id,\'">Edit</button>\',\'<button class="btn btn-xs red delete_item" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',item_spec_set.item_spec_set_id,\'">Delete</button>\',\'<button class="btn btn-xs blue item_barcode" data-toggle="modal" href="#barcode_quantity_form" id="barcode_\',item_spec_set.item_spec_set_id,\'">Barcode</button>\',\'<button class="btn btn-xs green update_item_price"  id="price_\',item_spec_set.item_spec_set_id,\'">Update Price</button>\') as "3"', false);
         // $this->db->select('concat(\'<input type="checkbox" class="checkboxes item_checkbox" id="\',item_spec_set_id,\'" value="1"/>\') as "2"', FALSE);
    	$this->db->from('item_spec_set');
    	$this->db->join('inventory', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
    	$this->db->join('buy_cart_items', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
     $this->db->group_by('item_spec_set.item_spec_set_id');// add group_by
     $this->db->where('item_spec_set.publication_status', 'activated');

     if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
     	$where ="(";
     	$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
     	$where.= "item_spec_set.barcode_number like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
     	$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('item_spec_set.spec_set', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('item_spec_set.spec_set', 'desc');
          }

          if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('inventory.quantity', 'asc');
          }
          if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('inventory.quantity', 'desc');
          }    


          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }
    /**
 * get all vendor information with datatable
 *
 * @return array[]
 * @param  filters, $total_count
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function all_vendors_info_datatable($filters,$total_count=false)
    {


    	$this->db->select(
    		'
    		vendors_name as "0",
    		'
       );
    	$this->db->select('concat(\'<button class="btn btn-sm green edit_vendor" id="edit_\',vendors_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_vendor" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',vendors_id,\'">Delete</button>\') as "1"', false);
    	$this->db->from('vendors');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "vendors_name like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('vendors_name', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('vendors_name', 'desc');
          }

          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }
        
        public function update_spec_set($spec_set_id,$data)
        {
        	return    $this->db->where('item_spec_set_id', $spec_set_id)->update('item_spec_set', $data);
        }


    /**
 * Collect all sizes information from database 
 *
 * @return array[]
 * @link   Size/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_sizes_info()
    {

    	$this->db->select('sizes_id,sizes_name');
    	$this->db->from('sizes');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }


    /**
 * Collect individual size's information from database by id
 *
 * @return array[]
 * @link   Brand/get_size_info
 * @param  string $sizes_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function size_info_by_id($sizes_id)
    {

    	$this->db->select('sizes_name');
    	$this->db->from('sizes');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('sizes_id', $sizes_id);
    	return $this->db->get()->row_array();

    }


    /**
 * Collect all vendors information from database 
 *
 * @return array[]
 * @link   Vendor/index
 * @param  null
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function all_vendors_info()
    {

    	$this->db->select('vendors_id,vendors_name,vendors_present_address,vendors_permanent_address,vendors_phone_1,vendors_phone_2,vendors_national_id,vendors_tin,vendors_trade_license');
    	$this->db->from('vendors');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }

    /**
 * Collect individual vendor's information from database by id
 *
 * @return array[]
 * @link   Brand/get_vendor_info
 * @param  string $vendors_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function vendor_info_by_id($vendors_id)
    {

    	$this->db->select('vendors_name,vendors_present_address,vendors_permanent_address,vendors_phone_1,vendors_phone_2,vendors_national_id,vendors_tin,vendors_trade_license,vendor_image');
    	$this->db->from('vendors');
    	$this->db->where('publication_status', 'activated');
    	$this->db->where('vendors_id', $vendors_id);
    	return $this->db->get()->row_array();

    }

    /**
 * Update individual vendor's information of database by id
 *
 * @return boolean
 * @link   Brand/update_vendor_info
 * @param  string  $vendors_id Parameter-1
 * @param  array[] $data       Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_vendor_info($vendors_id,$data)
    {

    	$this->db->where('vendors_id', $vendors_id)->update('vendors', $data);
    	return $this->db->affected_rows();
    }

    /**
 * Delete/Hide vendor's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Brand/delete_vendor
 * @param  string $vendors_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_vendor_info($vendors_id)
    {

    	return    $this->db->set('publication_status', 'deactivated')->where('vendors_id', $vendors_id)->update('vendors');
    }

    /**
 * Save data of vendor from  in database 
 *
 * @return string
 * @link   Size/save_vendor_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_vendor_info($data)
    {

    	$this->db->select('UUID()', false);    
    	$uuid =$this->db->get()->row_array();

    	$data['vendors_id']=$uuid['UUID()'];
    	$this->db->insert('vendors', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Collect all units information from database with datatable
 *
 * @return array[]
 * @param  filters, $total_count
 * @author Rasel <raselahmed2k7@gmail.com>
 **/

    public function all_units_info($filters,$total_count=false)
    {
    	$this->db->select(
    		'
    		units_name as "0"
    		'
       );

    	$this->db->select('concat(\'<button class="btn btn-sm green edit_unit" id="edit_\',units_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_unit" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',units_id,\'">Delete</button>\') as "1"', false);
    	$this->db->from('units');
    	$this->db->where('publication_status', 'activated');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "units_name like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('units_name', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('units_name', 'desc');
          }

          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }




        public function all_colors_info($filters,$total_count=false)
        {
        	$this->db->select(
        		'
        		colors_name as "0"
        		'
           );

        	$this->db->select('concat(\'<button class="btn btn-sm green edit_color" id="edit_\',colors_id,\'">Edit</button>\',\'<button class="btn btn-sm red delete_color" data-toggle="modal" href="#responsive_modal_delete" id="delete_\',colors_id,\'">Delete</button>\') as "1"', false);
        	$this->db->from('colors');
        	$this->db->where('publication_status', 'activated');

        	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
        		$where ="(";
        		$where.= "colors_name like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
        		$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('colors_name', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('colors_name', 'desc');
          }

          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }


        public function delete_specs_by_spec_set_id($spec_set_id)
        {
        	return    $this->db->set('publication_status', 'deactivated')->where('item_spec_set_id', $spec_set_id)->update('spec_set_connector_with_values');
        }

    /**
 * Save data of unit from  in database 
 *
 * @return string
 * @link   Unit/save_unit_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function all_categories_info()
    {

    	$this->db->select('categories_id,parent_categories_id,categories_name,categories_description');
    	$this->db->from('categories');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }

    /**
 * Item function --> insert / delete / update
 *
 * @return void
 * @author Shoaib
 **/

    public function all_items_info()
    {

    	$this->db->select('item_spec_set_id,items_name,categories_id,brands_id,units_id,sizes_id,unique_id,lot_size_id,items_description');
    	$this->db->from('items');
    	$this->db->where('publication_status', 'activated');
    	return $this->db->get()->result_array();
    }

    /**
 * Save data of category from  in database 
 *
 * @return string
 * @link   Category/save_category_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_category_info($data)
    {

    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();

    	$data['categories_id']=$uuid['UUID()'];
    	$this->db->insert('categories', $data);
    	return $uuid['UUID()'];
    }

    /**
 * Collect individual category's information from database by id
 *
 * @return array[]
 * @link   Category/get_category_info
 * @param  string $categories_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function category_info_by_id($categories_id)
    {

    	$this->db->select('categories.categories_name,categories.categories_description,categories.parent_categories_id,c.categories_name as parent_name');
    	$this->db->from('categories');
    	$this->db->join('categories as c', 'c.categories_id = categories.parent_categories_id', 'left');
    	$this->db->where('categories.publication_status', 'activated');
    	$this->db->where('categories.categories_id', $categories_id);
    	return $this->db->get()->row_array();

    }

    public function get_item_by_barcode($barcode_number)
    {
    	$this->db->select('*');
    	$this->db->from('item_spec_set');
    	$this->db->where('barcode_number', $barcode_number);
    	$this->db->where('publication_status', "activated");
    	return $this->db->get()->row_array();
    }

    public function get_item_price($item_spec_set_id)
    {
    	$this->db->select('*');
    	$this->db->from('buy_cart_items');
    	$this->db->where('item_spec_set_id', $item_spec_set_id);
      $this->db->where('publication_status', 'activated');
      $this->db->order_by('date_created', "desc");
      $this->db->limit(1);
      return $this->db->get()->row_array();
    }


    public function check_spec_value_exist($item_spec_set_id,$spec_name_id)
    {
    	$this->db->select('*');
    	$this->db->from('spec_set_connector_with_values');
    	$this->db->where('item_spec_set_id', $item_spec_set_id);
    	$this->db->where('spec_name_id', $spec_name_id);
    	return $this->db->get()->row_array();
    }


    public function update_combination_spec_value($connector_id,$data)
    {
    	$this->db->where('spec_set_connector_id', $connector_id);
    	$this->db->update("spec_set_connector_with_values",$data);
    }


    public function deactivate_extra_spect_combination_values($item_spec_set_id,$spec_name_ids,$dataa)
    {
    	$this->db->where("item_spec_set_id",$item_spec_set_id);
    	$this->db->where_not_in('spec_name_id', $spec_name_ids);
    	$this->db->update('spec_set_connector_with_values',$dataa); 
   // echo $this->db->last_query();exit;
    }

    /**
 * Update individual category's information of database by id
 *
 * @return boolean
 * @link   Category/update_category_info
 * @param  string  $categories_id Parameter-1
 * @param  array[] $data          Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function update_category_info($categories_id,$data)
    {

    	$this->db->where('categories_id', $categories_id)->update('categories', $data);
    	return $this->db->affected_rows();
    }


    /**
 * Delete/Hide category's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   category/delete_category
 * @param  string $categories_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_category_info($categories_id)
    {

    	$this->db->set('publication_status', 'deactivated');
    	$this->db->where('categories_id',$categories_id);
    	$this->db->or_where('parent_categories_id',$categories_id);
    	return $this->db->update('categories');

    }


    /**
 * Save data of item from in database 
 *
 * @return null
 * @link   Item/save_item_info
 * @param  array[] $data
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_item_info($data)
    {

    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();
    	$data['items_id']=$uuid['UUID()'];
    	$this->db->insert('items', $data);
    	return $data['items_id'];
    }



    public function save_spec_set($data)
    {
    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();
    	$data['item_spec_set_id']=$uuid['UUID()'];
    	$this->db->insert('item_spec_set', $data);
    	return $data['item_spec_set_id'];
    }

    public function save_spec_set_connector($data)
    {
    	$this->db->select('UUID()');    
    	$uuid =$this->db->get()->row_array();
    	$data['spec_set_connector_id']=$uuid['UUID()'];
    	return $this->db->insert('spec_set_connector_with_values', $data);
    }
    /**
 * Delete/Hide item's information from database. It actually change "publication_status" from "activated" to "deactivated" 
 *
 * @return boolean
 * @link   Item/delete_item
 * @param  string $item_spec_set_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_item_info($item_spec_set_id)
    {

    	return    $this->db->set('publication_status', 'deactivated')->where('item_spec_set_id', $item_spec_set_id)->update('item_spec_set');
    	return    $this->db->set('publication_status', 'deactivated')->where('item_spec_set_id', $item_spec_set_id)->update('spec_set_connector_with_values');
    }


    /**
 * Collect individual item's information from database by id
 *
 * @return array[]
 * @link   Item/edit_item
 * @param  string $item_spec_set_id
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function item_info_by_id($item_spec_set_id)
    {
    	$this->db->select('items.items_id,items.items_name,items.items_description,items.categories_id,categories.categories_name,item_spec_set.item_spec_set_id,item_spec_set.item_spec_set_image,item_spec_set.barcode_number,item_spec_set.unique_barcode,item_spec_set.spec_set,item_spec_set.barcode_number,item_spec_set.product_warranty');
    	$this->db->from('items');
    	$this->db->join('categories', 'categories.categories_id = items.categories_id', 'left');
    	$this->db->join('item_spec_set', 'item_spec_set.items_id = items.items_id', 'left');
    	$this->db->where('item_spec_set.publication_status', 'activated');
    	$this->db->where('item_spec_set_id', $item_spec_set_id);
    	return $this->db->get()->row_array();
    }


    public function spec_set_by_id($spec_set_id)
    {
    	$this->db->select('spec_set_connector_with_values.item_spec_set_id,spec_name_table.spec_name,spec_name_table.spec_name_id,spec_value.spec_value');
    	$this->db->from('spec_set_connector_with_values');
    	$this->db->join('spec_name_table', 'spec_set_connector_with_values.spec_name_id = spec_name_table.spec_name_id', 'left');
    	$this->db->join('spec_value', 'spec_set_connector_with_values.spec_value_id = spec_value.spec_value_id', 'left');
    	$this->db->where('spec_set_connector_with_values.item_spec_set_id', $spec_set_id);
    	$this->db->where('spec_set_connector_with_values.publication_status', 'activated');
    	return $this->db->get()->result_array();


         // $this->db->select('unique_id', FALSE);
         // $this->db->from('spec_set_connector_with_values');
         // $this->db->where('item_spec_set_id', $item_spec_set_id);
         // return $this->db->get()->row_array();
    }

    /**
 * Update individual item's information of database by id
 *
 * @return boolean
 * @link   Item/update_item_info
 * @param  string  $item_spec_set_id Parameter-1
 * @param  array[] $data             Parameter-2
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_item_info($item_id,$data)
    {

    	return    $this->db->where('items_id', $item_id)->update('items', $data);
    }

    public function barcode_info_by_id($item_spec_set_id)
    {

    	$this->db->select('barcode_number', false);
    	$this->db->from('item_spec_set');
    	$this->db->where('item_spec_set_id', $item_spec_set_id);
    	return $this->db->get()->row_array();
    }

    /**
 * get all shortage quantity item information for dashboard 
 *
 * @link   Dashboard/shortage_item_info_for_dashboard
 * @return array[]
 * @param  filters, $total_count
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function shortage_item_info_for_dashboard($filters,$total_count=false)
    {
    	$this->db->select(
    		'
    		item_spec_set.spec_set as "0",
    		inventory.quantity as "1",
    		'
       );

         // $this->db->select('concat(\'<input type="checkbox" class="checkboxes item_checkbox" id="\',item_spec_set_id,\'" value="1"/>\') as "2"', FALSE);
    	$this->db->from('item_spec_set');
    	$this->db->join('inventory', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
    	$this->db->where('item_spec_set.publication_status', 'activated');
    	$this->db->where('inventory_status', 'low');

    	if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
    		$where ="(";
    		$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%' or ";
    		$where.= "inventory.quantity like '%".$filters['search']['value']."%'";
             // $where.= "total_cashbox_amount like '%".$filters['search']['value']."%' or ";
             // $where.= "deposit_withdrawal_amount like '%".$filters['search']['value']."%'";
    		$where.= ")";
            /*
            */
            $this->db->where($where, null, false);
          }

          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('item_spec_set.spec_set', 'asc');
          }
          if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('item_spec_set.spec_set', 'desc');
          }

          if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
          	$this->db->order_by('inventory.quantity', 'asc');
          }
          if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
          	$this->db->order_by('inventory.quantity', 'desc');
          }


          if ($total_count) {
          	return $this->db->get()->num_rows();            
          } else {
          	$this->db->limit($filters['length'], $filters['start']);
          	$q = $this->db->get();
          	$a = $q->result_array();
          	return $a;
          }
        }

        public function search_brand($text)
        {
        	$this->db->select('brands_name,brands_id');
        	$this->db->from('brands');
        	$this->db->like('brands_name', $text, 'both');
        	$this->db->where('publication_status', 'activated');
        	$this->db->limit(10);
        	return $this->db->get()->result_array();
        }

        public function search_unit($text)
        {
        	$this->db->select('units_id,units_name');
        	$this->db->from('units');
        	$this->db->like('units_name', $text, 'both');
        	$this->db->where('publication_status', 'activated');
        	$this->db->limit(10);
        	return $this->db->get()->result_array();
        }

        public function search_size($text)
        {
        	$this->db->select('sizes_name,sizes_id');
        	$this->db->from('sizes');
        	$this->db->like('sizes_name', $text, 'both');
        	$this->db->where('publication_status', 'activated');
        	$this->db->order_by('sizes_name', 'asc');
        	$this->db->limit(20);
        	return $this->db->get()->result_array();
        }

        public function search_lot_size($text)
        {
        	$this->db->select('lot_size_name,lot_size_id');
        	$this->db->from('lot_size');
        	$this->db->like('lot_size_name', $text, 'both');
        	$this->db->where('publication_status', 'activated');
        	$this->db->limit(10);
        	return $this->db->get()->result_array();
        }

        public function get_uuid_short()
        {
        	$this->db->select('UUID_SHORT()');    
        	return  $uuid_short=$this->db->get()->row_array();
        }

        public function get_company_name()
        {
        	$this->db->select('name');
        	$this->db->from('shop');
        	return $this->db->get()->row_array();
        }

        public function getBuyCatItemsInfo($item_spec_set_id){

        	$this->db->select('buy_cart_items_id', FALSE);
        	$this->db->from('buy_cart_items');
        	$this->db->where('item_spec_set_id', $item_spec_set_id);
          $this->db->where('publication_status', 'activated');
          $this->db->order_by('date_created', 'desc');
          $getData=  $this->db->get()->row_array();
          return $getData['buy_cart_items_id'];

        }

        public function updateBuyCatItemsInfo($getLastBuyCartItemsId,$updateData){
        	$this->db->where('buy_cart_items_id', $getLastBuyCartItemsId)->update('buy_cart_items', $updateData);
        	return $this->db->affected_rows();
        }

        public function getItemPriceInfoById($item_id){

        	$this->db->select('whole_sale_price,retail_price', FALSE);
        	$this->db->from('buy_cart_items');
        	$this->db->where('item_spec_set_id', $item_id);
          $this->db->where('publication_status', 'activated');
          $this->db->order_by('date_created', 'desc');
          return $this->db->get()->row_array();
        }

      }

      /* End of file Item_model.php */
/* Location: ./application/models/Item_model.php */