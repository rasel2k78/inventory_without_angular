<?php 

/**
 * VAT Model includes vat controller functions.
 *
 * PHP Version 5.6
 * CodeIgniter 3.0
 *
 * @copyright copyright@2016
 * @license   MAX Group BD
 */


if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Vat_model extends CI_Model
{

  public $variable;

    /**
     * Constructor function load language files.
     * 
     * @return void
     * @author Shoaib <shofik.shoaib@gmail.com>
     **/
    
    public function __construct()
    {
      parent::__construct();
      $cookie = $this->input->cookie('language', true);
      $this->lang->load('model_lang', $cookie);

    }

    /**
 * Save vat information to database. It create new vat type.
 *
 *@param  array[] $data
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function save_vat_info($data)
    {

     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['vat_id']=$uuid['UUID()'];
     $this->db->insert('vat', $data);
     return $uuid['UUID()'];
   }

    /**
 * Collect all vat information for AJAX Datatable
 *
 * @param  array[] $filters
 * @param  mix[]   $total_count
 *@link   vat/all_vat_info_for_datatable
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com> 
 **/


    public function all_vat_info($filters,$total_count=false)
    {
     $this->db->select(
       '
       vat_name as "0",
       vat_percentage as "1",
       DATE_FORMAT(date_created, "%d-%b-%Y %h:%i %p") as "2"

       '
       );

     $lang_edit = $this->lang->line("model_edit");
     $lang_delete = $this->lang->line("model_delete");
     $lang_active= $this->lang->line("model_active");
     $lang_deactive= $this->lang->line("model_deactive");

     /*another way to display multiple language*/

         // $query_string = "concat('<button type=\"button\" class=\"btn btn-sm green edit_expenditure_type\" id=\"edit_', CAST(expenditure_types_id AS CHAR) ,'\">$lang</button>') as '3'";
         // $this->db->select($query_string, FALSE);


     $this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green edit_vat\" id=\"edit_', CAST(vat_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_vat\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(vat_id AS CHAR) ,'\">$lang_delete</button>',if(selection_status=\"selected\",concat('<button type=\"button\" class=\"btn btn-xs yellow \">$lang_active</button>'), concat('<button type=\"button\" class=\"btn btn-xs blue active_vat\" data-toggle=\"modal\" href=\"#responsive_modal_active\" id=\"active_', CAST(vat_id AS CHAR) ,'\">$lang_deactive</button>' ))) as '3'", false);

     $this->db->from('vat');
     $this->db->where('publication_status', 'activated');

     if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
      $where ="(";
      $where.= "vat_name like '%".$filters['search']['value']."%' or ";
      $where.= "vat_percentage like '%".$filters['search']['value']."%'";

      $where.= ")";
      $this->db->where($where, null, false);
    }

    if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
      $this->db->like('vat_name', $filters['columns']['0']['search']['value'], 'both');
    }
    if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
      $this->db->like('vat_percentage', $filters['columns']['1']['search']['value'], 'both');
    }
    if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
      $range_date = explode('_', $filters['columns']['2']['search']['value']);
      if ($range_date[1]!=0 && $range_date[0]==0) {
        $this->db->where('DATE(date_created) >', DATE("1992-01-01"));
        $this->db->where('DATE(date_created) <', DATE($range_date[1]));
      }
      if ($range_date[1]==0 && $range_date[0]!=0) {
        $this->db->where('DATE(date_created) >', DATE($range_date[0]));
        $this->db->where('DATE(date_created) <', "DATE(NOW())");
      }
      if ($range_date[1]>0 && $range_date[0]>0) {
        $this->db->where('DATE(date_created) >=', DATE($range_date[0]));
        $this->db->where('DATE(date_created) <=', DATE($range_date[1]));
      }
    }

    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('vat_name', 'asc');
    }
    if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('vat_name', 'desc');
    }

    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('vat_percentage', 'asc');
    }
    if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('vat_percentage', 'desc');
    }

    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
      $this->db->order_by('date_created', 'asc');
    }
    if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
      $this->db->order_by('date_created', 'desc');
    }

    if ($total_count) {
      return $this->db->get()->num_rows();            
    } else {
      $this->db->limit($filters['length'], $filters['start']);
      $q = $this->db->get();
      $a = $q->result_array();
      return $a;
    }


  }

    /**
 * Collect individual vat information by id
 *
 *@param  string $vat_id
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function vat_info_by_id($vat_id)
    {

     $this->db->select('vat_name,vat_percentage,vat_details');
     $this->db->from('vat');
     $this->db->where('publication_status', 'activated');
     $this->db->where('vat_id', $vat_id);
     return $this->db->get()->row_array();

   }

    /**
 * Update vat information by id
 *
 *@param  string  $vat_id
 *@param  array[] $data
 *@link   vat/update_vat_info
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function update_vat_info($vat_id,$data)
    {
     return $this->db->where('vat_id', $vat_id)->update('vat', $data);
   }

    /**
 * Delete vat information by id. It actually changes the publication status form "activated" to "deactivated".
 *
 *@param  string $vat_id
 *@link   vat/delete_vat
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/

    public function delete_vat_info($vat_id)
    {
     return    $this->db->set('publication_status', 'deactivated')->where('vat_id', $vat_id)->update('vat');
   }

    /**
 *Activate vat by id. It activate the vat type by id and deactivate previous one.
 *
 *@param  string $vat_id
 *@link   vat/activate_vat
 * @return void
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/


    public function activate_vat_info($vat_id)
    {
     return    $this->db->set('selection_status', 'selected')->where('vat_id', $vat_id)->update('vat');
   }

   public function count_rows()
   {
     return $this->db->count_all('vat');
   }

   public function change_status_of_vat()
   {
    return $this->db->set('selection_status', 'not_selected')->update('vat');
  }

}

/* End of file Vat_model.php */
/* Location: ./application/models/Vat_model.php */