<?php
/**
 * Exchange_with_customer_model CRUD model
 * 
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

/**
 * Exchange_with_customer_model class
 *
 * @package default
 * @author  Rasel <raselahmed2k7@gmail.com>
 **/
class Exchange_with_customer_model extends CI_Model
{

  public $variable;

    /**
 * This is a construtor functions.
 *
 * @return void
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function __construct()
    {
     parent::__construct();

   }
    /**
 * Collect individual customer voucher's information from database by id
 *
 * @return array[]
 * @link   Exchange_with_customers/get_voucher_info
 * @param  string $sell_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function sell_details($voucher_id)
    {
     return  $this->db->get_where('sell_details', array('sell_local_voucher_no' => $voucher_id ,'publication_status'=>'activated'))->row_array();
   }
/**
 * Get Item inventory 
 *
 * @return array[]
 * @link   Exchange_with_customers/check_items_on_inventory
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function current_items_on_inventory($item_spec_set_id)
{
 return $this->db->get_where('inventory', array('item_spec_set_id' => $item_spec_set_id))->row_array();
}
   /**
 * Update current inventory
 *
 * @return array[]
 * @link   Exchange_with_customers/save_sell_info
 * @param  string $sell_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
   public function update_inventory($update_inventory_data,$item_spec_set_id)
   {
    return $this->db->update('inventory', $update_inventory_data, array('item_spec_set_id'=>$item_spec_set_id));
  }
/**
 * Update Imei barcode data
 *
 * @return boolean
 * @link   Exchange_with_customers/save_sell_info
 * @param  string $imei_barcode
 * @param  array[] $imei_data
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_imei_barcode($imei_data,$imei_barcode,$item_spec_set_id)
{
  return $this->db->update('imei_barcode', $imei_data, array('item_spec_set_id'=>$item_spec_set_id,'imei_barcode'=>$imei_barcode));
}
  /**
 * Get item inventory
 *
 * @return array[]
 * @link   Exchange_with_customers/check_buy_cart_items
 * @param  string $items_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
  public function get_item_quantity_inventory($items_id)
  {
   return $this->db->get_where('inventory', array('item_spec_set_id' => $items_id))->row_array();
 }
 /**
 * Update cashbox amount
 *
 * @return array[]
 * @param  array[] $cashbox_amount_update_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
 public function update_final_cashbox_amount($cashbox_amount_update_data)
 {
   return $this->db->update('final_cashbox_amount', $cashbox_amount_update_data);
 }
/**
 * Get sell voucher items
 *
 * @return array[]
 * @link   Exchange_with_customers/save_sell_info
 * @param  string $sell_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_sell_cart_items($sell_details_id)
{
 return $this->db->get_where('sell_cart_items', array('sell_details_id' => $sell_details_id,'publication_status'=>'activated'))->result_array();
}
/**
 * Get sell voucher items
 *
 * @return array[]
 * @param  array[] $sdata
 * @param  string $sell_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_sell_details_in_return($sdata,$sell_details_id)
{
 return $this->db->update('sell_details', $sdata, array("sell_details_id"=>$sell_details_id));
}
/**
 * Get sell voucher items
 *
 * @return array[]
 * @param  array[] $update_sell_cart_item_data
 * @param  string $sell_details_id
 * @param  string $item_spec_set_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_sell_cart_item_return($update_sell_cart_item_data,$sell_details_id,$item_spec_set_id)
{
  return $this->db->update('sell_cart_items', $update_sell_cart_item_data, array('sell_details_id' =>$sell_details_id,'item_spec_set_id'=>$item_spec_set_id));
}
/**
 * Get sell voucher items
 *
 * @return array[]
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_sell_details_by_voucher($voucher_no)
{
 $this->db->select('*');
 $this->db->from('sell_details');
 $this->db->where('sell_local_voucher_no', $voucher_no);
 $this->db->limit(1);
 $q = $this->db->get();
 return $q->row_array();
}
/**
 * Item searching by baroode or item name 
 *
 * @return array[]
 * @param  string $text
 * @link   Exchange_with_customers/search_item_by_name
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function search_items($text)
{
 $this->db->select('item_spec_set_id,items_name,item_image');
 $this->db->from('items');
 $this->db->like('items_name', $text, 'both');
 $this->db->or_like('unique_id', $text, 'both');
 $this->db->where('publication_status', 'activated');
 $this->db->limit(10);
 return $this->db->get()->result_array();
}
/**
 * Get sell voucher items 
 *
 * @return array[]
 * @param  string $sell_voucher_no
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_sell_details($sell_voucher_no)
{
 return $this->db->get_where('sell_details', array('sell_local_voucher_no' => $sell_voucher_no))->row_array();
}
/**
 * Get current inventory of an item 
 *
 * @return boolean
 * @param  string $item_spec_set_id
 * @link   Exchange_with_customers/check_items_on_inventory
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_items_inventory($item_spec_set_id)
{
 return $this->db->get_where('inventory', array('item_spec_set_id'=>$item_spec_set_id))->row_array();
}
/**
 * Update sell voucher details after exchange
 *
 * @return array[]
 * @param  string $sell_details_id
 * @param  array[] $update_sell_details
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_sell_details($update_sell_details,$sell_details_id)
{
 return $this->db->update('sell_details', $update_sell_details, array('sell_details_id' =>$sell_details_id));
}
/**
 * inventory of an item
 *
 * @return array[]
 * @param  string $item_spec_set_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function items_on_inventory($item_spec_set_id)
{
 return $this->db->get_where('inventory', array('item_spec_set_id' => $item_spec_set_id))->row_array();
}
/**
 * sell voucher item quantity of an item
 *
 * @return array[]
 * @param  string $item_spec_set_id
 * @param  string $sell_details_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function items_on_cart($sell_details_id,$item_spec_set_id)
{
 return $this->db->get_where('sell_cart_items', array('sell_details_id' => $sell_details_id,'item_spec_set_id'=>$item_spec_set_id))->row_array();
}
/**
 * Delete item from sell cart item talbe which has imei 
 *
 * @return boolean
 * @param  string $sell_cart_items_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function delete_imei_from_cart($sell_cart_items_id)
{
  $this->db->where('sell_cart_items_id', $sell_cart_items_id);
  return $this->db->delete('sell_cart_items');
}
/**
 * Update sell cart item after exchange
 *
 * @return boolean
 * @param  array[] $update_sell_cart_item_data
 * @param  string $sell_details_id
 * @param  string $item_spec_set_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_cart_items($update_sell_cart_item_data,$sell_details_id,$item_spec_set_id)
{
 return $this->db->update('sell_cart_items', $update_sell_cart_item_data, array('sell_details_id' =>$sell_details_id,'item_spec_set_id'=>$item_spec_set_id));
}
/**
 * Get sell voucher 
 *
 * @return boolean
 * @param  array[] $update_sell_cart_item_data
 * @param  string $sell_details_id
 * @param  string $item_spec_set_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_sell_cart_items_voucher($sell_details_id,$item_spec_set_id)
{
 $this->db->select('*');
 $this->db->from('sell_cart_items');
 $this->db->where('sell_details_id', $sell_details_id);
 $this->db->where('item_spec_set_id', $item_spec_set_id);
 $this->db->limit(1);
 $q = $this->db->get();
 return $q->row_array();
}
/**
 * Update sell cart item for specific item
 *
 * @return boolean
 * @param  array[] $update_sell_cart_item_data
 * @param  array[] $insert_data
 * @param  string $sell_cart_id
 * @link   Exchange_with_customers/save_sell_info
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_cart_items_cart_id($insert_data,$sell_cart_id)
{
 return $this->db->update('sell_cart_items', $insert_data, array('sell_cart_items_id' =>$sell_cart_id));
}
/**
 * Get Item quantity from sell cart item table
 *
 * @return array[]
 * @param  array[] $update_sell_cart_item_data
 * @param  string $sell_details_id
 * @param  string $items_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_item_quantity_cart($sell_details_id,$items_id)
{
  return $this->db->get_where('sell_cart_items', array('sell_details_id' => $sell_details_id,'item_spec_set_id'=>$items_id))->row_array();
}
/**
 * Get sell voucher customer
 *
 * @return array[]
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_sell_customer($voucher_no)
{
  return $this->db->get_where('sell_details', array('sell_local_voucher_no' => $voucher_no))->row_array();
}
/**
 * Update sell cart item 
 *
 * @return boolean
 * @param  array[] $update_sell_cart_item_data
 * @param  string $cart_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_sell_cart_item_by_id($update_sell_cart_item_data,$cart_id)
{
  return $this->db->update('sell_cart_items', $update_sell_cart_item_data, array('sell_cart_items_id' =>$cart_id));
}
/**
 * Get sell voucher details by voucher no
 *
 * @return array[]
 * @param  string $voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_voucher_info_transfer($voucher_no)
{
 $this->db->select('sell_details.sell_details_id,sell_details.date_created,sell_details.sell_local_voucher_no,sell_details.grand_total,sell_details.sell_local_voucher_no,sell_details.paid,sell_details.due,sell_details.discount,sell_details.discount_percentage,sell_details.net_payable,sell_cart_items.quantity,sell_cart_items.selling_price,sell_cart_items.discount_amount,sell_cart_items.discount_type,sell_cart_items.sub_total, sell_cart_items.item_spec_set_id,item_spec_set.spec_set');
 $this->db->from('sell_details');
 $this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
 $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
 $this->db->where('sell_details.publication_status', 'activated');
 $this->db->where('sell_details.sell_local_voucher_no', $voucher_no);
 return $this->db->get()->result_array();
}
/**
 * Get sell voucher search value
 *
 * @return array[]
 * @param  string $search_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_voucher_info($search_data)
{

 $this->db->select('sell_details.sell_details_id,sell_details.date_created,sell_details.sell_local_voucher_no,sell_details.grand_total,sell_details.sell_local_voucher_no,sell_details.paid,sell_details.due,sell_details.discount,sell_details.net_payable,sell_cart_items.quantity,sell_cart_items.selling_price,sell_cart_items.item_spec_set_id,item_spec_set.spec_set,customers.customers_phone_1');
 $this->db->from('sell_details');
 $this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
 $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
 $this->db->join('customers', 'sell_details.customers_id = customers.customers_id', 'left');
 $this->db->where('sell_details.publication_status', 'activated');
 if(!empty($search_data['voucher_no'])) {$this->db->where('sell_details.sell_local_voucher_no', $search_data['voucher_no']);
}
if(!empty($search_data['voucher_date'])) {$this->db->where('DATE(sell_details.date_created)', DATE($search_data['voucher_date']));
}
if(!empty($search_data['item_name'])) {$this->db->like('item_spec_set.spec_set', $search_data['item_name']);
}
if(!empty($search_data['mobile_number'])) {$this->db->like('customers.customers_phone_1', $search_data['mobile_number']);
}

$this->db->group_by('sell_details.sell_local_voucher_no');
return $this->db->get()->result_array();
}
/**
 * Get sell voucher 
 *
 * @return array[]
 * @param  string $sell_local_voucher_no
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function get_voucher_info_by_id($sell_local_voucher_no)
{
 $this->db->select('DISTINCT imei_barcode.imei_barcode, sell_details.sell_details_id,sell_details.date_created,sell_details.sell_local_voucher_no,sell_details.grand_total,sell_details.sell_local_voucher_no,sell_details.paid,sell_details.due,sell_details.discount,sell_details.discount_percentage,sell_details.net_payable,sell_cart_items.quantity,sell_cart_items.selling_price,sell_cart_items.sub_total,sell_cart_items.discount_amount,sell_cart_items.discount_type,sell_cart_items.item_spec_set_id,sell_cart_items.used_imei_barcode,item_spec_set.spec_set,imei_barcode.barcode_status,imei_barcode.sell_details_id', false);
 $this->db->from('sell_details');
 $this->db->join('sell_cart_items', 'sell_cart_items.sell_details_id = sell_details.sell_details_id', 'left');
 $this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
 $this->db->join('imei_barcode', 'imei_barcode.sell_details_id = sell_details.sell_details_id AND imei_barcode.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
 $this->db->where('sell_details.publication_status', 'activated');
 $this->db->where('sell_details.sell_local_voucher_no', $sell_local_voucher_no);
 $this->db->where('sell_cart_items.quantity !=', 0);
 return $this->db->get()->result_array();
}
    /**
 * get items's information from database by id
 *
 * @return array[]
 * @link   Exchange_with_customers/select_item_price_info_row
 * @param  string $item
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function select_item_price_info_row($item)
    {
     $this->db->select('*', false);
     $this->db->from('buy_cart_items');
     $this->db->where('item_spec_set_id', $item);
     $this->db->order_by('date_updated', 'desc');
     $this->db->limit(1);
     $q = $this->db->get();
     return $q->row_array();
   }
    /**
 * save new items to inventory
 *
 * @return array[]
 * @link   Exchange_with_customers/get_voucher_info
 * @param  string $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function insert_items_to_inventory($data)
    {
     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();
     $data['inventory_id'] = $uuid['UUID()'];
     $this->db->insert('inventory', $data);
   }
    /**
 * save selling items to database
 *
 * @return array[]
 * @link   Exchange_with_customers/add_items_cart
 * @param  string $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function add_items_cart($data)
    {
     $this->db->select('UUID()');    
     $uuid =$this->db->get()->row_array();
     $data['sell_cart_items_id'] = $uuid['UUID()'];
     $this->db->insert('sell_cart_items', $data);
   }
    /**
 * save cashbox changes when customer exchange any amount to database
 *
 * @return array[]
 * @link   Exchange_with_customers/save_cash_box_info
 * @param  string $data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
    public function save_cash_box_info($data)
    {

     $this->db->select('UUID()', false);    
     $uuid =$this->db->get()->row_array();

     $data['cashbox_id']=$uuid['UUID()'];
     $this->db->insert('cashbox', $data);
         // return $uuid['UUID()'];
   }
/**
 * Save exchange data as history
 *
 * @return array[]
 * @param  array[] $exchange_hstory_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function save_exchange_history($exchange_hstory_data)
{
  $this->db->insert('exchange_return_history', $exchange_hstory_data);
}

/**
 * Update cashbox after exchange
 *
 * @return array[]
 * @param  array[] $cashbox_amount_update_data
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function update_cashbox_after_exc($cashbox_amount_update_data)
{
  return  $this->db->update('final_cashbox_amount', $cashbox_amount_update_data);
}
/**
 * Update sell voucher
 *
 * @return boolean
 * @param  array[] $update_sell_details
 * @param  string $sell_details_id
 * @author Rasel <raselahmed2k7@gmail.com>
 **/
public function sell_details_update($update_sell_details,$sell_details_id)
{
 return  $this->db->update('sell_details', $update_sell_details, array('sell_details_id' =>$sell_details_id));
}

}

/* End of file  */
/* Location: ./application/models/ */