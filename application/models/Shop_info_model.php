<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}

class Shop_info_model extends CI_Model
{

    public $variable;

    public function __construct()
    {
        parent::__construct();
        $this->load->database("inventory");
    }

    public function getShopInfo()
    {
        $this->db->select('*');
        $this->db->from('shop');
        $shopInfo = $this->db->get()->row_array();
        return $shopInfo;
    }

    public function update_shop_info($id,$data)
    {
        $this->db->where('shop_id', $id);
        return $this->db->update('shop', $data); 
    }

    public function update_printer_info($printer_data)
    {
        return $this->db->update('shop', $printer_data);
    }

}

/* End of file Shop_info_model.php */
/* Location: ./application/models/Shop_info_model.php */