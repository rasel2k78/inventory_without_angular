<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance_buy_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function save_adv_buy_basic_info($ready_data_basic)
	{
		$this->db->select('UUID()', false);    
		$uuid =$this->db->get()->row_array();

		$ready_data_basic['adv_buy_details_id']=$uuid['UUID()'];
		$this->db->insert('adv_buy_details', $ready_data_basic);
		return $uuid['UUID()'];
	}

	public function save_adv_buy_item_info($data,$buy_details_uuid)
	{
		foreach ($data as $key => $value) {
			$this->db->select('UUID()', false);    
			$uuid =$this->db->get()->row_array();
			unset($data[$key]['item_name']);
			unset($data[$key]['actual_quantity']);
			unset($data[$key]['unique_barcode']);
			unset($data[$key]['cart_temp_id']);
			$data[$key]['adv_buy_details_id'] = $buy_details_uuid;
			$data[$key]['user_id'] = $this->session->userdata('user_id');    
			$data[$key]['adv_buy_cart_items_id']=$uuid['UUID()'];

		}
		return $this->db->insert_batch('adv_buy_cart_items', $data);
	}

	public function all_adv_buy_info_list($filters,$total_count=false)
	{
		$no_vendor = $this->lang->line("model_empty");
		$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "0"', false);
		$this->db->select(
			'
			adv_buy_details.voucher_no as "1",
			DATE_FORMAT(adv_buy_details.receivable_date, "%d-%b-%Y") as "2"
			'
			,false);
		$lang_edit = "Edit";
		$lang_details = $this->lang->line("model_details");
		$lang_delete = $this->lang->line("model_delete");
		$this->db->select("concat('<button type=\"button\" class=\"btn btn-xs green adv_buy_details\" id=\"update_', CAST(adv_buy_details.adv_buy_details_id AS CHAR) ,'\">$lang_details</button>','<button type=\"button\" class=\"btn btn-xs blue adv_buy_edit\" id=\"edit_', CAST(adv_buy_details.adv_buy_details_id AS CHAR) ,'\">$lang_edit</button>','<button type=\"button\" class=\"btn btn-xs red delete_adv_buy\" data-toggle=\"modal\" href=\"#responsive_modal_delete\"  id=\"delete_', CAST(adv_buy_details.adv_buy_details_id AS CHAR) ,'\">$lang_delete</button>') as '3'", false);

		$this->db->from('adv_buy_details');
		$this->db->join('vendors', 'vendors.vendors_id = adv_buy_details.vendors_id', 'left');
		$this->db->join('adv_buy_cart_items', 'adv_buy_cart_items.adv_buy_details_id = adv_buy_details.adv_buy_details_id', 'left');
		$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = adv_buy_cart_items.item_spec_set_id', 'left');
		$this->db->where('adv_buy_details.publication_status', 'activated');
		$this->db->where('adv_buy_details.receival_status', 'pending');
		$this->db->group_by('adv_buy_details.voucher_no');

        // $this->db->order_by("date_created", "desc"); 

		if($filters['search']['value'] !="" && $filters['search']['value'] != null) {
			$where ="(";
			$where.= "item_spec_set.spec_set like '%".$filters['search']['value']."%'";
			$where.= ")";
			$this->db->where($where, null, false);
		}

		if($filters['columns']['0']['search']['value']!="" && $filters['columns']['0']['search']['value']!=null) {
			$this->db->like('vendors.vendors_name', $filters['columns']['0']['search']['value'], 'both');
		}

		if($filters['columns']['1']['search']['value']!="" && $filters['columns']['1']['search']['value']!=null) {
			$this->db->like('adv_buy_details.voucher_no', $filters['columns']['1']['search']['value'], 'both');
		}

		if($filters['columns']['2']['search']['value']!="" && $filters['columns']['2']['search']['value']!=null) {
			$range_date = explode('_', $filters['columns']['2']['search']['value']);
			if ($range_date[1]!=0 && $range_date[0]==0) {
				$this->db->where('DATE(adv_buy_details.receivable_date) >', DATE("1992-01-01"));
				$this->db->where('DATE(adv_buy_details.receivable_date) <', DATE($range_date[1]));
			}
			if ($range_date[1]==0 && $range_date[0]!=0) {
				$this->db->where('DATE(adv_buy_details.receivable_date) >', DATE($range_date[0]));
				$this->db->where('DATE(adv_buy_details.receivable_date) <', "DATE(NOW())");
			}
			if ($range_date[1]>0 && $range_date[0]>0) {
				$this->db->where('DATE(adv_buy_details.receivable_date) >=', DATE($range_date[0]));
				$this->db->where('DATE(adv_buy_details.receivable_date) <=', DATE($range_date[1]));
			}
		}

		if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='asc') {
			$this->db->order_by('vendors.vendors_name', 'asc');
		}
		if($filters['order']['0']['column'] == '0' && $filters['order']['0']['dir']=='desc') {
			$this->db->order_by('vendors.vendors_name', 'desc');
		}

		if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='asc') {
			$this->db->order_by('adv_buy_details.voucher_no', 'asc');
		}
		if($filters['order']['0']['column'] == '1' && $filters['order']['0']['dir']=='desc') {
			$this->db->order_by('adv_buy_details.voucher_no', 'desc');
		}

		if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='asc') {
			$this->db->order_by('adv_buy_details.receivable_date', 'asc');
		}
		if($filters['order']['0']['column'] == '2' && $filters['order']['0']['dir']=='desc') {
			$this->db->order_by('adv_buy_details.receivable_date', 'desc');
		}

		if ($total_count) {
			return $this->db->get()->num_rows();            
		} else {
			$this->db->limit($filters['length'], $filters['start']);
			$qry_get = $this->db->get();
			// echo $this->db->last_query();
			$output_arr =$qry_get->result_array();
			return $output_arr;
		}

	}

	public function adv_buy_details_by_id($buy_details_id)
	{
		$this->db->select('*');
		$this->db->from('adv_buy_details');
		$this->db->where('adv_buy_details_id', $buy_details_id);
		$this->db->where('publication_status', 'activated');
		return $this->db->get()->row_array();
	}

	public function details_adv_buy_info_by_id($buy_details_id)
	{

		$this->db->select('adv_buy_details.landed_cost,adv_buy_details.grand_total,adv_buy_details.voucher_no,adv_buy_details.paid,adv_buy_details.due,adv_buy_details.discount,adv_buy_details.net_payable,adv_buy_cart_items.quantity,adv_buy_cart_items.sub_total,adv_buy_cart_items.discount_type,adv_buy_cart_items.discount_amount,adv_buy_cart_items.buying_price,item_spec_set.spec_set,adv_buy_details.date_created,adv_buy_details.receivable_date');

		$this->db->from('adv_buy_details');
		$this->db->join('adv_buy_cart_items', 'adv_buy_cart_items.adv_buy_details_id = adv_buy_details.adv_buy_details_id', 'left');
		$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = adv_buy_cart_items.item_spec_set_id', 'left');
     // $this->db->join('imei_barcode', 'imei_barcode.buy_details_id = buy_details.buy_details_id', 'left');
		$this->db->where('adv_buy_details.publication_status', 'activated');
		$this->db->where('adv_buy_details.adv_buy_details_id', $buy_details_id);
		return $this->db->get()->result_array();

	}

	public function collect_adv_voucher_number_buy($buy_details_uuid)
	{
		$this->db->select('voucher_no,net_payable,grand_total,discount,paid,due,vendors_id', FALSE);
		$this->db->from('adv_buy_details');
		$this->db->where('adv_buy_details_id', $buy_details_uuid);
		return $this->db->get()->row_array();
	}

	public function update_adv_buy_status($adv_buy_id)
	{
		return $this->db->set('receival_status', 'done')->where('adv_buy_details_id', $adv_buy_id)->update('adv_buy_details');
	}

}

/* End of file Advance_buy_model.php */
/* Location: ./application/models/Advance_buy_model.php */