<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrationmodel');
	}

	public function index()
	{
		echo "ASDF";
	}

	public function checkKey()
	{

		$data = $this->Registrationmodel->getKeys();

		$postedKey = $this->input->post("key");

		// $output = $this->Registrationmodel->Registration($postedKey);
		$output = $this->Registrationmodel->checkKey($postedKey);
		if($output){

			$checkRegist = $this->checkRegistration();

			if($checkRegist){

				$this->updateKeyStatus($postedKey); 

				echo "Registration success";
			}

		}else{
			echo "Key not found";
		}
	}


	public function update_transfer_adjust_status($transfer_details_id)
	{
		$data['status'] = 'adjusted';
		$update_transfer_adjust = $this->Registrationmodel->update_transfer_status($transfer_details_id,$data);
		if($update_transfer_adjust)
		{
			echo json_encode(1);
		}
	}

	public function update_warehouse_status_from_pending($transfer_details_id)
	{
		$data['status'] = 'received';

		//echo $transfer_details_id;exit;
		$update_transfer_adjust = $this->Registrationmodel->update_transfer_status($transfer_details_id,$data);
		if($update_transfer_adjust)
		{
			echo json_encode(1);
		}
	}
	public function update_transfer_adjust_status_finish($transfer_details_id)
	{
		$data['status'] = 'finished';
		$update_transfer_adjust = $this->Registrationmodel->update_transfer_status($transfer_details_id,$data);
		if($update_transfer_adjust)
		{
			echo json_encode(1);
		}
	}

	public function get_my_transfer_list()
	{
		$check_transfer_items = $this->Registrationmodel->get_my_transfers($shop_id);
		echo json_encode($check_transfer_items);

	}

	public function get_transfer_item_details($shop_id)
	{
		//echo json_encode($shop_id);exit;
		$check_transfer_items = $this->Registrationmodel->check_if_transfer($shop_id);
		echo json_encode($check_transfer_items);
	}

	public function get_transfer_item_details_warehouse($shop_id)
	{
		$check_transfer_items = $this->Registrationmodel->check_if_transfer_warhouse($shop_id);
		echo json_encode($check_transfer_items);
	}

	public function get_if_transfer_receives($shop_id)
	{
		$check_transfer_items = $this->Registrationmodel->check_if_transfer_receives($shop_id);
		echo json_encode($check_transfer_items);
		exit;
	}

	public function get_if_transfer_receives_warehouse($shop_id)
	{
		$check_transfer_items = $this->Registrationmodel->check_if_transfer_receives_warehouse($shop_id);
		echo json_encode($check_transfer_items);
		exit;
	}

	public function get_transfer_items($transfer_details_id)
	{
		if($transfer_details_id)
		{
			$received_items = $this->Registrationmodel->get_transfer_items_by_details_id($transfer_details_id);
			echo json_encode($received_items);
			exit;
		}	
	}

	public function get_transfer_items_warehouse($transfer_details_id,$shop_id,$transfer_to_id)
	{
		if($transfer_details_id)
		{
			$transfer_and_items = array();
			$item_result = array();
			$item_result['item_spec_set'] = array();
			$item_result['items_name'] = array();
			$item_result['categories'] = array();
			$item_result['spec_connector_with_values'] = array();
			$item_result['spec_names'] = array();
			$item_result['spec_values'] = array();

			$received_items = $this->Registrationmodel->get_warehouse_items_by_details_id($transfer_details_id,$shop_id);
			foreach ($received_items as $key => $value) 
			{
				$item_result['users'] = $this->Registrationmodel->get_users($shop_id);

				$item_spec_set_table = $this->Registrationmodel->get_item_spec_set_individual($value['item_spec_set_id'],$shop_id);
				array_push($item_result['item_spec_set'],$item_spec_set_table);


				$items_table = $this->Registrationmodel->get_items_name_individual($item_spec_set_table['items_id'],$shop_id);
				array_push($item_result['items_name'],$items_table);


				$categories_table = $this->Registrationmodel->get_categories_name_individual($items_table['categories_id'],$shop_id);
				array_push($item_result['categories'],$categories_table);


				$spec_set_connector_table = $this->Registrationmodel->get_spec_set_connectors_individual($item_spec_set_table['item_spec_set_id'],$shop_id);
				

				foreach ($spec_set_connector_table as $skey => $svalue) 
				{
					array_push($item_result['spec_connector_with_values'],$svalue);
					$spec_name_table = $this->Registrationmodel->get_spec_names_individual($svalue['spec_name_id'],$shop_id);
					array_push($item_result['spec_names'],$spec_name_table);

					$spec_values_table = $this->Registrationmodel->get_spec_values_individual($svalue['spec_value_id'],$shop_id);
					array_push($item_result['spec_values'],$spec_values_table);
				}
			}

			$total_receives = $this->Registrationmodel->get_warehouse_total_receive_amount($transfer_to_id);
			$transfer_and_items['total_receives'] = $total_receives;
			$transfer_and_items['items'] = $item_result;
			$transfer_and_items['transfers_received_items'] = $received_items;
			echo json_encode($transfer_and_items);
			exit;
		}	
	}

	public function get_synched_shops($shop_id)
	{
		$shops = $this->Registrationmodel->get_all_shop($shop_id);
		echo json_encode($shops);
		exit;
	}

	public function get_spec_names_by_shop($shop_id)
	{

		$result = array();
		$result['users'] = $this->Registrationmodel->get_users($shop_id);
		$result['imei_barcodes'] = $this->Registrationmodel->get_imei_barcodes($shop_id);
		$result['spec_names'] = $this->Registrationmodel->get_spec_names($shop_id);
		$result['spec_values'] = $this->Registrationmodel->get_spec_values($shop_id);
		$result['categories'] = $this->Registrationmodel->get_categories($shop_id);
		$result['items_name'] = $this->Registrationmodel->get_item_names($shop_id);
		$result['item_spec_set'] = $this->Registrationmodel->get_item_spec_set($shop_id);
		$result['spec_connector_with_values'] = $this->Registrationmodel->get_spec_connector_with_values($shop_id);
		//echo json_encode(array("spec_names"=>$shop_id,"spec_values"=>"RRR"));
		echo json_encode($result);
		exit;
	}

	public function get_spec_names_warehouse()
	{
		$result = array();
		$result['users'] = $this->Registrationmodel->get_users_warehouse();
		$result['imei_barcodes'] = $this->Registrationmodel->get_imei_barcodes_warehouse();
		$result['spec_names'] = $this->Registrationmodel->get_spec_names_warehouse();
		$result['spec_values'] = $this->Registrationmodel->get_spec_values_warehouse();
		$result['categories'] = $this->Registrationmodel->get_categories_warehouse();
		$result['items_name'] = $this->Registrationmodel->get_item_names_warehouse();
		$result['item_spec_set'] = $this->Registrationmodel->get_item_spec_set_warehouse();
		$result['buy_details'] = $this->Registrationmodel->get_buy_details_warehouse();
		$result['buy_cart_items'] = $this->Registrationmodel->get_buy_cart_items_warehouse();
		$result['spec_connector_with_values'] = $this->Registrationmodel->get_spec_connector_with_values_warehouse();
		echo json_encode($result);
		exit;
	}

	public function get_transfered_items($shop_id)
	{
		if($shop_id)
		{
			$received_items = $this->Registrationmodel->get_transfer_items_by_id($shop_id);
			echo json_encode($received_items);
			exit;	
		}
	}

	public function update_transfer_received($transfer_details_id)
	{
		$data['status'] = 'received';
		$update_transfer = $this->Registrationmodel->update_transfer_status($transfer_details_id,$data);
		if($update_transfer)
		{
			echo json_encode(1);
		}
	}

	public function update_transfer_sent($transfer_details_id)
	{
		$data['status'] = 'completed';
		$update_transfer = $this->Registrationmodel->update_transfer_sent_completed($transfer_details_id,$data);
		if($update_transfer)
		{
			echo json_encode(1);
		}
	}


/**
	 * 
	 *
	 * checkRegistration method is for check registration with a valid key and register an user
	 * 
	 * @return null
	 * @author Musabbir 
	 **/
public function checkRegistration()    
{
	$saltString = "s!a@l#t%18+";
	$password = $this->input->post("owner_password");
	$data= array(
		'accounts_status' => $this->input->post("accounts_status"),
		'accounts_type' => $this->input->post("accounts_type"),
		'accounts_name' => $this->input->post("accounts_name"),
		'key' => $this->input->post("key"),
		'owner_name' => $this->input->post("owner_name"),
		'owner_email' => $this->input->post("owner_email"),
		'owner_password' => sha1($password.$saltString),
		'mac_address' => $this->input->post("mac_address"),
		'local_pin' => $this->input->post('local_pin'),

		'shop_name' => $this->input->post('shop_name'),
		'shop_address' => $this->input->post('shop_address'),
		'vat_reg_no' => $this->input->post('vat_reg_no')
	);
	$output = $this->Registrationmodel->Registration($data);
	return $output;
}


public function updateKeyStatus($key)
{
	$this->Registrationmodel->updateKeyStat($key);
}



/**
	 * 
	 *
	 * childPcCheck method is for check client PC under an account and register the child PC
	 * 
	 * @return null
	 * @author Musabbir 
	 **/
public function childPcCheck()
{
	$ownerMail = $this->input->post("owner_email");
	$ownerPass = $this->input->post("owner_pass");
	$childMAC =  $this->input->post("mac_address");

	$data = $this->Registrationmodel->childPcCheck($ownerMail,$ownerPass); 

	if( $ownerMail == $data['owner_email']  && $ownerPass == $data['owner_password'] ){
		$countClient = $this->Registrationmodel->countClient($ownerMail);
		if($countClient < 2){
			$clientRegistrationData= array(
				'owner_email' => $this->input->post("owner_email"),
				'mac_address' => $this->input->post("mac_address")
			);
			$data =	$this->Registrationmodel->registerClient($clientRegistrationData); 
			if($data){
				echo "Client registration success";
			}else{
				echo "Client registration fail";
			}	
		}else{
			echo "You can't add more client PC. Try our Premium package for more client PC.";
		}
	}else{
		echo "Sorry owner email & password didn't match";
	}
}

public function checkReInstall()
{
	$productKey = $this->input->post("product_key");
	$ownerMail = $this->input->post("owner_email");
	$ownerPass =  $this->input->post("owner_pass");

	$data = $this->Registrationmodel->checkReInstall($productKey,$ownerMail,$ownerPass); 

	if($data >0){
		echo "Account found";
	}else{
		echo "Sorry given information did not match";
	}
}

}

/* End of file Registration.php */
/* Location: ./application/controllers/Registration.php */