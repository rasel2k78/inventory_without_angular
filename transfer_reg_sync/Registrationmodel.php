<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrationmodel extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_my_transfers()
	{
		$this->db->select('*');
		$this->db->from('transfer_details');
		$this->db->where('transfer_from', $shop_id);
		$this->db->where('publication_status', "activated");
		return $this->db->get()->result_array();
	}

	public function get_transfer_items_by_id($shop_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->row_array();
	}

	public function get_spec_names($shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_name_table');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}

	public function get_spec_names_warehouse()
	{
		$this->db->select('*');
		$this->db->from('spec_name_table');
		return $this->db->get()->result_array();
	}

	public function get_spec_values($shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_value');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}

	public function get_spec_values_warehouse()
	{
		$this->db->select('*');
		$this->db->from('spec_value');
		return $this->db->get()->result_array();
	}
	public function get_categories($shop_id)
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}

	public function get_categories_warehouse()
	{
		$this->db->select('*');
		$this->db->from('categories');
		return $this->db->get()->result_array();
	}
	public function get_item_names($shop_id)
	{
		$this->db->select('*');
		$this->db->from('items');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}

	public function get_item_names_warehouse()
	{
		$this->db->select('*');
		$this->db->from('items');
		return $this->db->get()->result_array();
	}
	public function get_item_spec_set($shop_id)
	{
		$this->db->select('*');
		$this->db->from('item_spec_set');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}






	public function get_item_spec_set_individual($item_spec_set_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('item_spec_set');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('item_spec_set_id', $item_spec_set_id);
		return $this->db->get()->row_array();
	}
	public function get_items_name_individual($items_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('items');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('items_id', $items_id);
		return $this->db->get()->row_array();
	}
	public function get_categories_name_individual($categories_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('categories_id', $categories_id);
		return $this->db->get()->row_array();
	}
	public function get_spec_set_connectors_individual($item_spec_set_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_set_connector_with_values');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('item_spec_set_id', $item_spec_set_id);
		return $this->db->get()->result_array();
	}
	public function get_spec_names_individual($spec_name_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_name_table');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('spec_name_id', $spec_name_id);
		return $this->db->get()->row_array();
	}

	public function get_spec_values_individual($spec_value_id,$shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_value');
		$this->db->where('stores_id', $shop_id);
		$this->db->where('spec_value_id', $spec_value_id);
		return $this->db->get()->row_array();
	}






	public function get_item_spec_set_warehouse()
	{
		$this->db->select('*');
		$this->db->from('item_spec_set');
		return $this->db->get()->result_array();
	}
	public function get_spec_connector_with_values($shop_id)
	{
		$this->db->select('*');
		$this->db->from('spec_set_connector_with_values');
		$this->db->where('stores_id', $shop_id);
		$this->db->order_by("spec_set_connector_id","desc");
		// $this->db->order_by('title', 'desc');
		return $this->db->get()->result_array();
	}

	public function get_spec_connector_with_values_warehouse()
	{
		$this->db->select('*');
		$this->db->from('spec_set_connector_with_values');
		$this->db->order_by("spec_set_connector_id","desc");
		return $this->db->get()->result_array();
	}

	public function get_buy_details_warehouse()
	{
		$this->db->select('*');
		$this->db->from('buy_details');
		return $this->db->get()->result_array();
	}

	public function get_buy_cart_items_warehouse()
	{
		$this->db->select('*');
		$this->db->from('buy_cart_items');
		return $this->db->get()->result_array();
	}

	public function get_users($shop_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}
	public function get_users_warehouse()
	{
		$this->db->select('*');
		$this->db->from('users');
		return $this->db->get()->result_array();
	}

	public function get_imei_barcodes($shop_id)
	{
		$this->db->select('*');
		$this->db->from('imei_barcode');
		$this->db->where('stores_id', $shop_id);
		return $this->db->get()->result_array();
	}

	public function get_imei_barcodes_warehouse()
	{
		$this->db->select('*');
		$this->db->from('imei_barcode');
		return $this->db->get()->result_array();
	}

	public function check_if_transfer_receives_warehouse($shop_id)
	{
		$this->db->select('DISTINCT(transfer_details.transfer_details_id),transfer_details.transfer_from,transfer_details.transfer_to,transfer_details.date_created,transfer_details.date_updated,transfer_details.total_item_quantity,transfer_details.total_amount,transfer_details.stores_id,transfer_details.status,transfer_details.publication_status,shop.shop_id,shop.name');
		$this->db->from('transfer_details');
		//$this->db->join('shop','transfer_details.transfer_to = shop.shop_id');
		$this->db->join('shop','transfer_details.transfer_from = shop.shop_id');
		$this->db->where('transfer_details.transfer_to', $shop_id);
		$this->db->where('transfer_details.transfer_type', "warehouse");
		$this->db->where('transfer_details.publication_status', "activated");
		$this->db->order_by('transfer_details.date_created', 'desc');
		$this->db->order_by('transfer_details.publication_status', 'activated');
		//$this->db->where('status', 'pending');
		//return $this->db->query();exit;
		return $this->db->get()->result_array();
	}


	public function check_if_transfer_receives($shop_id)
	{
		$this->db->select('transfer_details.transfer_details_id,transfer_details.transfer_from,transfer_details.transfer_to,transfer_details.date_created,transfer_details.date_updated,transfer_details.total_item_quantity,transfer_details.total_amount,transfer_details.stores_id,transfer_details.status,transfer_details.publication_status,shop.shop_id,shop.name');
		$this->db->from('transfer_details');
		//$this->db->join('shop','transfer_details.transfer_to = shop.shop_id');
		$this->db->join('shop','transfer_details.transfer_from = shop.shop_id');
		$this->db->where('transfer_details.transfer_to', $shop_id);
		$this->db->where('transfer_details.publication_status', "activated");
		$this->db->order_by('transfer_details.date_created', 'asc');
		//$this->db->where('status', 'pending');
		//return $this->db->query();exit;
		return $this->db->get()->result_array();
	}
	public function check_if_transfer($shop_id)
	{
		$this->db->select('transfer_details.transfer_details_id,transfer_details.date_created,transfer_details.date_updated,transfer_details.transfer_from,transfer_details.transfer_to,transfer_details.total_item_quantity,transfer_details.total_amount,transfer_details.stores_id,transfer_details.status,transfer_details.publication_status,shop.shop_id,shop.name');
		$this->db->from('transfer_details');
		$this->db->join('shop','transfer_details.transfer_to = shop.shop_id');
		$this->db->where('transfer_details.transfer_from', $shop_id);
		$this->db->where('transfer_details.publication_status', "activated");
		$this->db->order_by('transfer_details.date_created', 'asc');
		return $this->db->get()->result_array();
	}

	public function check_if_transfer_warhouse($shop_id)
	{
		$this->db->select('DISTINCT(transfer_details.transfer_details_id),transfer_details.date_created,transfer_details.date_updated,transfer_details.transfer_from,transfer_details.transfer_to,transfer_details.total_item_quantity,transfer_details.total_amount,transfer_details.stores_id,transfer_details.status,transfer_details.publication_status,shop.shop_id,shop.name');
		$this->db->from('transfer_details');
		$this->db->join('shop','transfer_details.transfer_to = shop.shop_id');
		$this->db->where('transfer_details.transfer_from', $shop_id);
		$this->db->where('transfer_details.transfer_type', "warehouse");
		$this->db->where('transfer_details.publication_status', "activated");
		$this->db->order_by('transfer_details.date_created', 'desc');
		return $this->db->get()->result_array();
	}

	public function get_warehouse_total_receive_amount($transfer_to_id)
	{
		$this->db->select('SUM(transfer_details.total_amount) AS total_amount,transfer_details.transfer_from,shop.shop_id,shop.name');
		$this->db->from('transfer_details');
		$this->db->join('shop','transfer_details.transfer_from = shop.shop_id');
		$this->db->where('transfer_details.publication_status', 'activated');
		$this->db->where('transfer_details.transfer_to', $transfer_to_id);
		$this->db->where('transfer_details.status', "received");
		return $this->db->get()->row_array();
	}

	public function get_all_shop($shop_id)
	{
		$this->db->select('*');
		$this->db->from('shop');
		$this->db->where('shop_id!=', $shop_id);
		return $this->db->get()->result_array();
	}

	
	public function get_transfer_items_by_details_id($id)
	{
		$this->db->select('item_spec_set.item_spec_set_id,item_spec_set.spec_set,transfer.transfers_id,transfer.quantity,transfer.buying_price,transfer.selling_price,transfer.whole_sale_price');
		$this->db->from('item_spec_set');
		$this->db->join('transfer', 'transfer.item_spec_set_id = item_spec_set.item_spec_set_id', 'inner');
		$this->db->where('item_spec_set.publication_status','activated');
		$this->db->where('transfer.publication_status','activated');
		$this->db->where('transfer.transfer_details_id', $id);
		$this->db->group_by('item_spec_set.item_spec_set_id');
		return $this->db->get()->result_array();
	}


	public function get_warehouse_items_by_details_id($id,$shop_id)
	{
		$this->db->select('DISTINCT(transfer.imei_barcode), item_spec_set.item_spec_set_id,item_spec_set.spec_set,transfer.transfers_id,transfer.quantity,transfer.buying_price,transfer.selling_price,transfer.whole_sale_price');
		$this->db->from('item_spec_set');
		$this->db->join('transfer', 'transfer.item_spec_set_id = item_spec_set.item_spec_set_id', 'inner');
		$this->db->where('item_spec_set.publication_status','activated');
		$this->db->where('transfer.transfer_details_id', $id);
		$this->db->where('transfer.transfer_type', "warehouse");
		$this->db->where('item_spec_set.stores_id', $shop_id);
		$this->db->where('transfer.publication_status', "activated");
		// $this->db->group_by('item_spec_set.item_spec_set_id');
		return $this->db->get()->result_array();
	}

	public function update_transfer_status($id,$data)
	{
		return	$this->db->where('transfer_details_id', $id)->update('transfer_details',$data);
	}

	public function update_transfer_sent_completed($id,$data)
	{
		return	$this->db->where('transfer_details_id', $id)->update('transfer_details',$data);
	}

	public function Registration($data)
	{
		$registration = $this->load->database('registration',TRUE);
		return $registration->insert('accounts',$data);
	}

	public function checkKey($value)
	{
		$this->registration = $this->load->database('registration',TRUE);
		return $this->registration->get_where('activation_key', array('keys =' => $value))->row_array();
	}

	public function getKeys()
	{
		$this->registration = $this->load->database('registration',TRUE);
		$this->registration->select('keys');
		$this->registration->from('activation_key');
		return $this->registration->get()->result_array();
	}

	public function updateKeyStat($key)
	{
		$data['key_status'] = "activated";
		return	$this->registration->where('keys', $key)->update('activation_key',$data);
	}


	public function childPcCheck($ownerMail,$ownerPass)
	{
		$this->registration = $this->load->database('registration',TRUE);
		return $this->registration->get_where('accounts', array('owner_email =' => $ownerMail, 'owner_password =' => $ownerPass))->row_array();
	}

	public function countClient($ownerMail)
	{
		$registration = $this->load->database('registration',TRUE);

		$this->registration->where('owner_email', $ownerMail);
		$this->registration->from('client_pc');
		return $this->registration->count_all_results();
	}

	public function registerClient($clientRegistrationData)
	{
		$registration = $this->load->database('registration',TRUE);
		return $registration->insert('client_pc',$clientRegistrationData);
	}

	public function checkReInstall($productKey,$ownerMail,$ownerPass)
	{
		$registration = $this->load->database('registration',TRUE);
		
		$registration->select('key, owner_email, owner_password');
		$registration->from('accounts');
		$registration->where('key', $productKey);
		$registration->where('owner_email', $ownerMail);
		$registration->where('owner_password', $ownerPass);

		return $registration->count_all_results();
	}
}

/* End of file RegistrationModel.php */
/* Location: ./application/models/RegistrationModel.php */