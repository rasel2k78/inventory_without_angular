<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sync extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Syncmodel');
	}
	public function index()
	{ 
		$headers = $this->input->request_headers();
		$authorization_data = json_decode($headers['Authorization']);
		$stores_id = $authorization_data->stores_id;


		$is_account = $this->Syncmodel->check_accounts($authorization_data->email);
		if($is_account)
		{
			$is_shop = $this->Syncmodel->check_shop($authorization_data->stores_id,$is_account['accounts_id']);
			if($is_shop)
			{
				$all_data = json_decode($this->input->post('all_data'),1);
				$this->db->db_debug = false;
				$result=array();
				
				foreach ($all_data as $key => $value) {
					$this->db->trans_begin();
					$table_name = $value['table_name'];
					$columnIdValue = $value['row_id'];
					$type = $value['type'];
					$columnId = $value['pkey_column_name'];
					$data = $value['data'];
					
					if($type == "insert"){
						$insertData = $this->Syncmodel->insertLiveRow($data,$table_name);
					}else{
						$updateData =  $this->Syncmodel->updateLiveRow($data,$table_name,$columnId,$columnIdValue,$stores_id);
					}	

					if ($this->db->trans_status() === FALSE)
					{
						$result[$key]['success'] = 0; 
						$result[$key]['error_data'] =  $this->db->error();
						$result[$key]['id'] = $value['id'];
						$this->db->trans_rollback();
					}
					else
					{
						$result[$key]['success'] = 1; 
						$result[$key]['error_data'] = array();
						$result[$key]['id'] = $value['id'];
						$this->db->trans_commit();
					}
				}
				
				$this->db->db_debug = true;
				echo serialize($result);
			}
			else
			{
				echo "Invalid user";exit;
			}
		}

	}			
}

/* End of file Sync.php */
/* Location: ./application/controllers/Sync.php */