<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_access_check_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		$this->load->database("inventory");
	}

	public function check_access($user_id,$pages_address)
	{
		$this->db->select('pages.pages_id,pages.pages_address,pages_permission.user_id');
		$this->db->from('pages_permission');
		$this->db->join('pages', 'pages.pages_id = pages_permission.pages_id', 'left');
		$this->db->where('pages_permission.user_id', $user_id);
		$this->db->where('pages.pages_address', $pages_address);
		$data = $this->db->get();
		return $data->row_array();
	}

}

/* End of file User_access_check_model.php */
/* Location: ./application/models/User_access_check_model.php */