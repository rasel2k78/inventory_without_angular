 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>

 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Sales Representative Report</p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Total Invoice
                    </th>
                    <th>
                        Amount
                    </th>
                </tr>
            </thead>
            <tbody >
                <?php foreach ($sales_rep_data as $v_sales_rep_data): ?>
                    <tr>
                        <?php if ($v_sales_rep_data['sales_rep_name'] == ""): ?>
                            <td>Empty</td>
                        <?php endif ?>
                        <?php if ($v_sales_rep_data['sales_rep_name'] != ""): ?>
                            <td>
                                <?php echo $v_sales_rep_data['sales_rep_name']?>
                            </td>
                        <?php endif ?>
                        <td>
                            <?php echo $v_sales_rep_data['total_sale_qty']?>
                        </td>
                        <td>
                        <?php echo round($v_sales_rep_data['total_sale_amt'], 2)?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
       <ul class="list-unstyled amounts">


       </ul>
       <br/>
       <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
    }
</style>
                <!-- END PAGE CONTENT