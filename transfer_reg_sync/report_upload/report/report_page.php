				<!-- BEGIN DASHBOARD STATS -->
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/select2/select2.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

				<!-- date picker css starts-->
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
				<!-- date picker css ends -->
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/custom/css/buttons.dataTables.min.css">



				
				<div class="form-group">
					<label class="control-label col-md-9"></label>
					<div class="col-md-3 ">
						<select class="form-control" id="shop_drop_down" name="shop_drop_down" >
							<option class="form-control" >Select a shop</option>
						</select>
					</div>
				</div><br><br>

				
				<div class="row" id="row_id">
					<div id="buy_list_bolck" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light red-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc col-md-12" style="padding-top: 20%; font-size: 20px">
									
									<?php echo $this->lang->line('today_total_buy'); ?> 
								</div>
							</div>
						</a>
					</div>

					<div id="sell_list_bolck" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc col-md-12" style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('today_total_sell'); ?> 
								</div>
							</div>
						</a>
					</div>
					
					<div id="top_sell_items" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light blue-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc col-md-12" style="padding-top: 15%; font-size: 20px">
									<?php echo $this->lang->line('top_sell_items_lang'); ?> 
									
								</div>
							</div>
						</a>
					</div>

					<div id="sales_rep_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc col-md-12" style="padding-top: 10%; font-size: 19px">
									<?php echo $this->lang->line('top_sales_rep_lang'); ?> 
									
								</div>
							</div>
						</a>
					</div>

					<div id="top_customers" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light red-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('top_customer_lang'); ?> 
									
								</div>
							</div>
						</a>
					</div>

					<div id="total_expenses_30_days" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('expenses_lang'); ?> 
									
								</div>
							</div>
						</a>
					</div>

					<div id="demage_lost_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light blue-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('demage_lost_lang'); ?> 
									
								</div>
							</div>
						</a>
					</div>
					
					<div id="top_vendor_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">							
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('vendor_lang'); ?> 								
								</div>
							</div>
						</a>
					</div>

					<div id="loan_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light blue-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">							
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('loan_lang'); ?> 							
								</div>
							</div>
						</a>
					</div>
					<div id="buy_due_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">							
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('buy_due_lang'); ?> 
								</div>
							</div>
						</a>
					</div>
					<div id="sell_due_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light purple-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('sell_due_lang'); ?> 
								</div>
							</div>
						</a>
					</div>
					<div id="custom_report" class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
						<a class="dashboard-stat dashboard-stat-light green-soft" href="javascript:;">
							<div class="visual">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="details">
								<div class="desc " style="padding-top: 20%; font-size: 20px">
									<?php echo $this->lang->line('custom_report'); ?> 
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="custom_report_body" style="display: none">
						<div class="portlet box red" id="custom_report_portlet">
							<div class="portlet-title">
								<div class="caption left">
									<i class="fa fa-pencil-square-o"></i><span id="form_header_text"><?php echo $this->lang->line('generate_report'); ?> </span>
								</div>

							</div>
							<div class="portlet-body form" id="custom_report_div">
								<?php $form_attrib = array('id' => 'custom_report_form','class' => 'form-horizontal form-bordered');
								echo form_open('Reports/print_report_preview',  $form_attrib, '');?>
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3"><?php echo $this->lang->line('report_type'); ?> <sup><i class="fa fa-star custom-required"></i></sup>
										</label>
										<div class="col-md-9">
											<select class="form-control" id="custom_report_type_id" name="cus_report_type">
												<option value="buy_report"><?php echo $this->lang->line('buy_report'); ?></option>
												<option value="sell_report"><?php echo $this->lang->line('sell_report'); ?></option>
												<option value="item_inventory"><?php echo $this->lang->line('item_report'); ?></option>
												<option value="vat_report"><?php echo $this->lang->line('vat_report'); ?></option>
												<option value="expense_report"><?php echo $this->lang->line('expense_report'); ?></option>
												<option value="damage_lost_report"><?php echo $this->lang->line('damage_lost_report'); ?></option>
												<option value="loan_report"><?php echo $this->lang->line('loan_report'); ?></option>
												<option value="dpst_wtdrl_report"><?php echo $this->lang->line('dpst_wtdrl_report'); ?></option>
												<option value="buy_due_report"><?php echo $this->lang->line('buy_due_report'); ?></option>
												<option value="sell_due_report"><?php echo $this->lang->line('sell_due_report'); ?></option>
												<option  value="top_categories"><?php echo $this->lang->line('top_cat'); ?></option>
												<option  value="top_rev_items"><?php echo $this->lang->line('top_revenue_items'); ?></option>
												<option value="profit_loss"><?php echo $this->lang->line('profit_revenue_report'); ?></option>
												<option value="all_customers"><?php echo $this->lang->line('all_cus'); ?></option>
												<option value="all_vendors"><?php echo $this->lang->line('all_vendor'); ?></option>
												<option value="tot_amt_inv_items"><?php echo $this->lang->line('total_amt_inventory_items'); ?></option>
												<option value="vendor_payment_history"><?php echo $this->lang->line('vendor_payment_history_lang'); ?></option>
												<option value="customer_payment_history"><?php echo $this->lang->line('cus_payment_history'); ?></option>
												<option value="available_imei_report"><?php echo $this->lang->line('imei_report'); ?></option>
												<option value="bank_account_report"><?php echo $this->lang->line('bank_report'); ?>
												</option>
												<option value="card_payment_report"><?php echo $this->lang->line('card_report'); ?>
												</option>
												<option value="individual_sales_rep_report">Sales Representative Report
												</option>
											</select>
										</div>
										<input type="hidden" value="" name="stores_id" id="str_id">
									</div>
									<div class="form-group" id="date_range_div">
										<label class="control-label col-md-3">	<?php echo $this->lang->line('date_range'); ?> 	 <sup><i class="fa fa-star custom-required"></i></sup></label>
										<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
											<input type="text" style="width: 100px" class="form-control dont-search" name="from_create_date">
											<span class="input-group-addon">
												to
											</span>
											<input type="text" style="width: 100px" class="form-control dont-search" name="to_create_date">

										</div>
									</div>
									<div class="form-group" id="search_limit" style="display: none">
										<label class="control-label col-md-3">	<?php echo $this->lang->line('limit'); ?> 	 <sup><i class="fa fa-star custom-required"></i></sup>
										</label>
										<div class="col-md-9">
											<select class="form-control" id="custom_search_limit" name="cus_seach_limit">
												<option value="10">	<?php echo $this->lang->line('limit_ten'); ?> 	</option>
												<option value="50">	<?php echo $this->lang->line('limit_fifty'); ?> 	</option>
												<option value="100">	<?php echo $this->lang->line('limit_hun'); ?> 	</option>
												<option value="200">	<?php echo $this->lang->line('limit_two_hun'); ?> 	</option>
												<option value="0">	<?php echo $this->lang->line('limit_all'); ?> 	</option>
											</select>
										</div>
									</div>
									<div class="form-group" id="vat_amount_id" style="display: none">
										<label class="control-label col-md-3">	<?php echo $this->lang->line('report_with_vat'); ?> 	<sup><i class="fa fa-star custom-required"></i></sup>
										</label>
										<div class="col-md-9">
											<select class="form-control" id="custom_search_limit" name="vat_amt_selection">
												<option value="no">No</option>
												<option value="yes">Yes</option>
											</select>
										</div>
									</div>
									<div class="form-group" style="display: none"; id="item_search_type_div">
										<label class="control-label col-md-3"><?php echo $this->lang->line('item_select');?></label>
										<div class="radio-list">
											<label class="radio-inline">
												<input type="radio" name="item_type" id="optionsRadios4" value="all" checked><?php echo $this->lang->line('all');?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="item_type" id="optionsRadios5" value="individual"><?php echo $this->lang->line('individual');?>
											</label>
										</div>
									</div>
									<div class="form-group" style="display: none"; id="vendor_search_field" >
										<label class="control-label col-md-3"><?php echo $this->lang->line('vendor_select');?> 
										</label>
										<div class="col-md-6">
											<input type="text" autocomplete="off" name="vendors_name" placeholder="<?php echo $this->lang->line('right_select_vendor_placeholder'); ?> " id="vendor_select" class="form-control">
											<span class="help-block" id="vendor_help_block" ></span>
											<input type="hidden" name="vendors_id"  class="form-control">
											<table class="table table-condensed table-hover table-bordered clickable" id="vendor_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
											</table>
										</div>
									</div>
									<div class="form-group" style="display: none"; id="customer_search_field" >
										<label class="control-label col-md-3"><?php echo $this->lang->line('select_customer');?>
										</label>
										<div class="col-md-6">
											<input type="text" autocomplete="off" name="customers_name" placeholder="Search Customer" id="customers_select" class="form-control">
											<span class="help-block" id="customer_help_block" ></span>
											<input type="hidden" autocomplete="off" name="customers_id"  class="form-control">
											<table class="table table-condensed table-hover table-bordered clickable" id="customers_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
											</table>
										</div>
									</div>
									<div class="form-group" style="display: none" id="item_search_div">
										<label class="control-label col-md-3"><?php echo $this->lang->line('search_item');?><sup><i class="fa fa-star custom-required"></i></sup>
										</label>
										<div class="col-md-6 input-form input-icon">
											<input type="text" name="items_name" placeholder="Search Item" id="item_select" class="form-control">
											<input type="hidden" name="items_id"  class="form-control">
											<table class="table table-condensed table-hover table-bordered clickable" id="item_select_result" style="position: absolute;z-index: 10;background-color: #fff;">
											</table>
											<span class="help-block" style="display:none"><?php echo $this->lang->line('left_item_name_placeholder'); ?></span>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit"  id="generate_report_id" class="btn green pull-right">	<?php echo $this->lang->line('generate_report'); ?> 	</button>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="row">
					<div id="buy_list_id" class="col-md-12" style="display: none">
						<div class="portlet box green-haze" id="buy_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('today_buy_list');?>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_4">
										<thead>
											<tr>
												<th>
													<?php echo $this->lang->line('buy_voucher_no');?>
												</th>
												<th><?php echo $this->lang->line('grand_total'); ?></th>
												<th><?php echo $this->lang->line('discount'); ?></th>
												<th><?php echo $this->lang->line('paid'); ?></th>
												<th><?php echo $this->lang->line('due'); ?></th>
												<th><?php echo $this->lang->line('vendor');?></th>
												<th><?php echo $this->lang->line('date'); ?></th>

											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('left_customer');?></th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control all_buy_list" placeholder="Bigger" id="less_than_for_buy_list">
														<br>
														<input type="number" class="form-control all_buy_list" placeholder="Less" id="greater_than_for_buy_list">
													</div>
												</th>
												<th><?php echo $this->lang->line('left_customer');?></th>
												<th><?php echo $this->lang->line('left_voucher_no');?></th>
												<th><?php echo $this->lang->line('left_customer');?></th>
												<th><?php echo $this->lang->line('vendor');?></th>
												<th>
													<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control dont-search" name="from_create_date">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control dont-search" name="to_create_date">
													</div>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>

					<div id="sell_list_id" class="col-md-12" style="display: none;">
						<div class="portlet box green-haze" id="sell_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('today_sell_list');?>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_3">
										<thead>
											<tr>
												<th>
													<?php echo $this->lang->line('sell_voucher_no');?>
												</th>
												<th><?php echo $this->lang->line('grand_total'); ?></th>
												<th><?php echo $this->lang->line('discount'); ?></th>
												<th><?php echo $this->lang->line('receive'); ?></th>
												<th><?php echo $this->lang->line('payment_type'); ?></th>
												<th><?php echo $this->lang->line('customer');?></th>
												<th><?php echo $this->lang->line('date'); ?></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('sell_voucher_no');?></th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control all_sell_list" placeholder="Bigger" id="less_than_for_sell_list">
														<br>
														<input type="number" class="form-control all_sell_list" placeholder="Less" id="greater_than_for_sell_list">
													</div>
												</th>
												<th><?php echo $this->lang->line('discount');?></th>
												<th><?php echo $this->lang->line('receive');?></th>
												<th><?php echo $this->lang->line('customer');?></th>
												<th><?php echo $this->lang->line('date');?></th>
												<th>
													<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control dont-search" name="from_create_date">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control dont-search" name="to_create_date">
													</div>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
					
					<div class="col-md-7" id="top_sell_items_list" style="display: none;">


						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green-haze" id="top_sale_items_portlet">
							<div class="portlet-title">
								<div class="caption">

									<i class="fa fa-list"></i><?php echo $this->lang->line('top_sell_items_lang');?>
								</div>

							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_6">
										<thead>
											<tr>
												<th><?php echo $this->lang->line('items_name');?></th>
												<th>
													<?php echo $this->lang->line('no_of_item_sold');?>
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('items_name');?></th>
												<th></th>

											</tr>
										</tfoot>

									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="loan_list" style="display: none;">
						<div class="portlet box green-haze" id="loan_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i>	<?php echo $this->lang->line('loan_list'); ?> 
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_61">
										<thead>
											<tr>
												<th>
													<?php echo $this->lang->line('loan_type'); ?> 
												</th>
												<th>
													<?php echo $this->lang->line('loan_amount'); ?> 
												</th>
												<th>
													<?php echo $this->lang->line('loan_paymentmade'); ?> 
												</th>
												<th>
													<?php echo $this->lang->line('date'); ?> 
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th>	<?php echo $this->lang->line('loan_type'); ?> </th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control amount_of_loan" placeholder="Bigger" id="less_than_for_loan_taken">
														<br>
														<input type="number" class="form-control amount_of_loan" placeholder="Less" id="greater_than_for_loan_taken">
													</div>
												</th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control amount_of_paymentmade" placeholder="Bigger" id="less_than_for_loan_paid">
														<br>
														<input type="number" class="form-control amount_of_paymentmade" placeholder="Less" id="greater_than_for_loan_paid">
													</div>
												</th>
												<th>
													<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control dont-search" name="from_create_date">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control dont-search" name="to_create_date">
													</div>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="top_sales_rep_list" style="display: none;">
						<div class="portlet box green-haze" id="top_sales_rep_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('top_sales_rep_lang');?>
								</div>

							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_7">
										<thead>
											<tr>
												<th><?php echo $this->lang->line('sales_rep_name');?></th>
												<th>

													<?php echo $this->lang->line('no_of_sale');?>
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('sales_rep_name');?></th>
												<th>	</th>
											</tr>
										</tfoot>

									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="top_customers_list" style="display: none;">
						<div class="portlet box green-haze" id="top_customers_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('top_customer_lang');?>
								</div>

							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_8">
										<thead>
											<tr>
												<th><?php echo $this->lang->line('customers_name');?></th>
												<th>
													<?php echo $this->lang->line('amount_of_buy');?>
												</th>
												<th>
													<?php echo $this->lang->line('no_of_buy');?>
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('customers_name');?></th>
												<th></th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="expenses_list" style="display: none;">
						<div class="portlet box green-haze" id="expenses_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('expenses_lang');?>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_9">
										<thead>
											<tr>
												<th><?php echo $this->lang->line('expenses_name_lang');?></th>
												<th>
													<?php echo $this->lang->line('expenses_amount_lang');?>
												</th>
												<th>
													<?php echo $this->lang->line('date');?>
												</th>
												<th>
													<?php echo $this->lang->line('expenses_details_lang');?>
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('expenses_name_lang');?></th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control amount_of_expense" placeholder="Bigger" id="less_than_for_expense">
														<br>
														<input type="number" class="form-control amount_of_expense" placeholder="Less" id="greater_than_for_expense">
													</div>
												</th>
												<th>
													<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control dont-search" name="from_create_date">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control dont-search" name="to_create_date">
													</div>
												</th>
												<th>
													<?php echo $this->lang->line('expenses_details_lang');?>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="demage_lost_list" style="display: none;">
						<div class="portlet box green-haze" id="demage_lost_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i>    <?php echo $this->lang->line('demage_lost_lang');?>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_10">
										<thead>
											<tr>
												<th>    <?php echo $this->lang->line('items_name');?></th>
												<th>
													<?php echo $this->lang->line('quantity');?>
												</th>
												<th>
													<?php echo $this->lang->line('date');?>
												</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr>
												<th><?php echo $this->lang->line('items_name');?></th>
												<th class="text-center">
													<div class="input-group ">
														<input type="number" class="form-control quantity_of_demage_lost" placeholder="Bigger" id="less_than_for_demage_lost">
														<br>
														<input type="number" class="form-control quantity_of_demage_lost" placeholder="Less" id="greater_than_for_demage_lost">
													</div>
												</th>
												<th>
													<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control dont-search" name="from_create_date">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control dont-search" name="to_create_date">
													</div>
												</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-7" id="buy_due_list" style="display: none;">
						<div class="portlet box green-haze" id="buy_due_list_portlet">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-list"></i><?php echo $this->lang->line('buy_due_lang');?>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix table-responsive">
									<table class="table table-striped table-bordered table-hover" id="sample_62">
										<thead>
											<tr>
												<th>
													<?php echo $this->lang->line('vendor');?>
												</th>
												<th>
													<?php echo $this->lang->line('sell_voucher_no');?>.
												</th>
												<th>
													<?php echo $this->lang->line('grand_total');?>
												</th>
												<th>
													<?php echo $this->lang->line('paid');?>												</th>
													<th>
														<?php echo $this->lang->line('due');?>												</th>
														<th>
															<?php echo $this->lang->line('date');?>												</th>

														</tr>
													</thead>
													<tbody>
													</tbody>
													<tfoot>
														<tr>
															<th>	<?php echo $this->lang->line('vendor');?></th>
															<th>
																<?php echo $this->lang->line('sell_voucher_no');?>.
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control grand_total_for_buy_due" placeholder="Bigger" id="less_than_grand_total_for_buy_due">
																	<br>
																	<input type="number" class="form-control grand_total_for_buy_due" placeholder="Less" id="greater_than_grand_total_for_buy_due">
																</div>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control paid_for_buy_due" placeholder="Bigger" id="less_than_paid_for_buy_due">
																	<br>
																	<input type="number" class="form-control paid_for_buy_due" placeholder="Less" id="greater_than_paid_for_buy_due">
																</div>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control due_for_buy_due" placeholder="Bigger" id="less_than_due_for_buy_due">
																	<br>
																	<input type="number" class="form-control due_for_buy_due" placeholder="Less" id="greater_than_due_for_buy_due">
																</div>
															</th>
															<th>
																<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control dont-search" name="from_create_date">
																	<span class="input-group-addon">
																		to
																	</span>
																	<input type="text" class="form-control dont-search" name="to_create_date">
																</div>
															</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-7" id="sell_due_list" style="display: none;">
									<div class="portlet box green-haze" id="sell_due_list_portlet">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-list"></i><?php echo $this->lang->line('sell_due_lang');?>
											</div>
										</div>
										<div class="portlet-body">
											<div class="clearfix table-responsive">
												<table class="table table-striped table-bordered table-hover" id="sample_63">
													<thead>
														<tr>
															<th>
																<?php echo $this->lang->line('customer');?>
															</th>
															<th>
																<?php echo $this->lang->line('sell_voucher_no');?>
															</th>
															<th>
																<?php echo $this->lang->line('grand_total');?>
															</th>
															<th>
																<?php echo $this->lang->line('paid');?>
															</th>
															<th>
																<?php echo $this->lang->line('due');?>
															</th>
															<th>
																<?php echo $this->lang->line('date');?>
															</th>

														</tr>
													</thead>
													<tbody>
													</tbody>
													<tfoot>
														<tr>
															<th><?php echo $this->lang->line('customer');?></th>
															<th>
																<?php echo $this->lang->line('sell_voucher_no');?>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control grand_total_for_sell_due" placeholder="Bigger" id="less_than_grand_total_for_sell_due">
																	<br>
																	<input type="number" class="form-control grand_total_for_sell_due" placeholder="Less" id="greater_than_grand_total_for_sell_due">
																</div>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control paid_for_sell_due" placeholder="Bigger" id="less_than_paid_for_sell_due">
																	<br>
																	<input type="number" class="form-control paid_for_sell_due" placeholder="Less" id="greater_than_paid_for_sell_due">
																</div>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control due_for_sell_due" placeholder="Bigger" id="less_than_due_for_sell_due">
																	<br>
																	<input type="number" class="form-control due_for_sell_due" placeholder="Less" id="greater_than_due_for_sell_due">
																</div>
															</th>
															<th>
																<div class="input-group input-medium date-picker input-daterange" data-autoclose="true" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control dont-search" name="from_create_date">
																	<span class="input-group-addon">
																		to
																	</span>
																	<input type="text" class="form-control dont-search" name="to_create_date">
																</div>
															</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div id="vendor_list_id" class="col-md-7" style="display: none">
									<div class="portlet box green-haze" id="vendor_list_portlet">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-list"></i><?php echo $this->lang->line('vendor_lang');?>
											</div>
										</div>
										<div class="portlet-body">
											<div class="clearfix table-responsive">
												<table class="table table-striped table-bordered table-hover" id="sample_11">
													<thead>
														<tr>
															<th>
																<?php echo $this->lang->line('vendor_name');?>
															</th>
															<th><?php echo $this->lang->line('amount_of_buy'); ?></th>
															<th><?php echo $this->lang->line('no_of_buy'); ?></th>

														</tr>
													</thead>
													<tbody>
													</tbody>
													<tfoot>
														<tr>
															<th></th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control total_amount_purchase" placeholder="Bigger" id="less_than">
																	<br>
																	<input type="number" class="form-control total_amount_purchase" placeholder="Less" id="greater_than">
																</div>
															</th>
															<th class="text-center">
																<div class="input-group ">
																	<input type="number" class="form-control number_of_purchases" placeholder="Bigger" id="less_than_for_purchase">
																	<br>
																	<input type="number" class="form-control number_of_purchases" placeholder="Less" id="greater_than_for_purchase">
																</div>
															</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/select2/select2.min.js"></script>

							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/jquery.dataTables.min.js"></script>
							<!-- date picker js starts -->
							<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
							<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
							<!-- date picker js ends -->

							<!-- Boostrap modal starts-->
							<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
							<script src="<?php echo base_url();?>assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
							<script src="<?php echo base_url();?>assets/admin/pages/scripts/ui-extended-modals.js"></script>
							<!-- Boostrap modal end -->
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

							<!-- script for datatable export buttoon start -->
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/dataTables.buttons.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/jszip.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/pdfmake.min.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/vfs_fonts.js"></script>
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/buttons.html5.min.js"></script>
							<!-- script for datatable export buttoon end -->

							<!-- Script for print button -->
							<script type="text/javascript" src="<?php echo base_url();?>assets/custom/js/buttons.print.min.js"></script>
							<!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.js"></script>	 -->
							<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->



							<script type="text/javascript">

								jQuery(document).ready(function() {    
									$('.date-picker').datepicker();
									UIExtendedModals.init();
								});



								/* ********************** */
								/* All buy list for last 30 days*/
								/* ********************** */

								$(document).on('click', '#buy_list_bolck', function(event) {
									event.preventDefault();

									var check_empty_shop =  $('#shop_drop_down').val();
									if(check_empty_shop == "Select a shop"){
										alert("<?php echo $this->lang->line('select_shop'); ?>");
										return false;
									}

									$('#sample_4 tfoot th').each(function (idx,elm){
										if (idx == 0  || idx == 2 || idx == 3 || idx == 4 || idx == 5 ) { 
											var title = $('#example tfoot th').eq( $(this).index() ).text();
											$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
										}
									});
									var table_buy_list_all = $('#sample_4').DataTable({
										"processing": true,
										"serverSide": true,
										"destroy": true,
										"dom" : 'Bfrltip',
										"buttons" : [
										'copyHtml5',
										'excelHtml5',
										'csvHtml5',
										'pdfHtml5',
										'print'
										],
										"ajax": "<?php echo site_url(); ?>/Reports/monthly_buy_info_for_datatable/"+$('#shop_drop_down').val(),
										"lengthMenu": [
										[50, 100, 200,0],
										[50, 100, 200,"All"] 
										],
										"pageLength": 50,
										"language": {
											"lengthMenu": " _MENU_ records",
											"paging": {
												"previous": "Prev",
												"next": "Next"
											}
										},
										"columnDefs": [{  
											'orderable': true,
											'targets': [0]
										}, {
											"searchable": true,
											"targets": [0]
										}],
										"order": [
										[6, "desc"]
										]
									});    
									table_buy_list_all.columns().every( function () {
										var that = this;
										$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
											if (!($(this).hasClass('dont-search'))) {
												that
												.search( this.value )
												.draw();
											}
										});
										$( '.all_buy_list', this.footer() ).on( 'keyup change', function (event) {
											var less_than_val = $('#less_than_for_buy_list').val();
											var greater_than_val = $('#greater_than_for_buy_list').val();
											var final_string = '';
											if(less_than_val !='' || greater_than_val != ''){
												if (less_than_val!='' && greater_than_val=='') {
													final_string = less_than_val+'_0';
												}
												if (greater_than_val!='' && less_than_val=='') {
													final_string = '0_'+greater_than_val;
												}
												if (less_than_val!='' && greater_than_val!='') {
													final_string = less_than_val+'_'+greater_than_val;
												}
											}
											else{
												final_string = '0_0';
											}
											that.search(final_string).draw();
										});
										$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
											var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]', that.footer()).val();
											var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]', that.footer()).val();
											var final_string = '';
											if(date_from !='' || date_to != ''){
												if (date_from!='' && date_to=='') {
													final_string = date_from+'_0';
												}
												if (date_to!='' && date_from=='') {
													final_string = '0_'+date_to;
												}
												if (date_from!='' && date_to!='') {
													final_string = date_from+'_'+date_to;
												}
											}
											else{
												final_string = '0_0';
											}
											that.search(final_string).draw();
										});
									});
									$('.portlet').hide();
									$('#buy_list_id').show();
									$('#buy_list_portlet').show();
								});
								/* ********************** */
								/* All sell list for last 30 days*/
								/* ********************** */

								$(document).on('click', '#sell_list_bolck', function(event) {
									event.preventDefault();

									var check_empty_shop =  $('#shop_drop_down').val();
									if(check_empty_shop == "Select a shop"){
										alert("<?php echo $this->lang->line('select_shop'); ?>");
										return false;
									}

									$('#sample_3 tfoot th').each(function (idx,elm){
										if (idx == 0  || idx == 2 || idx == 3 || idx == 4 || idx == 5 ) { 
											var title = $('#example tfoot th').eq( $(this).index() ).text();
											$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
										}
									});
									var table_sell_list_all = $('#sample_3').DataTable({
										"processing": true,
										"serverSide": true,
										"destroy": true,
										"dom" : 'Bfrltip',
										"buttons" : [
										'copyHtml5',
										'excelHtml5',
										'csvHtml5',
										'pdfHtml5',
										'print'
										],
										"ajax": "<?php echo site_url(); ?>/Reports/monthly_sell_info_for_datatable/"+$('#shop_drop_down').val(),
										"lengthMenu": [
										[50, 100, 200,0],
										[50, 100, 200,"All"] 
										],
										"pageLength": 50,
										"language": {
											"lengthMenu": " _MENU_ records",
											"paging": {
												"previous": "Prev",
												"next": "Next"
											}
										},
										"columnDefs": [{  
											'orderable': true,
											'targets': [0]
										}, {
											"searchable": true,
											"targets": [0]
										}],
										"order": [
										[6, "desc"]
										]
									});    
									table_sell_list_all.columns().every( function () {
										var that = this;
										$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
											if (!($(this).hasClass('dont-search'))) {
												that
												.search( this.value )
												.draw();
											}
										});
										$( '.all_sell_list', this.footer() ).on( 'keyup change', function (event) {
											var less_than_val = $('#less_than_for_sell_list').val();
											var greater_than_val = $('#greater_than_for_sell_list').val();
											var final_string = '';

											if(less_than_val !='' || greater_than_val != ''){
												if (less_than_val!='' && greater_than_val=='') {
													final_string = less_than_val+'_0';
												}
												if (greater_than_val!='' && less_than_val=='') {
													final_string = '0_'+greater_than_val;
												}
												if (less_than_val!='' && greater_than_val!='') {
													final_string = less_than_val+'_'+greater_than_val;
												}
											}
											else{
												final_string = '0_0';
											}
											that.search(final_string).draw();
										});
										$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
											var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]', that.footer()).val();
											var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]', that.footer()).val();
											var final_string = '';
											if(date_from !='' || date_to != ''){
												if (date_from!='' && date_to=='') {
													final_string = date_from+'_0';
												}
												if (date_to!='' && date_from=='') {
													final_string = '0_'+date_to;
												}
												if (date_from!='' && date_to!='') {
													final_string = date_from+'_'+date_to;
												}
											}
											else{
												final_string = '0_0';
											}
											that.search(final_string).draw();
										});
									});
									$('.portlet').hide();
									$('#sell_list_id').show();
									$('#sell_list_portlet').show();
								});


//for sell history end


/* ********************** */
/* Loan Report */
/* ********************** */
$(document).on('click', '#loan_report', function(event) {
	event.preventDefault();
	var check_empty_shop =  $('#shop_drop_down').val();
	if(check_empty_shop == "Select a shop"){
		alert("<?php echo $this->lang->line('select_shop'); ?>");
		return false;
	}

	$('#sample_61 tfoot th').each(function (idx,elm){
		if (idx == 0) { 
			var title = $('#example tfoot th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
		}
	});
	var table_loan_list_all = $('#sample_61').DataTable({
		"processing": true,
		"serverSide": true,
		"destroy": true,
		"dom" : 'Bfrltip',
		"buttons" : [
		'copyHtml5',
		'excelHtml5',
		'csvHtml5',
		'pdfHtml5',
		'print'
		],
		"ajax": "<?php echo site_url(); ?>/Reports/all_loan_list/"+$('#shop_drop_down').val(),
		"lengthMenu": [
		[50, 100, 200,0],
		[50, 100, 200,"All"] 
		],
		"pageLength": 50,
		"language": {
			"lengthMenu": " _MENU_ records",
			"paging": {
				"previous": "Prev",
				"next": "Next"
			}
		},
		"columnDefs": [{  
			'orderable': true,
			'targets': [0]
		}, {
			"searchable": true,
			"targets": [0]
		}],
		"order": [
		[3, "desc"]
		]
	});	
	table_loan_list_all.columns().every( function () {
		var that = this;
		$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
			if (!($(this).hasClass('dont-search'))) {
				that
				.search( this.value )
				.draw();
			}
		});
		$( '.amount_of_paymentmade', this.footer() ).on( 'keyup change', function (event) {
			var less_than_val = $('#less_than_for_loan_paid').val();
			var greater_than_val = $('#greater_than_for_loan_paid').val();
			var final_string = '';

			if(less_than_val !='' || greater_than_val != ''){
				if (less_than_val!='' && greater_than_val=='') {
					final_string = less_than_val+'_0';
				}
				if (greater_than_val!='' && less_than_val=='') {
					final_string = '0_'+greater_than_val;
				}
				if (less_than_val!='' && greater_than_val!='') {
					final_string = less_than_val+'_'+greater_than_val;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
		$( '.amount_of_loan', this.footer() ).on( 'keyup change', function (event) {
			var less_than_val = $('#less_than_for_loan_taken').val();
			var greater_than_val = $('#greater_than_for_loan_taken').val();
			var final_string = '';

			if(less_than_val !='' || greater_than_val != ''){
				if (less_than_val!='' && greater_than_val=='') {
					final_string = less_than_val+'_0';
				}
				if (greater_than_val!='' && less_than_val=='') {
					final_string = '0_'+greater_than_val;
				}
				if (less_than_val!='' && greater_than_val!='') {
					final_string = less_than_val+'_'+greater_than_val;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
		$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
			var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]',that.footer()).val();
			var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]',that.footer()).val();

			var final_string = '';
			if(date_from !='' || date_to != ''){
				if (date_from!='' && date_to=='') {
					final_string = date_from+'_0';
				}
				if (date_to!='' && date_from=='') {
					final_string = '0_'+date_to;
				}
				if (date_from!='' && date_to!='') {
					final_string = date_from+'_'+date_to;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
	});
	$('.portlet').hide();
	$('#loan_list').show();
	$('#loan_list_portlet').show();

});

/* ********************** */
/* Buy Due Report */
/* ********************** */
$(document).on('click', '#buy_due_report', function(event) {
	event.preventDefault();
	var check_empty_shop =  $('#shop_drop_down').val();
	if(check_empty_shop == "Select a shop"){
		alert("<?php echo $this->lang->line('select_shop'); ?>");
		return false;
	}

	$('#sample_62 tfoot th').each(function (idx,elm){
		if (idx == 0 || idx == 1) { 
			var title = $('#example tfoot th').eq( $(this).index() ).text();
			$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
		}
	});
	var table_buy_due_list_all = $('#sample_62').DataTable({
		"processing": true,
		"serverSide": true,
		"destroy": true,
		"dom" : 'Bfrltip',
		"buttons" : [
		'copyHtml5',
		'excelHtml5',
		'csvHtml5',
		'pdfHtml5',
		'print'
		],
		"ajax": "<?php echo site_url(); ?>/Reports/all_buy_due_list/"+$('#shop_drop_down').val(),
		"lengthMenu": [
		[50, 100, 200,0],
		[50, 100, 200,"All"] 
		],
		"pageLength": 50,
		"language": {
			"lengthMenu": " _MENU_ records",
			"paging": {
				"previous": "Prev",
				"next": "Next"
			}
		},
		"columnDefs": [{  
			'orderable': true,
			'targets': [0]
		}, {
			"searchable": true,
			"targets": [0]
		}],
		"order": [
		[5, "desc"]
		]
	});	
	table_buy_due_list_all.columns().every( function () {
		var that = this;
		$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
			if (!($(this).hasClass('dont-search'))) {
				that
				.search( this.value )
				.draw();
			}
		});
		$( '.grand_total_for_buy_due', this.footer() ).on( 'keyup change', function (event) {
			var less_than_val = $('#less_than_grand_total_for_buy_due').val();
			var greater_than_val = $('#greater_than_grand_total_for_buy_due').val();
			var final_string = '';
			if(less_than_val !='' || greater_than_val != ''){
				if (less_than_val!='' && greater_than_val=='') {
					final_string = less_than_val+'_0';
				}
				if (greater_than_val!='' && less_than_val=='') {
					final_string = '0_'+greater_than_val;
				}
				if (less_than_val!='' && greater_than_val!='') {
					final_string = less_than_val+'_'+greater_than_val;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
		$( '.paid_for_buy_due', this.footer() ).on( 'keyup change', function (event) {
			var less_than_val = $('#less_than_paid_for_buy_due').val();
			var greater_than_val = $('#greater_than_paid_for_buy_due').val();
			var final_string = '';
			if(less_than_val !='' || greater_than_val != ''){
				if (less_than_val!='' && greater_than_val=='') {
					final_string = less_than_val+'_0';
				}
				if (greater_than_val!='' && less_than_val=='') {
					final_string = '0_'+greater_than_val;
				}
				if (less_than_val!='' && greater_than_val!='') {
					final_string = less_than_val+'_'+greater_than_val;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
		$( '.due_for_buy_due', this.footer() ).on( 'keyup change', function (event) {
			var less_than_val = $('#less_than_due_for_buy_due').val();
			var greater_than_val = $('#greater_than_due_for_buy_due').val();
			var final_string = '';
			if(less_than_val !='' || greater_than_val != ''){
				if (less_than_val!='' && greater_than_val=='') {
					final_string = less_than_val+'_0';
				}
				if (greater_than_val!='' && less_than_val=='') {
					final_string = '0_'+greater_than_val;
				}
				if (less_than_val!='' && greater_than_val!='') {
					final_string = less_than_val+'_'+greater_than_val;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
		$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
			var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]',that.footer()).val();
			var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]',that.footer()).val();
			var final_string = '';
			if(date_from !='' || date_to != ''){
				if (date_from!='' && date_to=='') {
					final_string = date_from+'_0';
				}
				if (date_to!='' && date_from=='') {
					final_string = '0_'+date_to;
				}
				if (date_from!='' && date_to!='') {
					final_string = date_from+'_'+date_to;
				}
			}
			else{
				final_string = '0_0';
			}
			that.search(final_string).draw();
		});
	});
	$('.portlet').hide();
	$('#buy_due_list').show();
	$('#buy_due_list_portlet').show();

});

				/* ********************** */
				/* Sell Due Report */
				/* ********************** */
				$(document).on('click', '#sell_due_report', function(event) {
					event.preventDefault();
					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}
					$('#sample_63 tfoot th').each(function (idx,elm){
						if (idx == 0 || idx == 1) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});
					var table_sell_due_list_all = $('#sample_63').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5',
						'print'
						],
						"ajax": "<?php echo site_url(); ?>/Reports/all_sell_due_list/"+$('#shop_drop_down').val(),
						"lengthMenu": [
						[50, 100, 200,0],
						[50, 100, 200,"All"] 
						],
						"pageLength": 50,
						"language": {
							"lengthMenu": " _MENU_ records",
							"paging": {
								"previous": "Prev",
								"next": "Next"
							}
						},
						"columnDefs": [{  
							'orderable': true,
							'targets': [0]
						}, {
							"searchable": true,
							"targets": [0]
						}],
						"order": [
						[5, "desc"]
						]
					});	
					table_sell_due_list_all.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});
						$( '.grand_total_for_sell_due', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_grand_total_for_sell_due').val();
							var greater_than_val = $('#greater_than_grand_total_for_sell_due').val();
							var final_string = '';
							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$( '.paid_for_sell_due', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_paid_for_sell_due').val();
							var greater_than_val = $('#greater_than_paid_for_sell_due').val();
							var final_string = '';
							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$( '.due_for_sell_due', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_due_for_sell_due').val();
							var greater_than_val = $('#greater_than_due_for_sell_due').val();
							var final_string = '';
							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
							var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]',that.footer()).val();
							var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]',that.footer()).val();
							var final_string = '';
							if(date_from !='' || date_to != ''){
								if (date_from!='' && date_to=='') {
									final_string = date_from+'_0';
								}
								if (date_to!='' && date_from=='') {
									final_string = '0_'+date_to;
								}
								if (date_from!='' && date_to!='') {
									final_string = date_from+'_'+date_to;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
					});
					$('.portlet').hide();
					$('#sell_due_list').show();
					$('#sell_due_list_portlet').show();

				});

				/* *************************  */
				/* Report of top selling items*/
				/* *************************  */

				$(document).on('click', '#top_sell_items', function(event) {
					event.preventDefault();

					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}

					/* Top Sell Items Report Starts*/
					$('#sample_6 tfoot th').each(function (idx,elm){
						if (idx == 0    ) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});

					var table_top_sell_item_list = $('#sample_6').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5',
						'print'
						],
						"ajax": "<?php echo site_url(); ?>/Reports/all_top_sell_items_report/"+$('#shop_drop_down').val(),
						"lengthMenu": [
						[50, 100, 200,0],
						[50, 100, 200,"All"] 
						],
						"pageLength": 50,
						"language": {
							"lengthMenu": " _MENU_ records",
							"paging": {
								"previous": "Prev",
								"next": "Next"
							}
						},
						"columnDefs": [{  
							'orderable': true,
							'targets': [0]
						}, {
							"searchable": true,
							"targets": [0]
						}],
						"order": [
						[1, "desc"]
						]
					});	


					table_top_sell_item_list.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});

					});

					$('.portlet').hide();
					$('#top_sell_items_list').show();
					$('#top_sale_items_portlet').show();
				});



				/* ************************* */
				/*  Top Sales Representative */ 
				/* ************************* */


				$(document).on('click', '#sales_rep_report', function(event) {
					event.preventDefault();

					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}

					$('#sample_7 tfoot th').each(function (idx,elm){
						if (idx == 0    ) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});

					var table_top_sales_rep_list = $('#sample_7').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5',
						'print'
						],
						"ajax": "<?php echo site_url(); ?>/Reports/all_top_sales_rep_report/"+$('#shop_drop_down').val(),
						"lengthMenu": [
						[50, 100, 200,0],
						[50, 100, 200,"All"] 
						],
						"pageLength": 50,
						"language": {
							"lengthMenu": " _MENU_ records",
							"paging": {
								"previous": "Prev",
								"next": "Next"
							}
						},
						"columnDefs": [{  
							'orderable': true,
							'targets': [0]
						}, {
							"searchable": true,
							"targets": [0]
						}],
						"order": [
						[1, "desc"]
						]
					});	


					table_top_sales_rep_list.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});
					});

					$('.portlet').hide();
					$('#top_sales_rep_list').show();
					$('#top_sales_rep_portlet').show();
				});



				/* ************************* */
				/*  Top Customers */ 
				/* ************************* */


				$(document).on('click', '#top_customers', function(event) {
					event.preventDefault();

					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}

					$('#sample_8 tfoot th').each(function (idx,elm){
						if (idx == 0    ) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});

					var table_top_customers_list = $('#sample_8').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5',
						'print'
						],
						"ajax": "<?php echo site_url(); ?>/Reports/all_top_customers_report/"+$('#shop_drop_down').val(),
						"lengthMenu": [
						[50, 100, 200,0],
						[50, 100, 200,"All"] 
						],
						"pageLength": 50,
						"language": {
							"lengthMenu": " _MENU_ records",
							"paging": {
								"previous": "Prev",
								"next": "Next"
							}
						},
						"columnDefs": [{  
							'orderable': true,
							'targets': [0]
						}, {
							"searchable": true,
							"targets": [0]
						}],
						"order": [
						[1, "desc"]
						]
					});	


					table_top_customers_list.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});
					});

					$('.portlet').hide();
					$('#top_customers_list').show();
					$('#top_customers_portlet').show();

				});


				/* ************************* */
				/*  Total Expenses for 30 Days */ 
				/* ************************* */


				$(document).on('click', '#total_expenses_30_days', function(event) {
					event.preventDefault();


					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}


					$('#sample_9 tfoot th').each(function (idx,elm){
						if (idx == 0 || idx == 3 ) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});
					var table_expenses_list_all = $('#sample_9').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
        // 'copyHtml5',
        // 'excelHtml5',
        // 'csvHtml5',
        // 'pdfHtml5',
        // 'print'
        ],
        "ajax": "<?php echo site_url(); ?>/Reports/all_expenses_list/"+$('#shop_drop_down').val(),
        "lengthMenu": [
        [50, 100, 200,0],
        [50, 100, 200,"All"] 
        ],
        "pageLength": 50,
        "language": {
        	"lengthMenu": " _MENU_ records",
        	"paging": {
        		"previous": "Prev",
        		"next": "Next"
        	}
        },
        "columnDefs": [{  
        	'orderable': true,
        	'targets': [0]
        }, {
        	"searchable": true,
        	"targets": [0]
        }],
        "order": [
        [2, "desc"]
        ]
    });    
					table_expenses_list_all.columns().every( function (idx,elm) {
						var that = this;

						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});

						$( '.amount_of_expense', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_for_expense').val();
							var greater_than_val = $('#greater_than_for_expense').val();
							var final_string = '';

							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
							var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]', that.footer()).val();
							var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]', that.footer()).val();
							var final_string = '';
							if(date_from !='' || date_to != ''){
								if (date_from!='' && date_to=='') {
									final_string = date_from+'_0';
								}
								if (date_to!='' && date_from=='') {
									final_string = '0_'+date_to;
								}
								if (date_from!='' && date_to!='') {
									final_string = date_from+'_'+date_to;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
					});
					$('.portlet').hide();
					$('#expenses_list').show();
					$('#expenses_list_portlet').show();
				});


				/* ********************** */
				/* Last 30 days demage and lost report*/
				/* ********************** */


				$(document).on('click', '#demage_lost_report', function(event) {
					event.preventDefault();

					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}

					$('#sample_10 tfoot th').each(function (idx,elm){
						if (idx == 0) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});
					var table_demage_lost_list_all = $('#sample_10').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
        // 'copyHtml5',
        // 'excelHtml5',
        // 'csvHtml5',
        // 'pdfHtml5',
        // 'print'
        ],
        "ajax": "<?php echo site_url(); ?>/Reports/all_demage_lost_list/"+$('#shop_drop_down').val(),
        "lengthMenu": [
        [50, 100, 200,0],
        [50, 100, 200,"All"] 
        ],
        "pageLength": 50,
        "language": {
        	"lengthMenu": " _MENU_ records",
        	"paging": {
        		"previous": "Prev",
        		"next": "Next"
        	}
        },
        "columnDefs": [{  
        	'orderable': true,
        	'targets': [0]
        }, {
        	"searchable": true,
        	"targets": [0]
        }],
        "order": [
        [2, "desc"]
        ]
    });    
					table_demage_lost_list_all.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});
						$( '.quantity_of_demage_lost', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_for_demage_lost').val();
							var greater_than_val = $('#greater_than_for_demage_lost').val();
							var final_string = '';

							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$('.input-group.input-medium.date-picker.input-daterange input', this.footer() ).on( 'change', function (){
							var date_from = $('.input-group.input-medium.date-picker.input-daterange input[name="from_create_date"]', that.footer()).val();
							var date_to = $('.input-group.input-medium.date-picker.input-daterange input[name="to_create_date"]', that.footer()).val();
							var final_string = '';
							if(date_from !='' || date_to != ''){
								if (date_from!='' && date_to=='') {
									final_string = date_from+'_0';
								}
								if (date_to!='' && date_from=='') {
									final_string = '0_'+date_to;
								}
								if (date_from!='' && date_to!='') {
									final_string = date_from+'_'+date_to;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
					});
					$('.portlet').hide();
					$('#demage_lost_list').show();
					$('#demage_lost_list_portlet').show();
				});

				/* ********************** */
				/* top vendors  report*/
				/* ********************** */
				$(document).on('click', '#top_vendor_report', function(event) {
					event.preventDefault();
					var check_empty_shop =  $('#shop_drop_down').val();
					if(check_empty_shop == "Select a shop"){
						alert("<?php echo $this->lang->line('select_shop'); ?>");
						return false;
					}

					$('#sample_11 tfoot th').each(function (idx,elm){
						if (idx == 0) { 
							var title = $('#example tfoot th').eq( $(this).index() ).text();
							$(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );
						}
					});
					var table_vendor_list_all = $('#sample_11').DataTable({
						"processing": true,
						"serverSide": true,
						"destroy": true,
						"dom" : 'Bfrltip',
						"buttons" : [
						'copyHtml5',
						'excelHtml5',
						'csvHtml5',
						'pdfHtml5',
						'print'
						],
						"ajax": "<?php echo site_url(); ?>/Reports/monthly_vendor_info/"+$('#shop_drop_down').val(),
						"lengthMenu": [
						[50, 100, 200,0],
						[50, 100, 200,"All"] 
						],
						"pageLength": 50,
						"language": {
							"lengthMenu": " _MENU_ records",
							"paging": {
								"previous": "Prev",
								"next": "Next"
							}
						},
						"columnDefs": [{  
							'orderable': true,
							'targets': [0]
						}, {
							"searchable": true,
							"targets": [0]
						}],
						"order": [
						[1, "asc"]
						]
					});	
					table_vendor_list_all.columns().every( function () {
						var that = this;
						$('input[type="text"]', this.footer() ).on( 'keyup change', function () {
							if (!($(this).hasClass('dont-search'))) {
								that
								.search( this.value )
								.draw();
							}
						});
						$( '.total_amount_purchase', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than').val();
							var greater_than_val = $('#greater_than').val();
							var final_string = '';
							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
						$( '.number_of_purchases', this.footer() ).on( 'keyup change', function (event) {
							var less_than_val = $('#less_than_for_purchase').val();
							var greater_than_val = $('#greater_than_for_purchase').val();
							var final_string = '';
							if(less_than_val !='' || greater_than_val != ''){
								if (less_than_val!='' && greater_than_val=='') {
									final_string = less_than_val+'_0';
								}
								if (greater_than_val!='' && less_than_val=='') {
									final_string = '0_'+greater_than_val;
								}
								if (less_than_val!='' && greater_than_val!='') {
									final_string = less_than_val+'_'+greater_than_val;
								}
							}
							else{
								final_string = '0_0';
							}
							that.search(final_string).draw();
						});
					});
					$('.portlet').hide();
					$('#vendor_list_id').show();
					$('#vendor_list_portlet').show();
				});
// Code for get all shop for dropdown
$.get('<?php echo site_url('/');?>Reports/getAllShop', function(data) {
	var shop_html = "";
	var shop_data = $.parseJSON(data);

	$.each(shop_data, function(index, val) {
		shop_html+= '<option class="form-control"  value="'+val.shop_id+'"  >'+val.name+'</option>';
	});
	$('#shop_drop_down').append(shop_html);
});

$('#custom_report_type_id').on('change', function() {
	if (this.value == "item_inventory" || this.value == "tot_amt_inv_items" || this.value == "available_imei_report" || this.value == "bank_account_report" ) {
		$('#item_search_type_div').hide();
		$('#date_range_div').hide();
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$('#vendor_search_field').hide();
		$('#customer_search_field').hide();
	}
	else if(this.value == "individual_sales_rep_report"){
		$('#item_search_type_div').hide();
		$('#date_range_div').show();
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$('#vendor_search_field').hide();
		$('#customer_search_field').hide();
	}

	else if(this.value == "all_customers" || this.value == "all_vendors"){
		$('#date_range_div').hide();
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$('#vendor_search_field').hide();
		$('#customer_search_field').hide();

	}
	else if (this.value == "top_rev_items" || this.value == "top_categories") {
		$('#search_limit').show();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$("input:radio[name='item_type'][value ='all']").prop('checked', true).click();
		$('#date_range_div').show();
		$('#vendor_search_field').hide();
		$('#customer_search_field').hide();

	}
	else if(this.value == "profit_loss"){
		$('#vat_amount_id').show();
		$('#search_limit').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$("input:radio[name='item_type'][value ='all']").prop('checked', true).click();
		$('#date_range_div').show();
		$('#vendor_search_field').hide();
		$('#customer_search_field').hide();

	}
	else if(this.value == "vendor_payment_history"){
		$('#date_range_div').hide();
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast');
		$('#customer_search_field').hide();
		$('#vendor_search_field').show();
	}
	else if(this.value == "customer_payment_history"){
		$('#date_range_div').hide();
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast');
		$('#customer_search_field').show();
		$('#vendor_search_field').hide();

	}
	else{
		$('#search_limit').hide();
		$('#vat_amount_id').hide();
		$('#item_search_type_div').hide();
		$('#item_search_div').hide('fast'); 
		$("input:radio[name='item_type'][value ='all']").prop('checked', true).click();
		$('#date_range_div').show();
	}
});
// var check_empty_shop =  $('#shop_drop_down').val();
// 					if(check_empty_shop != "Select a shop"){
// $('#str_id').val(check_empty_shop);
					// }
					$('#row_id').on('click', '#custom_report', function(event) {
						event.preventDefault();
						var check_empty_shop =  $('#shop_drop_down').val();
						if(check_empty_shop == "Select a shop"){
							alert("Select A Shop First");
							return false;
						}
						else{

							$('#str_id').val(check_empty_shop);		
						}

						$('.portlet').hide();
						$('#custom_report_body').show();
						$('#custom_report_portlet').show();
					});

					var timer;
					var csrf = $("input[name='csrf_test_name']").val();
					$("#vendor_select").keyup(function(event) 
					{
						$("#vendor_select_result").show();
						$("#vendor_select_result").html('');
						clearTimeout(timer);
						timer = setTimeout(function() 
						{
							var search_vendor = $("#vendor_select").val();
							var html = '';
							$.post('<?php echo site_url(); ?>/Reports/search_vendor_by_name',{q: search_vendor,csrf_test_name: csrf}, function(data, textStatus, xhr) {
								data = JSON.parse(data);
								$.each(data, function(index, val) {
									if(val.vendor_image == null || val.vendor_image == ''){
										val.vendor_image = "images/user_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.vendor_image+'">';
									html+= '<tr><td data="'+val.vendors_id+'">'+image_html+' '+ val.vendors_name+' -- '+val.vendors_phone_1+'</td></tr>';

								});
								$("#vendor_select_result").html(html);
							});
						}, 500);
					});
					$("#vendor_select_result").on('click', 'td', function(event) {
						$('input[name="vendors_name"]').val($(this).text());
						$('input[name="vendors_id"]').val($(this).attr('data'));
						$("#vendor_select_result").hide();
					});
					/*Select Customer Through Customize Boostrap Select*/
					$("#customers_select").keyup(function(event) 
					{
						$("#customers_select_result").show();
						$("#customers_select_result").html('');
						clearTimeout(timer);
						timer = setTimeout(function() 
						{
							var search_customers = $("#customers_select").val();
							var html = '';
							$.post('<?php echo site_url(); ?>/Reports/search_customer_by_name',{q: search_customers,csrf_test_name: csrf}, function(data, textStatus, xhr) {
								data = JSON.parse(data);
								$.each(data, function(index, val) {
									if(val.customers_image == null || val.customers_image == ''){
										val.customers_image = "images/user_images/no_image.png";
									}
									var image_html = '<img height="50" width="50" src="<?php echo base_url(); ?>'+val.customers_image+'">';
									html+= '<tr><td data="'+val.customers_id+'">'+image_html+' '+ val.customers_name+' -- '+val.customers_phone_1+'</td></tr>';
								});
								$("#customers_select_result").html(html);
							});
						}, 500);
					});
					$("#customers_select_result").on('click', 'td', function(event) {
						$('input[name="customers_name"]').val($(this).text());
						$('input[name="customers_id"]').val($(this).attr('data'));
						$("#customers_select_result").hide();
					});

				</script>