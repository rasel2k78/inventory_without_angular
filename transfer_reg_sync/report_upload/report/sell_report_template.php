 <!-- BEGIN PAGE CONTENT-->
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-xs-12">
					<h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
                    <p style="text-align: center;"><?php echo $store_info['address']?></p>
                    <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <p><strong>Report:</strong> Sell Report </p>
                    </div>
                            <div class="col-xs-6">
                        <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Voucher No
                                </th>
                                <th>
                                    Grand Total
                                </th>
                                <th class="hidden-480">
                                    Discount
                                </th>
                                <th class="hidden-480">
                                    Paid
                                </th>
                                <th class="hidden-480">
                                    Due
                                </th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody id="buy_report_template">
        <?php foreach ($sell_data as $v_sell_data): ?>
                                <tr>
                                    <td>
            <?php echo $v_sell_data['sell_local_voucher_no']?>
                                    </td>
                                    <td>
            <?php echo $v_sell_data['grand_total']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_data['discount']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_data['paid']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_data['due']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_data['date_updated']?>
                                    </td>
                                </tr>
        <?php endforeach ?>
                            <tr style="font-weight: bold; font-size: 14px">
                                <td>
                                    Total
                                </td>
                                <td>
            <?php echo $sell_details['grand_total']?>
                                </td>
                                <td class="hidden-480">
            <?php echo $sell_details['discount']?>    
                                </td>
                                <td class="hidden-480">
            <?php echo $sell_details['paid']?>
                                </td>
                                <td class="hidden-480">
            <?php echo $sell_details['due']?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8 invoice-block">
                    <ul class="list-unstyled amounts">
                        <li>
                            <strong>Total Sell Amount:</strong><?php echo nbs(2),$sell_details['grand_total']?>
                        </li>
                        <li>
                            <strong>Total Discount:</strong><?php echo nbs(8),$sell_details['discount']?>
                        </li>
                        <li>
                            <strong>Total Profit:</strong><?php echo nbs(14),$total_profit?>
                        </li>
                        <li>
                            <strong>Total Received:</strong><?php echo nbs(8),$sell_details['paid']?>
                        </li>
                        <li>
                            <strong>Total Receivable:</strong><?php echo nbs(5),$sell_details['due']?>
                        </li>
                    </ul>
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                        Print <i class="fa fa-print"></i>
                    </a>
                 <!--    <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT
