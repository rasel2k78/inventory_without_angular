 <!-- BEGIN PAGE CONTENT-->
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-xs-12">
					<h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
                    <p style="text-align: center;"><?php echo $store_info['address']?></p>
                    <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <p><strong>Report:</strong> Loan Report </p>
                    </div>
                            <div class="col-xs-6">
                        <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Type
                                </th>
                                <th>
                                    Loan Amount
                                </th>
                                <th class="hidden-480">
                                    Payment made
                                </th>
                                <th class="hidden-480">
                                    Date
                                </th>
                                <th class="hidden-480">
                                    Comments
                                </th>
                            </tr>
                        </thead>
                        <tbody id="buy_report_template">
        <?php foreach ($loan_data as $v_loan_data): ?>
                                <tr>
                                    <td>
            <?php echo $v_loan_data['type']?>
                                    </td>
                                    <td>
            <?php echo $v_loan_data['amount']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_loan_data['paymentmade']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_loan_data['date']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_loan_data['comments']?>
                                    </td>
                                </tr>
        <?php endforeach ?>
                            <tr style="font-weight: bold; font-size: 14px">
                                <td>
                                    Total
                                </td>
                                <td>
            <?php echo $loan_details['total_amount']?>
                                </td>
                                <td class="hidden-480">
            <?php echo $loan_details['total_paymentmade']?>    
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8 invoice-block">
                    <ul class="list-unstyled amounts">
                        <li>
                            <strong>Total Loan Amount:</strong> <?php echo nbs(3),$loan_details['total_amount']?>
                        </li>
                        <li>
                            <strong>Total Payment made:</strong>   <?php echo nbs(1),$loan_details['total_paymentmade']?>
                        </li>
                    </ul>
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                        Print <i class="fa fa-print"></i>
                    </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT