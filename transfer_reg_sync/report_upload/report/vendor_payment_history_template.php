 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>

       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Vendor Payment History Report </p>
                <p><strong>Vendor Information:</strong> <?php echo $vendor_data['vendors_name']?> -- <?php echo $vendor_data['vendors_phone_1']?></p>
            </div>
  <!--       <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div> -->
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        Voucher No.
                    </th>
                    <th>
                        Amount
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Action
                    </th>
                </tr>
            </thead>
            <tbody id="customers_list_template">
                <?php foreach ($payment_history as $v_payment_history): ?>
                    <?php foreach ($v_payment_history as $payment_info): ?>
                    <tr>
                        <td>
                            <?php echo $payment_info['voucher_no']?>
                        </td>
                        <td>
                            <?php echo $payment_info['deposit_withdrawal_amount']?>
                        </td>
                        <td>
                            <?php echo $payment_info['date_created']?>
                        </td>
                        <?php if ($payment_info['cash_type']=="buy"): ?>
                            <td>
                            Buy
                        </td>
                        <?php else: ?>
                        <td>
                        Payment made
                        </td>
                        <?php endif ?>
                    </tr>
                    <?php endforeach ?>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
    }
</style>
                <!-- END PAGE CONTENT