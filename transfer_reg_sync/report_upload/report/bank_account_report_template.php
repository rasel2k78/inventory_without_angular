 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>

 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Bank Accounts Report </p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        Bank Name
                    </th>
                    <th>
                        Account Number
                    </th>
                    <th>
                        Account Name
                    </th>
                    <th>
                        Current Balance (Tk)
                    </th>
                </tr>
            </thead>
            <tbody id="item_invtory_list_template">
                <?php foreach ($account_balance_data as $val_acc_data): ?>
                    <tr>
                        <td>
                            <?php echo $val_acc_data['banks_name']?>
                        </td>
                        <td>
                            <?php echo $val_acc_data['bank_acc_num']?>
                        </td>
                        <td>
                            <?php echo $val_acc_data['bank_acc_name']?>
                        </td>
                        <td>
                            <?php echo $val_acc_data['final_amount']?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
       <ul class="list-unstyled amounts">
        <li>
        <strong>Total Amount:</strong> <?php echo nbs(2), $total_amount['total_amount']?> TK
        </li>

    </ul>
    <br/>
    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
        Print <i class="fa fa-print"></i>
    </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
    }
</style>
                <!-- END PAGE CONTENT