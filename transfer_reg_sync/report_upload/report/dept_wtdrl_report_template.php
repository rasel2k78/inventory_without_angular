 <!-- BEGIN PAGE CONTENT-->
 <div class="portlet light">
   <div class="portlet-body">
      <div class="invoice">
         <div class="row">
            <div class="col-xs-12">
               <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
               <p style="text-align: center;"><?php echo $store_info['address']?></p>
               <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
           </div>
       </div>
       <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-6">
                <p><strong>Report:</strong> Deposit & Withdrawal Report </p>
            </div>
            <div class="col-xs-6">
                <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
            </div>
        </div>
        <div class="col-xs-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            Type
                        </th>
                        <th>
                            Amount
                        </th>
                        <th class="hidden-480">
                            Date
                        </th>
                        <th class="hidden-480">
                            By
                        </th>
                        <th class="hidden-480">
                            Comments
                        </th>
                    </tr>
                </thead>
                <tbody id="dpst_wtdrl_report_template">
                    <?php foreach ($deposit_wtdrl_data as $v_dpst_wtdrl_data): ?>
                        <tr>
                            <?php if ($v_dpst_wtdrl_data['cash_type']=="deposit"): ?>
                                <td>
                                    Deposit
                                </td>
                            <?php endif ?>
                            <?php if ($v_dpst_wtdrl_data['cash_type']=="withdrawal"): ?>
                               <td>
                                Withdrawal
                            </td>
                        <?php endif ?>
                        <td>
                            <?php echo $v_dpst_wtdrl_data['deposit_withdrawal_amount']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $v_dpst_wtdrl_data['date']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $v_dpst_wtdrl_data['name']?>
                        </td>
                        <td class="hidden-480">
                            <?php echo $v_dpst_wtdrl_data['cash_description']?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <strong>Total Deposit Amount:</strong> <?php echo nbs(7), round($deposit_details['amount'],2)?>
            </li>
            <li>
                <strong>Total Withdrawal Amount:</strong>   <?php echo nbs(2),round($withdrawal_details['amount'],2)?>
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
                  <!--   <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
    @page {
        size: auto;   /* auto is the initial value */
        margin: 0;   /*this affects the margin in the printer settings */
    }
</style>
                <!-- END PAGE CONTENT