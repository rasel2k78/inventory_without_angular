 <!-- BEGIN PAGE CONTENT--> 
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-xs-12">
					<h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
                    <p style="text-align: center;"><?php echo $store_info['address']?></p>
                    <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <p><strong>Report:</strong> Sell Due Report </p>
                    </div>
                              <div class="col-xs-6">
                        <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    Customer Name
                                </th>
                                   <th>
                                   Phone
                                </th>
                                <th>
                                    Voucher
                                </th>
                                <th>
                                    Grand Total
                                </th>
                                <th class="hidden-480">
                                    Paid
                                </th>
                                <th class="hidden-480">
                                    Due
                                </th>
                                <th class="hidden-480">
                                    Date
                                </th>
                            </tr>
                        </thead>
                        <tbody id="buy_report_template">
        <?php foreach ($sell_due_data as $v_sell_due_data): ?>
                                <tr>
                                    <td>
            <?php echo $v_sell_due_data['customer']?>
                                    </td>
                                    <td>
            <?php echo $v_sell_due_data['phone']?> 
                                    </td>
                                    <td>
            <?php echo $v_sell_due_data['voucher']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_due_data['net_payable']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_due_data['paid']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_due_data['due']?>
                                    </td>
                                    <td class="hidden-480">
            <?php echo $v_sell_due_data['date']?>
                                    </td>
                                </tr>
        <?php endforeach ?>
                            <tr style="font-weight: bold; font-size: 14px">
                                <td>
                                    Total
                                </td>
                                <td></td>
                                <td> <?php echo $sell_due_details['voucher']?></td>
                                <td>
            <?php echo $sell_due_details['total_net_payable']?>
                                </td>
                                <td class="hidden-480">
            <?php echo $sell_due_details['total_paid']?>    
                                </td>
                                <td><?php echo $sell_due_details['total_due']?></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-8 invoice-block">
                    <ul class="list-unstyled amounts">
                     <li>
                            <strong>Total Voucher Number:</strong> <?php echo nbs(2), $sell_due_details['voucher']?>
                        </li>
                        <li>
                            <strong>Total Sell Amount:</strong> <?php echo nbs(9), $sell_due_details['total_net_payable']?>
                        </li>
                        <li>
                            <strong>Total Received:</strong>   <?php echo nbs(14), $sell_due_details['total_paid']?>
                        </li>
                        <li>
                            <strong>Total Receivable:</strong>   <?php echo nbs(12), $sell_due_details['total_due']?>
                        </li>
                    </ul>
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                        Print <i class="fa fa-print"></i>
                    </a>
                <!--     <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT