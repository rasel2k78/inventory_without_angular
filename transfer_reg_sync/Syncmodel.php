<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Syncmodel extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database("maxrailw_inventory");
	}


	public function updateLiveRow($data,$table_name,$columnId,$columnIdValue,$stores_id)
	{

		$this->db->where($columnId, $columnIdValue);
		$this->db->where("stores_id", $stores_id);
		if($this->db->update($table_name, $data)){
			return true;
		}else{
			return false;
		}
	}

	public function insertLiveRow($data,$table_name)
	{	
		return $this->db->insert($table_name, $data);
	}

	public function check_accounts($email)
	{
		$this->registration = $this->load->database('registration',TRUE);
		return $this->registration->get_where('accounts', array('owner_email =' => $email))->row_array();
	}

	public function check_shop($shop_id,$account_id)
	{
		$this->registration = $this->load->database('registration',TRUE);
		return $this->registration->get_where('shop', array('shop_id =' => $shop_id,'account_id =' => $account_id))->row_array();
	}
}

/* End of file Sync_Model.php */
/* Location: ./application/models/Sync_Model.php */