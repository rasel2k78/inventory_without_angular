<?php
/**
 * Report model for generating all types of reports.
 *
 * PHP Version 5.6
 *
 * @copyright copyright@2015
 * @license   MAX Group BD
 * @author    Shoaib <shofik.shoaib@gmail.com>
 * @author    Musabbir Mamun <musabbir.mamun@gmail.com>
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Reports Model includes reports controller.
 *
 * @subpackage Model
 * @author     shoaib <shofik.shoaib@gmail.com>
**/
class Reports_model extends CI_Model {
	public $variable;
    /**
 * This is a constructor function.
 *
 * @return void
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
    public function __construct()
    {
    	parent::__construct();
    	$cookie = $this->input->cookie('language', TRUE);
    	$this->lang->load('model_lang', $cookie);
    }
	/**
 * Generate total buy list report
 *
 * @param array[] $filters
 * @param mix[] $total_count
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
	public function calculate_total_buy($from_create_date,$to_create_date)
	{
		$this->db->select('voucher_no,grand_total,discount,paid,due,DATE_FORMAT(date_created, "%d-%b-%Y") as "date_updated"', FALSE);
		$this->db->from('buy_details');
		$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
		$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
		$this->db->where('publication_status', 'activated');
		return $this->db->get()->result_array();
	}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(paid),2) as "paid",format(sum(grand_total),2) as "grand_total",format(sum(discount),2) as "discount",format(sum(due),2) as "due"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate all sell list report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_sell($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sell_local_voucher_no,grand_total,discount,paid,due,DATE_FORMAT(date_created, "%d-%b-%Y") as "date_updated"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	$this->db->order_by('date_created', 'asc');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(paid),2) as "paid",format(sum(grand_total),2) as "grand_total",format(sum(discount),2) as "discount",format(sum(due),2) as "due", sum(net_payable) as "net_payable"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect store info for report header.
 *
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function store_details($store_id)
{
	$this->db->select('name,address,website,phone', FALSE);
	$this->db->from('shop');
	$this->db->where('shop_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Generate expense report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_expense($from_create_date,$to_create_date)
{
	$this->db->select('
		expenditure_types.expenditure_types_name as "name",
		format(expenditures.expenditures_amount,2) as "amount",
		DATE_FORMAT(expenditures.date_created, "%d-%b-%Y") as "date",
		expenditures.expenditures_comments as "details"
		',false);
	$this->db->from('expenditures');
	$this->db->join('expenditure_types', 'expenditure_types.expenditure_types_id = expenditures.expenditure_types_id', 'left');
	$this->db->where('date_format(expenditures.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(expenditures.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('expenditures.publication_status','activated');
	return $this->db->get()->result_array();
}

public function calculate_total_warehouse_send($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('
		item_spec_set.spec_set as "item",
		transfer.quantity as "qty",
		transfer.buying_price as "buying_price",
		DATE_FORMAT(transfer.date_created, "%d-%b-%Y") as "date",
		');
	$this->db->from('transfer');
	$this->db->join('transfer_details', 'transfer.transfer_details_id = transfer_details.transfer_details_id', 'left');

	$this->db->join('item_spec_set', 'transfer.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('transfer_details.transfer_from',$stores_id);
	$this->db->where('transfer_details.publication_status','activated');
	$this->db->where('transfer.stores_id',$stores_id);
	$this->db->where('transfer.transfer_type','warehouse');
	
	$this->db->group_by('item_spec_set.spec_set');
	return $this->db->get()->result_array();
}

public function total_warehouse_send_details($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('format(sum(transfer.buying_price),2) as "price",format(sum(transfer.quantity),2) as "total_qty"', FALSE);
	$this->db->from('transfer');
	$this->db->join('transfer_details', 'transfer.transfer_details_id = transfer_details.transfer_details_id', 'left');
	$this->db->where('transfer_details.transfer_from',$stores_id);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('transfer_details.publication_status', 'activated');
	$this->db->where('transfer.stores_id', $stores_id);
	$this->db->where('transfer.transfer_type', 'warehouse');
	return $this->db->get()->row_array();
}


public function get_all_exchange_history($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('exchange_return_history.voucher_no,exchange_return_history.type,exchange_return_history.item_spec_set_id,exchange_return_history.quantity,exchange_return_history.date_created,item_spec_set.spec_set,DATE_FORMAT(exchange_return_history.date_created, "%d-%b-%Y") as "date",');
	$this->db->from('exchange_return_history');
	$this->db->join('item_spec_set', 'exchange_return_history.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
	$this->db->where('date_format(exchange_return_history.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(exchange_return_history.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('exchange_return_history.publication_status','activated');
	$this->db->where('exchange_return_history.stores_id',$stores_id);
	return $this->db->get()->result_array();
}

public function get_all_return_history($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('exchange_return_history.voucher_no,exchange_return_history.type,exchange_return_history.item_spec_set_id,exchange_return_history.quantity,exchange_return_history.date_created,item_spec_set.spec_set,DATE_FORMAT(exchange_return_history.date_created, "%d-%b-%Y") as "date",');
	$this->db->from('exchange_return_history');
	$this->db->join('item_spec_set', 'exchange_return_history.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
	$this->db->where('date_format(exchange_return_history.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(exchange_return_history.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('exchange_return_history.publication_status','activated');
	$this->db->where('exchange_return_history.stores_id',$stores_id);
	return $this->db->get()->result_array();
}


public function calculate_total_warehouse_receive($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('
		item_spec_set.spec_set as "item",
		transfer.quantity as "qty",
		transfer.buying_price as "buying_price",
		DATE_FORMAT(transfer.date_created, "%d-%b-%Y") as "date",
		');
	$this->db->from('transfer');
	$this->db->join('transfer_details', 'transfer.transfer_details_id = transfer_details.transfer_details_id', 'left');

	$this->db->join('item_spec_set', 'transfer.item_spec_set_id = item_spec_set.item_spec_set_id', 'left');
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('transfer_details.transfer_to',$stores_id);
	$this->db->where('transfer_details.publication_status','activated');
	$this->db->where('transfer.transfer_type','warehouse');
	
	$this->db->group_by('item_spec_set.spec_set');
	return $this->db->get()->result_array();
}

public function total_warehouse_receive_details($from_create_date,$to_create_date,$stores_id)
{
	$this->db->select('format(sum(transfer.buying_price),2) as "price",format(sum(transfer.quantity),2) as "total_qty"', FALSE);
	$this->db->from('transfer');
	$this->db->join('transfer_details', 'transfer.transfer_details_id = transfer_details.transfer_details_id', 'left');
	$this->db->where('transfer_details.transfer_to',$stores_id);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(transfer.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('transfer_details.publication_status', 'activated');
	$this->db->where('transfer.transfer_type', 'warehouse');
	return $this->db->get()->row_array();
}


/**
 * Generate total amount of expense for report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_expense_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(expenditures_amount),2) as "total_expense"', FALSE);
	$this->db->from('expenditures');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate all damage lost report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_damage_lost($from_create_date,$to_create_date)
{
	$this->db->select('
		item_spec_set.spec_set as "item",
		demage_lost.demage_lost_quantity as "qty",
		DATE_FORMAT(demage_lost.date_created, "%d-%b-%Y") as "date",
		demage_lost.demage_lost_description as "comments",
		');
	$this->db->from('demage_lost');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = demage_lost.item_spec_set_id', 'left');
	$this->db->where('date_format(demage_lost.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(demage_lost.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('demage_lost.publication_status','activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of paid,due,discount,grand total
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_damage_lost_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(demage_lost_quantity),2) as "total_qty"', FALSE);
	$this->db->from('demage_lost');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate loan report
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_loan($from_create_date,$to_create_date)
{
	$this->db->select('loan_type as "type"', FALSE);
	$this->db->select('
		format(loan_amount,2) as "amount",
		format(total_payout_amount,2) as "paymentmade",
		DATE_FORMAT(date_created, "%d-%b-%Y") as "date",
		loan_description as "comments",
		',false);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status','activated');
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of loan taken and payment made
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_loan_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(loan_amount),2) as "total_amount", format(sum(total_payout_amount),2) as "total_paymentmade"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Generate buy due list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_buy_due($from_create_date,$to_create_date)
{
	$no_vendor = "Empty";
	$this->db->select('IFNULL(CAST(vendors.vendors_name AS CHAR) , "'.$no_vendor.'" ) as "vendor"', FALSE);
	$this->db->select('
		buy_details.voucher_no as "voucher",
		format(buy_details.net_payable,2) as "net_payable",
		format(buy_details.paid,2) as "paid",
		format(buy_details.due,2) as "due",
		DATE_FORMAT(buy_details.date_created, "%d-%b-%Y") as "date"
		',false);
	$this->db->from('buy_details');
	$this->db->join('vendors', 'vendors.vendors_id = buy_details.vendors_id', 'left');
	$this->db->where('date_format(buy_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(buy_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('buy_details.publication_status','activated');
	$this->db->where('buy_details.due >', 0);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of net payable , paid and due
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_due_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(net_payable),2) as "total_net_payable", format(sum(paid),2) as "total_paid",  format(sum(due),2) as "total_due", format(count(voucher_no),2) as "voucher_no"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('due >', 0);
	return $this->db->get()->row_array();
}
/**
 * Generate sell due list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_sell_due($from_create_date,$to_create_date)
{
	$no_vendor = "Empty";
	$this->db->select('IFNULL(CAST(customers.customers_name AS CHAR) , "'.$no_vendor.'" ) as "customer"', FALSE);
	$this->db->select('
		customers.customers_phone_1 as "phone",
		sell_details.sell_local_voucher_no as "voucher",
		format(sell_details.net_payable,2) as "net_payable",
		format(sell_details.paid,2) as "paid",
		format(sell_details.due,2) as "due",
		DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "date"
		',false);
	$this->db->from('sell_details');
	$this->db->join('customers', 'customers.customers_id = sell_details.customers_id', 'left');
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_details.publication_status','activated');
	$this->db->where('sell_details.due >', 0);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of net payable , paid and due
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_due_details($from_create_date,$to_create_date)
{
	$this->db->select('format(sum(net_payable),2) as "total_net_payable", format(sum(paid),2) as "total_paid",  format(sum(due),2) as "total_due", format(count(sell_local_voucher_no),2) as "voucher"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('due >', 0);
	return $this->db->get()->row_array();
}
/**
 * Generate top revenue items list report.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_top_rev_items($from_create_date,$to_create_date,$search_limit)
{
	$this->db->select('item_spec_set.spec_set as "item_name",format(sum(sell_cart_items.sub_total),2) as "total_amount",sum(sell_cart_items.quantity) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'left');
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_cart_items.publication_status','activated');
	$this->db->group_by('sell_cart_items.item_spec_set_id');
	$this->db->order_by('sum(sell_cart_items.sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->result_array();
}
/**
 * Generate total sum of revenue amount and quantity
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function top_rev_items_details($from_create_date,$to_create_date,$search_limit)
{
	$this->db->select('format(sum(sub_total),2) as "total_amount", format(sum(quantity),2) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status','activated');
	$this->db->order_by('sum(sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->row_array();
}
/**
 * Generate purchase list report in a date range.
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_top_profit_items_for_buy($from_create_date,$to_create_date)
{
	$this->db->select('item_spec_set.spec_set,sum(buy_cart_items.quantity),format(sum(buy_cart_items.buying_price),2),', FALSE);
	$this->db->from('buy_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->join('sell_cart_items', 'sell_cart_items.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->where('buy_cart_items.date_created >=', $from_create_date);
	$this->db->where('buy_cart_items.date_created <=', $to_create_date);
	$this->db->where('buy_cart_items.publication_status', 'activated');
	return $this->db->get()->result_array();
}
/**
 * Collect total buy amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_buy_amount($from_create_date,$to_create_date)
{
	$this->db->select('sum(paid) as "paid"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total sell amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_sell_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(paid) as "paid"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect total loan amount taken in a specific date range for profit loss calculation
 *
 * @param string $total_damage_amt
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_loan_amt($from_create_date,$to_create_date)
{
	$this->db->select('sum(loan_amount) as "loan_amount"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total loan payment-made amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_loan_paid_amt($from_create_date,$to_create_date)
{
	$this->db->select('sum(total_payout_amount) as "total_payout_amount"', FALSE);
	$this->db->from('loan');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total deposit amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_deposit_amt($from_create_date,$to_create_date)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "deposit_withdrawal_amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('cash_type', 'deposit');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total withdrawal amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_withdrawal_amt($from_create_date,$to_create_date)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "deposit_withdrawal_amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('cash_type', 'withdrawal');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}
/**
 * Collect total damage lost amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_damage_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(sub_total) as "damage_lost_amount"', FALSE);
	$this->db->from('demage_lost');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect total expense amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_expense_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(expenditures_amount) as "expenditures_amount"', FALSE);
	$this->db->from('expenditures');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect total vat amount in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_vat_amount($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(sum(vat_amount),2) as "vat_amount"', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect total landed cost in a specific date range for profit loss calculation
 *
 * @param string $from_create_date
 * @param string $to_create_date
 * @return array[]
 * @link Central_reports/print_report_preview
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/ 
public function total_landed_cost_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(landed_cost) as "landed_cost"', FALSE);
	$this->db->from('buy_details');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
/**
 * Collect top categories with amount within a date range.
 *@param string $from_create_date
 *@param string $to_create_date
 *@param string $search_limit
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_top_rev_category($from_create_date,$to_create_date,$search_limit,$store_id)
{
	$this->db->select('categories.categories_name as "category",format(sum(sell_cart_items.sub_total),2) as "total_amount",sum(sell_cart_items.quantity) as "total_qty"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = sell_cart_items.item_spec_set_id', 'right');
	$this->db->join('items', 'items.items_id = item_spec_set.items_id', 'right');
	$this->db->join('categories', 'categories.categories_id = items.categories_id', 'right');
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_cart_items.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_cart_items.publication_status','activated');
	$this->db->where('sell_cart_items.stores_id', $store_id);
	$this->db->group_by('categories.categories_id');
	$this->db->order_by('sum(sell_cart_items.sub_total)', 'desc');
	$this->db->limit($search_limit);
	return $this->db->get()->result_array();
}
/**
 * Collect the items which are available in inventory with current quantity
 *@param string $from_create_date
 *@param string $to_create_date
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_items_inventory($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('inventory.quantity,item_spec_set.spec_set ', FALSE);
	$this->db->from('inventory');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = inventory.item_spec_set_id', 'left');
	// $this->db->where('buy_cart_items.date_created >=', $from_create_date);
	// $this->db->where('buy_cart_items.date_created <=', $to_create_date);
	// $this->db->where('buy_cart_items.stores_id', $store_id);
	// $this->db->where('buy_cart_items.publication_status', 'activated');
	$this->db->where('inventory.quantity >', 0);
	$this->db->where('inventory.stores_id', $store_id);
	return $this->db->get()->result_array();
}
/**
 * Collect total vat amount of sell voucher within a specific date range.
 *@param string $from_create_date
 *@param string $to_create_date
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_vat($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('format(vat_amount,2) as "vat", sell_local_voucher_no', FALSE);
	$this->db->from('sell_details');
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('vat_amount >', 0);
	$this->db->where('stores_id', $store_id);
	$this->db->group_by('sell_local_voucher_no');
	$this->db->order_by('vat_amount', 'desc');
	return $this->db->get()->result_array();

}
/**
 * Collect the data of all customers with purchased amount.
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function all_customers_list($store_id)
{
	$this->db->select('customers.customers_name,customers.customers_phone_1,customers.customers_present_address,customers.customers_email,format(sum(sell_details.net_payable),2) as "net_payable"', FALSE);
	$this->db->from('customers');
	$this->db->join('sell_details', 'sell_details.customers_id = customers.customers_id', 'left');
	$this->db->where('customers.publication_status', 'activated');
	$this->db->where('sell_details.publication_status', 'activated');
	$this->db->where('customers.stores_id', $store_id);
	$this->db->group_by('customers.customers_id');
	return $this->db->get()->result_array();
}
/**
 * Collect the data of all vendors with amount.
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function all_vendors_list($store_id)
{
	$this->db->select('vendors_name,vendors_present_address,vendors_phone_1,vendors_email', FALSE);
	$this->db->from('vendors');
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}
/**
 * Collect sum of total deposit and withdrawal amount.
 *@param string $from_create_date
 *@param string $to_create_date
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function calculate_total_dpst_wtdrl($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('cashbox.deposit_withdrawal_amount,cashbox.cash_description,users.name,cashbox.cash_type,DATE_FORMAT(cashbox.date_created, "%d-%b-%Y") as "date"', FALSE);
	$this->db->from('cashbox');
	$this->db->join('users', 'users.user_id = cashbox.user_id', 'left');
	$this->db->where('date_format(cashbox.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(cashbox.date_created, "%Y-%m-%d") <=', $to_create_date);
	$type = array('deposit', 'withdrawal');
	$this->db->where_in('cashbox.cash_type', $type);	
	$this->db->where('cashbox.publication_status','activated');
	$this->db->where('cashbox.stores_id', $store_id);
	$this->db->order_by('cashbox.date_created', 'asc');
	return $this->db->get()->result_array();
}
/**
 * Collect sum of total deposit amount.
 *@param string $from_create_date
 *@param string $to_create_date
 *@link Reports/print_report_preview
 * @return array[]
 * @author shoaib <shofik.shoaib@gmail.com>
 **/
public function total_dpst_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	$this->db->where('cash_type', 'deposit');
	return $this->db->get()->row_array();
}
/**
 * Collect sum of total withdrawal amount.
 *@param string $from_create_date
 *@param string $to_create_date
 *@link Reports/print_report_preview
 * @return array[]
 * @author Shoaib <shofik.shoaib@gmail.com>
 **/
public function total_wtdrl_details($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(deposit_withdrawal_amount) as "amount"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	$this->db->where('cash_type', 'withdrawal');
	return $this->db->get()->row_array();
}

// public function calculate_amt_of_inv_items()
// {

// }
public function available_items_info($store_id)
{
	$this->db->select('item_spec_set_id', FALSE);
	$this->db->from('inventory');
	$this->db->where('quantity >', 0);
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function buy_info_by_id($item_id,$store_id)
{
	$this->db->select('buy_cart_items.quantity,buy_cart_items.buying_price,buy_cart_items.item_spec_set_id,item_spec_set.spec_set', FALSE);
	$this->db->from('buy_cart_items');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = buy_cart_items.item_spec_set_id', 'left');
	$this->db->where('buy_cart_items.item_spec_set_id', $item_id);
	$this->db->where('buy_cart_items.publication_status', 'activated');
	$this->db->where('buy_cart_items.stores_id', $store_id);
	$this->db->order_by('buy_cart_items.date_created', 'asc');
	return $this->db->get()->result_array();
}

public function sell_info_by_id($item_id,$store_id)
{
	$this->db->select('quantity,selling_price,item_spec_set_id', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->where('item_spec_set_id', $item_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	$this->db->order_by('date_created', 'asc');
	return $this->db->get()->result_array();
}

public function damage_Lost_info_by_id($item_id,$store_id)
{
	$this->db->select('demage_lost_quantity', FALSE);
	$this->db->from('demage_lost');
	$this->db->where('item_spec_set_id', $item_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function all_buy_id_by_vendors($vendors_id,$store_id)
{
	$this->db->select('buy_details_id', FALSE);
	$this->db->from('buy_details');
	$this->db->where('vendors_id', $vendors_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}
public function all_sell_id_by_vendors($customer_id,$store_id)
{
	$this->db->select('sell_details_id', FALSE);
	$this->db->from('sell_details');
	$this->db->where('customers_id', $customer_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function all_payment_history_info($buy_id,$store_id)
{
	$payment_data= array();
	foreach ($buy_id as $val_buy_id) {
		$temp_buy_id = $val_buy_id['buy_details_id'];
		$this->db->select('cashbox.deposit_withdrawal_amount,cashbox.buy_or_sell_details_id,cashbox.cash_type,buy_details.voucher_no,DATE_FORMAT(cashbox.date_created, "%d-%b-%Y") as "date_created"', FALSE);
		$this->db->from('cashbox');
		$this->db->join('buy_details', 'buy_details.buy_details_id = cashbox.buy_or_sell_details_id', 'left');
		$search_cash_type = array('buy_due_paymentmade', 'buy');
		$this->db->where_in('cashbox.cash_type', $search_cash_type);
		$this->db->where('cashbox.deposit_withdrawal_amount >', 0);
		$this->db->where('cashbox.date_created >', '2017-07-17');
		$this->db->where('cashbox.buy_or_sell_details_id', $temp_buy_id);
		$this->db->where('cashbox.stores_id', $store_id);
	// $this->db->order_by('cashbox.date_created', 'asc');
		$data = $this->db->get()->result_array();
		array_push($payment_data, $data);
	}
	return $payment_data;
}
public function all_sell_payment_history_info($sell_id,$store_id)
{
	$payment_data= array();
	foreach ($sell_id as $val_sell_id) {
		$temp_sell_id = $val_sell_id['sell_details_id'];
		$this->db->select('cashbox.deposit_withdrawal_amount,cashbox.buy_or_sell_details_id,cashbox.cash_type,sell_details.sell_local_voucher_no,DATE_FORMAT(cashbox.date_created, "%d-%b-%Y") as "date_created"', FALSE);
		$this->db->from('cashbox');
		$this->db->join('sell_details', 'sell_details.sell_details_id = cashbox.buy_or_sell_details_id', 'left');
		$search_cash_type = array('sell_due_paymentmade', 'sell');
		$this->db->where_in('cashbox.cash_type', $search_cash_type);
		$this->db->where('cashbox.date_created >', '2017-07-20');
		$this->db->where('cashbox.buy_or_sell_details_id', $temp_sell_id);
		$this->db->where('cashbox.stores_id', $store_id);
		$data = $this->db->get()->result_array();
		array_push($payment_data, $data);
	}
	return $payment_data;
}


public function get_vendor_info_by_id($vendor_id,$store_id)
{
	$this->db->select('vendors_name,vendors_phone_1', FALSE);
	$this->db->from('vendors');
	$this->db->where('vendors_id', $vendor_id);
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}
public function get_customer_info_by_id($customer_id,$store_id)
{
	$this->db->select('customers_name,customers_phone_1', FALSE);
	$this->db->from('customers');
	$this->db->where('customers_id', $customer_id);
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}

public function all_sell_items_list($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('DISTINCT item_spec_set_id, sum(quantity) as "total_quantity"', FALSE);
	$this->db->from('sell_cart_items');
	$this->db->group_by('item_spec_set_id');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function get_avg_buying_price_info($selected_item_sped_id, $to_create_date,$store_id)
{
	$this->db->select('round(avg(buying_price),2) as "avg_price",item_spec_set_id', FALSE);
	$this->db->from('buy_cart_items');
	$condition_array = array('item_spec_set_id' => $selected_item_sped_id, 'date_format(date_updated, "%Y-%m-%d") <=' => $to_create_date, 'publication_status' => 'activated', 'stores_id' => $store_id);
	$this->db->where($condition_array);
	return $this->db->get()->row_array();
}

public function get_available_imei_info($store_id)
{
	$this->db->select('imei_barcode.imei_barcode,imei_barcode.item_spec_set_id,item_spec_set.spec_set', FALSE);
	$this->db->from('imei_barcode');
	$this->db->join('item_spec_set', 'item_spec_set.item_spec_set_id = imei_barcode.item_spec_set_id', 'left');
	$condition_array = array('imei_barcode.publication_status' => 'activated', 'imei_barcode.barcode_status'=>'not_used', 'imei_barcode.stores_id' => $store_id);
	$this->db->where($condition_array);
	$this->db->order_by('item_spec_set.spec_set', 'asc');
	return $this->db->get()->result_array();
}

public function calculate_bank_account_balance()
{
	$this->db->select('bank_accounts.final_amount,bank_accounts.bank_acc_num,banks.banks_name,bank_accounts.bank_acc_name', FALSE);
	$this->db->from('bank_accounts');
	$this->db->join('banks', 'banks.banks_id = bank_accounts.banks_id', 'left');
	$this->db->where('bank_accounts.publication_status', 'activated');
	$this->db->order_by('banks.banks_name', 'asc');
	return $this->db->get()->result_array();
}

public function total_amounts_details()
{
	$this->db->select('format(sum(final_amount),2) as "total_amount"', FALSE);
	$this->db->from('bank_accounts');
	$this->db->where('publication_status', 'activated');
	return $this->db->get()->row_array();
}

public function get_card_payment_info($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sell_payment_details.amount, sell_details.sell_local_voucher_no, DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "date" ', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->join('sell_details', 'sell_details.sell_details_id = sell_payment_details.sell_details_id', 'left');
	$this->db->where('sell_payment_details.payment_type', 'card');
	$this->db->where('sell_payment_details.publication_status', 'activated');
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_payment_details.stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function get_card_amout_for_cash_card_payment_info($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('bank_statement.deposit_withdrawal_amount, sell_details.sell_local_voucher_no, DATE_FORMAT(sell_details.date_created, "%d-%b-%Y") as "date" ', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->join('bank_statement', 'bank_statement.buy_sell_and_other_id = sell_payment_details.sell_details_id', 'left');
	$this->db->join('sell_details', 'sell_details.sell_details_id = sell_payment_details.sell_details_id', 'left');
	$this->db->where('sell_payment_details.payment_type', 'cash_card_both');
	$this->db->where('sell_payment_details.publication_status', 'activated');
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_payment_details.stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function get_total_card_collection($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(amount) as "card_amount"', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->where('payment_type', 'card');
	$this->db->where('publication_status', 'activated');
	$this->db->where('date_format(date_creted, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_creted, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->row_array();
}

public function get_cash_card_collection($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(bank_statement.deposit_withdrawal_amount) as "cash_card_both_amt"', FALSE);
	$this->db->from('sell_payment_details');
	$this->db->join('bank_statement', 'bank_statement.buy_sell_and_other_id = sell_payment_details.sell_details_id', 'left');
	$this->db->where('sell_payment_details.payment_type', 'cash_card_both');
	$this->db->where('sell_payment_details.publication_status', 'activated');
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_payment_details.date_creted, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_payment_details.stores_id', $store_id);
	return $this->db->get()->row_array();
}

public function transfer_info_by_id($item_id,$store_id)
{
	$this->db->select('quantity', FALSE);
	$this->db->from('transfer');
	$this->db->where('item_spec_set_id', $item_id);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	return $this->db->get()->result_array();
}

public function total_commission_amt($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('sum(cash_type) as "commission"', FALSE);
	$this->db->from('cashbox');
	$this->db->where('cash_type', 'commission');
	$this->db->where('date_format(date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('publication_status', 'activated');
	$this->db->where('stores_id', $store_id);
	$commission_amt= $this->db->get()->row_array();
	return $commission_amt['commission'];
}

public function get_sales_rep_by_id($from_create_date,$to_create_date,$store_id)
{
	$this->db->select('count(sell_details.sell_details_id) as "total_sale_qty",sales_representative.sales_rep_name, sum(net_payable) as "total_sale_amt"',FALSE);
	$this->db->from('sell_details');
	$this->db->join('sales_representative', 'sales_representative.sales_rep_id = sell_details.sales_rep_id', 'left');
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") >=', $from_create_date);
	$this->db->where('date_format(sell_details.date_created, "%Y-%m-%d") <=', $to_create_date);
	$this->db->where('sell_details.stores_id', $store_id);
	$this->db->group_by('sell_details.sales_rep_id');
	return $this->db->get()->result_array();
}

}

/* End of file Reports_model.php */
/* Location: ./application/models/Reports_model.php */