 <!-- BEGIN PAGE CONTENT--> 
 <div class="portlet light">
     <div class="portlet-body">
      <div class="invoice">
       <div class="row">
        <div class="col-xs-12">
         <h3 style="text-align: center;"><?php echo $store_info['name']?></h3>
         <p style="text-align: center;"><?php echo $store_info['address']?></p>
         <p style="text-align: center;">    Phone: <?php echo $store_info['phone']?>, Website: <?php echo $store_info['website']?></p>
     </div>
 </div>
 <div class="row">
    <div class="col-xs-12">
        <div class="col-xs-6">
            <p><strong>Report:</strong> Warehouse Receive Report </p>
        </div>
        <div class="col-xs-6">
            <p class="pull-right"><strong>From:</strong> <?php echo date('d-M-Y', strtotime($date_from));?> <strong>To:</strong>  <?php echo date('d-M-Y', strtotime($date_to));?></p>
        </div>
    </div>
    <div class="col-xs-12">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>
                        Item
                    </th>
                    <th>
                        Buying Price
                    </th>
                    <th class="hidden-480">
                        Quantity
                    </th>
                    <th class="hidden-480">
                        Date
                    </th>
                </tr>
            </thead>
            <tbody id="buy_report_template">
                <?php foreach ($wareouse_receive_data as $v_expense_data): ?>
                    <tr>
                        <td><?php echo $v_expense_data['item']?></td>
                        <td><?php echo $v_expense_data['buying_price']?></td>
                        <td><?php echo $v_expense_data['qty']?></td>
                        <td class="hidden-480"><?php echo $v_expense_data['date']?></td>
                    </tr>
                <?php endforeach ?>
                <tr style="font-weight: bold; font-size: 14px">
                    <td>Total</td>
                    <td><?php echo $warehouse_receive_details['price']?></td>
                    <td><?php echo $warehouse_receive_details['total_qty']?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
    </div>
    <div class="col-xs-8 invoice-block">
        <ul class="list-unstyled amounts">
            <li>
                <!-- <strong>Total Expense:</strong> <?php echo $warehouse_send_details['total_qty']?> -->
            </li>
        </ul>
        <br/>
        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
            Print <i class="fa fa-print"></i>
        </a>
                 <!--    <a class="btn btn-lg green hidden-print margin-bottom-5">
                        Save as PDF <i class="fa fa-file-pdf-o"></i>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;   /*this affects the margin in the printer settings */
}
</style>
                <!-- END PAGE CONTENT