<?php if (! defined('BASEPATH')) { exit('No direct script access allowed');
}
class Database_update extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {


        $this->db->trans_begin();
        $this->load->dbforge();
        if ($this->db->table_exists('db_version') ) {

            $result = $this->db->select('*')->from('db_version')->get()->result_array();
            if(count($result)==0) {
                $version = 'db00000000';
            }
            else
            {
                $version = $result[count($result)-1]['version_name'];
            }
        }
        else
        {
            $fields = array(
               'version_id' => array(
                  'type' => 'INT',
                  'unsigned' => true,
                  'auto_increment' => true
                  ),
               'version_name' => array(
                  'type' => 'VARCHAR',
                  'constraint' => '45',
                  'unique' => true,
                  ),
               'create_date' => array(
                  'type' => 'TIMESTAMP'
                  )
               );
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('version_id', true);
            $this->dbforge->create_table('db_version');
            $version = 'db00000000';
        }
        $db_array = array();
        
        //Query starts
        $dbarray['db20170103'] = "ALTER TABLE `demage_lost` ADD `item_buying_price` FLOAT NOT NULL AFTER `demage_lost_type`;ALTER TABLE `demage_lost` ADD `sub_total` FLOAT NOT NULL AFTER `item_buying_price`;CREATE TRIGGER `demage_lost_AFTER_INSERT` AFTER INSERT ON `demage_lost` FOR EACH ROW INSERT INTO sync (table_name, row_id, type, local_time,pkey_column_name) values ('demage_lost', NEW.demage_lost_id, 'insert', NOW(),'demage_lost_id');CREATE TRIGGER `demage_lost_AFTER_UPDATE` AFTER UPDATE ON `demage_lost` FOR EACH ROW  INSERT INTO sync (table_name, row_id, type, local_time, pkey_column_name) values ('expenditure_types', NEW.demage_lost_id, 'update', NOW(), 'demage_lost_id');"; //Written by Rasel on 
         /*
        Query Ends
        */
        $latest_version = "";
        $counter = 0;
        foreach ($dbarray as $key=>$value) {

            if($key>$version) {
                if($value=="") {
                    $latest_version = $key;
                }
                else
                {
                    $queries = explode(';', $value);
                    foreach ($queries as $k => $query)
                    {
                        $this->db->query($query);
                    }
                }
            }

            $counter++;
            if($counter==sizeof($dbarray)) {
                $latest_version = $key;
            }
        }

        if($version==$latest_version) {
            $this->db->trans_rollback();
            //return "Already up to date";
            echo "Already up to date";
            exit;
        }

        $version_data = array(
           'version_name' => $latest_version
           );
        

        $this->db->insert('db_version', $version_data);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }
}
/* End of file  */
/* Location: ./application/controllers/ */